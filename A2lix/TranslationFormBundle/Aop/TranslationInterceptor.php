<?php

namespace A2lix\TranslationFormBundle\Aop;

use CG\Proxy\MethodInvocation,
    CG\Proxy\MethodInterceptorInterface;
use Gedmo\Translatable\TranslatableListener;

/**
 * @author David ALLIX
 */
class TranslationInterceptor implements MethodInterceptorInterface
{
    private $translatableListener;

    /**
     * @param TranslatableListener $translatableListener
     */
    public function __construct(TranslatableListener $translatableListener)
    {
        $this->translatableListener = $translatableListener;
    }

    public function intercept(MethodInvocation $invocation)
    {
        
        /**
         * @todo - fix it if need. This commented by reason of not change external trans. logic.
         */
        // Force default locale
        //$this->translatableListener->setTranslatableLocale($this->translatableListener->getDefaultLocale());
//$this->translatableListener->setTranslatableLocale('lv');

        return $invocation->proceed();
    }
}