<?php

namespace A2lix\TranslationFormBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Translations fields of a locale
 *
 * @author David ALLIX
 */
class TranslationsLocaleType extends AbstractType
{
    protected $name;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach($options['fields'] as $fieldName => $fieldConfig) {
            $this->name = $fieldName;
            
            $fieldType = $fieldConfig['type'];
            unset($fieldConfig['type']);

//            if(isset($fieldConfig['required']) && $fieldConfig['required']) {
//                
//                $fieldConfig = $fieldConfig+ array('attr'=>array('required'=>'required'));
//           
//            }
            $builder->add($fieldName, $fieldType, $fieldConfig);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'error_bubbling' => true,
            'fields' => array(),
            'required' => false,
            'type' => 'text',
            'config_name' => ''
        ));
    }

    public function getName()
    {
        return 'a2lix_translationsLocale';
    }
}
