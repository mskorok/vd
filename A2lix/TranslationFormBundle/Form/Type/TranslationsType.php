<?php

namespace A2lix\TranslationFormBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormView,
    Symfony\Component\Form\FormInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;
use A2lix\TranslationFormBundle\TranslationForm\TranslationForm,
    A2lix\TranslationFormBundle\Form\DataMapper\TranslationMapper;

/**
 * Regroup by locales, all translations fields
 *
 * @author David ALLIX
 */
class TranslationsType extends AbstractType
{
    private $translationForm;
    private $locales;
    private $required;

    public function __construct(TranslationForm $translationForm, $locales, $required)
    {
        $this->translationForm = $translationForm;
        $this->locales = $locales;
        $this->required = $required;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translatableConfig = $this->translationForm->initTranslatableConfiguration($builder->getParent()->getDataClass());
        $childrenOptions = $this->translationForm->getChildrenOptions($options);

        $builder->setDataMapper(new TranslationMapper($translatableConfig['translationClass']));

   
        
        foreach ($options['locales'] as $locale) {
            
            $label = $locale;
            $required = 'false';
            $attr=array();
            if(!empty($options['default_locale']) && in_array($locale, $options['default_locale'])) { 
                $required = true;
                $childrenOptions[$locale] = $this->setRequired($childrenOptions[$locale]);
                $attr = array('required'=>'required');
                
            } else if(!empty($options['required_locales']) && in_array($locale, $options['required_locales'])) {

                 $required = true;
                 $childrenOptions[$locale] = $this->setRequired($childrenOptions[$locale]);
                 $attr = array('required'=>'required');
            }
            
            if (isset($childrenOptions[$locale])) {
                
                foreach( $childrenOptions[$locale] as $k => $v){
                    
                     if(isset($options['fields'][$k])) {
                        $childrenOptions[$locale][$k] = $childrenOptions[$locale][$k] + $options['fields'][$k];
                     }

                      if(isset($options['fields'][$k]['type'])) {
                        $childrenOptions[$locale][$k]['type'] = $options['fields'][$k]['type'];
                     }
                     
                    if($childrenOptions[$locale][$k]['label']) {
                        $childrenOptions[$locale][$k]['label'] = $childrenOptions[$locale][$k]['label'] . ' - '.$locale;
                    } else {
                         $childrenOptions[$locale][$k]['label'] = $locale;
                    }
                    
                     //$childrenOptions[$locale][$k]['label'] = $childrenOptions[$locale][$k]['label'] . ' - '.$locale;
                }

              
//                
//        foreach($options['fields'] as $fieldName => $fieldConfig) {
//            $fieldType = $fieldConfig['type'];
//            unset($fieldConfig['type']);
//
//            if($fieldConfig['required']) {
//                
//                $fieldConfig = $fieldConfig+ array('attr'=>array('required'=>'required'));
//           
//            }
//            $builder->add($fieldName, $fieldType, $fieldConfig);
//        }                
                
        
//        foreach($childrenOptions as $k => $v ) {
//            
//            foreach($v as $kk => $vv) {
//                $type = $vv['type'];
//                unset($vv['type']);
//                $builder->add($kk.'_'.$k, $type);
//            }
//        }

           

            

                $builder->add($locale, 'a2lix_translationsLocale',  array(
//                    'fields' => $options['fields'],
                    'fields' => $childrenOptions[$locale],
                    'label' =>false,
                    'required' => $required,
                    'attr' => $attr
                ));
            }
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['default_locale'] =  $options['default_locale'];(array) $this->translationForm->getDefaultLocale();
        $view->vars['locales'] = $options['locales'];
        $view->vars['required_locales'] = $options['required_locales'];
       // $view->vars['required'] = $options['required'];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'locales' => $this->locales,
            'required_locales' => array(),
            'default_locale' => 'en',
            'required' => $this->required,
            'fields' => array(),
            'field' => null,
            'remove_extra_fields' => false,
            'type' => 'text',
            'config_name' => null
        ));
    }

    public function getName()
    {
        return 'a2lix_translations';
    }
    
    private function setRequired($otions, $value = true) {
        
        foreach($otions as $k=>$v) {
            if(isset($v['required'])) {
            //    $otions[$k]['required'] = $value;
            }
        }
        
        return $otions;
    }
    
}
