<?php

namespace VisiDarbi\StatisticsBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;
use VisiDarbi\StatisticsBundle\Entity\Search;

/**
 * Custom EntityRepository for search entities
 *
 */
class SearchEntityRepository extends EntityRepository {

    /**
     * method to get popular search keywords to build tag cloud
     * @param int $countryId
     * @param string $locale
     * @param int $limit
     * @param int $minSearchCount
     * @return array
     */
    /*public function getSearchKeywordListForTagCloud($countryId, $locale,  $limit = 50, $minSearchCount = 3)
    {

        $query = $this->createQueryBuilder('s')
                ->select('s.keyword, COUNT(s.id) as cnt, s.keyword_type')
//                ->andWhere('s.keyword_type = :keyword_type')
                ->groupBy('s.keyword')
                ->having('cnt > :min_search_count')
                ->setMaxResults($limit)
//                ->setParameter('keyword_type', Search::KEYWORD_TYPE_WHERE)
                ->setParameter('min_search_count', $minSearchCount)
                ->getQuery();


        $searchKeywords = $query
            ->useResultCache(true, 600, 'search_keyword_cloud_'.$countryId.'_'.$locale )
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $maxKwOccurenceCount = 0;
        foreach ($searchKeywords as $kwData){
            $maxKwOccurenceCount = $kwData['cnt'] > $maxKwOccurenceCount ? (int)$kwData['cnt'] : $maxKwOccurenceCount;
        }

        foreach ($searchKeywords as $k=>$kwData){
            $searchKeywords[$k]['percent'] = floor(($kwData['cnt'] / $maxKwOccurenceCount) * 100);;
            $searchKeywords[$k]['keyword_title'] = str_replace('-',' ',$searchKeywords[$k]['keyword']);
        }

        shuffle($searchKeywords);

        return $searchKeywords;
    }*/

}
