<?php

namespace VisiDarbi\StatisticsBundle\Twig;

use VisiDarbi\StatisticsBundle\ViewCounter\ViewCounterInterface;
use Twig_Extension;
use Twig_Function_Method;


class StatisticsExtension extends Twig_Extension
{
    /**
     * @var ViewCounterInterface
     */
    private $viewCounter;


    public function __construct(ViewCounterInterface $viewCounter)
    {
        $this->viewCounter = $viewCounter;
    }

    public function getFunctions()
    {
        return array(
            'register_advertisement_view' => new Twig_Function_Method($this, 'registerAdvertisementView'),
            'ga_views_this_week' => new Twig_Function_Method($this, 'getGaViewCount'),

        );
    }

    public function registerAdvertisementView($object)
    {
        $this->viewCounter->registerAdvertisementView($object);
    }

    public function getGaViewCount($object)
    {
        $from = new \DateTime();
        $from->setTimestamp(strtotime('-6 days'));
        $from->setTime(0, 0, 0);

        $to = new \DateTime();

        return (int)$this->viewCounter->getGaViewCount($object, $from, $to);
    }


    public function getName()
    {
        return 'visidarbi_statistics';
    }
}
