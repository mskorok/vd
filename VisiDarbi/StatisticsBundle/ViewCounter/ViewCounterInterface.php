<?php

namespace VisiDarbi\StatisticsBundle\ViewCounter;

interface ViewCounterInterface 
{    
    public function registerAdvertisementView($object);
    public function getGaViewCount($object, \DateTime $from, \DateTime $to);
}