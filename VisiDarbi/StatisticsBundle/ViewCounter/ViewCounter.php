<?php

namespace VisiDarbi\StatisticsBundle\ViewCounter;

use VisiDarbi\StatisticsBundle\Entity\AdvertisementView;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Widop\GoogleAnalytics As Ga;
use Widop\HttpAdapter\CurlHttpAdapter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class ViewCounter implements ViewCounterInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    
    public function __construct(ContainerInterface $container) 
    {   
        $this->container = $container;
    }
    
    public function registerAdvertisementView($object)
    {   
        $request = $this->container->get('request');
        $session = $this->container->get('session');
        $em = $this->container->get('doctrine.orm.entity_manager');
        
        $objClass = $this->getClass($object);
        $objId = $object->getId();
        
        $objectViews = $session->get('object_views', array());
        if (isset($objectViews['advertisements']) && is_array($objectViews['advertisements']) && in_array($objId, $objectViews['advertisements'])) {
            return;
        }

        $view = new AdvertisementView();
        $view->setObjectId($objId);
        $view->setIp($request->getClientIp());

        $em->persist($view);
        $em->flush($view);
        
        $object->setCount($object->getCount() + 1);

        $em->createQuery('UPDATE '. $objClass .' b SET b.views = b.views + 1  WHERE b.id = :id')
            ->setParameter('id', $objId)
            ->execute();

        $objectViews['advertisements'][] = $objId;
        $session->set('object_views', $objectViews);
    }


    public function getGaViewCount($object, \DateTime $from,  \DateTime $to)
    {      
        $configParams = $this->container->getParameter('google'); 
        $cacheDriver = new \Doctrine\Common\Cache\ApcCache(); 

        $accountId = $configParams['analitics']['account_id'];
        $clientId = $configParams['client_id'];
        $privateKeyFile = $configParams['private_key_file'];     
        
        if( !$accountId || !$clientId || !$privateKeyFile ){
            return false;
        }
        

        $ga_views = $cacheDriver->fetch('ga_views');

        if(!$ga_views){

            $profileId = 'ga:'.$accountId;

            $httpAdapter = new CurlHttpAdapter();

            $client = new Ga\Client($clientId, $privateKeyFile, $httpAdapter);
            $token = $client->getAccessToken();

            $query = new Ga\Query($profileId);

            $query->setStartDate($from);
            $query->setEndDate($to);

            $query->setMetrics(array('ga:sessions'));         

            $service = new Ga\Service($client);
            $response = $service->query($query);
            $results = $response->getTotalsForAllResults();

            $ga_views = $results['ga:sessions'];

            $cacheDriver->save('ga_views', $ga_views, 60);
        }

        return $ga_views;
    }
    
    protected function getClass($object)
    {
        return \Doctrine\Common\Util\ClassUtils::getRealClass(get_class($object));
    }
}