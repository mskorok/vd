<?php

namespace VisiDarbi\StatisticsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * SearchHistory
 *
 * @ORM\Entity(repositoryClass="VisiDarbi\StatisticsBundle\EntityRepository\SearchHistoryRepository")
 * @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="keyword_type_idx", columns={"keyword_type"})
 *      },
 *      uniqueConstraints={@ORM\UniqueConstraint(name="keyword_idx", columns={"keyword"})}
 * )
 *
 */
class SearchHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="keyword", type="string", length=150, nullable=false)
     */
    private $keyword;

    /**
     * @var integer
     *
     * @ORM\Column(name="keyword_type", type="integer", nullable=false)
     */
    private $keyword_type;

    /**
     * @var integer
     *
     * @ORM\Column(name="search_count", type="integer", nullable=false)
     */
    private $search_count;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_searched_at", type="datetime", nullable=true)
     */
    private $last_searched_at;

//    /**
//     * @Gedmo\Slug(fields={"keyword"}, unique=true)
//     * @ORM\Column(type="string")
//     */
//    private $slug;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $created_at;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updated_at;

    /**
     * Constructor
     */
    public function __construct() {

        $this->is_highlighted = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return SearchHistory
     */
    public function setKeyword($keyword)
    {
        $this->search_count = 0;
    
        return $this;
    }

    /**
     * Get keyword
     *
     * @return string 
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set keyword_type
     *
     * @param integer $keywordType
     * @return SearchHistory
     */
    public function setKeywordType($keywordType)
    {
        $this->keyword_type = $keywordType;
    
        return $this;
    }

    /**
     * Get keyword_type
     *
     * @return integer 
     */
    public function getKeywordType()
    {
        return $this->keyword_type;
    }

    /**
     * Set search_count
     *
     * @param integer $searchCount
     * @return SearchHistory
     */
    public function setSearchCount($searchCount)
    {
        $this->search_count = $searchCount;
    
        return $this;
    }

    /**
     * Get search_count
     *
     * @return integer 
     */
    public function getSearchCount()
    {
        return $this->search_count;
    }

    /**
     * Set last_searched_at
     *
     * @param \DateTime $lastSearchedAt
     * @return SearchHistory
     */
    public function setLastSearchedAt($lastSearchedAt)
    {
        $this->last_searched_at = $lastSearchedAt;
    
        return $this;
    }

    /**
     * Get last_searched_at
     *
     * @return \DateTime 
     */
    public function getLastSearchedAt()
    {
        return $this->last_searched_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return SearchHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return SearchHistory
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

//    /**
//     * Set slug
//     *
//     * @param string $slug
//     * @return Company
//     */
//    public function setSlug($slug)
//    {
//        $this->slug = $slug;
//
//        return $this;
//    }
//
//    /**
//     * Get slug
//     *
//     * @return string
//     */
//    public function getSlug()
//    {
//        return $this->slug;
//    }
}
