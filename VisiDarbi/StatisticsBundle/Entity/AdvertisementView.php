<?php
namespace VisiDarbi\StatisticsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="object_id_idx", columns={"object_id"}),
 *         @ORM\Index(name="ip_idx", columns={"ip"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *     },
 *     options={"engine"="MyISAM"}
 * )
 */
class AdvertisementView
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(
     *     type="integer", 
     *     nullable=true,
     *     options={"comment" = "Advertisement id"}
     * )
     */
    private $object_id;

    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $ip;

    /** 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set object_id
     *
     * @param integer $objectId
     * @return AdvertisementView
     */
    public function setObjectId($objectId)
    {
        $this->object_id = $objectId;
    
        return $this;
    }

    /**
     * Get object_id
     *
     * @return integer 
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return AdvertisementView
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return AdvertisementView
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}