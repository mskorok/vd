<?php
namespace VisiDarbi\StatisticsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\StatisticsBundle\EntityRepository\SearchEntityRepository")
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="keyword_type_idx", columns={"keyword_type"}),
 *         @ORM\Index(name="keyword_idx", columns={"keyword"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"})
 *     },
 *     options={"engine"="MyISAM"}
 * )
 */
class Search
{
    const KEYWORD_TYPE_WHAT = 10;
    const KEYWORD_TYPE_WHERE = 20;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $keyword_type;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $keyword;


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyword_type
     *
     * @param integer $keywordType
     * @return Search
     */
    public function setKeywordType($keywordType)
    {
        $this->keyword_type = $keywordType;

        return $this;
    }

    /**
     * Get keyword_type
     *
     * @return integer
     */
    public function getKeywordType()
    {
        return $this->keyword_type;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return Search
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Search
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}