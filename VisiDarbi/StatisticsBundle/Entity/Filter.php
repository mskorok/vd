<?php
namespace VisiDarbi\StatisticsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(name="filter_key_idx", columns={"filter_key"})},
 *     options={"engine"="MyISAM"}
 * )
 */
class Filter
{   
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $filter_key;

    /** 
     * @ORM\Column(type="blob", nullable=false)
     */
    private $filter_value;


    /** 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;


    /**
     * Set filter_key
     *
     * @param integer $filterKey
     * @return Filter
     */
    public function setFilterKey($filterKey)
    {
        $this->filter_key = $filterKey;
    
        return $this;
    }

    /**
     * Get filter_key
     *
     * @return integer 
     */
    public function getFilterKey()
    {
        return $this->filter_key;
    }

    /**
     * Set filter_value
     *
     * @param string $filter_value
     * @return Filter
     */
    public function setFilterValue($filterValue)
    {
        $this->filter_value = $filterValue;
    
        return $this;
    }

    /**
     * Get filter_value
     *
     * @return string 
     */
    public function getFilterValue()
    {
        return $this->filter_value;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Filter
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}