<?php

namespace VisiDarbi\StatisticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command class to clear old (60 days) search keywords
 *
 * @author Aleksey
 */
class SearchClearCommand extends ContainerAwareCommand {

    const DAYS_PERIOD = 60;

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi:statistics:clear-search')->setDescription('Clear older than '. self::DAYS_PERIOD .' days search keyword records ');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $now = new \DateTime();
        $interval = new \DateInterval('P'.self::DAYS_PERIOD.'D');
        $interval->invert = 1;
        $date = $now->add($interval);

        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $q = $em->createQueryBuilder()
                ->delete('VisiDarbi\StatisticsBundle\Entity\Search', 's')
                ->where('s.created_at < :old_date')
                ->setParameter('old_date', $date)
                ->getQuery();
        
        $entries_deleted = $q->execute();

        //outputs in the terminal
        $output->writeln($entries_deleted.' - Searched Entries Older than ' . self::DAYS_PERIOD . ' days deleted');
    }

}

?>
