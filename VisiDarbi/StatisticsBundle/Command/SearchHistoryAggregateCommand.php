<?php

namespace VisiDarbi\StatisticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
//use Cocur\Slugify\Slugify;
//use VisiDarbi\StatisticsBundle\Entity\SearchHistory;

/**
 * Command class to aggregate searched keyword counts
 * run example:
 * php app/console visidarbi:statistics:search-history-aggregate [--recreate-slugs]
 */
class SearchHistoryAggregateCommand extends ContainerAwareCommand {

    protected function configure() {
        parent::configure();
        $this
            ->setName('visidarbi:statistics:search-history-aggregate')->setDescription('Aggregate searched keyword counts')
//            ->addOption('recreate-slugs', null, InputOption::VALUE_NONE, 'If set, the task will recreate slugs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $starttime = microtime(true);

        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $now = date("Y-m-d H:i:s");
/*
        $sql = "SELECT s.keyword, s.keyword_type, COUNT(s.id) AS search_count, MAX(s.created_at) AS last_searched_at
          FROM Search s
          GROUP BY s.keyword";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute(array());

        $rows = $stmt->fetchAll();
        $slugify = new Slugify();

        foreach ($rows as $row){

            $slug = $slugify->slugify($row['keyword']);

            if ($input->getOption('recreate-slugs')) {
                $recreateSlugSqlPart = ", slug = '{$slug}'";
            }

            $sql = "
                INSERT INTO `SearchHistory`
                (
                 `keyword`,
                 `keyword_type`,
                 `search_count`,
                 `last_searched_at`,
                 `slug`,
                 `created_at`,
                 `updated_at`)
              VALUES (:keyword, :keyword_type, :search_count, :last_searched_at,:slug ,'{$now}', '{$now}')
              ON DUPLICATE KEY UPDATE search_count = VALUES(search_count), last_searched_at = VALUES(last_searched_at), updated_at = '{$now}'{$recreateSlugSqlPart}
            ";

            $stmt = $em->getConnection()->prepare($sql);


            $params = array(
                'keyword'=> $row['keyword'],
                'keyword_type'=> $row['keyword_type'],
                'search_count'=> $row['search_count'],
                'last_searched_at'=>$row['last_searched_at'],
                'slug'=> $slug,
            );

            $stmt->execute($params);

        }
*/

        $sql = "
            INSERT INTO `SearchHistory`
            (
             `keyword`,
             `keyword_type`,
             `search_count`,
             `last_searched_at`,
             `created_at`,
             `updated_at`)
          SELECT s.keyword, s.keyword_type, COUNT(s.id) AS search_count, MAX(s.created_at) AS last_searched_at, '{$now}' AS created_at, NOW() AS updated_at
          FROM Search s
          GROUP BY s.keyword
          ON DUPLICATE KEY UPDATE search_count = VALUES(search_count), last_searched_at = VALUES(last_searched_at), updated_at = '{$now}'
        ";

        $em->getConnection()->exec($sql);


        $duration = round(microtime(true) - $starttime, 3);

        $message = "[".date('H:i:s')."][{$duration}secs] ";

        $output->writeln("{$message}Search history aggregation done.");
    }

}

?>
