<?php

namespace VisiDarbi\StatisticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command class to clear old (60 days) search keywords
 *
 * @author Aleksey
 */
class ClearStatisticsCommand extends ContainerAwareCommand {

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi:statistics-clear')->setDescription('Delete statistics records older than 14 days');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $checkDate = new \DateTime();
        $interval = new \DateInterval('P14D');
        $interval->invert = 1;        
        $checkDate->add($interval);

        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $q = $em->createQueryBuilder()
                ->delete('VisiDarbi\StatisticsBundle\Entity\AdvertisementView', 'v')
                ->where('v.created_at < :checkDate')
                ->setParameter('checkDate', $checkDate)
                ->getQuery();

        $q->execute();

        //outputs in the terminal
        $output->writeln('Old statistics records are removed');
    }

}

?>
