<?php

namespace VisiDarbi\MobileBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;


/**
 * Link
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class DocumentMobileFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $file;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\MobileBundle\Entity\DocumentMobile", inversedBy="files")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id", nullable=false)
     */	
    protected $document;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param Media $file
     *
     * @return $this
     */
    public function setFile(Media $file = null)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return Media
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set document
     *
     *
     *
     * @param DocumentMobile $document
     * @return $this
     */
    public function setDocument(DocumentMobile $document)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return DocumentMobile
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}