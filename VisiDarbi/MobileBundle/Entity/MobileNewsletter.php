<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.11.12
 * Time: 10.48
 */

namespace VisiDarbi\MobileBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping AS ORM;
use Symfony\Component\Validator\Constraints as Assert;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\ProfessionBundle\Entity\Category;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\ProfessionBundle\Entity\Profession;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity;


/**
 * Class MobileNewsletter
 * @package VisiDarbi\MobileBundle\Entity
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VisiDarbi\MobileBundle\Entity\MobileNewsletterRepository")
 *
 */
class MobileNewsletter
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Category")
     * @ORM\JoinTable(name="newsletters_categories",
     *      joinColumns={@ORM\JoinColumn(name="newsletter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     *
     * @var ArrayCollection
     */
    protected $categories;

    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Profession")
     * @ORM\JoinTable(name="newsletters_professions",
     *      joinColumns={@ORM\JoinColumn(name="newsletter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="profession_id", referencedColumnName="id")}
     *      )
     */
    protected $professions;

    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity")
     * @ORM\JoinTable(name="newsletters_cities",
     *      joinColumns={@ORM\JoinColumn(name="newsletter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")}
     *      )
     */
    protected $cities;

    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource")
     * @ORM\JoinTable(name="newsletters_sources",
     *      joinColumns={@ORM\JoinColumn(name="newsletter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="source_id", referencedColumnName="id")}
     *      )
     */
    protected $sources;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank(message="Duration must not be empty", groups={"New"})
     */
    protected $duration;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $keyword;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $place;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Duration must not be empty", groups={"New"})
     */
    protected $frequency;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     * @var integer|null
     */
    protected $nid;



    public  function  __construct(){

        $this->categories     = new ArrayCollection();
        $this->professions    = new ArrayCollection();
        $this->cities         = new ArrayCollection();
        $this->sources        = new ArrayCollection();
        $this->nid = null;
    }





    /********************* GETTERS AND SETTERS **************************/


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return mixed
     */
    public function getProfessions()
    {
        return $this->professions;
    }

    /**
     * @param mixed $professions
     */
    public function setProfessions($professions)
    {
        $this->professions = $professions;
    }

    /**
     * @return mixed
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param mixed $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    /**
     * @return mixed
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * @param mixed $sources
     */
    public function setSources($sources)
    {
        $this->sources = $sources;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * @return mixed
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param mixed $frequency
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return int|null
     */
    public function getNid()
    {
        return $this->nid;
    }

    /**
     * @param int|null $nid
     */
    public function setNid($nid)
    {
        $this->nid = $nid;
    }



}