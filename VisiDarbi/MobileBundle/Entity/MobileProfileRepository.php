<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.10.12
 * Time: 10.57
 */

namespace VisiDarbi\MobileBundle\Entity;
use Doctrine\ORM\EntityRepository;


/**
 * An EntityRepository serves as a repository for entities with generic as well as
 * business specific methods for retrieving entities.
 *
 * This class is designed for inheritance and users can subclass this class to
 * write their own repositories with business-specific methods to locate entities.
 *
 * @since   2.0
 * @author  Benjamin Eberlei <kontakt@beberlei.de>
 * @author  Guilherme Blanco <guilhermeblanco@hotmail.com>
 * @author  Jonathan Wage <jonwage@gmail.com>
 * @author  Roman Borschel <roman@code-factory.org>
 */
class MobileProfileRepository extends EntityRepository
{

}