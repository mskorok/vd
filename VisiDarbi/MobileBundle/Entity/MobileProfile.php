<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.10.12
 * Time: 10.48
 */

namespace VisiDarbi\MobileBundle\Entity;


use Doctrine\ORM\Mapping AS ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class MobileProfile
 * @package VisiDarbi\MobileBundle\Entity
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VisiDarbi\MobileBundle\Entity\MobileProfileRepository")
 *
 */
class MobileProfile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Name must not be empty", groups={"Profile"})
     *
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Surname must not be empty", groups={"Profile"})
     *
     */
    protected $surname;


    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="E-mail must not be empty", groups={"Profile"})
     * @Assert\Email(message="E-mail must contain valid e-mail address")
     */
    protected $email;


    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Phone must not be empty", groups={"Profile"})
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "[0,Inf]Phone must not be longer than {{ limit }} characters"
     *
     * )
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Address must not be empty", groups={"Profile"})
     */
    protected $address;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /********************* GETTERS AND SETTERS **************************/



}