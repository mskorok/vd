<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.11.14
 * Time: 12.25
 */

namespace VisiDarbi\MobileBundle\Controller;



use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Doctrine\ORM\Query\Expr;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\MobileBundle\Helpers\MobileDetect;
use VisiDarbi\SettingsBundle\Manager\SettingsManager;

trait PreExecute
{
    public function preExecuteMobile(){
        $uri = parse_url($this->getRequest()->getRequestUri());
        /** @var  $this Controller */
        $detect = new MobileDetect();
        $desktop = $this->getRequest()->getSession()->get('desktop')?:false;
        $mobile = $this->getRequest()->getSession()->get('mobile')?:false;
        if(!$mobile){
            $mobile = $detect->isMobile();
            $mobile = true;
            if($mobile){
                $this->getRequest()->getSession()->set('mobile',1);
            }
        }

        if($desktop || !$mobile){
            $uri = isset($uri['query'])? '?'.$uri['query']:'';
            $url = $this->getRequest()->getSchemeAndHttpHost();//.$uri;
            $response =  new RedirectResponse($url);
            $response->send();
            exit();

        }
        return false;
    }

}