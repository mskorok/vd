<?php

namespace VisiDarbi\MobileBundle\Controller;


use Doctrine\DBAL\Query\Expression\CompositeExpression;
use SwiftMailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\Query\Expr;
use VisiDarbi\CMSBundle\Entity\Document;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\MobileBundle\Entity\MobileProfile;
use VisiDarbi\MobileBundle\Form\ContactMobileType;
use VisiDarbi\MobileBundle\Form\MobileSearchAdvertisementType;
use VisiDarbi\MobileBundle\Form\ProfileType;
use VisiDarbi\MobileBundle\Form\RegType;
use VisiDarbi\MobileBundle\Helpers\MobileDetect;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\User;


/**
 * Controller is a simple implementation of a Controller.
 *
 * It provides methods to common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MobileController extends Controller
{

    use PreExecute;

    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \Swift_Mailer
     * @DI\Inject("mailer")
     */
    protected $mailer;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;


    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;

    /**
     * @var  \Symfony\Component\Security\Core\SecurityContext
     * @DI\Inject("security.context")
     */
    protected $security;

    /**
     * @var  TranslatorInterface
     * @DI\Inject("translator")
     */
    protected $translator;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator")
     */
    protected $tokenGenerator;


    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     * @DI\Inject("security.encoder_factory")
     */
    protected $encoderFactory;

    /**
     * @var integer
     */
    protected $limit = 3;






    /**
     * @Route("/", name="index_mobile")
     * @Template()
     */
    public function indexAction()
    {
        if('POST' === $this->request->getMethod() ){
            $param = $this->request->request->get('mobileSearchAdvertisement');
            $qs = '?';
            if(array_key_exists('what', $param) && !empty($param['what'])){
                $qs .='what='.$param['what'];
                if(array_key_exists('where', $param) && !empty($param['where'])){
                    $qs .='&where='.$param['where'];
                }
            } else {
                if(array_key_exists('where', $param) && !empty($param['where'])){
                    $qs .='where='.$param['where'];
                }
            }

            $path = $this->generateUrl('list_mobile');
            $path .= $qs;
            return $this->redirect($path);
        }


        $this->getRequest()->getSession()->set('rand', time());
        $sponsoredAdverts = $this->sponsored(0, 4);
        $searchForm = $this->createForm(new MobileSearchAdvertisementType());


        return [
            'search_form' => $searchForm->createView(),
            'sponsoredAdverts' => $sponsoredAdverts,
            'offset' => 5,
        ];
    }

    /**
     * @Route("/auth", name="auth_mobile")
     * @Template()
     */
    public function authAction(Request $request)
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('menu_mobile'));

        }

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR)->getMessage();
        } elseif (null !== $request->getSession() && $request->getSession()->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR)->getMessage();
            $request->getSession()->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        $error = ($error === 'Bad credentials') ?
            $this->translator->trans('Bad credentials', [], 'mobile') :
            $error;
        return [
            'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ];


    }

    /**
     * @Route("/remind", name="remind_mobile")
     * @Template()
     */
    public function remindAction(Request $request)
    {

        if('POST' === $request->getMethod() ){
            $email = $this->request->get('email', null);

            if (empty($email)) {
                $this->session->getFlashBag()->add('remind', true);
                $this->session->getFlashBag()
                    ->add('remind_message', $this->translator->trans('E-mail address not specified', array(), 'mobile_passw_recovery'));
                return [];
            }

            $user = $this->em->getRepository('VisiDarbiUserBundle:User')->findOneBy(array(
                'email' => $email,
                'country' => $this->countryManager->getCurrentCountry(),
                'is_admin' => false
            ));

            if ($user === null) {
                $this->session->getFlashBag()->add('remind', true);
                $this->session->getFlashBag()
                    ->add('remind_message', $this->translator->trans('User account with such e-mail not found', array(), 'mobile_passw_recovery'));
                return $this->redirect($request->getUri());

            }

            /* @var $user \VisiDarbi\UserBundle\Entity\User */

            if (! $user->isEnabled()) {
                $this->session->getFlashBag()->add('remind', true);
                $this->session->getFlashBag()
                    ->add('remind_message', $this->translator->trans('User account with such e-mail not confirmed', array(), 'mobile_passw_recovery'));
                return $this->redirect($request->getUri());

            }

            if ($user->isLocked()) {
                $this->session->getFlashBag()->add('remind', true);
                $this->session->getFlashBag()
                    ->add('remind_message', $this->translator->trans('User account with such e-mail is locked', array(), 'mobile_passw_recovery'));
                return [];

            }

            $tmpPassword = \substr(\str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );
            $siteLink = $this->generateUrl('guest_mobile', array(), true);

            $this->emailSender->send(
                $this->countryManager->getCurrentCountry(),
                $this->request->getLocale(),
                EmailTemplate::ID_TMP_PASSWORD,
                $user->getEmail(),
                array(
                    'tmpPassword' => $tmpPassword,
                    'siteLink' => sprintf('<a href="%s">%s</a>', $siteLink, $siteLink)
                ),
                array(
                    'email' => $this->settingsManager->get('emails.sender_email'),
                    'name' => $this->settingsManager->get('emails.sender_name')
                )
            );


            $encoder = $this->encoderFactory->getEncoder($user);

            $user->setTmpPassword(
                $encoder->encodePassword($tmpPassword, $user->getSalt())
            );
            $fakePassword = uniqid(md5(time()), true);
            $user->setPassword($fakePassword);

            $this->userManager->updateUser($user);

            if($user instanceof User){
                $this->session->getFlashBag()
                    ->add(
                        'remind_success',
                        $this->translator->trans('Temporary password successfully sent', [], 'mobile_passw_recovery'));

            }
        }

        return [];

    }

    /**
     * @Route("/reg", name="reg_mobile")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function regAction(Request $request)
    {
        $privatePerson = new User();
        $privatePerson->setCountry($this->countryManager->getCurrentCountry());

        $privatePerson->setType(User::TYPE_PRIVATE);
        $privatePerson->setPrivateProfile(new PrivateProfile());
        $form = $this->createForm(
            new RegType( $this->translator),
            $privatePerson
        );

        $hasErrors = false;
        $errors = [];
        if('POST' === $request->getMethod()){
            $form->bind($this->request);
            $reg = $this->request->request->get('reg');
            $email = array_key_exists('email', $reg) ? $reg['email'] : null;
            if ($form->isValid() && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $user = $form->getData();
                $this->onFormSuccess($user);
                $this->session->getFlashBag()->add('is_reg_success', true);
            }else{

                $collector = $this->get('form_error_collector');
                $errors = $collector->collectErrors($form);
                $hasErrors = true;
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $errors['email'] = ['Not valid email'];
                }
            }
        }

        return [
            'is_reg_success' => count($this->session->getFlashBag()->get('is_reg_success')) > 0,
            'has_errors' => $hasErrors,
            'errors' => $errors,
            'form'  => $form->createView()
        ];
    }


    /**
     * @Route("/logout", name="logout_mobile")
     *
     */
    public function logoutAction()
    {

        return new RedirectResponse($this->generateUrl('index_mobile'));
    }

    /**
     * @Route("/clear", name="clear_mobile")
     *
     */
    public function clearAction(Request $request)
    {
        $locale = $this->getRequest()->getLocale();

        $sid = session_id();
        unset($_COOKIE[$sid]);
        $_SESSION = [];
        session_destroy();
        return new RedirectResponse($this->generateUrl('index_mobile'));
    }


    /**
     * @Route("/guest", name="guest_mobile")
     * @Template()
     */
    public function guestAction()
    {
        if(!($user = $this->getUser())){
            $uns = unserialize($this->session->get('_security_user'));
            if($uns instanceof UsernamePasswordToken){
                $this->security->setToken($uns);
            }
        }
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('menu_mobile'));

        }
        return [];

    }

    /**
     * @Route("/menu", name="menu_mobile")
     * @Template()
     */
    public function menuAction()
    {

        if (!$this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('guest_mobile'));

        }
        if($this->getUser()->isLegal()){
            $this->getRequest()->getSession()->remove('mobile');
            $this->getRequest()->getSession()->set('desktop', 1);
            return $this->redirect($this->generateUrl('index'));
        }
        return [];

    }


    /**
     * @Route("/route", name="route_mobile")
     *
     */
    public function routeAction()
    {

        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            $owner = $this->getUser();
            if($owner->isLegal()){
                $this->getRequest()->getSession()->remove('mobile');
                $this->getRequest()->getSession()->set('desktop', 1);
                return $this->redirect($this->generateUrl('index'));
            }
            return $this->redirect($this->generateUrl('menu_mobile'));

        } else {
            return $this->redirect($this->generateUrl('guest_mobile'));
        }


    }

    /**
     * @Route("/profile", name="profile_mobile")
     * @Template()
     */
    public function profileAction(Request $request)
    {

        if (!$this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('guest_mobile'));
        }
        $profile = new MobileProfile();
        /** @var User $owner */
        $owner = $this->getUser();
        if($owner->isLegal()){
            $this->getRequest()->getSession()->remove('mobile');
            $this->getRequest()->getSession()->set('desktop', 1);
            return $this->redirect($this->generateUrl('index'));
        }

        $errors = [];
        $hasErrors = false;
        $is_change_success = false;

        if('POST' === $request->getMethod()){
            $form = $this->createForm(
                new ProfileType( $this->translator),
                $profile
            );
            $form->bind($this->request);
            if ($form->isValid()) {
                /** @var MobileProfile $profile */
                $profile = $form->getData();
                $user = $this->em->getRepository('VisiDarbiUserBundle:User')->find($profile->getUser());
                if($user){
                    if($owner->getId() !== $user->getId()){
                        return $this->redirect($this->generateUrl('index'));
                    }
                    $user->setEnabled(true);
                    $user->setUsername($profile->getEmail());
                    $user->setFullName($profile->getName());
                    $user->setSurname($profile->getSurname());
                    $user->setPhone($profile->getPhone());
                    $user->getPrivateProfile()->setAddress($profile->getAddress());
                    $pass = $profile->getPassword();
                    if(!empty($pass)){
                        $user->setPlainPassword($profile->getPassword());
                    }
                    $this->userManager->updateUser($user);
                    $profile->setUser($user);
                    if($profile->getId()){
                        $this->em->merge($profile);
                    } else {
                        $this->em->persist($profile);
                    }

                    $this->em->flush();
                    $is_change_success = true;
                }
            } else {
                $collector = $this->get('form_error_collector');
                $errors = $collector->collectErrors($form);
                $hasErrors = true;
            }
        }else{
            /** @var User $user */
            $user = $this->getUser();
            $profileFromDb = $this->em->getRepository('VisiDarbiMobileBundle:MobileProfile')->findOneBy(['user'=> $user->getId()]);
            if(!$profileFromDb){
                if($user){
                    $profile->setUser($user->getId());
                    $profile->setName($user->getFullName());
                    $profile->setEmail($user->getEmail());
                    $profile->setPhone($user->getPhone());
                    $profile->setSurname($user->getSurname());
                    $profile->setAddress($user->getPrivateProfile()->getAddress());
                    $profile->setId(null);
                } else {
                    $profile->setUser(0);
                    $profile->setId(null);
                }
            }else{
                $profile = $profileFromDb;
                $profile->setUser($profile->getUser()->getId());
            }

            $form = $this->createForm(
                new ProfileType( $this->translator),
                $profile
            );

        }
        return [
            'is_change_success' => $is_change_success,
            'has_errors' => $hasErrors,
            'errors' => $errors,
            'form' => $form->createView()
        ];

    }

    /**
     * @Route("/contacts", name="contacts_mobile")
     * @Template()
     */
    public function contactsAction()
    {
        $errorsSet = [];
        $form = $this->createForm(new ContactMobileType());

        $recipient = $this->settingsManager->get('contacts.email_recipient');
        if (empty($recipient)) {
            throw new \Exception('Contact form recipient email not configured');
        }


        if ($this->request->isMethod('POST')) {
            $form->bindRequest($this->request);

            if ($form->isValid()) {
                $data = $form->getData();

                $body = implode('', [
                    '<p>Name: ' . $data['name'] . '</p>',
                    '<p>Phone: ' . $data['phone'] . '</p>',
                    '<p>Email: ' . $data['email'] . '</p>',
                    '<p>Message:<br/><br/>' . nl2br(htmlentities($data['message'], ENT_COMPAT, 'UTF-8')) . '</p>',
                ]);

                $mailMessage = \Swift_Message::newInstance()
                    ->setSubject($this->settingsManager->get('contacts.email_subject'))
                    ->setFrom($data['email'], $data['name'])
                    ->setTo($recipient)
                    ->setBody($body)
                    ->setContentType('text/html')
                ;



                $this->mailer->send($mailMessage);

                $this->session->getFlashBag()->add('is_successfully', true);
                return $this->redirect($this->request->getUri());

            } else {
                foreach($form->getErrors() as $e){
                    $errorsSet[]= $this->translator->trans(/** @Ignore */ $e->getMessage());

                }
            }
        }

        return array(
            'form' => $form->createView(),
            'errors' => $errorsSet,
        );

    }







    /**
     * @Route("/desktop", name="desktop_mobile")
     */
    public function desktopAction(Request $request){
        $request->getSession()->set('desktop', 1);
        return $this->redirect($this->generateUrl('index'));
    }




    /************************* AJAX ****************************/




    /**
     * @Route("/next", name="next_mobile")
     * @Method({"POST"})
     */
    public function nextAction()
    {


        if ($this->request->isXmlHttpRequest()) {
            $offset = $this->limit + (int)$this->request->get('offset');

            $entities = $this->sponsored($offset, $this->limit);
            if (count($entities) === 0) {
                return (new Response)->setContent(json_encode(['content' => '', 'offset' => $offset]));
            }
            $content = $this->render('VisiDarbiMobileBundle:Mobile:next.html.twig', ['sponsoredAdverts' => $entities])->getContent();

            return (new Response)->setContent(json_encode(['content' => $content, 'offset' => ($offset)]));
        } else {
            return new Response(json_encode(['response' => 'not XmlHttp']));
        }

    }
    /**
     * @Route("/autocomplete-what", name="autocomplete_what_mobile")
     * @Method({"POST", "GET"})
     */
    public function autocompleteWhatAction()
    {
        if ($this->request->isXmlHttpRequest()) {
            $res = 0;
            return (new Response)->setContent(json_encode([$res]));
        } else {
            return new Response(json_encode(['response' => 'not XmlHttp']));
        }
    }

    /**
     * @Route("/autocomplete-where", name="autocomplete_where_mobile")
     * @Method({"POST"})
     */
    public function autocompleteWhereAction()
    {

        if ($this->request->isXmlHttpRequest()) {
            $qb = $this->em->createQueryBuilder('ac')
                ->select('ac.name')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'ac')
                ->andWhere("ac.AdvertisementCountry = :country")
                ->orderBy('ac.name')
                ->setParameter('country', $this->getCurrentCountry()->getDeafultAdvertisementCountry()->getId())
                ->setMaxResults(40)
            ;

            $query = $qb->getQuery();
            $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
            $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
            $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale());

            $cities = $query->getResult();
            $output = [];
            foreach ($cities as $c) {
                $output[] = ['name' =>$c['name']];
            }
            return new Response(json_encode($output));
        } else {
            return new Response(json_encode(['response' => 'not XmlHttp']));
        }
    }


    /****************************** STATIC  **************************************/


    public static function isMobile(Request $request){
        $detect = new MobileDetect();
        $desktop = $request->getSession()->get('desktop')?:false;
        if($detect->isMobile() && !$desktop){
            $redirect = new RedirectResponse($request->getSchemeAndHttpHost().'/mobile');
            $redirect->send();
            exit;
        }
    }

    /****************************** PRIVATE  **************************************/

    /**
     * @param $offset
     * @param $limit
     * @return array
     */

    protected function sponsored($offset, $limit)
    {


        $locale = $this->request->getLocale();
        $country = $this->countryManager->getCurrentCountry();
        $flag = ($this->getRequest()->getSession()->get('rand')) ? $this->getRequest()->getSession()->get('rand') : time();

        srand($flag);

        $qb = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement')->createQueryBuilder('a')
            ->select('a')
            ->where('a.country = :country')
            ->setParameter('country', $country->getId())
            ->andWhere('a.status = :status')
            ->setParameter('status', Advertisement::STATUS_ACTIVE)
            ->select('a, comp, ae, ea, cp, req, ac, actry, slugs')
            ->leftJoin('a.companies', 'comp')
            ->leftJoin('a.advertisementEmails', 'ae')
            ->leftJoin('a.externalAdvertisement', 'ea')
            ->leftJoin('ea.externalAdvertisementSource', 'eas')
            ->leftJoin('a.contactPerson', 'cp')
            ->leftJoin('a.requisites', 'req')
            ->leftJoin('a.AdvertisementCity', 'ac')
            ->leftJoin('a.AdvertisementCountry', 'actry')
            ->leftJoin('a.slugs', 'slugs')
            ->andWhere('a.on_startpage = 1')
//            ->andWhere('a.is_highlighted_mobile = 1')
            ->setMaxResults(100)
//            ->setFirstResult($offset)
            ->orderBy('a.id', 'DESC');


        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $res = $query
            ->useResultCache(true, 600, 'sponsored_advertisements_' . $country->getId() . '_' . $locale)
            ->getResult();

        /** @var Advertisement $item */
        foreach($res as $key=>$item){
            $ext = $item->getExternalAdvertisement();
            if($ext && $ext instanceof ExternalAdvertisement){
                $src = $ext->getExternalAdvertisementSource();
                if($src instanceof ExternalAdvertisementSource && !$src->getIsSupportedMobile()){
                    unset($res[$key]);
                }
            }
        }

        shuffle($res);
        if ($offset > count($res)) {
            return [];
        }
        $res = array_slice($res, $offset, $limit);

        return $res;
    }


    protected function onFormSuccess(User $user)
    {
        $user->setEnabled(false);
        $user->setUsername($user->getEmail());
        $user->setConfirmationToken($this->tokenGenerator->generateToken());

        $this->sendConfirmationEmailMessage($user);

        $user->setDefaultLocale($this->request->getLocale());
        $this->userManager->updateUser($user);
    }


    protected function sendConfirmationEmailMessage(User $user)
    {
        $confirmLink = $this->generateUrl('confirm_mobile', array('token' => $user->getConfirmationToken() ), true);

        $this->emailSender->send(
            $this->countryManager->getCurrentCountry(),
            $this->request->getLocale(),
            EmailTemplate::ID_REG_CONFIRMATION,
            $user->getEmail(),
            array(
                'confirmLink' => sprintf('<a href="%s">%s</a>', $confirmLink, $confirmLink)
            ),
            array(
                'email' => $this->settingsManager->get('emails.sender_email'),
                'name' => $this->settingsManager->get('emails.sender_name')
            )
        );
    }

    protected function authenticateUser($user, Response $response)
    {

        try {
            $this->get('fos_user.security.login_manager')->loginUser(
                'main',
                $user,
                $response
            );
        } catch (AccountStatusException $ex) {
            return $this->redirect($this->generateUrl('index_mobile'));
        }
    }

    protected function getCountryManager() {

        if (!$this->countryManager) {
            $this->countryManager = $this->container
                ->get('visidarbi.country_manager');
        }

        return $this->countryManager;
    }

    protected function getCurrentCountry(){
        return $this->getCountryManager()->getCurrentCountry();
    }


    protected function getLocale(){
        return $this->get('request')->getLocale();
    }
    /******************* SETTERS AND  GETTERS ************************/

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

}
