<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 16.1.19
 * Time: 20.22
 */

namespace VisiDarbi\MobileBundle\Controller;


use Cocur\Slugify\Slugify;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\CMSBundle\Entity\Document;
use Symfony\Component\Translation\TranslatorInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Controller is a simple implementation of a Controller.
 *
 * It provides methods to common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MobileConfirmController extends Controller
{


    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \Swift_Mailer
     * @DI\Inject("mailer")
     */
    protected $mailer;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;


    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;

    /**
     * @var  \Symfony\Component\Security\Core\SecurityContext
     * @DI\Inject("security.context")
     */
    protected $security;

    /**
     * @var  TranslatorInterface
     * @DI\Inject("translator")
     */
    protected $translator;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator")
     */
    protected $tokenGenerator;


    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     * @DI\Inject("security.encoder_factory")
     */
    protected $encoderFactory;

    /**
     * @Route("/confirm/{token}", name="confirm_mobile")
     *
     */
    public function confirmAction($token)
    {
        /** @var User $user */
        $user = $this->userManager->findUserByConfirmationToken($token);



        if (null === $user) {
            //throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
            //return new RedirectResponse($this->generateUrl('index'));
            $document = $this->em->getRepository('VisiDarbiCMSBundle:Document')->findOneBy(['special'=>Document::SPECIAL_TOKEN_FAIL, 'locale'=> $this->request->getLocale()]);
            $url = (!$document)?$this->generateUrl('index_mobile'):$this->generateUrl('document_mobile', array( 'id'=>null, 'slug' => $document->getPage()->getSlug()) );
            return $this->redirect($url);

        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime('now'));

        $this->userManager->updateUser($user);

        $userNs = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->findOneBy(array('email' => $user->getEmail()));

        if($userNs && $user->isPrivate()){
            $userNs->setUser($user);
            $userNs->setEmail(null);
            $this->em->persist($userNs);
            $this->em->flush();
        }

        $response = new RedirectResponse($this->generateUrl('menu_mobile'));

        $this->authenticateUser($user, $response);

        return $response;


    }

    /**
     * @Route("/to-mobile", name="to_mobile")
     *
     */
    public function mobileAction()
    {
        $this->request->getSession()->remove('desktop');
        return $this->redirect($this->generateUrl('index_mobile'));
    }




    protected function authenticateUser($user, Response $response)
    {

        try {
            $this->get('fos_user.security.login_manager')->loginUser(
                'main',
                $user,
                $response
            );
        } catch (AccountStatusException $ex) {
            return $this->redirect($this->generateUrl('index_mobile'));
        }
    }

    /**
     * @Route("/search-route", name="search_route_mobile")
     *
     */
    public function generateSearchUrlAction(Request $request){

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index_mobile'));
        }

        $slugify = new Slugify();

        $result = array('status' => 'false');

        $what = $request->get('what');
        $where = $request->get('where');

        if(strlen($what) > 70 || strlen($where) > 70){
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('list_mobile');

            return new JsonResponse($result);
        }

        $whatSlug = $slugify->slugify($what);
        $whereSlug = $slugify->slugify($where);
        $this->container->get('session')->getFlashBag()->clear();
        $this->container->get('session')->getFlashBag()->add('searchFieldsMobile', [
            'what' => ['slug' => $whatSlug, 'value' => $what],
            'where' => ['slug' => $whereSlug, 'value' => $where],
            'manualSearch' => true
        ]);
        if(!empty($what) && !empty($where)){
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('list_mobile',['what' => $whatSlug, 'where' => $whereSlug]);

        } elseif (empty($where) && !empty($what)) {
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('list_mobile',['what' => $whatSlug]);
        } elseif (empty($what) && !empty($where)) {
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('list_mobile',['where' => $whereSlug]);
        } else{
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('list_mobile');
        }

        return new JsonResponse($result);
    }

}