<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.11.14
 * Time: 12.25
 */

namespace VisiDarbi\MobileBundle\Controller;


use Doctrine\ORM\EntityManager;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\Request;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Doctrine\ORM\Query\Expr;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\SettingsBundle\Manager\SettingsManager;

trait FilterData
{
    /**
     * @param $em
     * @param $textFilter
     * @param $locale
     * @param $systemCountry
     * @param $cachePrefix
     * @return array
     */
    public static function getResources(EntityManager $em, Country $systemCountry = null, $locale='lv', $cachePrefix='mobile', $textFilter=null)
    {
        $countryId = ($systemCountry)?$systemCountry->getId():1;
        //resources

        $resources = $em->getRepository('VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementSource')
            ->createQueryBuilder('r')
            ->select('r')
            ->where('r.show_advertisiment = :show')
            ->setParameter('show', true)
//            ->select('r', 'COUNT(a.id) as adv_count')
//            ->leftJoin('r.externalAdvertisements', 'ext' )
//            ->leftJoin('ext.advertisements', 'a')
            ->setMaxResults(1000);
        if (!empty($textFilter)) {
            $resources
                ->leftJoin('r.externalAdvertisements', 'ext' )
                ->leftJoin('ext.advertisements', 'a')
                ->andWhere('a.id IN (:adids)')
                ->setParameter('adids', $textFilter);
        }
//        $resources->groupBy('r.resource')
//            ->orderBy('adv_count', 'DESC')
//            ->having('adv_count > 0');
        $resources = $resources->getQuery()->useResultCache(true, 600, $cachePrefix.'_resources_adv_'.$countryId.'_'.$locale);
        $resources = $resources->getResult();
        return $resources;
    }

    /**
     * @param EntityManager $em
     * @param SettingsManager $sm
     * @param Country $systemCountry
     * @param string $locale
     * @param string $cachePrefix
     * @param $textFilter
     * @return array
     * @throws \Exception
     */
    public static function getCompanies(EntityManager $em, SettingsManager $sm, Country $systemCountry = null, $locale='lv', $cachePrefix='mobile', $textFilter=null)
    {
        $countryId = ($systemCountry)?$systemCountry->getId():1;
        //companies

        $companies = $em->getRepository('VisiDarbiAdvertisementBundle:Company')
            ->createQueryBuilder('c')
            ->select('c.id, c.name, c.slug, COUNT(a.id) as adv_count')
            ->leftJoin('c.advertisement', 'a')
            ->where('a.country = :country')
            ->setParameter('country', $countryId)
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE);
        if (!empty($textFilter)) {
            $companies->andWhere('a.id IN (:adids)');
            $companies->setParameter('adids', $textFilter);
        }
        $companies->groupBy('c.slug')
            ->orderBy('adv_count', 'DESC')
            ->setMaxResults($sm->get('site.startpage_company_count'))
            ->having('adv_count > 0');
        $companies = $companies->getQuery()
            ->useResultCache(true, 600, $cachePrefix.'_companies_adv_'.$countryId.'_'.$locale)
            ->getResult();
        return $companies;
    }

    /**
     * @param $em
     * @param $textFilter
     * @param $locale
     * @param $systemCountry
     * @param $cachePrefix
     * @return array
     */
    public static function getCities(EntityManager $em, Country $systemCountry = null, $locale='lv', $cachePrefix='mobile', $textFilter=null)
    {
        $countryId = ($systemCountry)?$systemCountry->getId():1;
        if($systemCountry){
            $dac = $systemCountry->getDeafultAdvertisementCountry();
        }else{
            $dac = $em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCountry')->find(1);
        }

        $cities = $em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity')
            ->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, COUNT(a.id) as adv_count, ct.slug as city_slug')
            ->leftJoin('c.Advertisements', 'a')
            ->leftJoin('c.translations', 'ct', Expr\Join::WITH, "ct.locale=:locale AND ct.field = 'name' " )
            ->setParameter('locale', $locale)
            ->where('c.mark_on_frontend = 1')
            ->andWhere('c.AdvertisementCountry = :country')
            ->setParameter('country', $dac)
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE);
        if (!empty($textFilter)) {
            $cities->andWhere('a.id IN (:adids)');
            $cities->setParameter('adids', $textFilter);
        }
        $cities->groupBy('c.id')
            ->setMaxResults(1000)
            ->orderBy('city_name', 'ASC')
            ->having('adv_count > 0');
        $cities = $cities->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
            ->useResultCache(true, 600, $cachePrefix.'_special_adv_cities_'.$countryId.'_'.$locale)
            ->getResult();
        return $cities;
    }

    /**
     * @param $em
     * @param $textFilter
     * @param $locale
     * @param $systemCountry
     * @param $cachePrefix
     * @return array
     */
    public static function getPositions(EntityManager $em, Country $systemCountry = null, $locale='lv', $cachePrefix='mobile', $textFilter=null)
    {

        $countryId = ($systemCountry)?$systemCountry->getId():1;
        $positions = $em->getRepository('VisiDarbiProfessionBundle:Category')
            ->createQueryBuilder('c')
            ->select('c.id as category_id, c.name as category_name, ct.slug as category_slug, COUNT(a.id) as adv_count')
            ->leftJoin('c.Advertisements', 'a')
            ->leftJoin('c.translations', 'ct', Expr\Join::WITH, 'ct.locale=:locale' )
            ->setParameter('locale', $locale)
            ->where('c.country = :country')
            ->setParameter('country', $countryId)
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->setMaxResults(1000);
        if (!empty($textFilter)) {
            $positions->andWhere('a.id IN (:adids)');
            $positions->setParameter('adids', $textFilter);
        }
        $positions->having('adv_count > 0')
            ->groupBy('c.id')
            ->orderBy('adv_count', 'DESC');
        $positions = $positions->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
            ->useResultCache(true, 1000, $cachePrefix.'_positions_adv_'.$countryId.'_'.$locale)
            ->getResult();
        return $positions;
    }


    /**
     * @param $em
     * @param $textFilter
     * @param $locale
     * @param $systemCountry
     * @param $cachePrefix
     * @return array
     */
    public static function getProfessions(EntityManager $em, Country $systemCountry = null, $locale='lv', $cachePrefix='mobile', $textFilter=null)
    {

        $countryId = ($systemCountry)?$systemCountry->getId():1;
        $professions = $em->getRepository('VisiDarbiProfessionBundle:Profession')
            ->createQueryBuilder('pr')
            ->select('pr.id as p_id, pr.name as p_name,  COUNT(a.id) as adv_count')
            ->leftJoin('pr.Advertisements', 'a')
            ->leftJoin('pr.translations', 'rt', Expr\Join::WITH, 'rt.locale=:locale' )
            ->setParameter('locale', $locale)
            ->where('a.country = :country')
            ->setParameter('country', $countryId)
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->setMaxResults(1000);
        if (!empty($textFilter)) {
            $professions->andWhere('a.id IN (:adids)');
            $professions->setParameter('adids', $textFilter);
        }
        $professions->having('adv_count > 0')
            ->groupBy('pr.id')
            ->orderBy('adv_count', 'DESC');
        $professions = $professions->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
            ->useResultCache(true, 1000, $cachePrefix.'_positions_adv_'.$countryId.'_'.$locale)
            ->getResult();
        return $professions;
    }


    protected function getAdvertisementPagination(Request $request, $advertisementsQuery, $limit = 5)
    {
        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate(
            $advertisementsQuery,
            $request->get('page', 1),
            $limit,
            ['distinct' => true]
        );
        $params = $pagination->getParams();
        $filterKey = $request->query->get('filter', null);
        if($filterKey && !array_key_exists('filter', $params)){
            $pagination->setParam('filter', $filterKey);
        }
        $routeName = $request->get('_route');
        $pagination->setCurrentPageNumber($request->get('page', 1));

        $pagination->setUsedRoute($routeName);
        $pagination->setTemplate('VisiDarbiMobileBundle::pager.html.twig');



        return $pagination;
    }

}