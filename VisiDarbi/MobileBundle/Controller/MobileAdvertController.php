<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.10.12
 * Time: 21.39
 */

namespace VisiDarbi\MobileBundle\Controller;


use Cocur\Slugify\Slugify;
use Doctrine\ORM\QueryBuilder;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\AdvertisementBundle\Controller\AdvertisementController;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory;
use VisiDarbi\MobileBundle\Manager\AdvertisementManager;
use JMS\DiExtraBundle\Annotation as DI;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use VisiDarbi\MobileBundle\Form\ShareAdvertisementType;
use VisiDarbi\MobileBundle\Form\SubscribeAdvertisementType;
use Doctrine\ORM\Query\Expr;
use Foolz\SphinxQL\Connection;
use VisiDarbi\CommonBundle\SphinxQL\SphinxQL;
use VisiDarbi\SettingsBundle\Manager\SettingsManager;
use VisiDarbi\StatisticsBundle\Entity\Search;
use VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement;
use VisiDarbi\MobileBundle\Helpers\MobileDetect;


class MobileAdvertController extends  AdvertisementController
{

    use FilterData;
    use PreExecute;

    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;


    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;

    /**
     * @var  \Symfony\Component\Security\Core\SecurityContext
     * @DI\Inject("security.context")
     */
    protected $security;

    /**
     * @var  TranslatorInterface
     * @DI\Inject("translator")
     */
    protected $translator;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator")
     */
    protected $tokenGenerator;


    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     * @DI\Inject("security.encoder_factory")
     */
    protected $encoderFactory;

    /**
     * @var \VisiDarbi\MobileBundle\Manager\AdvertisementManager
     * @DI\Inject("mobile.advertisement_manager")
     */
    protected $am;


    /**
     * @var \Symfony\Component\Security\Core\SecurityContext
     * @DI\Inject("security.context")
     */
    protected $securityContext;



    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator")
     */
    protected $paginator;




    /**
     * @Route("/list", name="list_mobile")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $em = $this->em;
        /** @var AdvertisementManager $am */
        $am = $this->am;
        $locale = $this->request->getLocale();
        $systemCountry =  $this->countryManager->getCurrentCountry();
        $adDefaultCountry = $systemCountry->getDeafultAdvertisementCountry();
        $textFilter = false;
        $filtered = false;
        $searched = false;
        $postResult = null;
        $filterKey = null;
        $filterSearch = null;
        $this->sortRoutesUrlValues = $this->getPaginationRoutesUrlValues();
        // Initialize ShareForm
        $shareForm = $this->createForm(new ShareAdvertisementType());
        //Initialize Subsribe Form
        $subscribeForm = $this->createForm(
            new SubscribeAdvertisementType(),
            null,
            [
                'user_authorized' => ($this->currentUser ? true : false),
            ]
        );
        $slugify = new Slugify();
        /* share */
        if ($request->isMethod('POST')) {
            $params = $this->request->request->all();
            if(array_key_exists('sort', $params)){
                $filter = $am->setFilter($params['sort']);
                $filterSearch = $filter['value'];
                $filterKey = $filter['key'];

            }
            if (array_key_exists('shareadvertisementform', $params)) {
                $result = $this->processShare($request);
                $postResult = $result['result'];
                $shareForm = $result['form'];

            }

            if (array_key_exists('subscribeadvertisementform', $params)) {
                $result = $this->processSubscribe($request);
                $postResult = $result['result'];
                $subscribeForm = $result['form'];

            }
        }



        //Get  values what, where, filter,page, sort, direction
        $searchParams = [];
        $slug = [];
        foreach(['what', 'where'] as $key){
            $fromSession = $this->session->getFlashBag()->peek($key);
            $searchParams[$key] = null;
            if($fromParams = $this->getParameter($key)){
                if($fromSession && ($fromParams !== $fromSession[0])){
                    $this->session->getFlashBag()->set($key, $fromParams);
                }
                $searchParams[$key] = $fromParams;
            } elseif($fromSession) {
                $searchParams[$key] = $fromSession[0];
            }
            $slug[$key] = $slugify->slugify($searchParams[$key]);
        }
        unset($key);

        $fromSession = $this->session->getFlashBag()->get('searchFieldsMobile');

        $whatFromSession = (isset($fromSession[0]['what']['value'])) ? $fromSession[0]['what']['value'] :null;
        if($whatFromSession && $searchParams['what'] && ($slugify->slugify($whatFromSession) == $searchParams['what'])){
            $whatToSession = $whatFromSession;
        } else {
            $whatToSession = null;
        }
        $whereFromSession = (isset($fromSession[0]['where']['value'])) ? $fromSession[0]['where']['value'] : null;
        if($whereFromSession && $searchParams['where'] && ($slugify->slugify($whereFromSession) == $searchParams['where'])){
            $whereToSession = $whereFromSession;
        } else {
            $whereToSession = null;
        }
        if($whatToSession || $whereFromSession) {
            $this->session->getFlashBag()->clear();
            $this->session->getFlashBag()->add('searchFieldsMobile', [
                'what' => ['slug' => $searchParams['what'], 'value' => $whatToSession],
                'where' => ['slug' => $searchParams['where'], 'value' => $whereToSession],
                'manualSearch' => true
            ]);
        }
        $arrowWhat = (isset($whatFromSession)) ? $whatFromSession : $searchParams['what'];
        $arrowWhere = (isset($whereFromSession)) ? $whereFromSession : $searchParams['where'];
        $arrowDesc = $arrowWhat.' '.$arrowWhere;
        if(!$filterSearch){
            if(!$filterKey){
                $filterKey = $this->getParameter('filter');
            }
            $filterSearch = $am->getDecodedFilter($filterKey);
        }

        foreach(['what', 'where'] as $key){
            $fromSession = $this->session->getFlashBag()->peek($key);
            $searchParams[$key] = null;
            if($fromParams = $this->getParameter($key)){
                if($fromSession && ($fromParams !== $fromSession[0])){
                    $this->session->getFlashBag()->set($key, $fromParams);
                }
                $searchParams[$key] = $fromParams;
            } elseif($fromSession) {
                $searchParams[$key] = $fromSession[0];
            }
            $slug[$key] = $slugify->slugify($searchParams[$key]);
        }

        if($filterKey){
            $request->query->set('filter', $filterKey);
        }

        $whatValue = $searchParams['what'];
        $whatSlug = $slug['what'];
        $whereValue = $searchParams['where'];
        $whereSlug = $slug['where'];
        $pageValue = $this->getParameter('page', 1);



        if(!$searchParams['what'] && !$searchParams['where'] && !$filterSearch && $filterKey){
            return $this->redirect($this->generateUrl('list_mobile') );
        }



        //SELECT AND PROCESS ADVERTISEMENT LIST
        /** @var QueryBuilder $advertisements */
        $advertisements = $em->createQueryBuilder()
            ->select('advertisement, company, adcountry, adcity, externalAd, resource, favorite, slugs')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')
            ->leftJoin('advertisement.companies', 'company')
            ->join('advertisement.AdvertisementCountry', 'adcountry')
            ->leftJoin('advertisement.AdvertisementCity', 'adcity')
            //->leftJoin('advertisement.profession', 'profession')
            //->leftJoin('advertisement.category', 'category')
            ->leftJoin('advertisement.externalAdvertisement', 'externalAd')
            ->leftJoin('externalAd.externalAdvertisementSource', 'resource' )
            ->leftJoin('advertisement.userFavoriteAdvertisements', 'favorite')
            ->leftJoin('advertisement.slugs', 'slugs')
            ->andWhere(' resource.show_advertisiment = 1 ')
            ->orWhere(' resource.show_advertisiment IS NULL ')
            ->andWhere('advertisement.country = :country')
            ->andWhere('advertisement.status = :status')
            ->setParameter('country', $this->getCurrentCountry()->getId())
            ->setParameter('status', Advertisement::STATUS_ACTIVE)
            ->groupBy('advertisement.id')
            //->setMaxResults(1000)

        ;

        $advertCount = $em->createQueryBuilder()
            ->select('advertisement.id')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')
            ->leftJoin('advertisement.companies', 'company')
            ->join('advertisement.AdvertisementCountry', 'adcountry')
            ->leftJoin('advertisement.AdvertisementCity', 'adcity')
            //->leftJoin('advertisement.profession', 'profession')
            //->leftJoin('advertisement.category', 'category')
            ->leftJoin('advertisement.externalAdvertisement', 'externalAd')
            ->leftJoin('externalAd.externalAdvertisementSource', 'resource' )
            ->leftJoin('advertisement.userFavoriteAdvertisements', 'favorite')
            ->leftJoin('advertisement.slugs', 'slugs')
            ->andWhere(' resource.show_advertisiment = 1 ')
            ->orWhere(' resource.show_advertisiment IS NULL ')
            ->andWhere('advertisement.country = :country')
            ->andWhere('advertisement.status = :status')
            ->setParameter('country', $this->getCurrentCountry()->getId())
            ->setParameter('status', Advertisement::STATUS_ACTIVE)
            ->groupBy('advertisement.id')
            //->setMaxResults(1000)

        ;



        $sphinxFilter = false;
        $sphinxTotalcount = 0;
        //Process What / Where field values and generated slugs
        if ($whatValue || $whereValue) {
            $searched = true;


            $sphinxResults = $this->getSearchFilter([
                'what' => $whatValue,
                'where' => $whereValue,
                'page' => $pageValue,
                'sort_by' => $this->getPaginationSortValue($request, 'list_mobile' , 'field'),
                'sort_direction' => $this->getPaginationDirectionValue($request, 'list_mobile' , 'direction')
            ]);

            $textFilter = $sphinxResults['ids'];
            $sphinxTotalCount = $sphinxResults['total'];
            $sphinxFilter = $sphinxResults['filter'];

//$textFilter = [39745];
            if($this->currentUser && ($arrowWhat || $arrowWhere)) {
                $this->storeSearchHistory($arrowWhat, $arrowWhere, $this->currentUser);
            }

            if (!empty($textFilter)) {
                $advertisements->andWhere('advertisement.id IN (:adids)');
                $advertisements->setParameter('adids', $textFilter);

                $advertCount->andWhere('advertisement.id IN (:adids)');
                $advertCount->setParameter('adids', $textFilter);

            } else {
                $advertisements->andWhere('true = false');

                $advertCount->andWhere('true = false');
            }

            $request->request->set('searchWhat', $whatValue);
            $request->request->set('searchWhere', $whereValue);

        }

        //$advertisements->addOrderBy('advertisement.is_highlighted_mobile', 'DESC');
        $advertisements->addOrderBy('advertisement.is_highlighted', 'DESC');

        //Process Filter
        //filtering

        /** Filter company order */
        if (is_array($filterSearch) &&
            array_key_exists('date-direction', $filterSearch) &&
            in_array(trim($filterSearch['date-direction']), ['ASC', 'DESC'])) {
            $advertisements->addOrderBy('advertisement.date_from', trim($filterSearch['date-direction']));
        }  else {
            $advertisements->addOrderBy('advertisement.date_from', 'DESC');
        }

        if (is_array($filterSearch) &&
            array_key_exists('cities', $filterSearch) &&
            is_array($filterSearch['cities']) &&
            count($filterSearch['cities']) >0 ) {
            $filtered = true;
            $advertisements->andWhere('adcity.id IN (:adcityid)');
            $advertisements->setParameter('adcityid', array_keys($filterSearch['cities']));

            $advertCount->andWhere('adcity.id IN (:adcityid)');
            $advertCount->setParameter('adcityid', array_keys($filterSearch['cities']));
        }

        if (is_array($filterSearch) &&
            array_key_exists('countries', $filterSearch) &&
            is_array($filterSearch['countries']) &&
            count($filterSearch['countries']) >0) {
            $filtered = true;
            $advertisements->andWhere('adcountry.id IN (:adcountryid)');
            $advertisements->setParameter('adcountryid', array_keys($filterSearch['countries']));

            $advertCount->andWhere('adcountry.id IN (:adcountryid)');
            $advertCount->setParameter('adcountryid', array_keys($filterSearch['countries']));
        }

        if (is_array($filterSearch) &&
            array_key_exists('positions', $filterSearch) &&
            is_array($filterSearch['positions']) &&
            count($filterSearch['positions']) >0) {
            $filtered = true;
            $advertisements->leftJoin('advertisement.Categories', 'ac');
            $advertisements->andWhere('ac IN (:categoryid)');
            $advertisements->setParameter('categoryid', array_keys($filterSearch['positions']));

            $advertCount->leftJoin('advertisement.Categories', 'ac');
            $advertCount->andWhere('ac IN (:categoryid)');
            $advertCount->setParameter('categoryid', array_keys($filterSearch['positions']));

            /** Filter position order */
            if (
                array_key_exists('position-direction', $filterSearch) &&
                in_array(trim($filterSearch['position-direction']), ['ASC', 'DESC'])) {
                $advertisements->addOrderBy('ac.id', trim($filterSearch['position-direction']));
            }
        }

        if (is_array($filterSearch) &&
            array_key_exists('professions', $filterSearch) &&
            is_array($filterSearch['professions']) &&
            count($filterSearch['professions']) >0) {
            $filtered = true;
            $advertisements->leftJoin('advertisement.Professions', 'ap');
            //$advertisements->leftJoin('ap.translations', 'apt');
            //$advertisements->andWhere('apt.slug IN (:professionslug)');
            $advertisements->andWhere('ap.id IN (:professionsIds)');
            $advertisements->setParameter('professionsIds', array_keys($filterSearch['professions']));



            $advertCount->leftJoin('advertisement.Professions', 'ap');

            $advertCount->andWhere('ap.id IN (:professionsIds)');
            $advertCount->setParameter('professionsIds', array_keys($filterSearch['professions']));
        }

        if (is_array($filterSearch) &&
            array_key_exists('companies', $filterSearch) &&
            is_array($filterSearch['companies']) &&
            count($filterSearch['companies']) >0) {
            $filtered = true;

            /**
             * @todo remove code below when Company entity logic will be fixed (duplicate rows currently)
             */
            $companiesArray = $this->getDoctrine()
                ->getEntityManager()
                ->createQueryBuilder()
                ->select('c.slug')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
                ->where('c.id IN (:cids)')
                ->setParameter('cids', array_keys($filterSearch['companies']))
                ->setMaxResults(1000)
                ->getQuery()
                ->useResultCache(true, 1000, 'advertisement_companies_filter_'.md5(serialize($advertisements->getParameters())).'_'.$this->getCurrentCountry()->getId().'_'.$this->getLocale())
                ->getResult();

            $filterSearch['company_slug'] = array();
            foreach ($companiesArray as $v) {
                $filterSearch['company_slug'][] = $v['slug'];
            }

            $advertisements->andWhere('company.slug IN (:companySlugs)');
            $advertisements->setParameter('companySlugs', $filterSearch['company_slug']);


            $advertCount->andWhere('company.slug IN (:companySlugs)');
            $advertCount->setParameter('companySlugs', $filterSearch['company_slug']);


            /** Filter company order */
            if (
                array_key_exists('company-direction', $filterSearch) &&
                in_array(trim($filterSearch['company-direction']), ['ASC', 'DESC'])) {
                $advertisements->addOrderBy('company.id', trim($filterSearch['company-direction']));
            }

            /**
             * @todo return code below back when Company entity logic will be fixed (duplicate rows currently)
             */
            /*
            $advertisements->andWhere('company.id IN (:companyIds)');
            $advertisements->setParameter('companyIds', array_keys($filterSearch['companies']));             *
             */
        }

        if (is_array($filterSearch) &&
            array_key_exists('resources', $filterSearch) &&
            is_array($filterSearch['resources']) &&
            count($filterSearch['resources']) >0) {
            $filtered = true;
            $advertisements->andWhere('resource.id IN (:resourceid)');
            $advertisements->setParameter('resourceid', array_keys($filterSearch['resources']));

            $advertCount->andWhere('resource.id IN (:resourceid)');
            $advertCount->setParameter('resourceid', array_keys($filterSearch['resources']));


            /** Filter resource order */
            if (
                array_key_exists('source-direction', $filterSearch) &&
                in_array(trim($filterSearch['source-direction']), ['ASC', 'DESC'])) {
                $advertisements->addOrderBy('resource.id', trim($filterSearch['source-direction']));
            }


        }
        $advertQuery = $advertisements->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        $count = $advertCount->getQuery()->getResult();



        if(is_array($count)){
            $count = count($count);
        } elseif($count) {
            $count = (int) $count;
        }else{
            $count = 0;
        }
        $advertQuery->setHint('knp_paginator.count', $count);

        //END OF SELECT AND PROCESS ADVERTISEMENT LIST
        $advertQuery->useResultCache(true, 600, 'mobile_advertisement_list_'.$this->getLocale().'_'.md5(serialize($advertisements->getParameters())));

        if ($this->currentUser) {
            $shareForm->setData(['email_from' => $this->currentUser->getEmail()]);
        }

        // Initialize and set default options for Paginator

        $pagination = $this->getAdvertisementPagination($request, $advertQuery, 10);
        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */


        if ($this->currentUser) {
            $subscribeForm->setData(['email' => $this->currentUser->getEmail()]);
        }


        /** data for Filter  */

        /** @var SettingsManager $sm */
        $sm = $this->settingsManager;
        $positions = self::getPositions($em, $systemCountry, $locale, 'mobile_advert', $textFilter);
        $cities = self::getCities($em, $systemCountry, $locale, 'mobile_advert', $textFilter);
        $companies = self::getCompanies($em, $sm, $systemCountry, $locale, 'mobile_advert', $textFilter);
        $resources = self::getResources($em, $systemCountry, $locale, 'mobile_advert', $textFilter);

        $last = $pagination->getItems();
        $frm = $subscribeForm->createView();
        $pageData = $pagination->getPaginationData();
        if(count($last)> 0){
            $last = end($last);
            $last = $last->getId();
        } else{
            $last = null;
        }

        $options = [
            'share_form'             => $shareForm->createView(),
            'subscribe_form'         => $subscribeForm->createView(),
            'favorites'              => $this->currentUserFavorites,
            'allowSaveAdvertisement' => $this->allowSaveAdvertisement,
            'pagination'             => $pagination,
            'filterData'             => $this->getFiltersData($textFilter, $sphinxFilter),
            'filterSearch'           => $filterSearch,
            'filtered'               => $filtered,
            'searched'               => $searched,
            'whatValue'              => $whatValue,
            'whereValue'             => $whereValue,
            'whatSlug'               => $whatSlug,
            'whereSlug'              => $whereSlug,
            'filterValue'            => $filterKey,
            'user'                   => $this->currentUser,
            'pageData'               => $pagination->getPaginationData(),
            'arrowTitle'             => $this->translator->trans('You searched', [], 'mobile'),
            'arrowDesc'              => $arrowDesc,
            'lastId'                 => $last,
            'adverts'                => $pagination->getItems(),
            'postResult'             => $postResult,
            'companies'              => $companies,
            'cities'                 => $cities,
            'resources'              => $resources,
            'positions'              => $positions,
        ];

        return $this->render('VisiDarbiMobileBundle:MobileAdvert:list.html.twig', $options);
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/saved", name="saved_mobile")
     * @Template()
     */
    public function savedAction(Request $request)
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }

        $favAdvRepo = $this->em->getRepository('VisiDarbiUserBundle:UserFavoriteAdvertisement');

        $qb = $favAdvRepo->createQueryBuilder('favAdv')
            ->select('favAdv, a, adcountry, adcity')
            ->innerJoin('favAdv.advertisement', 'a')
            ->leftJoin('a.AdvertisementCountry', 'adcountry')
            ->leftJoin('a.AdvertisementCity', 'adcity')
            ->where('favAdv.User = :user')
            ->setParameter('user', $user->getId())
            ->orderBy('favAdv.id', 'DESC')
            ->setMaxResults(1000)
        ;

        $query = $qb->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */
        $pagination = $this->getAdvertisementPagination($request, $query, 10);


        return array(
            'pagination' => $pagination,
            'adverts'    => $pagination->getItems(),
            'pageData'   => $pagination->getPaginationData(),
        );
    }


    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/delete-saved/{id}", name="delete_saved_mobile", defaults={"id"=0} )
     */
    public function deleteSavedAction()
    {

        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */



        $id = $this->request->get('id');
        if (!$user->isPrivate() || empty($id) || $id === 0) {
            return $this->redirect($this->generateUrl('saved_mobile'));
        }

        $favAdvRepo = $this->em->getRepository('VisiDarbiUserBundle:UserFavoriteAdvertisement');
        $favAdv = $favAdvRepo->findOneBy([
            'id' => $id,
            'User' => $user
        ]);



        if ($favAdv instanceof UserFavoriteAdvertisement) {
            $this->em->remove($favAdv);
            $this->em->flush();
        }



        return $this->redirect($this->generateUrl('saved_mobile'));
    }


    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/search-history", name="search_history_mobile")
     * @Template()
     */
    public function searchAction(Request $request)
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }

        $historyRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertismentSearchHistory');

        $qb = $historyRepo->createQueryBuilder('h')
            ->select('h')
            ->where('h.user = :user')
            ->setParameter('user', $user->getId())
            ->orderBy('h.last_searched_at', 'DESC')
            ->setMaxResults(1000)
        ;

        $query = $qb->getQuery();


        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */
        $pagination = $this->getAdvertisementPagination($request, $query, 10);

        return [
            'pagination' => $pagination,
            'adverts'    => $pagination->getItems(),
            'pageData'               => $pagination->getPaginationData(),
        ];
    }


    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/delete-search-history/{id}", name="delete_search_history_mobile", defaults={"_format"="json"}, options={"i18n"="false"} )
     */
    public function deleteSearchAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */


        $id = $this->request->get('id');
        if (empty($id) || $id === 0) {
            return $this->redirect($this->generateUrl('search_history_mobile'));
        }

        $historyRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertismentSearchHistory');
        $history = $historyRepo->findOneBy([
            'id' => $id,
            'user' => $user
        ]);

        if ($history instanceof AdvertismentSearchHistory) {
            $this->em->remove($history);
            $this->em->flush();
        }



        return $this->redirect($this->generateUrl('search_history_mobile'));
    }


    /**
     * @Route("/save", name="save_mobile")
     * @Secure(roles="ROLE_USER")
     */
    public function saveAction(Request $request)
    {

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index_mobile'), 302 );
        }
        return parent::saveAction($request);
    }

    /**
     * @Route("/search-clear", name="search_clear_mobile")
     * @Secure(roles="ROLE_USER")
     */
    public function searchClearAction(Request $request)
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        $historyRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertismentSearchHistory');
        $histories = $historyRepo->findBy([
            'user' => $user
        ]);
        foreach($histories as $history){
            $this->em->remove($history);
        }
        $this->em->flush();

        return $this->redirect($this->generateUrl('search_history_mobile'));

    }



    /**
     * @Route("/show/{id}", name="show_mobile", requirements={"id" = "\d+"})
     * @Template()
     */
    public function showAction(Request $request)
    {

        $postResult = null;
        $shareForm = $this->createForm(new ShareAdvertisementType());
        if ($request->isMethod('POST')) {
            $params = $this->request->request->all();

            if (array_key_exists('shareadvertisementform', $params)) {
                $result = $this->processShare($request);

                $postResult = $result['result'];
                $shareForm = $result['form'];

            }
        }
        $id = $request->get('id');
        $what = $request->get('what');
        $where = $request->get('where');
        $returnUrl = $this->generateUrl('list_mobile').'?what='.$what.'&where='.$where;
        if(!$id){
            return $this->redirect($returnUrl);
        }
        $advert = $this->em
            ->createQueryBuilder()
            ->select('advertisement', 'externaladvertisement', 'companies', 'contactperson', 'dutieses', 'requirementses', 'offers', 'AdvertisementCity', 'AdvertisementCountry')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement','advertisement')
            ->where('advertisement.id = :id')
            ->leftJoin('advertisement.externalAdvertisement', 'externaladvertisement')
            ->leftJoin('advertisement.companies', 'companies' )
            ->leftJoin('advertisement.contactPerson', 'contactperson' )
            ->leftJoin('advertisement.dutieses', 'dutieses')
            ->leftJoin('advertisement.requirementses', 'requirementses')
            ->leftJoin('advertisement.offers', 'offers')
            ->leftJoin('advertisement.AdvertisementCity', 'AdvertisementCity')
            ->leftJoin('advertisement.AdvertisementCountry', 'AdvertisementCountry')
            ->setParameter('id', $id)
            ->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getSingleResult()
        ;
        if(!($advert instanceof Advertisement)){
            return $this->redirect($returnUrl);
        }
        $statisticManager = $this->get('visidarbi.statistics.view_counter');
        $statisticManager->registerAdvertisementView($advert);

        $arrowDesc = substr($advert->getTitle(),0,30);
        if ($this->currentUser) {
            $shareForm->setData(['email_from' => $this->currentUser->getEmail()]);
        }
        //If it's external advertisement
        if($advert->getExternalAdvertisement()) {

            $url = $advert->getExternalAdvertisement()->getLink();
            $isUrlSecured = (0 === strpos($url, 'https'));
            if ((!$isUrlSecured && $request->isSecure()) || (!$isUrlSecured && !$request->isSecure()) ){

                return $this->redirect( $this->generateUrl('show_none_secure_mobile',
                    array('id'=> $id, 'what' =>$what, 'where' => $where) ));

            }


//            if(!@file_get_contents($url)){
//                return $this->redirect($this->generateUrl('index_mobile' ));
//            }



            return $this->render('VisiDarbiMobileBundle:MobileAdvert:showExternal.html.twig',
                [
                    'adv' => $advert,
                    'url' => $url,
                    'user'                   => $this->currentUser,
                    'locale'                 => $this->getLocale(),
                    'share_form'             => $shareForm->createView(),
                    'advertisementManager'   => $this->advertisementManager,
                    'allowSaveAdvertisement' => $this->allowSaveAdvertisement,
                    'favorites'              => $this->currentUserFavorites,
                    'arrowTitle'             => $this->translator->trans('Profession',[],'mobile'),
                    'arrowDesc'              => $arrowDesc,
                    'arrowPath'              => $returnUrl,
                    'postResult'             => $postResult
                ]
            );
        }




        $options = [
            'open_ad'                => 1,
            'user'                   => $this->currentUser,
            'share_form'             => $shareForm->createView(),
            'locale'                 => $this->getLocale(),
            'adv'                    => $advert,
            'advertisementManager'   => $this->advertisementManager,
            'allowSaveAdvertisement' => $this->allowSaveAdvertisement,
            'favorites'              => $this->currentUserFavorites,
            'arrowTitle'             => $this->translator->trans('Profession',[],'mobile'),
            'arrowDesc'              => $arrowDesc,
            'arrowPath'              => $returnUrl,
            'postResult'             => $postResult,


        ];
        $format = $advert->getFormat();
        switch($format){
            case 'Free':
                return  $this->render('VisiDarbiMobileBundle:MobileAdvert:showText.html.twig', $options);
                break;
            case 'Html':
                return $this->render('VisiDarbiMobileBundle:MobileAdvert:showHtml.html.twig', $options);
                break;
            case 'ImagePdf':
                return $this->render('VisiDarbiMobileBundle:MobileAdvert:showPdf.html.twig', $options);
                break;
            default:
                return $options;
        }

    }



    /**
     * @Route("/show-none-secure/{id}", name="show_none_secure_mobile", requirements={"id" : "\d+", "_scheme":  "http"})
     *
     */
    public function showNonSecureAction(Request $request)
    {

        $postResult = null;
        $shareForm = $this->createForm(new ShareAdvertisementType());
        if ($request->isMethod('POST')) {
            $params = $this->request->request->all();

            if (array_key_exists('shareadvertisementform', $params)) {
                $result = $this->processShare($request);

                $postResult = $result['result'];
                $shareForm = $result['form'];

            }
        }
        $id = $request->get('id');
        $what = $request->get('what');
        $where = $request->get('where');
        $returnUrl = $this->generateUrl('list_mobile').'?what='.$what.'&where='.$where;
        if(!$id){
            return $this->redirect($returnUrl);
        }
        $advert = $this->em
            ->createQueryBuilder()
            ->select('advertisement', 'externaladvertisement', 'companies', 'contactperson', 'dutieses', 'requirementses', 'offers', 'AdvertisementCity', 'AdvertisementCountry')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement','advertisement')
            ->where('advertisement.id = :id')
            ->leftJoin('advertisement.externalAdvertisement', 'externaladvertisement')
            ->leftJoin('advertisement.companies', 'companies' )
            ->leftJoin('advertisement.contactPerson', 'contactperson' )
            ->leftJoin('advertisement.dutieses', 'dutieses')
            ->leftJoin('advertisement.requirementses', 'requirementses')
            ->leftJoin('advertisement.offers', 'offers')
            ->leftJoin('advertisement.AdvertisementCity', 'AdvertisementCity')
            ->leftJoin('advertisement.AdvertisementCountry', 'AdvertisementCountry')
            ->setParameter('id', $id)
            ->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getSingleResult();

        if(!($advert instanceof Advertisement)){
            return $this->redirect($returnUrl);
        }
        $statisticManager = $this->get('visidarbi.statistics.view_counter');
        $statisticManager->registerAdvertisementView($advert);

        $arrowDesc = substr($advert->getTitle(),0,30);
        if ($this->currentUser) {
            $shareForm->setData(['email_from' => $this->currentUser->getEmail()]);
        }
        //If it's external advertisement
        if($advert->getExternalAdvertisement()) {

            $url = $advert->getExternalAdvertisement()->getLink();


//            if(!@file_get_contents($url)){
//                return $this->redirect($this->generateUrl('index_mobile' ));
//            }



            return $this->render('VisiDarbiMobileBundle:MobileAdvert:showExternal.html.twig',
                [
                    'adv' => $advert,
                    'url' => $url,
                    'user'                   => $this->currentUser,
                    'locale'                 => $this->getLocale(),
                    'share_form'             => $shareForm->createView(),
                    'advertisementManager'   => $this->advertisementManager,
                    'allowSaveAdvertisement' => $this->allowSaveAdvertisement,
                    'favorites'              => $this->currentUserFavorites,
                    'arrowTitle'             => $this->translator->trans('Profession',[],'mobile'),
                    'arrowDesc'              => $arrowDesc,
                    'arrowPath'              => $returnUrl,
                    'postResult'             => $postResult,

                ]
            );
        }




        $options = [
            'open_ad'                => 1,
            'user'                   => $this->currentUser,
            'share_form'             => $shareForm->createView(),
            'locale'                 => $this->getLocale(),
            'adv'                    => $advert,
            'advertisementManager'   => $this->advertisementManager,
            'allowSaveAdvertisement' => $this->allowSaveAdvertisement,
            'favorites'              => $this->currentUserFavorites,
            'arrowTitle'             => $this->translator->trans('Profession',[],'mobile'),
            'arrowDesc'              => $arrowDesc,
            'arrowPath'              => $returnUrl,
            'postResult'             => $postResult

        ];
        $format = $advert->getFormat();
        switch($format){
            case 'Free':
                return $this->render('VisiDarbiMobileBundle:MobileAdvert:showText.html.twig', $options);
                break;
            case 'Html':
                return $this->render('VisiDarbiMobileBundle:MobileAdvert:showHtml.html.twig', $options);
                break;
            case 'ImagePdf':
                return $this->render('VisiDarbiMobileBundle:MobileAdvert:showPdf.html.twig', $options);
                break;
            default:
                return $this->render('VisiDarbiMobileBundle:MobileAdvert:show.html.twig', $options);
        }

    }


    /**
     * @Route("/share-validate", name="share_validate_mobile")
     *
     */
    public function shareValidateAction(Request $request)
    {
        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index_mobile'), 302 );
        }
        $res['success'] = false;
        $form = $this->createForm(
            new ShareAdvertisementType(),
            null,
            [
                'captcha'            => true,
                'csrf_protection'    => false
            ]
        );
        $request->request->set('shareadvertisementform', $request->query->get('shareadvertisementform'));
        $form->bindRequest($request);
        if($form->isValid()){
            $res['success'] = true;
        } else {
            $html = '';
            foreach($form as $k=>$v){
                if($v->hasErrors()){
                    foreach($v->getErrors() as $e){
                        $html .= '<div class="error-list">'.$this->translator->trans(/** @Ignore */ $e->getMessage()).'</div>';
                    }
                }
            }
            $res['errors'] = $html;
        }
        return new Response(
            json_encode( $res ),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/subscribe-validate", name="subscribe_validate_mobile")
     *
     */
    public function subscribeValidateAction(Request $request)
    {
        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index_mobile'), 302 );
        }
        $res['success'] = false;
        $auth = ($this->currentUser ? true : false);
        $form = $this->createForm(
            new SubscribeAdvertisementType(),
            null,
            [
                'user_authorized' => $auth,
                'captcha'         => true,
                'csrf_protection' => false,

            ]
        );
        $param = $request->query->get('subscribeadvertisementform');
        if($auth){
            unset($param['email']);
        }
        $request->request->set('subscribeadvertisementform', $param);
        $form->bindRequest($request);
        if($form->isValid()){
            $res['success'] = true;
        } else {
            $html = '';
            foreach($form as $k=>$v){
                if($v->hasErrors()){
                    foreach($v->getErrors() as $e){
                        $html .= '<div class="error-list">'.$this->translator->trans(/** @Ignore */ $e->getMessage()).'</div>';
                    }
                }
            }
            $res['errors'] = $html;
        }

        return new Response(
            json_encode( $res ),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    protected function getPaginationUsedRoute(Request $request)
    {

        return 'list_mobile';
    }



    protected function getParameter($string, $def = null)
    {
        $param = $this->request->request->get($string, $def);
        if(!$param){
            $param = $this->request->query->get($string, $def);
        }
        return $param;
    }

    protected function getSearchFilter($params) {

        $config = $this->container->getParameter('sphinx');

        if (empty($params)) {
            return array();
        }

        $conn = new Connection();
        SphinxQL::forge($conn);

        if (! empty($params['what']) || ! empty($params['where'])) {

            if (! empty($params['what'])) {

                $params['what'] = trim($params['what']);
                $keyword = new Search();
                //Store keyword
                $keyword->setKeywordType(Search::KEYWORD_TYPE_WHAT);
                $keyword->setKeyword($params['what']);
                $this->getDoctrine()->getEntityManager()->persist($keyword);

            }

            if (! empty($params['where'])) {

                $params['where'] = trim($params['where']);
                $keyword = new Search();
                //Store keyword
                $keyword->setKeywordType(Search::KEYWORD_TYPE_WHERE);
                $keyword->setKeyword($params['where']);
                $this->getDoctrine()->getEntityManager()->persist($keyword);
            }

            $this->getDoctrine()->getEntityManager()->flush();
        }



        ////// Dabonam kopējo skaitu ///////
        $query = SphinxQL::forge();
        $query->selectFromStr('@id,  (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if(!$paramSettled) {
            return null;
        }




        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);

        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        $total = count($result);
        //////// Dabonam mūsu 10 ierakstu idus ////
        $order = 'asc';
        if($params['sort_direction']=='desc'){ $order = 'desc'; }

        switch($params['sort_by']){
            case 'company.name':
                $query->orderBy('cname', $order);
                break;
            case 'resource.resource':
                $query->orderBy('aresource', $order);
                break;
            case 'advertisement.title':
                $query->orderBy('atitle', $order);
                break;
            case 'advertisement.date_from':
            default:
                if($order == 'desc'){
                    $query->orderBy('is_highlighted', $order);
                }
                $query->orderBy('date_from', $order);
                break;
        }


        $query->limit(0,$config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $result = $query->execute();
        $ids=array();
        foreach ($result as $v) {
            $ids[] = $v['id'];
        }



        /////// Grupējam //////
        $groups = array();

        //// Pēc profession_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("profession_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['profession'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc profession_id /////

        //// Pēc city_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("city_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['city'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc city_id /////

        //// Pēc source_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("source_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['source'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc source_id /////

        //// Pēc company_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("company_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['company'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc company_id /////


        //// Pēc country_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("country_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['country'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc country_id /////

        //// Pēc category_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("category_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['category'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc category_id /////

        return array('ids'=>$ids,'total'=>$total, 'filter'=>$groups);


    }

    protected function processShare($request)
    {
        $shareForm = $this->createForm(
            new ShareAdvertisementType(),
            null,
            ['captcha' => false]
        );
        $shareForm->bindRequest($request);
        if ($shareForm->isValid()) {
            $data = $shareForm->getData();
            $advertisement = $this->getDoctrine()
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                ->findOneBy(['publicId' => $data['share_id']]);
            $externalAdvertisement = $advertisement->getExternalAdvertisement();
            if ($externalAdvertisement instanceof \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement) {
                $shareLink = $externalAdvertisement->getLink();
            } else {
                $shareLink = $this->generateUrl('visidarbi_advertisement_show', ['advertisementId' => $data['share_id']], true);
            }
//            $this->emailSender->send(
//                $this->getCurrentCountry(),
//                $request->getLocale(),
//                EmailTemplate::ID_SHARE_ADVERTISEMENT,
//                $data['email_to'],
//                [
//                    'shareLink' => sprintf('<a href="%s">%s</a>', $shareLink, $shareLink),
//                    'emailFrom' => $data['email_from'],
//                    'message' => $data['message'],
//
//                ],
//                [
//                    'email' => $this->settingsManager->get('emails.sender_email'),
//                    'name' => $this->settingsManager->get('emails.sender_name')
//                ]
//            );

            $result['success'] = $this->translator->trans('You successfully share advertisement', [], 'mobile');;
            $shareForm = $this->createForm(new ShareAdvertisementType());
        } else {
            $errorsSet = [];
            foreach ($shareForm as $k => $v) {
                if ($v->hasErrors()) {
                    foreach ($v->getErrors() as $e) {
                        $errorsSet[] = $this->translator->trans(/** @Ignore */
                            $e->getMessage());
                    }
                }
            }

            $result['errors'] = $errorsSet;
        }

        return ['form' => $shareForm, 'result' => $result];
    }

    protected function processSubscribe($request)
    {
        $subscribeForm = $this->createForm(
            new SubscribeAdvertisementType(),
            null,
            [
                'user_authorized' => ($this->currentUser ? true : false),
                'captcha' => false
            ]
        );
        $subscribeForm->bindRequest($request);

        if($subscribeForm->isValid()) {
            $data = $subscribeForm->getData();
            $filters = $this->am->getDecodedFilter($data['filter']);
            $this->subscribeOnAdvertisements($data, $filters);
            $subscribeForm = $this->createForm(
                new SubscribeAdvertisementType(),
                null,
                [
                    'user_authorized' => ($this->currentUser ? true : false),
                ]
            );
            $result['success'] = $this->translator->trans('You successfully subscribed search result', [], 'mobile');
        } else {
            $errorsSet = array();
            foreach($subscribeForm as $k=>$v){
                if($v->hasErrors()){
                    foreach($v->getErrors() as $e){
                        $errorsSet[]= $this->translator->trans(/** @Ignore */ $e->getMessage());
                    }
                }
            }
            $result['errors'] = $errorsSet;
        }

        return ['form' => $subscribeForm, 'result' => $result];
    }

}