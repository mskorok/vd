<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.11.12
 * Time: 23.07
 */

namespace VisiDarbi\MobileBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use VisiDarbi\MobileBundle\Entity\DocumentMobile;
use VisiDarbi\MobileBundle\Helpers\MobileDetect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
//use VisiDarbi\StatisticsBundle\Entity\Search;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\Query\Expr;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity;
use VisiDarbi\CMSBundle\Entity\Document;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\MobileBundle\Entity\MobileNewsletter;
use VisiDarbi\MobileBundle\Entity\MobileProfile;
use VisiDarbi\MobileBundle\Form\MobileNewsletterType;
use VisiDarbi\MobileBundle\Form\MobileSearchAdvertisementType;
use VisiDarbi\MobileBundle\Form\ProfileType;
use VisiDarbi\MobileBundle\Form\RegType;
use VisiDarbi\MobileBundle\Menu\MenuMobileBuilder;
use VisiDarbi\ProfessionBundle\Entity\Category;
use VisiDarbi\ProfessionBundle\Entity\Profession;
use VisiDarbi\SettingsBundle\Manager\SettingsManager;
use VisiDarbi\StatisticsBundle\EntityRepository\SearchHistoryRepository;
use VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\QueryBuilder;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use VisiDarbi\AdvertisementBundle\Controller\AdvertisementController;
use VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory;
use VisiDarbi\MobileBundle\Manager\AdvertisementManager;
use VisiDarbi\MobileBundle\Form\ShareAdvertisementType;
use VisiDarbi\MobileBundle\Form\SubscribeAdvertisementType;
use Foolz\SphinxQL\Connection;
use VisiDarbi\CommonBundle\SphinxQL\SphinxQL;
use VisiDarbi\StatisticsBundle\Entity\Search;
use VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement;


/**
 * Controller is a simple implementation of a Controller.
 *
 * It provides methods to common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class MobileContentController extends Controller
{

    use FilterData;
    use PreExecute;

    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;


    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;

    /**
     * @var  \Symfony\Component\Security\Core\SecurityContext
     * @DI\Inject("security.context")
     */
    protected $security;

    /**
     * @var  TranslatorInterface
     * @DI\Inject("translator")
     */
    protected $translator;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator")
     */
    protected $tokenGenerator;


    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     * @DI\Inject("security.encoder_factory")
     */
    protected $encoderFactory;


    /**
     * @var \VisiDarbi\MobileBundle\Manager\AdvertisementManager
     * @DI\Inject("mobile.advertisement_manager")
     */
    protected $am;



    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator")
     */
    protected $paginator;





    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/newsletters", name="newsletters_mobile")
     * @Template()
     */
    public function newslettersAction(Request $request)
    {
        $user = $this->security->getToken()->getUser();
        $repo = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings');

        $qb = $repo->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $user)
            ->orderBy('e.created_at', 'DESC')
            ->setMaxResults(3)
        ;

        $query = $qb->getQuery();



        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */
        $pagination = $this->getAdvertisementPagination($request, $query, 10);


        return [
            'pagination' => $pagination,
            'adverts'    => $pagination->getItems(),
            'pageData'   => $pagination->getPaginationData(),
        ];
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/add-newsletter", name="add_newsletter_mobile")
     * @Template()
     */
    public function addNewsletterAction(Request $request)
    {
        $em = $this->em;

        $locale = $this->request->getLocale();
        $systemCountry =  $this->countryManager->getCurrentCountry();

        $positions = self::getPositions($em, $systemCountry, $locale, 'mobile_content');
        $professions = self::getProfessions($em, $systemCountry, $locale, 'mobile_content');
        $cities = self::getCities($em, $systemCountry, $locale, 'mobile_content');
        $resources = self::getResources($em, $systemCountry, $locale, 'mobile_content');



        $form = $this->createForm(new MobileNewsletterType());
        $errorsSet = [];
        if ($request->isMethod('POST')) {
            $params = $this->request->request->all();



//            $citiesFromRequest = $this->request->request->get('newsletter_cities', []);
//            $citiesFromRequest = explode(',',$citiesFromRequest);
//
//            $mixed = $params['newsletter'];
//            if(array_key_exists('cities', $mixed)){
//                $mc = $mixed['cities'];
//                $cities = array_unique(array_merge($mc,$citiesFromRequest));
//            }
//
//            $mixed['cities'] = $citiesFromRequest;
//            $this->request->request->set('newsletter', $mixed);

            if (array_key_exists('newsletter', $params)) {
                $form->bindRequest($request);
                if($form->isValid()){

                    $this->createSubscription($params['newsletter']);
                    return $this->redirect($this->generateUrl('newsletters_mobile'));

                } else {
                    foreach($form as $k=>$v){
                        if($v->hasErrors()){
                            foreach($v->getErrors() as $e){
                                $errorsSet[]= $this->translator->trans(/** @Ignore */ $e->getMessage());
                            }
                        }
                    }
                }
            }
        }
        $nid = $request->query->get('nid', null);
        if($nid){
            $form = $this->fillNewsletter($form, $nid);
        }

        return [
            'form' => $form->createView(),
            'errors' => $errorsSet,
            'cities'                 => $cities,
            'sources'               => $resources,
            'positions'              => $positions,
            'professions'            => $professions,
        ];
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/delete-newsletter/{id}", name="delete_newsletter_mobile")
     *
     */
    public function deleteNewsletterAction(Request $request){

        $user = $this->security->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */


        $id = $this->request->get('id');
        if (empty($id) || $id === 0) {
            return $this->redirect($this->generateUrl('newsletters_mobile'));
        }

        $repo = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings');
        $newsletter = $repo->findOneBy(['id' => $id,'user' => $user]);

        if ($newsletter instanceof EmailSubscriptionSettings) {
            $this->em->remove($newsletter);
            $this->em->flush();
        }

        return $this->redirect($this->generateUrl('newsletters_mobile'));
    }


    /**
     * @Route("/documents", name="documents_mobile")
     * @Template()
     */
    public function documentsAction(Request $request){
        $entities = $this->em
            ->getRepository('VisiDarbiMobileBundle:DocumentMobile')
            ->findBy([
                'locale' => $request->getLocale(),
                'country' => $this->countryManager->getCurrentCountry(),
                'is_mobile' => true,
            ]);
        /** @var SlidingPagination $pagination */
        $pagination = $this->paginator->paginate($entities, $request->query->get('page', 1),10);
        $pagination->setTemplate('VisiDarbiMobileBundle::pager.html.twig');
        return [
         'docs'        => $pagination->getItems(),
         'pagination'  => $pagination,
         'pageData'   => $pagination->getPaginationData(),
        ];
    }


    /**
     * @Route("/document/{id}", name="document_mobile", requirements={"id" = "\d+"})
     * @Template()
     */
    public function documentAction($id)
    {
        $slug = $this->request->query->get('slug',null);
        if(!$id){
            return $this->redirect($this->generateUrl('documents_mobile'));
        }
        $doc = null;
        if($slug){
            $doc = $this->em->getRepository('VisiDarbiMobileBundle:DocumentMobile')->findOneBy(['slug' =>$slug]);
        }
        if(!$doc){
            $doc = $this->em->getRepository('VisiDarbiMobileBundle:DocumentMobile')->find((int) $id);
        }

        if(!($doc instanceof DocumentMobile) ||  !($doc->getEnabled())){
            return $this->redirect($this->generateUrl('documents_mobile'));
        }
        return [
            'document' => $doc
        ];
    }



    protected function createSubscription(array $data){
        if (!$this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('guest_mobile'));

        }
        $user = $this->security->getToken()->getUser();
        if(array_key_exists('nid', $data) && !empty($data['nid'])){
            $entity = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->find((int) $data['nid']);
            $entity = ($entity instanceof EmailSubscriptionSettings)?$entity:(new EmailSubscriptionSettings());

        } else {
            $entity = new EmailSubscriptionSettings();
            $entity->generateSubscriptionId();
        }

        $filter = [];

        if(array_key_exists('categories', $data)){
            $filter['categories'] = $data['categories'];
            foreach($data['categories'] as $cid){
                $cat = $this->em->getRepository('VisiDarbiProfessionBundle:Category')->find((int) $cid);
                if($cat instanceof Category){
                    /** @var $this->categories ArrayCollection */
                    $entity->addCategorie($cat);
                }
            }
        }
        if(array_key_exists('cities', $data)){
            $filter['cities'] = $data['cities'];
            foreach($data['cities'] as $cityId){
                $city = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity')->find((int) $cityId);
                if($city instanceof AdvertisementCity){
                    /** @var $this->cities ArrayCollection */
                    $entity->addCitie($city);
                }
            }
        }
        if(array_key_exists('professions', $data)){
            $filter['professions'] = $data['professions'];
            foreach($data['professions'] as $professionId){
                $profession = $this->em->getRepository('VisiDarbiProfessionBundle:Profession')->find((int) $professionId);
                if($profession instanceof Profession){
                    /** @var $this->professions ArrayCollection */
                    $entity->addProfession($profession);
                }
            }
        }
        if(array_key_exists('sources', $data)){
            $filter['sources'] = $data['sources'];
            foreach($data['sources'] as $sourceId){
                $source = $this->em->getRepository('VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementSource')->find((int) $sourceId);
                if($source instanceof ExternalAdvertisementSource){
                    /** @var $this->sources ArrayCollection */
                    $entity->addSource($source);
                }
            }
        }
        $filterKey = $this->am->setFilter($filter);
        if(is_array($filterKey) && array_key_exists('key', $filterKey)){
            $filterKey = $filterKey['key'];
            $entity->setFilterKey($filterKey);
        }
        if(array_key_exists('duration', $data) && is_string($data['duration'])){
            $entity->setAdvertisementMaxAge($data['duration']);
        }
        if(array_key_exists('keyword', $data) && is_string($data['keyword'])){
            $entity->setWhatValue($data['keyword']);
        }
        if(array_key_exists('place', $data) && is_string($data['place'])){
            $entity->setWhereValue($data['place']);
        }
        if(array_key_exists('frequency', $data) && is_string($data['frequency'])){
            $entity->setSendingFrequency($data['frequency']);
        }
        $entity->setLanguage($this->request->getLocale());
        $entity->setEmail($user->getEmail());
        $userId = $user->getId();
        if($userId){
            $userFromDb = $this->em->getRepository('VisiDarbiUserBundle:User')->find((int) $userId);
            $entity->setUser($userFromDb);
        }


        $this->em->persist($entity);
        $this->em->flush();

        return $entity;

    }

    /**
     * @param  $form
     * @param $nid
     * @return \Symfony\Component\Form\Form
     */
    protected function fillNewsletter($form, $nid)
    {

        $entity = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->find((int) $nid);
        if($entity instanceof EmailSubscriptionSettings){
            $model = new MobileNewsletter();
            $model->setCategories($entity->getCategories());
            $model->setCities($entity->getCities());
            $model->setProfessions($entity->getProfessions());
            $model->setSources($entity->getSources());
            $model->setDuration($entity->getAdvertisementMaxAge());
            $model->setFrequency($entity->getSendingFrequency());
            $model->setKeyword($entity->getWhatValue());
            $model->setPlace($entity->getWhereValue());
            $model->setNid($entity->getId());
            $form = $this->createForm(new MobileNewsletterType(), $model);

        }

        return $form;
    }


}