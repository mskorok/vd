<?php

namespace VisiDarbi\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller is a simple implementation of a Controller.
 *
 * It provides methods to common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class DefaultController extends Controller
{
    /**
     * Default action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function indexAction(Request $request)
    {
        $response = $this->forward('VisiDarbiMobileBundle:Mobile:index', array());

        return $response;
    }
}
