<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15.12.8
 * Time: 12.15
 */

namespace VisiDarbi\MobileBundle\Security;


use HWI\Bundle\OAuthBundle\Connect\AccountConnectorInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Security\UserProvider\FrontendUserProvider;

use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;




class MobileOAuthUserProvider extends FrontendUserProvider implements  AccountConnectorInterface, OAuthAwareUserProviderInterface
{


    /**
     * @var array
     */
    protected $properties = array(
        'identifier' => 'id',
    );

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * @var Translator
     */
    protected $translator;


    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager FOSUB user provider.
     * @param array                $properties  Property mapping.
     */
    public function __construct(UserManagerInterface $userManager, array $properties = [])
    {
        parent::__construct($userManager);
        $this->properties  = array_merge($this->properties, $properties);
        $this->accessor    = PropertyAccess::createPropertyAccessor();
    }


    /**
     * Loads the user by a given UserResponseInterface object.
     *
     * @param UserResponseInterface $response
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {




//        $username = $response->getUsername();
//        $nickname = $response->getNickname();
        $realName = $response->getRealName();
        $email    = $response->getEmail();

        $resourceOwnerName = $response->getResourceOwner()->getName();

        $user = $this->userManager->findUserBy(
            ['socialResourceName' => $resourceOwnerName, 'email' => $email]
        );



        if (null === $user) {
            /** @var User $user */
            $user->getSocialResourceName();
            $user = $this->userManager->createUser();

            $name = explode(' ', $realName);
            if(count($name) > 1){
                $surname = $name[1];
                $username = $name[0];
            }elseif(count($name) > 0){
                $username = $name[0];
                $surname = '';
            }else{
                $username = uniqid('username'.time(), true);
                $surname = '';
            }
            $privateProfile = new PrivateProfile();
            $privateProfile->setAddress(' ');
            $privateProfile->setAddressPostcode(' ');
            $user->setUsername($email);
            $user->setSocialResourceName($resourceOwnerName);
            $user->setFullName($username);
            $user->setSurname($surname);
            $user->setEmail($email);
            $user->setEnabled(true);
            $user->setType('private');
            $user->setPrivateProfile($privateProfile);
            $user->setPlainPassword($username);
            $user->setLastLogin(new \DateTime());
            $user->setDefaultLocale('lv');
            $this->userManager->updateUser($user);


        }

        return $user;


    }

    /**
     * Connects the response the the user object.
     *
     * @param UserInterface $user The user object
     * @param UserResponseInterface $response The oauth response
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Expected an instance of FOS\UserBundle\Model\User, but got "%s".', get_class($user)));
        }

        $property = $this->getProperty($response);

        // Symfony <2.5 BC
        if ((method_exists($this->accessor, 'isWritable') && !$this->accessor->isWritable($user, $property))
            || (!method_exists($this->accessor, 'isWritable') && !method_exists($user, 'set'.ucfirst($property)))) {
            throw new \RuntimeException(sprintf("Class '%s' must have defined setter method for property: '%s'.", get_class($user), $property));
        }

        $username = $response->getUsername();

        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $this->accessor->setValue($previousUser, $property, null);

            $this->userManager->updateUser($previousUser);
        }

        $this->accessor->setValue($user, $property, $username);

        $this->userManager->updateUser($user);
    }


    /**
     * Gets the property for the response.
     *
     * @param UserResponseInterface $response
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getProperty(UserResponseInterface $response)
    {
        $resourceOwnerName = $response->getResourceOwner()->getName();

        if (!isset($this->properties[$resourceOwnerName])) {
            throw new \RuntimeException(sprintf("No property defined for entity for resource owner '%s'.", $resourceOwnerName));
        }

        return $this->properties[$resourceOwnerName];
    }

    /**
     * @param mixed $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

}