<?php

namespace VisiDarbi\MobileBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use JMS\DiExtraBundle\Annotation as DI;

/**
 *
 *
 * @author Mikhail
 */
class ParseFullNameCommand extends ContainerAwareCommand {



    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    protected function configure() {
        parent::configure();
        $this->setName('full-name:parse')
            ->setDescription('Check and change advertisement statuses');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $users = $this->em ->getRepository('VisiDarbiUserBundle:User')->findBy(['legalProfile' => null]);

        $forParsing2 = [];
//        $forParsing3 = [];
//        $forParsing4 = [];
//        $forParsing5 = [];
//        $forParsing6 = [];
//        $forParsing7 = [];
        foreach ($users as $user) {
            $full = $user->getFullName();
            $array = explode(' ', $full);
            if(count($array) === 2){
                $forParsing2[] = ['user' => $user, 'full' => $array];
                $user->setSurname($array[1]);
                $user->setFullName($array[0]);
                $this->em->persist($user);
            }
//            if(count($array) === 3){
//                $forParsing3[] = ['user' => $user, 'full' => $array];
//            }
//            if(count($array) === 4){
//                $forParsing4[] = ['user' => $user, 'full' => $array];
//            }
//            if(count($array) === 5){
//                $forParsing5[] = ['user' => $user, 'full' => $array];
//            }
//            if(count($array) === 6){
//                $forParsing6[] = ['user' => $user, 'full' => $array];
//            }
//            if(count($array) === 7){
//                $forParsing7[] = ['user' => $user, 'full' => $array];
//            }

        }
        $this->em->flush();



        //outputs in the terminal
        $output->writeln('Advertisement full name and surname updated. Rows '.count($forParsing2));
    }

}

?>
