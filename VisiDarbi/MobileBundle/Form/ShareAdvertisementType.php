<?php

namespace VisiDarbi\MobileBundle\Form;

use VisiDarbi\AdvertisementBundle\Form\ShareAdvertisementType as ParentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Email;


class ShareAdvertisementType extends ParentType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder->add('share_id', 'hidden', ['error_bubbling' => false, 'attr'=> ['value'=>'', 'class'=>'share_id']]);
        $builder->add('email_from', 'email', ['attr'=> ['maxlength'=>50], 'error_bubbling' => false, 'label'=>'E-mail', 'translation_domain' => 'mobile_admin', 'constraints' =>
            [
                new Email(['message' => $this->trans('Incorrect sender email',  [], 'mobile_form')]),
                new NotBlank(['message' => $this->trans('Sender email can\'t be empty.', [], 'mobile_form')]),
                new MaxLength(['limit' => 50, 'message' => $this->trans('Sender email must be not longer than 50 characters.', [], 'mobile_form')])]]);
        $builder->add('email_to', 'email', ['attr'=> ['maxlength'=>50], 'error_bubbling' => false, 'required'=>true, 'translation_domain' => 'mobile_admin', 'label'=>'Recipient e-mail', 'constraints' =>
            [
                new Email(['message' => $this->trans('Recipient email - incorrect email address.', [], 'mobile_form')]),
                new NotBlank(['message' => $this->trans('Recipient email can\'t be empty.', [], 'mobile_form')]),
                new MaxLength(['limit' => 50, 'message' => $this->trans('Recipient email must be not longer than 100 characters.', [], 'mobile_form')])]]);
        $builder->add('message', 'textarea',
            [
                'error_bubbling' => false,
                'label'=>'Hi! I suggest you this vacance from visidarbi.lv.',
                'translation_domain' => 'mobile_admin',
                'required' => false,
                'constraints' => [
                new MaxLength(['limit' => 100, 'message' => $this->trans('Message must be not longer than 100 characters.', [], 'mobile_form')])
                ]
            ]
        );
        if($options['captcha']){
            $builder->add('captcha', 'genemu_captcha', ['error_bubbling' => false, 'label' => 'Security code.', 'translation_domain' => 'mobile_admin',]);

        } else {
            $builder->add('captcha', 'text');

        }
    }

    public function getDefaultOptions(array $options) {


        return [
            'csrf_protection' => true,
            'captcha'         => true
        ];
    }



}
