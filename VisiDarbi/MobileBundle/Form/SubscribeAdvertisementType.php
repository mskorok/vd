<?php

namespace VisiDarbi\MobileBundle\Form;

use VisiDarbi\AdvertisementBundle\Form\SubscribeAdvertisementType as ParentType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Email;


use VisiDarbi\UserBundle\Validator\Constraints\UniqSubscriberEmail;

class SubscribeAdvertisementType extends ParentType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['user_authorized']) {

            $builder->add('email', 'email', [
                    'attr' => ['maxlength' => 255],
                    'error_bubbling' => false,
                    'label' => 'Your e-mail',
                    'translation_domain' => 'mobile_admin',
                    'constraints' => [
                        new Email(['message' => $this->trans('Incorrect email', [], 'mobile_form')]),
                        new NotBlank(['message' => $this->trans('Email can\'t be empty.', [], 'mobile_form')]),
                        new MaxLength(['limit' => 255, 'message' => $this->trans('Email must be not longer than 255 characters.', [], 'mobile_form')]),
                        new UniqSubscriberEmail(['message' => $this->trans('Email already registered', [], 'mobile_form')])
                    ]
                ]
            );


        }
        if($options['captcha']){
            $builder->add('captcha', 'genemu_captcha', ['error_bubbling' => false, 'label' => 'Security code.','translation_domain' => 'mobile_admin',]);

        } else {
            $builder->add('captcha', 'text');

        }
        $builder->add('filter', 'hidden', []);
        $builder->add('searchWhere', 'hidden', []);
        $builder->add('searchWhat', 'hidden', []);

    }

    public function getDefaultOptions(array $options) {
        return [
            'user_authorized' => false,
            'csrf_protection' => true,
            'captcha'         => true
        ];
    }



}
