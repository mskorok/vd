<?php

namespace VisiDarbi\MobileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;

class ContactMobileType extends DummyTransAbstractType
{


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => /** @Ignore */$this->trans('[Sender]Name', [], 'mobile_contacts'),
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => $this->trans('Name must not be empty', [], 'mobile_contacts')])
                ],
            ])
            ->add('phone', 'text', [
                'label' => /** @Ignore */$this->trans('Phone', [], 'mobile_contacts'),
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => $this->trans('Phone must not be empty', [], 'mobile_contacts')])
                ],
            ])
            ->add('email', 'email', [
                'label' => /** @Ignore */$this->trans('E-mail', [], 'mobile_contacts'),
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => $this->trans('Email must not be empty', [], 'mobile_contacts')]),
                    new Email(['message' => $this->trans('Email must contain valid e-mail address', [], 'mobile_contacts')]),
                ],
            ])
            ->add('message', 'textarea', [
                'label' => /** @Ignore */$this->trans('Message', [], 'mobile_contacts'),
                'required' => true,
                'trim' => true,
                'constraints' => [
                    new NotBlank(['message' => $this->trans('Message must not be empty', [], 'mobile_contacts')])
                ],
            ])
        ;
    }

    public function getDefaultOptions(array $options) {
        return [
            'csrf_protection' => true,
            'captcha'         => false
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mobile_contact';
    }
}
