<?php

namespace VisiDarbi\MobileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\UserBundle\Entity\User;

class ProfileType extends AbstractType
{
    private $translator;

    public function __construct( TranslatorInterface $translator = null)
    {

        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                /** @Ignore */
                'label' => $this->translator->trans('Name', array(), 'mobile_form'),
                'max_length' => 255,
                'required' => true,
            ))
            ->add('surname', 'text', array(
                /** @Ignore */
                'label' => $this->translator->trans('Surname', array(), 'mobile_form'),
                'max_length' => 255,
                'required' => true,
            ))
            ->add('email', 'email', [
                /** @Ignore */
                'label' => $this->translator->trans('E-mail', [], 'mobile_form'),
                'max_length' => 255,
                'required' => true,
            ])
            ->add('phone', 'text', User::phoneValidationParams(array('label'=>/** @Ignore */$this->translator->trans('Phone', array(), 'mobile_form'))))

            ->add('password', 'password', [
                'label' => $this->translator->trans('Password', [], 'mobile_form'),
                'max_length' => 20,
                'required' => false,
            ])
            ->add('address', 'text', array(
                /** @Ignore */
                'label' => $this->translator->trans('Address', array(), 'mobile_form'),
                'max_length' => 255,
                'required' => true,
            ))
            ->add('user', 'hidden', array(
                'required' => true,
            ))
            ->add('id', 'hidden', array(
                'required' => true,
            ))
        ;
    }



    public function getDefaultOptions(array $options) {
        return [
            'data_class' => 'VisiDarbi\MobileBundle\Entity\MobileProfile',
            'validation_groups' => ['Profile'],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'profile';
    }
}
