<?php

namespace VisiDarbi\MobileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;


/**
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class MobileSearchAdvertisementType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder->add('what', 'text', [
            'required' =>false
        ]);
        $builder->add('where', 'text', [
            'required' =>false
        ]);
        
    }

    public function getName() {
        return 'mobileSearchAdvertisement';
    }

}
