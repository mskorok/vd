<?php

namespace VisiDarbi\MobileBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;

class MobileNewsletterType extends DummyTransAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('duration', 'choice', [
                'choices'  => [
                    '14' => '14 dienas',
                    '3' => '3 Day',
                    '7' => 'Week'

                ],
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'translation_domain' => 'mobile_form'
            ])
            ->add('keyword', 'text', [
                'required' => false,
                'translation_domain' => 'mobile_form'
            ])
            ->add('nid', 'hidden', [
                'required' => false,
                'translation_domain' => 'mobile_form'
            ])
            ->add('place', 'text', [
                'required' => false,
                'translation_domain' => 'mobile_form'
            ])
            ->add('frequency', 'choice', [
                'choices'  => [
                    'daily' => 'Daily',
                    'weekly' => 'Weekly',
                    'monthly' => 'Monthly',
                ],
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'translation_domain' => 'mobile_form'
            ])
            ->add('categories', 'entity', [
                'label' => $this->trans('Category', [], 'mobile'),
                'class' => 'VisiDarbi\ProfessionBundle\Entity\Category',
                'property' => 'name',
                'required' => false,
                'multiple' => true,
                'translation_domain' => 'mobile_form',
//                'empty_data' => $this->trans('Izvēlies kategoriju', [], 'mobile'),
                'empty_value' => $this->trans('Izvēlies kategoriju', [], 'mobile'),
                'query_builder' =>
                    function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                    }])
            ->add('professions', 'entity', [
                'label' => $this->trans('Profession', [], 'mobile'),
                'class' => 'VisiDarbi\ProfessionBundle\Entity\Profession',
                'property' => 'name',
                'empty_value' => $this->trans('Izvēlies profesiju', [], 'mobile'),
                'required' => false,
                'multiple' => true,
                'translation_domain' => 'mobile_form',
                'query_builder' =>
                    function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')->orderBy('p.name', 'ASC');
                    }])
            ->add('cities', 'entity', [
                'label' => $this->trans('City', [], 'mobile'),
                'class' => 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity',
                'property' => 'name',
                'required' => false,
                'multiple' => true,
                'translation_domain' => 'mobile_form',
                'empty_value' => $this->trans('Izvēlies pilsētu', [], 'mobile'),
                'query_builder' =>
                    function(EntityRepository $er) {
                        return $er->createQueryBuilder('city')->orderBy('city.name', 'ASC');
                    }])
            ->add('sources', 'entity', [
                'label' => $this->trans('Resource', [], 'mobile'),
                'class' => 'VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource',
                'property' => 'resource',
                'required' => false,
                'multiple' => true,
                'translation_domain' => 'mobile_form',
                'empty_value' => $this->trans('Izvēlies avotu', [], 'mobile'),
                'query_builder' =>
                    function(EntityRepository $er) {
                        return $er->createQueryBuilder('s')->orderBy('s.resource', 'ASC');
                    }])
        ;
    }



    public function getDefaultOptions(array $options) {
        return [
            'data_class' => 'VisiDarbi\MobileBundle\Entity\MobileNewsletter',
            'csrf_protection' => true,
            'captcha'         => false
        ];
    }

    public function getName()
    {
        return 'newsletter';
    }
}
