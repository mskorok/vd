<?php

namespace VisiDarbi\MobileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

class RegType extends AbstractType
{
    private $translator;

    public function __construct( TranslatorInterface $translator = null)
    {

        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', [
                /** @Ignore */
                'label' => $this->translator->trans('E-mail', [], 'mobile_form'),
                'max_length' => 255,
                'required' => true,
                'attr' => ['class' => 'input-email'],
//                'label' => false,
            ])
            ->add('password', 'repeated', [
                'property_path' => 'plainPassword',
                'required' => true,
                'type' => 'password',
                'invalid_message' => 'Password does not match confirmation',
                'translation_domain' => 'mobile_form',
                'first_options'  => [
                    /** @Ignore */
                    'label' => $this->translator->trans('Password', [], 'mobile_form'),
                    'max_length' => 20,

                ],
                'second_options' => [
                    /** @Ignore */
                    'label' => $this->translator->trans('Confirm password', [], 'mobile_form'),
                    'max_length' => 20,

                ],
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'VisiDarbi\UserBundle\Entity\User',
            'validation_groups' => ['Mobile'],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'reg';
    }
}
