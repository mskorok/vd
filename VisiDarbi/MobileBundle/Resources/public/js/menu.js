window.onload = function(){
    var open =  document.getElementById('image-opened');
    var close = document.getElementById('image-closed');
    var close_text = document.getElementById('text-closed');
    var open_text = document.getElementById('text-opened');
    var menu = document.getElementById('content-footer');
    open.style.display = 'none';
    open_text.style.display = 'none';
    menu.style.display = 'none';
    close.onclick = function(){
        open.style.display = 'inline-block';
        open_text.style.display = 'inline-block';
        menu.style.display = 'block';
        close.style.display = 'none';
        close_text.style.display = 'none';
    };
    close_text.onclick = function(){
        open.style.display = 'inline-block';
        open_text.style.display = 'inline-block';
        menu.style.display = 'block';
        close.style.display = 'none';
        close_text.style.display = 'none';
    };
    open.onclick = function(){
        close.style.display = 'inline-block';
        close_text.style.display = 'inline-block';
        open.style.display = 'none';
        open_text.style.display = 'none';
        menu.style.display = 'none';
    };
    open_text.onclick = function(){
        close.style.display = 'inline-block';
        close_text.style.display = 'inline-block';
        open.style.display = 'none';
        open_text.style.display = 'none';
        menu.style.display = 'none';
    };


};