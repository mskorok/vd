var bgm = null;
var fb = [ null, null, null, null, null ];

function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function saveFavorite(id){
    var r = getXmlHttp();
    var url = '/mobile/save?id='+id;
    var img1Id = 'img1-'+id;
    var img2Id = 'img2-'+id;
    r.open("POST", url, true);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function () {
        if (r.readyState != 4 || r.status != 200){
            //console.log('error');
        }
        if (r.readyState == 4 && r.status == 200){
            var res = JSON.parse(r.responseText);
            var img1 = document.getElementById(img1Id);
            var img2 = document.getElementById(img2Id);
            if(res.action != undefined && res.action == 'saved'){
                img1.classList.add('wr-hidden');
                img2.classList.remove('wr-hidden');
            } else if(res.action != undefined && res.action == 'removed'){
                img1.classList.remove('wr-hidden');
                img2.classList.add('wr-hidden');
            }
            var d = new Date();
            console.log('success', d.getTime(), res);
        }
    };
    r.send();
}
function shareAdvert(id){
    var container = document.getElementById('mc-'+id);
    var modal = document.getElementById('sh-'+id);
    modal.classList.remove('wr-hidden');
    var wm = modal.offsetWidth;
    var fsz = window.getComputedStyle(modal).getPropertyValue('font-size');

    modal.classList.remove('wrapper-share-width');
    var new_modal = container.removeChild(modal);

    var body = document.getElementsByTagName('body');
    body = body[0];
    var h = body.offsetHeight;
    var wb = body.offsetWidth;
    var wr = window.document.getElementById('wrapper');
    var div = document.createElement("div");
    div.setAttribute('id', 'modal-container-on-fly');
    var bg = document.createElement("div");
    bg.setAttribute('id', id);
    bg.classList.add('bg-modal');
    bg.setAttribute('onclick', 'bgClose();');

    div.style.width = "100%";
    div.style.display = "inline-block";
    div.style.position = "relative";
    bg.style.height = h+'px';
    bg.style.width = "100%";
    bg.style.position = "absolute";
    bg.style.backgroundColor = "#000000";
    bg.style.opacity = 0.5;
    body.style.backgroundColor = "#151618";
    bg.style.zIndex = 75;
    bgm = bg;



    body.insertBefore(div, wr);
    div.appendChild(bg);
    div.appendChild(new_modal);

    new_modal.style.width = wm + 'px';
    new_modal.style.fontSize = fsz;
    new_modal.style.marginLeft = (wb-wm)/2 +'px';
    new_modal.style.marginTop = '1.5em';
    new_modal.style.backgroundColor = '#ffffff';
    new_modal.style.opacity = 1;
    new_modal.style.zIndex = 150;
    refreshCaptcha(id);
    var els = new_modal.getElementsByTagName('form');
    var form = els[0];
    form.addEventListener('submit', shareValidation);
    window.scrollTo(0, 0);

}
function refreshCaptcha(c_id){
    var url = '/genemu-captcha-refresh?' + Math.random();
    var id = c_id+"_image";

    document.getElementById(id).setAttribute('src', url);
}

function bgClose(type){
    type = type || null;
    var container = null;
    var modal = null;
    var id = bgm.id;
    var body = document.getElementsByTagName('body');
    body = body[0];
    if(type == 1){
        container = document.getElementById('header');
        modal = document.getElementById('wrapper-subscribe');
    }else if(type == 2){
        container = document.getElementById('header');
        modal = document.getElementById('wrapper-filter');
    }else{
        container = document.getElementById('mc-'+id);
        modal = document.getElementById('sh-'+id);
    }

    var bg = document.getElementById(id);
    var on_fly_container = document.getElementById('modal-container-on-fly');
    var new_modal = on_fly_container.removeChild(modal);
    container.appendChild(new_modal);
    var els = new_modal.getElementsByTagName('form');
    if(els instanceof Array){
        var form = els[0];
    }

    //if(type){
    //    form.removeEventListener('submit', subscribeValidation);//??????
    //}else{
    //    form.removeEventListener('submit', shareValidation);//??????
    //}
    new_modal.classList.add('wr-hidden');
    on_fly_container.removeChild(bg);
    body.removeChild(on_fly_container);
    body.style.backgroundColor = "#FFFFFF";
    bgm = null;

}


function shareValidation(e){

    var els = this.getElementsByTagName('input');
    var form  = this;
    var id = form.getAttribute('data-id');
    var text = this.getElementsByTagName('textarea');
    var message = text[0].value;


    var from = els[0].value;
    var to = els[1].value;
    var captcha = els[2].value;
    var share_id= els[3].value;



    var r = getXmlHttp();
    var url = '/mobile/share-validate?shareadvertisementform[email_from]='+from+
        '&shareadvertisementform[captcha]='+captcha+
        '&shareadvertisementform[email_to]='+to+
        '&shareadvertisementform[message]='+message+
        '&shareadvertisementform[share_id]='+share_id;
    r.open("POST", url, true);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function () {
        if (r.readyState != 4 || r.status != 200){
            //console.log('error');
        }
        if (r.readyState == 4 && r.status == 200){
            var res = JSON.parse(r.responseText);
            if(res.errors != undefined){
                var target = window.document.getElementById('form-'+id);
                var parent = window.document.getElementById('message-share-'+id);
                var div = document.createElement("div");
                div.innerHTML = res.errors;
                parent.insertBefore(div, target);
                window.scrollTo(0, 0);
                refreshCaptcha(id);
                e.preventDefault();
                e.stopPropagation();
                return false;
            }else{
                form.submit();
                return true;
            }

        } else if(r.readyState == 4 && r.status != 200){

            e.preventDefault();
            e.stopPropagation();
            return false;
        }


    };
    r.send();
    e.preventDefault();
    e.stopPropagation();
    return false;
}