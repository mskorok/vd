var toggle_menu = null;
function citiesPopup(){

    var container = document.getElementById('popup');
    var modal = document.getElementById('popup-container');
    var wm = document.getElementById('wrapper').offsetWidth;
    wm = 0.95 * wm;
    var fsz = window.getComputedStyle(document.getElementById('wrapper')).getPropertyValue('font-size');

    var new_modal = container.removeChild(modal);

    var body = document.getElementsByTagName('body');
    body = body[0];
    var h = body.offsetHeight;
    var wb = body.offsetWidth;
    var wr = window.document.getElementById('wrapper');
    var div = document.createElement("div");
    div.setAttribute('id', 'modal-container-on-fly');
    var bg = document.createElement("div");
    bg.setAttribute('id', 'bg-layer');
    bg.classList.add('bg-modal');
    bg.setAttribute('onclick', 'popupClose(0);');

    div.style.width = "100%";
    div.style.display = "inline-block";
    div.style.position = "relative";
    bg.style.height = h+'px';
    bg.style.width = "100%";
    bg.style.position = "absolute";
    bg.style.backgroundColor = "#000000";
    bg.style.opacity = 0.5;
    body.style.backgroundColor = "#151618";
    bg.style.zIndex = 75;


    body.insertBefore(div, wr);
    div.appendChild(bg);
    div.appendChild(new_modal);

    new_modal.style.width = wm + 'px';
    new_modal.style.fontSize = fsz;
    new_modal.style.marginLeft = (wb-wm)/2 +'px';
    new_modal.style.marginTop = '1.5em';
    new_modal.style.opacity = 1;
    new_modal.style.zIndex = 150;
    new_modal.style.position = 'absolute';




    window.scrollTo(0, 0);
}

function  popupClose(index){


    var body = document.getElementsByTagName('body');
    body = body[0];
     var container = document.getElementById('popup');
    var modal = document.getElementById('popup-container');
    if(index == 1){
        setSelect();
    }

    var bg = document.getElementById('bg-layer');
    var on_fly_container = document.getElementById('modal-container-on-fly');
    var new_modal = on_fly_container.removeChild(modal);
    container.appendChild(new_modal);


    on_fly_container.removeChild(bg);
    body.removeChild(on_fly_container);
    body.style.backgroundColor = "#2f3236";
    bgm = null;
}

function setSelect(){
    var inputs = window.document.getElementsByClassName('cities-inputs');

    var names = null;
    var ids = null;
    var data_id;
    var data_name;

    for (var i = 0; i < inputs.length; i++) {
        if(inputs[i].checked){
            data_id = parseInt(inputs[i].getAttribute('data-id'));
            if(ids){
                ids = ids + ','+data_id;
            }else{
                ids = data_id;
            }
            data_name = inputs[i].getAttribute('data-name');
            if(names){
                names = names + ','+data_name;
            }else{
                names = data_name;
            }
        }
    }
    window.document.getElementById('input-cities').setAttribute('value', ids);
    window.document.getElementById('input-cities').setAttribute('placeholder', names);

}

window.onload = function(){
    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    };


    window.document.getElementById('input-1').onfocus = function(){
        window.document.getElementById('select-1').classList.remove('wp-hidden');
        window.document.getElementById('img-1').classList.add('wp-hidden');
        this.classList.add('wp-hidden');
    };
    window.document.getElementById('input-2').onfocus = function(){
        window.document.getElementById('select-2').classList.remove('wp-hidden');
        window.document.getElementById('img-2').classList.add('wp-hidden');
        this.classList.add('wp-hidden');
    };
    window.document.getElementById('input-3').onfocus = function(){
        //citiesPopup();
        window.document.getElementById('select-3').classList.remove('wp-hidden');
        window.document.getElementById('img-3').classList.add('wp-hidden');
        this.classList.add('wp-hidden');
    };
    window.document.getElementById('input-4').onfocus = function(){
        window.document.getElementById('select-4').classList.remove('wp-hidden');
        window.document.getElementById('img-4').classList.add('wp-hidden');
        this.classList.add('wp-hidden');
    };


    var toggle_button = window.document.getElementById('toggle-menu');

    toggle_button.onclick = function(){
        var el = window.document.getElementById('hidden-menu');
        if(toggle_menu){
            el.classList.add('wr-hidden');
            toggle_menu = null;
        } else {
            el.classList.remove('wr-hidden');
            toggle_menu = 1;
        }
    };

};