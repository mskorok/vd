var bgm = null;
var fb = [ null, null, null, null, null ];

function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}
function registerUser(id){
    var modal_id = 'wr-'+id;

    var modal = document.getElementById(modal_id);
    modal.classList.remove('wr-hidden');
    setTimeout(function(){
            modal.classList.add('wr-hidden');
    },
    20000
    )

}
function saveFavorite(id){
    console.log(id);
    var r = getXmlHttp();
    var url = '/mobile/save?id='+id;
    var img1Id = 'img1-'+id;
    var img2Id = 'img2-'+id;
    r.open("POST", url, true);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function () {
        if (r.readyState != 4 || r.status != 200){
            //console.log('error');
        }
        if (r.readyState == 4 && r.status == 200){
            console.log(r.responseText);
            var res = JSON.parse(r.responseText);
            var img1 = document.getElementById(img1Id);
            var img2 = document.getElementById(img2Id);
            if(res.action != undefined && res.action == 'saved'){
                img1.classList.add('wr-hidden');
                img2.classList.remove('wr-hidden');
            } else if(res.action != undefined && res.action == 'removed'){
                img1.classList.remove('wr-hidden');
                img2.classList.add('wr-hidden');
            }
            var d = new Date();
            console.log('success', d.getTime(), res);
        }
    };
    r.send();
}
function shareAdvert(id){
    var container = document.getElementById('mc-'+id);
    var modal = document.getElementById('sh-'+id);
    modal.classList.remove('wr-hidden');
    var wm = modal.offsetWidth;
    var fsz = window.getComputedStyle(modal).getPropertyValue('font-size');

    modal.classList.remove('wrapper-share-width');
    var new_modal = container.removeChild(modal);

    var body = document.getElementsByTagName('body');
    body = body[0];
    var h = body.offsetHeight;
    var wb = body.offsetWidth;
    var wr = window.document.getElementById('wrapper');
    var div = document.createElement("div");
    div.setAttribute('id', 'modal-container-on-fly');
    var bg = document.createElement("div");
    bg.setAttribute('id', id);
    bg.classList.add('bg-modal');
    bg.setAttribute('onclick', 'bgClose();');

    div.style.width = "100%";
    div.style.display = "inline-block";
    div.style.position = "relative";
    bg.style.height = h+'px';
    bg.style.width = "100%";
    bg.style.position = "absolute";
    bg.style.backgroundColor = "#000000";
    bg.style.opacity = 0.5;
    body.style.backgroundColor = "#151618";
    bg.style.zIndex = 75;
    bgm = bg;



    body.insertBefore(div, wr);
    div.appendChild(bg);
    div.appendChild(new_modal);

    new_modal.style.width = wm + 'px';
    new_modal.style.fontSize = fsz;
    new_modal.style.marginLeft = (wb-wm)/2 +'px';
    new_modal.style.marginTop = '1.5em';
    new_modal.style.backgroundColor = '#ffffff';
    new_modal.style.opacity = 1;
    new_modal.style.zIndex = 150;
    refreshCaptcha(id);
    var els = new_modal.getElementsByTagName('form');
    var form = els[0];
    form.addEventListener('submit', shareValidation);
    window.scrollTo(0, 0);

}

function subscribeAdvert(id){
    var container = document.getElementById('header');
    var modal = document.getElementById('wrapper-subscribe');
    modal.classList.remove('wr-hidden');
    var wm = container.offsetWidth;
    wm = 0.95 * wm;
    var fsz = window.getComputedStyle(modal).getPropertyValue('font-size');

    modal.classList.remove('wrapper-share-width');
    var new_modal = container.removeChild(modal);

    var body = document.getElementsByTagName('body');
    body = body[0];
    var h = body.offsetHeight;
    var wb = body.offsetWidth;
    var wr = window.document.getElementById('wrapper');
    var div = document.createElement("div");
    div.setAttribute('id', 'modal-container-on-fly');
    var bg = document.createElement("div");
    bg.setAttribute('id', id);
    bg.classList.add('bg-modal');
    bg.setAttribute('onclick', 'bgClose(1);');

    div.style.width = "100%";
    div.style.display = "inline-block";
    div.style.position = "relative";
    bg.style.height = h+'px';
    bg.style.width = "100%";
    bg.style.position = "absolute";
    bg.style.backgroundColor = "#000000";
    bg.style.opacity = 0.5;
    body.style.backgroundColor = "#151618";
    bg.style.zIndex = 75;
    bgm = bg;

    body.insertBefore(div, wr);
    div.appendChild(bg);
    div.appendChild(new_modal);

    new_modal.style.width = wm + 'px';
    new_modal.style.fontSize = fsz;
    new_modal.style.marginLeft = (wb-wm)/2 +'px';
    new_modal.style.marginTop = '1.5em';
    new_modal.style.backgroundColor = '#ffffff';
    new_modal.style.opacity = 1;
    new_modal.style.zIndex = 150;
    refreshCaptcha(id);
    var els = new_modal.getElementsByTagName('form');
    var form = els[0];
    form.addEventListener('submit', subscribeValidation);
    window.scrollTo(0, 0);

}

function filterAdvert(){

    var container = document.getElementById('header');
    var modal = document.getElementById('wrapper-filter');
    modal.classList.remove('wr-hidden');
    var wm = container.offsetWidth;
    wm = 0.95 * wm;
    var fsz = window.getComputedStyle(modal).getPropertyValue('font-size');

    var new_modal = container.removeChild(modal);

    var body = document.getElementsByTagName('body');
    body = body[0];
    var h = body.offsetHeight;
    var wb = body.offsetWidth;
    var wr = window.document.getElementById('wrapper');
    var div = document.createElement("div");
    div.setAttribute('id', 'modal-container-on-fly');
    var bg = document.createElement("div");
    bg.setAttribute('id', 'bg-layer');
    bg.classList.add('bg-modal');
    bg.setAttribute('onclick', 'bgClose(2);');

    div.style.width = "100%";
    div.style.display = "inline-block";
    div.style.position = "relative";
    bg.style.height = h+'px';
    bg.style.width = "100%";
    bg.style.position = "absolute";
    bg.style.backgroundColor = "#000000";
    bg.style.opacity = 0.5;
    body.style.backgroundColor = "#151618";
    bg.style.zIndex = 75;
    bgm = bg;

    body.insertBefore(div, wr);
    div.appendChild(bg);
    div.appendChild(new_modal);

    new_modal.style.width = wm + 'px';
    new_modal.style.fontSize = fsz;
    new_modal.style.marginLeft = (wb-wm)/2 +'px';
    new_modal.style.marginTop = '1.5em';
    new_modal.style.backgroundColor = '#ffffff';
    new_modal.style.opacity = 1;
    new_modal.style.zIndex = 150;

    var filterButton1 = window.document.getElementById('filter-button-1');
    var filterButton2 = window.document.getElementById('filter-button-2');
    var filterButton3 = window.document.getElementById('filter-button-3');
    var filterButton4 = window.document.getElementById('filter-button-4');

    filterButton1.addEventListener('click', function(e){

        fbClick(1, e, this);
    });
    filterButton2.addEventListener('click', function (e) {

        fbClick(2, e, this);
    });
    filterButton3.addEventListener('click', function (e) {

        fbClick(3, e, this);
    });
    filterButton4.addEventListener('click', function (e) {

        fbClick(4, e, this);
    });


    window.scrollTo(0, 0);
}




function refreshCaptcha(c_id){
    var url = '/genemu-captcha-refresh?' + Math.random();
    var id = c_id+"_image";

    document.getElementById(id).setAttribute('src', url);
}

function bgClose(type){
    type = type || null;
    var container = null;
    var modal = null;
    var id = bgm.id;
    var body = document.getElementsByTagName('body');
    body = body[0];
    if(type == 1){
        container = document.getElementById('header');
        modal = document.getElementById('wrapper-subscribe');
    }else if(type == 2){
        container = document.getElementById('header');
        modal = document.getElementById('wrapper-filter');
    }else{
        container = document.getElementById('mc-'+id);
        modal = document.getElementById('sh-'+id);
    }

    var bg = document.getElementById(id);
    var on_fly_container = document.getElementById('modal-container-on-fly');
    var new_modal = on_fly_container.removeChild(modal);
    container.appendChild(new_modal);
    var els = new_modal.getElementsByTagName('form');
    if(els instanceof Array){
        var form = els[0];
    }

    //if(type){
    //    form.removeEventListener('submit', subscribeValidation);//??????
    //}else{
    //    form.removeEventListener('submit', shareValidation);//??????
    //}
    new_modal.classList.add('wr-hidden');
    on_fly_container.removeChild(bg);
    body.removeChild(on_fly_container);
    body.style.backgroundColor = "#FFFFFF";
    bgm = null;

}


function shareValidation(e){

    var els = this.getElementsByTagName('input');
    var form  = this;
    var id = form.getAttribute('data-id');
    var text = this.getElementsByTagName('textarea');
    var message = text[0].value;


    var from = els[0].value;
    var to = els[1].value;
    var captcha = els[2].value;
    var share_id= els[3].value;



    var r = getXmlHttp();
    var url = '/mobile/share-validate?shareadvertisementform[email_from]='+from+
        '&shareadvertisementform[captcha]='+captcha+
        '&shareadvertisementform[email_to]='+to+
        '&shareadvertisementform[message]='+message+
        '&shareadvertisementform[share_id]='+share_id;
    r.open("POST", url, true);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function () {
        if (r.readyState != 4 || r.status != 200){
            //console.log('error');
        }
        if (r.readyState == 4 && r.status == 200){
            var res = JSON.parse(r.responseText);
            if(res.errors != undefined){
                var target = window.document.getElementById('form-'+id);
                var parent = window.document.getElementById('message-share-'+id);
                var div = document.createElement("div");
                div.innerHTML = res.errors;
                parent.insertBefore(div, target);
                window.scrollTo(0, 0);
                refreshCaptcha(id);
                e.preventDefault();
                e.stopPropagation();
                return false;
            }else{
                form.submit();
                return true;
            }

        } else if(r.readyState == 4 && r.status != 200){

            e.preventDefault();
            e.stopPropagation();
            return false;
        }


    };
    r.send();
    e.preventDefault();
    e.stopPropagation();
    return false;
}

function subscribeValidation(e){
    var els = this.getElementsByTagName('input');
    var form = this;



    var email = els[0].value;
    var captcha = els[1].value;
    var searchWhere = els[2].value;
    var searchWhat = els[3].value;
    var filter = els[4].value;




    var r = getXmlHttp();
    var url = '/mobile/subscribe-validate?subscribeadvertisementform[email]='+email+
        '&subscribeadvertisementform[captcha]='+captcha+
        '&subscribeadvertisementform[searchWhere]='+searchWhere+
        '&subscribeadvertisementform[searchWhat]='+searchWhat+
        '&subscribeadvertisementform[filter]='+filter;
    r.open("POST", url, true);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function () {
        if (r.readyState != 4 || r.status != 200){
            //console.log('error');
        }
        if (r.readyState == 4 && r.status == 200){
            var res = JSON.parse(r.responseText);
            if(res.errors != undefined){
                var target = window.document.getElementById('subscribe-text');
                var parent = window.document.getElementById('subscribe-message');
                var div = document.createElement("div");
                div.innerHTML = res.errors;
                parent.insertBefore(div, target);
                window.scrollTo(0, 0);
                refreshCaptcha(cid);
                e.preventDefault();
                e.stopPropagation();
                return false;
            }else{
                form.submit();
                return true;
            }

        } else if(r.readyState == 4 && r.status != 200){

            e.preventDefault();
            e.stopPropagation();
            return false;
        }


    };
    r.send();
    e.preventDefault();
    e.stopPropagation();
    return false;

}

function fbClick(key, e, el ){


    var img,  btn_def, btn_mod, input;
    img = window.document.getElementById('hidden-image-filter'+key);
    input = window.document.getElementById('sort-'+key);

    btn_def = 'submit-button-default';
    btn_mod = 'submit-button-modified';
    if(fb[key] === null){

        el.classList.remove(btn_def);
        el.classList.add(btn_mod);
        input.setAttribute('value', 'DESC');
        img.style.display = 'inline-block';
        fb[key] = 1;

    } else {
        el.classList.remove(btn_mod);
        el.classList.add(btn_def);
        input.setAttribute('value', 'ASC');
        img.style.display = 'none';
        fb[key] = null;
    }
}


function showDropDown5(el) {
    if (window.document.createEvent) { // All
        var evt = window.document.createEvent("MouseEvents");
        evt.initMouseEvent("mousedown", false, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        var res = el.dispatchEvent(evt);
        console.log(res);
    } else if (el.fireEvent) { // IE
        el.fireEvent("onmousedown");
    }
}

function showDropDown(targetNode) {


    if (targetNode) {
        //--- Simulate a natural mouse-click sequence.
        triggerMouseEvent (targetNode, "mousedown");

    }  else {
        console.log ("*** Target node not found!");
    }

    function triggerMouseEvent (node, eventType) {
        var clickEvent = document.createEvent ('MouseEvents');
        clickEvent.initEvent (eventType, true, true);
        console.log(clickEvent);
        var res = node.dispatchEvent (clickEvent);
        console.log(res);
    }

}


function toggleSelect(i){
    var el1 = document.getElementById('select-1');
    var el2 = document.getElementById('select-2');
    var el3 = document.getElementById('select-3');
    var el4 = document.getElementById('select-4');
    console.log(el1);
    switch (i) {
        case 1:
            //showDropDown(el1);
            el1.setAttribute('size', '5');
            el1.focus();
            el2.setAttribute('size', '1');
            el3.setAttribute('size', '1');
            el4.setAttribute('size', '1');
            break;
        case 2:
            //showDropDown(el2);
            el1.setAttribute('size', '1');
            el2.setAttribute('size', '5');
            el2.focus();
            el3.setAttribute('size', '1');
            el4.setAttribute('size', '1');
            break;
        case 3:
            //showDropDown(el3);
            el1.setAttribute('size', '1');
            el2.setAttribute('size', '1');
            el3.setAttribute('size', '5');
            el3.focus();
            el4.setAttribute('size', '1');
            break;
        case 4:
            //showDropDown(el4);
            el1.setAttribute('size', '1');
            el2.setAttribute('size', '1');
            el3.setAttribute('size', '1');
            el4.setAttribute('size', '5');
            el4.focus();
            break;
    }
}

window.onload = function(){
    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    };
    var subscribe = window.document.getElementById('subscribe-modal-up');
    subscribe.onclick= function(){
        subscribeAdvert(cid);
    };

    window.document.getElementById('select-1').addEventListener('click', function(e) {
       toggleSelect(1);
    });
    window.document.getElementById('select-2').addEventListener('click', function(e) {
        toggleSelect(2);
    });
    window.document.getElementById('select-3').addEventListener('click', function(e) {
        toggleSelect(3);
    });
    window.document.getElementById('select-4').addEventListener('click', function(e) {
        toggleSelect(4);
    });
    window.document.getElementById('input-1').onfocus = function(e){
        e.preventDefault();
        e.stopPropagation();
        this.classList.add('filter-hide');
        window.document.getElementById('div-select-1').classList.remove('filter-hide');
        window.document.getElementById('img-1').classList.add('filter-hide');
        setTimeout(function(){
            window.document.getElementById('select-1').click();
            window.document.getElementById('select-1').focus();
        },100);

    };
    window.document.getElementById('input-2').onfocus = function(e){
        e.preventDefault();
        e.stopPropagation();
        window.document.getElementById('div-select-2').classList.remove('filter-hide');
        window.document.getElementById('img-2').classList.add('filter-hide');
        this.classList.add('filter-hide');
        setTimeout(function(){
            window.document.getElementById('select-2').click();
            window.document.getElementById('select-2').focus();
        },100);

    };
    window.document.getElementById('input-3').onfocus = function(e){
        e.preventDefault();
        e.stopPropagation();
        window.document.getElementById('div-select-3').classList.remove('filter-hide');
        window.document.getElementById('img-3').classList.add('filter-hide');
        this.classList.add('filter-hide');
        setTimeout(function(){
            window.document.getElementById('select-3').click();
            window.document.getElementById('select-3').focus();
        },100);

    };
    window.document.getElementById('input-4').onfocus = function(e){
        e.preventDefault();
        e.stopPropagation();
        window.document.getElementById('div-select-4').classList.remove('filter-hide');
        window.document.getElementById('img-4').classList.add('filter-hide');
        this.classList.add('filter-hide');
        setTimeout(function(){
            window.document.getElementById('select-4').click();
            window.document.getElementById('select-4').focus();
        },100);

    }




};