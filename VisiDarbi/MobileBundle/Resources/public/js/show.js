function registerUser(id){
    var modal_id = 'wr-'+id;

    var modal = document.getElementById(modal_id);
    modal.classList.remove('wr-hidden');
    setTimeout(function(){
            modal.classList.add('wr-hidden');
        },
        20000
    )

}


window.onload = function(){
    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    }
};