/**
 * Created by mike on 15.9.29.
 */

var offset = 4;
var allowScroll = true;

function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}
var iii = 0;
function addItems(){
    allowScroll = false;
    var r = getXmlHttp();
    var url = '/mobile/next?offset='+offset + '&rr=987';
    r.open("POST", url, true);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function () {
        var disp;
        if (r.readyState != 4 || r.status != 200){
            document.getElementById('load-block').style.display = 'none';
            disp = document.getElementById('load-block').style.display;
            allowScroll = true;
            return;
        }
        document.getElementById('load-block').style.display = 'none';
        var res = JSON.parse(r.responseText);
        offset = res.offset;
       // console.log(iii++,offset);
        if(res.content !== ''){
            var span = document.createElement("span");
            span.innerHTML = res.content;
            var parent = document.getElementById('content-center');
            parent.insertBefore(span, parent.lastChild);
            allowScroll = true;
        }else{
            allowScroll = false;
        }



    };
    document.getElementById('load-block').style.display = 'block';
    r.send();


}

function getCoordinates(){
    navigator.geolocation.getCurrentPosition(GetLocation);
    function GetLocation(location) {
        getPlace(location.coords.latitude, location.coords.longitude);
    }
}
function getPlace(lat, long){
    var r = getXmlHttp();
    var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+long+'&sensor=false';
    r.open("GET", url, true);
    r.onreadystatechange = function () {
        if (r.readyState == 4 && r.status == 200){
            var res = JSON.parse(r.responseText);
            res.results.forEach(function(val, i, array){
                if(val.types != undefined){
                    if(val.types[0] == 'locality' && val.address_components[0].short_name != undefined){
                        var input = window.document.getElementById("mobileSearchAdvertisement_where");
                        var place = val.address_components[0].short_name;
                        input.setAttribute('value', place);
                        input.setAttribute('placeholder', place);

                    }
                }
            });
        }
    };
    r.send();
}

function autocompleteWhat(){

    var ajax = getXmlHttp();
    var method = 'GET';
    var url = '/mobile/autocomplete-what';
    ajax.open(method, url, true);
    ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    ajax.onload = function() {
        {

            var list = JSON.parse(ajax.responseText).map(function(i) {
                return i.name;
            });

            new Awesomplete(document.querySelector("#ajax-complete-what input"),{list:list});
        }
    };
    ajax.send();
}


function autocompleteWhere(){

    var ajax = getXmlHttp();
    var method = 'POST';
    var url = '/mobile/autocomplete-where';
    ajax.open(method, url, true);
    ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    ajax.onload = function() {
        {

            var list = JSON.parse(ajax.responseText).map(function(i) {
                return i.name;
            });

            new Awesomplete(document.querySelector("#ajax-complete-where input"),{list:list});
        }
    };
    ajax.send();
}

function  submitSearch(){

    var what = document.getElementById('mobileSearchAdvertisement_what').value;
    what.trim();
    var where = document.getElementById('mobileSearchAdvertisement_where').value;
    where.trim();


    var ajax = getXmlHttp();
    var method = 'POST';
    var url = '/mobile/search-route?what='+what+'&where='+where;
    ajax.open(method, url, true);
    ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    ajax.onload = function() {
        if (ajax.readyState == 4 && ajax.status == 200){
            var results = JSON.parse(ajax.responseText);
            if(results.status == 'true'){
                document.location.href = results.link;
            }

        }
    };
    ajax.send();

}

window.onload = function(){

    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    };
    var element = document.getElementById('wrapper');
    var size = window.getComputedStyle(element).fontSize;
    var offsetHeight = parseFloat(document.getElementById('wrapper').offsetHeight);

    window.document.getElementById('geo-point').onclick = function(){
        getCoordinates();

    };

    document.getElementById('search-form').addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        submitSearch();
    });

    //autocompleteWhat();

    autocompleteWhere();

    window.onscroll = function() {
        if(allowScroll){
            var scrolled = window.pageYOffset || document.documentElement.scrollTop;
            scrolled = parseFloat(scrolled);
            size = parseFloat(size);
            var level = scrolled/size;
            var ah = parseFloat(window.screen.availHeight);
            if((offsetHeight/size -level - ah/size) < 0){
                addItems();
            }
        }
    };

};



