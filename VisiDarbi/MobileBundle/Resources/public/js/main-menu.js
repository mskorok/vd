var toggle_menu = null;

window.onload = function(){
    var toggle_button = window.document.getElementById('toggle-menu');

    toggle_button.onclick = function(){
        var el = window.document.getElementById('hidden-menu');
        if(toggle_menu){
            el.classList.add('wr-hidden');
            toggle_menu = null;
        } else {
            el.classList.remove('wr-hidden');
            toggle_menu = 1;
        }
    };
};


