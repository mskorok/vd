var toggle_menu = null;
window.onload = function(){
    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    };
    var name = document.getElementById('profile_name');
    var surname = document.getElementById('profile_surname');
    var email = document.getElementById('profile_email');
    var phone = document.getElementById('profile_phone');
    var password = document.getElementById('profile_password');
    var address = document.getElementById('profile_address');
    var button = document.getElementById('profile-submit');
    var once = true;
    function changeButtonText(){
        button.textContent = save;
    }

    name.onfocus = function(e){
        e.stopPropagation();
        if(once){
            once = false;
            changeButtonText();
        }

    };
    surname.onfocus = function(e){
        e.stopPropagation();
        if(once){
            once = false;
            changeButtonText();
        }

    };
    email.onfocus = function(e){
        e.stopPropagation();
        if(once){
            once = false;
            changeButtonText();
        }
    };
    phone.onfocus = function(e){
        e.stopPropagation();
        if(once){
            once = false;
            changeButtonText();
        }
    };
    password.onfocus = function(e){
        e.stopPropagation();
        if(once){
            once = false;
            changeButtonText();
        }
    };
    address.onfocus = function(e){
        e.stopPropagation();
        if(once){
            once = false;
            changeButtonText();
        }
    };


    var toggle_button = window.document.getElementById('toggle-menu');

    toggle_button.onclick = function(){
        var el = window.document.getElementById('hidden-menu');
        if(toggle_menu){
            el.classList.add('wr-hidden');
            toggle_menu = null;
        } else {
            el.classList.remove('wr-hidden');
            toggle_menu = 1;
        }
    };


};