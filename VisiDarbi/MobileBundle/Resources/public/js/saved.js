var toggle_menu = null;

function shortAdvert(id){
    var targetId = 'job-row-'+id;
    var elId = 'job-row-opened-'+id;
    changeHidden(elId,targetId);
}

function openedAdvert(id){
    var targetId = 'job-row-opened-'+id;
    var elId = 'job-row-'+id;
    changeHidden(elId,targetId);
}

function changeHidden(elId, targetId){
    var target = window.document.getElementById(targetId);
    var el = window.document.getElementById(elId);
    target.classList.remove('job-hidden');
    el.classList.add('job-hidden');
}


window.onload = function(){
    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    };


    var toggle_button = window.document.getElementById('toggle-menu');

    toggle_button.onclick = function(){
        var el = window.document.getElementById('hidden-menu');
        if(toggle_menu){
            el.classList.add('wr-hidden');
            toggle_menu = null;
        } else {
            el.classList.remove('wr-hidden');
            toggle_menu = 1;
        }
    };
};