/**
 * Created by mike on 15.10.9.
 */

window.onload = function(){
    document.getElementById('country-selectors').style.display = 'none';
    var selector = document.getElementById('select-country');
    selector.onclick = function(){
        var selectors = document.getElementById('country-selectors');

        if(selectors.style.display == 'none'){
            document.getElementById('country-selectors').style.display = 'block';
        }else{
            document.getElementById('country-selectors').style.display = 'none';
        }

    }
}
