<?php

namespace VisiDarbi\MobileBundle\Manager;

use Doctrine\ORM\EntityManager;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation;
use Symfony\Component\HttpFoundation\File\File as FileObject;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation;
use VisiDarbi\AdvertisementBundle\Entity\DutyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\OfferTranslation;
use VisiDarbi\AdvertisementBundle\Entity\RequirementTranslation;
use VisiDarbi\StatisticsBundle\Entity\Filter;

use VisiDarbi\StatisticsBundle\Entity\TextFilter;

use Gedmo\Sluggable\Util\Urlizer;
use VisiDarbi\AdvertisementBundle\Manager\AdvertisementManager as Base;

class AdvertisementManager extends  Base
{



    public function encodeFilter(array $filterData){

        $encoded_filter = base64_encode(serialize($filterData));
        $filter_key = md5($encoded_filter);

        $filterExists = $this->getDecodedFilter($filter_key);
        if(!$filterExists){

            $filter = new Filter();
            $filter->setFilterKey($filter_key);
            $filter->setFilterValue($encoded_filter);

            $this->em->persist($filter);
            $this->em->flush();
        }

        return array(
            'value' => $filterExists,
            'key' => $filter_key
        );
    }

    public function getDecodedFilter($filterKey, $type=false){
        /** @var Filter $filterExists */
        $filterExists = $this->em->getRepository('VisiDarbi\StatisticsBundle\Entity\Filter')->findOneBy(array('filter_key' => $filterKey));

        if($filterExists){
            $filterValue = $filterExists->getFilterValue();
            if( is_resource($filterValue)){
                $filterValue = unserialize(base64_decode(stream_get_contents($filterValue)));
            } else {
                $filterValue = unserialize(base64_decode($filterValue));
            }

            return (is_array($filterValue))?$filterValue:null;
        }

        return false;
    }

    public function setFilter($sort)
    {


        $data['countries'] = (array_key_exists('countries', $sort))?$sort['countries']:null;
        $data['cities']    = (array_key_exists('cities', $sort))?$sort['cities']:null;
        $data['positions'] = (array_key_exists('positions', $sort))?$sort['positions']:null;
        $data['companies'] = (array_key_exists('companies', $sort))?$sort['companies']:null;
        $data['resources'] = (array_key_exists('resources', $sort))?$sort['resources']:null;
        $data['professions'] = (array_key_exists('professions', $sort))?$sort['professions']:null;
        $data['position-direction'] = (array_key_exists('position-direction', $sort))?$sort['position-direction']:null;
        $data['company-direction'] = (array_key_exists('company-direction', $sort))?$sort['company-direction']:null;
        $data['date-direction'] = (array_key_exists('date-direction', $sort))?$sort['date-direction']:null;
        $data['source-direction'] = (array_key_exists('source-direction', $sort))?$sort['source-direction']:null;

        if(
            $data['countries']         ||
            $data['cities']            ||
            $data['positions']         ||
            $data['companies']         ||
            $data['resources']         ||
            $data['source-direction']  ||
            $data['date-direction']    ||
            $data['company-direction'] ||
            $data['position-direction']
        ){

            $filteredData = array();
            foreach($data as $key => $valueArray){
                $filteredValues = array();
                if($valueArray && is_array($valueArray) && in_array($key, ['countries', 'cities', 'positions', 'companies', 'resources', 'professions'])){
                    foreach($valueArray as $value){
                        if( (int)$value && !array_key_exists((int)$value, $filteredValues)){
                            $filteredValues[(int)$value] = true;
                        }
                    }
                    $filteredData[$key] = $filteredValues;
                } else {
                    $filteredData[$key] = $valueArray;
                }

            }

            $filter_decoded = $this->encodeFilter($filteredData);

//            $filter_key = $filter_decoded['key'];
            return $filter_decoded;
        } else {
            return null;
        }
    }





}