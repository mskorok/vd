<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.10.8
 * Time: 17.21
 */

namespace VisiDarbi\MobileBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\CMSBundle\Entity\Page;


class MenuMobileBuilder extends ContainerAware{

    /**
     * @var MenuHelper
     */
    protected $helper;
    /**
     * @var FactoryInterface
     */
    protected $factory;
    /**
     * @var TranslatorInterface
     */
    protected $translator;






    /**
     *
     * @return \Knp\Menu\ItemInterface
     *
     *
     */
    public function regTopMenu()
    {
        $menu = $this->factory->createItem('root');
        $profile      = $this->translator->trans('Rediģēt profilu', [], 'mobile_menu');
        $menu->addChild($profile, ['route' => 'profile_mobile']);
        $menu[$profile]->setAttribute('class', 'menu-item');
        $menu = $this->menuCommon($menu);



        return $this->menuCommon($menu);
    }

    public function guestTopMenu(){
        $menu = $this->factory->createItem('root');
        return $this->menuCommon($menu);
    }
    public function regBottomMenu(){
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'simple');

        $menuPages = $this->helper->getPages();
        if(is_array($menuPages) && count($menuPages) >0 ){
            return $this->getBottomMenuFromDb($menuPages);
        }else{
            return $this->getRegBottomMenu();
        }
    }


    protected function menuCommon(ItemInterface $menu)
    {
        $email    = $this->translator->trans('E-pasta paziņojumi par vakancēm', [], 'mobile_menu');
        $search      = $this->translator->trans('Meklētie sludinājumi', [], 'mobile_menu');
        $saved    = $this->translator->trans('Saglabātie sludinājumi', [], 'mobile_menu');


        $menu->addChild($email, ['route' => 'newsletters_mobile']);
        $menu->addChild($search, ['route' => 'search_history_mobile']);
        $menu->addChild($saved, ['route' => 'saved_mobile']);



        $menu[$email]->setAttribute('class', 'menu-item');
        $menu[$search]->setAttribute('class', 'menu-item');
        $menu[$saved]->setAttribute('class', 'menu-item');

        return $menu;

    }

    /**
     *
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->init();
    }

    private function init()
    {
        /** @var MenuHelper $helper */
        $this->helper = $this->container->get('mobile.menu_builder');
        $this->factory = $this->helper->getFactory();
        $this->translator = $this->helper->getTranslator();
    }

    private function getRegBottomMenu()
    {
        $menu = $this->factory->createItem('root');
        $item1    = $this->translator->trans('CV paraugs', [], 'mobile_menu');
        $item2    = $this->translator->trans('CV rakstīšana', [], 'mobile_menu');
        $item3    = $this->translator->trans('Motivācijas / pieteikuma vēstule', [], 'mobile_menu');
        $item4    = $this->translator->trans('Darba intervija', [], 'mobile_menu');
        $item5    = $this->translator->trans('Darba piedāvājumu analīze', [], 'mobile_menu');
        $item6    = $this->translator->trans('Darba interviju kļūdas', [], 'mobile_menu');

        $menu->addChild($item1, [
            'route' => 'document_mobile',
            'routeParameters' => ['id'=> 47,'slug' => $this->translator->trans('cv-paraugs', [], 'mobile_menu')]
        ]);
        $menu->addChild($item2, [
            'route' => 'document_mobile',
            'routeParameters' => ['id'=> 44, 'slug' => $this->translator->trans('cv-rakstisana', [], 'mobile_menu')]
        ]);
        $menu->addChild($item3, [
            'route' => 'document_mobile',
            'routeParameters' => ['id'=> 45, 'slug' => $this->translator->trans('motivacijas-vestule', [], 'mobile_menu')]
        ]);
        $menu->addChild($item4, [
            'route' => 'document_mobile',
            'routeParameters' => ['id'=> 46, 'slug' => $this->translator->trans('darba-intervija', [], 'mobile_menu')]
        ]);
        $menu->addChild($item5, [
            'route' => 'document_mobile',
            'routeParameters' => ['id'=> 48, 'slug' => $this->translator->trans('darba-piedavajumu-analize', [], 'mobile_menu')]
        ]);
        $menu->addChild($item6, [
            'route' => 'document_mobile',
            'routeParameters' => ['id'=> 49, 'slug' => $this->translator->trans('darba-interviju-kludas', [], 'mobile_menu')]
        ]);





        $menu[$item1]->setAttribute('class', 'menu-item');
        $menu[$item2]->setAttribute('class', 'menu-item');
        $menu[$item3]->setAttribute('class', 'menu-item');
        $menu[$item4]->setAttribute('class', 'menu-item');
        $menu[$item5]->setAttribute('class', 'menu-item');
        $menu[$item6]->setAttribute('class', 'menu-item');

        return $menu;
    }

    private function getBottomMenuFromDb($menuPages)
    {
        $menu = $this->factory->createItem('root');
        /** @var Page $page */
        foreach ($menuPages as $page) {
            $menu->addChild($page->getTitle(), [
                'route' => 'page_mobile',
                'routeParameters' => ['slug' => $page->getSlug()],
            ]);
        }
        return $menu;
    }


}