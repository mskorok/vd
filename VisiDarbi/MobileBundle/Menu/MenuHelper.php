<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.10.8
 * Time: 22.28
 */

namespace VisiDarbi\MobileBundle\Menu;


use Knp\Menu\FactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\CMSBundle\Menu\MenuBuilder;

class MenuHelper extends  MenuBuilder
{

    public function getPages(){
        return $this->getPageQb('mobile')->getQuery()->getResult();
    }

    /**
     * @return FactoryInterface
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslator()
    {
        return $this->translator;
    }




}