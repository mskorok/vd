<?php

namespace VisiDarbi\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

use VisiDarbi\MobileBundle\Entity\DocumentMobileLink;


class DocumentMobileLinkAdmin extends Admin
{
    /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;
    
    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {   
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('url')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('url', null, array(
                'required' => true
            ))
            ->add('title')
            ->add('target', 'choice', array(
                'choices' => array(
                    '_blank' => 'New window',
                    '_self' => 'Same window',
                ),
                'translation_domain' => 'mobile_admin',
            ))
        ;
    }
    
    
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
        ;
    }
}