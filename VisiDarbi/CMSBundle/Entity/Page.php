<?php

namespace VisiDarbi\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Page
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VisiDarbi\CMSBundle\Repository\PageRepository")
 * @Gedmo\Tree(type="nested")
 */
class Page
{
    const MENU_FOOTER = 'footer';
    const MENU_LEFT_SIDE = 'left_side';
    
    const SPECIAL_CONTACTS = 'contacts';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $enabled;
    
    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $locale;

    
    //@Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\InversedRelativeSlugHandler", options={
    //          @Gedmo\SlugHandlerOption(name="relationClass", value="VisiDarbi\CMSBundle\Entity\Document"),
    //          @Gedmo\SlugHandlerOption(name="mappedBy", value="page"),
    //          @Gedmo\SlugHandlerOption(name="inverseSlugField", value="slug")
    //      })
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Gedmo\Slug(fields={"slug_source"}, unique=true, unique_base="country", handlers={
     *         
     *     @Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\TreeSlugHandler", options={
     *          @Gedmo\SlugHandlerOption(name="parentRelationField", value="parent"),
     *          @Gedmo\SlugHandlerOption(name="separator", value="/")
     *      })
     * })
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="menu", type="string", length=255, nullable=true)
     */
    private $menu;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $seo_title;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $seo_description;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $seo_keywords;
    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\CMSBundle\Entity\Document", mappedBy="page", cascade={"persist", "remove"})
     */
    private $documents;
    
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="VisiDarbi\CMSBundle\Entity\Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\CMSBundle\Entity\Page", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;
    
    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;
    
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug_source;
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\CMSBundle\Entity\Page", inversedBy="mirroredPages") 
     * @ORM\JoinColumn(name="mirror_page_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */	
    private $mirrorPage;
    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\CMSBundle\Entity\Page", mappedBy="mirrorPage")
     */
    private $mirroredPages;
    
    
    public function __toString()
    {
        if ($this->parent === null) {
            return (string)$this->title;
        }
        
        return (string)$this->parent . ' / ' . $this->title;
    }

    public function getLeveledTitle()
    {
        return (string)$this;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set menu
     *
     * @param string $menu
     * @return Page
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    
        return $this;
    }

    /**
     * Get menu
     *
     * @return string 
     */
    public function getMenu()
    {
        return $this->menu;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        
        $this->enabled = false;
    }
    
    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Page
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seo_title = $seoTitle;
    
        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Page
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seo_description = $seoDescription;
    
        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Page
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seo_keywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string 
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     * @return Page
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    
        return $this;
    }

    /**
     * Get lft
     *
     * @return integer 
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     * @return Page
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;
    
        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer 
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     * @return Page
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    
        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer 
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param integer $root
     * @return Page
     */
    public function setRoot($root)
    {
        $this->root = $root;
    
        return $this;
    }

    /**
     * Get root
     *
     * @return integer 
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Add documents
     *
     * @param \VisiDarbi\CMSBundle\Entity\Document $documents
     * @return Page
     */
    public function addDocument(\VisiDarbi\CMSBundle\Entity\Document $documents)
    {
        $this->documents[] = $documents;
    
        return $this;
    }

    /**
     * Remove documents
     *
     * @param \VisiDarbi\CMSBundle\Entity\Document $documents
     */
    public function removeDocument(\VisiDarbi\CMSBundle\Entity\Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set parent
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $parent
     * @return Page
     */
    public function setParent(\VisiDarbi\CMSBundle\Entity\Page $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \VisiDarbi\CMSBundle\Entity\Page 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $children
     * @return Page
     */
    public function addChildren(\VisiDarbi\CMSBundle\Entity\Page $children)
    {
        $this->children[] = $children;
    
        return $this;
    }

    /**
     * Remove children
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $children
     */
    public function removeChildren(\VisiDarbi\CMSBundle\Entity\Page $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return Page
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Page
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Page
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    
        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    

    /**
     * Set slugSource
     *
     * @param string $slugSource
     * @return Page
     */
    public function setSlugSource($slugSource)
    {
        $this->slug_source = $slugSource;
    
        return $this;
    }

    /**
     * Get slugSource
     *
     * @return string 
     */
    public function getSlugSource()
    {
        return $this->slug_source;
    }

    /**
     * Set mirrorPage
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $mirrorPage
     * @return Page
     */
    public function setMirrorPage(\VisiDarbi\CMSBundle\Entity\Page $mirrorPage = null)
    {
        $this->mirrorPage = $mirrorPage;
    
        return $this;
    }

    /**
     * Get mirrorPage
     *
     * @return \VisiDarbi\CMSBundle\Entity\Page 
     */
    public function getMirrorPage()
    {
        return $this->mirrorPage;
    }

    /**
     * Add mirroredPages
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $mirroredPages
     * @return Page
     */
    public function addMirroredPage(\VisiDarbi\CMSBundle\Entity\Page $mirroredPages)
    {
        $this->mirroredPages[] = $mirroredPages;
    
        return $this;
    }

    /**
     * Remove mirroredPages
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $mirroredPages
     */
    public function removeMirroredPage(\VisiDarbi\CMSBundle\Entity\Page $mirroredPages)
    {
        $this->mirroredPages->removeElement($mirroredPages);
    }

    /**
     * Get mirroredPages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMirroredPages()
    {
        return $this->mirroredPages;
    }
    
    
    public function getSeoData()
    {
        return array(
            'title' => $this->seo_title ?: $this->title,
            'description' => $this->seo_description ?: $this->title,
            'keywords' => $this->seo_keywords ?: '',
        );
    }
    
    
    public function getRootPage()
    {
        if ($this->parent === null) {
            return $this;
        }
        
        return $this->parent->getRootPage();
    }
}