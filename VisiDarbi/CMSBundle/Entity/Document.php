<?php

namespace VisiDarbi\CMSBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\CMSBundle\Repository\DocumentRepository")
 */
class Document
{
    const SPECIAL_ADV_SOURCES = 'adv_sources';
    const SPECIAL_TOKEN_FAIL  = 'token_fail';
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @Gedmo\SortableGroup
     * @ORM\Column(type="string", nullable=false)
     */
    private $locale;
    
    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /** 
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /** 
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /** 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $image_alt;
    
    /** 
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $enabled;


    /**
     * @ORM\Column(type="boolean", nullable=false, options={"unsigned"=true, "default" = 0})
     */
    protected $is_mobile;
    
    /** 
     * @ORM\Column(type="date", nullable=true)
     */
    private $publication_date;

    /** 
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /** 
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /** 
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $seo_title;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $seo_description;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $seo_keywords;
    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\CMSBundle\Entity\DocumentLink", mappedBy="document", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $links;
    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\CMSBundle\Entity\DocumentFile", mappedBy="document", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $files;
    
    /**
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="VisiDarbi\CMSBundle\Entity\Page", inversedBy="documents") 
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */	
    protected $page;
    
    //@Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\RelativeSlugHandler", options={
    //          @Gedmo\SlugHandlerOption(name="relationField", value="page"),
    //          @Gedmo\SlugHandlerOption(name="relationSlugField", value="slug"),
    //          @Gedmo\SlugHandlerOption(name="separator", value="/")
    //      })
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Gedmo\Slug(fields={"slug_source"}, updatable=true, unique=true, unique_base="country")
     */
    private $slug;
    
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $special;
    
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug_source;
    
    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    private $position;


    public function __toString() 
    {
        return $this->title == null ? 'New document' : $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Document
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set summary
     *
     * @param string $summary
     * @return Document
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    
        return $this;
    }

    /**
     * Get summary
     *
     * @return string 
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Document
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Document
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Document
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Document
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return Document
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Document
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set publication_date
     *
     * @param \DateTime $publicationDate
     * @return Document
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publication_date = $publicationDate;
    
        return $this;
    }

    /**
     * Get publication_date
     *
     * @return \DateTime 
     */
    public function getPublicationDate()
    {
        return $this->publication_date;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Document
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seo_title = $seoTitle;
    
        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Document
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seo_description = $seoDescription;
    
        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Document
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seo_keywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string 
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Document
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    
        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set document
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $document
     * @return Document
     */
    public function setDocument(\VisiDarbi\CMSBundle\Entity\Page $document = null)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return \VisiDarbi\CMSBundle\Entity\Page 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Document
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set page
     *
     * @param \VisiDarbi\CMSBundle\Entity\Page $page
     * @return Document
     */
    public function setPage(\VisiDarbi\CMSBundle\Entity\Page $page = null)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page
     *
     * @return \VisiDarbi\CMSBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Add links
     *
     * @param \VisiDarbi\CMSBundle\Entity\DocumentLink $links
     * @return Document
     */
    public function addLink(\VisiDarbi\CMSBundle\Entity\DocumentLink $links)
    {
        $this->links[] = $links;
        
        $links->setDocument($this);
    
        return $this;
    }

    /**
     * Remove links
     *
     * @param \VisiDarbi\CMSBundle\Entity\DocumentLink $links
     */
    public function removeLink(\VisiDarbi\CMSBundle\Entity\DocumentLink $links)
    {
        $this->links->removeElement($links);
    }

    /**
     * Get links
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Add files
     *
     * @param \VisiDarbi\CMSBundle\Entity\DocumentFile $files
     * @return Document
     */
    public function addFile(\VisiDarbi\CMSBundle\Entity\DocumentFile $files)
    {
        $this->files[] = $files;
        
        $files->setDocument($this);
    
        return $this;
    }

    /**
     * Remove files
     *
     * @param \VisiDarbi\CMSBundle\Entity\DocumentFile $files
     */
    public function removeFile(\VisiDarbi\CMSBundle\Entity\DocumentFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set special
     *
     * @param string $special
     * @return Document
     */
    public function setSpecial($special)
    {
        $this->special = $special;
    
        return $this;
    }

    /**
     * Get special
     *
     * @return string 
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * Set slug_source
     *
     * @param string $slugSource
     * @return Document
     */
    public function setSlugSource($slugSource)
    {
        $this->slug_source = $slugSource;
    
        return $this;
    }

    /**
     * Get slug_source
     *
     * @return string 
     */
    public function getSlugSource()
    {
        return $this->slug_source;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Document
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    
    public function getSeoData()
    {
        return array(
            'title' => $this->seo_title ?: $this->title,
            'description' => $this->seo_description ?: strip_tags($this->content),
            'keywords' => $this->seo_keywords ?: '',
        );
    }

    /**
     * Set image_alt
     *
     * @param string $imageAlt
     * @return Document
     */
    public function setImageAlt($imageAlt)
    {
        $this->image_alt = $imageAlt;
    
        return $this;
    }

    /**
     * Get image_alt
     *
     * @return string 
     */
    public function getImageAlt()
    {
        return $this->image_alt;
    }

    /**
     * @return mixed
     */
    public function getIsMobile()
    {
        return $this->is_mobile;
    }

    /**
     * @param mixed $is_mobile
     */
    public function setIsMobile($is_mobile)
    {
        $this->is_mobile = $is_mobile;
    }


}