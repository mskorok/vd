<?php

namespace VisiDarbi\CMSBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

class PageAdminController extends Controller
{
    /**
     * @Route("/get-pages/{locale}", name="visidarbi_admin_get_pages", options={"expose"=true})
     */
    public function getPagesAction($locale)
    {    
        $countryManager = $this->get('visidarbi.country_manager');
        /* @var $countryManager  \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface */
        
        $currentCountry = $countryManager->getCurrentAdminCountry();
        if (! in_array($locale, $currentCountry->getLocales())) {
            throw new NotFoundHttpException('Invalid locale');
        }
        
        $qb = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository('VisiDarbiCMSBundle:Page')
                    ->createQueryBuilder('p')
        ;
        
        $qb
            ->select('p')
            ->where('p.country = :country')
            ->andWhere('p.locale = :locale')
            ->setParameter(':country', $currentCountry)
            ->setParameter(':locale', $locale)
            ->orderBy('p.root, p.lft', 'ASC');
        
        $pages = array();
        foreach ($qb->getQuery()->getResult() as $page) {
            $pages[] = array('id' => $page->getId(), 'title' => (string)$page );
        }
        
        return new Response(json_encode($pages), 200);
    }
}