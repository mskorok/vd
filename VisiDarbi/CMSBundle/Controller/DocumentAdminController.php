<?php

namespace VisiDarbi\CMSBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DocumentAdminController extends Controller
{
    /**
     * @Route("/doc-admin/move-up/{id}", name="_visidarbi_admin_doc_move_up")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function moveDocumentUpAction($id)
    {    
        $em = $this->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('VisiDarbiCMSBundle:Document');
        $document = $repo->find($id);
        if ($document === null) {
            throw new NotFoundHttpException('Invalid document ID');
        }
        
        $maxPos = $repo->getMaxPosition(
                $document->getCountry(),
                $document->getLocale(), 
                $document->getPage() ? $document->getPage()->getId() : null
        );
        
        if ($document->getPosition() >= $maxPos) {
            throw new NotFoundHttpException('Invalid sorting action');
        }
        
        $document->setPosition(
            $document->getPosition() + 1
        );
        
        $em->persist($document);
        $em->flush();

        
        return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
    }
    
    /**
     * @Route("/doc-admin/move-down/{id}", name="visidarbi_admin_doc_move_down")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function moveDocumentDownAction($id)
    { 
        $em = $this->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('VisiDarbiCMSBundle:Document');
        $document = $repo->find($id);
        if ($document === null) {
            throw new NotFoundHttpException('Invalid document ID');
        }
        
        $minPos = $repo->getMinPosition(
                $document->getCountry(),
                $document->getLocale(), 
                $document->getPage() ? $document->getPage()->getId() : null
        );
        
        if ($document->getPosition() <= $minPos) {
            throw new NotFoundHttpException('Invalid sorting action');
        }
        
        $document->setPosition(
            $document->getPosition() - 1
        );
        
        $em->persist($document);
        $em->flush();

        
        return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
    }
}