<?php

namespace VisiDarbi\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

use VisiDarbi\CMSBundle\Entity\Page;
use VisiDarbi\CMSBundle\Entity\Document;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\TranslationBundle\Annotation\Ignore;


class PageController extends Controller
{
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;
    
    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator")
     */
    protected $paginator;
    
    /**
     * @Route("/page/{slug}", name="page", requirements={"slug" = "[a-z0-9\-\/]+"})
     */
    public function doclistAction($slug)
    {
        if ($slug[strlen($slug)-1] == '/') {
            $slug = trim($slug, '/');
            return $this->redirect(
                empty($slug) ? $this->generateUrl('index') : $this->generateUrl('page', array( 'slug' => $slug))
            );
        }
        
        $pageRepo = $this->em->getRepository('VisiDarbiCMSBundle:Page');
        /* @var $pageRepo \VisiDarbi\CMSBundle\Repository\PageRepository */
        
        $page = $pageRepo->getPageBySlug(
            $this->countryManager->getCurrentCountry(), 
            $this->request->getLocale(), 
            $slug
        );
        
        if ($page === null) {
            //throw new NotFoundHttpException('Page not found.');
            return $this->redirectInvalidPageSlug($slug);
        }
        
        $docRepo = $this->em->getRepository('VisiDarbiCMSBundle:Document');
        $qb = $docRepo->createQueryBuilder('d')
              ->select('d, i')
              ->leftJoin('d.image', 'i')
              ->where('d.country = :country')
              ->andWhere('d.locale = :locale')
              ->andWhere('d.enabled = 1')
              ->andWhere('d.is_mobile = 0')
              ->andWhere('d.page = :page')
              ->setParameter(':country', $this->countryManager->getCurrentCountry()->getId())
              ->setParameter(':locale', $this->request->getLocale())
              ->setParameter(':page', $page->getId())
              ->orderBy('d.position', 'DESC')
        ;

        try {
            $pagination = $this->paginator->paginate(
                    $qb, 
                    $this->request->query->get('page', 1), 
                    10
            );
        }
        catch(\Doctrine\ORM\Query\QueryException $ex) {
            //VISIDA-348
            return $this->redirect($this->generateUrl('page', array(
                'slug' => $page->getSlug()
            )));
        }

        
        if ($pagination->getTotalItemCount() === 1) {
            foreach ($pagination as $doc) {
                return $this->redirect(
                    $this->generateUrl('document', array('slug' => $doc->getSlug() ))
                );
            }
        }
        
        return $this->render('VisiDarbiCMSBundle:Page:doclist.html.twig', array(
            'pagination' => $pagination,
            'seo_context' => $page,
            'lng_switch_choices' => $this->getLanguageSwitchChoices($page)
        ));
    }
    
    /**
     * @Route("/document/{slug}", name="document", requirements={"slug" = "[a-z0-9\-\/]+"})
     */
    public function opendocAction($slug)
    {
        $def_template  = 'VisiDarbiCMSBundle:Page:opendoc.html.twig';
        $fail_template = 'VisiDarbiCMSBundle:Page:tokenfail.html.twig';

        if ($slug[strlen($slug)-1] == '/') {
            $slug = trim($slug, '/');
            return $this->redirect(
                empty($slug) ? $this->generateUrl('index') : $this->generateUrl('document', array( 'slug' => $slug))
            );
        }
        
        $docRepo = $this->em->getRepository('VisiDarbiCMSBundle:Document');
        $doc = $docRepo->findOneBy(array(
            'country' => $this->countryManager->getCurrentCountry(),
            'locale' => $this->request->getLocale(),
            'slug' => $slug,
            'enabled' => true,
            'is_mobile' => false,
        ));
        
        if ($doc === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $this->render( ( $doc->getSpecial() === Document::SPECIAL_TOKEN_FAIL ) ? $fail_template : $def_template , array(
            'document' => $doc,
            'seo_context' => $doc,
            'lng_switch_choices' => $this->getLanguageSwitchChoices($doc->getPage())
        ));
    }
    
    protected function getLanguageSwitchChoices(Page $page = null) 
    {
        
        if ($page === null) {
            return $this->getDefaultLanguageSwitchChoices();
        }
        
        $cc = $this->countryManager->getCurrentCountry();
        $locales = $cc->getLocales();
        $defaultLocale = $cc->getDefaultLocale();
        
        $pageRepo = $this->em->getRepository('VisiDarbiCMSBundle:Page');
        /* @var $pageRepo \VisiDarbi\CMSBundle\Repository\PageRepository */
        
        $docRepo = $this->em->getRepository('VisiDarbiCMSBundle:Document');
        /* @var $pageRepo \VisiDarbi\CMSBundle\Repository\DocumentRepository */
        
        $mirrorTarget = null;
        if ($page->getLocale() == $defaultLocale) {
            $mirrorTarget = $page;
        }
        elseif($page->getMirrorPage() != null) {
           $mirrorTarget = $page->getMirrorPage();
        }
        
        $choices = array();
        foreach ($locales as $locale) {
            $url = $this->generateUrl('index', array('_locale' => $locale), true);
            if ($page->getLocale() == $locale) {
                $url = $this->generateUrl('page', array('_locale' => $locale, 'slug' => $page->getSlug()), true);
            }
            elseif ($mirrorTarget !== null) {
                if ($mirrorTarget->getLocale() == $locale && $mirrorTarget->getEnabled() && $docRepo->getDocumentCount($mirrorTarget) > 0) {
                    $url = $this->generateUrl('page', array('_locale' => $locale, 'slug' => $mirrorTarget->getSlug()), true);
                }
                elseif (($mirrorPage = $pageRepo->getPageByMirrorTarget($cc, $locale, $mirrorTarget)) !== null) {
                    $url = $this->generateUrl('page', array('_locale' => $locale, 'slug' => $mirrorPage->getSlug()), true);
                }
            }

            $choices[] = array(
                'label' => /** @Ignore */$locale,
                'url' => $url,
            );
        }
        
        
        return $choices;
    }
    
    protected function getDefaultLanguageSwitchChoices() 
    {
        $choices = array();
        foreach ($this->countryManager->getCurrentCountry()->getLocales() as $locale) {
            $choices[] = array(
                'label' => /** @Ignore */$locale,
                'url' => $this->generateUrl('index', array('_locale' => $locale), true),
            );
        }
        
        return $choices;
    }
    
    protected function redirectInvalidPageSlug($slug) 
    {
        $slug = trim($slug, '/');
        $segments = explode('/', $slug);
        
        if (count($segments) == 0 || count($segments) == 1) {
            return $this->redirect($this->generateUrl('index'));
        }
        unset($segments[count($segments) - 1]);
        
        $slug = implode('/', $segments);
        
        $pageRepo = $this->em->getRepository('VisiDarbiCMSBundle:Page');
        /* @var $pageRepo \VisiDarbi\CMSBundle\Repository\PageRepository */
        
        $page = $pageRepo->getPageBySlug(
            $this->countryManager->getCurrentCountry(), 
            $this->request->getLocale(), 
            $slug
        );
        
        if ($page !== null) {
            return $this->redirect($this->generateUrl('page', array(
                'slug' => $page->getSlug()
            )));
        }
        
        return $this->redirectInvalidPageSlug($slug);
    }
    
    
}