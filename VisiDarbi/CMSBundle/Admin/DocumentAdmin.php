<?php

namespace VisiDarbi\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

use VisiDarbi\CMSBundle\Entity\Document;
use Doctrine\ORM\EntityManager;


class DocumentAdmin extends Admin
{
    /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;
    
    
    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }
    
    
    /**
     * @var \Doctrine\ORM\EntityManager 
     */
    protected $entityManager;
    
    public function setEntityManager(EntityManager $em)
    {
        $this->entityManager = $em;
    }
    
    
    
    protected function getLocales() 
    {
        $locales = array();
        $country = $this->countryManager->getCurrentAdminCountry();
        foreach ($country->getLanguages() as $lng) {
            $locales[$lng->getLocale()] = $lng->getName();
        }
        
        return $locales;
    }
    
    protected $pageChoices = null;


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {   
        $filterParams = $this->getFilterParameters();
        $locale = $filterParams['locale']['value'];
        $pageId = empty($filterParams['page']['value']) ? null : (int)$filterParams['page']['value'];
  
        
        $repo = $this->entityManager->getRepository('VisiDarbiCMSBundle:Document');
        //$minPos = $repo->getMinPosition($this->countryManager->getCurrentAdminCountry(), $locale, $pageId);
        //$maxPos = $repo->getMaxPosition($this->countryManager->getCurrentAdminCountry(), $locale, $pageId);
        
        $maxPositions = $repo->getAllMaxPositions($this->countryManager->getCurrentAdminCountry(), $locale, $pageId);
        
        $listMapper
            ->addIdentifier('id', null, array(
                'sortable' => false
            ))
            ->add('locale', 'string', array(
                'label' => 'Language',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->getLocales(),
                'sortable' => false
            ))
            ->add('page', null, array(
                'sortable' => false
            ))
            ->add('title', null, array(
                'sortable' => false
            ))
            ->add('publication_date', null, array(
                'label' => 'Date',
                'sortable' => false,
            ))
            ->add('enabled', null, array(
                'editable' => true,
                'sortable' => false
            ))
            ->add('is_mobile', null, array(
                'editable' => true,
                'sortable' => false
            ))
            ->add('position', 'string', array(
                'sortable' => false,
                'label' => 'Order',
                'template' => 'VisiDarbiCMSBundle:Admin:list_action_order.html.twig',
                'min_position' => 0,
                'max_positions' => $maxPositions,
                
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterParams = $this->getFilterParameters();
        $locale = $filterParams['locale']['value'];
        $country = $this->countryManager->getCurrentAdminCountry();
        
        $filterMapper
            ->add('locale', 'doctrine_orm_callback', array(
                'label' => 'Language',
                'operator_type' => 'hidden',
                'field_options' => array(
                    'attr' => array('class' => 'doclist-lng-selector' ),
                    'empty_value' => false,
                    'required' => true,
                    'choices' => $this->getLocales()
                ),
                'field_type' => 'choice',
                'callback' => function($queryBuilder, $alias, $field, $value) {
                    if (empty($value['value'])) {
                        return;
                    }
                    
                    $queryBuilder->andWhere(sprintf('%s.locale = :locale', $alias));
                    $queryBuilder->setParameter('locale', $value['value']);

                    return true;
                }
                
            ))
            ->add('page', null, array(
                'operator_type' => 'hidden',
                'field_options' => array(
                    'attr' => array('class' => 'doclist-page-selector' ),
                    //'empty_value' => '',
                    'class' => 'VisiDarbiCMSBundle:Page',
                    'query_builder' => function($er) use ($country, $locale) {
                        $qb = $er->createQueryBuilder('p')
                            ->where('p.country = :country')
                            ->setParameter('country', $country->getId())
                            ->andWhere('p.locale = :locale')
                            ->setParameter('locale', $locale)
                            ->orderBy('p.root, p.lft', 'ASC')
                        ;
                        return $qb;
                    },
                )
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();
        $id = $subject->getId();
        
        if ($id) {
            $segments = explode('/', $subject->getSlug());
            $subject->setSlugSource(
                end($segments)
            );
        }
        
        $formMapper
            ->with('Document')
                ->add('enabled', null, array( 'required' => false ))
                ->add('locale', 'choice', array(
                        'choices' => $this->getLocales(),
                        'required' => true,
                        'label' => 'Language',
                        'attr' => array('class' => 'locale-selector span5')
                ))
                ->add('page', null, array(
                    'label' => 'Page', 
                    'required' => true, 
                    'query_builder' => function($er) {
                          $qb = $er->createQueryBuilder('p');
                          $qb->orderBy('p.root, p.lft', 'ASC');
                          return $qb;
                    },
                    'attr' => array('class' => 'page-selector span5')
                ))
                ->add('title')
                ->add('publication_date', 'date', array(
                    //'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'label' => 'Date' 
                ))
                ->add('summary')
                ->add('image', 'sonata_type_model_list', 
                    array( 'required' => false ), 
                    array(
                        'link_parameters' => array('context' => 'document', 'provider' => 'sonata.media.provider.image')
                    )
                )
                ->add('image_alt')
                ->add('special', 'choice', array(
                        'choices' => array( 
                            Document::SPECIAL_ADV_SOURCES => 'Advertisement sources',
                            Document::SPECIAL_TOKEN_FAIL => 'Token fail page',
                        ),
                        'required' => false,
                        'label' => 'Special document',
                ))
                ->add('slugSource', 'text', array(
                    'label' => 'URL',
                    'required' => false,
                    'help' => $id == null ? 'If left blank URL will be autogenerated' : sprintf('Current URL: %s', $this->routeGenerator->generate('document', array('slug' => $subject->getSlug()), true))
                ))
                ->add('content', 'ckeditor', array(
                    'required' => false, 
                    'attr' => array('class' => 'ckeditor span5'),
                    'config_name' => 'admin',
                ))
            ->end()
            ->with('Attached files and links', array('collapsed' => true))
                ->add('links', 'sonata_type_collection', array( 'required' => false, 'by_reference' => false ), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
                ->add('files', 'sonata_type_collection', array( 'required' => false, 'by_reference' => false ), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->end()
            ->with('SEO', array('collapsed' => true))
                ->add('seo_title', null, array( 'required' => false, 'label' => 'Title' ))
                ->add('seo_keywords', null, array( 'required' => false, 'label' => 'Keywords' ))
                ->add('seo_description', 'textarea', array( 'required' => false, 'label' => 'Description' ))
            ->end()
        ;
    }
    
    
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement

        ;
    }
    
    public function prePersist($document) 
    {
        $document->setCountry(
            $this->countryManager->getCurrentAdminCountry()
        );
        
        if (! trim(strlen($document->getSlugSource()))) {
            $document->setSlugSource(
                $document->getTitle()
            );
        }
    }

    public function preUpdate($document)
    {
        if (! trim(strlen($document->getSlugSource()))) {
            $document->setSlugSource(
                $document->getTitle()
            );
        }
    }    
    
    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'VisiDarbiCMSBundle:Admin:cms_edit.html.twig';
        }
        if ($name == 'list') {
            return 'VisiDarbiCMSBundle:Admin:document_list.html.twig';
        }
        return parent::getTemplate($name);
    }
    
    public function createQuery($context = 'list')
    {
        $query = $this->getModelManager()->createQuery($this->getClass(), 'd');
        $query->select('d, p');
        $query->leftJoin('d.page', 'p');
        $query->where('d.country = :country');
        $query->setParameter(':country', $this->countryManager->getCurrentAdminCountry());
        $query->orderBy('d.page', 'DESC');
        $query->addOrderBy('d.position', 'DESC');
        
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }
    
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
                'locale' => array(
                    'value' => $this->countryManager->getCurrentAdminCountry()->getDefaultLocale(),
                )
            ),
            $this->datagridValues

        );
        return parent::getFilterParameters();
    }
    
}