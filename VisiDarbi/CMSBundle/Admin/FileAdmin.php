<?php

namespace VisiDarbi\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

use VisiDarbi\CMSBundle\Entity\DocumentFile;


class FileAdmin extends Admin
{
    /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;
    
    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {   
        $listMapper
            ->addIdentifier('id')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('file', 'sonata_type_model_list', 
                array( 'required' => true ), 
                array(
                    'link_parameters' => array('context' => 'document', 'provider' => 'sonata.media.provider.file')
                )
            )
        ;
    }
    
    
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
        ;
    }
}