<?php

namespace VisiDarbi\CMSBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use VisiDarbi\CMSBundle\Entity\Document;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use Doctrine\ORM\EntityManager;
use VisiDarbi\CMSBundle\Entity\Page;
use Knp\Menu\MenuItem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\SecurityContext;

class MenuBuilder
{
    protected $factory;
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     */
    protected $countryManager;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    /**
     * @var \Symfony\Component\HttpFoundation\Request;
     */
    protected $request;
    
    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    protected $router;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;
    
    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     */
    protected $securityContext;
    
    private $documentPageUri = null;
    
    
    private $currentPage = null;
    
    private $currentDocument = null;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, CountryManagerInterface $countryManager, 
            EntityManager $em, Request $request, UrlGeneratorInterface $router, 
            TranslatorInterface $translator, SecurityContext $securityContext)
    {
        $this->factory = $factory;
        $this->countryManager = $countryManager;
        $this->em = $em;
        $this->request = $request;
        $this->router = $router;
        $this->translator = $translator;
        $this->securityContext = $securityContext;
    }

    public function creatFooterMenu()
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'simple');
        
        $footerMenuPages = $this->getPageQb(Page::MENU_FOOTER)->getQuery()->getResult();
        
        foreach ($footerMenuPages as $page) {
            $menu->addChild($page->getTitle(), array(
                'route' => 'page',
                'routeParameters' => array('slug' => $page->getSlug()),
            ));
        }
        
        $menu->addChild($this->translator->trans('Contacts', array(), 'footer_menu'), array('route' => 'contacts'));

        return $menu;
    }
    
    public function creatLeftSideMenu()
    {
        $requestUri = $this->router->generate(
                $this->request->attributes->get('_route'), 
                $this->request->attributes->get('_route_params')
        );
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'main');
        
        $qb = $this->getPageQb(Page::MENU_LEFT_SIDE);
        
        $currentRootPage = $this->getCurrentPage()->getRootPage();
        
        if ($currentRootPage->getMenu() === Page::MENU_FOOTER) {
            //children pages only
            $qb = $this->em->getRepository('VisiDarbiCMSBundle:Page')->getChildrenQueryBuilder($currentRootPage)
              ->leftJoin('node.documents', 'd')
              ->andWhere('node.country = :country')
              ->andWhere('node.locale = :locale')
              ->andWhere('node.menu = :menu')
              ->andWhere('node.enabled = 1') 
              ->andWhere('d is NOT NULL')
              ->setParameter(':country', $this->countryManager->getCurrentCountry()->getId())
              ->setParameter(':locale', $this->request->getLocale())
              ->setParameter(':menu', Page::MENU_LEFT_SIDE)  
              ->orderBy('node.root, node.lft', 'ASC')
            ;
        }
        
        $tree = $this->em->getRepository('VisiDarbiCMSBundle:Page')->buildTree(
            $qb->getQuery()->getArrayResult()
        );
        
        foreach ($tree as $page) {
            $child = $menu->addChild($page['title'], array(
                'route' => 'page',
                'routeParameters' => array('slug' => $page['slug']),
                'displayChildren' => false
            ));
            
            $this->checkIfCurrent($child, $requestUri);
            
            foreach ($page['__children'] as $subpage) {
                $subChild = $child->addChild($subpage['title'], array(
                    'route' => 'page',
                    'routeParameters' => array('slug' => $subpage['slug'])
                ));
                $this->checkIfCurrent($subChild, $requestUri);
            }
        }
        
        foreach ($menu as $item) {
            if (($item->isCurrent() || $item->isCurrentAncestor()) && $item->count() > 0) {
                $item->setAttribute('class', 'active havesub');
                $item->setChildrenAttribute('class', 'sub');
                $item->setDisplayChildren(true);
                
                foreach($item->getChildren() as $subitem) {
                    if ($subitem->isCurrent()) {
                        $subitem->setAttribute('class', 'active');
                    }
                }
            }
            elseif ($item->isCurrent()) {
                $item->setAttribute('class', 'active');
            }
        }

        return $menu;
    }
    
    /**
     * 
     * @return Page
     */
    protected function getCurrentPage()
    {
        if ($this->request->attributes->get('_route') == 'document') {
            return $this->getCurrentDocument()->getPage();
        }
        
        if ($this->request->attributes->get('_route') != 'page') {
            return null;
        }
        
        if ($this->currentPage !== null) {
            return $this->currentPage;
        }
        
        $slug = $this->request->attributes->get('slug');
        $this->currentPage = $this->em->getRepository('VisiDarbiCMSBundle:Page')->getPageBySlug(
                $this->countryManager->getCurrentCountry(),
                $this->request->getLocale(), 
                $slug
        );
        
        return $this->currentPage;
    }
    
    /**
     * @return Document
     */
    protected function getCurrentDocument()
    {
        if ($this->request->attributes->get('_route') != 'document') {
            return null;
        }
        
        if ($this->currentDocument !== null) {
            return $this->currentDocument;
        }
        
        $slug = $this->request->attributes->get('slug');
        $docRepo = $this->em->getRepository('VisiDarbiCMSBundle:Document');
        $this->currentDocument = $docRepo->findOneBy(array(
            'country' => $this->countryManager->getCurrentCountry(),
            'locale' => $this->request->getLocale(),
            'slug' => $slug,
            'enabled' => true,
            'is_mobile' => false,
        ));
        
        return $this->currentDocument;
    }


    protected function getPageQb($menu) 
    {
        return $this->em->getRepository('VisiDarbiCMSBundle:Page')
              ->createQueryBuilder('p')
              ->select('p')
              ->leftJoin('p.documents', 'd')
              ->where('p.country = :country')
              ->andWhere('p.locale = :locale')
              ->andWhere('p.enabled = 1') 
              ->andWhere('p.menu = :menu')
              ->andWhere('d.enabled = 1') 
              ->andWhere('d is NOT NULL')
              ->setParameter(':country', $this->countryManager->getCurrentCountry()->getId())
              ->setParameter(':locale', $this->request->getLocale())
              ->setParameter(':menu', $menu)  
              ->orderBy('p.root, p.lft', 'ASC')
        ;
    }
    
    protected function checkIfCurrent(MenuItem $item, $requestUri) 
    {
        $route = $this->request->attributes->get('_route');
        if ($route === 'document') {
            if ($this->getDocumentPageUri() == $item->getUri()) {
                $item->setCurrent(true);
            }
        }
        if ($route === 'page') {
            if ($requestUri == $item->getUri()) {
                $item->setCurrent(true);
            }
        }
    }

    
    protected function getDocumentPageUri() 
    {
        if ($this->documentPageUri !== null) {
            return $this->documentPageUri;
        }
        
        $doc = $this->getCurrentDocument();
        
        if ($doc === null) {
            return;
        }
        
        $this->documentPageUri = $this->router->generate('page', array('slug' => $doc->getPage()->getSlug()));
        
        return $this->documentPageUri; 
    }
    

    public function createUserOfficeMenu()
    {
        if ($this->securityContext->isGranted('ROLE_USER')) {
            
            $user = $this->securityContext->getToken()->getUser();
            $requestUri = $this->router->generate(
                    $this->request->attributes->get('_route'), 
                    $this->request->attributes->get('_route_params')
            );  
            
                        
            $menu = $this->factory->createItem('root');
            $menu->setCurrentUri($requestUri);

            $menu->addChild($this->translator->trans('Profile', array(), 'user_office_menu'), array(
                'route' => 'edit_profile',
                'routeParameters' => array(),
            ));
            //$menu->addChild('Mani sludinājumi', array(
            //    //'route' => 'edit_profile',
            //    //'routeParameters' => array(),
            //    'uri' => 'http://www.google.lv',
            //));

            if ($user->isLegal()) {
                $menu->addChild($this->translator->trans('My advertisements', array(), 'user_office_menu'), array(
                    'route' => 'my_advertisments',
                    'routeParameters' => array(),
                ));
            }

            if ($user->isPrivate()) {
                $menu->addChild($this->translator->trans('Saved advertisements', array(), 'user_office_menu'), array(
                    'route' => 'saved_advertisments',
                    'routeParameters' => array(),
                ));
                
                $menu->addChild($this->translator->trans('Advertisment search history', array(), 'user_office_menu'), array(
                    'route' => 'advertisment_search_history',
                    'routeParameters' => array(),
                ));

                $menu->addChild($this->translator->trans('E-mail subscription', array(), 'user_office_menu'), array(
                    'route' => 'list_email_subscription',
                    'routeParameters' => array(),
                ));
            }



            return $menu;
        }
    }
    
}
