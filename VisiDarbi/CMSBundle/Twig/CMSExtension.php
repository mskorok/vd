<?php

namespace VisiDarbi\CMSBundle\Twig;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use Twig_Extension;
use Twig_Function_Method;
use Doctrine\ORM\EntityManager;
use VisiDarbi\CMSBundle\Entity\Document;

class CMSExtension extends Twig_Extension
{
    /**
     * @var CountryManagerInterface
     */
    private $countryManager;
    
    /**
     * @var EntityManager 
     */
    protected $em;
    


    public function __construct(CountryManagerInterface $sm, EntityManager $em)
    {
        $this->countryManager = $sm;
        $this->em = $em;
    }

    public function getFunctions()
    {
        return array(
            'special_document' => new Twig_Function_Method($this, 'getSpecialDocument'),
        );
    }

    public function getSpecialDocument($locale, $special)
    {
        return $this->em->getRepository('VisiDarbiCMSBundle:Document')->getSpecialDocuments(
                $this->countryManager->getCurrentCountry(),
                $locale,
                $special,
                true 
        );
    }

    public function getName()
    {
        return 'visidarbi_cms';
    }
}
