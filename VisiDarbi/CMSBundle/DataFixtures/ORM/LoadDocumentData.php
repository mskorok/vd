<?php

namespace VisiDarbi\CMSBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\CMSBundle\Entity\Document;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadDocumentData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        return;
        
        $countries = array(
            $this->getReference('country-lat'),
            //$this->getReference('country-lit'),
            //$this->getReference('country-est')
        );
        
        
        foreach ($countries as $country) {
            $locales = $country->getLocales();
            foreach (array_reverse($locales) as $locale) {
                for ($i = 0; $i < 160; $i++) {
                    
                    $document = new Document();
                    $document->setPage($this->getReference('page-news-lat-' . $locale));
                    $document->setCountry($country);
                    $document->setLocale($locale);
                    $document->setTitle(
                        sprintf('Lorem ipsum dolor sit amet, consectetur adipiscing elit %s %.', $locale, ($i + 1))
                    );
                    $document->setSummary(
                        sprintf(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sodales convallis cursus. Quisque nibh ligula, tincidunt a pharetra nec, scelerisque id magna. Ut bibendum, nisi a hendrerit suscipit, lorem nisi sodales odio, a mollis nisl eros eu justo. Morbi aliquam, ligula vulputate auctor eleifend, orci odio ornare neque, et placerat massa lorem lobortis ante. Donec eu nibh eu libero rhoncus venenatis vitae eu neque %s %s.', 
                            $locale, ($i + 1)
                        )    
                    );
                    
                    $document->setContent(implode('', array(
                        '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sodales convallis cursus. Quisque nibh ligula, tincidunt a pharetra nec, scelerisque id magna. Ut bibendum, nisi a hendrerit suscipit, lorem nisi sodales odio, a mollis nisl eros eu justo. Morbi aliquam, ligula vulputate auctor eleifend, orci odio ornare neque, et placerat massa lorem lobortis ante. Donec eu nibh eu libero rhoncus venenatis vitae eu neque. Maecenas ut porttitor justo. Vestibulum blandit mi vel neque fermentum commodo. Quisque id magna eu dolor luctus consectetur in nec leo. Mauris lacinia consequat est, ut rutrum elit rutrum ac. Nam augue ante, vulputate posuere porttitor ut, euismod ut felis. Quisque varius commodo est et pulvinar. Nullam congue, tortor ac condimentum imperdiet, neque ante sollicitudin magna, eget bibendum nunc libero at urna. Praesent eu lacus id neque consectetur hendrerit. Aliquam aliquet posuere ligula quis pretium. Suspendisse sodales, enim quis faucibus feugiat, massa mi tristique justo, quis aliquam augue sapien bibendum elit. Nulla facilisis vulputate lorem vitae egestas.</p>',
                        '<p>Donec feugiat tellus eu lorem tempor mollis. Nullam porttitor massa vel leo porttitor ac vulputate mi eleifend. Etiam aliquam, felis id hendrerit auctor, lacus quam rutrum dui, sed euismod sem neque quis tellus. Mauris ac sem non turpis ultricies vulputate. Nam ut auctor libero. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sapien justo, ullamcorper lacinia luctus non, sollicitudin nec augue. Sed vitae gravida lorem. Cras augue tortor, condimentum ut euismod et, semper non nisi. Integer sagittis luctus neque, nec lacinia leo accumsan sed. Nulla vulputate lacus semper erat commodo sed aliquet tortor rutrum. In tempor libero gravida neque facilisis vel vestibulum metus tristique. In lobortis egestas nulla, ut dignissim lorem commodo eget. Nulla facilisi. Nunc a nibh et eros ullamcorper rutrum dignissim a augue.</p>',
                        '<p>Donec blandit placerat erat, eget tempus diam ultrices porttitor. Fusce erat elit, faucibus sit amet viverra ut, varius non libero. Quisque sed lectus erat, sit amet elementum massa. Maecenas volutpat, ligula sed condimentum ornare, turpis nunc congue ipsum, id lacinia turpis tortor non lectus. Donec nec tempor purus. Morbi eget gravida orci. Aliquam euismod arcu et tortor mollis luctus.</p>',
                    )));
                    
                    $document->setEnabled(true);
                    $document->setPublicationDate(new \DateTime('now'));
                    
                    $document->setSlugSource($document->getTitle());
                    
                    $manager->persist($document);
                }
            }
        }

        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 16;
    }  
    
}
