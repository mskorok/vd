<?php

namespace VisiDarbi\CMSBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\CMSBundle\Entity\Page;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadPageData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        
        $countries = array(
            $this->getReference('country-lat'),
            //$this->getReference('country-lit'),
            //$this->getReference('country-est')
        );
        
        $pages = array(
            array(
                'title' => 'Jaunumi', 
                'reference' => 'page-news',
            ),
            array(
                'title' => 'Par mums',   
                'reference' => 'page-about',
            ),
            array(
                'title' => 'Cita informācija',
                'reference' => 'page-other',
            ),
            array(
                'title' => 'Kontakti', 
                'reference' => 'page-contact',
            ),
        );
        
        foreach ($countries as $country) {
            
            $mirrorTargets = array();
            
            $locales = $country->getLocales();
            foreach ($locales as $locale) {
                foreach ($pages as $page) {
                    $pageObj = new Page();
                    $pageObj->setCountry($country);
                    $pageObj->setLocale($locale);
                    $pageObj->setTitle($page['title'] . ' ('. $locale  .')');
                    $pageObj->setMenu(Page::MENU_FOOTER);
                    $pageObj->setEnabled(true);
                    $pageObj->setSlugSource($pageObj->getTitle());
                    
                    if ($locale == 'lv') {
                        $mirrorTargets[$page['reference']] = $pageObj;
                    }
                    
                    if (isset($mirrorTargets[$page['reference']])) {
                        $pageObj->setMirrorPage($mirrorTargets[$page['reference']]);
                    }

                    $manager->persist($pageObj);
                    
                    $this->addReference($page['reference'] . '-lat-' . $locale, $pageObj);
                    
                    if ($page['reference'] == 'page-news') {
                        
                        for ($i = 0; $i < 7; $i++) {
                            $child = new Page();
                            $child->setCountry($country);
                            $child->setLocale($locale);
                            $child->setTitle('Sub-category' . ' ('. $locale  .') No.' . ($i + 1));
                            $child->setSlugSource($child->getTitle());
                            $child->setMenu(Page::MENU_LEFT_SIDE);
                            $child->setParent($pageObj);
                            $child->setEnabled(true);
                            $manager->persist($child);
                            
                            if ($i < 2) {
                                for ($j = 0; $j < 4; $j++) {
                                    $childChild = new Page();
                                    $childChild->setCountry($country);
                                    $childChild->setLocale($locale);
                                    $childChild->setTitle('Sub-Sub-category' . ' ('. $locale  .') No.' . ($j + 1));
                                    $childChild->setSlugSource($childChild->getTitle());
                                    $childChild->setMenu(Page::MENU_LEFT_SIDE);
                                    $childChild->setEnabled(true);
                                    $childChild->setParent($child);
                                   
                                    $manager->persist($childChild);
                                }
                            }
                        }
                        
                    }
                }
            }
        }

        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 15;
    }    
    
}
