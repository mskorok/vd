<?php

namespace VisiDarbi\CVonlineExportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction;

class DefaultController extends Controller {

    private $em;
    private $configParams;

    public function preExecute() {
        $this->em = $this->getDoctrine()->getEntityManager();
        //$this->em->getFilters()->disable('soft-deleteable');
        $this->configParams = $this->container->getParameter('crm_export');
    }

    public function indexAction(Request $request) {
        $this->checkAccess($request);

        //Sanitize param
        $date = new \DateTime($request->get('date', null));
        $dateObject1 = clone $date;
        $dateObject2 = clone $date;
        $date = $date->format('Y-m-d');

        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        $accaunts = $this->getAccountsByDate($date);
        $noAccounts = $this->getNoAccountsByDate($dateObject1);
        $payments = $this->getPaymentsByDate($dateObject2);

        $itemsToExport = $this->prepareData($accaunts);

        return $this->render('VisiDarbiCVonlineExportBundle:Default:index.html.twig',
                array('items' => $itemsToExport, 'noAccounts' => $noAccounts, 'payments' => $payments),
                $response);
    }

    public function archiveAction(Request $request) {
        $this->checkAccess($request);

        //Sanitize param
        $date = new \DateTime($request->get('date', null));
        $date = $date->format('Y-m-d');

        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        $accaunts = $this->getAccountsBeforeDate($date);
        $itemsToExport = $this->prepareData($accaunts);

        return $this->render('VisiDarbiCVonlineExportBundle:Default:index.html.twig', array('items' => $itemsToExport), $response);
    }

    /**
     * Prepare import data
     * @param array $accaunts
     * @return array
     */
    private function prepareData($accaunts) {
        $itemsToExport = array();

        //Preapre data
        foreach ($accaunts as $account) {

            $item = array();
            $item['id'] = $account['id'];
            $item['name'] = $account['company_name'];
            $item['regnumber'] = $account['registration_number'];
            $item['vatnumber'] = $account['vat_number'];
            $item['address_text'] = $account['legal_address'];
            $item['email'] = $account['email'];
            $item['phone'] = $account['phone'];


            $itemContact = array();

            $names = $this->splitName($account['full_name']);
            $itemContact['id'] = $account['user_id'];
            $itemContact['firstname'] = $names['firstname'];
            $itemContact['lastname'] = $names['lastname'];
            $itemContact['email'] = $account['email'];
            $itemContact['phone'] = $account['phone'];

            $item['contacts'][] = $itemContact;

            //Get related contac persons from advertisement
            $user = $this->em->createQueryBuilder('c')
                            ->select('u')
                            ->from('VisiDarbi\UserBundle\Entity\User', 'u')
                            ->where('u.id = (:cids)')
                            ->setParameter('cids', $account['user_id'])
                            ->getQuery()
                            ->setMaxResults(1)
                            ->getResult();

            $userEntity  = isset($user[0]) ? $user[0] : false;

            if($userEntity){

                $advertisements = $userEntity->getAdvertisements();

                foreach ($advertisements as $advertisement) {

                    //Contact person
                    $itemContact = array();

                    $person = $advertisement->getContactPerson();

                    $names = $this->splitName($person->getName());
                    $itemContact['id'] = 'CP' . $person->getId();
                    $itemContact['firstname'] = $names['firstname'];
                    $itemContact['lastname'] = $names['lastname'];
                    $itemContact['email'] = $person->getEmail();
                    $itemContact['phone'] = $person->getPhone();
                    $item['contacts'][] = $itemContact;

                    //Requisites person
                    $person = $advertisement->getRequisites();

                    if($person && $account['email'] != $person->getEmail()){
                        $itemContact = array();
                        $names = $this->splitName($person->getContactPersonName());
                        $itemContact['id'] = 'RP' . $person->getId();
                        $itemContact['firstname'] = $names['firstname'];
                        $itemContact['lastname'] = $names['lastname'];
                        $itemContact['email'] = $person->getEmail();
                        $itemContact['phone'] = '';
                        $item['contacts'][] = $itemContact;
                    }

                }
            }

            $itemsToExport[] = $item;
        }

        return $itemsToExport;
    }

    /**
     * Returns account with last update at;
     * @param string $date
     * @return array Description
     */
    private function getAccountsByDate($date) {

        $query = 'SELECT u.*, p.* FROM User u
            JOIN LegalProfile p ON u.id = p.user_id
            WHERE DATE(u.created) = :date1 OR DATE(u.updated) = :date2 OR DATE(p.last_advertisement_at) = :date3';


        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->bindValue('date1', $date);
        $stmt->bindValue('date2', $date);
        $stmt->bindValue('date3', $date);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    /**
     * Returns account with last update at;
     * @param \DateTime $date
     * @return array Description
     */
    private function getNoAccountsByDate($date) {

        $dates = $this->prepareDateRange($date);

        $data = $this->em->createQueryBuilder('a')
                ->select('a', 'r', 'cp')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->join('a.requisites', 'r')
                ->join('a.contactPerson', 'cp')
                ->where('a.user is NULL')
                ->andWhere('a.cretaed_at >= :date1')
                ->andWhere('a.cretaed_at < :date2')
                //additional condition to prevent AD in report withour requisites and not highlighted external AD
                //may be need to test with different cases of adding AD and report creation
                ->andWhere('(a.externalAdvertisement IS NULL) OR (r.registration_number IS NOT NULL)')
                ->setParameter('date1',$dates['0'])
                ->setParameter('date2',$dates['1'])
                ->getQuery()
                ->getResult()
                ;

        return $data;
    }

    private function getPaymentsByDate($date){

        $dates = $this->prepareDateRange($date);

        $data = $this->em->createQueryBuilder('a')
                ->select('pt', 'r', 'aa', 'pp')
                ->from('VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction', 'pt')
                ->leftJoin('pt.advertisement', 'aa')
                ->leftJoin('pt.paidPeriod', 'pp')
                ->join('pt.requisites', 'r')
                ->where('pt.status = :status')
                ->andWhere('pt.created_at >= :date1')
                ->andWhere('pt.created_at < :date2')
                ->setParameter('status', PaymentTransaction::RETURN_STATUS_SUCCESS)
                ->setParameter('date1',$dates[0])
                ->setParameter('date2',$dates[1])
                ->getQuery()
                ->getResult()
                ;

        return $data;

    }



    /**
     * Returns account with last update before date;
     * @param string $date
     * @return array Description
     */
    private function getAccountsBeforeDate($date) {

        $query = 'SELECT u.*, p.* FROM User u
            JOIN LegalProfile p ON u.id = p.user_id
            WHERE DATE(u.created) < :date1 AND DATE(u.updated) < :date2 AND (DATE(p.last_advertisement_at) < :date3 OR p.last_advertisement_at IS NULL)';

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->bindValue('date1', $date);
        $stmt->bindValue('date2', $date);
        $stmt->bindValue('date3', $date);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Check access rights to controller actions
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws NotFoundHttpException
     */
    private function checkAccess(Request $request) {

        if (!$request->isMethod(strtoupper($this->configParams['method']))) {
            throw $this->createNotFoundException();
        }

        $password = $request->get('password', null);
        $login = $request->get('login', null);

        if ($this->configParams['login'] != $login ||
                $this->configParams['password'] != $password) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * Split one string name for first and last name
     * @param string $name
     * @return array
     */
    private function splitName($name) {
        $data = array();
        $name = explode(' ', $name);
        $data['firstname'] = $name[0];
        unset($name[0]);
        $data['lastname'] = implode(' ', $name);

        return $data;
    }


    /**
     * Prepare dates range
     * @param \DateTime|string $date
     * @return array
     */
    private function prepareDateRange($date){

        if( !($date instanceof  \DateTime)) {
            $date = new \DateTime($date);
        }

        $date1 = new \DateTime($date->format('Y-m-d'));

        $date2 = $date->add(new \DateInterval('P1D'));

        return array($date1, $date2);
    }

}
