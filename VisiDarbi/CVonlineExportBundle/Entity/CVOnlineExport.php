<?php
namespace VisiDarbi\CVonlineExportBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 */
class CVOnlineExport
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;

    /** 
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\UserBundle\Entity\LegalProfile", inversedBy="CVOnlineExports")
     * @ORM\JoinColumn(name="legal_profile_id", referencedColumnName="id", nullable=false)
     */
    private $LegalProfile;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return CVOnlineExport
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CVOnlineExport
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set LegalProfile
     *
     * @param \VisiDarbi\UserBundle\Entity\LegalProfile $legalProfile
     * @return CVOnlineExport
     */
    public function setLegalProfile(\VisiDarbi\UserBundle\Entity\LegalProfile $legalProfile)
    {
        $this->LegalProfile = $legalProfile;
    
        return $this;
    }

    /**
     * Get LegalProfile
     *
     * @return \VisiDarbi\UserBundle\Entity\LegalProfile 
     */
    public function getLegalProfile()
    {
        return $this->LegalProfile;
    }
}