<?php

namespace VisiDarbi\LocalePlaceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use VisiDarbi\LocalePlaceBundle\Entity\Language;

class LanguageInCountryAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('language', 'sonata_type_model_list', array(
                'required' => true,
                'label' => 'Language'
            ))
                
            ->add('enabled', null, array('required' => false))
            ->add('position', 'hidden')
        ;
    }
}