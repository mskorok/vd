<?php

namespace VisiDarbi\LocalePlaceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use VisiDarbi\LocalePlaceBundle\Entity\Language;

class SecondaryCurrencyAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('currency', 'sonata_type_model', array(
                'required' => true
            ))
            ->add('rate_to_primary_currency', null, array(
                'required' => true,
                'label' => 'Rate to primary currency'
            ))
        ;
    }
}