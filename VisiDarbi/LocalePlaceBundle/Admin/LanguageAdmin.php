<?php

namespace VisiDarbi\LocalePlaceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use VisiDarbi\LocalePlaceBundle\Entity\Language;

class LanguageAdmin extends Admin
{
    //protected function configureRoutes(RouteCollection $collection)
    //{
    //    $collection
    //        ->remove('create')
    //        ->remove('delete')
    //        ->remove('edit')
    //    ;
    //}
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('locale')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'required' => true
            ))
            ->add('locale', null, array(
                'required' => true
            ))
        ;
    }
}