<?php

namespace VisiDarbi\LocalePlaceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use VisiDarbi\LocalePlaceBundle\Entity\Country;

class CountryAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
            //->remove('edit')
        ;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('domain')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'required' => true
            ))
            ->add('domain', null, array(
                'required' => true
            ))
            ->add('primaryCurrency', 'sonata_type_model', array(
                'required' => true,
                'label' => 'Currency',
            ),  array('allow_add' => false))
            ->add('deafultAdvertisementCountry', 'sonata_type_model', array(
                'required' => false,
                'label' => 'Deafult advertisement country',
            ))
            ->add('secondaryCurrencies', 'sonata_type_collection', array( 'required' => false, 'by_reference' => false, 'label' => 'Secondary currencies' ), array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable'  => 'position'
            ))
            ->add('languagesInCountry', 'sonata_type_collection', array( 'required' => false, 'by_reference' => false, 'label' => 'Languages' ), array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable'  => 'position'
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
                ->add('id')
                ->add('name')
                ->add('domain')
            ->end()
        ;
    }
}