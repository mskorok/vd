<?php

namespace VisiDarbi\LocalePlaceBundle\Twig;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use Twig_Extension;
use Twig_Function_Method;


class CountriesExtension extends Twig_Extension
{
    /**
     * @var CountryManagerInterface
     */
    private $countryManager;


    public function __construct(CountryManagerInterface $sm)
    {
        $this->countryManager = $sm;
    }

    public function getFunctions()
    {
        return array(
            'all_countries' => new Twig_Function_Method($this, 'getAllCountries'),
            'current_country' => new Twig_Function_Method($this, 'getCurrentCountry'),
            'current_admin_country' => new Twig_Function_Method($this, 'getCurrentAdminCountry'),
        );
    }

    public function getAllCountries()
    {
        return $this->countryManager->getAllCountries();
    }

    public function getCurrentCountry()
    {
        return $this->countryManager->getCurrentCountry();
    }
    
    public function getCurrentAdminCountry()
    {
        return $this->countryManager->getCurrentAdminCountry();
    }

    public function getName()
    {
        return 'visidarbi_countries';
    }
}
