<?php

namespace VisiDarbi\LocalePlaceBundle\Manager;

use VisiDarbi\LocalePlaceBundle\Entity\Country;

interface CountryManagerInterface 
{
    /**
     * @return array
     */
    public function getAllCountries();

    /**
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country
     */
    public function getCurrentCountry();
    
    /**
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country
     */
    public function getCurrentAdminCountry();
    
    /**
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     */
    public function setCurrentAdminCountry(Country $country);
}