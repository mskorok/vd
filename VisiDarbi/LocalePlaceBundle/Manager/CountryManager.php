<?php

namespace VisiDarbi\LocalePlaceBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\DependencyInjection\ContainerInterface;


class CountryManager implements CountryManagerInterface 
{
    private $countries = null;
    
    /**
     * @var EntityManager 
     */
    private $em = null;
    
    /**
     * @var SessionInterface 
     */
    private $session = null;
    
    /**
     * @var string 
     */
    private $countryEnvParam = null;

    public function __construct(EntityManager $em, SessionInterface $session, $countryEnvParam)
    {
        $this->em = $em;
        $this->session = $session;
        $this->countryEnvParam = $countryEnvParam;
    }
    
    public function getAllCountries() 
    {
        if ($this->countries === null) {
            
            $qb = $this->em->getRepository('VisiDarbiLocalePlaceBundle:Country')
                ->createQueryBuilder('c')
                ->select('c, lic, lng, dac')
                ->leftJoin('c.languagesInCountry', 'lic')
                ->leftJoin('lic.language', 'lng')
                ->leftJoin('c.deafultAdvertisementCountry', 'dac')
            ;
            
            $query = $qb->getQuery();
            
            $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
            $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
            
            $this->countries = $query->getResult();
            
        }
        
        return $this->countries;
    }

    public function getCurrentCountry() 
    {
        $countries = $this->getAllCountries();
        foreach ($countries as $country) {
            if ($country->getCode() == $this->countryEnvParam) {
                return $country;
            }
        }
        return reset($countries);
    }
    
    public function getCurrentAdminCountry() 
    {
        $currentId = $this->session->get('current_admin_country_id');
        if ($currentId === null) {
            return $this->getDefaultAdminCountry();
        }
        
        $country = $this->getCountry($currentId);
        
        return $country !== null ? $country : $this->getDefaultAdminCountry();
    }
    
    public function setCurrentAdminCountry(Country $country) 
    {
        $this->session->set('current_admin_country_id', $country->getId());
    }
    
    protected function getCountry($id) 
    {
        foreach ($this->getAllCountries() as $country) {
            if ($country->getId() == $id) {
                return $country;
            }
        }
        return null;
    }
    
    protected function getDefaultAdminCountry() 
    {
        $countries = $this->getAllCountries();
        return empty($countries) ? null : reset($countries);
    }
}

