<?php
namespace VisiDarbi\LocalePlaceBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 */
class City
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace", mappedBy="city")
     */
    private $externalPlace;

    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", inversedBy="Cities")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $Country;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->externalPlace = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add externalPlace
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace
     * @return City
     */
    public function addExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace)
    {
        $this->externalPlace[] = $externalPlace;
    
        return $this;
    }

    /**
     * Remove externalPlace
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace
     */
    public function removeExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace)
    {
        $this->externalPlace->removeElement($externalPlace);
    }

    /**
     * Get externalPlace
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalPlace()
    {
        return $this->externalPlace;
    }

    /**
     * Set Country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return City
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->Country = $country;
    
        return $this;
    }

    /**
     * Get Country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->Country;
    }
}