<?php

namespace VisiDarbi\LocalePlaceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecondaryCurrency
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SecondaryCurrency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     **/
    private $currency;
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", inversedBy="secondaryCurrencies") 
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */	
    protected $country;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $rate_to_primary_currency;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rate_to_primary_currency
     *
     * @param float $rateToPrimaryCurrency
     * @return SecondaryCurrency
     */
    public function setRateToPrimaryCurrency($rateToPrimaryCurrency)
    {
        $this->rate_to_primary_currency = $rateToPrimaryCurrency;
    
        return $this;
    }

    /**
     * Get rate_to_primary_currency
     *
     * @return float 
     */
    public function getRateToPrimaryCurrency()
    {
        return $this->rate_to_primary_currency;
    }

    /**
     * Set currency
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Currency $currency
     * @return SecondaryCurrency
     */
    public function setCurrency(\VisiDarbi\LocalePlaceBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return SecondaryCurrency
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
}