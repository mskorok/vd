<?php
namespace VisiDarbi\LocalePlaceBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 */
class Country
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /** 
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $code;
    
    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $domain;
    
    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $uri;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Category", mappedBy="country")
     */
    private $categories;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice", mappedBy="country")
     */
    private $advertisingTypePrices;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", mappedBy="country")
     */
    private $advetisements;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource", 
     *     mappedBy="country"
     * )
     */
    private $externalAdvertisementSources;

    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\LanguageInCountry", mappedBy="country", cascade={"all"}, orphanRemoval=true)
     */
    private $languagesInCountry;
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     **/
    private $primaryCurrency;
    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency", mappedBy="country", cascade={"all"}, orphanRemoval=true)
     */
    private $secondaryCurrencies;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry", mappedBy="country")
     */
    private $advertisementCountries;    

    
    
    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry")
     * @ORM\JoinColumn(name="default_adv_country_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $deafultAdvertisementCountry;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->advertisementCountries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Cities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->advertisingTypePrices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->advetisements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->externalAdvertisementSources = new \Doctrine\Common\Collections\ArrayCollection();      
        $this->languagesInCountry = new \Doctrine\Common\Collections\ArrayCollection();
        $this->externalProfessions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->externalPlaces = new \Doctrine\Common\Collections\ArrayCollection();
     }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     * @return Country
     */
    public function addCategories(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     */
    public function removeCategories(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }



    /**
     * Add advertisingTypePrices
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices
     * @return Country
     */
    public function addAdvertisingTypePrice(\VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices)
    {
        $this->advertisingTypePrices[] = $advertisingTypePrices;
    
        return $this;
    }

    /**
     * Remove advertisingTypePrices
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices
     */
    public function removeAdvertisingTypePrice(\VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices)
    {
        $this->advertisingTypePrices->removeElement($advertisingTypePrices);
    }

    /**
     * Get advertisingTypePrices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisingTypePrices()
    {
        return $this->advertisingTypePrices;
    }

    /**
     * Add advetisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements
     * @return Country
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements)
    {
        $this->advetisements[] = $advetisements;
    
        return $this;
    }

    /**
     * Remove advetisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements)
    {
        $this->advetisements->removeElement($advetisements);
    }

    /**
     * Get advetisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisements()
    {
        return $this->advetisements;
    }


    /**
     * Add advetisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements
     * @return Country
     */
    public function addAdvetisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements)
    {
        $this->advetisements[] = $advetisements;
    
        return $this;
    }

    /**
     * Remove advetisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements
     */
    public function removeAdvetisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advetisements)
    {
        $this->advetisements->removeElement($advetisements);
    }

    /**
     * Get advetisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvetisements()
    {
        return $this->advetisements;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Country
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get locales
     *
     * @return array 
     */
    public function getLocales()
    {
        $locales = array();
        foreach ($this->languagesInCountry as $lic) {
            $locales[] = $lic->getLanguage()->getLocale();
        }
        return $locales;
    }
    
    /**
     * 
     * @return array
     */
    public function getLanguages()
    {
        $lngs = array();
        foreach ($this->languagesInCountry as $lic) {
            $lngs[$lic->getLanguage()->getLocale()] = $lic->getLanguage();
        }
        return $lngs;
    }

    /**
     * Add externalAdvertisementSources
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSources
     * @return Country
     */
    public function addExternalAdvertisementSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSources)
    {
        $this->externalAdvertisementSources[] = $externalAdvertisementSources;
    
        return $this;
    }

    /**
     * Remove externalAdvertisementSources
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSources
     */
    public function removeExternalAdvertisementSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSources)
    {
        $this->externalAdvertisementSources->removeElement($externalAdvertisementSources);
    }

    /**
     * Get externalAdvertisementSources
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalAdvertisementSources()
    {
        return $this->externalAdvertisementSources;
    }

    public function __toString() 
    {
        return $this->getName();
    }

    /**
     * Add languagesInCountry
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\LanguageInCountry $languagesInCountry
     * @return Country
     */
    public function addLanguagesInCountry(\VisiDarbi\LocalePlaceBundle\Entity\LanguageInCountry $languagesInCountry)
    {
        $this->languagesInCountry[] = $languagesInCountry;
        
        $languagesInCountry->setCountry($this);
    
        return $this;
    }

    /**
     * Remove languagesInCountry
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\LanguageInCountry $languagesInCountry
     */
    public function removeLanguagesInCountry(\VisiDarbi\LocalePlaceBundle\Entity\LanguageInCountry $languagesInCountry)
    {
        $this->languagesInCountry->removeElement($languagesInCountry);
    }

    /**
     * Get languagesInCountry
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLanguagesInCountry()
    {
        return $this->languagesInCountry;
    }

    /**
     * Set primaryCurrency
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Currency $primaryCurrency
     * @return Country
     */
    public function setPrimaryCurrency(\VisiDarbi\LocalePlaceBundle\Entity\Currency $primaryCurrency = null)
    {
        $this->primaryCurrency = $primaryCurrency;
    
        return $this;
    }

    /**
     * Get primaryCurrency
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Currency 
     */
    public function getPrimaryCurrency()
    {
        return $this->primaryCurrency;
    }

    /**
     * Add secondaryCurrencies
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency $secondaryCurrencies
     * @return Country
     */
    public function addSecondaryCurrencie(\VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency $secondaryCurrencies)
    {
        $this->secondaryCurrencies[] = $secondaryCurrencies;
        
        $secondaryCurrencies->setCountry($this);
  
        return $this;
    }

    /**
     * Remove secondaryCurrencies
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency $secondaryCurrencies
     */
    public function removeSecondaryCurrencie(\VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency $secondaryCurrencies)
    {
        $this->secondaryCurrencies->removeElement($secondaryCurrencies);
    }
    
    public function addSecondaryCurrency(\VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency $secondaryCurrencies) 
    {
        $this->addSecondaryCurrencie($secondaryCurrencies);
    }
    
    public function removeSecondaryCurrency(\VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency $secondaryCurrencies) 
    {
        $this->removeSecondaryCurrencie($secondaryCurrencies);
    }

    /**
     * Get secondaryCurrencies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSecondaryCurrencies()
    {
        return $this->secondaryCurrencies;
    }
    
    public function getSecondaryCurrencyByCode($code){
        $collection = $this->getSecondaryCurrencies();
        foreach($collection as $v) {
            if($v->getCurrency()->getCode() == $code) {
                return $v;
            }
        }
        
        $firstSecondaryCurrency = $this->getFirstSecondaryCurrency();

        if($firstSecondaryCurrency instanceof \VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency && $firstSecondaryCurrency->getCurrency() != $this->getPrimaryCurrency()) {
            return $firstSecondaryCurrency;
        }
        
        return null;
    }
    
    public function getFirstSecondaryCurrency(){
        
        $collection = $this->getSecondaryCurrencies();
        
        if(count($collection)) {
            return $collection[0];
        }
        
        return null;
    }


    /**
     * Get advetisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisementCountries()
    {
        return $this->advertisementCountries;
    }


    /**
     * Add advertisement country
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry
     * @return Country
     */
    public function addAdvertisementCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry)
    {
        $this->advertisementCountries[] = $advertisementCountry;
    
        return $this;
    }

    /**
     * Remove advertisementcountry
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry
     */
    public function removeAdvertisementCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry)
    {
        $this->advertisementCountries->removeElement($advertisementCountry);
    }    
    

    /**
     * Add advertisementCountries
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountries
     * @return Country
     */
    public function addAdvertisementCountrie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountries)
    {
        $this->advertisementCountries[] = $advertisementCountries;
    
        return $this;
    }

    /**
     * Remove advertisementCountries
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountries
     */
    public function removeAdvertisementCountrie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountries)
    {
        $this->advertisementCountries->removeElement($advertisementCountries);
    }



    /**
     * Add externalPlaces
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces
     * @return Country
     */
    public function addExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces)
    {
        $this->externalPlaces[] = $externalPlaces;
    
        return $this;
    }

    /**
     * Remove externalPlaces
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces
     */
    public function removeExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces)
    {
        $this->externalPlaces->removeElement($externalPlaces);
    }

    /**
     * Set deafultAdvertisementCountry
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $deafultAdvertisementCountry
     * @return Country
     */
    public function setDeafultAdvertisementCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $deafultAdvertisementCountry = null)
    {
        $this->deafultAdvertisementCountry = $deafultAdvertisementCountry;
    
        return $this;
    }

    /**
     * Get deafultAdvertisementCountry
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry 
     */
    public function getDeafultAdvertisementCountry()
    {
        return $this->deafultAdvertisementCountry;
    }
    
    

    /**
     * Set uri
     *
     * @param string $uri
     * @return Country
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    
        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }
    
    /**
     * Returns default locale
     * @return string
     */
    public function getDefaultLocale(){
        /**
         * @todo rewrite functionalty
         */
        $locales = $this->getLocales();
        return $locales[0];
    }
}