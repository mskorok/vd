<?php

namespace VisiDarbi\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\LocalePlaceBundle\Entity\Language;
use VisiDarbi\LocalePlaceBundle\Entity\LanguageInCountry;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCountries extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $countryLv = new Country();
        $countryLv
            ->setName('Latvia')
            ->setDomain('dev.visidarbi.lv')
            ->setUri('http://' . $countryLv->getDomain())
            ->setCode('lv')
            ->setPrimaryCurrency($this->getReference('currency-LVL'))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-lv'), true, 0))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-ru'), true, 1))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-en'), true, 2))
        ;
        
        $countryLt = new Country();
        $countryLt
            ->setName('Lithuania')
            ->setDomain('dev.visidarbi.lt')
            ->setUri('http://' . $countryLt->getDomain())
            ->setCode('lt')
            ->setPrimaryCurrency($this->getReference('currency-EUR'))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-lt'), true, 0))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-ru'), true, 1))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-en'), true, 2))
        ;
        
        $countryEe = new Country();
        $countryEe
            ->setName('Estonia')
            ->setDomain('dev.visidarbi.ee')
            ->setUri('http://' . $countryEe->getDomain())
            ->setCode('ee')
            ->setPrimaryCurrency($this->getReference('currency-EUR'))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-ee'), true, 0))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-ru'), true, 1))
            ->addLanguagesInCountry(new LanguageInCountry($this->getReference('language-en'), true, 2))
        ;
        
        $manager->persist($countryLv);
        $manager->persist($countryLt);
        $manager->persist($countryEe);
        
        $this->addReference('country-lat', $countryLv);
        $this->addReference('country-lit', $countryLt);
        $this->addReference('country-est', $countryEe);
        
        $manager->flush();
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }    
}
