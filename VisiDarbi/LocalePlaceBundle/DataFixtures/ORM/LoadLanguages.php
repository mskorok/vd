<?php

namespace VisiDarbi\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\LocalePlaceBundle\Entity\Language;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadLanguages extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        
        $langs = array(
            'lv' => 'Latvian',
            'ru' => 'Russian',
            'en' => 'English',
            'lt' => 'Lithuanian',
            'ee' => 'Estonian'
        );
        
        foreach ($langs as $locale => $name) {
            $lng = new Language();
            $lng->setName($name);
            $lng->setLocale($locale);
            $manager->persist($lng);
            
            $this->addReference('language-' . $locale, $lng);
        }
        
        $manager->flush();
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9;
    }    
}
