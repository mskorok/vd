<?php

namespace VisiDarbi\SlugBundle\Listener;

class SluggableListener extends \Gedmo\Sluggable\SluggableListener{
    
    public function __construct(){
        $this->setTransliterator(array('VisiDarbi\SlugBundle\Utils\Transliterator', 'transliterate'));
    }
}