<?php 

namespace VisiDarbi\SlugBundle\Utils;
use Cocur\Slugify\Slugify;
 
class Transliterator {
	
    public static function transliterate($text, $separator, $object){

    	$slugify = new Slugify();
       	$text = $slugify->slugify($text);

        return \Gedmo\Sluggable\Util\Urlizer::urlize($text, $separator);
    }
}