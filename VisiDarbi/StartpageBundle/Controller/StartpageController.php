<?php
namespace VisiDarbi\StartpageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
//use VisiDarbi\StatisticsBundle\Entity\Search;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\Query\Expr;
use VisiDarbi\MobileBundle\Controller\MobileController;

class StartpageController extends Controller
{
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;

    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {
        //MobileController::isMobile($this->getRequest());
        $searchHistoryRepo = $this->em->getRepository('VisiDarbiStatisticsBundle:SearchHistory');
        /* @var $searchHistoryRepo \VisiDarbi\VisiDarbiStatisticsBundle\EntityRepository\SearchHistoryRepository */

        $startpageTagCloudKeywords = $searchHistoryRepo->getSearchKeywordListForTagCloud(
            $this->countryManager->getCurrentCountry(),
            $this->request->getLocale(),
            50, //max keyword count for cloud
            3 // min search count for keyword
        );

        $advRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');
        /* @var $advRepo \VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository */

        /**
         * array of top/featured ads for homepage "featured ads" slider block
         * (see column data preparation at the bottom of current action)
         */
        $startpageAdverts = $advRepo->getFeaturedAdvertisements(
                $this->countryManager->getCurrentCountry(),
                $this->request->getLocale(),
                90
        );

        /*$latestAdverts = $advRepo->getLatestAdvertisements(
                $this->countryManager->getCurrentCountry(),
                $this->request->getLocale(),
                30
        );*/

        $socialAdverts = array();
        /*
        $socialAdverts = $advRepo->getAdvertisementsFromSocialNetworks(
                $this->countryManager->getCurrentCountry(),
                $this->request->getLocale(),
                $this->settingsManager->get('site.startpage_ext_adv_count'),
                'linkedin'
        );
        */

        $locale = $this->request->getLocale();
        $systemCountry =  $this->countryManager->getCurrentCountry();
        $adDefaultCountry = $systemCountry->getDeafultAdvertisementCountry();

        //companies
        $companyRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Company');
        $qb = $companyRepo->createQueryBuilder('c')
                ->select('c.id, c.name, c.slug, COUNT(c.id) as adv_count')
                ->leftJoin('c.advertisement', 'a')
                ->where('a.country = :country')
                ->setParameter('country', $systemCountry->getId())
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->groupBy('c.slug')
                ->orderBy('adv_count', 'DESC')
                ->setMaxResults($this->settingsManager->get('site.startpage_company_count'))
                ->having('adv_count > 0')
        ;
        $companies = $qb->getQuery()
                        ->useResultCache(true, 600, 'start_page_companies_'.$systemCountry->getId().'_'.$locale)
                        ->getResult();



        //profession categories
        $categoryRepo = $this->em->getRepository('VisiDarbiProfessionBundle:Category');
        $qb = $categoryRepo->createQueryBuilder('c')
                ->select('c.id as category_id, c.name as category_name, ct.slug as category_slug, COUNT(a.id) as adv_count')
                ->leftJoin('c.Advertisements', 'a')
                ->leftJoin('c.translations', 'ct', Expr\Join::WITH, 'ct.locale=:locale' )
                ->setParameter('locale', $locale)
                ->where('c.country = :country')
                ->setParameter('country', $systemCountry->getId())
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->having('adv_count > 0')
                ->groupBy('c.id')
                ->orderBy('adv_count', 'DESC')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $query->useResultCache(true, 1000, 'startpage_profession_categories_'.$systemCountry->getId().'_'.$locale);
        $categories = $query->getResult();


       //countries/cities
        $advCountryRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCountry');
        $qb = $advCountryRepo->createQueryBuilder('c')
                ->select('c.id as country_id, c.name as country_name, COUNT(a.id) as adv_count, ct.slug as country_slug')
                ->leftJoin('c.Advertisements', 'a')
                ->leftJoin('c.translations', 'ct', Expr\Join::WITH, 'ct.locale=:locale' )
                ->setParameter('locale', $locale)

                ->where('c.country = :country')
                ->setParameter('country', $systemCountry->getId())
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->groupBy('c.id')
                ->orderBy('country_name', 'ASC')
                ->having('adv_count > 0')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $query->useResultCache(true, 600, 'startpage_countries_cities_'.$systemCountry->getId().'_'.$locale);
        $advCountries = $query->getResult();

        $advCityRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity');

        $qb = $advCityRepo->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, COUNT(a.id) as adv_count, ct.slug as city_slug')
            ->leftJoin('c.Advertisements', 'a')
            ->leftJoin('c.translations', 'ct', Expr\Join::WITH, "ct.locale=:locale AND ct.field = 'name' " )
            ->setParameter('locale', $locale)
            ->where('c.mark_on_frontend = 1')
            ->andWhere('c.AdvertisementCountry = :country')
            ->setParameter('country', $adDefaultCountry)
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->groupBy('c.id')
            ->orderBy('city_name', 'ASC')
            ->having('adv_count > 0')
        ;

        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $query->useResultCache(true, 600, 'startpage_special_adv_cities_'.$systemCountry->getId().'_'.$locale);
        $specialAdvCities = $query->getResult();

        $qb = $advCityRepo->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, COUNT(a.id) as adv_count, ct.slug as city_slug')
            ->leftJoin('c.Advertisements', 'a')
            ->leftJoin('c.translations', 'ct', Expr\Join::WITH, "ct.locale=:locale AND ct.field = 'name' " )
            ->where('c.mark_on_frontend = 0')
            ->andWhere('c.AdvertisementCountry = :country')
            ->setParameter('country', $adDefaultCountry)
            ->setParameter('locale', $locale)
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->groupBy('c.id')
            ->orderBy('c.name', 'ASC')
            ->orderBy('city_name', 'ASC')
            ->having('adv_count > 0')
        ;

        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $query->useResultCache(true, 600, 'startpage_adv_cities_'.$systemCountry->getId().'_'.$locale);
        $advCities = $query->getResult();

        /*
        $linkedinSource = $this->em->getRepository('VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementSource')->findOneBy(array('resource' => 'linkedin'));
        $allSocAdvListUrl = $linkedinSource === null ? 'javascript:;' : $this->generateUrl('visidarbi_advertisement_list_filter', array('filter'=>base64_encode(implode('|', array('', '', '', $linkedinSource->getId())))), true);
        */
        $allSocAdvListUrl = null;

        return array(
            'tag_cloud_keywords' => $startpageTagCloudKeywords,

            'startpage_advert_cols' => $this->makeColumns($startpageAdverts, 3, true),

            /*'latest_advert_cols' => $this->makeColumns($latestAdverts, 2),*/
            'social_adverts' => $socialAdverts,

            'adv_cities' => $advCities,
            'special_adv_cities' => $specialAdvCities,
            'defaultAdCountry' => $adDefaultCountry,
            'adv_countries' => $advCountries,

            'categories' => $categories,
            'companies' => $companies,

            'all_soc_adv_list_url' => $allSocAdvListUrl,

        );

        return array();
    }

    protected function makeColumns($collection, $itemsPerCol, $forceFullColumns = false)
    {
        $cols = array();
        foreach ($collection as $key => $item) {
            $colKey = ceil( ($key +1 ) / $itemsPerCol ) - 1;
            $cols[ $colKey ][] = $item;
            if ($forceFullColumns && $key == (count($collection)-1) && count($cols[ $colKey ]) < $itemsPerCol){ // remove column if it not full
                unset($cols[ $colKey ]);
            }
        }
        return $cols;
    }
}
