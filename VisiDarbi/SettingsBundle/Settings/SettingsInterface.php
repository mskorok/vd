<?php

namespace VisiDarbi\SettingsBundle\Settings;

interface SettingsInterface 
{
    public function buildSettings(SettingsBuilderInterface $settingsBuilder);
}
