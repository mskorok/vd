<?php

namespace VisiDarbi\SettingsBundle\Settings;

interface SettingsRegistryInterface 
{
    public function getAllSettings();

    public function registerSettings(SettingsInterface $settings);

    public function getSettings($namespace);
    
    public function getNamespaces();
}
