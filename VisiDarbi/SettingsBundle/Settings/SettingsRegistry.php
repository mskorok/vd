<?php

namespace VisiDarbi\SettingsBundle\Settings;

class SettingsRegistry implements SettingsRegistryInterface
{
    private $settings = array();
    
    public function getAllSettings() 
    {
        return $this->settings;
    }

    public function registerSettings(SettingsInterface $settings)
    {
        $builder = new SettingsBuilder();
        $settings->buildSettings($builder);
        
        foreach ($builder as $key => $setting) {
            if (! isset($this->settings[$key])) {
                $this->settings[$key] = array();
            }
            $this->settings[$key] = array_merge($this->settings[$key], $setting);
        }
    }

    public function getSettings($namespace) 
    {
        return isset($this->settings[$namespace]) ? $this->settings[$namespace] : array();
    }
    
    public function getNamespaces() 
    {
        return array_keys($this->settings);
    }
}