<?php

namespace VisiDarbi\SettingsBundle\Settings;

interface SettingsBuilderInterface extends \Traversable
{
    /**
     * @param string $namespace
     * @return SettingsBuilderInterface
     */
    public function in($namespace);
    
    /**
     * 
     * @param string $name
     * @param string $label
     * @param boolean $required
     * @param string $defaultValue
     * @return SettingsBuilderInterface
     */
    public function add($name, $label, $required = false, $defaultValue = null, $translatable = false, $type = 'text');
    
    /**
     * @return SettingsBuilderInterface
     */
    public function end();
}

