<?php

namespace VisiDarbi\SettingsBundle\Settings;
use JMS\TranslationBundle\Annotation\Ignore;

class SettingsBuilder implements \IteratorAggregate, SettingsBuilderInterface
{
    
    protected $settings = array();
    
    protected $currentNamespace = null;
    
    /**
     * {@inheritdoc}
     */
    public function in($namespace)
    {
        $this->currentNamespace = $namespace;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function add($name, $label, $required = false, $defaultValue = null, $translatable = false, $type = 'text')
    {
        if ($this->currentNamespace === null) {
            //TODO: throw exeption
        }
        
        $this->settings[$this->currentNamespace][$name] = array(
            'name' => $name,
            'label' => /** @Ignore */$label,
            'required' => $required,
            'defaultValue' => $defaultValue,
            'translatable' => $translatable,
            'type' => $type
        );
        
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function end()
    {
        $this->currentNamespace = null;
        
        return $this;
    }
    
    public function getIterator()
    {
        return new \ArrayIterator($this->settings);
    }
}