<?php

namespace VisiDarbi\SettingsBundle\Manager;

use VisiDarbi\LocalePlaceBundle\Entity\Country;

/**
 * Manager, responsible for retrieving and updating setting values
 */
interface SettingsManagerInterface 
{
    /**
     * Returns array of all setting values of given namespace
     * 
     * @param string $namespace
     */
    public function getValues($namespace, Country $country = null);

    /**
     * Updates setting values of given namespace
     * 
     * @param string $namespace
     * @param array $data
     */
    public function updateValues($namespace, $data, Country $country);
    
    /**
     * Returns value of individual setting
     * Setting key must be in format namespace.name
     * 
     * @param string $key
     * @return string|null
     */
    public function get($key);
}