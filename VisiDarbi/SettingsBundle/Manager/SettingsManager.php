<?php

namespace VisiDarbi\SettingsBundle\Manager;

use VisiDarbi\SettingsBundle\Settings\SettingsRegistryInterface;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use Doctrine\ORM\EntityManager;
use VisiDarbi\SettingsBundle\Entity\SettingsParameter;
use Doctrine\Common\Collections\ArrayCollection;

use VisiDarbi\LocalePlaceBundle\Entity\Country;

class SettingsManager implements SettingsManagerInterface 
{
    
    /**
     * @var SettingsRegistryInterface 
     */
    protected $registry;
    
    /**
     * @var EntityManager 
     */
    protected $em;
    
    /**
     * @var CountryManagerInterface
     */
    protected $countryManager;
   
    
    protected $cache = array();


    public function __construct(SettingsRegistryInterface $registry, EntityManager $em, CountryManagerInterface $countryManager) 
    {
        $this->registry = $registry;
        $this->em = $em;
        $this->countryManager = $countryManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getValues($namespace, Country $country = null)
    {
        if (isset($this->cache[$namespace])) {
            return $this->cache[$namespace];
        }
        
        if ($country == null) {
            $country = $this->countryManager->getCurrentCountry();
        }
        
        //get array of all defined settings in given namespace and their inital values
        $settings = array();
        foreach ($this->registry->getSettings($namespace) as $s) {
            if ($s['translatable']) {
                foreach ($country->getLocales() as $lng) {
                    $settings[$s['name'] . '_' . $lng] = $s['defaultValue'];
                }
            }
            else {
                $settings[$s['name']] = $s['defaultValue'];
            }
        }
        
        
        
        //get actual values
        $repo = $this->em->getRepository('VisiDarbiSettingsBundle:SettingsParameter');
        $params = $repo->findBy(array( 'namespace' => $namespace, 'country' => $country ));
        $paramCollection = new ArrayCollection($params);
        
        foreach ($settings as $name => & $value) {
            $filter = function(SettingsParameter $param) use ($name) {
                return $param->getName() == $name;
            };
            $param = $paramCollection->filter($filter)->first();
            if ($param !== false) {
                $value = $param->getValue();
            }
        }
        
        $this->cache[$namespace] = $settings;
        
        return  $settings;
    }

    /**
     * {@inheritdoc}
     */
    public function updateValues($namespace, $data, Country $country)
    {
        if (empty($data) || ! is_array($data)) {
            return;
        }
        
        $repo = $this->em->getRepository('VisiDarbiSettingsBundle:SettingsParameter');
        foreach ($data as $key => $value) {
            $param = $repo->findOneBy(array( 'name' => $key, 'namespace' => $namespace, 'country' => $country ));
            if ($param === null) {
                $param = new SettingsParameter();
                $param
                    ->setName($key)
                    ->setNamespace($namespace)
                    ->setCountry($country)
                ;
            }
            $param->setValue($value);
            $this->em->persist($param);
        }
        
        $this->em->flush();
        
        if (isset($this->cache[$namespace])) {
            unset($this->cache[$namespace]);
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function get($key, $lng = null) 
    {
        $x = \explode('.', $key);
        if (count($x) != 2) {
            throw new \Exception('Invalid setting key. Must be in format namespace.name.');
        }
        
        $namespace = $x[0];
        $name = $x[1] . ($lng === null ? '' : '_' . $lng);
        
        $settings = $this->getValues($namespace);
        
        if (! array_key_exists($name, $settings)) {
            throw new \Exception(\sprintf('Unexisting setting key "%s".', $name));
        }
        
        return $settings[$name];
    }
}