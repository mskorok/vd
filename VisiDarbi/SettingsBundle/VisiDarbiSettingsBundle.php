<?php

namespace VisiDarbi\SettingsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use VisiDarbi\SettingsBundle\DependencyInjection\Compiler\RegisterSettingsPass;

class VisiDarbiSettingsBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new RegisterSettingsPass());
    }
}
