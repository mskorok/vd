<?php

namespace VisiDarbi\SettingsBundle\Twig;

use VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface;
use Twig_Extension;
use Twig_Function_Method;


class SettingsExtension extends Twig_Extension
{
    /**
     * @var SettingsManagerInterface
     */
    private $settingsManager;


    public function __construct(SettingsManagerInterface $sm)
    {
        $this->settingsManager = $sm;
    }

    public function getFunctions()
    {
        return array(
            'setting' => new Twig_Function_Method($this, 'getSetting'),
        );
    }

    public function getSetting($key, $lng = null)
    {
        return $this->settingsManager->get($key, $lng);
    }

    public function getName()
    {
        return 'visidarbi_settings';
    }
}
