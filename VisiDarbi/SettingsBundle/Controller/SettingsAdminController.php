<?php

namespace VisiDarbi\SettingsBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VisiDarbi\SettingsBundle\Settings\SettingsRegistryInterface;
use VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use Symfony\Component\Validator\Constraints\Image;

use VisiDarbi\LocalePlaceBundle\Entity\Country;
use JMS\TranslationBundle\Annotation\Ignore;

class SettingsAdminController extends CRUDController
{
    
    /**
     * @return SettingsRegistryInterface
     */
    public function getSettingsRegistry() 
    {
        return $this->get('visidarbi.settings.registry');
    }
    
    /**
     * @return SettingsManagerInterface
     */
    public function getSettingsManager() 
    {
        return $this->get('visidarbi.settings.manager');
    }
    
    /**
     * @return CountryManagerInterface
     */
    public function getCountryManager() 
    {
        return $this->get('visidarbi.country_manager');
    }


    public function listAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }
        
        $registry = $this->getSettingsRegistry();
        $manager = $this->getSettingsManager();
        $request = $this->getRequest();
        
        $namespaces = $registry->getNamespaces();
        $country = $this->getCountryManager()->getCurrentAdminCountry();
        
        $currentNamespace = reset($namespaces);
        if (in_array($request->get('namespace'), $namespaces)) {
            $currentNamespace = $request->get('namespace');
        }

        $settingsValues = $manager->getValues($currentNamespace, $country);

        $form = $this->createFormBuilder(
            $settingsValues
        );
        $uploadConfig = $this->container->getParameter('upload_settings');
        $constrains = array(new Image(array('maxSize'=>$uploadConfig['max_size'])));


        $settingDescriptions = $registry->getSettings($currentNamespace);
        $settingKeys = array();
        foreach ($settingDescriptions as $setting) {
            if ($setting['translatable']) {
                foreach ($country->getLocales() as $lng) {
                    $settingKeys[] = $setting['name'] . '_' . $lng;
                    $form->add($setting['name'] . '_' . $lng, !empty($setting['type']) ? $setting['type'] : 'text', array(
                        'label' => /** @Ignore */$setting['label'] . ' ('. strtoupper($lng) . ')',
                        'data_class' => null,
                        'required' => $setting['required'],
                        'constraints' => ($setting['type'] == 'file') ? $constrains : array(),
                        'attr' => array('class' => 'span5'),
                    ));
                }
            }
            else {
                $settingKeys[] = $setting['name'];
                $form->add($setting['name'], !empty($setting['type']) ? $setting['type'] : 'text', array(
                     'label' => /** @Ignore */$setting['label'],
                     'data_class' => null,
                     'required' => $setting['required'],
                     'constraints' => ($setting['type'] == 'file') ? $constrains : array(),
                     'attr' => array('class' => 'span5'),
                 )); 
            }
            
        }
        $form = $form->getForm();
        
        if ($request->isMethod('POST') && $form->bind($request)->isValid() ) {
            $form_data = $form->getData();
            $this->saveFiles($settingsValues, $form_data);
            $manager->updateValues(
                $currentNamespace,
                $form_data,
                $country
            );
            
            $this->get('session')->setFlash('sonata_flash_success', 'Settings updated successfully!');
        }
        
        
        $formView = $form->createView();
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFormTheme());

        return $this->render('VisiDarbiSettingsBundle:SettingsAdmin:list.html.twig', array(
            'action'   => 'list',
            'namespaces' => $namespaces,
            'currentNamespace' => $currentNamespace,
            'form'     => $formView,
            'setting_keys' => $settingKeys,
        ));
    }

    private function saveFiles($settingValues, &$form_data) {
        $files = $this->getRequest()->files->get('form');
        if (!empty($files) && is_array($files)) {
            $uploadConfig = $this->container->getParameter('upload_settings');
            foreach ($files as $field => $file) {
                if (!empty($file)) {
                    $extension = $file->guessExtension();
                    $path = __DIR__ . '/../../../../web/' . $uploadConfig['stored_path'].'/';
                    $newName = $field.'.' . $extension;

                    if ($file->move($path, $newName)) {
                        try {
                            $uploadedFile = $path.$newName;
                            $img = $this->get('image.handling')->open($uploadedFile);
                            $img->resize(390,null)
                                ->save($uploadedFile, $img->guessType());
                        } catch (\Exception $e) {}
                        $form_data[$field] = $newName;
                        if (!empty($settingValues[$field]) && $settingValues[$field] != $newName && file_exists($path.$settingValues[$field])) {
                            unlink($path.$settingValues[$field]);
                        }
                    }
                } else {
                    $form_data[$field] = $settingValues[$field];
                }
            }
        }
    }
}