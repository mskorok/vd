<?php

namespace VisiDarbi\SettingsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


class RegisterSettingsPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (! $container->hasDefinition('visidarbi.settings.registry')) {
            return;
        }

        $settingsRegistry = $container->getDefinition('visidarbi.settings.registry');

        foreach ($container->findTaggedServiceIds('visidarbi.settings.definition') as $id => $attributes) {
            $settingsRegistry->addMethodCall('registerSettings', array(new Reference($id)));
        }
    }
}
