<?php

namespace VisiDarbi\EmailTemplatingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadEmailTemplateData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        
        $countries = array(
            $this->getReference('country-lat'),
            $this->getReference('country-lit'),
            $this->getReference('country-est')
        );
        
        foreach ($countries as $country) {
            $locales = $country->getLocales();
            foreach (array_reverse($locales) as $locale) {
        
                //registration confirmation
                $regConfBody = implode("", array(
                    '<p>Jūs esat reģistrējies visidarbi.lv vortālā!</p>',
                    '<p>Sekojiet hipersaitei, lai pabeigtu reģistrāciju: {{confirmLink}}</p>',
                ));
        
                $regConfTemplate = new EmailTemplate();
                $regConfTemplate
                    ->setCode(EmailTemplate::ID_REG_CONFIRMATION)
                    ->setCountry($country)
                    ->setLocale($locale)
                    ->setSubject('Registrācija ('. $locale .')')
                    ->setBody($regConfBody)
                    ->setPlaceholders(array('confirmLink'))
                ;
                
                $manager->persist($regConfTemplate);
                          
                //temporary password
                $tmpPasswBody = implode('', array(
                    '<p>Jūs esat pieprasījis pagaidu paroli visidarbi.lv vortālā!</p>',
                    '<p>Tā ir šāda: {{tmpPassword}}</p>',
                    '<p>Ejiet ielogoties {{siteLink}} un nomainiet šo pagaidu paroli uz jaunu!</p>',
                ));
                $tmpPasswTemplate = new EmailTemplate();
                $tmpPasswTemplate
                    ->setCode(EmailTemplate::ID_TMP_PASSWORD)
                    ->setCountry($country)
                    ->setLocale($locale)
                    ->setSubject('Pagaidu parole ('. $locale .')')
                    ->setBody($tmpPasswBody)
                    ->setPlaceholders(array('tmpPassword', 'siteLink'))
                ;
                
                $manager->persist($tmpPasswTemplate);
                
                
                //registration confirmation
                $changeEmailBody = implode('', array(
                    '<p>Jūs esat pieprasījis e-pasta nomaiņu visidarbi.lv vortālā!</p>',
                    '<p>Lai šis pieprasījums stātos spēkā, sekojiet šai saitei: {{confirmLink}}</p>',
                ));
        
                $changeEmailTemplate = new EmailTemplate();
                $changeEmailTemplate
                    ->setCode(EmailTemplate::ID_EMAIL_CHANGE_CONFIRMATION)
                    ->setCountry($country)
                    ->setLocale($locale)
                    ->setSubject('E-pasta maiņas apstiprinājums ('. $locale .')')
                    ->setBody($changeEmailBody)
                    ->setPlaceholders(array('confirmLink'))
                ;
                
                $manager->persist($changeEmailTemplate);
                
                
                
                //share advertisement
                $shareAdvertisementBody = implode("", array(
                    '<p>You have suggested vacancy link from your friend - {{emailFrom}}!</p>',
                    '<p>View vacancy on: {{shareLink}}</p>',
                    '<p>{{message}}</p>',                    
                ));
        
                $shareAdvertisementTemplate = new EmailTemplate();
                $shareAdvertisementTemplate
                    ->setCode(EmailTemplate::ID_SHARE_ADVERTISEMENT)
                    ->setCountry($country)
                    ->setLocale($locale)
                    ->setSubject('Suggested vacancy ('. $locale .')')
                    ->setBody($shareAdvertisementBody)
                    ->setPlaceholders(array('shareLink', 'emailFrom', 'message'))
                ;
                
                $manager->persist($shareAdvertisementTemplate);                
                
                
                
            }
        }

        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }    
    
}
