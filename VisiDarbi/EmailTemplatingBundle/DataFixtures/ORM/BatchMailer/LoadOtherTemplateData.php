<?php

namespace VisiDarbi\EmailTemplatingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadOtherTemplateData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $country = $manager->getRepository('VisiDarbiLocalePlaceBundle:Country')->findOneBy(array('code' => 'lv'));

        $locales = $country->getLocales();
        foreach (array_reverse($locales) as $locale) {

            //user newsletter
            $userNewsletterBody = implode("", array(
                '<p>{{newsLetter}}</p>',
                '<div><a href="{{viewAllAdvertsLink}}">View all advertisements</a></div>',
                '<div><a href="{{unsubscriptionLink}}">Unsubscribe</a></div>'
            ));

            $userNewsletterTemplate = new EmailTemplate();
            $userNewsletterTemplate
                ->setCode(EmailTemplate::ID_USER_NEWSLETTER)
                ->setCountry($country)
                ->setLocale($locale)
                ->setSubject('Jaunākie sludinājumi no visidarbi.lv (' . $locale . ')')
                ->setBody($userNewsletterBody)
                ->setPlaceholders(array('newsLetter', 'unsubscriptionLink'));

            $manager->persist($userNewsletterTemplate);

            //highlighting offer
            $hlOfferBody = implode("", array(
                '<p>Paldies par sludinājuma izvietošanu portālā VisiDarbi!</p>',
                '<p>Jūs varat izcelt savu sludinājumu:</p>',
                '<p>{{advHlLink}}</p>'
            ));

            $hlOfferTemplate = new EmailTemplate();
            $hlOfferTemplate
                ->setCode(EmailTemplate::ID_HIGHLIGHTING_OFFER)
                ->setCountry($country)
                ->setLocale($locale)
                ->setSubject('Izcelšana (' . $locale . ')')
                ->setBody($hlOfferBody)
                ->setPlaceholders(array('advHlLink'));

            $manager->persist($hlOfferTemplate);

        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }

}
