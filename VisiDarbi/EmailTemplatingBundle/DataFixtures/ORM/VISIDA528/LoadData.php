<?php

namespace VisiDarbi\EmailTemplatingBundle\DataFixtures\ORM\VISIDA528;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $countries = $manager->getRepository('VisiDarbiLocalePlaceBundle:Country')->findAll();
        foreach ($countries as $country) {
            $locales = $country->getLocales();
            foreach (array_reverse($locales) as $locale) {
                $manager->persist($this->getActivationTemplate($country, $locale));
                $manager->persist($this->getDeactivationTemplate($country, $locale));
            }
        }
        $manager->flush();
    }
    
    private function getActivationTemplate($country, $locale)
    {
        $body = implode('', array(
            '<p>Jūsu kontam aktivizēts šāds pakalpojums: {{serviceTitle}}</p>',
        ));

        $emailTemplate = new EmailTemplate();
        $emailTemplate
            ->setCode(EmailTemplate::ID_SERVICE_ACTIVATED)
            ->setCountry($country)
            ->setLocale($locale)
            ->setSubject('Jūsu kontam aktivizēts jauns pakalpojums ('. $locale .')')
            ->setBody($body)
            ->setPlaceholders(array('serviceTitle'))
        ;
        
        return $emailTemplate;
    }
    
    private function getDeactivationTemplate($country, $locale)
    {
        $body = implode('', array(
            '<p>Jūsu kontam deaktivizēts šāds pakalpojums: {{serviceTitle}}</p>',
        ));

        $emailTemplate = new EmailTemplate();
        $emailTemplate
            ->setCode(EmailTemplate::ID_SERVICE_DEACTIVATED)
            ->setCountry($country)
            ->setLocale($locale)
            ->setSubject('Jūsu kontam deaktivizēts pakalpojums ('. $locale .')')
            ->setBody($body)
            ->setPlaceholders(array('serviceTitle'))
        ;
        
        return $emailTemplate;
    }
    
}
