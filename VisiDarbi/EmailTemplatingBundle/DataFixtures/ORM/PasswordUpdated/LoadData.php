<?php

namespace VisiDarbi\EmailTemplatingBundle\DataFixtures\ORM\PasswordUpdated;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        
        $countries = $manager->getRepository('VisiDarbiLocalePlaceBundle:Country')->findAll();
        
        foreach ($countries as $country) {
            $locales = $country->getLocales();
            foreach (array_reverse($locales) as $locale) {
                $body = implode('', array(
                    '<p>Jūsu jaunā parole: {{password}}</p>',
                ));
        
                $emailTemplate = new EmailTemplate();
                $emailTemplate
                    ->setCode(EmailTemplate::ID_PASSWORD_UPDATED)
                    ->setCountry($country)
                    ->setLocale($locale)
                    ->setSubject('Jūsu visidarbi.lv parole uzstādīta ('. $locale .')')
                    ->setBody($body)
                    ->setPlaceholders(array('password'))
                ;
                
                $manager->persist($emailTemplate);
            }
        }

        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }    
    
}
