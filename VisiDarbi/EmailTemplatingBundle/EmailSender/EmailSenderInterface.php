<?php

namespace VisiDarbi\EmailTemplatingBundle\EmailSender;

use VisiDarbi\LocalePlaceBundle\Entity\Country;

interface EmailSenderInterface 
{
    public function send(Country $country, $locale, $templateCode, $recipient, array $data, array $sender);
}
