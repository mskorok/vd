<?php

namespace VisiDarbi\EmailTemplatingBundle\EmailSender;

use \Swift_Mailer;
use Doctrine\ORM\EntityManager;

use VisiDarbi\LocalePlaceBundle\Entity\Country;

class EmailSender implements EmailSenderInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager 
     */
    protected $em;

    /**
     * @var \Swift_Mailer 
     */
    protected $mailer;
    
    public function __construct(EntityManager $em, Swift_Mailer $mailer)
    {   
        $this->em = $em;
        $this->mailer = $mailer;
    }

    public function send(Country $country, $locale, $templateCode, $recipient, array $data, array $sender) 
    {
        $template = $this->em->getRepository('VisiDarbiEmailTemplatingBundle:EmailTemplate')->findOneBy(array(
            'country' => $country,
            'locale' => $locale,
            'code' => $templateCode
        ));
        
        if ($template === null) {
            throw new \Exception(
                sprintf('Unable to find template "%s" in country "%s" and locale "%s".', $templateCode, $country->getId(), $locale)
            );
        }
        
        $body = $template->getBody();
        foreach ($template->getPlaceholders() as $placeholder) {
            if (! array_key_exists($placeholder, $data)) {
                throw new \Exception(
                    sprintf('Template "%s" requires parameter "%s".', $templateCode, $placeholder)
                );
            }
            $body = str_replace('{{' . $placeholder . '}}', $data[$placeholder], $body);
        }

        $message = \Swift_Message::newInstance()
            ->setSubject($template->getSubject())
            ->setFrom($sender['email'], $sender['name'])
            ->setTo($recipient)
            ->setBody($body)
            ->setContentType('text/html')
        ;

        $this->mailer->send($message);
    }
}
