<?php

namespace VisiDarbi\EmailTemplatingBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;


class EmailTemplateAdmin extends Admin
{
    /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;
   
    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }
    
    protected function getLocales() 
    {
        $locales = array();
        $country = $this->countryManager->getCurrentAdminCountry();
        foreach ($country->getLanguages() as $lng) {
            $locales[$lng->getLocale()] = $lng->getName();
        }
        
        return $locales;
    }
    
    protected function getEmailTemplates() 
    {
        return array(
            EmailTemplate::ID_REG_CONFIRMATION => 'Registration confirmation',
            EmailTemplate::ID_TMP_PASSWORD => 'Temporary password',
            EmailTemplate::ID_EMAIL_CHANGE_CONFIRMATION => 'E-mail change confirmation',
            EmailTemplate::ID_HIGHLIGHTING_OFFER => 'Advertisement highlighting offer',
            EmailTemplate::ID_USER_NEWSLETTER => 'User newsletter',
            EmailTemplate::ID_PASSWORD_UPDATED => 'Password updated',
            EmailTemplate::ID_SERVICE_ACTIVATED => 'Service activated',
            EmailTemplate::ID_SERVICE_DEACTIVATED => 'Service deactivated',
            EmailTemplate::ID_GOT_NEW_PAYMENT => 'Got new payment',
        );
    }
    

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {   
        $listMapper
            ->addIdentifier('id')
            ->add('locale', 'string', array(
                'label' => 'Language',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->getLocales()
            ))
            ->add('code', 'string', array(
                'label' => 'Template name',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->getEmailTemplates()
            ))
            ->add('subject', 'string')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('locale', 'doctrine_orm_choice', array('label' => 'Language',
                'field_options' => array(
                    'required' => false,
                    'choices' => $this->getLocales()
                ),
                'field_type' => 'choice'
            ))
            ->add('code', 'doctrine_orm_choice', array('label' => 'Template name',
                'field_options' => array(
                    'required' => false,
                    'choices' => $this->getEmailTemplates()
                ),
                'field_type' => 'choice'
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Email template')
                ->add('locale', 'choice', array(
                        'choices' => $this->getLocales(),
                        'required' => true,
                        'label' => 'Language',
                        'disabled' => true,
                ))
                ->add('code', 'choice', array(
                        'choices' => $this->getEmailTemplates(),
                        'required' => true,
                        'label' => 'Template name',
                        'disabled' => true,
                ))
                ->add('subject', null, array( 'required' => true ))
                ->add('body', 'email_body', array(
                    'required' => false, 
                    'attr' => array('class' => 'ckeditor span5'),
                    'config_name' => 'admin',
                    'placeholders' => $this->getSubject()->getPlaceholders()
                ))

            ->end()
        ;
    }
    
   

    public function getTemplate($name)
    {
        return parent::getTemplate($name);
    }
    
    public function createQuery($context = 'list')
    {
        $query = $this->getModelManager()->createQuery($this->getClass(), 't');
        $query->select('t');
        $query->where('t.country = :country');
        $query->setParameter(':country', $this->countryManager->getCurrentAdminCountry());
        $query->orderBy('t.id', 'DESC');
        
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }
    
    /*
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('EmailTemplatingBundle:EmailTemplateAdmin:placeholders.html.twig')
        );
    }
     * 
     */
}