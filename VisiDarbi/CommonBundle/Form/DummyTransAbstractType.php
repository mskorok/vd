<?php

namespace VisiDarbi\CommonBundle\Form;
use Symfony\Component\Form\AbstractType;

/**
 * This class implement dummy trans method to allow parsin of trnslations in form classes
 *
 * @author Aleksey
 */
abstract class DummyTransAbstractType extends AbstractType {

    protected  $translator = null;


    protected function trans($text, $raplacements = array(), $domain = 'messages') {

        if($this->translator == null) {
            return $text;
        }

        return $this->translator->trans($text, $raplacements, $domain);
    }

}

?>
