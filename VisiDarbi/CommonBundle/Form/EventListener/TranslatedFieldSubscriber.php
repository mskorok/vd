<?php
namespace VisiDarbi\CommonBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Util\PropertyPath;
use Symfony\Component\Validator\ValidatorInterface;

/**
 * Translated field subscriber
 *
 * @todo: add validations
 */
class TranslatedFieldSubscriber implements EventSubscriberInterface
{
    const SELF_KEY = 'self';

    /**
     * @var string
     */
    protected $transField;

    /**
     * @var string
     */
    protected $transClass;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string
     */
    protected $fieldType;

    /**
     * @var array
     */
    protected $fieldOptions;

    /**
     * @var array
     */
    protected $locales;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var bool
     */
    protected $translationsValidation;

    /**
     * Construct
     *
     * @param FormFactoryInterface $formFactory
     * @param ValidatorInterface $validator
     * @param array $locales
     * @param array $options
     */
    public function __construct(FormFactoryInterface $formFactory, ValidatorInterface $validator, array $locales, array $options)
    {
        // Required options
        $this->transField = $options['trans_field'];
        $this->transClass = $options['trans_class'];
        $this->fieldName = $options['field_name'];
        $this->fieldType = $options['field_type'];
        $this->fieldOptions = $options['field_options'];
        $this->translationsValidation = null;// $options['translations_validation'];

        $this->locales = $locales;
        $this->formFactory = $formFactory;
        $this->validator = $validator;
    }

    /**
     * Pre set data
     *
     * @param FormEvent $event
     * @return null
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $translations = $event->getData();

        // Add self
        $selfEntity = $form->getParent()->getData();

        if($translations) {
            if (!$selfEntity) {
                return;
            }

        

            $propertyPath = new PropertyPath($this->fieldName);

            $form->add($this->formFactory->createNamed(
                self::SELF_KEY,
                $this->fieldType,
                $selfEntity ? $propertyPath->getValue($form->getParent()->getData()) : null,
                array('label' => 'En') + $this->fieldOptions
            ));

            foreach ($this->locales as $locale) {
                $content = $this->getTransForLocale($translations, $locale);
                $form->add($this->formFactory->createNamed(
                    $locale,
                    $this->fieldType,
                    $content ? $content->getContent() : null,
                    $this->fieldOptions
                ));
            }
        } else {
            
            
            
            
            $propertyPath = new PropertyPath($this->fieldName);

            $form->add($this->formFactory->createNamed(
                self::SELF_KEY,
                $this->fieldType,
                null,
                array('label' => 'En') + $this->fieldOptions +array('property_path'=>false)
            ));

            foreach ($this->locales as $locale) {
                $content = $this->getTransForLocale($translations, $locale);
                $form->add($this->formFactory->createNamed(
                    $locale,
                    $this->fieldType,
                    null,
                    $this->fieldOptions+array('property_path'=>false)
                ));
            }            
            
        }          
        
    }

    /**
     * Post bind
     *
     * @param FormEvent $event
     */
    public function postBind(FormEvent $event)
    {
        $form = $event->getForm();

        // Get translations and entity
        $translations = $form->getData();
        $selfEntity = $form->getParent()->getData();

        if (!$selfEntity) {
            return;
        }

        foreach ($this->locales as $locale) {
            $content = $form->get($locale)->getData();

         //   $this->validateTranslatedContent($form->get($locale), $selfEntity, $content);

            if(!$trans = $this->getTransForLocale($translations, $locale)) {
                // Create new trans
                $transClass = $this->transClass;

                /** @var $translation \Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation */
                $translation = new $transClass($locale, $this->fieldName, $content);
                $translation->setObject($selfEntity);
                $translations[] = $translation;
            } else {
                $trans->setContent($content);
            }
        }

        // Unset string variables
        foreach ($translations as $trans) {
            if (!$trans instanceof $this->transClass) {
                
                $translations->removeElement($trans);
            }
        }

        // Set self property
        $selfData = $form->get(self::SELF_KEY)->getData();

  //      $this->validateTranslatedContent($form->get(self::SELF_KEY), $selfEntity, $selfData);

//        // Set data to self entity
//        $propertyPath = new PropertyPath($this->fieldName);
//        $propertyPath->setValue($selfEntity, $selfData);
    }

    /**
     * Validate translated content
     *
     * @param FormInterface $form
     * @param mixed $parentData
     * @param mixed $content
     */
    protected function validateTranslatedContent(FormInterface $form, $parentData, $content)
    {
        if ($this->translationsValidation) {
            // Validate translated content
            $constraintViolationList = $this->validator->validatePropertyValue(
                get_class($parentData),
                $this->fieldName,
                $content
            );

            if (count($constraintViolationList)) {
                foreach ($constraintViolationList as $constraintViolation) {
                    /** @var \Symfony\Component\Validator\ConstraintViolation $constraintViolation */
                    $form->addError(new FormError(
                        $constraintViolation->getMessageTemplate(),
                        $constraintViolation->getMessageParameters(),
                        $constraintViolation->getMessagePluralization()
                    ));
                }
            }
        }
    }

    /**
     * Get content for locale
     *
     * @param object $data
     * @param string $locale
     * @return mixed
     */
    protected function getTransForLocale($data, $locale)
    {
        if(empty($data)) {
            return null;
        }
        
        foreach ($data as $itemTrans) {
            if ($itemTrans instanceof $this->transClass) {
                /** @var $itemTrans \Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation */
                if ($itemTrans->getField() == $this->fieldName && $itemTrans->getLocale() == $locale) {
                    return $itemTrans;
                }
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => array('preSetData', 1000),
            FormEvents::POST_BIND => array('postBind', 1000)
        );
    }
}