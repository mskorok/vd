<?php

namespace VisiDarbi\CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;


class SearchAdvertisementType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder->add('what', 'genemu_jqueryautocomplete_text', array(
            'route_name' => 'visidarbi_autocomplete_what',
            'configs' => array('minLength' => 2, 'appendTo'=>'#searchadvertisement_what_box')
        ));
        $builder->add('where', 'genemu_jqueryautocomplete_text', array(
            'route_name' => 'visidarbi_autocomplete_where',
            'configs' => array('minLength' => 1, 'appendTo'=>'#searchadvertisement_where_box')
        ));
        
    }

    public function getName() {
        return 'searchadvertisement';
    }

}
