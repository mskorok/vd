<?php
namespace VisiDarbi\CommonBundle\Listener;
 
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
 
class ExceptionListener
{
    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    protected $router;
    
    public function __construct(UrlGeneratorInterface $router) 
    {
        $this->router = $router;
    }
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $reqUri = $event->getRequest()->getRequestUri();
        
        if ($exception instanceof NotFoundHttpException) {
            if (preg_match('/^\/(app_dev\.php\/)?(lv|ru|en|ee|lt)\/?(.*)/', $reqUri, $matches)) {
                $redirectUrl = $this->router->generate('index', array('_locale' => $matches[2]) );
            } else {
                $redirectUrl = $this->router->generate('index' );
            }
            $response = new RedirectResponse($redirectUrl);
            $event->setResponse($response);
        }
    }
}