$(document).ready( function(){
    
        var advertisementToShare = null;
	$.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase()); 
	var safari = false;
	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) safari = true;
	var ie = false;
	if(safari){$('body').addClass('safari');}
	if($.browser.chrome){$('body').addClass('chrome');}
	
	// Disable href-# jump-to-top problem
	$('body').delegate('[href="#"]', 'click', function(e){ 
		e.preventDefault();
	});
	
	/* CSS3_PIE */
	if (window.PIE) {
		$('.css3, .ui-datepicker').each(function() {
			PIE.attach(this);
		});
		$(window).scroll(function(){
			updatePIE()
		});
	}
	updatePIE = function () {
		if (window.PIE) {
			$('.css3, .ui-datepicker').each(function () {
				PIE.detach(this);
				PIE.attach(this);
			});
		}
    };
	/* CSS3_PIE */
	
	if (Function('/*@cc_on return document.documentMode===10@*/')()){
    $('body').addClass('ie10');
	}
	
	simpledrop();

	autocomplete();

	$('[data-edit-form]').each(function(){
		var th = $(this);
		$('.bttn.edit .orangebutton', th).click(function(){
			th.toggleClass('no_edit edit').find('input').removeAttr('disabled');
		});
		$('.bttn .cancel', th).click(function(){
			th.toggleClass('no_edit edit').find('input').attr('disabled', 'disabled');
		});
	});
    $('.radioblock .preview span').hover(function(){
        if(!$('body.ie8').size())$(this).next('.pop').stop(1,1).fadeIn(200);
        else $(this).next('.pop').show(0);
    }, function(){
        if(!$('body.ie8').size())$(this).next('.pop').stop(1,1).fadeOut(200);
        else $(this).next('.pop').hide(0);
    });

	$('.txtshadow').each(function(){
		$('span', this).clone().appendTo($(this)).addClass('sh');
	});

	$('.ef-input').efumoFormElements();

	$('.input.standart').click(function(){
		if(!$('input', this).is(':disabled'))$('input', this).focus();
	});

	$('.ef-selectbox').efumoSelectbox({
		slideTime: 100
	});

	$('#iframe').each(function(){
		var th = $(this);
		th.height($(window).height()-$('#header').height());
		$(window).resize(function(){
			th.height($(window).height()-$('#header').height());
		});
	});
        
	$('.checkall').each(function(){
		var th = $(this);
		if($('input', th).is(':checked'))th.parent().addClass('allcities');
		else th.parent().removeClass('allcities');
		th.click(function(){
			setTimeout(function(){
				if($('input', th).is(':checked'))th.parent().addClass('allcities');
				else th.parent().removeClass('allcities');
				updatePIE();
			}, 20);
		});
	});

	simplePopup();
	
	$('.tabsmenu').delegate('li:not(.sel) a', 'click', function(){
		$(this).closest('ul').find('.sel').removeClass('sel').end().end().closest('li').addClass('sel');
		/*var active = $(this);
		var tabid = active.attr('id');
		var tabid_last=$('.tabs li.sel a').attr('id');*/
		
		/*$('.'+tabid_last).fadeOut(100, function(){
			$('.'+tabid).fadeIn(100);
		});*/
	});
	
	$('.link_list .right .link_list').each(function(){
		var el = $(this);
		if(el.parent().height() > el.parent().prev().height()){
			$('<ul class="hidden"></ul>').appendTo(el);
			$(el).addClass('cols').find('ul:not(.hidden) li:nth-child(' + Math.ceil($('ul:not(.hidden) li', el).length / 2) + ')', el).nextAll().appendTo($('ul.hidden', el));
		}
	});
	
	$('.table').each(function(){
		$(this).delegate('.remove', 'click', function(e){e.stopPropagation();});
		$(this).delegate('tbody tr', 'click', function(e){
			if ($('a.hidden', this).length && !$(e.target).is('input, .remove, .copy, .edit') ) window.open($('a.hidden',this).attr('href'), $('a.hidden',this).attr('target'));
		});
	});
	
	$('.popup').delegate('.close', 'click', function(){
		$(this).closest('.popup').hide();
		if($(this).closest('.filter_popup').size()) $('#filter_popup .filter_popup').removeClass('v2').removeAttr('style');
	});
	
	JA_tabs();
	JA_tabs2();
	
	/*$('.deals').delegate('.box', 'click', function(e){
		if($('a',this).length) window.location = $('a',this).attr('href');
	});*/
	
    /*$('.deals .item H3').each(function(){
        clamp( $(this).get(0), 3);
    });*/
    
	jQuery('.nonautoslide').Carousel({
		auto:true,
		speed:600
	});
	
        initSelectors();
	
	if($('[data-autosize]').size()){
		$('[data-autosize]').autosize();
	}
	$('.input.textarea').click(function(){
		$('textarea', this).focus();
	});
	
	$('.form .upload').delegate('.button', 'click', function(){
		$(this).closest('.upload').find('input').click();
	});
	
	$('[data-clone]').live('focus', function(){
		var this_el = $(this).closest('.input'),
			this_elems = $('[data-index="'+$(this).data('index')+'"][data-nr="'+$(this).data('nr')+'"]').closest('.input');
		if(this_el.is(':last-child') && this_el.prev('.input').find('input').val() != '') {
			this_elems.each(function(){
				$(this).clone().appendTo($(this).parent()).addClass('added').removeClass('no_ok').removeClass('focus').find('input').val('').removeAttr('id').data('nr');
				$(this).next().find('input').attr('name', $('input', this).attr('data-name') + "[" + $(this).index() + "]");
				$(this).next().find('input').attr('id', $('input', this).attr('data-id') + $(this).index());
				$(this).next().find('input').attr('data-nr', $(this).index()).data('nr', $(this).index());
			});
		}
	}).live('blur', function(){
		$(this).closest('.input').removeClass('focus');
	});

	$('body').delegate('.input .deletethis', 'click', function(){
		if($(this).parent().find('[data-index]').size()){
			var thisinput = $(this).closest('.input'),
				block = thisinput.parent(),
				ind = $(this).parent().find('[data-index]').data('index'),
				nr = $(this).parent().find('[data-index]').data('nr');
			$('.input input[data-index="'+ind+'"][data-nr="'+nr+'"]').parent().remove();
			$('.added [data-index]').each(function(){
				var thp = $(this).closest('.input'),
					th = $(this);
				th.attr('name', th.attr('data-name') + "[" + (thp.index()-1) + "]");
				th.attr('id', th.attr('data-id') + (thp.index()-1));
				th.attr('data-nr', (thp.index()-1)).data('nr', (thp.index()-1));
			});
		}
	});
	
	contactsFormFields();
	
	if($('.input.calendar').size()){
		$('.input.calendar.interval input').each(function(){
			var th = $(this),
				from = $('#addadvertisementstep3_date_from').val(),
				to = $('#addadvertisementstep3_date_to').val();
			if(jQuery().datepick){
				function setDateRange(dates){
					$('.datepick-popup').addClass('css3');
					setTimeout(function(){
						$('.datepick-popup td .datepick-today').addClass('always-selected');
						$('.datepick-popup .datepick-selected').addClass('really-selected').parent().prevAll('td').find('a').addClass('datepick-selected');
						$('.datepick-popup .really-selected').parent().parent().prevAll('tr').find('a').addClass('datepick-selected');
						if(!$('.datepick-popup .really-selected').size() && $('.datepick-popup .always-selected').size()){
							$('.datepick-popup .always-selected').addClass('datepick-selected').parent().nextAll('td').find('a').addClass('datepick-selected');
							$('.datepick-popup .always-selected').parent().parent().nextAll('tr').find('a').addClass('datepick-selected');
						}
					}, 10);
				}
				function setHiddenDate(){
					$('#addadvertisementstep3_date_to').val($(th).val());
				}
				$(this).datepick({firstDay: 1, dateFormat:'yyyy-mm-dd', minDate: from, maxDate: to, validateDateMinMax:true, changeMonth: false, pickerClass: 'noPrevNext', showAnim: 'slideDown', showSpeed:0, onShow:function(dates){setDateRange(dates); updatePIE();}, onClose:function(dates){setHiddenDate(); updatePIE();}} );
				$(this).datepick('setDate', to);
				$(this).prev().prev().html(from);
				$('body').delegate('.datepick-popup td a','mouseenter mouseleave', function(event) {
				    if( event.type === 'mouseenter' ){
				    	$('.datepick-popup .datepick-selected').removeClass('datepick-selected');
				    	$('.datepick-popup .datepick-highlight').addClass('datepick-selected').parent().prevAll('td').find('a').addClass('datepick-selected');
						$('.datepick-popup .datepick-highlight').parent().parent().prevAll('tr').find('a').addClass('datepick-selected');
				    }else{
				    	$('.datepick-popup .datepick-selected').removeClass('datepick-selected');
				    	$('.datepick-popup .really-selected').addClass('datepick-selected').parent().prevAll('td').find('a').addClass('datepick-selected');
						$('.datepick-popup .really-selected').parent().parent().prevAll('tr').find('a').addClass('datepick-selected');
				    }
				});
			}
		});
	}
	
	formFields();
	
	$('.tooltip').live('mousemove', function(event) {
		var tt = document.getElementById('hint');
		if ((tt.style.display != 'block') && ($(this).next('.tooltip_text').length)) { 
			$('#hint .txt').append($(this).next('.tooltip_text').html());
			tt.style.display = 'block';
		}
		var oh = tt.offsetHeight,
			t = $(this).offset().top,
			l = $(this).offset().left,
			w = $('#hint').outerWidth();
		$('#hint').css({top: t - oh - 8, left: l - w / 2 + $(this).width() / 2});
	}).live('mouseout', function(){
		$('#hint').hide().find('.txt').html('');
	});
	
	$('.button .plane').each(function(){
		$(this).closest('.button').on('click', function(){
			$('#mailing_popup').show();
		});
	});
	
	mailingPopup();
	saveFn();
	owPopups();
	
	/*
	*	Script goes here
	*/
	$('.autocomplete input').keydown(function(e){
		if(e.keyCode==13 && !$(this).closest('.autocomplete').find('.ui-autocomplete a.ui-state-focus').size()) submitSearch();
	});

    $('#helper').each(function(){
        var th = $(this),
            btn = $('.infobutton', th),
            x = $('.close', th),
            pop = $('.helppop', th);
        btn.click(function(){pop.fadeToggle(200);});
        x.click(function(){pop.fadeToggle(200);});
        $(document).click(function (e){
            if (th.has(e.target).length === 0)pop.fadeOut(200);
        });
    });
	
	$('.filters .ef-input.checkbox input').each(function(){
		$(this).removeAttr('disabled');
	})
	
	$('.choice_type .list').delegate('.orangebutton.auth', 'click', function(){
		var hint = $('#login_hint'), most = 0;
		if(!hint.is(':visible')) hint.show();
		if($(this).closest('.most').size()) most = 10;
		var tt = document.getElementById('login_hint').offsetHeight,
			el = $(this).closest('section');
		hint.css({top: el.position().top + $(this).position().top - tt + 10 - most, left: el.position().left + (el.width() - hint.outerWidth()) / 2});
		
		hint.delegate('.close', 'click', function(e){
			hint.hide();
		});
	});

	$('#tag-cloud a').on('click',function(e){
		e.preventDefault();

		var kwType = $(this).attr('type');
		var kwVal = $(this).text();
		var where = kwType == "where" ? kwVal : "";
		var what = kwType == "what" ? kwVal : "";

		var request_url = Routing.generate('visidarbi_advertisement_search');

		$.post(
			request_url,
			{ 'where' : where, 'what' : what },
			function(results){
				if(results.status == 'true'){
					document.location.href = results.link;
				}
			}
		);

	});
	if($.cookie('E_Privacy_Directive_EU') == 'forbidden'){
		disable_cookie();
	} else if ($.cookie('E_Privacy_Directive_EU') != 'allowed'){
		$('#wr-cookies').show();
		$('#cookie-forbidden').click(function(){
			$.cookie('E_Privacy_Directive_EU', 'forbidden', { expires: 365, path: '/' });
			disable_cookie();
			$('#wr-cookies').hide();
		});
		$('#cookie-notice').click(function(){
			$.cookie('E_Privacy_Directive_EU', 'allowed', { expires: 365, path: '/' });
			$('#wr-cookies').hide();
		})
	}

} );

$(window).load(function(){
	wrapperHeight();
	equalHeight($('#footer .col'));
	equalHeight($('.openwork, #openwork'));
	equalHeight($('#contacts .left, #contacts form'));
	if($('[data-scrollbox]').size())scrollBox();
	if($('[data-scrolltabs]').size())scrollTabs();
	
	$('.deals').each(function(){
		equalHeight($('.box:first-child', this));
		equalHeight($('.box + .box', this));
	});
	equalHeight($('.choice_type .list h2'));
});

$(window).resize(function(){
	wrapperHeight();
	equalHeight($('.openwork, #openwork'));
	equalHeight($('.choice_type .list h2'));
	equalHeight($('#contacts .left, #contacts form'));
	contactsFormFields();
	$('.choice_type #login_hint').hide();
});

/*
*	Functions goes here
*/

function equalHeight(group) {
	var tallest = 0;
	group.css('min-height', '').each(function() {
		var thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	}).css('min-height', tallest+'px');
}

function simpledrop(){
	$('.simpledrop:not(.lang, .user)').each(function(){
		var th = $(this),
			dt = $('dt a', th),
			ul = $('ul', th);

		dt.click(function(){
			if(!$(th).hasClass('active')){
				$('ul', th).stop().slideDown(200, function(){$(th).addClass('active');});
			} else {
				$('ul', th).stop().slideUp(200, function(){$(th).removeClass('active');});
			}
		});
		$(document).mouseup(function (e){
			if (dt.has(e.target).length === 0){
				ul.stop().slideUp(200, function(){th.removeClass('active');});
			}
		});
	}); 
	$('.simpledrop.lang, .simpledrop.user').each(function(){
		var th = $(this),
			dt = $('dt a', th),
			ul = $('ul', th);
		ul.height('auto');
		var hh = ul.height();
		ul.css('display', 'block').height(0);

		dt.click(function(){
			if(!$(th).hasClass('active')){
				$('ul', th).stop().animate({height:hh}, 200, function(){$(th).addClass('active');});
			} else {
				$('ul', th).stop().animate({height:0}, 200, function(){$(th).removeClass('active');});
			}
		});
		$(document).mouseup(function (e){
			if (dt.has(e.target).length === 0){
				ul.stop().animate({height:0}, 200, function(){th.removeClass('active');});
			}
		});
	});
}

function wrapperHeight(){
	$('#wrapper .content').css('min-height', ( $(window).height() - $('#header').height() - $('#footer').height() ) );
}

function simplePopup(){
	var ie8 = $('body.ie8').size();
	$('.simplepopup').each(function(){
		var th = $(this),
			pop = $('.pop:not(#recovery)', th),
			dt = $('dt a', th);
		dt.click(function(){
			if(!$(this).hasClass('active')){
				if(!ie8)pop.fadeIn(200);
				else pop.css('right', '0');
				setTimeout(function(){
					dt.addClass('active');
				}, 300);
			} else {
				if(!ie8)pop.fadeOut(200).next().fadeOut(200);
				else pop.css('right', '9999px').next().css('right', '9999px');
				setTimeout(function(){
					dt.removeClass('active');
				}, 300);
			}
		});
		$('.forgot a', pop).click(function(){
			if(!ie8)pop.fadeOut(200, function(){pop.next().fadeIn(200);});
			else pop.css('right', '9999px').next().css('right', '0');
		});
		$(document).click(function (e){
			if (th.has(e.target).length === 0){
				if(!ie8)pop.fadeOut(200).next().fadeOut(200);
				else pop.css('right', '9999px').next().css('right', '9999px');
				setTimeout(function(){
					dt.removeClass('active');
				}, 300);
			}
		});
	});
}

function scrollBox(){
	$('[data-scrollbox]').each(function(){
		var th = $(this);
		var minTop = th.offset().top;
		var thHeight = th.outerHeight(true)-20; /* with this number you can regulate block jumping when reaching bottom */
		var scrollfield = th.closest('[data-scrollholder]');
		var maxBottom = scrollfield.offset().top + scrollfield.height() - thHeight;
		
		var wTop = $(window).scrollTop();
		
		th.width(th.width());
		
		if(wTop>minTop && th.css('position')!='fixed' )th.css({
			position: 'fixed',
			top: '0',
			bottom: ''
		});
		if(th.offset().top > maxBottom)th.css({
			position: 'absolute',
			top: '',
			bottom: 0
		});
		if(th.offset().top<minTop || $(window).scrollTop() < minTop)th.css({
			position: '',
			top: '',
			bottom: ''
		});

		
		$(window).scroll(function(){
			var wTop = $(window).scrollTop();
			var maxBottom = scrollfield.offset().top + scrollfield.height() - thHeight;
			if(wTop>minTop && th.css('position')!='fixed' )th.css({
				position: 'fixed',
				top: '0',
				bottom: ''
			});
			if(th.offset().top > maxBottom)th.css({
				position: 'absolute',
				top: '',
				bottom: 0
			});
			if(th.offset().top<minTop || $(window).scrollTop() < minTop)th.css({
				position: '',
				top: '',
				bottom: ''
			});
		});
	});
}

function scrollTabs(){
	$('[data-scrolltabs]').each(function(){
		var th = $(this);
		var minTop = th.offset().top;
		var thHeight = th.outerHeight(true); /* with this number you can regulate block jumping when reaching bottom */
		var scrollfield = $('#wrapper');
		var maxBottom = minTop + scrollfield.height();
		var wTop = $(window).scrollTop();
		
		th.width(th.width());
		
		if(wTop>minTop && th.css('position')!='fixed' )th.css({
			position: 'fixed',
			top: '0',
			bottom: ''
		});
		if(wTop > maxBottom)th.css({'margin-top': scrollfield.height() + minTop - wTop});
		else th.css({'margin-top': ''});
		if(th.offset().top<minTop || $(window).scrollTop() < minTop)th.css({
			position: '',
			top: '',
			bottom: ''
		});

		
		$(window).scroll(function(){
			var wTop = $(window).scrollTop();
			if(wTop>minTop && th.css('position')!='fixed' )th.css({
				position: 'fixed',
				top: '0',
				bottom: ''
			});
			if(wTop > maxBottom)th.css({'margin-top': scrollfield.height() + minTop - wTop});
			else th.css({'margin-top': ''});
			if(th.offset().top<minTop || $(window).scrollTop() < minTop)th.css({
				position: '',
				top: '',
				bottom: ''
			});
		});
	});
}



/* Efumo FormElements plugin */
(function( $ ){

	$.fn.efumoFormElements = function() { 
		return this.each(function() {
			var obj = $(this);

			if(obj.hasClass('text') || obj.hasClass('textarea')){
				var	inn = $('.inner', obj),
					def = $('.def', obj),
					inp = $('.realinput', obj);
			
				if(inp.val()=='')obj.addClass('default');
				else obj.removeClass('default');

				inp.focus(function(){
					focusFunc();
				}).blur(function(){
					blurFunc();
				}).mouseup(function(){
					def.height($(this).height());
				});
				def.click(function(){
					inp.focus();
				});
				obj.click(function(){
					inp.focus();
				});
				function focusFunc(){
					inn.height(inn.height());
					obj.removeClass('default').addClass('focus');
					inn.height('');
					if(!$('.after', obj).size() && $('body').hasClass('ie8')){
						$('<span class="after"></span>').appendTo($('.inner', obj));
						$('<span class="before"></span>').appendTo($('.inner', obj));
					}
				}
				function blurFunc(){
					inn.height(inn.height());
					if(inp.val()=='')obj.addClass('default');
					obj.removeClass('focus');
					inn.height('');
					if($('body').hasClass('ie8')){
						$('.after', obj).remove();
						$('.before', obj).remove();
					}
				}
			}

			if(obj.hasClass('checkbox') || obj.hasClass('radio')){
				var	inn = $('.inn', obj),
					lab = $('label', obj),
					inp = $('input', obj);
			
				if(inp.is(':checked'))obj.addClass('checked');
				else obj.removeClass('checked');

				inp.focus(function(){
					obj.addClass('focus');
					check();
				}).blur(function(){
					obj.removeClass('focus');
					check();
				}).keydown(function(e){
					if(e.keyCode=='13'){
						e.preventDefault();
						inp.click();
					}
				}).click(function(e){
					check();
				}).change(function(){
					if(obj.closest('.filters').size()){
						$('.filters .ef-input.checkbox input').each(function(){
							$(this).attr('disabled','disabled');
						});
					}
				});

				function check(){
					if(obj.hasClass('radio')){
						$('input[name="'+inp.attr('name')+'"]').each(function(){
							$(this).closest('.radio').removeClass('checked');
						});
					}
					if(inp.is(':checked'))obj.addClass('checked');
					else obj.removeClass('checked');
					
					$('.filter_popup .head').each(function(){
						/*console.log($(obj).find('input').attr('id').match(/\d+/));*/
						if(obj.hasClass('sel')) filterPopup(1);
						if(obj.hasClass('desel')) filterPopup(0);
					});
				}
			}
			
			/*$('.filters .ef-input.checkbox input').each(function(){
				$(this).removeAttr('disabled');
			});*/
		});   			
	};
})( jQuery );
/* end FormElements plugin */

function filterPopup(el){
	if((el == 1) && $('.ef-input.checkbox.sel').hasClass('checked')){
		$('.filter_popup .tab:visible .list').each(function(){
			$('input', this).attr('checked','checked').closest('.checkbox').addClass('checked');
			$('.ef-input.checkbox.desel').removeClass('checked').find('input').removeAttr('checked');
		});
	} else {
		if((el == 1) || $('.ef-input.checkbox.desel').hasClass('checked')){
			$('.filter_popup .tab:visible .list').each(function(){
				$('input', this).removeAttr('checked').closest('.checkbox').removeClass('checked');
				$('.ef-input.checkbox.sel').removeClass('checked').find('input').removeAttr('checked');
			});
		}
	}
}

function autocomplete(){
	$('.autocomplete').each(function(){
		var th = $(this),
			inp = $('input', th),
			ul = $('ul', th),
			ph = $('.ph', th);
		inp.focus(function(e){
			th.addClass('focus');
			if($('body').hasClass('ie10') && ph.size() && inp.val() == '') ph.show(0);
		}).blur(function(){
			th.removeClass('focus');
			if($('body').hasClass('ie10') && ph.size()) ph.hide(0);
			if($('#searchadvertisement_what').size() == 0) ul.slideUp(100);
		}).blur();
		$('dt', th).click(function(){
			$('input', th).focus();
		});

		if($('body').hasClass('ie10') || $('body').hasClass('ie8') || $('body').hasClass('ie9')){
			if(inp.val() == '') ph.hide(0);
			inp.keyup(function(){
				if(inp.val() != '') ph.hide(0);
				else ph.show(0);
			});
		}

		if($('#searchadvertisement_what').size() == 0){
			$('a', th).hover(function(){
				$('.hover', th).removeClass('hover');
				$(this).addClass('hover');
			},function(){
				$(this).removeClass('hover');
			}).mousedown(function(){
				inp.val($(this).text());
				ul.slideUp(100, function(){
					inp.focus();
				});
			});

			inp.keydown(function(e){
				if(e.keyCode==13 || e.keyCode==32){
					if($('.hover', th).size()){
						e.preventDefault();
						$('.hover', th).trigger('mousedown');
					}
				}
			}).keyup(function(e){
				var hov = $('.hover', th);
					nx = hov.parent().next('li').find('a'),
					pr = hov.parent().prev('li').find('a');
				if(!nx.size())nx = $('li:first a', ul);
				if(!pr.size())pr = $('li:last a', ul);
				if(inp.val().length > 0 && !ul.is(':visible') && e.keyCode!=9){
					ul.slideDown(100);
				}else if(inp.val().length == 0){
					ul.slideUp(100);
				}

				if(e.keyCode==38){
					hov.removeClass('hover');
					pr.addClass('hover');
				}
				if(e.keyCode==40){
					hov.removeClass('hover');
					nx.addClass('hover');
				}
			});
		}
	});
}


/* Selectbox plugin */
(function( $ ){

	$.fn.efumoSelectbox = function( options, method ) {
	
			// Fix bug with closing opened selectbox in IE when clicking outside
			$(document).bind('click.efumoSelectbox', function(e) {
				if ($(e.target).parents('.ef-selectbox').length > 0)
				{	return;}
				var o = options;
				methods.closeAll(o);
			});
	
			// Create some defaults, extending them with any options that were provided
            var settings = {
				slideTime: 200,
				selectBoxClass: '.ef-selectbox'
            };
             
            var options = $.extend(settings, options);	
         
			var methods = {
				close : function (options, obj, tabs){
					$(obj).find('div').slideUp(options.slideTime);
					$(obj).removeClass('focus');
					if($(obj).find('li.selected').length){
						if(tabs == true){
							var cur = 0,
								active = 0;
							$('[data-tabgroup-id]').each(function(){
								cur = $(obj).find('li.active a').data('call-form');
							});
							$(obj).find('li.active').removeClass('active').end().find('li.selected').removeClass('selected').addClass('active');
							$('[data-tabgroup-id]').each(function(){
								active = $(obj).find('li.active a').data('call-form');
								if(cur != active){
									$('[data-id=' + cur + ']').slideUp(300);
									$('[data-id=' + active + ']').slideDown(300);
								}
							});
						} else {
							$(obj).find('li.active').removeClass('active').end().find('li.selected').removeClass('selected').addClass('active');
						}
					}
					if($('dt a span', obj).text() == "" && $('input', obj).val() != "" && $('.filter_form .col.v2 .select').size()){
						var elv = $('input', obj).val(), el = $('li span#' + elv, obj);
						if($('input', obj).hasClass('country-selector')) getCities(elv);
					}
				},
				closeAll : function (options){
					$(options.selectBoxClass).find('div').slideUp(options.slideTime);
					$(options.selectBoxClass).removeClass('focus');					
					$(options.selectBoxClass).find('li.selected').removeClass('selected');
				},				
				open : function (options, obj){
					$(obj).find('div').slideDown(options.slideTime);					
					$(obj).addClass('focus');					
				},
				selectElement : function(selectedLi,selectedLiNext){									
					$(selectedLi).removeClass('selected');	
					$(selectedLiNext).addClass('selected');								
				},
				activeElement : function(selectedLi,selectedLiNext){									
					$(selectedLi).removeClass('active');	
					$(selectedLiNext).addClass('active');								
				},
				changeSelectedValue: function(obj){
					/*console.log('hh');*/
					var selected_id = $('.selected span').attr('id');
					var text = $('.selected span').html();
					$(obj).find('dt a span').html(text).attr('id', selected_id + '_sel');
					$(obj).find('input').val(selected_id);
                                        if($('input', obj).hasClass('country-selector')) {getCities(selected_id);
																					/*var ms = $('.city-selector').parent();
																					if($('.overview', ms).height() > ms.height()) $('.scrollbar', ms).show();
																					else $('.scrollbar', ms).hide();
																					ms.tinyscrollbar_update();*/
																				}
				}
			}
		 
      return this.each(function() {
				var o = options;
				var obj = this;
				var tabchange = 0;
				
				$(window).delegate('*:not(.focus)', 'click', function(e){
					if($(e.target).closest(o.selectBoxClass).length == 0){
						methods.closeAll(o);
					}
					return true
				});
				
				
				$(obj).delegate('dt a', 'click', function(e) {
					e.preventDefault();					
					if($(obj).hasClass('focus') == true){
						methods.close(o, obj, true);
					}else{
						methods.closeAll(o);
						methods.open(o, obj);			
					}
				});

				$(obj).delegate('dd a', 'click', function(e) {
					var thth = $(this);
					$('[data-tabgroup-id]').each(function(){
						if(!$(thth).parent().hasClass('active')){
							var active = $('.active a', obj);
							$('[data-id='+active.data('call-form')+']').slideUp(300);
							$('[data-id='+$(thth).data('call-form')+']').slideDown(300);
						}
					});
					
					e.preventDefault();
					methods.selectElement($(obj).find('li.selected'),$(this).parent());
					methods.activeElement($(obj).find('li.active'),$(this).parent());
					methods.changeSelectedValue(obj);
					methods.close(o, obj, false);
				});						

				$(obj).keydown(function(e) {			
					var selectedLi = $(obj).find('li.selected');
					
					/* Selectbox list slide */
					this.selectUp = function(next)
					{
						var topX = next.position();
						var ul = next.parents('ul:first');
						var top = topX.top + ul.scrollTop();

						if(topX.top <= 0 || topX.top >= ul.height()){
							ul.scrollTop(top);
						}
					}
					this.selectDown = function(next)
					{
						var topX = next.position();
						var ul = next.parents('ul:first');
						var top = topX.top - ul.height() + ul.scrollTop()+next.height();
						if(topX.top<0 || topX.top > ul.height()-next.height()){
							ul.scrollTop(top);
						}
					}
					/* end Selectbox list slide */
					
					var chcode = e.charCode || e.keyCode;

					switch (chcode) {

						// DOWN
						case 40:
							e.preventDefault();	
							methods.open(o, obj);									
							if(selectedLi.next().length){					
								methods.selectElement(selectedLi,selectedLi.next());
								methods.changeSelectedValue(obj);								
							}
							else{
								methods.selectElement(selectedLi,$(this).find('li:first-child'));
								methods.changeSelectedValue(obj);
							}
							
							next = $(this).find('.selected');
							this.selectDown(next);
							return false;
							
							break;
						
						// UP
						case 38:
							e.preventDefault();	
							methods.open(o, obj);	
							if(selectedLi.prev().length){
								methods.selectElement(selectedLi,selectedLi.prev());
								methods.changeSelectedValue(obj);								
							}
							else{
								methods.selectElement(selectedLi,$(this).find('li:last'));
								methods.changeSelectedValue(obj);
							}
							
							next = $(this).find('.selected');
							this.selectUp(next);
							return false;
							
							break;
						
						// TAB
						case 9:
							methods.close(o, obj, true);
							return true;
							break;
						
						// ENTER
						case 13:
							e.preventDefault();		
							methods.close(o, obj, true);
							break;

						// SPACE
						case 32:
							e.preventDefault();
							methods.close(o, obj, true);	
							break;
						
						// ESCAPE
						case 27: 
							methods.close(o, obj, true);	
							break;

						default:
							var letter = String.fromCharCode(chcode);
							letter = letter.toLowerCase();							

							var index = (selectedLi.index() == selectedLi.parent().find('li').length-1) ? 0 : selectedLi.index();

							var next;
							$(this).find('li').each( function(k){								
								if(k<index){
									return true;
								}
								var txt = $(this).text().toLowerCase().substring(0,1);
								
								if(txt == letter){									
									if($(this).hasClass("selected")){
										return true;
									}											
									$(selectedLi).removeClass('selected');	
									next = $(this).addClass('selected');
									return false;
								}
								
								if($(obj).find('li').length-1 == k){
									$(obj).find('li').each( function(k){
										var txt = $(this).text().toLowerCase().substring(0,1);
										if(txt == letter){
											$(selectedLi).removeClass('selected');	
											next = $(this).addClass('selected');											
											return false;
										}
									});
								}
							});
							/*methods.changeSelectedValue(obj);*/
							return false;
							break;
					}

				});
					
				$('li', obj).hover(function() {					
					$(this).addClass('selected');
				}, function() {
					$(this).removeClass('selected');
				});
				
				if($('dt a span', obj).text() == "" && $('input', obj).val() != "" && $('.filter_form .col.v2 .select').size()){
					var elv = $('input', obj).val(), el = $('li span#' + elv, obj);
					if($('li.active', obj).size() == 0) el.closest('li').addClass('active');
					$('dt a span', obj).html(el.text()).attr({id: elv + '_sel'}).closest('a').attr({title: el.text()});
					if($('input', obj).hasClass('country-selector')) getCities(elv);
				}
				
      });   			
	};
})( jQuery );
/* end Selectbox plugin */

function JA_tabs(){
	$('[data-tabgroup-id]').each(function(){
		var tabmenu = $('.tabmenu[data-tabgroup='+$(this).data('tabgroup-id')+']');
		var tabhold = $('.tabholder[data-tabgroup='+$(this).data('tabgroup-id')+']');
		var animation = $(this).data('animation');
		if(!animation)$('.tab:not(.active)', tabhold).slideUp(0);
		if(animation=="fade")$('.tab:not(.active)', tabhold).css('display', 'none');
		$('[data-call]', tabmenu).click(function(){
			if(!$(this).hasClass('active')){
				var active = $('.active', tabmenu);
				active.removeClass('active');
				$(this).addClass('active');
				if(!animation){
					var thth = $(this);
					$('[data-id='+active.data('call')+']').slideUp(300);
					$('[data-id='+$(thth).data('call')+']').slideDown(300);
				}else if(animation=="fade"){
					var thth=$(this);
					$('[data-id='+active.data('call')+']').fadeOut(150, function(){
						$('[data-id='+$(thth).data('call')+']').fadeIn(150);
						if(thth.closest('.filter_popup').size()){
							filterTabs();
							filterPopup();
						}
					});
				}
			}
		});
		$('.xx', tabmenu).click(function(e){
			e.stopPropagation();
		});
		$('.filters').each(function(){
			$('[data-filter]', this).click(function(){
				if(!$('[data-call='+$(this).data('filter')+']', tabmenu).hasClass('active')){
					$('[data-call]', tabmenu).removeClass('active').parent().removeClass('sel');
					$('[data-id]').hide();
					$('[data-call='+$(this).data('filter')+']', tabmenu).addClass('active').parent().addClass('sel');
					$('[data-id='+$(this).data('filter')+']').show();
				}
				$('#filter_popup').show();
				filterPopup();
				filterTabs();
				
				if($('.work .item.sel').size()){
					$('.work .item.sel').removeClass('sel');
					var ie8 = $('body.ie8').size(),
						pop = $('.mailing_popup'),
						hint = $('#login_hint');
					if(pop.is(':visible')){
						if(!ie8) pop.fadeOut(200);
						else pop.css({left: -9999});
					}
					if(hint.is(':visible')) hint.hide();
				}
			});
		});
	});
	function filterTabs(){
		var all = true;
		$('.filter_popup .tab:visible .list .checkbox').each(function(){
			if(!$(this).hasClass('checked')) all = false;
		});
		if(all) $('.ef-input.checkbox.sel').addClass('checked').find('input').attr('checked','checked');
		else $('.ef-input.checkbox.sel').removeClass('checked').find('input').removeAttr('checked');
	}
	function filterPopup(){
		var fp = $('#filter_popup .filter_popup');
		if($(window).height() < fp.height() + 185){
			fp.addClass('v2').css({top: $(window).scrollTop() + 185}).animate({top: 185}, 600);
			$('html, body').animate({scrollTop: 0}, 600); 
		} else if(fp.hasClass('v2')) fp.removeClass('v2').css({top: 185 - $(window).scrollTop()}).animate({top: 185}, 600, function(){fp.removeAttr('style');});
	}
}

function JA_tabs2(){
	jQuery('[data-tabgroups]').each(function(){
		var tabs = jQuery(this),
			intabs = jQuery('.tabscontent', this),
			menu = jQuery('[data-tabmenu='+jQuery(tabs).data('tabgroups')+']'),
			animation = jQuery(tabs).data('animation'),
			speed = jQuery(tabs).data('speed'),
			heightresize = jQuery(tabs).data('heightresize');
		if(heightresize){
			jQuery('[data-tabid]', tabs).css('display', 'none');
			jQuery('[data-tabid]:first', tabs).css('display', '').height('');
		}
		if(animation=='slide'){
			var active = jQuery('a.active', menu),
				act = jQuery(active).data('call');
			intabs.width((jQuery('[data-tabid]', tabs).size()*150)+'%');
			if(!jQuery('[data-call]:first', menu).hasClass('active'))jQuery('[data-tabid='+act+']', tabs).insertBefore(jQuery('[data-tabid]:first', tabs));
			tabs.data('onmove', 0);
			jQuery('[data-call]', menu).click(function(){
				if(!jQuery(this).hasClass('active') && tabs.data('onmove')=='0'){
					tabs.data('onmove', 1);
					var thisel = jQuery(this),
						thisid = jQuery(this).data('call');
						
                    try {
                        if (typeof beforeTabMove === 'function') {
                            beforeTabMove(thisid, tabs);
                        }
                    } catch (e) {}


					jQuery('[data-tabid]', tabs).css('height', '');
					jQuery('[data-tabid]', tabs).css('display', '');
					if(heightresize && jQuery('[data-tabid]:first', tabs).height()!=jQuery('[data-tabid='+thisid+']', tabs).height()){
						if(!jQuery('[data-tabshow]').size()) tabs.height(jQuery('[data-tabid]:first', tabs).height()).animate({height: jQuery('[data-tabid='+thisid+']', tabs).height()}, speed, function(){jQuery(this).css('height', '');});
						else {tabs.height(jQuery('[data-tabid='+thisid+']', tabs).height());jQuery('[data-tabid='+thisid+']', tabs).height('')}
					} else if(heightresize) tabs.height(jQuery('[data-tabid='+thisid+']', tabs).height());
					if(thisel.closest('li').prevAll('.active').size()){
						jQuery('[data-tabid='+thisid+']', tabs).insertAfter(jQuery('[data-tabid]:first', tabs));
						if(!jQuery('[data-tabshow]').size()) jQuery('[data-tabid]:first', tabs).animate({marginLeft: '-'+jQuery('[data-tabid]:first', tabs).width()+'px'}, speed, function(){
							jQuery(this).insertAfter(jQuery('[data-tabid]:last', tabs)).attr('style', '');
							if(heightresize){
								jQuery('[data-tabid]:not([data-tabid='+thisid+'])', tabs).height(jQuery('[data-tabid='+thisid+']', tabs).height())
								jQuery('[data-tabid]', tabs).css('display', 'none');
								jQuery('[data-tabid]:first', tabs).css('display', '').height('');
								jQuery(tabs).height('');
							}
							tabs.data('onmove', 0);
						});
						else {
							jQuery('[data-tabid]:first', tabs).insertAfter(jQuery('[data-tabid]:last', tabs)).attr('style', '');
							if(heightresize){
								jQuery('[data-tabid]:not([data-tabid='+thisid+'])', tabs).height(jQuery('[data-tabid='+thisid+']', tabs).height())
								jQuery('[data-tabid]', tabs).css('display', 'none');
								jQuery('[data-tabid]:first', tabs).css('display', '').height('');
								jQuery(tabs).height('');
							}
							tabs.data('onmove', 0);
						}
					}else{
						if(!jQuery('[data-tabshow]').size()){
							jQuery('[data-tabid='+thisid+']', tabs).insertBefore(jQuery('[data-tabid]:first', tabs));
							if(tabs.hasClass('v2')) jQuery('.tab', tabs).width(tabs.width());
							jQuery('[data-tabid='+thisid+']', tabs).css('margin-left', '-'+jQuery('[data-tabid]:first', tabs).width()+'px').animate({marginLeft: '0'}, speed, function(){
								if(heightresize){
									jQuery('[data-tabid]:not([data-tabid='+thisid+'])', tabs).height(jQuery('[data-tabid='+thisid+']', tabs).height())
									jQuery('[data-tabid]', tabs).css('display', 'none');
									jQuery('[data-tabid]:first', tabs).css('display', '').height('');
									jQuery(tabs).height('');
								}
								tabs.data('onmove', 0);
							});
						} else {
							jQuery('[data-tabid='+thisid+']', tabs).insertBefore(jQuery('[data-tabid]:first', tabs));
							if(tabs.hasClass('v2')) jQuery('.tab', tabs).width(tabs.width());
								if(heightresize){
									jQuery('[data-tabid]:not([data-tabid='+thisid+'])', tabs).height(jQuery('[data-tabid='+thisid+']', tabs).height())
									jQuery('[data-tabid]', tabs).css('display', 'none');
									jQuery('[data-tabid]:first', tabs).css('display', '').height('');
									jQuery(tabs).height('');
								}
								tabs.data('onmove', 0);
						}
					}
					active.removeClass('active').closest('li').removeClass('active');
					thisel.addClass('active').closest('li').addClass('active');
					active=thisel;
					/*if(!jQuery('body.ie8').size()){
						tabs.data('onmove', 0);
					} else {
						tabs.data('onmove', 0);
					}*/

					try {
                        if (typeof afterTabMove === 'function') {
                            afterTabMove(thisid, tabs);
                        }
                    } catch (e) {}
				}
			});
		}
		
		if(tabs.hasClass('v2')) jQuery('.tab', tabs).width(tabs.width());
		$(window).resize(function(){
			if(tabs.hasClass('v2')) jQuery('.tab', tabs).width(tabs.width());
		});
	});
}

/* Carousel plugin */
(function( jQuery ){

	jQuery.fn.Carousel = function( options, method ) {
			
			// Create some defaults, extending them with any options that were provided
      var settings = {
				timeout : 3000, // Time between carousel transitions
				pauseInterval: 2000, // Time for how long carousel transition pauses when user clicks on switch buttons
				speed: 400, // Transition speed
				//visibleElements: 3, // How many elements should be visible at once
				auto : true,
				tallest : true
      };
			
      var options = jQuery.extend(settings, options);
			
			var methods = {
				init : function(object) {
					var obj = object;
					if($('.item', obj).length <= 3) $('.switcher', obj).remove();
				}
			};
		 	
      return this.each(function() {
				var o = options,
					obj = this,
					timeout,
					onmove = 0,
					active = 1,
					elcount = 3,
					hover = 0;
				
				methods.init(obj);
				
				if(options.auto && $('.item', obj).length > elcount) autoSlide();
				
				jQuery(obj).delegate('.switcher a:not(.active)', 'click', function() {
					if(onmove==0){
						onmove=1;
						clearTimeout(timeout);
						var ind = $(this).index();
						var next_in=$(this).index() + 1;
						var this_sw=$('.switcher a:nth-child('+active+')', obj);
						var next_sw=$('.switcher a:nth-child('+next_in+')', obj);
						var diff = jQuery(next_sw).index() - jQuery(this_sw).index();
						if(diff > 0){
							var this_el=jQuery('.item:lt(' + elcount * Math.abs(diff) + ')', obj);
							jQuery(this_el).clone().appendTo(jQuery(this_el).parent());
							var el_width=jQuery(this_el).width()+parseInt(jQuery(this_el).css('margin-right'));
							el_width *= -1*elcount;
							active=next_in;
							jQuery('.item:lt(' + elcount * Math.abs(diff) + ')', obj).hide(0);
							jQuery('.item:lt(' + elcount+ ')', obj).show(0);
							slideItem(el_width, jQuery(this_el), jQuery(this_el), jQuery(this_sw), jQuery(next_sw), true);
						} else {
							var prev_el=jQuery('.item', obj).slice(-elcount * Math.abs(diff));
							var i = 0;
							jQuery('.item', obj).each(function(){
								if(i != elcount * Math.abs(diff)){
									jQuery('.item:nth-child(' + (jQuery('.item', obj).length - i) + ')',obj).clone().prependTo(jQuery('.item:last-child', obj).parent());
									i=i+1;
								}
							});
							var this_el=jQuery('.item:first-child', obj);
							var el_width=jQuery(this_el).width()+parseInt(jQuery(this_el).css('margin-right'));
							jQuery(this_el).parent().css({'margin-left': '-'+el_width*elcount+'px'});
							active=next_in;
							jQuery('.item:lt(' + elcount * Math.abs(diff) + ')', obj).hide(0);
							jQuery('.item:lt(' + elcount+ ')', obj).show(0);
							slideItem(0, jQuery(this_el), jQuery(prev_el), jQuery(this_sw), jQuery(next_sw), true);
						}
					}
				});
				
				function autoSlide(){
					clearTimeout(timeout);
					var this_el=jQuery('.item:lt(' + elcount + ')', obj);
					var this_sw=$('.switcher a:nth-child('+active+')', obj);
					timeout=setTimeout(function(){
						onmove=1;
						clearTimeout(timeout);
						if($(this_sw).next().size()){
							var next_sw=$(this_sw).next();
							active++;
						} else {
							var next_sw=$('.switcher a:first-child', obj);
							active=1;
						}
						var this_el=jQuery('.item:lt(' + elcount + ')', obj);
						jQuery(this_el).clone().appendTo(jQuery(this_el).parent());
						var el_width=jQuery(this_el).width()+parseInt(jQuery(this_el).css('margin-right'));
						el_width *= -1*elcount;
						slideItem(el_width, jQuery(this_el), jQuery(this_el), jQuery(this_sw), jQuery(next_sw), true);
					}, o.timeout);
				}
				
				function slideItem( marginLeft, itemToSlide, itemToDelete, switcherDeactiv, switcherActiv, autoplay ) {
					jQuery(itemToSlide).parent().stop().animate({'margin-left': marginLeft + 'px'}, o.speed, function(){
						onmove=0;
						jQuery(itemToSlide).parent().css({'margin-left': ''});
						jQuery(itemToDelete).remove();
						if(hover == 0){
							if(autoplay == true  && options.auto){
								setTimeout(function(){
									if(hover == 0) autoSlide();
								},o.pauseInterval);
							}
						}
						jQuery('.item', obj).removeAttr('style');
					});
					$(switcherDeactiv).removeClass('active');
					$(switcherActiv).addClass('active');
				}
				
				$(obj).on('hover', function(e){
					if(e.type == 'mouseenter'){
						hover=1;
						clearTimeout(timeout);	
					} else if(e.type == 'mouseleave'){
						hover=0;
						if(options.auto && $('.item', obj).length > elcount){
							setTimeout(function(){
								if(hover == 0) autoSlide();
							},o.pauseInterval);
						}
					}
				});
				
      });
	};

})( jQuery );
/* end Carousel plugin */

function contactsFormFields(){
	$('#contacts form .inputblock').each(function(){
		var fix = $(this).closest('form').find('.textarea').width() - 20,
			size = Math.floor(fix / 3);
		if($(this).not(':first-child')) $(this).width(size);
		else $(this).width(fix - size * 2);
	});
}

function formFields(){
	$('.input.standart:not(.select) input, .input.textarea textarea').each(function(){
		$(this).focus(function(){
			var obj = $(this).closest('.input');
			obj.addClass('focus');
			if(!$('.part', obj).size() && $(obj).hasClass('standart') && $('body').hasClass('ie8')) $('<span class="part"></span>').appendTo(obj);
		}).blur(function(){
			var obj = $(this).closest('.input');
			obj.removeClass('focus');
			if($('body').hasClass('ie8')) $('.part', obj).remove();
		});
	});
}

function mailingPopup(){
	var ie8 = $('body.ie8').size();
	$('.work .item').each(function(){
		var sel = 0,
			ind = 0,
			pop = $('.mailing_popup'),
			box = $('.work'),
			hint = $('#login_hint');
		$(this).delegate('.share', 'click', function(e){
			var obj = $(this).closest('.item');
                        advertisementToShare = obj.attr('id');
                        initSocialButtons(advertisementToShare);
			if(hint.is(':visible')){
				hint.hide();
				$('.work .item.sel').removeClass('sel');
			}
			if(obj.parent().find('.item.sel').size()){
				ind = obj.parent().find('.item.sel').index();
				if(ind == obj.index()) sel = 0;
				else sel = 1;
			}
			obj.toggleClass('sel');
			if(obj.hasClass('sel')){
				$('.realinput', pop).val('').closest('.ef-input').addClass('default');
				if(sel == 1){
					$('.item:nth-child(' + (ind + 1) + ')', box).removeClass('sel');
					if(!ie8) pop.fadeOut(200, function(){pop.css({top: $('.mailing_popup_content', obj).offset().top + 15}).fadeIn(200);});
					else pop.css({top: $('.mailing_popup_content', obj).offset().top + 15, left: 0});
				} else {
					pop.css({top: $('.mailing_popup_content', obj).offset().top + 15});
					if(!ie8) pop.fadeIn(200);
					else pop.css({left: 0});
                                        refershCaptcha();
				}
			} else {
				if(!ie8) pop.fadeOut(200);
				else pop.css({left: -9999});
			}
		});
		pop.delegate('.close', 'click', function(e){
			box.find('.item.sel').removeClass('sel');
			if(!ie8) pop.fadeOut(200);
			else pop.css({left: -9999});
		});
	});
}


function mailingPopupShowView(){
	var ie8 = $('body.ie8').size();
	$('.work .item').each(function(){
		var sel = 0,
			ind = 0,
			pop = $('.mailing_popup'),
			box = $('.work'),
			hint = $('#login_hint');
		$(this).delegate('.share', 'click', function(e){
                    
			var obj = $(this).closest('.item');
                        advertisementToShare = obj.attr('id');
                        console.debug(advertisementToShare);
                        initSocialButtons(advertisementToShare);                        
			if(hint.is(':visible')){
				hint.hide();
				$('.work .item.sel').removeClass('sel');
			}
			if(obj.parent().find('.item.sel').size()){
				ind = obj.parent().find('.item.sel').index();
				if(ind == obj.index()) sel = 0;
				else sel = 1;
			}
			obj.toggleClass('sel');
			if(obj.hasClass('sel')){

                            $('.share_id').val(obj.attr('id'));
                            
				$('.realinput', pop).val('').closest('.ef-input').addClass('default');
				if(sel == 1){
					$('.item:nth-child(' + (ind + 1) + ')', box).removeClass('sel');
					if(!ie8) pop.fadeOut(200, function(){pop.css({top: $('.mailing_popup_content', obj).offset().top + 15}).fadeIn(200);});
					else pop.css({top: $('.mailing_popup_content', obj).offset().top + 15, left: 0});
				} else {
					pop.css({top: $('.mailing_popup_content', obj).offset().top + 15});
					if(!ie8) pop.fadeIn(200);
					else pop.css({left: 0});
				}
			} else {
				if(!ie8) pop.fadeOut(200);
				else pop.css({left: -9999});
			}
		});
		pop.delegate('.close', 'click', function(e){
			box.find('.item.sel').removeClass('sel');
			if(!ie8) pop.fadeOut(200);
			else pop.css({left: -9999});
		});
	});
}

function saveFn(){
	var ie8 = $('body.ie8').size();
	$('.work .item').each(function(){
		var obj = $(this),
			pop = $('.mailing_popup'),
			hint = $('#login_hint'),
			tt = document.getElementById('login_hint');
		if($('.simpledrop.user').is(':visible')){
//			obj.delegate('.save', 'click', function(){
//				$(this).hide();
//				$('.saved', obj).show();
//				hideActive();
//			});
//			obj.delegate('.saved', 'click', function(){
//				$(this).hide();
//				$('.save', obj).show();
//				hideActive();
//			});
		} else {
			obj.delegate('.save', 'click', function(){
				if(!hint.is(':visible')) hint.show();
				hideActive();
				var oh = tt.offsetHeight,
					t = $(this).offset().top,
					l = $(this).offset().left,
					w = hint.outerWidth();
				obj.addClass('sel');
				hint.css({top: t - oh - 4, left: l - w / 2 + $(this).width() / 2 + 10});
			});
			hint.delegate('.close', 'click', function(e){
				$('.work .item.sel').removeClass('sel');
				hint.hide();
			});
		}
		function hideActive(){
			$('.work .item.sel').removeClass('sel');
			if(pop.is(':visible')){
				if(!ie8) pop.fadeOut(200);
				else pop.css({left: -9999});
			}
		}
	});
}

function owPopups(){
	$('.openwork .buttons').each(function(){
		var ie8 = $('body.ie8').size(),
			obj = $(this),
			pop = $('.mailing_popup'),
			hint = $('#login_hint'),
			tt = document.getElementById('login_hint');
		if($('.simpledrop.user').is(':visible')){
//			/*$('.button .save', obj).parent().on('click', function(){
//				$('.button.hidden', obj).removeClass('hidden');
//				$(this).addClass('hidden');
//			});
//			$('.button .saved', obj).parent().on('click', function(){
//				$('.button.hidden', obj).removeClass('hidden');
//				$(this).addClass('hidden');
//			});*/
		} else {
			$('.button .save', obj).parent().on('click', function(){
				if(!$('.popup_box').find(hint).size()) hint.appendTo($('.popup_box'));
				if(!hint.is(':visible')) hint.css({top: - hint.outerHeight() - 1}).show();
			});
			hint.delegate('.close', 'click', function(e){
				hint.hide();
			});
		}
		
		$('.button .share', obj).parent().on('click', function(e){ 
                    
			var obj = $('a.share');
                        advertisementToShare = obj.attr('data-id');
                        initSocialButtons(advertisementToShare);                    
			if(!$('.popup_box').find(pop).size()) pop.css({top: $(this).position().top + 18}).appendTo($('.popup_box'));
			if(!ie8) pop.fadeIn(200);
			else pop.css({right: 0});
		});
		pop.delegate('.close', 'click', function(e){
			if(!ie8) pop.fadeOut(200);
			else pop.css({right: 9999});
		});
	});
}

function updateTScrollbar(el){
	$('.scroll-pane').each(function(){
		if($('select.profession-selector', this).size() && el == 'prof'){
			if($(this).data('plugin'))$(this).tinyscrollbar_update();
			if($('.overview', this).height() > $('.viewport', this).height()){
					//Next line not works in some cases
	                $('.scrollbar', this).show();
			}else{
					//Next line not works in some cases
	                $('.scrollbar', this).hide();
			}
		}
		if($('select.city-selector', this).size() && el == 'city'){
			if($(this).data('plugin'))$(this).tinyscrollbar_update();
			if($('.overview', this).height() > $('.viewport', this).height()){
					//Next line not works in some cases
	                $('.scrollbar', this).show();
			}else{
					//Next line not works in some cases
	                $('.scrollbar', this).hide();
			}
		}
	});
}

function initSelectors() {
	$('.scroll-pane').each(function(){
		/*if(!$(this).hasClass('tiny')){
			$(this).jScrollPane({
				scrollbarWidth:10,
				verticalGutter:0
			});
			$(this).live('mousewheel DOMMouseScroll', function(e){
				var scrollTo = null;
				if(e.type == 'mousewheel') scrollTo = (e.originalEvent.wheelDelta * -1);
				else if(e.type == 'DOMMouseScroll') scrollTo = 40 * e.originalEvent.detail;
				if(scrollTo){
					e.preventDefault();
					$(this).scrollTop(scrollTo + $(this).scrollTop());
				}
			});
		} else {*/
			$(this).data('plugin', 'added').tinyscrollbar({
					axis: 'y',
					wheel: 40,
					sizethumb: 89
				});
			if($('.overview', this).height() <= $(this).height()){      
                       //Next line not works in some cases
                		$('.scrollbar', this).hide();     
			}
		/*}*/
		$('select option', this).each(function(){
			var this_el = $('li:nth-child(' + ($(this).index() + 1) + ')', $(this).closest('.scroll-pane'));
			
			if(typeof $(this).attr('selected') != 'undefined') this_el.addClass('sel');
			else this_el.removeClass('sel');
		});
		
			var oo = $(this).closest('.scroll-pane');
			if($('select', oo).hasClass('category-selector'))countrySelectorCheck();

			var obj = $(this);
			obj.keyup(function(e){
				e.preventDefault();
				var ee = jQuery.Event("keypress");
				ee.which = e.wich;
				ee.keyCode = e.keyCode;
				$('select', obj).focus();
				$('select', obj).trigger(ee);
				setTimeout(function(){
					$('ul li.sel', obj).removeClass('sel');
					$('ul li:nth-child('+ ($('option:selected', obj).index() + 1) +')', obj).addClass('sel');
					if($('select', obj).hasClass('category-selector'))countrySelectorCheck();
					obj.tinyscrollbar_update(0);
					var off = 0;
					if($('li.sel', obj).size() && $('.viewport', obj).size())off = $('li.sel', obj).offset().top + 30 - ( $('.viewport', obj).offset().top + $('.viewport', obj).height() );
					if(off<0)off=0;
					obj.tinyscrollbar_update(off);
				}, 20);
			});

		$('li', this).live('click', function(){
            var maxsel = $(this).closest('.multiselect').data('maxselect');
            var already = $(this).parent().find('.sel').size();
            if(!maxsel)maxsel=99999;

            $('select', obj).focus();

			var this_el = $('select option:nth-child(' + ($(this).index() + 1) + ')', obj);
			if(typeof obj.attr('data-nomultiple') != 'undefined'){                            
				var sel_li = obj.find('li.sel:first'),
					sel_el = $('select option:nth-child(' + (sel_li.index() + 1) + ')', obj);
                                function changeSelected(){
					this_el.attr('selected','selected');
					sel_li.removeClass('sel');
					sel_el.removeAttr('selected');
                                        if($('select', obj).hasClass('category-selector'))countrySelectorCheck();
                                        
                                        if($('select', obj).hasClass('category_id')) getCategoryId(this_el.val());
                                        

				}
                                if(typeof obj.attr('data-empty') != 'undefined'){
					if($(this).hasClass('sel')) this_el.removeAttr('selected');
					else changeSelected();
                                        $(this).toggleClass('sel');
                                } else {
                                	if(!$(this).hasClass('sel')) changeSelected();
                                        $(this).addClass('sel');
                                }
			} else {
				if(already<maxsel){
					if($(this).hasClass('sel')) this_el.removeAttr('selected');
					else this_el.attr('selected','selected');
					$(this).toggleClass('sel');
				}else{
					this_el.removeAttr('selected');
					$(this).removeClass('sel');
				}
				if($('select', obj).hasClass('category-selector'))countrySelectorCheck();
			}
		});
	});     
}	

function countrySelectorCheck() {

    if($('.category-selector').val()) {
        categorySelector($('.category-selector').val().join());
    }  else {
        categorySelector(0); 
    }    
}

function  submitSearch(){

	var where = $.trim($('.search-where').val());
    var what = $.trim($('.search-what').val());
  	
  	var request_url = Routing.generate('visidarbi_advertisement_search'); 

	$.post(
		request_url,
		{ 'where' : where, 'what' : what }, 
        function(results){
        	if(results.status == 'true'){
        		document.location.href = results.link;	
        	}                                                                 
    	}
    );

}
function disable_cookie(){
	window['ga-disable-UA-2904825-4'] = true;
	var dm = '.'+ location.host;
	$.cookie('_ga', '', { expires: 1, path: '/', domain: dm });
	$.cookie('_dc_gtm_UA-2904825-4', '', { expires: 1, path: '/', domain: dm });
}