<?php

namespace VisiDarbi\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use VisiDarbi\MobileBundle\Controller\MobileController;


/**
 * Description of FrontendController
 *
 * @author Aleksey
 */
class FrontendController extends Controller {
    
    
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    protected $countryManager;
    protected $em;
    protected $locale;

//    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
//        $this->container = $container;
//    }

    public function preExecute() {
        MobileController::isMobile($this->getRequest());
        $this->em = $this->getDoctrine()->getEntityManager();
    }
    
    public function getLocale(){
        return $this->get('request')->getLocale();
    }
    
    protected function getCountryManager() {

        if (!$this->countryManager) {
            $this->countryManager = $this->container
                    ->get('visidarbi.country_manager');
        }

        return $this->countryManager;
    }
    
    public function getCurrentCountry(){
        return $this->getCountryManager()->getCurrentCountry();
    }

    /**
     * Returns locales for current admin country
     * @return array
     */
    protected function getLocales() {
        return $this->getCountryManager()
                        ->getCurrentCountry()
                        ->getLocales();
    }
    
    //put your code here
}

?>
