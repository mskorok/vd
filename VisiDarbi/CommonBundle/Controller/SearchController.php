<?php

namespace VisiDarbi\CommonBundle\Controller;

use VisiDarbi\CommonBundle\Controller\FrontendController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use VisiDarbi\CommonBundle\Form\SearchAdvertisementType;
use VisiDarbi\StatisticsBundle\Entity\Search;
use Symfony\Component\HttpFoundation\JsonResponse;
use Cocur\Slugify\Slugify;
use JMS\DiExtraBundle\Annotation as DI;


/**
 * Description of SearchController
 *
 * @author Aleksey
 */
class SearchController extends FrontendController {

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;


    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;



    public function indexAction(Request $request) {

        $searchForm = $this->prepareSearcForm($request);

        $advButtons = $this->prepareAdvAddButtons();

        return $this->render('VisiDarbiCommonBundle:Search:index.html.twig', 
            array(  
                'search_form' => $searchForm->createView(), 
                'showAddAdvertisementButton' => $advButtons['showAddAdvertisementButton'],
                'showAddAdvertisementButtonUrl' => $advButtons['showAddAdvertisementButtonUrl']
            )
         );
    }

    public function startPageAction(Request $request) {
        $searchForm = $this->prepareSearcForm($request);

        $advButtons = $this->prepareAdvAddButtons();

        return $this->render('VisiDarbiCommonBundle:Search:startPage.html.twig', 
            array(
                'search_form' => $searchForm->createView(),
                'showAddAdvertisementButton' => $advButtons['showAddAdvertisementButton'],
                'showAddAdvertisementButtonUrl' => $advButtons['showAddAdvertisementButtonUrl']
            )
        );
    } 

    public function generarteSearchUrlAction(Request $request){

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index'), 302 );
        }
        
        $slugify = new Slugify();
        $session = $this->container->get('session');

        $result = array('status' => 'false');

        $what = $request->get('what');
        $where = $request->get('where');

        if(strlen($what) > 70 || strlen($where) > 70){
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('visidarbi_advertisement_list');

            return new JsonResponse($result);
        }

        $whatSlug = $slugify->slugify($what);
        $whereSlug = $slugify->slugify($where);

        $session->getFlashBag()->add('searchFields', array(
            'what' => array( 'slug' => $whatSlug, 'value' => $what ),  
            'where' => array( 'slug' => $whereSlug, 'value' => $where ),
            'manualSearch' => true  
        ));

        if($what || $where){

            $result['status'] = 'true';
            
            if( $what && $where ){
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search', array('what' => $whatSlug, 'where' => $whereSlug));
            }
            elseif( $where ){
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_where', array('where' => $whereSlug));
            }
            elseif( $what ){
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_what', array('what' => $whatSlug));   
            }
            
        }
        else{
            $result['status'] = 'true';
            $result['link'] = $this->generateUrl('visidarbi_advertisement_list');
        } 

        return new JsonResponse($result);
    }   

    
    public function autocompliteWhatAction(Request $request) {

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index'), 301 );
        }

        $value = $request->get('term');

        $output = array();

        $now = new \DateTime();
        $interval = new \DateInterval('P60D');
        $interval->invert = 1;
        $date = $now->add($interval);

        $keywords = $this->em->createQueryBuilder('s')
                ->select('s.keyword, COUNT(s.id) as key_kount')
                ->from('VisiDarbi\StatisticsBundle\Entity\Search', 's')
                ->andWhere("s.keyword_type = :type")
                ->andWhere("s.keyword LIKE :term")
                ->andWhere("s.created_at >= :created")
                ->orderBy('key_kount')
                ->orderBy('s.keyword')
                ->groupBy('s.keyword')
                ->setParameter('created', $date)
                ->setParameter('type', Search::KEYWORD_TYPE_WHAT)
                ->setParameter('term', "{$value}%")
                ->setMaxResults(60)
                ->getQuery();

        $keywords->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $keywords = $keywords->getResult();

        $output = array();
        foreach ($keywords as $k) {
            $output[] = $k['keyword'];
        }

        $response = new Response();
        $response->setContent(json_encode($output));

        return $response;
    }

    public function autocompliteWhereAction(Request $request) {

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index'), 301 );
        }

        $value = $request->get('term');

        $output = array();

        $qb = $this->em->createQueryBuilder('ac')
                ->select('ac.name')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'ac')
                ->andWhere("ac.country = :country")
                ->andWhere("ac.name LIKE :term")
                ->orderBy('ac.name')
                ->setParameter('term', "{$value}%")
                ->setParameter('country', $this->getCurrentCountry()->getId())
                ->setMaxResults(10)
                ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale());

        $countries = $query->getResult();

        foreach ($countries as $c) {
            $output[] = $c['name'];
        }

        $qb = $this->em->createQueryBuilder('ac')
                ->select('ac.name')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'ac')
                ->andWhere("ac.AdvertisementCountry = :country")
                ->andWhere("ac.name LIKE :term")
                ->orderBy('ac.name')
                ->setParameter('term', "{$value}%")
                ->setParameter('country', $this->getCurrentCountry()->getDeafultAdvertisementCountry()->getId())
                ->setMaxResults(10)
                ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale());

        $cities = $query->getResult();

        foreach ($cities as $c) {
            $output[] = $c['name'];
        }

        $response = new Response();
        $response->setContent(json_encode($output));

        return $response;
    }
    
    private function prepareSearcForm(Request $request) {
        
        $params = array();

        if ($request->request->get('searchWhat', null)) {
            $params['what'] = $request->request->get('searchWhat', null);
        }

        if ($request->request->get('searchWhere', null)) {
            $params['where'] = $request->request->get('searchWhere', null);
        }

        $searchForm = $this->createForm(new SearchAdvertisementType());
        $searchForm->setData($params);  
        
        return $searchForm;
        
    }

    private function prepareAdvAddButtons(){

        $showAddAdvertisementButton = true;
        $showAddAdvertisementButtonUrl = $this->generateUrl('visidarbi_advertisement_add_step1');

        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if( $user instanceof \VisiDarbi\UserBundle\Entity\User){

            if($user->isLegal()){

                $userRepo = $this->em->getRepository('VisiDarbiUserBundle:User');
                $userActivePaidPeriod = $userRepo->getCurrentActiveUserPeriod($user);  

                if($userActivePaidPeriod){
                    $showAddAdvertisementButtonUrl = $this->generateUrl('add_advertisement_by_period', array('id' => $userActivePaidPeriod->getId() ));
                }
            }
            elseif($user->isPrivate()){
                $showAddAdvertisementButton = false;  
            }
        }


        return array(
            'showAddAdvertisementButton' => $showAddAdvertisementButton,
            'showAddAdvertisementButtonUrl' => $showAddAdvertisementButtonUrl    
        );

    }

}

?>
