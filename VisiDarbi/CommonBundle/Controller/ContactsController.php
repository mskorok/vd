<?php

namespace VisiDarbi\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\DiExtraBundle\Annotation as DI;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\TranslationBundle\Annotation\Ignore;

class ContactsController extends Controller
{
    
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;
    
    /**
     * @var \SwiftMailer
     * @DI\Inject("mailer") 
     */
    protected $mailer;
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     * @DI\Inject("translator") 
     */
    protected $translator;
    
    
    /**
     * @Route("/contacts", name="contacts")
     * @Template()
     */
    public function formAction()
    {
        $message = $this->getMessage();
        
        $hasErrors = false;
        $errors = array();
        $form = $this->getContactForm($message);
        
        $recipient = $this->settingsManager->get('contacts.email_recipient');
        if (empty($recipient)) {
            throw new \Exception('Contact form recipient email not configured');
        }

        
        if ($this->request->isMethod('POST')) {
            $form->bind($this->request);
            if ($form->isValid()) {
                $data = $form->getData();
                
                $body = implode('', array(
                    '<p>Name: ' . $data['name'] . '</p>',
                    '<p>Phone: ' . $data['phone'] . '</p>',
                    '<p>Email: ' . $data['email'] . '</p>',
                    '<p>Message:<br/><br/>' . nl2br(htmlentities($data['message'], ENT_COMPAT, 'UTF-8')) . '</p>',
                ));
                
                $mailMessage = \Swift_Message::newInstance()
                    ->setSubject($this->settingsManager->get('contacts.email_subject'))
                    ->setFrom($data['email'], $data['name'])
                    ->setTo($recipient)
                    ->setBody($body)
                    ->setContentType('text/html')
                ;
                
                //$logger = new \Swift_Plugins_Loggers_EchoLogger();
                //$this->mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

                $this->mailer->send($mailMessage);
                
                $this->session->getFlashBag()->add('is_cont_success', true);
                return $this->redirect($this->generateUrl('contacts'));
                
            }
            else {
                $collector = $this->get('form_error_collector');
                $errors = $collector->collectErrors($form);
                $hasErrors = true;
            }
        }
                
        return array(
            'form' => $form->createView(),
            'has_errors' => $hasErrors,
            'errors' => $errors,
            'is_cont_success' => count($this->session->getFlashBag()->get('is_cont_success')) > 0,
        );
    }
    
    /**
     * @Route("/contacts/validate", name="contacts_validate", defaults={"_format"="json"}, options={"i18n"="false"})
     */
    public function validateAction() 
    {
        $message = $this->getMessage();
        $form = $this->getContactForm($message);
        
        $valid = false;
        $errors = array();
        $form->bind($this->request);
        if (! $form->isValid()) {
            $collector = $this->get('form_error_collector');
            $errors = $collector->collectErrors($form);
            $valid = true;
        }
        
        return new JsonResponse(
            array('valid' => $valid, 'errors' => $errors ) 
        );
    }
    
    protected function getMessage()
    {
        return array(
            'name' => '',
            'phone' => '',
            'email' => '',
            'message' => '',
        );
    }

    protected function getContactForm($message) 
    {
        $form = $this->createFormBuilder($message)
            ->add('name', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('[Sender]Name', array(), 'contacts'),
                'required' => true,
                'constraints' => array(
                    new NotBlank(array( 'message' => $this->translator->trans('Name must not be empty', array(), 'contacts')))
                ),
            ))
            ->add('phone', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Phone', array(), 'contacts'),
                'required' => true,
                'constraints' => array(
                    new NotBlank(array( 'message' => $this->translator->trans('Phone must not be empty', array(), 'contacts') ))
                ),
            ))
            ->add('email', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('E-mail', array(), 'contacts'),
                'required' => true,
                'constraints' => array(
                    new NotBlank(array( 'message' => $this->translator->trans('Email must not be empty', array(), 'contacts') )),
                    new Email(array( 'message' => $this->translator->trans('Email must contain valid e-mail address', array(), 'contacts') )),
                ),
            ))
            ->add('message', 'textarea', array(
                'label' => /** @Ignore */$this->translator->trans('Message', array(), 'contacts'),
                'required' => true,
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array( 'message' => $this->translator->trans('Message must not be empty', array(), 'contacts') ))
                ),
            ))
            ->getForm()
        ;
        
        return $form;
    }
    

}
