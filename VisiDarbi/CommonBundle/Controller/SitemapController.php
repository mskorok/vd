<?php

namespace VisiDarbi\CommonBundle\Controller;

use VisiDarbi\CommonBundle\Controller\FrontendController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use VisiDarbi\CommonBundle\Form\SearchAdvertisementType;
use VisiDarbi\StatisticsBundle\Entity\Search;

/**
 * Description of SitemapController
 *
 * @author Aleksey
 */
class SitemapController extends FrontendController {

    const AD_PER_SITEMAP = '10000';
    const SITEMAP_AD_OFFSET = 2;

    protected $urlsSet = array();
    protected $now;
    protected $response;
    protected $locales;
    protected $dateMod;
    protected $changeFreq;
    protected $priority;

    public function preExecute() {

        $this->now = new \DateTime();
        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'xml');
        $this->locales = $this->getLocales();

        $this->dateMod = $this->now->format('Y-m-d');
        $this->changeFreq = 'daily';
        $this->priority = '0.8';


        parent::preExecute();
    }

    public function indexAction(Request $request) {

        $this->urlsSet[] = $this->generateUrl('visidarbi_sitemap_common', array(), true);
        $this->urlsSet[] = $this->generateUrl('visidarbi_sitemap_pages', array(), true);


        $adCount = $this->getAdvertisementCount();

        $pages = ceil($adCount / self::AD_PER_SITEMAP);

        for ($i = 1; $i <= $pages; $i++) {
            $this->urlsSet[] = $this->generateUrl('visidarbi_sitemap_advertisements', array('page' => ($i + self::SITEMAP_AD_OFFSET)), true);
        }

        return $this->render('VisiDarbiCommonBundle:Sitemap:index.html.twig', array('units' => $this->urlsSet), $this->response);
    }

    public function commonUrlAction() {

        $routes = array('index', 'contacts', 'visidarbi_advertisement_list');

        foreach ($this->locales as $locale) {
            foreach ($routes as $route) {
                $this->urlsSet[] = array('lastmod' => $this->dateMod,
                    'changefreq' => $this->changeFreq,
                    'priority' => $this->priority,
                    'loc' => $this->generateUrl($route, array('_locale' => $locale), true)
                );
            }
        }

        return $this->render('VisiDarbiCommonBundle:Sitemap:list.html.twig', array('units' => $this->urlsSet), $this->response);
    }

    public function pagesAction() {

        $country = $this->getCurrentCountry();
        $router = $this->get('router');

        foreach ($this->locales as $locale) {

            $documents = $this->em->getRepository('VisiDarbiCMSBundle:Document')->findBy(array(
                'country' => $country,
                'locale' => $locale,
                'enabled' => true,
            ));
            foreach ($documents as $doc) {
                $url = $router->generate('document', array('_locale' => $locale, 'slug' => $doc->getSlug()), true);

                $this->urlsSet[] = array('lastmod' => $this->dateMod,
                    'changefreq' => $this->changeFreq,
                    'priority' => $this->priority,
                    'loc' => $url
                );
            }


            $pages = $this->em->getRepository('VisiDarbiCMSBundle:Page')
                    ->createQueryBuilder('p')
                    ->select('p')
                    ->leftJoin('p.documents', 'd')
                    ->where('p.country = :country')
                    ->andWhere('p.locale = :locale')
                    ->andWhere('p.enabled = 1')
                    ->andWhere('d.enabled = 1')
                    ->andWhere('d is NOT NULL')
                    ->setParameter(':country', $country->getId())
                    ->setParameter(':locale', $locale);

            $pages = $pages->getQuery()->getResult();

            foreach ($pages as $page) {
                $url = $router->generate('page', array('_locale' => $locale, 'slug' => $page->getSlug()), true);
                $this->urlsSet[] = array('lastmod' => $this->dateMod,
                    'changefreq' => $this->changeFreq,
                    'priority' => $this->priority,
                    'loc' => $url
                );
            }
        }

        return $this->render('VisiDarbiCommonBundle:Sitemap:list.html.twig', array('units' => $this->urlsSet), $this->response);
    }

    public function advertisementsAction() {

        $page = (int) $this->getRequest()->get('page', 3);

        $page = $page - self::SITEMAP_AD_OFFSET;

        $adCount = $this->getAdvertisementCount();

        $from = ($page - 1) * self::AD_PER_SITEMAP;

        if ($adCount < $from) {
            $from = 0;
        }

        $advertisements = $this->getDoctrine()
                ->getEntityManager()
                ->createQueryBuilder('advertisement')
                ->select("advertisement.publicId, advertisement.updated_at")
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')
                ->where('advertisement.country = :country')
                ->andWhere('advertisement.externalAdvertisement is NULL')
                ->andWhere('advertisement.status = :status')
                ->orderBy('advertisement.date_from', 'ASC')
                ->setParameter('status', \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_ACTIVE)
                ->setParameter('country', $this->getCurrentCountry())
                ->setFirstResult($from)
                ->setMaxResults(self::AD_PER_SITEMAP);


        $advertisements = $advertisements->getQuery()->getResult();

        $route = 'visidarbi_advertisement_show';

        foreach ($advertisements as $ad) {
            foreach ($this->locales as $locale) {
                $this->urlsSet[] = array('lastmod' => $ad['updated_at'],
                    //'changefreq' => $this->changeFreq,
                    'priority' => $this->priority,
                    'loc' => $this->generateUrl($route, array('_locale' => $locale, 'advertisementId' => $ad['publicId']), true)
                );
            }
        }

        return $this->render('VisiDarbiCommonBundle:Sitemap:list.html.twig', array('units' => $this->urlsSet), $this->response);
    }

    protected function getAdvertisementCount() {
        $advertisements = $this->getDoctrine()
                ->getEntityManager()
                ->createQueryBuilder('advertisement')
                ->select("count(advertisement.id) as ad_count")
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')
                ->where('advertisement.country = :country')
                ->andWhere('advertisement.externalAdvertisement is NULL')
                ->andWhere('advertisement.status = :status')
                ->orderBy('advertisement.date_from', 'ASC')
                ->setParameter('status', \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_ACTIVE)
                ->setParameter('country', $this->getCurrentCountry());

        return $advertisements = $advertisements->getQuery()->getSingleScalarResult();
    }

}

?>
