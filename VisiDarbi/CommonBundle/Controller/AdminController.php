<?php

namespace VisiDarbi\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

use JMS\SecurityExtraBundle\Annotation\Secure;

class AdminController extends Controller
{
    /**
     * @Route("/switch-country/{countryId}", name="visidarbi_admin_switch_country")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function swichCountryAction($countryId)
    {
        $country = $this->getDoctrine()->getEntityManager()->getRepository('VisiDarbiLocalePlaceBundle:Country')->find($countryId);
        if ($country === null) {
            throw new NotFoundHttpException('Invalid country ID');
        }
        
        $countryManager = $this->get('visidarbi.country_manager');
        /* @var $countryManager  \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface */
        
        $countryManager->setCurrentAdminCountry($country);
        
        return $this->redirect(
            $this->generateUrl('sonata_admin_dashboard')
        );
    }
    
    /**
     * @Route("/clear-cache", name="visidarbi_admin_clear_cache")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function clearCacheAction() 
    {
        $kernelDir = $this->get('service_container')->getParameter('kernel.root_dir');
        
        $cmd = 'php ' . $kernelDir . DIRECTORY_SEPARATOR . 'console cache:clear --env=' . $this->get('service_container')->getParameter('kernel.environment');
        $output = shell_exec($cmd);

        echo '<pre>';
        echo $output;
        echo PHP_EOL;
        die('Done');
        
        $this->get('session')->setFlash('sonata_flash_success', 'The cache was cleared successfully!');
        
        return $this->redirect(
            $this->generateUrl('sonata_admin_dashboard')
        );
    }
    
    /**
     * @Route("/extract-translations", name="visidarbi_admin_extract_translations")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function extractTranslationsAction() 
    {
        $countryManager = $this->get('visidarbi.country_manager');
        /* @var $countryManager  \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface */
        
        $kernelDir = $this->get('service_container')->getParameter('kernel.root_dir');
        
        $cmd = 'php ' . $kernelDir . DIRECTORY_SEPARATOR . 'console translation:extract ' . implode(' ', $countryManager->getCurrentAdminCountry()->getLocales()) . ' --config=app';
        $output = shell_exec($cmd);
        
        echo '<pre>';
        echo $output;
        echo PHP_EOL;
        die('Done');
        
        $this->get('session')->setFlash('sonata_flash_success', 'Translations was extracted successfully!');
        
        return $this->redirect(
            $this->generateUrl('sonata_admin_dashboard')
        );
    }
}