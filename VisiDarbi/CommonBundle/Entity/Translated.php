<?php

namespace VisiDarbi\CommonBundle\Entity;

/**
 * Description of Translated
 *
 * @author Aleksey
 */
class Translated {
    
    protected $translations;
    
    protected $translated;

    public function getTranslated($field, $locale, $defaultLocale = null) {

        $default = null;
        
        foreach ($this->translations as $t) {
            if ($t->getLocale() == $locale && $t->getField() == $field) {
                return $t->getContent();
            }

            if ($t->getLocale() == $defaultLocale && $t->getField() == $field) {
               $default = $t->getContent();
            }
        }

        if($default && $defaultLocale) {
            return $default;
        }
        
        return '';
    }
    
    public function getSlug($field, $locale, $defaultLocale = null) {

        $default = null;
        
        foreach ($this->translations as $t) {
            if ($t->getLocale() == $locale && $t->getField() == $field) {
                return $t->getSlug();
            }

            if ($t->getLocale() == $defaultLocale && $t->getField() == $field) {
               $default = $t->getSlug();
            }
        }

        if($default && $defaultLocale) {
            return $default;
        }
        
        return '';
    }    
}

?>
