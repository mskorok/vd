<?php
namespace VisiDarbi\CommonBundle\Doctrine;
//use Doctrine\ORM\Query\Node\FunctionNode;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class FieldOrder extends FunctionNode {

    public $field = null;
    public $fieldOrder = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser) {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->field = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->fieldOrder = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker) {
        return 'FIELD(' . trim($this->field->dispatch($sqlWalker),"'") . ', ' .trim($this->fieldOrder->dispatch($sqlWalker),"'") . ')';
    }

}