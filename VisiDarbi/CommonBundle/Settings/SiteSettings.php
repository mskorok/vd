<?php

namespace VisiDarbi\CommonBundle\Settings;

use VisiDarbi\SettingsBundle\Settings\SettingsInterface;
use VisiDarbi\SettingsBundle\Settings\SettingsBuilderInterface;

class SiteSettings implements SettingsInterface 
{
    public function buildSettings(SettingsBuilderInterface $settingsBuilder) 
    {
        $settingsBuilder
            ->in('seo')
                ->add('default_title', 'Default title', false, 'Default title', true)
                ->add('default_keywords', 'Default keywords', false, 'Default keywords', true)
                ->add('default_description', 'Default description', false, 'Default description', true)
            ->end()
            ->in('contacts')
                ->add('email_recipient', 'Email recipient', true)
                ->add('email_subject', 'Email subject', true, 'New message from VisiDarbi contact form')
                
                ->add('info_phone', 'Info / phone', false, '', true)
                ->add('info_address', 'Info / address', false, '', true)
                ->add('info_email', 'Info / email', false, '', true)
                ->add('info_fax', 'Info / fax', false, '', true)
                
                ->add('requisites_name', 'Requisites / name', false, '', true)
                ->add('requisites_vat_number', 'Requisites / VAT number', false, '', true)
                ->add('requisites_address', 'Requisites / address', false, '', true)
                ->add('requisites_bank', 'Requisites / Bank', false, '', true)
                ->add('requisites_iban', 'Requisites / IBAN', false, '', true)
                ->add('requisites_swift', 'Requisites / SWIFT', false, '', true)
                
            ->end()
            ->in('payments')
                ->add('pvn', 'PVN', true, '21.0')
            ->end()
            ->in('emails')
                ->add('sender_email', 'Email email', true, 'noreply@inbox.lv')
                ->add('sender_name', 'Email name', true, 'Visi darbi')
            ->end()
            ->in('site')
                ->add('name', 'Site name', true, 'VisiDarbi')
                ->add('date_format', 'Date format', true, 'd F, Y')
                ->add('adv_sources_count', 'Advertisment sources count', false, '0')
                ->add('startpage_company_count', 'Company count in startpage', true, '30')
                ->add('startpage_ext_adv_count', 'Ext. ad. count in startpage', true, '30')
                //->add('good_to_know_helper', 'Orange good to know button in edit, copy, highlight views', true, '30')
            ->end()
            ->in('prices')
                ->add('price_for_first_page', 'Advert price for first page', false,11.11)
                ->add('price_for_advert_highlighting', 'Price for advert highlighting', false,9.99)
                ->add('price_for_mobile_advert_highlighting', 'Price for mobile advert highlighting', false,9.99)
            ->end()       
                
            ->in('social')
                ->add('draugiem_url', 'Draugiem URL', true, 'http://www.draugiem.lv')
                ->add('facebook_url', 'Facebook URL', true, 'http://www.facebook.com')
                ->add('twitter_url', 'Ttwitter URL', true, 'http://www.twitter.com')
            ->end()
            ->in('tooltips')
                ->add('full_form_image', 'Visidarbi full form image', false, null, true, 'file')
                ->add('html_form_image', 'HTML form image', false, null, true, 'file')
                ->add('imagepdf_form_image', 'Picture/pdf form image', false, null, true, 'file')
                ->add('free_form_image', 'Free text input form image', false, null, true, 'file')
            ->end()
        ;
    }
}
