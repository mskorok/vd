<?php

namespace VisiDarbi\CommonBundle\Util;

use Symfony\Component\Translation\TranslatorInterface;

class FormErrorCollector 
{
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface 
     */
    private $translator;
    
    public function __construct(TranslatorInterface $translator) 
    {
        $this->translator = $translator;
    }
    
    public function collectErrors(\Symfony\Component\Form\Form $form, $flat = true)
    {
        $errors = $this->doCollect($form);
        if ($flat) {
            foreach ($errors as $key => $error) {
                $errors[$key] = $this->flatten($error);
            }
        }
        return $errors;
    }
    
    protected function doCollect(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $template = $error->getMessageTemplate();
            $parameters = $error->getMessageParameters();
            $pluralization = $error->getMessagePluralization();
            
            $message = $pluralization === null ?
                $this->translator->trans( /** @Ignore */ $template, $parameters, 'validators' ) :
                $this->translator->transChoice( /** @Ignore */ $template, $pluralization, $parameters, 'validators');

            //foreach($parameters as $var => $value){
            //    $template = str_replace($var, $value, $template);
            //}

            $errors[$key] = $message;
        }
        if ($form->hasChildren()) {
            foreach ($form->getChildren() as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = $this->doCollect($child);
                }
            }
        }
        return $errors;
    }
    
    protected function flatten($errors) 
    {
        if (! is_array($errors)) {
            return array($errors);
        }
        else {
            $arr = array();

            foreach ($errors as $error) {
                $arr = array_merge($arr, $this->flatten($error));
            }
            
            return $arr;
        }
    }
}