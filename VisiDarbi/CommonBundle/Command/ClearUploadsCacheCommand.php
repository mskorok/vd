<?php

namespace VisiDarbi\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ClearUploadsCacheCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('visidarbi:common:clear-upload-cache')
                ->setDescription('Clear uploads cache (logo).')
                ->addArgument(
                        'hours', InputArgument::OPTIONAL, '', 24
                )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $uploasConfig = $this->getContainer()->getParameter('upload_logo');

        $dir = __DIR__ . "/../../../../web/" . $uploasConfig['tmp_path'] . '/'; //dir absolute path       
        $this->clearFolder($dir, $input, $output);
        $dir = __DIR__ . "/../../../../web/" . $uploasConfig['cache_path'] . '/'; //dir absolute path       
        $this->clearFolder($dir, $input, $output);

        $output->writeln('Done.');
    }

    private function clearFolder($dir, $input, $output) {

        //$interval = strtotime('-1 hours'); //files older than 24hours
        $interval = strtotime('-' . $input->getArgument('hours') . ' hours'); //files older than 24hours

        foreach (glob($dir . "*") as $file) {
            if (filemtime($file) <= $interval) {
                $output->writeln('Delete: ' . $file);
                if (is_dir($file)) {
                    self::rrmdir($file);
                } else {
                    unlink($file);
                }
            }
        }
    }

    private static function rrmdir($dir) {
        foreach(glob($dir . '/*') as $file) {
            if(is_dir($file))
                self::rrmdir($file);
            else
                unlink($file);
        }
        
        rmdir($dir);
    }    
    
}