<?php

namespace VisiDarbi\CommonBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PostDemoDeploymentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('visidarbi:common:post-demo-deployment')
            ->setDescription('Does some DEMO env. specific deployment tasks.')
            ->addArgument(
                'hours',
                InputArgument::OPTIONAL,
                '',
                24
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('VisiDarbiLocalePlaceBundle:Country');
        
        $domains = array(
            'lv' => 'visidarbidemo.hostnet.lv',
            'lt' => 'visidarbidemo-lt.efumosoftware.lv',
            'ee' => 'visidarbidemo-ee.efumosoftware.lv',
        );
        
        foreach ($domains as $code => $domain) {
            $country = $repo->findOneBy(array('code' => $code));
            $country->setDomain($domain);
            $country->setUri('http://' . $domain);
            $em->persist($country);
        }
        
        $em->flush();
        
        $output->writeln('Done.');
    }
}