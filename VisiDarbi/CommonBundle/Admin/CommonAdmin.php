<?php

namespace VisiDarbi\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use JMS\TranslationBundle\Annotation\Ignore;

class CommonAdmin extends Admin {

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    protected $countryManager;
    protected $em;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC', // sort direction 
        '_sort_by' => 'id' // field name 
    );

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
        $this->container = $container;
    }

    protected function getCountryManager() {

        if (!$this->countryManager) {
            $this->countryManager = $this->container
                    ->get('visidarbi.country_manager');
        }

        return $this->countryManager;
    }

    /**
     * Returns locales for current admin country
     * @return array
     */
    protected function getLocales() {
        return $this->getCountryManager()
                        ->getCurrentAdminCountry()
                        ->getLocales();
    }

    /**
     * Return Entity manager
     * @return Doctrine\Bundle\DoctrineBundle\Registry;
     */
    protected function getEm() {

        if (!$this->em) {
            $this->em = $this->container->get('doctrine')->getEntityManager();
        }
        return $this->em;
    }

    public function getCurrentLocale(){
        
    }
    
    
    protected function getAllCountries() {
        
    }

    protected function getAllLocales() {
        
    }

    
    protected function getTranslatableFieldParam($entity, $field, $title, $required = true, $options = array()) {

        
//                ->add('name', 'a2lix_translations', array(
//                    'property_path'=>'translations',
//                    'locales' => array('lv', 'ru', 'en'),
//                    'required_locales' =>array('lv', 'ru', 'en'),
//                    'default_locale' => array('lv'),
//                    'required' => true,
//                    //'label'=>'Name'



$params = array(
            'property_path'=>'translations',
            'required' => $required,
            'label' => /** @Ignore */'Translations',
            'default_locale' => array($this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale()),
            'locales' => $this->getLocales(),
            'required_locales' => $this->getLocales(),
            //'field' => $field,
    'by_reference' => false
            );

        foreach ($options as $k => $v) {
            $params[$k] = $v;
        }

        return $params;
    }

    public function createQuery($context = 'list') {

        if (!method_exists($this->getClass(), 'getcountry')) {
            return parent::createQuery($context);
        }

        $query = $this->getModelManager()->createQuery($this->getClass(), 'o');
        $query->select('o');
        $query->where('o.country = :country');
        $query->setParameter(':country', $this->getCountryManager()->getCurrentAdminCountry());

        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

    public function prePersist($object) {
        if (method_exists($this->getClass(), 'getcountry')) {
            $object->setCountry(
                    $this->getCountryManager()->getCurrentAdminCountry()
            );
        }
    }
    
    
    public function sortByTranslations($q, $fieldName = 'name') {

        return $q->join('fms.translations', 't')
        ->where('t.locale = :locale')
        ->andWhere('t.field = :field_name')
        ->setParameter('locale',$this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale())
        ->setParameter('field_name', $fieldName);
                                
    }
    
}

?>
