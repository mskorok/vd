<?php

namespace VisiDarbi\CommonBundle\Twig;

use Twig_Extension;
use Twig_Filter_Method;

class ColumnsExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            'columns' => new Twig_Filter_Method($this, 'columns'),
        );
    }

    /**
     * @param array $input
     * @param int $size
     *
     * @return mixed
     */
    function columns($input, $size) {
        
        $num = ceil((count($input))/$size);
        return array_chunk($input, ($num < 1)?1:$num);
        
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName() {
        return 'columns';
    }

}

?>