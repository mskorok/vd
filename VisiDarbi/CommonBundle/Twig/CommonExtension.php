<?php

namespace VisiDarbi\CommonBundle\Twig;

class CommonExtension extends \Twig_Extension {

    /**
     * EntityManager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    protected static $advertisementsInCategories;
    protected static $advertisementsInProfession;
    protected static $tempGlobalVars = array();

    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        $this->conn = $em->getConnection();
    }

    public function getFilters() {
        return array(
            'ceil' => new \Twig_Filter_Method($this, 'ceil'),
            'advertisementFilterUrlStr' => new \Twig_Filter_Method($this, 'advertisementFilterUrlStr'),
        );
    }

    public function getFunctions() {
        return array(
            'vendor_info' => new \Twig_Function_Method($this, 'vendoInfo'),
            'set_global_val' => new \Twig_Function_Method($this, 'setGlobalVal'),
            'get_global_val' => new \Twig_Function_Method($this, 'getGlobalVal'),
            'advertisementDateMark' => new \Twig_Function_Method($this, 'advertisementDateMark'),
            'advertisementsCountInCategory' => new \Twig_Function_Method($this, 'advertisementsCountInCategory'),
            'advertisementsCountInProfession' => new \Twig_Function_Method($this, 'advertisementsCountInProfession'),
        );
    }

    public function ceil($number) {
        return ceil($number);
    }

    public function advertisementFilterUrlStr($value, $type) {
        //format: cities + '|'+ category + '|' + companies + '|' + resources + '|' + profession +  '|' country

        $filter = array('city' => '0',
            'position' => '0',
            'company' => '0',
            'resource' => '0',
            'profession' => '0',
            'country' => '0');

        $filter[$type] = $value;

        $filterStr = implode('|', $filter);

        return base64_encode($filterStr);
    }

    public function advertisementDateMark($date) {

        $dateToCheck = new \DateTime();
        $dateToCheck->add(new \DateInterval('P3D'));

        if ($dateToCheck >= $date) {
            return true;
        }

        return false;
    }

    public function vendoInfo($countryCode, $localeCode) {
        $urls = array(
            'lv' => array(
                'lv' => 'www.efumo.lv',
                'ru' => 'www.efumo.lv/ru',
                'en' => 'www.efumo.lv/en',
            ),
            'lt' => array(
                'lt' => 'www.efumo.lt',
                'ru' => 'www.efumo.lt/ru',
                'en' => 'www.efumo.lt/en',
            ),
            'ee' => array(
                'ee' => 'www.efumo.ee',
                'ru' => 'www.efumo.ee/ru',
                'en' => 'www.efumo.ee/en',
            ),
        );

        $url = isset($urls[$countryCode][$localeCode]) ? 'http://' . $urls[$countryCode][$localeCode] : null;

        return array(
            'title' => 'Efumo',
            'url' => $url,
        );
    }

    public function getGlobalVal($varName) {

        if (isset(self::$tempGlobalVars[$varName])) {

            return self::$tempGlobalVars[$varName];
        }

        return null;
    }

    public function setGlobalVal($varName, $value) {

        self::$tempGlobalVars[$varName] = $value;
    }

    public function getName() {
        return 'visidarbi_common_extension';
    }

    /**
     * Count advertisements in category
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country
     */
    public function advertisementsCountInCategory($category) {

        $countryId = $category->getCountry()->getId();

        if (empty(self::$advertisementsInCategories[$countryId])) {

            $q = $this->em->createQueryBuilder()
                    ->select('COUNT (a.id) as ad_count, c.id as category_id')
                    ->from('\VisiDarbi\ProfessionBundle\Entity\Category', 'c')
                    ->join('c.Advertisements', 'a')
                    ->where('a.status = :status')
                    ->andWhere('a.country = :country')
                    ->setParameter('status', \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_ACTIVE)
                    ->setParameter('country', $countryId)
                    ->groupBy('c.id');

            $adCount = $q->getQuery()->getResult();

            foreach ($adCount as $v) {
                self::$advertisementsInCategories[$countryId][$v['category_id']] = $v['ad_count'];
            }
        }

        if (isset(self::$advertisementsInCategories[$countryId][$category->getId()])) {
            return self::$advertisementsInCategories[$countryId][$category->getId()];
        }

        return 0;
    }

    /**
     * Count advertisements in category
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $profession
     */
    public function advertisementsCountInProfession($profession, $locale) {

        $categoryObj = $profession->getCategory();
        $countryId = $categoryObj->getCountry()->getId();  
        $categoryId = $categoryObj->getId();      
        
        $slug = $profession->getTranslations();

        foreach ($slug as $s) {
            if ($s->getLocale() == $locale) {
                $slug = $s->getSlug();
                break;
            }
        }

        $countryIdLocale = $countryId.'_'.$locale;
        if (empty(self::$advertisementsInProfession[$countryIdLocale])) {

            $q = $this->em->createQueryBuilder()
                    ->select('COUNT (a.id) as ad_count, pt.slug as slug')
                    ->from('\VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                    ->join('p.Advertisements', 'a')
                    ->join('p.translations', 'pt')
                    ->where('a.status = :status')
                    ->andWhere('a.country = :country')
                    ->andWhere('pt.locale = :locale')
                    ->andWhere('p.category = :category_id')
                    ->setParameter('status', \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_ACTIVE)
                    ->setParameter('country', $countryId)
                    ->setParameter('locale', $locale)
                    ->setParameter('category_id', $categoryId)
                    ->groupBy('pt.slug')
            ;

            $adCount = $q->getQuery()
                    ->getResult();

            foreach ($adCount as $v) {
                self::$advertisementsInProfession[$countryIdLocale][$v['slug']] = $v['ad_count'];
            }
        }

        if (isset(self::$advertisementsInProfession[$countryIdLocale][$slug])) {
            return self::$advertisementsInProfession[$countryIdLocale][$slug];
        }

        return 0;
    }

}
