<?php

namespace VisiDarbi\CommonBundle\Twig;

class AdminExtension extends \Twig_Extension {



    /**
     * EntityManager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $conn;

    protected static $advertisementsCount;
    protected static $citiesCount;
    protected static $advertisementsCountByCity;
    protected static $externalAdvertisementsCountByResource;
    
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        $this->conn = $em->getConnection();
    }    
    
    
    public function getFilters() {
        return array();
    }

    public function getFunctions() {
        return array(
            'advertisementCitiesCount' => new \Twig_Function_Method($this, 'advertisementCitiesCount'),
            'advertisementsCountInCountry' => new \Twig_Function_Method($this, 'advertisementsCountInCountry'),
            'advertisementsCountByCity' => new \Twig_Function_Method($this, 'advertisementsCountByCity'),
            'externalAdvertisementsByResource' => new \Twig_Function_Method($this, 'externalAdvertisementsByResource'),
        );
    }

    
    /**
     * Count cities in country
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country
     */
    public function advertisementCitiesCount($country) {
        
        if(empty(self::$citiesCount)) {
        
            $q = $this->em->createQueryBuilder()
                    ->select('COUNT(ac.id) as city_count, acc.id as country_id')
                    ->from('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'ac')
                    ->join('ac.AdvertisementCountry', 'acc')
                    ->groupBy('acc.id');

            $cytiesCount = $q->getQuery()->getResult();

            foreach($cytiesCount as $v){
                self::$citiesCount[$v['country_id']] = $v['city_count'];
            }

        }
        
        if(isset(self::$citiesCount[$country->getId()])) {
            return self::$citiesCount[$country->getId()];
        }
        
        return 0;
        
    }
    

    /**
     * Count advertisements in country
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country
     */
    public function advertisementsCountInCountry($country) {
        
        if(empty(self::$advertisementsCount)) {
        
            $q = $this->em->createQueryBuilder()
                    ->select('COUNT(a.id) as advertisement_count, acc.id as country_id')
                    ->from('\VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                    ->join('a.AdvertisementCountry', 'acc')
                    ->groupBy('acc.id');

            $cytiesCount = $q->getQuery()->getResult();

            foreach($cytiesCount as $v){
                self::$advertisementsCount[$v['country_id']] = $v['advertisement_count'];
            }

        }
        
        if(isset(self::$advertisementsCount[$country->getId()])) {
            return self::$advertisementsCount[$country->getId()];
        }
        
        return 0;
        
    }
    
    
    /**
     * Count advertisements in country
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $city
     */
    public function advertisementsCountByCity($city) {
        
        if(empty(self::$advertisementsCountByCity)) {
        
            $q = $this->em->createQueryBuilder()
                    ->select('COUNT(a.id) as advertisement_count, acc.id as city_id')
                    ->from('\VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                    ->join('a.AdvertisementCity', 'acc')
                    ->groupBy('acc.id');

            $cytiesCount = $q->getQuery()->getResult();

            foreach($cytiesCount as $v){
                self::$advertisementsCountByCity[$v['city_id']] = $v['advertisement_count'];
            }

        }
        
        if(isset(self::$advertisementsCountByCity[$city->getId()])) {
            return self::$advertisementsCountByCity[$city->getId()];
        }
        
        return 0;
        
    }
        
    
public function externalAdvertisementsByResource($resource){
        
    if(empty(self::$externalAdvertisementsCountByResource)) {
        
            $q = $this->em->createQueryBuilder()
                    ->select('COUNT(ea.id) as advertisement_count, r.id as resource_id')
                    ->from('\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement', 'ea')
                    ->join('ea.externalAdvertisementSource', 'r')
                    ->groupBy('r.id');

            $count = $q->getQuery()->getResult();

            foreach($count as $v){
                self::$externalAdvertisementsCountByResource[$v['resource_id']] = $v['advertisement_count'];
            }

        }
        
        if(isset(self::$externalAdvertisementsCountByResource[$resource->getId()])) {
            return self::$externalAdvertisementsCountByResource[$resource->getId()];
        }
        
        return 0;    
}
    
    public function getName() {
        return 'visidarbi_admin_extension';
    }

}
