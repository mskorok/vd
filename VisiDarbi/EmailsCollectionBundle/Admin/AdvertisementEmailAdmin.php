<?php

namespace VisiDarbi\EmailsCollectionBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

use VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail;

use VisiDarbi\CommonBundle\Admin\CommonAdmin;


class AdvertisementEmailAdmin extends CommonAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {   
        $listMapper
            ->add('email', 'string', array('label' => 'Email'))
            ->add('advertisement.publicId', null, array('label' => 'Advertisment ID'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('advertisement.publicId', 'doctrine_orm_string', array('label' => 'Advertisment ID'))
        ;
    }
    
    public function createQuery($context = 'list') 
    {
        $query = $this->getModelManager()->createQuery($this->getClass(), 'o');
        $query->select('o, a, c, r, com');
        $query->leftJoin('o.advertisement', 'a');
        $query->leftJoin('a.contactPerson ', 'c');
        $query->leftJoin('a.requisites ', 'r');
        $query->leftJoin('a.companies ', 'com');
        
        $query->where('o.country = :country');
        $query->setParameter(':country', $this->getCountryManager()->getCurrentAdminCountry());

        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }


}