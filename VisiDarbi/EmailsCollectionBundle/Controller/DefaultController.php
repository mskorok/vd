<?php

namespace VisiDarbi\EmailsCollectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('VisiDarbiEmailsCollectionBundle:Default:index.html.twig', array('name' => $name));
    }
}
