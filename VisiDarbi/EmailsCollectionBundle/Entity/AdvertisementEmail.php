<?php
namespace VisiDarbi\EmailsCollectionBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 * @ORM\Table(
 *     indexes={@ORM\Index(name="email_idx", columns={"email"})}, 
 *     uniqueConstraints={@ORM\UniqueConstraint(name="advertisement_id_email_idx", columns={"from_advertisement_id","email"})}
 * )
 */
class AdvertisementEmail
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $email;

    /** 
     * 
     * @ORM\JoinColumn(name="from_advertisement_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", 
     *     inversedBy="advertisementEmails"
     * )
     */
    private $advertisement;
    
    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;
    
    
    public function __toString() 
    {
        return (string)$this->email;
    }
    
    public function getAdvertismentId()
    {
        return $this->advertisement->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return AdvertisementEmail
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return AdvertisementEmail
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return AdvertisementEmail
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
}