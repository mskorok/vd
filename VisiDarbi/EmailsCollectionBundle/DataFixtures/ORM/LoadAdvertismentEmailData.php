<?php

namespace VisiDarbi\CMSBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadAdvertismentEmailData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        return;
        
        foreach ($manager->getRepository('VisiDarbiAdvertisementBundle:Advertisement')->findAll() as $adv) {
            $advEmail = new AdvertisementEmail();
            $advEmail->setAdvertisement($adv);
            $advEmail->setEmail($adv->getContactPerson()->getEmail());
            $advEmail->setCountry($adv->getCountry());
            
            $manager->persist($advEmail);
        }

        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 59;
    }  
    
}
