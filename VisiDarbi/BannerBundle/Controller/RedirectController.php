<?php

namespace VisiDarbi\BannerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


use JMS\DiExtraBundle\Annotation as DI;


class RedirectController extends Controller
{
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \VisiDarbi\BannerBundle\Manager\BannerManagerInterface
     * @DI\Inject("visidarbi.banner_manager")
     */
    protected $bannerManager;
    
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;
    

    /**
     * @Route("/go-to/{bannerId}", name="go_to_banner")
     */
    public function goToAction($bannerId)
    {
        $banner = $this->em->getRepository('VisiDarbiBannerBundle:Banner')->find($bannerId);
        if ($banner === null) {
            throw new NotFoundHttpException('Invalid banner id.');
        }
        
        //TODO: check banner activity?
        
        $this->bannerManager->updateClickCount($banner);
        
        return $this->redirect($banner->getUrl());
        
    }
}