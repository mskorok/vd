<?php

namespace VisiDarbi\BannerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/banner-test")
     * @Template()
     */
    public function indexAction()
    {
        //$milliseconds = round(microtime(true) * 1000);
        //var_dump($milliseconds);
        //die();
        
        $bannerManager = $this->get('visidarbi.banner_manager');
        $country = $this->get('visidarbi.country_manager')->getCurrentCountry();
        
        $expCnt = 10000;
        for ($i = 0; $i < $expCnt; $i++ ) {
            $banner = $bannerManager->getBannerForExposition($country, 'lv', 'startpage');
            $bannerManager->updateViewCount($banner);
        }
        
        die(sprintf('Tika atraditi %d banneri', $expCnt));
    }
}
