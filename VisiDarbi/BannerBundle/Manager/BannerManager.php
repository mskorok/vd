<?php

namespace VisiDarbi\BannerBundle\Manager;

use Doctrine\ORM\EntityManager;

use VisiDarbi\BannerBundle\Entity\Banner;
use VisiDarbi\LocalePlaceBundle\Entity\Country;

class BannerManager implements BannerManagerInterface
{
    /**
     * @var EntityManager 
     */
    protected $em;
   
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getBannerForExposition(Country $country, $locale, $location) 
    {
        $today= new \DateTime('now');
        $qb = $this->em
                ->getRepository('VisiDarbiBannerBundle:Banner')
                ->createQueryBuilder('b')
                ->select('b, f')
                ->leftJoin('b.file', 'f')
                ->where('b.country = :country')
                ->andwhere('b.locale = :locale')
                ->andwhere('b.location = :location')
                ->andWhere('(b.active_from <= :today AND b.active_to >= :today)')
                ->setParameter('country', $country->getId())
                ->setParameter('locale', $locale)
                ->setParameter('location', $location)
                ->setParameter('today', $today)
                ->orderBy('b.views', 'ASC')
                ->addOrderBy('b.id', 'DESC')
                ->setMaxResults(1)
        ;
        
        return $qb->getQuery()->getOneOrNullResult();
    } 
    
    public function updateViewCount(Banner $banner)
    {
        $this->em
            ->createQuery('UPDATE '. get_class($banner) .' b SET b.views = b.views + 1  WHERE b.id = :id')
            ->setParameter('id', $banner->getId())
            ->execute()
        ;
    }
    
    public function updateClickCount(Banner $banner)
    {
        $this->em
            ->createQuery('UPDATE '. get_class($banner) .' b SET b.clicks = b.clicks + 1  WHERE b.id = :id')
            ->setParameter('id', $banner->getId())
            ->execute()
        ;
    }
}