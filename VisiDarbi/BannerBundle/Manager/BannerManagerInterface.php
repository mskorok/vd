<?php

namespace VisiDarbi\BannerBundle\Manager;

use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\BannerBundle\Entity\Banner;

interface BannerManagerInterface 
{
    public function getBannerForExposition(Country $country, $locale, $location); 
    public function updateViewCount(Banner $banner); 
    public function updateClickCount(Banner $banner); 
}
