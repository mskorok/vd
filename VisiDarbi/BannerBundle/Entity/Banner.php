<?php

namespace VisiDarbi\BannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banner
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VisiDarbi\BannerBundle\Entity\BannerRepository")
 */
class Banner
{
    
    const TYPE_STATIC = 'static';
    const TYPE_FLASH = 'flash';
    const TYPE_HTML = 'html';

    const LOCATION_STARTPAGE = 'startpage';
    const LOCATION_SIDEBAR = 'sidebar';
    const LOCATION_SIDEBAR_1 = 'sidebar1';
    const LOCATION_SIDEBAR_2 = 'sidebar2';
    const LOCATION_FOOTER = 'footer';
    const LOCATION_MOBILE_TOP = 'mobile-top';
    const LOCATION_MOBILE_BOTTOM = 'mobile-bottom';
    
    const STATUS_PLANNED = 'planned';
    const STATUS_ACTIVE = 'active';
    const STATUS_EXPIRED = 'expired';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=255, nullable=false)
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=false)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="active_from", type="date", nullable=true)
     */
    private $active_from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="active_to", type="date", nullable=true)
     */
    private $active_to;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=255, nullable=true)
     */
    private $target;
    

    /** 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", nullable=true)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="html_code", type="text", nullable=true)
     */
    private $html_code;
    
    /**
     * @ORM\Column(name="views", type="integer", nullable=false)
     */
    private $views;

    /**
     * @ORM\Column(name="clicks", type="integer", nullable=false)
     */
    private $clicks;
    
    
    
    public function __construct() 
    {
        $this->views = 0;
        $this->clicks = 0;
    }
    
    public function __toString() 
    {
        return $this->name === null ? 'New banner' : $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Banner
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    
        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Banner
     */
    public function setLocation($location)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Banner
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set active_from
     *
     * @param \DateTime $activeFrom
     * @return Banner
     */
    public function setActiveFrom($activeFrom)
    {
        $this->active_from = $activeFrom;
    
        return $this;
    }

    /**
     * Get active_from
     *
     * @return \DateTime 
     */
    public function getActiveFrom()
    {
        return $this->active_from;
    }

    /**
     * Set active_to
     *
     * @param \DateTime $activeTo
     * @return Banner
     */
    public function setActiveTo($activeTo)
    {
        $this->active_to = $activeTo;
    
        return $this;
    }

    /**
     * Get active_to
     *
     * @return \DateTime 
     */
    public function getActiveTo()
    {
        return $this->active_to;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Banner
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Banner
     */
    public function setFile($file)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set html_code
     *
     * @param string $htmlCode
     * @return Banner
     */
    public function setHtmlCode($htmlCode)
    {
        $this->html_code = $htmlCode;
    
        return $this;
    }

    /**
     * Get html_code
     *
     * @return string 
     */
    public function getHtmlCode()
    {
        return $this->html_code;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Banner
     */
    public function setViews($views)
    {
        $this->views = $views;
    
        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set clicks
     *
     * @param integer $clicks
     * @return Banner
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
    
        return $this;
    }

    /**
     * Get clicks
     *
     * @return integer 
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return Banner
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    public function getStatus() 
    {
        $now = new \DateTime('now');
        
        $from = new \DateTime();
        $from->setDate($this->active_from->format('Y'), $this->active_from->format('m'), $this->active_from->format('d'));
        $from->setTime(0, 0, 0);
        
        $to = new \DateTime();
        $to->setDate($this->active_to->format('Y'), $this->active_to->format('m'), $this->active_to->format('d'));
        $to->setTime(23, 59, 59);
        
        if ($now < $from) {
            return self::STATUS_PLANNED;
        }
        
        if ($now > $to) {
            return self::STATUS_EXPIRED;
        }
        
        return self::STATUS_ACTIVE;
    }

    /**
     * Set target
     *
     * @param string $target
     * @return Banner
     */
    public function setTarget($target)
    {
        $this->target = $target;
    
        return $this;
    }

    /**
     * Get target
     *
     * @return string 
     */
    public function getTarget()
    {
        return $this->target;
    }
}