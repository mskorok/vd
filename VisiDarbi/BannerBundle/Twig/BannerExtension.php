<?php

namespace VisiDarbi\BannerBundle\Twig;

use VisiDarbi\BannerBundle\Manager\BannerManagerInterface;
use Twig_Extension;
use Twig_Function_Method;
use VisiDarbi\BannerBundle\Entity\Banner;
use VisiDarbi\LocalePlaceBundle\Entity\Country;

use Sonata\MediaBundle\Provider\Pool;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BannerExtension extends Twig_Extension
{
    /**
     * @var \VisiDarbi\BannerBundle\Manager\BannerManagerInterface 
     */
    protected $bm;
    
    /**
     * @var \Sonata\MediaBundle\Provider\Pool 
     */
    protected $mediaService;
    
    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    protected $router;


    public function __construct(BannerManagerInterface $bm, Pool $mediaService,  UrlGeneratorInterface $router )
    {
        $this->bm = $bm;
        $this->mediaService = $mediaService;
        $this->router = $router;
    }

    public function getFunctions()
    {
        return array(
            'get_banner_for_exposition' => new Twig_Function_Method($this, 'getBannerForExposition'),
            'expose_banner' => new Twig_Function_Method($this, 'exposeBanner', array('is_safe' => array('html'))),
        );
    }

    public function getBannerForExposition(Country $country, $locale, $location)
    {
        return $this->bm->getBannerForExposition($country, $locale, $location);
    }
    
    public function exposeBanner(Banner $banner) 
    {
        $output = null;
        $url = $this->router->generate('go_to_banner', array('bannerId' => $banner->getId() ));
        
        switch($banner->getType()) {
            case Banner::TYPE_STATIC;
                
                $image = $banner->getFile();
                $provider = $this->mediaService->getProvider($image->getProviderName());
                
                $format = 'small';
                if (in_array($banner->getLocation(), array(Banner::LOCATION_SIDEBAR, Banner::LOCATION_SIDEBAR_1, Banner::LOCATION_SIDEBAR_2))) {
                    $format = 'sidebar';
                }
                
                if (in_array($banner->getLocation(), array(Banner::LOCATION_STARTPAGE))) {
                    $format = 'startpage';
                }

                if (in_array($banner->getLocation(), array(Banner::LOCATION_MOBILE_TOP, Banner::LOCATION_MOBILE_BOTTOM))) {
                    $format = 'mobile';
                }

                $target = $banner->getTarget();
                $target = empty($target) ? '_blank' : $target;
                $output = sprintf(
                    '<a href="%s" title="%s" target="%s" ><img src="%s" alt="%s" /></a>',
                    $url,
                    $banner->getName(),
                    $target,
                    $provider->generatePublicUrl($image, 'banner_' . $format),
                    $image->getName()
                );
                
                break;
            case Banner::TYPE_HTML:
                $output = $banner->getHtmlCode();
                break;
            
            case Banner::TYPE_FLASH:

                $file = $banner->getFile();
                $provider = $this->mediaService->getProvider($file->getProviderName());
                $path = $provider->generatePublicUrl($file, 'reference');
                
                $output = '<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="%width%" height="%height%">
                                <param name="movie" value="%swf_url%" />
                                <param name="quality" value="high" />
                                <embed src="%swf_url%" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="%width%" height="%height%"></embed>
                          </object>';
                
                $output = strtr($output, array(
                    '%swf_url%' => $provider->generatePublicUrl($file, 'reference'),
                    '%width%' => '100%',
                    '%height%' => '100%',
                ));
                break;
            
            default:
                throw new \Exception(sprintf('Not implemented exposion of banner with type "%s"', $banner->getType()));
                break;
        }
        
        $this->bm->updateViewCount($banner);
        
        return $output;

    }

    public function getName()
    {
        return 'visidarbi_banner';
    }
}
