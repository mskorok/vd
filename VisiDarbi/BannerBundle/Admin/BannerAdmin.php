<?php

namespace VisiDarbi\BannerBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use VisiDarbi\BannerBundle\Entity\Banner;


class BannerAdmin extends Admin
{
    /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;
   
    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }
    
    protected function getLocales() 
    {
        $locales = array();
        $country = $this->countryManager->getCurrentAdminCountry();
        foreach ($country->getLanguages() as $lng) {
            $locales[$lng->getLocale()] = $lng->getName();
        }
        
        return $locales;
    }
    
    protected function getLocations() 
    {
        return array(
            Banner::LOCATION_STARTPAGE => 'Startpage',
            Banner::LOCATION_SIDEBAR => 'Sidebar #1',
            Banner::LOCATION_SIDEBAR_1 => 'Sidebar #2',
            Banner::LOCATION_SIDEBAR_2 => 'Sidebar #3',
            Banner::LOCATION_FOOTER => 'Footer'
        );
    }
    
    protected function getStatuses() 
    {
        return array(
            Banner::STATUS_PLANNED => 'Planned',
            Banner::STATUS_ACTIVE => 'Active',
            Banner::STATUS_EXPIRED => 'Expired'
        );
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {   
        $listMapper
            ->addIdentifier('id')
            ->add('locale', 'string', array(
                'label' => 'Language',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->getLocales()
            ))
            ->add('name')
            ->add('location', 'string', array(
                'label' => 'Location',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->getLocations(),
            ))
            ->add('active_from')
            ->add('active_to')
            ->add('status', 'string', array(
                'label' => 'Status',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->getStatuses()
            ))
            ->add('views')
            ->add('clicks')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('locale', 'doctrine_orm_choice', array('label' => 'Language',
                'field_options' => array(
                    'required' => false,
                    'choices' => $this->getLocales()
                ),
                'field_type' => 'choice'
            ))
            ->add('status', 'doctrine_orm_callback', array(
                'callback' => function($queryBuilder, $alias, $field, $value) {
                    $value = ! empty($value['value']) ? $value['value'] : null;
                    if (! array_key_exists($value, $this->getStatuses())) {
                        return;
                    }
                    
                    $today = new \DateTime('now');
                    
                    if ($value === Banner::STATUS_PLANNED) {
                        $queryBuilder->andWhere($alias . '.active_from > :today');
                    }
                    
                    if ($value === Banner::STATUS_ACTIVE) {
                        $queryBuilder->andWhere('('. $alias .'.active_from <= :today AND '. $alias .'.active_to >= :today)');
                    }
                    
                    if ($value === Banner::STATUS_EXPIRED) {
                        $queryBuilder->andWhere($alias . '.active_to < :today');
                    }
                    
                    $queryBuilder->setParameter(':today', $today);

                    return true;
                },
                'field_type' => 'choice',
                'label' => 'Status',
                'field_options' => array(
                    'choices' => $this->getStatuses(),
                    'required' => false,
                    //'multiple' => true,
                    //'attr' => array('size' => 7),
                ),
                
            ))
            ->add('location', 'doctrine_orm_choice', array('label' => 'Location',
                'field_options' => array(
                    'required' => false,
                    'choices' => $this->getLocations(),
                ),
                'field_type' => 'choice'
            ))
            ->add('active_from', 'doctrine_orm_date', array(
                'label' => 'Active from'
            ))
            ->add('active_to', 'doctrine_orm_date', array(
                'label' => 'Active to'
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Banner')
                ->add('locale', 'choice', array(
                        'choices' => $this->getLocales(),
                        'required' => true,
                        'label' => 'Language',
                ))
                ->add('name', null, array( 'required' => true ))
                ->add('location', null, array( 'required' => true ))
                ->add('location', 'choice', array(
                        'choices' => $this->getLocations(),
                        'required' => true,
                        'label' => 'Location',
                ))
                ->add('type', 'choice', array(
                        'choices' => array(
                            Banner::TYPE_STATIC => 'Static',
                            Banner::TYPE_FLASH => 'Flash',
                            Banner::TYPE_HTML => 'HTML'
                        ),
                        'required' => true,
                        'label' => 'Type',
                        'attr' => array('class' => 'type-selector span5')
                ))
                ->add('active_from', 'date', array(
                    //'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => true
                ))
                ->add('active_to', 'date', array(
                    //'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => true
                ))
                ->add('url', null, array( 'required' => false ))
                ->add('target', 'choice', array(
                    'choices' => array(
                        '_blank' => 'New window',
                        '_self' => 'Same window',
                    ),
                    'required' => false,
                    'empty_value' => false,
                ))
                ->add('file', 'sonata_type_model_list', 
                        array(
                            'required' => false, 
                            'attr' => array('class' => 'file-selector span5'),
                            'help' => 'Desirable banner dimensions are: <br> Startpage: 940x148px <br>Sidebar: 160x600px'
                        ), 
                        array(
                            'link_parameters' => array('context' => 'banner')
                        )
                )
                ->add('html_code', null, array( 
                    'required' => false,
                    'attr' => array('class' => 'html-code-selector span5')
                ))
            ->end()
        ;
    }
    
    
    public function validate(ErrorElement $errorElement, $banner)
    {
        if ($banner->getType() == Banner::TYPE_HTML) {
            $errorElement
                ->with('html_code')
                    ->assertNotNull(array('message' => 'HTML code required for selected banner type.'))
                ->end()
            ;
        }
        
        //if ($banner->getType() == Banner::TYPE_STATIC) {
        //    $errorElement
        //        ->with('url')
        //            ->assertNotBlank(array('message' => 'URL required for selected banner type.'))
        //        ->end()
        //    ;
        //}
        
        if (in_array($banner->getType(), array(Banner::TYPE_STATIC, Banner::TYPE_FLASH))) {
            $errorElement
                ->with('url')
                    ->assertNotBlank(array('message' => 'URL required for selected banner type.'))
                ->end()
                ->with('file')
                    ->assertNotNull(array('message' => 'File required for selected banner type.'))
                ->end()
            ;
        }
        

        
        if ($banner->getActiveFrom() && $banner->getActiveTo() && 
                $banner->getActiveFrom() > $banner->getActiveTo()) {
            $errorElement
                ->with('value')
                    ->addViolation('Invalid banner activity interval.')
                ->end()
            ;
        }
    }
    
    public function prePersist($banner) 
    {
        $banner->setCountry(
            $this->countryManager->getCurrentAdminCountry()
        );
        
        $this->unsetTypeRelatedData($banner);
    }

    public function preUpdate($banner) 
    {
        $this->unsetTypeRelatedData($banner);
    }
    
    protected function unsetTypeRelatedData(Banner $banner) 
    {
        if ($banner->getType() == Banner::TYPE_HTML) {
            $banner->setFile(null);
        }
        
        if (in_array($banner->getType(), array(Banner::TYPE_STATIC, Banner::TYPE_FLASH))) {
            $banner->setHtmlCode(null);
        }
    }

    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'VisiDarbiBannerBundle:BannerAdmin:edit.html.twig';
        }
        return parent::getTemplate($name);
    }
    
    public function createQuery($context = 'list')
    {
        $query = $this->getModelManager()->createQuery($this->getClass(), 'b');
        $query->select('b');
        $query->where('b.country = :country');
        $query->setParameter(':country', $this->countryManager->getCurrentAdminCountry());
        //$query->orderBy('b.id', 'DESC');
        
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }
    
        
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'id' // field name
    );
}