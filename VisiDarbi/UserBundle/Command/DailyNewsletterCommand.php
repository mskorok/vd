<?php

namespace VisiDarbi\UserBundle\Command;

use Symfony\Component\Translation\IdentityTranslator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;

use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use VisiDarbi\EmailTemplatingBundle\EmailSender;

use VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface;
use VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings;

use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

use VisiDarbi\CommonBundle\SphinxQL\SphinxQL;
use Foolz\SphinxQL\Connection;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * command to send daily newsletters to users
 * which have active subscription
 */
class DailyNewsletterCommand extends ContainerAwareCommand {



    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine")
     */
    protected $em;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;


    /**
     * @var \Symfony\Component\Routing
     * @DI\Inject("router")
     */
    protected $router;


    protected $routerContext;

    /**
     * @var Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $output;


    /**
     * @var array of user subscriptions
     */
    protected $userSubscriptions;


    const ADVERTISEMENTS_PER_EMAIL = 10;
    const SECONDS_IN_ONE_DAY = 86400;




    protected function configure() {
        parent::configure();
        $this->setName('user:send_newsletters')->setDescription('Send user daily newsletters');
    }

    protected function execute(InputInterface $input, OutputInterface $output ) {

        $this->em = $this->getContainer()->get('Doctrine')->getEntityManager();
        $this->emailSender = $this->getContainer()->get('visidarbi.email_templating.sender');
        $this->countryManager = $this->getContainer()->get('visidarbi.country_manager');
        $this->settingsManager = $this->getContainer()->get('visidarbi.settings.manager');
        $this->router = $this->getContainer()->get('router');
        $this->routerContext = $this->router->getContext();
        $this->routerContext->setHost($this->countryManager->getCurrentCountry()->getDomain());
        $this->output = $output;

        //$output->writeln("\nExecuting...");
        ////user newsletter subscription commands

        //Get all subscribers
        $this->collectUserSubscriptions();

        //Process subscribers data
        $this->processSubscriptions();

        $output->writeln('<info>Done</info>');
    }

    /**
     * reads user subcription settings
     */
    private function collectUserSubscriptions() {

        $this->output->writeln('<info>Collecting Subscribers...</info>');


        $this->userSubscriptions = $this->em->createQueryBuilder('es')
                                        ->select('emailsettings', 'user')
                                        ->from('VisiDarbiUserBundle:EmailSubscriptionSettings', 'emailsettings')
                                        ->leftJoin('emailsettings.user', 'user')
                                        ->andwhere('user.enabled = 1')
                                        ->orWhere('user.enabled IS NULL')
                                        ->andwhere('user.locked = 0')
                                        ->orWhere('user.locked IS NULL')
                                        ->getQuery()
                                        ->getResult()
        ;

        if($this->userSubscriptions == null){
            $this->output->writeln('<comment>Subscribers not found.</comment>');
            exit;
        }
    }


    /**
     * interate through subscriptions and determine
     * if need to process it now depending on period
     */
    private function processSubscriptions() {

        $this->output->writeln('<info>Processing subscriber data...</info>');

        foreach ($this->userSubscriptions as $subscription) {

            $this->output->writeln('<info>-----');
            $this->output->writeln('Country: '.$this->countryManager->getCurrentCountry()->getName());

            $this->output->writeln('Language: '.$subscription->getLanguage());

            $this->routerContext->setParameter('_locale', $subscription->getLanguage());


            $sendingFrequency = $subscription->getSendingFrequency();
            $lastTimeSent = $subscription->getLastTimeSent();

            $sent_days_before = 0;
            if($lastTimeSent != NULL){
                $sent_days_before = intval((strtotime(date('Y-m-d')) - $lastTimeSent->format('U')) / self::SECONDS_IN_ONE_DAY ) ;
            }

            $daysInPeriod = -1;
            switch($sendingFrequency) {
                case EmailSubscriptionSettings::FREQ_DAILY: {
                    $daysInPeriod = 1;

                    break;
                }
                case EmailSubscriptionSettings::FREQ_WEAKLY: {
                    $daysInPeriod = 7;

                        break;
                }
                case EmailSubscriptionSettings::FREQ_MONTHLY: {
                    $daysInPeriod = 31;

                    break;
                }

            }

            if($subscription->getLastTimeSent() instanceof \DateTime){
                $this->output->writeln('Last time sent: '.$subscription->getLastTimeSent()->format('Y-m-d'));
            }
            else{
                $this->output->writeln('Last time sent: Never');
            }

            if( ($sent_days_before - $daysInPeriod) >= 0 || !$lastTimeSent){
                $user = $subscription->getUser() ? $subscription->getUser() : false;
                $userEmail = $user ? $user->getEmail() : $subscription->getEmail();
                $this->output->writeln( 'Email to: '.$userEmail );

                $subscriptionData = $this->collectSubscriptionData($subscription);

                if($userEmail AND isset($subscriptionData['newsletter']) AND !empty($subscriptionData['newsletter'])){

                    $placeHolderArray = array(
                        'newsLetter' => true,
                        'viewAllAdvertsLink' => true,
                        'unsubscriptionLink' => true,
//                        'criterias' => true
                    );

                    //send Newsletter
                    $this->sendEMailMessage($userEmail, $placeHolderArray, $subscriptionData, EmailTemplate::ID_USER_NEWSLETTER, $subscription);

                    $this->output->writeln( 'Success: Email sent.' );
                }
                else{
                    $this->output->writeln( '<error>Failed: Email not found OR Newsletter not generated </error>' );
                }

                $subscription->setLastTimeSent(new \DateTime());
                $this->em->persist($subscription);
                $this->em->flush();


            }
            else{
                $this->output->writeln( '<comment>Subscriber frequency: '.$sendingFrequency.' </comment>' );
            }

            $this->output->writeln('-----</info>');

        }

    }

    private function collectSubscriptionData(EmailSubscriptionSettings $subscription){

        $translator = $this->getContainer()->get('translator');
        $translator->setLocale($subscription->getLanguage());

        $criteriaHtml = '';
        $advMaxAge = $subscription->getAdvertisementMaxAge();
        $categories = $subscription->getCategories();
        $professions = $subscription->getProfessions();
        $cities = $subscription->getCities();
        $sources = $subscription->getSources();
        $locale = $subscription->getLanguage();



        $adv_from_date = date('Y-m-d', strtotime(date('Y-m-d')) - self::SECONDS_IN_ONE_DAY * ($advMaxAge-1));

        $this->output->writeln('Adv max age: '.$advMaxAge);
        $this->output->writeln('Adv from date: '. $adv_from_date );


        $categoryIds = array();
        $professionIds = array();
        $citiesIds = array();
        $sourcesIds = array();
        $professionsSlugsSet = array();

        if ($categories->count()){
//            $catTitles = '';
            foreach ($categories as $k=>$category) {
//                $catTitles = $catTitles . ($k > 0 ? ', ' : '') . $category->getName();
                $categoryIds[$category->getId()] = true;
            }
//            $catTitles = addslashes($catTitles);
//            $criteriaHtml .= "<li><strong>{$translator->trans('Category')}:</strong> {$catTitles}</li>\r\n";
        }


        if ($professions->count()) {
//            $profTitles = '';
            foreach ($professions as $k=>$profession) {
//                $profTitles = $profTitles . ($k > 0 ? ', ' : '') . $profession->getName();
                $professionIds[$profession->getId()] = true;
            }
//            $profTitles = addslashes($profTitles);
//            $criteriaHtml .= "<li><strong>{$translator->trans('Profession')}:</strong> {$profTitles}</li>\r\n";
        }

        /*
        $professionsSlugs = empty($professionIds) ? array() : $this->em->createQueryBuilder('p')
                ->select("pt.slug")
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->leftJoin('p.translations', 'pt')
                ->where('p.id IN (:professionIds)')
                ->andWhere('pt.locale = :locale')
                ->setParameter('professionIds', array_keys($professionIds))
                ->setParameter('locale', $locale)
                ->getQuery()->getResult();

        foreach($professionsSlugs as $slug) {
            $professionsSlugsSet[] = $slug['slug'];
        }
        */

        if ($cities->count()) {
//            $cityTitles = '';
            foreach ($cities as $k=>$city) {
//                $cityTitles = $cityTitles . ($k > 0 ? ', ' : '') . $city->getName();
                $citiesIds[$city->getId()] = true;
            }
//            $cityTitles = addslashes($cityTitles);
//            $criteriaHtml .= "<li><strong>{$translator->trans('Location')}:</strong> {$cityTitles}</li>\r\n";
        }

        if ($sources->count()) {
//            $sourceTitles = '';
            foreach ($sources as $k=>$source) {
//                $sourceTitles = $sourceTitles . ($k > 0 ? ', ' : '') . $source->getShowAs();
                $sourcesIds[$source->getId()] = true;
            }
//            $sourceTitles = addslashes($sourceTitles);
//            $criteriaHtml .= "<li><strong>{$translator->trans('Location')}:</strong> {$sourceTitles}</li>\r\n";
        }

        if ($subscription->getWhatValue()){
//            $whatValue = addslashes($subscription->getWhatValue());
//            $criteriaHtml .= "<li><strong>{$translator->trans('Keywords')}:</strong> {$whatValue}</li>\r\n";
        }

        if ($subscription->getWhereValue()){
//            $whereValue = addslashes($subscription->getWhereValue());
//            $criteriaHtml .= "<li><strong>{$translator->trans('Place')}:</strong> {$whereValue}</li>\r\n";
        }


        //get sphinx filter results by given keywords
        $sphinxResults = array();
        if ($subscription->getWhatValue() || $subscription->getWhereValue()) {
            $sphinxResults = $this->getSearchFilter(array('what' => $subscription->getWhatValue(), 'where' => $subscription->getWhereValue()));
        }

        $advertsRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');

        $filteredAdverts = $this->em->createQueryBuilder('adv')
                ->select("advertisement, company, adcity, externalAd, resource,  slugs")
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')

                ->leftJoin('advertisement.companies', 'company')

                ->leftJoin('advertisement.AdvertisementCity', 'adcity')
                ->leftJoin('advertisement.Professions', 'profession')
                ->leftJoin('advertisement.Categories', 'category')
                ->leftJoin('advertisement.externalAdvertisement', 'externalAd')
                ->leftJoin('externalAd.externalAdvertisementSource', 'resource' )
                ->leftJoin('advertisement.slugs', 'slugs')

                ->where('advertisement.date_from >= :advAge')

                ->andWhere('advertisement.country = :country')
                ->andWhere('advertisement.status = :status')

                ->setParameter('advAge', $adv_from_date)

                ->setParameter('country', $this->countryManager->getCurrentCountry()->getId())
                ->setParameter('status', Advertisement::STATUS_ACTIVE)
                ->orderBy('advertisement.date_from', 'DESC')
                ->setMaxResults(self::ADVERTISEMENTS_PER_EMAIL)
                ->groupBy('advertisement.id');

        if($subscription->getLastTimeSent()){
            $filteredAdverts->andWhere('advertisement.date_from > :time_sent');
            $filteredAdverts->setParameter('time_sent', $subscription->getLastTimeSent());
        }

        $filteredAdverts->orderBy('advertisement.is_highlighted', 'DESC');

        if (! empty($categoryIds)) {
            $filteredAdverts
                ->andWhere('category IN (:categoryIds)')
                ->setParameter('categoryIds', array_keys($categoryIds))
            ;
        }

        if (! empty($professionsSlugsSet)) {
            $filteredAdverts
                ->leftJoin('profession.translations', 'profession_trans')
                ->andWhere('profession.id IN (:professionsIds)')
                //->andWhere('profession_trans.slug IN (:professionslug)')
                ->setParameter('professionsIds', array_keys($professionIds))
                //->setParameter('professionslug', $professionsSlugsSet)
            ;
        }

        if (!empty($citiesIds)) {
            $filteredAdverts
                ->andWhere('adcity.id IN  (:cities)')
                ->setParameter('cities', array_keys($citiesIds))
            ;
        }

        if (!empty($sourcesIds)) {
            $filteredAdverts
                ->andWhere('resource.id IN (:sources) OR resource.id IS NULL ')
                ->setParameter('sources', array_keys($sourcesIds))
            ;
        }

        if (!empty($sphinxResults)) {
            $filteredAdverts
                ->andwhere('advertisement.id IN (:advIds)')
                ->setParameter('advIds',  $sphinxResults)
            ;
        }

        $filteredAdverts = $filteredAdverts->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $filteredAdverts = $filteredAdverts->getResult();

        if($filteredAdverts && !empty($filteredAdverts)) {

            $this->output->writeln('Advertisments found: '.count($filteredAdverts));

            $newsletterData = '';
            foreach ($filteredAdverts as $advertisement) {



                $adv_title = $advertisement->getTitle();
                if(!empty($adv_title)){

                    $advUrl = $this->router->generate('visidarbi_advertisement_show', array('advertisementId' => $advertisement->getSlugOrPublicId($locale)), true);

                    $company_name =  '';
                    if($advertisement->getCompany()){
//                        $company_name =  '('. $advertisement->getCompany()->getName() .')';
                        $company_name =  $advertisement->getCompany()->getName();
                    }

                    $location = $advertisement->getAdvertisementCity()->getName();

                    //<tr><td style="padding-left:0;"><a href="">MĀRKETINGA SPECIĀLISTS/-E</a></td><td><a href="">4finance A/S</a></td><td>Rīga, Pārdaugava</td><td>2015.01.19</td><td>2015.01.19</td></tr>

                    $newsletterData .= sprintf('<tr><td style="padding-left:0;"><a href="%s">%s</a></td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                        $advUrl,
                        $adv_title,
                        $company_name,
                        $location,
                        $advertisement->getDateFrom()->format('Y-m-d'),
                        $advertisement->getDateTo()->format('Y-m-d')
                    )."\n";

//                    $newsletterData .= '<div><a href="'.$advUrl.'">' . $adv_title . $company_name . '</a></div>';

                }
            }

            $newsletterUrl = $this->generarteAdvListUrl($subscription->getWhatSlug(), $subscription->getWhereSlug(), $subscription->getFilterKey());

            $unsubscribeLink = $this->router->generate('unsubscribe_from_news', array('subscriptionId' => $subscription->getSubscriptionId()), true);
            return array(
                'newsletter' => $newsletterData,
                'newsletterUrl' => $newsletterUrl,
                'unsubscriptionLink' => $unsubscribeLink,
//                'criterias' => $criteriaHtml
            );

        }
        else{
            $this->output->writeln('Advertisments found: 0');
            return false;
        }

    }

    private function sendEMailMessage($email_to, $placeHolderArray, $subscriptionData, $email_template, $subscription){

        $this->em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $prepareNewsletter = array(
            'newsLetter' => $subscriptionData['newsletter'],
//            'criterias' => $subscriptionData['criterias'],
        );


        if(isset($placeHolderArray['viewAllAdvertsLink'])){
            $prepareNewsletter['viewAllAdvertsLink'] = $subscriptionData['newsletterUrl'];
        }

        if(isset($placeHolderArray['unsubscriptionLink'])){
            $prepareNewsletter['unsubscriptionLink'] = $subscriptionData['unsubscriptionLink'];
        }

        $sendFromEmail = $this->settingsManager->get('emails.sender_email');
        $sendFromName = $this->settingsManager->get('emails.sender_name');

        $this->emailSender->send(
            $this->countryManager->getCurrentCountry(),
            $subscription->getLanguage(),
            $email_template,
            $email_to,
            $prepareNewsletter,
            array(
                'email' => $sendFromEmail,
                'name' => $sendFromName
            )
        );

        return true;

    }

    protected function getSearchFilter($params) {

        $config = $this->getContainer()->getParameter('sphinx');

        if (empty($params)) {
            return array();
        }

        $conn = new Connection();
        SphinxQL::forge($conn);

        $query = SphinxQL::forge();
        $query->selectFromStr('@id,  (@weight * adwigth) as w');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

                $searchPhrase = $params['what'];
                if($this->getSphinxKeyword($searchPhrase)) {
                    $paramSettled = true;
                    $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
                }
        }

        if (!empty($params['where'])) {

                $searchPhrase = $params['where'];
                if($this->getSphinxKeyword($searchPhrase)) {
                    $paramSettled = true;
                    $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
                }
        }

        if(!$paramSettled) {
            return null;
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);

        $result = $query->execute();

        $ids = array();
        foreach ($result as $v) {
            $ids[] = $v['id'];
        }
        return $ids;
    }


    private function getSphinxKeyword($sQuery) {

        $sSphinxKeyword = null;
        $aKeyword = array();
        $aRequestString = preg_split('/[\s,-]+/', $sQuery, 5);
        if ($aRequestString) {
            foreach ($aRequestString as $sValue) {
                if (strlen($sValue) >= 3) {
                    $aKeyword[] .= "(" . SphinxQL::forge()->escapeMatch($sValue) . " | *" . SphinxQL::forge()->escapeMatch($sValue) . "*)";
                }
            }

            if(!empty($aKeyword)){
                $sSphinxKeyword = implode(" & ", $aKeyword);
            }
        }

        return $sSphinxKeyword;
    }


    private function generarteAdvListUrl( $what_slug = false, $where_slug = false, $filter_key = false ){

        $link = false;

        $whatSlug = false;

        $whereSlug = false;

        if( $what_slug && !empty($what_slug) ){
            $whatSlug = $what_slug;
        }

        if($where_slug && !empty($where_slug)){
            $whereSlug = $where_slug;
        }

        if($filter_key){
            if ($whatSlug && $whereSlug) {
                $link = $this->router->generate('visidarbi_advertisement_list_search_filter', array( 'filter' => $filter_key, 'what' => $whatSlug, 'where' => $whereSlug ), true);

            } elseif ($whatSlug && !$whereSlug) {
                $link = $this->router->generate('visidarbi_advertisement_list_search_what_filter', array( 'filter' => $filter_key, 'what' => $whatSlug ), true);

            } elseif ($whereSlug && !$whatSlug) {
                $link = $this->router->generate('visidarbi_advertisement_list_search_where_filter', array( 'filter' => $filter_key, 'where' => $whereSlug ), true);

            }
            else{
                $link = $this->router->generate('visidarbi_advertisement_list_filter', array( 'filter' => $filter_key ), true);
            }
        }
        else if($whatSlug || $whereSlug){

            if( $whatSlug && $whereSlug ){
                $link = $this->router->generate('visidarbi_advertisement_list_search', array('what' => $whatSlug, 'where' => $whereSlug), true);
            }
            elseif( $whereSlug ){
                $link = $this->router->generate('visidarbi_advertisement_list_search_where', array('where' => $whereSlug), true);
            }
            elseif( $whatSlug ){
                $link = $this->router->generate('visidarbi_advertisement_list_search_what', array('what' => $whatSlug), true);
            }

        }
        else{
            $link = $this->router->generate('visidarbi_advertisement_list', array(), true);
        }

        return $link;
    }


}