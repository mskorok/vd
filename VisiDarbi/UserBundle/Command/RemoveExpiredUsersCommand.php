<?php

namespace VisiDarbi\UserBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveExpiredUsersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:remove-expired')
            ->setDescription('Removes from database users which have not confirmed their registration from email in 24h period.')
            ->addArgument(
                'hours',
                InputArgument::OPTIONAL,
                'Who do you want to greet?',
                24
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hours = $input->getArgument('hours');
        
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        
        $expiredUsers = $em->getRepository('VisiDarbiUserBundle:User')->getExpiredUsers($hours);
        
        $output->writeln(\sprintf('Found %d users not confirmed in %d hour period.', count($expiredUsers), $hours));
        
        if (count($expiredUsers) == 0) {
            $output->writeln('Nothing to delete.');
        }
        else {
            $cnt = 0;
            foreach ($expiredUsers as $user) {
                $em->remove($user);
                $cnt++;
            }
            $em->flush();
            $output->writeln(sprintf('Permanently deleted %d user records from database.', $cnt));
        }
    }
}