<?php

namespace VisiDarbi\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\LegalProfile;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $password = 'password';
        $userCnt = 62;
        
        $userManager = $this->container->get('fos_user.user_manager');
        
        $user = new User();
        $user->setFullName('Admin Adminowsky');
        $user->setType(User::TYPE_PRIVATE);
        $user->setUsername('admin');
        $user->setEmail('admin@example.com');
        $user->setPhone('22222222');
        $user->setRoles(array('ROLE_SUPER_ADMIN'));
        $user->setEnabled(true);
        $user->setPrivateProfile(new PrivateProfile('Rīga, Lācplēša 73', 'LV-1011'));
        $user->setPlainPassword($password);
        $user->setIsAdmin(true);
        $userManager->updateUser($user);
        
        /*
        for ($i = 0; $i < $userCnt; $i++) {
            $user = new User();
            $user->setFullName('Test user' . $i);
            $user->setEmail('user' . $i . '@example.com');
            $user->setPhone($this->randomNum(8));
            $user->setEnabled(true);
            $user->setRoles(array('ROLE_USER'));
            $user->setCountry($this->getReference('country-lat'));
            
            if (\mt_rand(0, 1) == 1) {
                $user->setType(User::TYPE_PRIVATE);
                $user->setPrivateProfile(new PrivateProfile('Rīga, Lācplēša ' . $i, 'LV-1011'));
            }
            else {
                $user->setType(User::TYPE_LEGAL);
                $lp = new LegalProfile();
                $lp
                    ->setCompanyName('Test company ' . $i)
                    ->setVatNumber($this->randomNum(11))
                    ->setRegistrationNumber($this->randomNum(11))
                    ->setLegalAddress('Rīga, Brīvības ' . $i)
                    ->setLegalAddressPostcode('LV2299')
                    ->setFactualAddress('Rīga, Matīsa ' . $i)
                    ->setFactualAddressPostcode('LV5557')
                ;
                $user->setLegalProfile($lp);
            }

            $user->setPlainPassword($password);
            $userManager->updateUser($user);
        }*/
        
        $manager->flush();
    }
    
    private function randomNum($len) 
    {
        $characters = '0123456789';
        $string = '';
        for ($i = 0; $i < $len; $i++) {
             $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 20;
    }    
    
}
