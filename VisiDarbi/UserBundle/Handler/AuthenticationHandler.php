<?php

namespace VisiDarbi\UserBundle\Handler;

use Symfony\Component\HttpFoundation\RedirectResponse as RR;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;


class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    
    public function __construct(TranslatorInterface $translator, SecurityContext $securityContext) {
        $this->translator = $translator;
        $this->securityContext = $securityContext;
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($request->isXmlHttpRequest()) {
            $result = array('success' => true);
            return new Response(\json_encode($result));
        } else {
            $this->securityContext->setToken($token);
            $target = $request->request->get('_target_path');
            $url = (!empty($target))?$target:'/mobile/menu';
            return new RR($request->getSchemeAndHttpHost().$url);
        }
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->isXmlHttpRequest()) {
            $result = array( 
                'success' => false, 
                'message' => $exception->getMessage() === 'Bad credentials' ?
                        $this->translator->trans('Bad credentials') : 
                        $exception->getMessage() 
            );
            return new Response(\json_encode($result));
        } else {

            $url = parse_url($request->server->get('HTTP_REFERER'));
            $url = $url['path'];
            $url = (!empty($url))?$url:'/mobile/auth';
            $request->getSession()->set(SecurityContextInterface::AUTHENTICATION_ERROR, $exception);
           return new RR($request->getSchemeAndHttpHost().$url);
        }
    }
}