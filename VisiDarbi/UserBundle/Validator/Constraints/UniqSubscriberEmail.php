<?php

namespace VisiDarbi\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class UniqSubscriberEmail extends Constraint{

	public $message = 'Email already registered';

	public function validatedBy(){
    	return 'uniq_subscriber_email';
	}

}
