<?php

namespace Visidarbi\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

use Visidarbi\UserBundle\Entity\User;
use Visidarbi\UserBundle\Entity\EmailSubscriptionSettings;



class UniqSubscriberEmailValidator extends ConstraintValidator{

	public function __construct(EntityManager $em, SecurityContext $securityContext ){
       
		$this->em = $em;
        $this->securityContext = $securityContext;
	}


	public function validate($value, Constraint $constraint){
       
     	if($value === null || empty($value) || $this->securityContext->isGranted('ROLE_USER')){
            return;
        }

        $user = $this->em->getRepository('VisiDarbi\UserBundle\Entity\User')->findOneBy(array('email' => $value));
        $unregistered_user = $this->em->getRepository('VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings')->findOneBy(array('email' => $value));
     	
        if($user || $unregistered_user){
     		$this->context->addViolation($constraint->message);
     	}
    }

}
