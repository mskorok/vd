<?php

namespace VisiDarbi\UserBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

use Symfony\Component\Validator\Constraints as Assert;

/** 
 * @ORM\Entity
 */
class PrivateProfile
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\UserBundle\Entity\User", inversedBy="privateProfile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $User;

    /** 
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Address must not be empty")
     */
    private $address;

    /** 
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Postcode must not be empty")
     * @Assert\Length(
     *      max = "7",
     *      maxMessage = "[0,Inf]Address postcode must not be longer than {{ limit }} characters"
     *
     * )
     */
    private $address_postcode;
    
    function __construct($address = '', $addressPostcode = '') 
    {
        $this->address = $address;
        $this->address_postcode = $addressPostcode;
    }

        /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return PrivateProfile
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address_postcode
     *
     * @param string $addressPostcode
     * @return PrivateProfile
     */
    public function setAddressPostcode($addressPostcode)
    {
        $this->address_postcode = $addressPostcode;
    
        return $this;
    }

    /**
     * Get address_postcode
     *
     * @return string 
     */
    public function getAddressPostcode()
    {
        return $this->address_postcode;
    }

    /**
     * Set User
     *
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @return PrivateProfile
     */
    public function setUser(\VisiDarbi\UserBundle\Entity\User $user)
    {
        $this->User = $user;
    
        return $this;
    }

    /**
     * Get User
     *
     * @return \VisiDarbi\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->User;
    }
}