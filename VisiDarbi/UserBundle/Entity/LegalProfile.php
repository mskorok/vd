<?php
namespace VisiDarbi\UserBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class LegalProfile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\UserBundle\Entity\User", inversedBy="legalProfile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\CVonlineExportBundle\Entity\CVOnlineExport", mappedBy="LegalProfile")
     */
    private $CVOnlineExports;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Company name must not be empty")
     */
    private $company_name;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(
     *      max = "15",
     *      maxMessage = "[0,Inf]VAT number must not be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     *      pattern="/^[A-Za-z0-9]+$/",
     *      message="VAT number must contain only alphanumeric symbols."
     * )
     */
    private $vat_number;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Registration number must not be empty", groups={"Register", "Edit"})
     * @Assert\Length(
     *      max = "15",
     *      min = "2",
     *      maxMessage = "[0,Inf]Registration number must not be longer than {{ limit }} characters",
     *      minMessage = "[0,Inf]Registration number must be more or equal to {{ limit }} characters"
     * )
     * @Assert\Regex(
     *      pattern="/^[A-Za-z0-9]+$/",
     *      message="Registration number must contain only alphanumeric symbols."
     * )
     */
    private $registration_number;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Legal address must not be empty", groups={"Register", "Edit"})
     */
    private $legal_address;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(message="Legal address postcode must not be empty", groups={"Register", "Edit"})
     * @Assert\Length(
     *      max = "7",
     *      maxMessage = "[0,Inf]Legal address postcode must not be longer than {{ limit }} characters"
     *
     * )
     */
    private $legal_address_postcode;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Factual address must not be empty", groups={"Register", "Edit"})
     */
    private $factual_address;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(message="Factual address postcode must not be empty", groups={"Register", "Edit"})
     * @Assert\Length(
     *      max = "7",
     *      maxMessage = "[0,Inf]Factual address postcode must not be longer than {{ limit }} characters"
     *
     * )
     */
    private $factual_address_postcode;


    /**
     * @ORM\Column(name="last_advertisement_at", type="datetime", nullable=true)
     */
    private $lastAdvertisementAt;

    /**
     * Return last advertisement time
     * @return \DateTime
     */
    public function getLastAdvertisementAt() {
        return $this->lastAdvertisementAt;
    }

    /**
     * Set lat advertisement time
     * @param \DateTime $lastAdvertisementAt
     * @return \VisiDarbi\UserBundle\Entity\LegalProfile
     */
    public function setLastAdvertisementAt(\DateTime $lastAdvertisementAt) {
        $this->lastAdvertisementAt = $lastAdvertisementAt;
        return $this;
    }

        /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     * @return LegalProfile
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set registration_number
     *
     * @param string $registrationNumber
     * @return LegalProfile
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registration_number = $registrationNumber;

        return $this;
    }

    /**
     * Get registration_number
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registration_number;
    }

    /**
     * Set legal_address
     *
     * @param string $legalAddress
     * @return LegalProfile
     */
    public function setLegalAddress($legalAddress)
    {
        $this->legal_address = $legalAddress;

        return $this;
    }

    /**
     * Get legal_address
     *
     * @return string
     */
    public function getLegalAddress()
    {
        return $this->legal_address;
    }

    /**
     * Set legal_address_postcode
     *
     * @param string $legalAddressPostcode
     * @return LegalProfile
     */
    public function setLegalAddressPostcode($legalAddressPostcode)
    {
        $this->legal_address_postcode = $legalAddressPostcode;

        return $this;
    }

    /**
     * Get legal_address_postcode
     *
     * @return string
     */
    public function getLegalAddressPostcode()
    {
        return $this->legal_address_postcode;
    }

    /**
     * Set factual_address
     *
     * @param string $factualAddress
     * @return LegalProfile
     */
    public function setFactualAddress($factualAddress)
    {
        $this->factual_address = $factualAddress;

        return $this;
    }

    /**
     * Get factual_address
     *
     * @return string
     */
    public function getFactualAddress()
    {
        return $this->factual_address;
    }

    /**
     * Set factual_address_postcode
     *
     * @param string $factualAddressPostcode
     * @return LegalProfile
     */
    public function setFactualAddressPostcode($factualAddressPostcode)
    {
        $this->factual_address_postcode = $factualAddressPostcode;

        return $this;
    }

    /**
     * Get factual_address_postcode
     *
     * @return string
     */
    public function getFactualAddressPostcode()
    {
        return $this->factual_address_postcode;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->CVOnlineExports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set User
     *
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @return LegalProfile
     */
    public function setUser(\VisiDarbi\UserBundle\Entity\User $user)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get User
     *
     * @return \VisiDarbi\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * Add CVOnlineExports
     *
     * @param \VisiDarbi\CVonlineExportBundle\Entity\CVOnlineExport $cVOnlineExports
     * @return LegalProfile
     */
    public function addCVOnlineExport(\VisiDarbi\CVonlineExportBundle\Entity\CVOnlineExport $cVOnlineExports)
    {
        $this->CVOnlineExports[] = $cVOnlineExports;

        return $this;
    }

    /**
     * Remove CVOnlineExports
     *
     * @param \VisiDarbi\CVonlineExportBundle\Entity\CVOnlineExport $cVOnlineExports
     */
    public function removeCVOnlineExport(\VisiDarbi\CVonlineExportBundle\Entity\CVOnlineExport $cVOnlineExports)
    {
        $this->CVOnlineExports->removeElement($cVOnlineExports);
    }

    /**
     * Get CVOnlineExports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCVOnlineExports()
    {
        return $this->CVOnlineExports;
    }

    /**
     * Set vat_number
     *
     * @param string $vatNumber
     * @return LegalProfile
     */
    public function setVatNumber($vatNumber)
    {
        $this->vat_number = $vatNumber;

        return $this;
    }

    /**
     * Get vat_number
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vat_number;
    }
}