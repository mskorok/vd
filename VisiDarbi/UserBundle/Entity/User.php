<?php
namespace VisiDarbi\UserBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\UserBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"email", "country", "is_admin"}, message="User with such email already registered")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class User extends BaseUser
{
    const TYPE_PRIVATE = 'private';
    const TYPE_LEGAL = 'legal';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\UserBundle\Entity\PrivateProfile", mappedBy="User", cascade={"persist", "remove"} )
     * @ORM\JoinColumn(name="private_profile_id", referencedColumnName="id", nullable=true, unique=true, onDelete="CASCADE")
     * @Assert\Valid
     */
    private $privateProfile;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\UserBundle\Entity\LegalProfile", mappedBy="User", cascade={"persist", "remove"} )
     * @ORM\JoinColumn(name="legal_profile_id", referencedColumnName="id", nullable=true, unique=true, onDelete="CASCADE")
     * @Assert\Valid
     */
    private $legalProfile;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $is_admin;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(message="Phone must not be empty", groups={"Register", "Edit"})
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "[0,Inf]Phone must not be longer than {{ limit }} characters"
     *
     * )
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="E-mail must not be empty", groups={"Register", "Edit", "AdminEdit"})
     * @Assert\Email(message="E-mail must contain valid e-mail address")
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $emailCanonical;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $socialResourceName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $usernameCanonical;


    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(message="Full name must not be empty", groups={"Register", "Edit", "AdminEdit"})
     */
    private $full_name;


    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country")
     * @ORM\JoinTable(name="users_admin_countries",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id")}
     * )
     */
    private $adminCountries;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", mappedBy="user")
     */
    private $advertisements;

    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;


    /**
     * @var string
     *
     * @ORM\Column(name="default_locale", type="string", length=255, nullable=false)
     */
    private $default_locale;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tmp_password;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tmp_email;

     /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement", mappedBy="User")
     */
    private $userFavoriteAdvertisements;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory", mappedBy="user", cascade={"remove"})
     */
    private $advertismentSearchHistories;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings", mappedBy="user", cascade={"remove"})
     */
    private $emailSubscriptionSettingsList;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * The salt to use for hashing
     *
     * @var string
     * @ORM\Column(type="string")
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @ORM\Column(type="string")
     */

    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     * @Assert\NotBlank(message="Password must not be empty", groups={"Register","Mobile"})
     * @Assert\Length(
     *      max = "20",
     *      minMessage = "[0,Inf]Password must contain at least {{ limit }} characters",
     *      maxMessage = "[0,Inf]Password must not be longer than {{ limit }} characters"
     *
     * )
     */
    protected $plainPassword;


    /**
     * @var \DateTime
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @var string
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $groups;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $locked;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $expired;

    /**
     * @var \DateTime
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $roles;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="credentials_expired")
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime
     * @ORM\Column(name="credentials_expire_at", type="datetime", nullable=true)
     */
    protected $credentialsExpireAt;


    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaidPeriod", mappedBy="user")
     */
    private $paidPeriods;


    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        parent::__construct();
        // your own logic

        $this->is_admin = false;
        $this->advertisements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->adminCountries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        //return (string)$this->full_name;
        return $this->getFullName().'  '.$this->getSurname();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set privateProfile
     *
     * @param \VisiDarbi\UserBundle\Entity\PrivateProfile $privateProfile
     * @return User
     */
    public function setPrivateProfile(\VisiDarbi\UserBundle\Entity\PrivateProfile $privateProfile)
    {
        $this->privateProfile = $privateProfile;

        $privateProfile->setUser($this);

        return $this;
    }

    /**
     * Get privateProfile
     *
     * @return \VisiDarbi\UserBundle\Entity\PrivateProfile
     */
    public function getPrivateProfile()
    {
        return $this->privateProfile;
    }

    /**
     * Set legalProfile
     *
     * @param \VisiDarbi\UserBundle\Entity\LegalProfile $legalProfile
     * @return User
     */
    public function setLegalProfile(\VisiDarbi\UserBundle\Entity\LegalProfile $legalProfile)
    {
        $this->legalProfile = $legalProfile;

        $legalProfile->setUser($this);

        return $this;
    }

    /**
     * Get legalProfile
     *
     * @return \VisiDarbi\UserBundle\Entity\LegalProfile
     */
    public function getLegalProfile()
    {
        return $this->legalProfile;
    }

    public function isLegal()
    {
        return $this->type == self::TYPE_LEGAL;
    }

    public function isPrivate()
    {
        return $this->type == self::TYPE_PRIVATE;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set is_admin
     *
     * @param boolean $isAdmin
     * @return User
     */
    public function setIsAdmin($isAdmin)
    {
        $this->is_admin = $isAdmin;

        return $this;
    }

    /**
     * Get is_admin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->is_admin;
    }

    public function getSingleRole()
    {
        return reset($this->roles);
    }

    public function setSingleRole($singleRole)
    {
        $this->setRoles(array($singleRole));
    }


    /**
     * Add adminCountries
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $adminCountries
     * @return User
     */
    public function addAdminCountrie(\VisiDarbi\LocalePlaceBundle\Entity\Country $adminCountries)
    {
        $this->adminCountries[] = $adminCountries;

        return $this;
    }

    /**
     * Remove adminCountries
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $adminCountries
     */
    public function removeAdminCountrie(\VisiDarbi\LocalePlaceBundle\Entity\Country $adminCountries)
    {
        $this->adminCountries->removeElement($adminCountries);
    }

    /**
     * Get adminCountries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdminCountries()
    {
        return $this->adminCountries;
    }

    public function addAdminCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $adminCountries)
    {
        return $this->addAdminCountrie($adminCountries);
    }

    public function removeAdminCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $adminCountries)
    {
        $this->removeAdminCountrie($adminCountries);
    }

     /**
     * Add advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return AdvertisementType
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->advertisements[] = $advertisements;

        return $this;
    }

    /**
     * Remove advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->advertisements->removeElement($advertisements);
    }

    /**
     * Get advertisements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvertisements()
    {
        return $this->advertisements;
    }


    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return User
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }


    /**
     * Set default_locale
     *
     * @param string $default_locale
     * @return Banner
     */
    public function setDefaultLocale($default_locale)
    {
        $this->default_locale = $default_locale;

        return $this;
    }

    /**
     * Get default_locale
     *
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->default_locale;
    }

    /**
     * Set full_name
     *
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->full_name = $fullName;

        return $this;
    }

    /**
     * Get full_name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * Set tmp_password
     *
     * @param string $tmpPassword
     * @return User
     */
    public function setTmpPassword($tmpPassword)
    {
        $this->tmp_password = $tmpPassword;

        return $this;
    }

    /**
     * Get tmp_password
     *
     * @return string
     */
    public function getTmpPassword()
    {
        return $this->tmp_password;
    }

    /**
     * Returns UserFavoriteAdvertisement
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserFavoriteAdvertisement() {
        return $this->userFavoriteAdvertisements;
    }

    /**
     * set UserFavoriteAdvertisement
     * @param type $userFavoriteAdvertisement
     * @return \VisiDarbi\UserBundle\Entity\User
     */
    public function setUserFavoriteAdvertisement($userFavoriteAdvertisement) {
        $this->userFavoriteAdvertisements = $userFavoriteAdvertisement;
        return $this;
    }




    /**
     * Add userFavoriteAdvertisements
     *
     * @param \VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements
     * @return User
     */
    public function addUserFavoriteAdvertisement(\VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements)
    {
        $this->userFavoriteAdvertisements[] = $userFavoriteAdvertisements;

        return $this;
    }

    /**
     * Remove userFavoriteAdvertisements
     *
     * @param \VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements
     */
    public function removeUserFavoriteAdvertisement(\VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements)
    {
        $this->userFavoriteAdvertisements->removeElement($userFavoriteAdvertisements);
    }

    /**
     * Get userFavoriteAdvertisements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserFavoriteAdvertisements()
    {
        return $this->userFavoriteAdvertisements;
    }

    /**
     * Add advertismentSearchHistories
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory $advertismentSearchHistories
     * @return User
     */
    public function addAdvertismentSearchHistorie(\VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory $advertismentSearchHistories)
    {
        $this->advertismentSearchHistories[] = $advertismentSearchHistories;

        return $this;
    }

    /**
     * Remove advertismentSearchHistories
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory $advertismentSearchHistories
     */
    public function removeAdvertismentSearchHistorie(\VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory $advertismentSearchHistories)
    {
        $this->advertismentSearchHistories->removeElement($advertismentSearchHistories);
    }

    /**
     * Get advertismentSearchHistories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvertismentSearchHistories()
    {
        return $this->advertismentSearchHistories;
    }

    /**
     * Set emailSubscriptionSettings
     *
     * @param \VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings $emailSubscriptionSettings
     * @return User
     */
    /*public function setEmailSubscriptionSettingsList(\VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings $emailSubscriptionSettings = null)
    {
        $this->emailSubscriptionSettings = $emailSubscriptionSettings;

        return $this;
    }*/

    /**
     * Add emailSubscriptionSettings
     *
     * @param \VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings $emailSubscriptionSettings
     * @return User
     */
    public function addEmailSubscriptionSettingsListItem(\VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings $emailSubscriptionSettings)
    {
        $this->emailSubscriptionSettingsList[] = $emailSubscriptionSettings;

        return $this;
    }

    /**
     * Remove emailSubscriptionSettings
     *
     * @param \VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings $emailSubscriptionSettings
     */
    public function removeEmailSubscriptionSettingsListItem(\VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings $emailSubscriptionSettings)
    {
        $this->emailSubscriptionSettingsList->removeElement($emailSubscriptionSettings);
    }

    /**
     * Get emailSubscriptionSettings collection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailSubscriptionSettingsList()
    {
        return $this->emailSubscriptionSettingsList;
    }

    /**
     * Set tmp_email
     *
     * @param string $tmpEmail
     * @return User
     */
    public function setTmpEmail($tmpEmail)
    {
        $this->tmp_email = $tmpEmail;

        return $this;
    }

    /**
     * Get tmp_email
     *
     * @return string
     */
    public function getTmpEmail()
    {
        return $this->tmp_email;
    }

    /**
     * Set emailCanonical
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Get emailCanonical
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set usernameCanonical
     *
     * @param string $usernameCanonical
     * @return User
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * Get usernameCanonical
     *
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

        /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     * @return User
     */
    public function setLastLogin(\DateTime $lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set confirmationToken
     *
     * @param string $confirmationToken
     * @return User
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get confirmationToken
     *
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Set passwordRequestedAt
     *
     * @param \DateTime $passwordRequestedAt
     * @return User
     */
    public function setPasswordRequestedAt(\DateTime $passwordRequestedAt = null)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    /**
     * Get passwordRequestedAt
     *
     * @return \DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return User
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set expired
     *
     * @param boolean $expired
     * @return User
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;

        return $this;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     * @return User
     */
    public function setExpiresAt(\DateTime $expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles)
    {
        return parent::setRoles($roles);
    }


    /**
     * Returns the user roles
     *
     * @return array The roles
     */
    public function getRoles()
    {
        return parent::getRoles();
    }

    /**
     * Set credentialsExpired
     *
     * @param boolean $credentialsExpired
     * @return User
     */
    public function setCredentialsExpired($credentialsExpired)
    {
        $this->credentialsExpired = $credentialsExpired;

        return $this;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * Set credentialsExpireAt
     *
     * @param \DateTime $credentialsExpireAt
     * @return User
     */
    public function setCredentialsExpireAt(\DateTime $credentialsExpireAt)
    {
        $this->credentialsExpireAt = $credentialsExpireAt;

        return $this;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return \DateTime
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }


    public function getLegalPersonAdvCount()
    {
        if ($this->isPrivate()) {
            return null;
        }
        return $this->advertisements->count();
    }

    public static function phoneValidationParams($options = array())
    {
        $arr = array('max_length' => 20);

        $prop = array('label', 'required');
        foreach ($prop as $key) {
            if (isset($options[$key])) {
                $arr[$key] = $options[$key];
            }
        }
        return $arr;
    }

    public static function registrationNumValidationParams($options = array(), $scenario = null)
    {
        $const_arr = array('max_length' => 15, 'min_length' => 2, 'pattern' => '/^[A-Za-z0-9]+$/');

        $arr = array(
            'max_length' => $const_arr['max_length'],
        );
        $prop = array('label', 'property_path', 'required');
        foreach ($prop as $key) {
            if (isset($options[$key])) {
                $arr[$key] = $options[$key];
            }
        }

        if ($scenario == 'advertisement') {
            $arr['constraints'] = array(
                new Length(array('max' => $const_arr['max_length'], 'min' => $const_arr['min_length'],
                    'maxMessage' => !empty($options['maxMessage']) ? $options['maxMessage'] : 'Registration number must not be longer than {{ limit }} characters',
                    'minMessage' => !empty($options['minMessage']) ? $options['minMessage'] : 'Registration number must be more or equal to {{ limit }} characters')
                ),
                new NotBlank(array('message' => !empty($options['notBlankMessage']) ? $options['notBlankMessage'] : 'Registration number can\'t be empty.')),
                new Regex(array('pattern' => $const_arr['pattern'], 'message' => !empty($options['regexMessage']) ? $options['regexMessage'] : 'Registration number must contain only alphanumeric symbols.')),
            );
        } else if ($scenario == 'admin') {
            $arr = $const_arr;
        }

        return $arr;
    }

    public static function vatValidationParams($options = array(), $scenario = null, $withVatValidation = true)
    {
        $const_arr = array('max_length' => 15, 'pattern' => '/^[A-Za-z0-9]+$/');

        $arr = array(
            'max_length' => $const_arr['max_length'],
        );
        $prop = array('label', 'property_path', 'required');
        foreach ($prop as $key) {
            if (isset($options[$key])) {
                $arr[$key] = $options[$key];
            }
        }

        if ($scenario == 'advertisement') {
            $arr['constraints'] = array(
                new Length(array('max' => $const_arr['max_length'],
                        'maxMessage' => !empty($options['maxMessage']) ? $options['maxMessage'] : 'VAT number must not be longer than {{ limit }} characters',
                )),
                new Regex(array('pattern' => $const_arr['pattern'], 'message' => !empty($options['regexMessage']) ? $options['regexMessage'] : 'VAT number must contain only alphanumeric symbols.')),
            );
            if ($withVatValidation) {
                $arr['constraints'][] = new NotBlank(array('message' => !empty($options['notBlankMessage']) ? $options['notBlankMessage'] : 'VAT can\'t be empty.'));
            }
        } else if ($scenario == 'admin') {
            $arr = $const_arr;
        }

        return $arr;
    }

    /**
     * Add paidPeriods
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriods
     * @return User
     */
    public function addPaidPeriod(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriods)
    {
        $this->paidPeriods[] = $paidPeriods;

        return $this;
    }

    /**
     * Remove paidPeriods
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriods
     */
    public function removePaidPeriod(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriods)
    {
        $this->paidPeriods->removeElement($paidPeriods);
    }

    /**
     * Get paidPeriods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaidPeriods()
    {
        return $this->paidPeriods;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return mixed
     */
    public function getSocialResourceName()
    {
        return $this->socialResourceName;
    }

    /**
     * @param mixed $socialResourceName
     */
    public function setSocialResourceName($socialResourceName)
    {
        $this->socialResourceName = $socialResourceName;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }





}