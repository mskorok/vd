<?php

namespace VisiDarbi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * EmailSubscriptionSettings
 *
 * @ORM\Entity
 * @ORM\Table(indexes={
 *     @ORM\Index(name="email_idx", columns={"email"}),
 *     @ORM\Index(name="subscriptions_idx", columns={"subscription_id"}),
 * })
 * @UniqueEntity(fields={"email"}, message="Email already registered")
 */
class EmailSubscriptionSettings
{
    
    const FREQ_DAILY = 'daily';
    const FREQ_WEAKLY = 'weakly';
    const FREQ_MONTHLY = 'monthly';
    
    const AGE_3_DAYS = 3;
    const AGE_7_DAYS = 7;
    const AGE_14_DAYS = 14;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\UserBundle\Entity\User", inversedBy="emailSubscriptionSettingsList")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;
    
    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Category")
     * @ORM\JoinTable(name="emailsubscriptionsettings_categories",
     *      joinColumns={@ORM\JoinColumn(name="setting_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    private $categories;
    
    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Profession")
     * @ORM\JoinTable(name="emailsubscriptionsettings_professions",
     *      joinColumns={@ORM\JoinColumn(name="setting_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="profession_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    private $professions;
    
    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity")
     * @ORM\JoinTable(name="emailsubscriptionsettings_cities",
     *      joinColumns={@ORM\JoinColumn(name="setting_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="cascade")}
     *      )
     */
    private $cities;
    
    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry")
     * @ORM\JoinTable(name="emailsubscriptionsettings_countries",
     *      joinColumns={@ORM\JoinColumn(name="setting_id", referencedColumnName="id", onDelete="cascade" )},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="cascade")}
     *      )
     */
    private $countries;
    
    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource")
     * @ORM\JoinTable(name="emailsubscriptionsettings_sources",
     *      joinColumns={@ORM\JoinColumn(name="setting_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="source_id", referencedColumnName="id", onDelete="cascade")}
     *      )
     */
    private $sources;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    private $language;


    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $what_value;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $what_slug;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $where_value;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $where_slug;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $filter_key;


    /**
     * @var string
     *
     * @ORM\Column(name="sending_frequency", type="string", length=255, nullable=false)
     */
    private $sending_frequency;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $advertisement_max_age;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $subscription_id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $last_time_sent;

    /**
     * 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $email;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sending_frequency
     *
     * @param string $sendingFrequency
     * @return EmailSubscriptionSettings
     */
    public function setSendingFrequency($sendingFrequency = null)
    {
        $this->sending_frequency = $sendingFrequency;
    
        return $this;
    }

    /**
     * Get sending_frequency
     *
     * @return string 
     */
    public function getSendingFrequency()
    {
        return $this->sending_frequency;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->countries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sources = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->professions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set user
     *
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @return EmailSubscriptionSettings
     */
    public function setUser(\VisiDarbi\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \VisiDarbi\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add cities
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $cities
     * @return EmailSubscriptionSettings
     */
    public function addCitie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $cities)
    {
        $this->cities[] = $cities;
    
        return $this;
    }

    /**
     * Remove cities
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $cities
     */
    public function removeCitie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $cities)
    {
        $this->cities->removeElement($cities);
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Add sources
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $sources
     * @return EmailSubscriptionSettings
     */
    public function addSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $sources)
    {
        $this->sources[] = $sources;
    
        return $this;
    }

    /**
     * Remove sources
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $sources
     */
    public function removeSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $sources)
    {
        $this->sources->removeElement($sources);
    }

    /**
     * Get sources
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * Set language
     *
     * @return User
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    
        return $this;
    }

    /**
     * Get language
     *
     * @return $langueage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set what_value
     *
     * @return User
     */
    public function setWhatValue($what_value)
    {
        $this->what_value = $what_value;
    
        return $this;
    }

    /**
     * Get what_value
     *
     * @return $langueage
     */
    public function getWhatValue()
    {
        return $this->what_value;
    }

    /**
     * Set what_slug
     *
     * @return User
     */
    public function setWhatSlug($what_slug)
    {
        $this->what_slug = $what_slug;
    
        return $this;
    }

    /**
     * Get what_slug
     *
     * @return $langueage
     */
    public function getWhatSlug()
    {
        return $this->what_slug;
    }

    /**
     * Set where_value
     *
     * @return User
     */
    public function setWhereValue($where_value)
    {
        $this->where_value = $where_value;
    
        return $this;
    }

    /**
     * Get where_value
     *
     * @return $langueage
     */
    public function getWhereValue()
    {
        return $this->where_value;
    }

    /**
     * Set where_slug
     *
     * @return User
     */
    public function setWhereSlug($where_slug)
    {
        $this->where_slug = $where_slug;
    
        return $this;
    }

    /**
     * Get where_slug
     *
     * @return $langueage
     */
    public function getWhereSlug()
    {
        return $this->where_slug;
    }

    /**
     * Set filter_key
     *
     * @return User
     */
    public function setFilterKey($filter_key)
    {
        $this->filter_key = $filter_key;
    
        return $this;
    }

    /**
     * Get filter_key
     *
     * @return $filter_key
     */
    public function getFilterKey()
    {
        return $this->filter_key;
    }

    /**
     * Set advertisement_max_age
     *
     * @param string $advertisementMaxAge
     * @return EmailSubscriptionSettings
     */
    public function setAdvertisementMaxAge($advertisementMaxAge)
    {
        $this->advertisement_max_age = $advertisementMaxAge;
    
        return $this;
    }

    /**
     * Set subscription_id
     *
     * @param string $subscriptionId
     * @return EmailSubscriptionSettings
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscription_id = $subscriptionId;

        return $this;
    }

    /**
     * Get advertisement_max_age
     *
     * @return string 
     */
    public function getAdvertisementMaxAge()
    {
        return $this->advertisement_max_age;
    }

    /**
     * Get subscription_id
     *
     * @return string
     */
    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    /**
     * Add countries
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $countries
     * @return EmailSubscriptionSettings
     */
    public function addCountrie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $countries)
    {
        $this->countries[] = $countries;
    
        return $this;
    }

    /**
     * Remove countries
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $countries
     */
    public function removeCountrie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $countries)
    {
        $this->countries->removeElement($countries);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Set last_time_sent
     *
     * @param \DateTime $lastTimeSent
     * @return EmailSubscriptionSettings
     */
    public function setLastTimeSent($lastTimeSent) {
        $this->last_time_sent = $lastTimeSent;

        return $this;
    }

    /**
     * Get last_time_sent
     *
     * @return \DateTime
     */
    public function getLastTimeSent() {
        return $this->last_time_sent;
    }

    /**
     * Add categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     * @return EmailSubscriptionSettings
     */
    public function addCategorie(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     */
    public function removeCategorie(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     * @return EmailSubscriptionSettings
     */
    public function addProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->professions[] = $professions;
    
        return $this;
    }

    /**
     * Remove professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     */
    public function removeProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->professions->removeElement($professions);
    }

    /**
     * Get professions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfessions()
    {
        return $this->professions;
    }
    
    public function generateSubscriptionId() 
    {
        if (empty($this->subscription_id) ) {
            $this->subscription_id = uniqid();
        }
    }


    /**
     * Get cretaed_at
     *
     * @return \DateTime 
     */
    public function getCretaedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Advertisement
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdateddAt() {
        return $this->updated_at;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EmailSubscriptionSettings
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setDefaultAdvertisementMaxAge(){
        $this->advertisement_max_age = self::AGE_14_DAYS;
    }

    public function setDefaultFrequency(){
        $this->sending_frequency = self::FREQ_DAILY;
    }

    public static function getAvailableAgeSettings(){
        return array(
            self::AGE_3_DAYS,
            self::AGE_7_DAYS,
            self::AGE_14_DAYS,
        );
    }

    public static function getAvailableSendingFrequencies(){
        return array(
            self::FREQ_DAILY,
            self::FREQ_WEAKLY,
            self::FREQ_MONTHLY,
        );
    }

}