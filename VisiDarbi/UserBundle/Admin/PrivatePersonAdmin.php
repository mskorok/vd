<?php

namespace VisiDarbi\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use FOS\UserBundle\Model\UserManagerInterface;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Admin\UserAdmin;


class PrivatePersonAdmin extends UserAdmin
{
    protected $classnameLabel = 'Private person';
    protected $baseRouteName = 'privatepersons';
    protected $baseRoutePattern = 'privatepersons';
   
    
    protected function getUserType() 
    {
        return User::TYPE_PRIVATE;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
        ;
    }
   
}