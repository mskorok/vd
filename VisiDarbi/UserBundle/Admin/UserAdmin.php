<?php

namespace VisiDarbi\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use FOS\UserBundle\Model\UserManagerInterface;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

abstract class UserAdmin extends Admin
{
    
    /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;
    

    protected function configureListFields(ListMapper $listMapper)
    {   
        $actions = array(
            'view' => array(),
            'edit' => array(),
            'delete' => array(),
        );
        
        $listMapper
            ->addIdentifier('id')
            ->add('full_name', 'string');
        
        if ($this->getUserType() == User::TYPE_LEGAL) {
            $listMapper
                ->add('legalProfile.company_name',  null, array('label' => 'Company name'))
                ->add('legal_person_adv_count', 'string', array('label' => 'Adv. count'));
            
            $actions['Services'] = array(
                'template' => 'VisiDarbiUserBundle:Admin:list_action_prepaid_services.html.twig'
            );
        }
        
        $listMapper    
            ->add('email')
            ->add('created_at', null, array('label' => 'Created at'))
            ->add('enabled', null, array('editable' => true, 'label' => 'Confirmed'))
            ->add('locked', null, array('editable' => true))
            ->add('_action', 'actions', array(
                'actions' => $actions,
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        if ($this->getUserType() == User::TYPE_LEGAL) {
            $filterMapper->add('legalProfile.company_name', null, array('label' => 'Company name'));
        }
        
        $filterMapper
            ->add('full_name')
            ->add('email', null, array('label' => 'Email')) 
            ->add('enabled', 'doctrine_orm_choice', array('label' => 'Confirmation status',
                'field_options' => array(
                    'required' => false,
                    'choices' => array(
                        1 => 'Confirmed', 
                        0 => 'Not confirmed',
                    )
                ),
                'field_type' => 'choice'
            ))
            ->add('locked', 'doctrine_orm_choice', array('label' => 'Lock status',
                'field_options' => array(
                    'required' => false,
                    'choices' => array(
                        1 => 'Locked', 
                        0 => 'Not locked',
                    )
                ),
                'field_type' => 'choice'
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->hasSubject()) {
            
            $user = $this->getSubject();
            /* @var $user \VisiDarbi\UserBundle\Entity\User */
            
            if ($user->isPrivate()) {
                $showMapper
                    ->with('General')
                        ->add('full_name', 'text', array('label' => 'Full name') )                   
                        ->add('email', 'text', array('label' => 'Email'))
                        ->add('phone', 'text', array('label' => 'Phone'))
                    ->end()
                    ->with('Address')
                        ->add('privateProfile.address', 'text', array('label' => 'Address'))
                        ->add('privateProfile.address_postcode', 'text', array('label' => 'Postcode'))
                    ->end()
                    ->with('Other')
                        ->add('enabled', 'boolean', array('label' => 'Account confirmed'))
                        ->add('created_at', 'datetime', array('label' => 'Created at'))
                        ->add('last_login', 'datetime', array('label' => 'Last login'))
                    ->end()
                ;    
            }
            
            if ($user->isLegal()) {
                $showMapper
                    ->with('General')
                        ->add('full_name', 'text', array('label' => 'Full name') )
                        ->add('legalProfile.company_name', 'text', array('label' => 'Company name')) 
                        ->add('legalProfile.registration_number', 'text', array('label' => 'Registration number'))
                        ->add('email', 'text', array('label' => 'Email'))
                        ->add('phone', 'text', array('label' => 'Phone'))
                    ->end()
                    ->with('Legal address')
                        ->add('legalProfile.legal_address', 'text', array('label' => 'Address'))
                        ->add('legalProfile.legal_address_postcode', 'text', array('label' => 'Postcode'))
                    ->end()
                    ->with('Factual address')
                        ->add('legalProfile.factual_address', 'text', array('label' => 'Address'))
                        ->add('legalProfile.factual_address_postcode', 'text', array('label' => 'Postcode'))
                    ->end()
                    ->with('Other')
                        ->add('enabled', 'boolean', array('label' => 'Account confirmed'))
                        ->add('created_at', 'datetime', array('label' => 'Created at'))
                        ->add('last_login', 'datetime', array('label' => 'Last login'))
                    ->end()
                ;  
            }
            
            $showMapper
                ->with('Other')
                    ->add('country.name', 'text', array('label' => 'Registered in country'))
                ->end()
            ;
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {   
        if ($this->hasSubject()) {
            
            $user = $this->getSubject();
            /* @var $user \VisiDarbi\UserBundle\Entity\User */

            if ($user->isPrivate()) {
                $formMapper
                    ->with('Private person')
                        ->add('full_name', 'text', array('required' => true))
                        
                        ->add('address', 'text', array('property_path' => 'privateProfile.address', 'required' => true))
                        ->add('address_postcode', 'text', array(
                            'property_path' => 'privateProfile.address_postcode', 
                            'required' => true,
                            'label' => 'Postcode'
                        ))
                        
                        ->add('email', 'text', array('required' => true))
                        ->add('phone', 'text', User::phoneValidationParams(array('required' => true)))
                    ->end()
                ;    
            }
            
            if ($user->isLegal()) {
                $formMapper
                    ->with('Legal person')
                        ->add('full_name', 'text', array('required' => true))
                        ->add('company_name', 'text', array('property_path' => 'legalProfile.company_name', 'required' => true))
                        ->add('vat_number', 'text', User::vatValidationParams(array('property_path' => 'legalProfile.vat_number', 'required' => true,)))
                        ->add('registration_number', 'text', User::registrationNumValidationParams(array('property_path' => 'legalProfile.registration_number', 'required' => true)))
                        
                        ->add('legal_address', 'text', array('property_path' => 'legalProfile.legal_address', 'required' => true))
                        ->add('legal_address_postcode', 'text', array(
                            'property_path' => 'legalProfile.legal_address_postcode', 
                            'required' => true,
                            'label' => 'Postcode'
                        ))
                        
                        ->add('factual_address', 'text', array('property_path' => 'legalProfile.factual_address', 'required' => true))
                        ->add('factual_address_postcode', 'text', array(
                            'property_path' => 'legalProfile.factual_address_postcode', 
                            'required' => true,
                            'label' => 'Postcode'
                        ))
                        
                        ->add('email', 'text', array('required' => true))
                        ->add('phone', 'text', User::phoneValidationParams(array('required' => true)))
                    ->end()
                ;   
            }
        }
    }
    
    
    public function validate(ErrorElement $errorElement, $object)
    {
        $phone_params = User::phoneValidationParams();

        $errorElement
            ->with('email')
                ->assertEmail(array('message' => 'Invalid email address specified.'))
            ->end()
            ->with('phone')
                ->assertMaxLength(array('limit' => $phone_params['max_length'], 'message'=>'[0,Inf]Phone must not be longer than {{ limit }} characters'))
            ->end()
        ;
        
        if ($object->isLegal()) {
            $reg_num_params = User::registrationNumValidationParams(array(), 'admin');
            $vat_params = User::vatValidationParams(array(), 'admin');

            $errorElement
                ->with('legalProfile.registration_number')
                    ->assertLength(array('max' => $reg_num_params['max_length'], 'min'=>$reg_num_params['min_length'], 'minMessage'=>'[0,Inf]Registration number must be more or equal to {{ limit }} characters', 'maxMessage'=>'[0,Inf]Registration number must not be longer than {{ limit }} characters'))
                    ->assertRegex(array('pattern' => $reg_num_params['pattern'], 'message'=>'Registration number must contain only alphanumeric symbols.'))
                ->end()
                ->with('legalProfile.vat_number')
                    ->assertMaxLength(array('limit' => $vat_params['max_length'], 'message'=>'[0,Inf]VAT number must not be longer than {{ limit }} characters'))
                    ->assertRegex(array('pattern' => $vat_params['pattern'], 'message'=>'VAT number must contain only alphanumeric symbols.'))
                ->end()
            ;

            if(!$object->getPassword()){

                $errorElement
                    ->with('plainPassword')
                    ->assertNotNull(array('message' => 'Password required'))
                    ->end();
            }
        }


    }
    
    //public function prePersist($user) 
    //{
    //    $user->setUsername($user->getEmail());
    //}

    public function preUpdate($user)
    {
        //$user->setUsername($user->getEmail());
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }
   
    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }
    
    
    public function getTemplate($name)
    {
        return parent::getTemplate($name);
    }
    
    public function createQuery($context = 'list')
    {
        $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
        $query->select('u, lp, pp, s');
        $query->leftJoin('u.legalProfile', 'lp');
        $query->leftJoin('u.privateProfile', 'pp');
        $query->leftJoin('u.emailSubscriptionSettingsList', 's');
        $query->where('u.is_admin = 0');
        $query->andWhere('u.type = :type');
        $query->setParameter(':type', $this->getUserType());
        //$query->orderBy('u.id', 'DESC');
        
        if ($context == 'list') {
            $query->select('u, lp, pp, s');
            $query->leftJoin('u.advertisements', 'a');
            //$query->groupBy('u.id');
        }
        
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }
    
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'id' // field name
    );
    
    protected abstract function getUserType();


    protected function getLocales() 
    {
        $locales = array();
        $country = $this->countryManager->getCurrentAdminCountry();
        foreach ($country->getLanguages() as $lng) {
            $locales[$lng->getLocale()] = $lng->getName();
        }
        
        return $locales;
    }
    

}