<?php

namespace VisiDarbi\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;
use FOS\UserBundle\Model\UserManagerInterface;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Admin\UserAdmin;
use VisiDarbi\UserBundle\Entity\LegalProfile;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LegalPersonAdmin extends UserAdmin
{

    protected $classnameLabel = 'Legal person';
    protected $baseRouteName = 'legalpersons';
    protected $baseRoutePattern = 'legalpersons';

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     */
    protected $settingsManager;
    private $lastUpdatedPassword = null;

    protected function getUserType()
    {
        return User::TYPE_LEGAL;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $user = $this->getSubject();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        $formMapper
            ->with('Legal person')
            ->add('default_locale', 'choice', array(
                'choices' => $this->getLocales(),
                'required' => false,
                'label' => 'Language',
            ))
            ->add('full_name', 'text', array('required' => true))
            ->add('company_name', 'text', array('property_path' => 'legalProfile.company_name', 'required' => true))
            ->add('vat_number', 'text', User::vatValidationParams(array('property_path' => 'legalProfile.vat_number', 'required' => false,)))
            ->add('registration_number', 'text', User::registrationNumValidationParams(array('property_path' => 'legalProfile.registration_number', 'required' => false)))
            ->add('legal_address', 'text', array('property_path' => 'legalProfile.legal_address', 'required' => false))
            ->add('legal_address_postcode', 'text', array(
                'property_path' => 'legalProfile.legal_address_postcode',
                'required' => false,
                'label' => 'Postcode'
            ))
            ->add('factual_address', 'text', array('property_path' => 'legalProfile.factual_address', 'required' => false))
            ->add('factual_address_postcode', 'text', array(
                'property_path' => 'legalProfile.factual_address_postcode',
                'required' => false,
                'label' => 'Postcode'
            ))
            ->add('email', 'text', array('required' => true))
            ->add('phone', 'text', User::phoneValidationParams(array('required' => false)))
            ->add('password', 'repeated', array(
                'property_path' => 'plainPassword',
                'required' => $this->getSubject()->getId() === null,
                'type' => 'password',
                'invalid_message' => 'user.passwords.not_match',
                'first_options' => array('label' => 'Password', 'attr' => array('class' => 'password-first-fld span3')),
                'second_options' => array('label' => 'Password repeat', 'attr' => array('class' => 'password-second-fld span3')),
            ))
            ->end()
        ;
    }

    /**
     * overwrite default assigned validation groups to skip some validators in admin mode
     * @return type
     */
    public function getFormBuilder()
    {
        $this->formOptions = array('validation_groups' => array('Default', 'AdminEdit'));
        $formBuilder = parent::getFormBuilder();
        return $formBuilder;
    }

    public function getNewInstance()
    {

        $user = parent::getNewInstance();
        $user->setCountry($this->countryManager->getCurrentAdminCountry());
        $user->setType(User::TYPE_LEGAL);
        $user->setLegalProfile(new LegalProfile());
        $user->setEnabled(true);

        return $user;
    }

    public function preUpdate($user)
    {
        $this->lastUpdatedPassword = $user->getPlainPassword();
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    public function prePersist($user)
    {
        $this->lastUpdatedPassword = $user->getPlainPassword();
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    public function postUpdate($user)
    {
        if (!empty($this->lastUpdatedPassword)) {
            $this->sendPasswordChangeNotification($user, $this->lastUpdatedPassword);
        }
    }

    public function postPersist($user)
    {
        if (!empty($this->lastUpdatedPassword)) {
            $this->sendPasswordChangeNotification($user, $this->lastUpdatedPassword);
        }
    }

    private function sendPasswordChangeNotification($user, $password)
    {
        $country = $this->countryManager->getCurrentAdminCountry();

        /* Edited user email */
        $email = $user->getEmail();

        /* Sending notification mail to user */
        $this->emailSender->send(
            $country, $country->getDefaultLocale(), EmailTemplate::ID_PASSWORD_UPDATED, $email, array(
            'password' => $password,
            ), array(
            'email' => $this->settingsManager->get('emails.sender_email'),
            'name' => $this->settingsManager->get('emails.sender_name')
            )
        );

        /* Site info email */
        $siteContactsEmail = $this->settingsManager->get('contacts.info_email', $country->getDefaultLocale());
        if (empty($siteContactsEmail)) {
            throw new \Exception('Site contact recipient email is not configured');
        }

        /* Sending notification mail to site administration */
        $this->emailSender->send(
            $country, $country->getDefaultLocale(), EmailTemplate::ID_PASSWORD_UPDATED, $siteContactsEmail, array(
            'password' => sprintf(
                /* sending username and company name as password to be able to use same template as for user */
                "%s (User: %s. Company: %s)", $password, $user->getEmail(), $user->getLegalProfile()->getCompanyName()
            ),
            ), array(
            'email' => $this->settingsManager->get('emails.sender_email'),
            'name' => $this->settingsManager->get('emails.sender_name')
            )
        );
    }

    public function setEmailSender($emailSender)
    {
        $this->emailSender = $emailSender;
    }

    public function setSettingsManager($settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(), array('VisiDarbiUserBundle:Admin:legalperson_form_theme.html.twig')
        );
    }

    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'VisiDarbiUserBundle:Admin:generated_password_edit.html.twig';
        }
        return parent::getTemplate($name);
    }

}
