<?php

namespace VisiDarbi\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

use FOS\UserBundle\Model\UserManagerInterface;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;


class AdministratorAdmin extends Admin
{
     /**
     * @var CountryManagerInterface 
     */
    protected $countryManager;

    protected $classnameLabel = 'Administrator';
    protected $baseRouteName = 'administrators';
    protected $baseRoutePattern = 'administrators';
    
    protected $adminRoles = array(
        'ROLE_ADMIN' => 'Regular admin',
        'ROLE_SUPER_ADMIN' => 'Superadmin'
    );


    protected function configureRoutes(RouteCollection $collection)
    {
        
    }

    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }

    protected function configureListFields(ListMapper $listMapper)
    {  
        $listMapper
            ->addIdentifier('id')
            ->add('username')
            ->add('singleRole', 'string', array(
                'label' => 'Role',
                'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
                'dictionary' => $this->adminRoles
            ))
            ->add('adminCountries', 'string', array(
                'label' => 'Admin in countries',
                'template' => 'VisiDarbiUserBundle:UserAdmin:list_countries.html.twig',
            ))
            ->add('created_at', 'datetime', array('label' => 'Created at'))
            ->add('last_login', 'datetime', array('label' => 'Last login'))
            ->add('locked', null, array('editable' => true))
            ->add('_action', 'actions', array(
                'actions' => array(
                    //'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper

        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {   
        $formMapper
            ->with('Site administrator')
                ->add('locked', null, array( 'required' => false ))
                ->add('full_name', 'text', array('required' => true))
                ->add('singleRole', 'choice', array(
                        'choices' => $this->adminRoles,
                        'required' => true,
                        'label' => 'Role',
                        'attr' => array('class' => 'role-selector span5')
                ))
                ->add('adminCountries', null, array(
                        //'choices' => $this->adminRoles,
                        //'required' => true,
                        //'multiple' => true,
                        'expanded' => true,
                        'label' => 'Admin in countries',
                        'attr' => array('class' => 'country-selector span5')
                ))
                ->add('email')
                ->add('username', null,  array(
                    'required' => true,
                    'label' => 'Username',
                ))
                ->add('password', 'repeated', array(
                    'property_path' => 'plainPassword',
                    'required' => $this->getSubject()->getId() === null,
                    'type' => 'password',
                    'invalid_message' => 'user.passwords.not_match',
                    'first_options'  => array('label' => 'Password'),
                    'second_options' => array('label' => 'Password repeat'),
                )) 
            ->end()
        ;   
        
    }
    
    
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('email')
                ->assertEmail(array('message' => 'Invalid email address specified.'))
            ->end()
        ;
        
        if ($object->getSingleRole() === 'ROLE_ADMIN') {
            $errorElement
                ->with('adminCountries')
                    ->assertCount(array('min' => 1,  'minMessage' => 'Admin with such role at least one country must be specified.'))
                ->end()
            ;
        }

        $errorElement
            ->with('username')
            ->assertNotNull(array('message' => 'Username required.'))
            ->end()
        ;

        if(!$object->getPassword()){

            $errorElement
                ->with('plainPassword')
                ->assertNotNull(array('message' => 'Password required'))
                ->end();
        }
    }
    
    public function getNewInstance() 
    {
        $user = parent::getNewInstance();
        $user->setEnabled(true);
        $user->setType(User::TYPE_PRIVATE);
        $user->setPhone('Null');
        //$user->setPrivateProfile(new PrivateProfile());
        $user->setIsAdmin(true);
        
        return $user;
    }
    
    public function prePersist($user) 
    {
        $this->setRole($user);
        $user->setDefaultLocale($this->getDefaultCountryDomain());
    }

    public function preUpdate($user)
    {
        //$user->setUsername($user->getEmail());
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
        
        $this->setRole($user);
    }
    
    private function setRole(User $user) 
    {
        //$user->setRoles(
        //    array( $user->getSingleRole() )
        //);
        
        if ($user->getSingleRole() == 'ROLE_SUPER_ADMIN') {
            foreach ($user->getAdminCountries() as $ac) {
                $user->removeAdminCountry($ac);
            }
        }
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }
    
    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'VisiDarbiUserBundle:AdministratorAdmin:edit.html.twig';
        }
        return parent::getTemplate($name);
    }
    
    public function createQuery($context = 'list')
    {
        $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
        $query->select('u, lp, pp');
        $query->leftJoin('u.legalProfile', 'lp');
        $query->leftJoin('u.privateProfile', 'pp');
        $query->where('u.is_admin = 1');
        $query->orderBy('u.id', 'DESC');
        
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

    protected function getDefaultCountryDomain() 
    {
        $locales = array();
        $country = $this->countryManager->getCurrentAdminCountry();
        foreach ($country->getLanguages() as $lng) {
            return $lng->getLocale();
        }
    }
}