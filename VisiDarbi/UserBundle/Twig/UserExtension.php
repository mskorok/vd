<?php

namespace VisiDarbi\UserBundle\Twig;

use Twig_Extension;
use Twig_Function_Method;
use Doctrine\ORM\EntityManager;

class UserExtension extends Twig_Extension
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * Total registered user count
     * @var integer
     */
    private $userTotalCnt = null;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return array(
            'total_user_count' => new Twig_Function_Method($this, 'getTotalUserCount'),
        );
    }

    /**
     * returns total regisered users
     * @return integer
     */
    public function getTotalUserCount()
    {
        if ($this->userTotalCnt !== null) {
           return $this->userTotalCnt;
        }

        $query = $this->em->createQuery('SELECT COUNT(u.id) FROM VisiDarbiUserBundle:User u WHERE u.enabled = :enabled AND u.is_admin = :is_admin AND u.type = :user_type');
        $query->setParameter('enabled', true);
        $query->setParameter('is_admin', false);
        $query->setParameter('user_type', "private");
        $query->useResultCache(true, 300, 'user_total_count');

        $this->userTotalCnt = $query->getSingleScalarResult();

        $queryCacheProfile = $query->getQueryCacheProfile();

        return $this->userTotalCnt;
    }

    public function getName()
    {
        return 'visidarbi_user';
    }
}
