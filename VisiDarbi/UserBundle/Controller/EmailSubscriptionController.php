<?php

namespace VisiDarbi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\DiExtraBundle\Annotation as DI;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings;
use Doctrine\ORM\Query\Expr;

class EmailSubscriptionController extends Controller
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;
    
    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender") 
     */
    protected $emailSender;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;

    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator")
     */
    protected $paginator;

    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     * @DI\Inject("translator") 
     */
    protected $translator;
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;

    /**
     * @var \VisiDarbi\AdvertisementBundle\Manager\AdvertisementManagerInterface
     * @DI\Inject("visidarbi.advertisement_manager")
     */
    protected $advertisementManager;
    
    
    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;

    const SUBSCRIPTION_ID_LENGTH = 30;

    /**
     * @Route("/unsubscribe/{subscriptionId}", name="unsubscribe_from_news" )
     * @Template()
     */
    public function unsubscribeAction(Request $request)
    {
        $subscriptionId = $request->get('subscriptionId');

        $subscription = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->findOneBy(array('subscription_id' => $subscriptionId));
        /* @var $subscription \VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings */

        $isUnsubscribed = false;
        if ($this->request->isMethod('POST')) {

            $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->createQueryBuilder('s')
                ->delete()
                ->where('s.subscription_id = :sid')
                ->setParameter('sid', $subscriptionId)
                ->getQuery()
                ->execute()
            ;

            $isUnsubscribed = true;
        }

        if ($subscription) {
            return $this->render('VisiDarbiUserBundle:EmailSubscription:unsubscribe.html.twig', array(
                'isUnsubscribed' => $isUnsubscribed
                ));
        }
        else {
            return $this->redirect($this->generateUrl('index'));
        }
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/save-email-subscription", name="save_email_subscription" )
     * @Template()
     */
    public function saveEmailSubscriptionAction(Request $request)
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (!$user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }

        if (!$this->request->isMethod('POST')) {
            throw new NotFoundHttpException('Access denied');
        }

        $emailSubscriptionId = $request->get('emailSubscriptionId');

        $data = $this->getSubscriptionFormDefaultData();

        if ($emailSubscriptionId) {
            $emailSubscriptionSettingsRepo = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings');
            $subscriptionEntity = $emailSubscriptionSettingsRepo->findOneBy(array(
                'id' => $emailSubscriptionId,
                'user' => $user
            ));
        } else {
            $subscriptionEntity = new EmailSubscriptionSettings();
            $subscriptionEntity->setUser($user);
        }

        if(!$subscriptionEntity){
            throw new NotFoundHttpException('Subscription not found');
        }

        $data['settings'] = $subscriptionEntity;

        $data['professionListIds'] = $subscriptionEntity->getProfessions()->map(function ($entity) {
            return $entity->getId();
        })->toArray();


        $categoryIds = $this->request->get('category_ids', array());
        $professionIds = $this->request->get('profession_ids', array());
        $sourceIds = $this->request->get('source_ids', array());
        $maxAge = $this->request->get('advertisement_max_age');
        $keywords = $this->request->get('keywords');
        $freq = $this->request->get('sending_frequency');

        $allLocations = $this->request->get('all_locations') !== null;
        $allSources = $this->request->get('all_sources') !== null;

        $what_value = $this->request->get('what_value', null);
        $where_value = $this->request->get('where_value', null);

        $subscriptionEntity->getCategories()->clear();
        if (!empty($categoryIds)) {
            foreach ($categoryIds as $catId) {
                $filtered = array_filter($data['categories'], function ($c) use ($catId) {
                    return $catId == $c->getId();
                });
                $category = reset($filtered);
                if ($category) {
                    $subscriptionEntity->addCategorie($category);
                }
            }
        }

        $professions = $this->getProfessions($subscriptionEntity->getCategories());
        $subscriptionEntity->getProfessions()->clear();
        if (!empty($professionIds)) {
            foreach ($professionIds as $profId) {
                $filtered = array_filter($professions, function ($p) use ($profId) {
                    return $profId == $p->getId();
                });
                $profession = reset($filtered);
                if ($profession) {
                    $subscriptionEntity->addProfession($profession);
                }
            }
        }

        $cityIds = array();
        $countryIds = array();
        foreach ($this->request->get('city_ids', array()) as $cid) {
            if (strpos($cid, 'city-') !== false) {
                $cityIds[] = str_replace('city-', '', $cid);
            }
            if (strpos($cid, 'country-') !== false) {
                $countryIds[] = str_replace('country-', '', $cid);
            }
        }

        $subscriptionEntity->getCities()->clear();
        if ($allLocations) {
            foreach ($data['cities'] as $city) {
                $subscriptionEntity->addCitie($city);
                $cityIds[] = $city->getId();
            }
        } elseif (!empty($cityIds)) {
            foreach ($cityIds as $cid) {
                $filtered = array_filter($data['cities'], function ($c) use ($cid) {
                    return $cid == $c->getId();
                });
                $city = reset($filtered);
                if ($city) {
                    $subscriptionEntity->addCitie($city);
                }
            }
        }

        $subscriptionEntity->getCountries()->clear();
        if ($allLocations) {
            foreach ($data['countries'] as $country) {
                $subscriptionEntity->addCountrie($country);
                $countryIds[] = $country->getId();
            }
        } elseif (!empty($countryIds)) {
            foreach ($countryIds as $cid) {
                $filtered = array_filter($data['countries'], function ($c) use ($cid) {
                    return $cid == $c->getId();
                });
                $country = reset($filtered);
                if ($country) {
                    $subscriptionEntity->addCountrie($country);
                }
            }
        }

        $subscriptionEntity->getSources()->clear();
        if ($allSources) {
            foreach ($data['sources'] as $source) {
                $subscriptionEntity->addSource($source);
                $sourceIds[] = $source->getId();
            }
        } elseif (!empty($sourceIds)) {
            foreach ($sourceIds as $sid) {
                $filtered = array_filter($data['sources'], function ($s) use ($sid) {
                    return $sid == $s->getId();
                });
                $source = reset($filtered);
                if ($source) {
                    $subscriptionEntity->addSource($source);
                }
            }
        }

        if (in_array($maxAge, $data['ages'])) {
            $subscriptionEntity->setAdvertisementMaxAge($maxAge);
        } else {
            $subscriptionEntity->setDefaultAdvertisementMaxAge();
        }

        if (in_array($freq, $data['sendingFrequencies'])) {
            $subscriptionEntity->setSendingFrequency($freq);
        } else {
            $subscriptionEntity->setDefaultFrequency();
        }


        $subscriptionEntity->generateSubscriptionId();
        $subscriptionEntity->setUpdatedAt(new \DateTime());

        //Lets genereta what / where / filter_key value

        if (!empty($what_value)) {

            $whatValues = $this->advertisementManager->processTextSearchSlug('what', $what_value, $this->request->getLocale(), false);

            $subscriptionEntity->setWhatValue($whatValues['value']);
            $subscriptionEntity->setWhatSlug($whatValues['slug']);
        } else {
            $subscriptionEntity->setWhatValue(null);
            $subscriptionEntity->setWhatSlug(null);
        }

        if (!empty($where_value)) {

            $whereValues = $this->advertisementManager->processTextSearchSlug('where', $where_value, $this->request->getLocale(), false);

            $subscriptionEntity->setWhereValue($whereValues['value']);
            $subscriptionEntity->setWhereSlug($whereValues['slug']);
        } else {
            $subscriptionEntity->setWhereValue(null);
            $subscriptionEntity->setWhereSlug(null);
        }

        if ($cityIds || !empty($countryIds) || !empty($categoryIds) || !empty($professionIds) || !empty($sourceIds)) {

            $filterData = array(
                'cities' => $cityIds,
                'countries' => $countryIds,
                'positions' => $categoryIds,
                'professions' => $professionIds,
                'resources' => $sourceIds
            );

            $filteredData = array();
            foreach ($filterData as $key => $valueArray) {
                $filteredValues = array();
                if ($valueArray && is_array($valueArray)) {
                    foreach ($valueArray as $value) {
                        if ((int)$value && !isset($filteredValues[(int)$value])) {
                            $filteredValues[(int)$value] = true;
                        }
                    }
                }
                $filteredData[$key] = $filteredValues;
            }

            $filter_decoded = $this->advertisementManager->encodeFilter($filteredData);

            $filter_key = $filter_decoded['key'];

            $subscriptionEntity->setFilterKey($filter_key);
        }

        $subscriptionEntity->setLanguage($this->request->getLocale());

        $this->em->persist($subscriptionEntity);
        $this->em->flush();

        $user->addEmailSubscriptionSettingsListItem($subscriptionEntity);

        $this->get('session')->getFlashBag()->add('email_settings_saved', true);

        return $this->redirect($this->generateUrl('list_email_subscription'));

    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/email-subscription-list", name="list_email_subscription")
     * @Template()
     */
    public function listAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }

        /**
         * Edit form
         */

        $data = $this->getSubscriptionFormDefaultData();

        if(!$subscriptionEntity = $user->getEmailSubscriptionSettingsList()->first()){
            $subscriptionEntity = new EmailSubscriptionSettings();
            $subscriptionEntity->setUser($user);
        }

        $data['settings'] = $subscriptionEntity;

        $data['professionListIds'] = $subscriptionEntity->getProfessions()->map(function($entity)  {
            return $entity->getId();
        })->toArray();

        /**
         * Listing
         */
        $emailSubscriptionRepo = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings');

        $qb = $emailSubscriptionRepo->createQueryBuilder('e')
            ->select('e')
//            ->join('e.categories', 'c')
//            ->join('e.cities', 'ci')
//            ->join('e.professions', 'p')
//            ->join('e.sources', 's')
            ->where('e.user = :user')
            ->setParameter('user', $user->getId())
            ->orderBy('e.created_at', 'DESC')
        ;

        $query = $qb->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $pagination = $this->paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),
            10
        );

        return array_merge($data, array(
            'pagination' => $pagination
        ));
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/email-subscription-list/new", name="new_email_subscription", options={"i18n"="false","expose"=true} )
     */
    public function newEmailSubscriptionAction(Request $request)
    {
        $request = $this->getRequest();

        return $this->forward('VisiDarbiUserBundle:EmailSubscription:editEmailSubscription', array('request' => $request));
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/email-subscription-list/edit", name="edit_email_subscription", options={"i18n"="false","expose"=true} )
     */
    public function editEmailSubscriptionAction(Request $request)
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }

        if($request->isXmlHttpRequest()) {

            /**
             * Edit form
             */

            $emailSubscriptionId = $request->get('emailSubscriptionId');

            $data = $this->getSubscriptionFormDefaultData();

            if ($emailSubscriptionId) {
                $emailSubscriptionSettingsRepo = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings');
                $subscriptionEntity = $emailSubscriptionSettingsRepo->findOneBy(array(
                    'id' => $emailSubscriptionId,
                    'user' => $user
                ));
            } else {
                $subscriptionEntity = new EmailSubscriptionSettings();
                $subscriptionEntity->setUser($user);

                $subscriptionEntity->setDefaultAdvertisementMaxAge();
                $subscriptionEntity->setDefaultFrequency();
            }

            if(!$subscriptionEntity){
                throw new NotFoundHttpException('Subscription not found');
            }

            $data['settings'] = $subscriptionEntity;

            $data['professionListIds'] = $subscriptionEntity->getProfessions()->map(function($entity)  {
                return $entity->getId();
            })->toArray();

            return $this->render('VisiDarbiUserBundle:EmailSubscription:form.html.twig', $data);
        }

    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/email-subscription-list/delete", name="delete_email_subscription", defaults={"_format"="json"}, options={"i18n"="false"} )
     */
    public function deleteAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */


        $id = $this->request->get('emailSubscriptionId');
        if (empty($id)) {
            throw new NotFoundHttpException('Empty advertisement search history id.');
        }

        $emailSubscriptionSettingsRepo = $this->em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings');
        $emailSubscriptionSettingsItem = $emailSubscriptionSettingsRepo->findOneBy(array(
            'id' => $id,
            'user' => $user
        ));

        if ($emailSubscriptionSettingsItem == null) {
            throw new NotFoundHttpException('Invalid email subscription settings id.');
        }

        $this->em->remove($emailSubscriptionSettingsItem);
        $this->em->flush();

        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }

    private function getSubscriptionFormDefaultData(){

        $app_country = $this->countryManager->getCurrentCountry();
        $app_adv_default_country = $app_country->getDeafultAdvertisementCountry();

        $categories = $this->getCategories($app_country);
        $countries = $this->getCountries($app_country);
        $cities = $app_adv_default_country !== null ? $this->getCities($app_adv_default_country) : array();
        $sources = $this->getSources($app_country);
        $ages = EmailSubscriptionSettings::getAvailableAgeSettings();
        $sendingFrequencies = EmailSubscriptionSettings::getAvailableSendingFrequencies();

        return array(
            'countries' => $countries,
            'cities' => $cities,
            'categories' => $categories,
            'sources' => $sources,
            'ages' => $ages,
            'sendingFrequencies' => $sendingFrequencies,
        );
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/get-professions", name="get_professions", defaults={"_format"="json"} )
     */
    public function getProfessionsAction()
    {
        $categoryId = $this->request->get('categoryId');
        if (empty($categoryId)) {
            throw new NotFoundHttpException('Empty category id.');
        }
        
        $category = $this->em->getRepository('VisiDarbiProfessionBundle:Category')->find($categoryId);
        if ($category === null) {
            throw new NotFoundHttpException('Invalid category id.');
        }
        
        $professions = array();
        foreach ($this->getProfessions($category) as $prof) {
            $professions[] = array(
                'id' => $prof->getId(),
                'name' => $prof->getName(),
            );
        }
        
        return new JsonResponse(array(
            'status' => 'OK',
            'professions' => $professions,
        )); 
    }
    
    protected function getProfessions($categories) 
    {
        $categoryIds = array();

        foreach ($categories as $category) {
            $categoryIds[] = $category instanceof \VisiDarbi\ProfessionBundle\Entity\Category ? $category->getId() : $category;
        }
        
        if (empty($categoryIds)) {
            return $categoryIds;
        }

        $professionQb =  $this->em->getRepository('VisiDarbiProfessionBundle:Profession')
            ->createQueryBuilder('p')
            ->where('p.category IN (:catIds)')
            ->setParameter('catIds', $categoryIds)
            ->orderBy('p.name','ASC')
        ;
        
        $professionQuery = $professionQb->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        return $professionQuery->getResult();
    }
    
    protected function getCategories($country) 
    {   
       $categoryQb = $this->em->getRepository('VisiDarbiProfessionBundle:Category')
           ->createQueryBuilder('c')
           ->where('c.country = :country')
           ->setParameter('country', $country->getId())
            ->orderBy('c.name','ASC')
       ;

       $categoryQuery = $categoryQb->getQuery()
           ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
           ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
           ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
       ;
       
       return $categoryQuery->getResult();
    }
     
    protected function getCities($advCountry)
    {
       $citiesQb = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity')
            ->createQueryBuilder('c')
            ->where('c.AdvertisementCountry = :ac')
            ->setParameter('ac', $advCountry->getId())
            ->orderBy('c.name','ASC')
       ;

       $citiesQuery = $citiesQb->getQuery()
           ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
           ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
           ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
       ;

       return $citiesQuery->getResult();
    }
    
    protected function getCountries($country)
    {
       $qb = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCountry')
           ->createQueryBuilder('c')
            ->orderBy('c.name','ASC')
       ;
       
       if ($country->getDeafultAdvertisementCountry() !== null) {
           $qb->where('c.id <> :id')->setParameter('id', $country->getDeafultAdvertisementCountry()->getId());
       }
       
       
       $query = $qb->getQuery()
           ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
           ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
           ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
       ;
       return $query->getResult();
    }
     
    protected function getSources(Country $country = null)
    {
        $country_id = $country ? $country->getId() : 1;
        $query = $this->em->getRepository('VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementSource')
                ->createQueryBuilder('s')
                ->where('s.show_advertisiment = :show')->setParameter('show', 1)
                ->orderBy('s.show_as', 'ASC');
        if($country){
            $query = $query->andWhere('s.country = :country_id')->setParameter('country_id', $country_id);
        }
        $query = $query->getQuery()->getResult();
        return $query;
    }
}