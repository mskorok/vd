<?php

namespace VisiDarbi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\DiExtraBundle\Annotation as DI;

use VisiDarbi\UserBundle\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

use JMS\SecurityExtraBundle\Annotation\Secure;

use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

class SavedAdvertismentController extends Controller
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;
    
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator") 
     */
    protected $paginator;
    
    
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/saved-advertisments", name="saved_advertisments" )
     * @Template()
     */
    public function listAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        
        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }
                
        $favAdvRepo = $this->em->getRepository('VisiDarbiUserBundle:UserFavoriteAdvertisement');
        
        $qb = $favAdvRepo->createQueryBuilder('favAdv')
            ->select('favAdv, a, adcountry, adcity')
            ->innerJoin('favAdv.advertisement', 'a')
            ->leftJoin('a.AdvertisementCountry', 'adcountry')
            ->leftJoin('a.AdvertisementCity', 'adcity')
            ->where('favAdv.User = :user')
            ->setParameter('user', $user->getId()) 
            ->orderBy('favAdv.id', 'DESC')
            //->where('a.country = :country')
            //->setParameter('country', $country->getId())
            //->andWhere('a.status = :status')
            //->setParameter('status', Advertisement::STATUS_ACTIVE)
        ;
        
        $query = $qb->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        $pagination = $this->paginator->paginate(
            $query, 
            $this->get('request')->query->get('page', 1), 
            20
        );
                
        return array(
            'pagination' => $pagination
        );
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/saved-advertisments/delete", name="delete_saved_advertisment", defaults={"_format"="json"} )
     */
    public function deleteAction()
    {

        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        
        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }
        
        $id = $this->request->get('favId');
        if (empty($id)) {
            throw new NotFoundHttpException('Empty favorited advertisement id.');
        }

        $favAdvRepo = $this->em->getRepository('VisiDarbiUserBundle:UserFavoriteAdvertisement');
        $favAdv = $favAdvRepo->findOneBy(array(
            'id' => $id,
            'User' => $user
        ));
        
        if ($favAdv == null) {
            throw new NotFoundHttpException('Invalid favorited advertisement id.');
        }
        
        $this->em->remove($favAdv);
        $this->em->flush();
        
        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }
}
