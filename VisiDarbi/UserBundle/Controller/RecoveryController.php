<?php

namespace VisiDarbi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

//use FOS\UserBundle\Controller\RegistrationController as Controller;

use JMS\DiExtraBundle\Annotation as DI;

use VisiDarbi\UserBundle\Entity\User;

use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

class RecoveryController extends Controller
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;
    
    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender") 
     */
    protected $emailSender;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     * @DI\Inject("translator") 
     */
    protected $translator;
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    
    /** 
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;
    
    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     * @DI\Inject("security.encoder_factory")
     */
    protected $encoderFactory;


    
    /**
     * @Route("/request-tmp-password", name="request_tmp_password" )
     */
    public function requestTmpPasswordAction()
    {
        $email = $this->request->get('email', null);
        
        if (empty($email)) {
            return $this->jsonResponse(
                'FAIL',
                $this->translator->trans('E-mail address not specified', array(), 'passw_recovery')
            );
        }
        
        $user = $this->em->getRepository('VisiDarbiUserBundle:User')->findOneBy(array(
            'email' => $email,
            'country' => $this->countryManager->getCurrentCountry(),
            'is_admin' => false
        ));
        
        if ($user === null) {
            return $this->jsonResponse(
                'FAIL',
                $this->translator->trans('User account with such e-mail not found', array(), 'passw_recovery')
            );
        }
        
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        
        if (! $user->isEnabled()) {
            return $this->jsonResponse(
                'FAIL',
                $this->translator->trans('User account with such e-mail not confirmed', array(), 'passw_recovery')
            );
        }
        
        if ($user->isLocked()) {
            return $this->jsonResponse(
                'FAIL',
                $this->translator->trans('User account with such e-mail is locked', array(), 'passw_recovery')
            );
        }
        
        $tmpPassword = \substr(\str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );
        $siteLink = $this->generateUrl('index', array(), true);
        
        $this->emailSender->send(
            $this->countryManager->getCurrentCountry(),
            $this->request->getLocale(),
            EmailTemplate::ID_TMP_PASSWORD, 
            $user->getEmail(),
            array(
                'tmpPassword' => $tmpPassword,
                'siteLink' => sprintf('<a href="%s">%s</a>', $siteLink, $siteLink)
            ),
            array(
                'email' => $this->settingsManager->get('emails.sender_email'), 
                'name' => $this->settingsManager->get('emails.sender_name')
            )
        );
        
        
        $encoder = $this->encoderFactory->getEncoder($user);
                
        $user->setTmpPassword(
            $encoder->encodePassword($tmpPassword, $user->getSalt())  
        );
        
        $this->userManager->updateUser($user);
        
        return $this->jsonResponse(
            'OK',
            $this->translator->trans('Temporary password successfully sent', array(), 'passw_recovery')
        );
    }
    
    protected function jsonResponse($status, $message = '') 
    {
        return new JsonResponse(
            array('status' => $status, 'message' => $message )  
        );
    }
}