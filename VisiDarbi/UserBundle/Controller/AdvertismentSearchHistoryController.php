<?php

namespace VisiDarbi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use JMS\DiExtraBundle\Annotation as DI;

use VisiDarbi\UserBundle\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

use JMS\SecurityExtraBundle\Annotation\Secure;

use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

class AdvertismentSearchHistoryController extends Controller
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;
    
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator") 
     */
    protected $paginator;
    
    
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/advertisment-search-history", name="advertisment_search_history")
     * @Template()
     */
    public function listAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        
        if (! $user->isPrivate()) {
            throw new NotFoundHttpException('Access denied for non-private persons.');
        }
                
        $historyRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertismentSearchHistory');
        
        $qb = $historyRepo->createQueryBuilder('h')
            ->select('h')
            ->where('h.user = :user')
            ->setParameter('user', $user->getId()) 
            ->orderBy('h.last_searched_at', 'DESC')
        ;
        
        //$query = $qb->getQuery()
        //    ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
        //    ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale() )
        //    ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        //;

        $pagination = $this->paginator->paginate(
            $qb->getQuery(), 
            $this->get('request')->query->get('page', 1), 
            20
        );
                
        return array(
            'pagination' => $pagination
        );
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/advertisment-search-history/delete", name="delete_advertisment_search_history", defaults={"_format"="json"}, options={"i18n"="false"} )
     */
    public function deleteAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        

        $id = $this->request->get('historyId');
        if (empty($id)) {
            throw new NotFoundHttpException('Empty advertisement search history id.');
        }

        $historyRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertismentSearchHistory');
        $history = $historyRepo->findOneBy(array(
            'id' => $id,
            'user' => $user
        ));
        
        if ($history == null) {
            throw new NotFoundHttpException('Invalid advertisement search history id.');
        }
        
        $this->em->remove($history);
        $this->em->flush();
        
        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/advertisment-search-history/delete-all", name="delete_all_advertisment_search_history", defaults={"_format"="json"}, options={"i18n"="false"} )
     * @Method({"POST"})
     */
    public function deleteAllAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        
        $qb = $this->em->createQueryBuilder()
                ->delete('VisiDarbiAdvertisementBundle:AdvertismentSearchHistory', 'h')
                ->where('h.user = :user')
                ->setParameter('user', $user->getId())
        ;
        
        $qb->getQuery()->execute();
        
        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }
}
