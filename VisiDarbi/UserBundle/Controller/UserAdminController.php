<?php

namespace VisiDarbi\UserBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class for user administration actions in backend(admin)
 */
class UserAdminController extends Controller
{

    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    protected $em;

    /**
     * Generates random password
     * @Route("/generate-password", name="visidarbi_admin_generate_password", options={"expose"=true})
     */
    public function generatePasswordAction()
    {
        $userRepository = $this->em->getRepository('VisiDarbiUserBundle:User');

        $generatedPassword = $userRepository->generateStrongPassword(8);

        return new Response($generatedPassword, 200);
    }

}
