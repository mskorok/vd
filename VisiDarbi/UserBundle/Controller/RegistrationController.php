<?php

namespace VisiDarbi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

//use FOS\UserBundle\Controller\RegistrationController as Controller;

use JMS\DiExtraBundle\Annotation as DI;

use VisiDarbi\CMSBundle\Entity\Document;
use VisiDarbi\MobileBundle\Form\RegType;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\LegalProfile;
use VisiDarbi\UserBundle\Form\PrivatePersonType;
use VisiDarbi\UserBundle\Form\LegalPersonType;

use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\TranslationBundle\Annotation\Ignore;

class RegistrationController extends Controller
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;
    
    /** 
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;
    
    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator") 
     */
    protected $tokenGenerator;
    
    
    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender") 
     */
    protected $emailSender;
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     * @DI\Inject("translator") 
     */
    protected $translator;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;
    
    /**
     * @Route("/register", name="registration" )
     * @Template()
     */
    public function registerAction()
    {        
        
        if ($this->securityContext->isGranted('ROLE_USER')) {
            return $this->redirect(
                $this->generateUrl('index')
            );
        }
        
        $privatePerson = new User();
        $privatePerson->setCountry($this->countryManager->getCurrentCountry());
        
        $privatePerson->setType(User::TYPE_PRIVATE);
        $privatePerson->setPrivateProfile(new PrivateProfile());
        
        $legalPerson = new User();
        $legalPerson->setCountry($this->countryManager->getCurrentCountry());
        $legalPerson->setType(User::TYPE_LEGAL);
        $legalPerson->setLegalProfile(new LegalProfile());
        
//        $privatePersonForm = $this->createForm(
//            new PrivatePersonType(false, $this->translator),
//            $privatePerson
//        );


        $privatePersonForm = $this->createForm(
            new RegType($this->translator),
            $privatePerson
        );
        
        $legalPersonForm = $this->createForm(
            new LegalPersonType(false, $this->translator),
            $legalPerson
        );
        
        $hasErrors = false;
        $errors = array();
        
        
        if ($this->request->isMethod('POST') && in_array($this->request->get('type'), array(User::TYPE_PRIVATE, User::TYPE_LEGAL))) {
            
            $form = $this->request->get('type') == User::TYPE_PRIVATE ? $privatePersonForm : $legalPersonForm;
            $form->bind($this->request);

            if ($form->isValid()) {
                $user = $form->getData();
                $this->onFormSuccess($user);
                $this->session->getFlashBag()->add('is_reg_success', true);
                return $this->redirect($this->generateUrl('registration'));
            }
            else {
                $collector = $this->get('form_error_collector');
                $errors[$this->request->get('type')] = $collector->collectErrors($form);
                $hasErrors = true;
            }
        }
                
        return array(
            'private_form' => $privatePersonForm->createView(),
            'legal_form' => $legalPersonForm->createView(),
            'show_legal' => $this->request->get('type') === User::TYPE_LEGAL,
            'is_reg_success' => count($this->session->getFlashBag()->get('is_reg_success')) > 0,
            'has_errors' => $hasErrors,
            'errors' => $errors,
        );
    }
    
    /**
     * @Route("/registration/validate", name="registration_validate", defaults={"_format"="json"})
     */
    public function validateAction() 
    {
        $privatePerson = new User();
        $privatePerson->setCountry($this->countryManager->getCurrentCountry());
        
        $privatePerson->setType(User::TYPE_PRIVATE);
        $privatePerson->setPrivateProfile(new PrivateProfile());
        
        $legalPerson = new User();
        $legalPerson->setCountry($this->countryManager->getCurrentCountry());
        $legalPerson->setType(User::TYPE_LEGAL);
        $legalPerson->setLegalProfile(new LegalProfile());
        
        $privatePersonForm = $this->createForm(
            new PrivatePersonType(false, $this->translator),
            $privatePerson
        );
        
        $legalPersonForm = $this->createForm(
            new LegalPersonType(false, $this->translator),
            $legalPerson
        );
        
        $errors = array();
        $form = $this->request->get('type') == User::TYPE_PRIVATE ? $privatePersonForm : $legalPersonForm;
        $form->bind($this->request);

        if (! $form->isValid()) {
            $collector = $this->get('form_error_collector');
            $errors = $collector->collectErrors($form);
        }
        
        return new JsonResponse(
            array('errors' => $errors ) 
        );
    }
    
    protected function onFormSuccess(User $user) 
    {
        $user->setEnabled(false);
        $user->setUsername(null);
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        
        $this->sendConfirmationEmailMessage($user);
        
        $user->setDefaultLocale($this->request->getLocale());
        $this->userManager->updateUser($user);
    }
    
    protected function sendConfirmationEmailMessage(User $user)
    {
        $confirmLink = $this->generateUrl('confirm_registration', array('token' => $user->getConfirmationToken() ), true);

        $this->emailSender->send(
            $this->countryManager->getCurrentCountry(),
            $this->request->getLocale(),
            EmailTemplate::ID_REG_CONFIRMATION,
            $user->getEmail(),
            array(
                'confirmLink' => sprintf('<a href="%s">%s</a>', $confirmLink, $confirmLink)
            ),
            array(
                'email' => $this->settingsManager->get('emails.sender_email'), 
                'name' => $this->settingsManager->get('emails.sender_name')
            )
        );
    }
    
    /**
     * @Route("/confirm-registration/{token}", name="confirm_registration" )
     */
    public function confirmAction($token)
    {
        $user = $this->userManager->findUserByConfirmationToken($token);

        $em = $this->getDoctrine()->getEntityManager();

        if (null === $user) {
            //throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
            //return new RedirectResponse($this->generateUrl('index'));
            $document = $em->getRepository('VisiDarbiCMSBundle:Document')->findOneBy(['special'=>Document::SPECIAL_TOKEN_FAIL, 'locale'=> $this->request->getLocale()]);
            return new RedirectResponse(
                (!$document) ?  $this->generateUrl('index') :
                                $this->generateUrl('page', array( 'slug' => $document->getPage()->getSlug()) )
            );
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime('now'));

        $this->userManager->updateUser($user);

        $userNs = $em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->findOneBy(array('email' => $user->getEmail()));

        if($userNs){
            if($user->isPrivate()){
                $userNs->setUser($user);
                $userNs->setEmail(null);

                $em->persist($userNs);
                $em->flush();
            }

            if($user->isLegal()){
               $em->remove($userNs); 
               $em->flush();  
            }
        }
        
        $response = new RedirectResponse($this->generateUrl('index'));
        
        $this->authenticateUser($user, $response);

        return $response;
    }
    
    protected function authenticateUser($user, Response $response)
    {
        
        try {
            $this->get('fos_user.security.login_manager')->loginUser(
                'main',
                $user,
                $response
            );
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }
}
