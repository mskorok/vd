<?php

namespace VisiDarbi\UserBundle\Controller;

use VisiDarbi\CommonBundle\Controller\FrontendController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use JMS\DiExtraBundle\Annotation as DI;

use VisiDarbi\UserBundle\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

use JMS\SecurityExtraBundle\Annotation\Secure;

use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementType;
use VisiDarbi\AdvertisementBundle\Entity\PaidPeriod;

class MyAdvertismentsController extends FrontendController
{

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;
    
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \Knp\Component\Pager\Paginator
     * @DI\Inject("knp_paginator") 
     */
    protected $paginator;
    
    

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/my-advertisments", name="my_advertisments" )
     * @Template()
     */
    public function listAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        $advRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');

        $qb = $advRepo->getUserActiveAdvertisementQb($user);
        
        $pagination = $this->paginator->paginate(
            $qb->getQuery(), 
            $this->get('request')->query->get('page', 1), 
            7
        );   

        $advButtons = $this->prepareAdvAddButtons();
                           
        return array(
            'pagination' => $pagination,
            'showAddAdvertisementButton' => $advButtons['showAddAdvertisementButton'],
            'showAddAdvertisementButtonUrl' => $advButtons['showAddAdvertisementButtonUrl']
        );
    }

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/my-advertisments/update", name="toggle_my_advertisment", defaults={"_format"="json"} )
     */
    public function toggleAction()
    {

        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isLegal()) {
            throw new NotFoundHttpException('Access denied for non-legal persons.');
        }

        $id = $this->request->get('advId');
        if (empty($id)) {
            throw new NotFoundHttpException('Empty advertisement id.');
        }

        $status = $this->request->get('advStatus');
        if (empty($status)) {
            throw new NotFoundHttpException('Empty advertisement status.');
        }

        $myAdv = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement')->findOneBy(
            array(
                'publicId' => $id,
                'user' => $user->getId()
            )
        );

        if ($myAdv == null) {
            throw new NotFoundHttpException('Invalid advertisement id.');
        }

        if ($status == Advertisement::STATUS_INACTIVE) {
            $status = Advertisement::STATUS_ACTIVE;
        }
        else if ($status == Advertisement::STATUS_ACTIVE) {
            $status = Advertisement::STATUS_INACTIVE;
        }

        $myAdv->setStatus($status);
        $this->em->persist($myAdv);
        $this->em->flush();

        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/my-advertisments/highlight-advertisements", name="highlight_advertisements", defaults={"_format"="json"} )
     */
    public function highlightAdvertisementsAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isLegal()) {
            throw new NotFoundHttpException('Access denied for non-legal persons.');
        }
        
        $ids = $this->request->get('ids', []);

        $ids_mobile = $this->request->get('ids_mobile', []);

        if(count($ids) > 10  || count($ids_mobile) >10){
            throw new NotFoundHttpException('Invalid advertisement id.');  
        }

        $advertisements = array();
        foreach($ids as $id){
            $advRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');

            $adv = $advRepo->findOneBy(array(
                'publicId' => $id,
                'user' => $user->getId()
            ));
            $advertisements[] = $adv->getPublicId();
        }

        $advertisementsMobile = [];
        foreach($ids_mobile as $id){
            $advRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');

            $adv = $advRepo->findOneBy(array(
                'publicId' => $id,
                'user' => $user->getId()
            ));
            $advertisementsMobile[] = $adv->getPublicId();
        }

        if(count($advertisements) === 0 && count($advertisementsMobile) === 0){
            throw new NotFoundHttpException('Invalid advertisement id.');   
        }

        $this->session->set('stepfast1', array('ids' => $advertisements, 'ids_mobile' => $ids_mobile));

        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }    
    
    
    

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/my-advertisments/delete", name="delete_my_advertisment", defaults={"_format"="json"} )
     */
    public function deleteAction()
    {
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if (! $user->isLegal()) {
            throw new NotFoundHttpException('Access denied for non-legal persons.');
        }

        $id = $this->request->get('advId');
        if (empty($id)) {
            throw new NotFoundHttpException('Empty advertisement id.');
        }

        $myAdvRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');
        $myAdv = $myAdvRepo->findOneBy(array(
            'publicId' => $id,
            'user' => $user->getId()
        ));

        if ($myAdv == null) {
            throw new NotFoundHttpException('Invalid advertisement id.');
        }

        $myAdv->setStatus(Advertisement::STATUS_DELETED);
        $this->em->flush();

        return new JsonResponse(array(
            'status' => 'OK'
        ));
    }
    
    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/add-advertisement-by-period/{id}", name="add_advertisement_by_period", defaults={"id" = null} )
     * 
     */    
    public function redirectToAddAdvertisementStep2Action($id) {
        
        $user = $this->securityContext->getToken()->getUser();
        $userRepo = $this->em->getRepository('VisiDarbiUserBundle:User');  
        
        
        
        if ($id === null) {
            $userActivePaidPeriod = $userRepo->getCurrentActiveUserPeriod($user);        
        } else {
            $paidPeriod = $this->em->getRepository('VisiDarbiAdvertisementBundle:PaidPeriod')->find($id); 
            if ($paidPeriod === null) {
                throw new NotFoundHttpException(sprintf('Paid period "%d" not found.', $id));
            }
            $userActivePaidPeriod = ($paidPeriod->getUser()->getId() == $user->getId() && $paidPeriod->getStatus() == PaidPeriod::STATUS_ACTIVE)  ? $paidPeriod : null;
        }
        
        if(! $userActivePaidPeriod) {
            return $this->redirect($this->generateUrl('index'));        
        }
        
        
        
        $query = $this->em->createQueryBuilder('t')
                ->select('t.id, t.name, t.unlimited, p.price, p.description, p.payment_description, p.payment_description_first_page, p.payment_description_higlighting, p.payment_description_period ,  p.most_popular, t.days_period')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementType', 't')
                ->join('t.advertisingTypePrices', 'p')
                ->andWhere("p.country = :country")
                ->setParameter('country', $this->getCurrentCountry()->getId())
                ->orderBy('t.name');


        $query = $query->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);


        $types = $query->getResult();
        $items = array();

        foreach ($types as $type) {
            $item = array();
            $item['type'] = $type;
            $items[$type['id']] = $item;
        }                
        
        $type = $userActivePaidPeriod->getAdvertisementType();
        
        if( $type instanceof AdvertisementType) {
            $addType = $type->getId();
        } else {
            return $this->redirect($this->generateUrl('index'));        
        }
       
        
        $data = array('advertisement_type' => $addType);
        
        $this->session->set('step1', array('valid' => 'true', 'form' => $data, 'type' => $items[$addType]['type']));
        return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step2'));
    }


    private function prepareAdvAddButtons(){

        $showAddAdvertisementButton = true;
        $showAddAdvertisementButtonUrl = $this->generateUrl('visidarbi_advertisement_add_step1');

        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */

        if( $user instanceof \VisiDarbi\UserBundle\Entity\User){

            if($user->isLegal()){

                $userRepo = $this->em->getRepository('VisiDarbiUserBundle:User');
                $userActivePaidPeriod = $userRepo->getCurrentActiveUserPeriod($user);  

                if($userActivePaidPeriod){
                    $showAddAdvertisementButtonUrl = $this->generateUrl('add_advertisement_by_period', array('id' => $userActivePaidPeriod->getId() ));
                }
            }
            elseif($user->isPrivate()){
                $showAddAdvertisementButton = false;  
            }
        }


        return array(
            'showAddAdvertisementButton' => $showAddAdvertisementButton,
            'showAddAdvertisementButtonUrl' => $showAddAdvertisementButtonUrl    
        );

    }
}
