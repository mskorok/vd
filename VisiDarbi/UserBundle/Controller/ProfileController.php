<?php

namespace VisiDarbi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

//use FOS\UserBundle\Controller\RegistrationController as Controller;

use JMS\DiExtraBundle\Annotation as DI;

use VisiDarbi\CMSBundle\Entity\Document;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\UserBundle\Entity\PrivateProfile;
use VisiDarbi\UserBundle\Entity\LegalProfile;
use VisiDarbi\UserBundle\Form\PrivatePersonType;
use VisiDarbi\UserBundle\Form\LegalPersonType;

use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

use JMS\SecurityExtraBundle\Annotation\Secure;
use VisiDarbi\AdvertisementBundle\Entity\PaidPeriod;
use VisiDarbi\UserBundle\Form\ProfileDesktopType;

class ProfileController extends Controller
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /** 
     * @var \Symfony\Component\HttpFoundation\Session\Session
     * @DI\Inject("session")
     */
    protected $session;
    
    /** 
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;
    
    
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;
    
    
    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;
    
    /**
     * @var \Symfony\Component\Security\Core\SecurityContext 
     * @DI\Inject("security.context")
     */
    protected $securityContext;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager") 
     */
    protected $em;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     * @DI\Inject("translator") 
     */
    protected $translator;
    
    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator") 
     */
    protected $tokenGenerator;
    
    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender") 
     */
    protected $emailSender;

    /**
     * @Secure(roles="ROLE_USER")
     * @Route("/edit-profile", name="edit_profile" )
     * @Template()
     */
    public function editAction()
    {       
        $user = $this->securityContext->getToken()->getUser();
        /* @var $user \VisiDarbi\UserBundle\Entity\User */
        
        $oldEmail = $user->getEmail();
        
        $form = null;
        if ($user->getType() === User::TYPE_PRIVATE) {
//            $form = $this->createForm(
//                new PrivatePersonType(true, $this->translator),
//                $user
//            );

            $form = $this->createForm(
                new ProfileDesktopType($this->translator),
                $user
            );

        }
        
        if ($user->getType() === User::TYPE_LEGAL) {
            $form = $this->createForm(
                new LegalPersonType(true, $this->translator),
                $user
            );
        }
        
        if ($this->request->isMethod('POST') ) {
            
            $form->bind($this->request);

            if ($form->isValid()) {
                $user = $form->getData();
                
                $this->session->getFlashBag()->add('is_success', true);
                
                $newEmail = $user->getEmail();
                if ($oldEmail != $newEmail) {
                    $this->session->getFlashBag()->add('email_changed', true);
                    $user->setEmail($oldEmail);
                    $user->setTmpEmail($newEmail);
                    $this->onEmailChanged($user);
                } else {
                    $user->setUsername($user->getEmail());
                }

                $this->userManager->updateUser($user);
                
                return $this->redirect($this->generateUrl('edit_profile'));
            }
        }

        return array(
            'form' => $form->createView(),
            'is_success' => count($this->session->getFlashBag()->get('is_success')) > 0,
            'email_changed' => count($this->session->getFlashBag()->get('email_changed')) > 0,
            'is_email_change_success' => count($this->session->getFlashBag()->get('is_email_change_success')) > 0,
            
            'active_adv_count' => $this->getActiveAdvCount($user),
            'active_paid_periods' => $this->getActivePaidPeriods($user),
            
            'is_post_request' => $this->request->isMethod('POST'),
        );
    }
    
    protected function getActiveAdvCount(User $user) 
    {
        if (! $user->isLegal()) {
            return 0;
        }
        
        $advRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');
        
        return $advRepo->getUserActiveAdvertisementQb($user)->select('COUNT(h.id)')->getQuery()->getSingleScalarResult();
    }
    
    
    protected function getActivePaidPeriods(User $user) 
    {
        if (! $user->isLegal()) {
            return array();
        }

        
        $qb = $this->em->createQueryBuilder('pp')
            ->select('pp', 't', 'p')
            ->from('VisiDarbi\AdvertisementBundle\Entity\PaidPeriod', 'pp')
            ->join('pp.advertisementType', 't')
            ->join('t.advertisingTypePrices', 'p')
            ->where('pp.user = :user')
            ->andWhere('pp.from_date <= :nowDate')
            ->andWhere('pp.to_date >= :nowDate')
            ->andWhere('pp.status IN (:statuses)')
            ->setParameter('statuses', array(PaidPeriod::STATUS_ACTIVE, PaidPeriod::STATUS_CLOSED))
            ->setParameter('user', $user->getId())
            ->setParameter('nowDate', new \DateTime())
            ->orderBy('pp.from_date', 'DESC')
        ;
        $query = $qb->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->request->getLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        
   
        return $query->getResult();
    }


    protected function onEmailChanged(User $user)
    {
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $confirmLink = $this->generateUrl('confirm_email_change', array('token' => $user->getConfirmationToken()), true);

        $this->emailSender->send(
            $this->countryManager->getCurrentCountry(),
            $this->request->getLocale(),
            EmailTemplate::ID_EMAIL_CHANGE_CONFIRMATION,
            $user->getTmpEmail(),
            array(
                'confirmLink' => sprintf('<a href="%s">%s</a>', $confirmLink, $confirmLink)
            ),
            array(
                'email' => $this->settingsManager->get('emails.sender_email'), 
                'name' => $this->settingsManager->get('emails.sender_name')
            )
        );
    }
    
    /**
     * @Route("/confirm-email/{token}", name="confirm_email_change" )
     */
    public function confirmEmailAction($token)
    {
        $user = $this->userManager->findUserByConfirmationToken($token);

        $em = $this->getDoctrine()->getEntityManager();

        if (null === $user) {
            //throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
            //return new RedirectResponse($this->generateUrl('index'));
            $document = $em->getRepository('VisiDarbiCMSBundle:Document')->findOneBy(['special'=>Document::SPECIAL_TOKEN_FAIL, 'locale'=> $this->request->getLocale()]);
            return new RedirectResponse(
                (!$document) ?  $this->generateUrl('index') :
                    $this->generateUrl('page', array( 'slug' => $document->getPage()->getSlug()) )
            );
        }

        $user->setConfirmationToken(null);
        $user->setEmail($user->getTmpEmail());
        $user->getUsername($user->getEmail());
        $user->setTmpEmail(null);
        
        $this->userManager->updateUser($user);
        
        $this->session->getFlashBag()->add('is_email_change_success', true);
        $response = new RedirectResponse($this->generateUrl('edit_profile'));
        
        if ($this->securityContext->isGranted('ROLE_USER')) {
            $token = $this->securityContext->getToken();
            if ($token->getUser()->getId() != $user->getId()) {
                $this->authenticateUser($user, $response);
            }
        }
        else {
            $this->authenticateUser($user, $response);
        }
        
        return $response;
    }
    
    
    protected function authenticateUser($user, Response $response)
    {
        
        try {
            $this->get('fos_user.security.login_manager')->loginUser(
                'main',
                $user,
                $response
            );
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }
}
