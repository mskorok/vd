<?php

namespace VisiDarbi\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;
use VisiDarbi\UserBundle\Entity\User;

class ProfileDesktopType extends AbstractType
{
    private $translator;

    public function __construct( TranslatorInterface $translator = null)
    {

        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('full_name', 'text', array(
                /** @Ignore */
                'label' => $this->translator->trans('Name', array(), 'desktop_changed_form'),
                'max_length' => 255,
                'required' => true,
            ))
            ->add('surname', 'text', array(
                /** @Ignore */
                'label' => $this->translator->trans('Surname', array(), 'desktop_changed_form'),
                'max_length' => 255,
                'required' => true,
            ))
            ->add('email', 'email', [
                /** @Ignore */
                'label' => $this->translator->trans('E-mail', [], 'desktop_changed_form'),
                'max_length' => 255,
                'required' => true,
            ])
            ->add('phone', 'text', User::phoneValidationParams(array('label'=>/** @Ignore */$this->translator->trans('Phone', array(), 'user_form'))))

            ->add('password', 'repeated', [
                'property_path' => 'plainPassword',
                'required' => true,
                'type' => 'password',
                'invalid_message' => 'Password does not match confirmation',
                'first_options'  => [
                    /** @Ignore */
                    'label' => $this->translator->trans('Password', [], 'desktop_changed_form'),
                    'max_length' => 20,

                ],
                'second_options' => [
                    /** @Ignore */
                    'label' => $this->translator->trans('Confirm password', [], 'desktop_changed_form'),
                    'max_length' => 20,

                ],
            ])
            ->add('address', 'text', array(
                /** @Ignore */
                'label' => $this->translator->trans('Address', array(), 'mobile_form'),
                'property_path' => 'privateProfile.address',
                'max_length' => 255,
                'required' => true,
            ))

        ;
    }



    public function getDefaultOptions(array $options) {
        return [
            'data_class' => 'VisiDarbi\UserBundle\Entity\User',
            'validation_groups' => ['Profile'],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'profile';
    }
}
