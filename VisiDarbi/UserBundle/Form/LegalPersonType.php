<?php

namespace VisiDarbi\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VisiDarbi\UserBundle\Entity\User;
use Symfony\Component\Form\CallbackValidator;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\Translation\TranslatorInterface;
use JMS\TranslationBundle\Annotation\Ignore;

class LegalPersonType extends AbstractType 
{
    private $isEdit;
    
    private $translator;
    
    public function __construct($isEdit = false, TranslatorInterface $translator = null) 
    {
        $this->isEdit = $isEdit;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('full_name', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Full name', array(), 'user_form'),
                'max_length' => 255,
            ))
            ->add('company_name', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Company name', array(), 'user_form'),
                'property_path' => 'legalProfile.company_name',
                'max_length' => 255,
            ))
            ->add('registration_number', 'text',User::registrationNumValidationParams(array('label'=>/** @Ignore */$this->translator->trans('Registration number', array(), 'user_form'), 'property_path'=>'legalProfile.registration_number')) )
            ->add('vat_number', 'text', User::vatValidationParams(array(
                'label' => /** @Ignore */$this->translator->trans('VAT number', array(), 'user_form'),
                'property_path' => 'legalProfile.vat_number',
                'required' => false
            )))
            ->add('legal_address', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Legal address', array(), 'user_form'),
                'property_path' => 'legalProfile.legal_address',
                'max_length' => 255,
            ))
            ->add('legal_address_postcode', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Postcode', array(), 'user_form'),
                'property_path' => 'legalProfile.legal_address_postcode',
                'max_length' => 7,
            ))
            ->add('factual_address', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Factual address', array(), 'user_form'),
                'property_path' => 'legalProfile.factual_address',
                'max_length' => 255,
            ))
            ->add('factual_address_postcode', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('Postcode', array(), 'user_form'),
                'property_path' => 'legalProfile.factual_address_postcode',
                'max_length' => 7,
            ))
            ->add('email', 'text', array(
                'label' => /** @Ignore */$this->translator->trans('E-mail', array(), 'user_form'),
                'max_length' => 255,
            ))
            ->add('phone', 'text', User::phoneValidationParams(array('label'=>/** @Ignore */$this->translator->trans('Phone', array(), 'user_form'))))
            ->add('password', 'repeated', array(
                'property_path' => 'plainPassword',
                'required' => ! $this->isEdit,
                'type' => 'password',
                'invalid_message' => 'Password does not match confirmation',
                'first_options'  => array(
                    'label' => /** @Ignore */$this->translator->trans('Password', array(), 'user_form'),
                    'max_length' => 20,
                ),
                'second_options' => array(
                    'label' => /** @Ignore */$this->translator->trans('Confirm password', array(), 'user_form'),
                    'max_length' => 20,
                ),
            ))  
        ;
    }

    public function getName() 
    {
        return 'private_person';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) 
    {
        $resolver->setDefaults(array(
            'data_class' => 'VisiDarbi\UserBundle\Entity\User',
            'validation_groups' => array( 'Default', 'Legal', $this->isEdit ? 'Edit' : 'Register' ),
        ));
    }
}
