<?php

namespace VisiDarbi\UserBundle\Security\UserProvider;

use FOS\UserBundle\Security\EmailUserProvider as UserProvider;

class AdminUserProvider extends UserProvider 
{
    protected function findUser($username) 
    {
        
        return $this->userManager->findUserBy(array(
            'username' => $username,
            'is_admin' => true
        ));
    }
    
}