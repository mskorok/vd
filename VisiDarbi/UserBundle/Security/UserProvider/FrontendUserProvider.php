<?php

namespace VisiDarbi\UserBundle\Security\UserProvider;

use FOS\UserBundle\Security\EmailUserProvider as UserProvider;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManager;

class FrontendUserProvider extends UserProvider 
{
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManager 
     */
    protected $countryManager;
    
    public function setCountryManager(CountryManager $countryManager) 
    {
        $this->countryManager = $countryManager;
    }
    
    protected function findUser($username)
    {
        return $this->userManager->findUserBy(array(
            'emailCanonical' => mb_convert_case($username, MB_CASE_LOWER, mb_detect_encoding($username)), //TODO: use canonicalizer service,
            'country' => $this->countryManager->getCurrentCountry(),
            'is_admin' => false
        ));
    }
}