<?php


namespace VisiDarbi\UserBundle\Security\Authentication\Provider;

use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Provider\DaoAuthenticationProvider;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

use FOS\UserBundle\Model\UserManagerInterface;


class TmpPasswordAuthProvider extends DaoAuthenticationProvider
{
    private $encoderFactory;
    
    private $userProvider;
    
    private $userManager;

    /**
     * Constructor.
     *
     * @param UserProviderInterface   $userProvider               An UserProviderInterface instance
     * @param UserCheckerInterface    $userChecker                An UserCheckerInterface instance
     * @param string                  $providerKey                The provider key
     * @param EncoderFactoryInterface $encoderFactory             An EncoderFactoryInterface instance
     * @param Boolean                 $hideUserNotFoundExceptions Whether to hide user not found exception or not
     */
    public function __construct(UserProviderInterface $userProvider, UserCheckerInterface $userChecker, $providerKey, EncoderFactoryInterface $encoderFactory, $hideUserNotFoundExceptions, UserManagerInterface $userManager)
    {
        parent::__construct($userProvider, $userChecker, $providerKey, $encoderFactory, $hideUserNotFoundExceptions);

        $this->encoderFactory = $encoderFactory;
        $this->userProvider = $userProvider;
        $this->userManager = $userManager;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function checkAuthentication(UserInterface $user, UsernamePasswordToken $token)
    {
        $currentUser = $token->getUser();

        if ($currentUser instanceof UserInterface) {
            if ($currentUser->getPassword() !== $user->getPassword()) {
                throw new BadCredentialsException('The credentials were changed from another session.');
            }
        } 
        else {
            if ('' === ($presentedPassword = $token->getCredentials())) {
                throw new BadCredentialsException('The presented password cannot be empty.');
            }
            
            $encoder = $this->encoderFactory->getEncoder($user);
            
            $realPasswordValid = $user->getPassword() && $encoder->isPasswordValid($user->getPassword(), $presentedPassword, $user->getSalt());
            $tmpPasswordValid = $user->getTmpPassword() && $encoder->isPasswordValid($user->getTmpPassword(), $presentedPassword, $user->getSalt());
            

            if (! ($realPasswordValid || $tmpPasswordValid)) {
                throw new BadCredentialsException('The presented password is invalid.');
            }

            if ($user->getTmpPassword() !== null) {
                if ($tmpPasswordValid) {
                    $user->setPassword($user->getTmpPassword());
                }
                $user->setTmpPassword(null);
                $this->userManager->updatePassword($user);
            }
            
            
        }
    }
}
