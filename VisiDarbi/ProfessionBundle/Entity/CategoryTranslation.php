<?php

namespace VisiDarbi\ProfessionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @ORM\Table(name="category_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class CategoryTranslation extends AbstractPersonalTranslation
{
    
    /**
     * @Gedmo\Slug(fields={"content"})
     * @ORM\Column(type="string")
     */
    private $slug;         
    
    
    /**
     * Convinient constructor
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($locale = '', $field = '', $value = '')
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\ProfessionBundle\Entity\Category", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
    
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return CategoryTranslation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }    
    
    
    }

?>
