<?php
namespace VisiDarbi\ProfessionBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation")
 */
class Profession extends \VisiDarbi\CommonBundle\Entity\Translated
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;
    
    private $adminName;

    /** 
     * @ORM\ManyToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", mappedBy="Professions")
     */
    private $Advertisements;

    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\ProfessionBundle\Entity\Category", inversedBy="professions")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;
    
    
    /** 
     * @ORM\ManyToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession", 
     *     mappedBy="professions"
     * )
     */
    private $externalProfessions;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->Advertisements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->externalProfessions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Profession
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }




    /**
     * Get advertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisements()
    {
        return $this->Advertisements;
    }

    /**
     * Set category
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $category
     * @return Profession
     */
    public function setCategory(\VisiDarbi\ProfessionBundle\Entity\Category $category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \VisiDarbi\ProfessionBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    public function addTranslation(ProfessionTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }
    
    

    /**
     * Remove translations
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }
    
    
    public function __toString() {
        return (string) $this->getName();
    }

    /**
     * Add Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return Profession
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->Advertisements[] = $advertisements;
    
        return $this;
    }

    /**
     * Remove Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->Advertisements->removeElement($advertisements);
    }

    /**
     * Add externalProfessions
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions
     * @return Profession
     */
    public function addExternalProfession(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions)
    {
        $this->externalProfessions[] = $externalProfessions;
    
        return $this;
    }

    /**
     * Remove externalProfessions
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions
     */
    public function removeExternalProfession(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions)
    {
        $this->externalProfessions->removeElement($externalProfessions);
    }

    /**
     * Get externalProfessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalProfessions()
    {
        return $this->externalProfessions;
    }
    
    public function getAdminName() {
        
        return $this->getCategory()->getName() . ' / ' . $this->getName();
        //return $this->adminName;
    }

    public function setAdminName($adminName) {
        $this->adminName = $adminName;
        
        return $this;
    }


}