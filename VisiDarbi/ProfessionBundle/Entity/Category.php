<?php
namespace VisiDarbi\ProfessionBundle\Entity;
use VisiDarbi\ProfessionBundle\Entity\Profession;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\ProfessionBundle\Entity\CategoryTranslation")
 */
class Category extends \VisiDarbi\CommonBundle\Entity\Translated
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", inversedBy="categories")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Profession", mappedBy="category", cascade={"persist"})
     */
    private $professions;

    /** 
     * @ORM\ManyToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", mappedBy="Categories")
     */
    private $Advertisements;

    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\ProfessionBundle\Entity\CategoryTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->professions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->advertisements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return Category
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     * @return Category
     */
    public function addProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->professions[] = $professions;
    
        return $this;
    }

    /**
     * Remove professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     */
    public function removeProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->professions->removeElement($professions);
    }

    /**
     * Get professions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfessions()
    {
        return $this->professions;
    }

    /**
     * Add advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return Category
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->advertisements[] = $advertisements;
    
        return $this;
    }



    
    public function getTranslations()
    {
        return $this->translations;
    }

    public function addTranslation(CategoryTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    
    public function __toString() {
        return (string) $this->getName();
    }
    

    /**
     * Remove translations
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\CategoryTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\ProfessionBundle\Entity\CategoryTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
    
    /**
     * Returns count of advertisements in category
     * @todo Need refactoring
     * @return int
     */
    public function getAdvertisementsCount(){
        $count = 0;
//        $professions = $this->getProfessions();
//        foreach($professions as $profession) {
//            $count = $count +$profession->getAdvertisements()->count();
//        }
//        
//        $ad = $this->getAdvertisements();
//        foreach($professions as $profession) {
//            $count = $count +$profession->getAdvertisements()->count();
//        }
        
        return $count;
    }
    

    /**
     * Remove Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->Advertisements->removeElement($advertisements);
    }

    /**
     * Get Advertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisements()
    {
        return $this->Advertisements;
    }
}