<?php

namespace VisiDarbi\ProfessionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @ORM\Table(name="profession_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class ProfessionTranslation extends AbstractPersonalTranslation {
    
    /**
     * @Gedmo\Slug(fields={"content"}, unique=false)
     * @ORM\Column(type="string", nullable=true)
     */
    private $slug;
    
     /**
     * @Gedmo\Slug(fields={"content"}, unique=false)
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $az;      
    
   /**
     * Convinient constructor
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($locale = '', $field = '', $value = '')
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\ProfessionBundle\Entity\Profession", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Company
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }    
    
     /**
     * Set az
     *
     * @param string $az
     * @return Company
     */
    public function setAz($az)
    {
        $this->az = $az;
    
        return $this;
    }

    /**
     * Get az
     *
     * @return string 
     */
    public function getAz()
    {
        return $this->az;
    }   
    
}

?>
