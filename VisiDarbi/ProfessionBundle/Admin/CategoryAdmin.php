<?php

namespace VisiDarbi\ProfessionBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of CategoryAdmin
 *
 * @author Aleksey
 */
class CategoryAdmin extends CommonAdmin {

    protected $baseRouteName = 'category';
    protected $baseRoutePattern = 'category';

    /**
     *
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     * @return void
     */
    protected function configureRoutes(RouteCollection $collection) {
        $routes = parent::getRoutes();
        $routes->remove('view');
        return $routes;
    }

    protected function configureFormFields(FormMapper $formMapper) {


        $formMapper
                
//                ->add('Country', 'entity', array('label' => 'Country', 'class' => 'VisiDarbi\LocalePlaceBundle\Entity\Country', 'property' => 'name', 'required' => true, 'query_builder' =>
//                    function(EntityRepository $er) {
//                        return $er->createQueryBuilder('fms')->orderBy('fms.name', 'ASC');
//                    }))
                ->add('name', 'a2lix_translations',
                $this->getTranslatableFieldParam(
                        'VisiDarbi\ProfessionBundle\Entity\Category', 
                        'name', 'Name',true))
                ->add('professions', 'sonata_type_collection', array('type_options'=>array( 'delete'=>false),'required' => true, 'by_reference' => false, 'label' => 'Professions'), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position'
                ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
//                ->add('country', null, array('label' => 'Country'))
                ->add('name', null, array('label' => 'Category'))
                ->add('_professionsCount', 'null', array(
                    'label' => 'Profession count',
                    'sortable' => false,
                    'template' => 'VisiDarbiProfessionBundle:CategoryAdmin:profession_count.html.twig'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))

        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('country', null, array('label' => 'Country'))
                ->add('name', null, array('label' => 'Category'));
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

}