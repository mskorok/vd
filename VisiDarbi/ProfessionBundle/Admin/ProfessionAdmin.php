<?php

namespace VisiDarbi\ProfessionBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of ProfessionAdmin
 *
 * @author Aleksey
 */
class ProfessionAdmin extends CommonAdmin {

    protected $baseRouteName = 'profession';
    protected $baseRoutePattern = 'profession';

    protected $categoryId = null;
    
    /**
     *
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     * @return void
     */
    protected function configureRoutes(RouteCollection $collection) {        
        $collection->remove('view');
        return $collection;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $attr = array();
        
        if ($this->getRoot()->getClassnameLabel() == 'Category') {           
            $this->categoryId = $this->getRoot()->getSubject()->getId();           
            
            $attr = array('readonly'=>'readonly', 'style' => 'width: 300px;');
        }
        
        
        $formMapper
                ->add('Category', 'entity', array( 'attr'=>$attr, 'label' => 'Category', 'class' => 'VisiDarbi\ProfessionBundle\Entity\Category', 'property' => 'name', 'required' => true, 'query_builder' =>
                    function(EntityRepository $er) {
                    
                        $q = $er->createQueryBuilder('fms')
                                ->orderBy('t.content', 'ASC');
            
                        $q->join('fms.translations', 't');
                        $q->andWhere('t.field = :field')
                                ->andWhere('t.locale =:locale')
                                ->setParameter('field', 'name')
                                ->setParameter('locale', $this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale());
                        if($this->categoryId){
                            $q->andWhere('fms.id = :catId');
                            $q->setParameter('catId', $this->categoryId);
                        }
                        
                        return $q;
                    }))
                ->add('name', 'a2lix_translations', $this->getTranslatableFieldParam(
                                'VisiDarbi\ProfessionBundle\Entity\Profession', 'name', 'Profession name', true))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->add('name', null, array('label' => 'Profession'))
                ->add('category', null, array('label' => 'Category'))
                ->add('_advertisementCount', 'null', array(
                    'label' => 'Advertisements count',
                    'sortable' => false,
                    'template' => 'VisiDarbiProfessionBundle:ProfessionAdmin:advertisement_count.html.twig'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))

        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('category', null, array('label' => 'Category'))
                ->add('name', null, array('label' => 'Profession'));
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

}