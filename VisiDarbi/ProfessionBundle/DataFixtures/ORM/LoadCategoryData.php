<?php

namespace VisiDarbi\ProfessionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\ProfessionBundle\Entity\Category as Category;
use VisiDarbi\ProfessionBundle\Entity\CategoryTranslation;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCategoryData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        
        return;
        
        $categoryCount = 100;

        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $this->getReference('country-lat');
        $locales = $country->getLocales();

        //$country = new Country(); 

        for ($i = 0; $i < $categoryCount; $i++) {

            $category = new Category();

            foreach ($locales as $k => $v) {
                $category->setName('Category - #' . $i);
                $category->addTranslation(new CategoryTranslation($v, 'name', 'Category - ' . $v . ' #' . $i));
                $category->setCountry($country);
            }

            $manager->persist($category);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 30;
    }

}
