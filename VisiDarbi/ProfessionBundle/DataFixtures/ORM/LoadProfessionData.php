<?php

namespace VisiDarbi\ProfessionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\ProfessionBundle\Entity\Profession as Profession;
use VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadProfessionData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        
        return;
        
        $abc = array('a','b','d','f','g','x','q','p','w');
        
        $professionCount = 200;

        /** @var VisiDarbi\ProfessionBundle\Entity\Category $category */
        $categories = $manager->getRepository('VisiDarbi\ProfessionBundle\Entity\Category')->findAll();

        $categoriesCount = count($categories);
        
        //$country = new Country(); 

        for ($i = 0; $i < $professionCount; $i++) {

            
            $profession = new Profession();
            $category = $categories[rand(0, $categoriesCount-1)];
            $locales = $category->getCountry()->getLocales();
            
            foreach ($locales as $k => $v) {
                $profession->setName('Profession - #' . $i);
                $profession->addTranslation(new ProfessionTranslation($v, 'name', $abc[rand(0, count($abc)-1)].' - Profession - ' . $v . ' #' . $i));
                $profession->setCategory($category);
            }

            $manager->persist($profession);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 40;
    }

}
