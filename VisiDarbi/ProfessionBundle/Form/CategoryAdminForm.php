<?php

namespace VisiDarbi\ProfessionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\ProfessionBundle\Entity\Category;

class CategoryAdminForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        
        
        $builder->add('country', 'entity', array('label' => 'Country', 'class' => 'VisiDarbi\LocalePlaceBundle\Entity\Country', 'property' => 'name', 'required' => true, 'query_builder' =>
            function(EntityRepository $er) {
                return $er->createQueryBuilder('fms')->orderBy('fms.name', 'ASC');
            }));
            
        $builder->add('name', 'text');



    }

    public function getName() {
        return 'categoryadminform';
    }

}
