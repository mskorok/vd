<?php
namespace VisiDarbi\ExternalAdvertisementBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 */
class ExternalProfession
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement", 
     *     mappedBy="externalProfession"
     * )
     */
    private $externalAdvertisements;

    /** 
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Profession", inversedBy="externalProfessions")
     * @ORM\JoinTable(
     *     name="ProfessionToExternalProfession", 
     *     joinColumns={@ORM\JoinColumn(name="external_profession_id", referencedColumnName="id", nullable=false)}, 
     *     inverseJoinColumns={@ORM\JoinColumn(name="profession_id", referencedColumnName="id", nullable=false)}
     * )
     */
    private $professions;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource", 
     *     inversedBy="externalProfessions"
     * )
     * @ORM\JoinColumn(name="external_advertisement_source_id", referencedColumnName="id", nullable=false)
     */
    private $externalAdvertisementSource;   
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->externalAdvertisements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ExternalProfession
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     * @return ExternalProfession
     */
    public function addExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements[] = $externalAdvertisements;
    
        return $this;
    }

    /**
     * Remove externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     */
    public function removeExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements->removeElement($externalAdvertisements);
    }

    /**
     * Get externalAdvertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalAdvertisements()
    {
        return $this->externalAdvertisements;
    }
    
    public function __toString() {
        
//        if(!$this->getProfession()) {
//            return (string)$this->getName();
//        }
        
        return $this->getName() ;//. ' - ' . $this->getProfession()->getName();
        
    }
    

    /**
     * Set externalAdvertisementSource
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSource
     * @return ExternalProfession
     */
    public function setExternalAdvertisementSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSource)
    {
        $this->externalAdvertisementSource = $externalAdvertisementSource;
    
        return $this;
    }

    /**
     * Get externalAdvertisementSource
     *
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource 
     */
    public function getExternalAdvertisementSource()
    {
        return $this->externalAdvertisementSource;
    }

    /**
     * Add professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     * @return ExternalProfession
     */
    public function addProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->professions[] = $professions;
    
        return $this;
    }


    public function setProfessions($professions)
    {
        $this->professions = $professions;
    
        return $this;
    }    
    
    
    
    /**
     * Remove professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     */
    public function removeProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->professions->removeElement($professions);
    }

    /**
     * Get professions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfessions()
    {
        return $this->professions;
    }
}