<?php
namespace VisiDarbi\ExternalAdvertisementBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(name="country_resource_idx", columns={"country_id","resource"})}
 * )
 */
class ExternalAdvertisementSource
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string",  nullable=false)
     */
    private $resource;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $show_as;
    
    
    /** 
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $is_social;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : true})
     */
    private $is_supported_mobile;

    /** 
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $show_advertisiment;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession", 
     *     mappedBy="externalAdvertisementSource"
     * )
     */
    private $externalProfessions;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement", 
     *     mappedBy="externalAdvertisementSource"
     * )
     */
    private $externalAdvertisements;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace", 
     *     mappedBy="externalAdvertisementSource"
     * )
     */
    private $externalPlaces;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", 
     *     inversedBy="externalAdvertisementSources"
     * )
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->externalAdvertisements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resource
     *
     * @param string $resource
     * @return ExternalAdvertisementSource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    
        return $this;
    }

    /**
     * Get resource
     *
     * @return string 
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Set is_social
     *
     * @param boolean $isSocial
     * @return ExternalAdvertisementSource
     */
    public function setIsSocial($isSocial)
    {
        $this->is_social = $isSocial;
    
        return $this;
    }

    /**
     * Get is_social
     *
     * @return boolean 
     */
    public function getIsSocial()
    {
        return $this->is_social;
    }

    /**
     * Set show_advertisiment
     *
     * @param boolean $showAdvertisiment
     * @return ExternalAdvertisementSource
     */
    public function setShowAdvertisiment($showAdvertisiment)
    {
        $this->show_advertisiment = $showAdvertisiment;
    
        return $this;
    }

    /**
     * Get show_advertisiment
     *
     * @return boolean 
     */
    public function getShowAdvertisiment()
    {
        return $this->show_advertisiment;
    }

    /**
     * Add externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     * @return ExternalAdvertisementSource
     */
    public function addExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements[] = $externalAdvertisements;
    
        $this->externalAdvertisementsCount = count($this->externalAdvertisements);
        
        return $this;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getExternalAdvertisementsCount(){
        
        return 0;
        
       $this->externalAdvertisementsCount = $this->getExternalAdvertisements()->count();
        
       return $this->externalAdvertisementsCount;
       
    }
    
    /**
     * Remove externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     */
    public function removeExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements->removeElement($externalAdvertisements);
    }

    /**
     * Get externalAdvertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalAdvertisements()
    {
        return $this->externalAdvertisements;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return ExternalAdvertisementSource
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
        
    public function __toString() {
        return (string) $this->getResource();
    }

    /**
     * Set externalAdvertisementsCount
     *
     * @param integer $externalAdvertisementsCount
     * @return ExternalAdvertisementSource
     */
    public function setExternalAdvertisementsCount($externalAdvertisementsCount)
    {
        $this->externalAdvertisementsCount = $externalAdvertisementsCount;
    
        return $this;
    }

    /**
     * Add externalProfessions
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions
     * @return ExternalAdvertisementSource
     */
    public function addExternalProfession(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions)
    {
        $this->externalProfessions[] = $externalProfessions;
    
        return $this;
    }

    /**
     * Remove externalProfessions
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions
     */
    public function removeExternalProfession(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfessions)
    {
        $this->externalProfessions->removeElement($externalProfessions);
    }

    /**
     * Get externalProfessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalProfessions()
    {
        return $this->externalProfessions;
    }

    /**
     * Add externalPlaces
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces
     * @return ExternalAdvertisementSource
     */
    public function addExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces)
    {
        $this->externalPlaces[] = $externalPlaces;
    
        return $this;
    }

    /**
     * Remove externalPlaces
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces
     */
    public function removeExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlaces)
    {
        $this->externalPlaces->removeElement($externalPlaces);
    }

    /**
     * Get externalPlaces
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalPlaces()
    {
        return $this->externalPlaces;
    }
    
    /**
     * Get show as
     * @return string
     */
    public function getShowAs() {
        
        if($this->show_as) {
            return $this->show_as;
        }
        
        return $this->getResource();
        
    }

    /**
     *  Set show as
     * @param string $show_as
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource
     */
    public function setShowAs($show_as) {
        $this->show_as = $show_as;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsSupportedMobile()
    {
        return $this->is_supported_mobile;
    }

    /**
     * @param mixed $is_supported_mobile
     */
    public function setIsSupportedMobile($is_supported_mobile)
    {
        $this->is_supported_mobile = $is_supported_mobile;
    }


    
}