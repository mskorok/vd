<?php
namespace VisiDarbi\ExternalAdvertisementBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\ExternalAdvertisementBundle\EntityRepository\ExternalAdvertisementEntityRepository")
 */
class ExternalAdvertisement
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $external_id;      
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $link;      

    /** 
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updated_at;

    /** 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;

    /** 
     * @ORM\OneToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", 
     *     mappedBy="externalAdvertisement"
     * )
     */
    private $advertisements;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource", 
     *     inversedBy="externalAdvertisements"
     * )
     * @ORM\JoinColumn(name="external_advertisement_source_id", referencedColumnName="id", nullable=false)
     */
    private $externalAdvertisementSource;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession", 
     *     inversedBy="externalAdvertisements"
     * )
     * @ORM\JoinColumn(name="external_profession_id", referencedColumnName="id", nullable=false)
     */
    private $externalProfession;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace", 
     *     inversedBy="externalAdvertisements"
     * )
     * @ORM\JoinColumn(name="external_place_id", referencedColumnName="id", nullable=false)
     */
    private $externalPlace;

    /** 
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /** 
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $company_name;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $profession_name;       
    
    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $approved_at;

    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expire_at;    
    
    /** 
     * @ORM\Column(type="boolean", nullable=false, options={"default" = 0})
     */
    private $approved = false;

    /** 
     * @ORM\Column(type="boolean", nullable=false, options={"default" = 0})
     */
    private $rejected = false;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementType", 
     *     inversedBy="externalAdvertisements"
     * )
     * @ORM\JoinColumn(name="advertisement_type_id", referencedColumnName="id", nullable=false)
     */
    private $advertisementType;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return SocialAdvertisementData
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SocialAdvertisementData
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set approved_at
     *
     * @param \DateTime $approvedAt
     * @return SocialAdvertisementData
     */
    public function setApprovedAt($approvedAt)
    {
        $this->approved_at = $approvedAt;
    
        return $this;
    }

    /**
     * Get approved_at
     *
     * @return \DateTime 
     */
    public function getApprovedAt()
    {
        return $this->approved_at;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return SocialAdvertisementData
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    
        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set advertisementType
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType
     * @return SocialAdvertisementData
     */
    public function setAdvertisementType(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType)
    {
        $this->advertisementType = $advertisementType;
    
        return $this;
    }

    /**
     * Get advertisementType
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType 
     */
    public function getAdvertisementType()
    {
        return $this->advertisementType;
    }
    
    public function __toString() {
        return (string) $this->getTitle();
    }
    
    /**
     * Returns company name
     * @return type
     */
    public function getCompanyName() {
        return $this->company_name;
    }

    /**
     * Sets company name
     * @param type $company_name
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\SocialAdvertisementData
     */
    public function setCompanyName($company_name) {
        $this->company_name = $company_name;
        return $this;
    }

    
    public function getExpireAt() {
        return $this->expire_at;
    }

    public function setExpireAt($expire_at) {
        $this->expire_at = $expire_at;
        return $this;
    }


    /**
     * Set advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return ExternalAdvertisement
     */
    public function setAdvertisements(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements = null)
    {
        $this->advertisements = $advertisements;
    
        return $this;
    }

    /**
     * Get advertisements
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisements()
    {
        return $this->advertisements;
    }

    /**
     * Set externalAdvertisementSource
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSource
     * @return ExternalAdvertisement
     */
    public function setExternalAdvertisementSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSource)
    {
        $this->externalAdvertisementSource = $externalAdvertisementSource;
    
        return $this;
    }

    /**
     * Get externalAdvertisementSource
     *
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource 
     */
    public function getExternalAdvertisementSource()
    {
        return $this->externalAdvertisementSource;
    }

    /**
     * Set externalProfession
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfession
     * @return ExternalAdvertisement
     */
    public function setExternalProfession(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession $externalProfession)
    {
        $this->externalProfession = $externalProfession;
    
        return $this;
    }

    /**
     * Get externalProfession
     *
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession 
     */
    public function getExternalProfession()
    {
        return $this->externalProfession;
    }

    /**
     * Set externalPlace
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace
     * @return ExternalAdvertisement
     */
    public function setExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace)
    {
        $this->externalPlace = $externalPlace;
    
        return $this;
    }

    /**
     * Get externalPlace
     *
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace 
     */
    public function getExternalPlace()
    {
        return $this->externalPlace;
    }

    /**
     * Set updated_at
     *
     * @param string $updatedAt
     * @return ExternalAdvertisement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return string 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return ExternalAdvertisement
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }





    
    
//    public function __toString() {
//        if(!$this->getId()){
//            return '';
//        }
//        
//        if($this->getSocialAdvertisementData()) {
//            return $this->getExternalAdvertisementSource()->getResource() . ' - '.
//                    $this->getSocialAdvertisementData()->getTitle();
//        } else {
//            return $this->getExternalAdvertisementSource()->getResource();
//            //. '/'. $this->getSocialAdvertisementData()->getTitle();
//        }
//        
//    }
    
    public function getExternalId() {
        return $this->external_id;
    }

    public function setExternalId($externalId) {
        $this->external_id = $externalId;
        return $this;
    }

    public function getUpdated_at() {
        return $this->updated_at;
    }


    public function getCreated_at() {
        return $this->created_at;
    }


    public function getProfessionName() {
        return $this->profession_name;
    }

    public function setProfessionName($profession_name) {
        $this->profession_name = $profession_name;
        return $this;
    }

    /**
     * Returns advertisement link
     * @return string
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * Sets advertisement link
     * @param type $link
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement
     */
    public function setLink($link) {
        $this->link = $link;
        return $this;
    }

    public function getRejected() {
        return $this->rejected;
    }

    public function setRejected($rejected) {
        $this->rejected = $rejected;
        return $this;
    }



}