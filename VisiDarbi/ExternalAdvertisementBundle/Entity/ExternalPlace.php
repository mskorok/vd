<?php
namespace VisiDarbi\ExternalAdvertisementBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 */
class ExternalPlace
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement", 
     *     mappedBy="externalPlace"
     * )
     */
    private $externalAdvertisements;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity", 
     *     inversedBy="externalPlace"
     * )
     * @ORM\JoinColumn(name="map_to_city_id", referencedColumnName="id", nullable=true)
     */
    private $advertisementCity;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry", 
     *     inversedBy="externalPlaces"
     * )
     * @ORM\JoinColumn(name="map_to_country_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $advertisementCountry;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource", 
     *     inversedBy="externalPlaces"
     * )
     * @ORM\JoinColumn(name="external_advertisement_source_id", referencedColumnName="id", nullable=false)
     */
    private $externalAdvertisementSource; 
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->externalAdvertisements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ExternalPlace
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     * @return ExternalPlace
     */
    public function addExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements[] = $externalAdvertisements;
    
        return $this;
    }

    /**
     * Remove externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     */
    public function removeExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements->removeElement($externalAdvertisements);
    }

    /**
     * Get externalAdvertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalAdvertisements()
    {
        return $this->externalAdvertisements;
    }

    /**
     * Set city
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $city
     * @return ExternalPlace
     */
    public function setAdvertisementCity(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $city = null)
    {
        $this->advertisementCity = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity 
     */
    public function getAdvertisementCity()
    {
        return $this->advertisementCity;
    }

    /**
     * Set advertisementCountry
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry
     * @return ExternalPlace
     */
    public function setAdvertisementCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry = null)
    {
        $this->advertisementCountry = $advertisementCountry;
    
        return $this;
    }

    /**
     * Get advertisementCountry
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry 
     */
    public function getAdvertisementCountry()
    {
        return $this->advertisementCountry;
    }
    
    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return ExternalProfession
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }
    
    public function __toString() {
        return (string) $this->getName();
    }
    

    /**
     * Set externalAdvertisementSource
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSource
     * @return ExternalPlace
     */
    public function setExternalAdvertisementSource(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource $externalAdvertisementSource)
    {
        $this->externalAdvertisementSource = $externalAdvertisementSource;
    
        return $this;
    }

    /**
     * Get externalAdvertisementSource
     *
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource 
     */
    public function getExternalAdvertisementSource()
    {
        return $this->externalAdvertisementSource;
    }
}