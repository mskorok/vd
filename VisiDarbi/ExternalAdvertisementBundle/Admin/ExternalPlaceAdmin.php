<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of ExternalPlaceAdmin
 *
 * @author Aleksey
 */
class ExternalPlaceAdmin extends CommonAdmin {

    protected $baseRouteName = 'externalplace';
    protected $baseRoutePattern = 'externalplace';

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->add('externalAdvertisementSource.resource', null, array('label' => 'Resource'))
                ->add('name', null, array('label' => 'Resource place'))
                ->add('advertisementCountry.name', null, array(
                    'label' => 'Map to country',
                ))
                ->add('advertisementCity.name', null, array(
                    'label' => 'Map to city',
                ))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                    //'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))

        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('externalAdvertisementSource', null, array('label' => 'Resource', 'read_only' => true, 'disabled' => true))
                ->add('name', null, array(
                    'label' => 'Resource profession',
                    'required' => true,
                    'read_only' => true
                ))
                ->add('advertisementCountry', null, array(
                    'attr' => array('class' => 'country-selector span5'),
                    'label' => 'Map to country',
                    'class' => 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'property' => 'name', 'required' => false, 'query_builder' =>
                    function(EntityRepository $er) {

                        $query = $er->createQueryBuilder('c')
                                ->where('c.country = :country')->orderBy('c.name', 'ASC')
                                ->setParameter('country', $this->getCountryManager()
                                        ->getCurrentAdminCountry())
                                ->join('c.translations', 't')
                                ->andWhere('t.field=\'name\'')
                                ->orderBy('t.content');

                        return $query;
                    }))
                ->add('advertisementCity', null, array(
                    'attr' => array('class' => 'city-selector span5'),
                    'label' => 'Map to city',
                ))

        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('externalAdvertisementSource', null, array(
                    'label' => 'Resource',
                ))->add('name', null, array(
            'label' => 'Resource place name',
        ))
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

    public function getTemplate($name) {
        if ($name == 'edit') {
            return 'VisiDarbiExternalAdvertisementBundle:ExternalPlaceAdmin:place_edit.html.twig';
        }
        return parent::getTemplate($name);
    }

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection) {
        $collection->remove('create');
        $collection->remove('delete');
    }

    public function preUpdate($externalPlace) {
        parent:: preUpdate($externalPlace);

        $query = $this->getEm()
                ->createQueryBuilder('ea')
                ->select('a')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->join('a.externalAdvertisement', 'ea')
                ->join('ea.externalPlace', 'ep')
                ->where('ep.id = :externalPlaceId')
                ->setParameter('externalPlaceId', $externalPlace->getId());

        $query = $query->getQuery();
        $r = $query->getResult();

        foreach ($r as $ad) {

            if($externalPlace->getAdvertisementCountry()) {
                $ad->setAdvertisementCountry($externalPlace->getAdvertisementCountry());
            } else {
                $defCountry = $this->getCountryManager()->getCurrentCountry()->getDeafultAdvertisementCountry();
                $ad->setAdvertisementCountry($defCountry);
            }
            
            $ad->setAdvertisementCity($externalPlace->getAdvertisementCity());


            $this->getEm()->flush($ad);
        }
    }

}

?>
