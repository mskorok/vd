<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;
use Doctrine\ORM\EntityRepository;
use JMS\TranslationBundle\Annotation\Ignore;

/**
 * SocialAdvertisementAdmin class
 *
 * @author Aleksey
 */
class SocialAdvertisementAdmin extends CommonAdmin {

    protected $baseRouteName = 'socialadvertisement';
    protected $baseRoutePattern = 'socialadvertisement';
    protected $classnameLabel ='SocialAdvertisement';

    protected function configureListFields(ListMapper $listMapper) {
                
        $actions = array(
                        'edit' => array(),
                        'approve' => array('template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:list__action_approve.html.twig' ),
                        'delete' => array(),
                    );
        
        
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->add('title', null, array('label' => 'title'))
                ->add('description', null, array('label' => 'Description', 'template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:description_short.html.twig'))
                ->add('externalAdvertisementSource', null, array('label' => 'Resource'))
                ->add('created_at', null, array('label'=>'Created at', 'time_widget'=>'single_text', 'date_widget'=>'single_text', 'format'=>'yyy-M-D h:m'))
                ->add('approved', 'boolean', array('label' => 'Approved'))
                ->add('rejected', 'boolean', array('label' => 'Rejected'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array('template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:list__action_edit.html.twig'),
                        'approve' => array('template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:list__action_approve.html.twig' ),
                        'reject' => array('template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:list__action_reject.html.twig' ),
                        //'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))

        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('title', null, array('label'=>'Title'))
                ->add('description', 'textarea', array('label'=>'Description'))
                ->add('company_name', null, array('label'=>'Company'))
                ->add('created_at', null, array('read_only'=>true, 'disabled'=>true ,'label'=>'Created at', 'time_widget'=>'single_text', 'date_widget'=>'single_text', 'format'=>'yyy-M-D h:m'))

                ->add('externalProfession', null, array('label' => 'Profession',  'query_builder' =>
                    function(EntityRepository $er) {                        
                        $query = $er->createQueryBuilder('o')
                                ->where('o.externalAdvertisementSource = :source')->orderBy('o.name', 'ASC')
                                ->setParameter('source', $this->getSubject()->getExternalAdvertisementSource());
            
                        return $query;
                    }))
                    ->add('externalPlace', null, array('label' => 'Place', 'query_builder' =>
                    function(EntityRepository $er) {                        
                        $query = $er->createQueryBuilder('o')
                                ->where('o.externalAdvertisementSource = :source')->orderBy('o.name', 'ASC')
                                ->setParameter('source', $this->getSubject()->getExternalAdvertisementSource());
            
                        return $query;
                    }))
                          
            ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        
        /**
         * @todo fix listbox order - VISIDA-309
         */
        $datagridMapper
                ->add('title', null, array('label' => 'Title'))
                ->add('externalAdvertisementSource', null, array('label' => 'Resource', 'field_options' => array('class' => 'VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource', 'property' => 'resource', 'query_builder' =>
                        function(EntityRepository $er) {

                            $q = $er->createQueryBuilder('q')
                                    ->orderBy('q.resource', 'ASC');
                            return $q;
                        })))                
                ->add('approved', null, array('label' => 'Is approved'))
                ->add('rejected', null, array('label' => 'Is rejected'))
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }
    
    
    public function createQuery($context = 'list') {

        $query = $this->getModelManager()->createQuery($this->getClass(), 'o');
        $query->select(array('o', 'es'))
                ->leftJoin('o.externalAdvertisementSource', 'es')
                ->where('es.country = :country ')
                ->andWhere('es.is_social = true')
                ->setParameter(':country', $this->getCountryManager()->getCurrentAdminCountry());

        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }    
  
    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection
            ->add('approve',
                'approve/{id}',
                array('_controller' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:approve'),
                array('id' => '\d+')
            );
        $collection->add('delete',
                'delete/{id}',
                array('_controller' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:delete'),
                array('id' => '\d+')
            )
        ;
        
        $collection->remove('create');
    }    
    
    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:social_edit.html.twig';
        }
        return parent::getTemplate($name);
    }    
    
    
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
                'rejected' => array(
                    'value' => 2,
                )
            ),
            $this->datagridValues

        );
        return parent::getFilterParameters();
    }    
    
}

?>
