<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of SocialAdvertisementDataAdmin
 *
 * @author Aleksey
 */
class SocialAdvertisementDataAdmin extends CommonAdmin {
    
    
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('title', null, array(
                    'label' => 'Title',
                    'required' => true
                ))
                ->add('description', null, array(
                    'label' => 'Descripion',
                    'required' => true
                 )) 
                ->add('advertisementType', null, array(
                    'label' => 'Type',
                    'required' => true
                 ))                
;
    }

}

?>
