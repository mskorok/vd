<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of ExternalAdvertisementSourceAdmin
 *
 * @author Aleksey
 */
class ExternalProfessionAdmin extends CommonAdmin {

    protected $baseRouteName = 'externalprofession';
    protected $baseRoutePattern = 'externalprofession';

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->add('externalAdvertisementSource.resource', null, array('label' => 'Resource'))
                ->add('name', null, array('label' => 'Resource profession'))
                ->add('_map_to', null, array(
                    'label' => 'Map to profession',
                    'template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:map_profession.html.twig'
                    ))             
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                    //'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))

        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('externalAdvertisementSource', null, array('label' => 'Resource', 'read_only' => true, 'disabled' => true))
                ->add('name', null, array(
                    'label' => 'Resource profession',
                    'required' => true,
                    'read_only' => true
                ))
                ->add('professions', 'entity', array(
                    'label' => 'Map to profession',
                    'choices' => $this->getProfessionsChoice(),
                    'multiple' => true,
                    'property' => 'adminName',
                    'class' => 'VisiDarbi\ProfessionBundle\Entity\Profession',
                    'attr'=>array('style'=>'min-width:500px; min-height:400px;')))
        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('externalAdvertisementSource.resource', null, array('label' => 'Resource'))
                ->add('name', null, array(
                    'label' => 'Resource profession',
                    'required' => true
                ))
                ->add('professions.id', 'doctrine_orm_choice', array('label' => 'Map to profession',
                    'field_options' => array(
                        'choices' => $this->getProfessionsChoice(),
                    ),
                    'field_type' => 'choice'
                ))
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

    public function preUpdate($externalProfession) {
        parent:: preUpdate($externalProfession);

        $query = $this->getEm()
                ->createQueryBuilder('ea')
                ->select('a')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->join('a.externalAdvertisement', 'ea')
                ->join('ea.externalProfession', 'ep')
                ->where('ep.id = :externalProfessionId')
                ->setParameter('externalProfessionId', $externalProfession->getId());

        $query = $query->getQuery();
        $r = $query->getResult();

        foreach ($r as $ad) {

            if (!$ad->getProfessions()->count()) {

                $mapped = $externalProfession->getProfessions();
                $ad->setProfessions($mapped);

                foreach ($mapped as $p) {
                    $ad->addCategories($p->getCategory());
                }

                $this->getEm()->flush($ad);
            }
        }
    }

    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection) {

        $collection->remove('create');
        $collection->remove('delete');
    }

    private function getProfessionsChoice() {

        $professionsList = array();

        $query = $this->getEm()->createQueryBuilder('p')
                ->select('p, c')
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->join('p.category', 'c')
                ->where('c.country = :country')
                ->orderBy('c.name, p.name')
                ->setParameter('country', $this->getCountryManager()->getCurrentAdminCountry());

        $query = $query->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale())
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $r = $query->getResult();

        foreach ($r as $p) {
            $professionsList[$p->getId()] = $p;
        }

        return $professionsList;
    }

}

?>
