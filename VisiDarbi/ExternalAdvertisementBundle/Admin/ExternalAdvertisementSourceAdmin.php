<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of ExternalAdvertisementSourceAdmin
 *
 * @author Aleksey
 */
class ExternalAdvertisementSourceAdmin extends CommonAdmin {

    protected $baseRouteName = 'externalsource';
    protected $baseRoutePattern = 'externalsource';

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->add('resource', null, array('label' => 'Resource'))
                ->add('show_as', null, array('label' => 'Front-end alias'))
                
                ->add('_adCount', null, array(
                    'label' => 'Advertisements count',
                    //'sortable' => 'externalAdvertisementsCount',
                    'template' => 'VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementSourceAdmin:advertisement_count.html.twig'
                    ))
                ->add('show_advertisiment', null, array('label' => 'Active'))                
                ->add('is_social', null, array('label' => 'Is Social resource'))                
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))

        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('resource', null, array(
                    'label' => 'Resource',
                    'read_only' => true,
                    'disabled' => true,                    
                    'required' => true
                ))
                ->add('show_as', null, array(
                    'label' => 'Front-end alias',
                    'required' => false
                ))
                ->add('is_social', null, array(
                    'label' => 'Is social',
                    'required' => false,
                    'read_only' => true,
                    'disabled' => true,
                    'attr'=>array('readonly' => true)
                ))
                ->add('show_advertisiment', null, array('label' => 'Active'))
                ->add('is_supported_mobile', null, array(
                    'required' => false,
                    'label' => 'Does source support mobile version'
                ))
                
//                ->with('Places', array('collapsed' => true))
//                    ->add('externalPlaces', 'sonata_type_collection', 
//                            array('required' => false, 'by_reference' => false, 'label' => 'Places'), array(
//                                'edit' => 'inline',
//                                'inline' => 'table',
//                                'sortable' => false
//                    )) 
//                ->end()
//                ->with('Professions', array('collapsed' => true))                
//                ->add('externalProfessions', 'sonata_type_collection', 
//                        array('required' => false, 'by_reference' => false, 'label' => 'Professions'), array(
//                            'edit' => 'inline',
//                            'inline' => 'table',
//                            'sortable' => false
//                ))
//                ->end()            
            ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('resource', null, array('label' => 'Resource'))
                ->add('show_advertisiment', null, array('label' => 'Is active'))
                ->add('is_social', null, array('label' => 'Is social resource'))
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }
    
    
//    public function createQuery($context = 'list') {
//
//        $query = $this->getModelManager()->createQuery($this->getClass(), 'o');
//        $query->select(array('o', 'count(c.id)'))
//                ->leftJoin('o.externalAdvertisements', 'c')
//                ->where('o.country = :country')
//                
//                ->setParameter(':country', $this->getCountryManager()->getCurrentAdminCountry());
//
//        foreach ($this->extensions as $extension) {
//            $extension->configureQuery($this, $query, $context);
//        }
//
//        return $query;
//    }    
    
    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }      
  

}

?>
