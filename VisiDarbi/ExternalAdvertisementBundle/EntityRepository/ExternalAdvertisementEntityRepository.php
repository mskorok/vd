<?php

namespace VisiDarbi\ExternalAdvertisementBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Company;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\ProfessionBundle\Entity\Profession;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Translatable\Entity\Translation;

/**
 * Custom EntityRepository for external advertisements entities needs
 *
 * @author Aleksey
 */
class ExternalAdvertisementEntityRepository extends EntityRepository {

    public function getImportedExternalAdvertisementsIds($country, $source, $date) {

        $q = $this->_em->createQueryBuilder()
                ->select('ea.external_id')
                ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement', 'ea')
                ->join('ea.externalAdvertisementSource', 's')
                ->where('s.resource = :source')
                ->andWhere('ea.created_at >= :nowDate')
                ->andWhere('s.country = :country')
                ->setParameter('country', $country->getId())
                ->setParameter('nowDate', $date)
                ->setParameter('source', $source)
                ->getQuery();

        $q->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $existentAd = $q->getResult();

        $dataSet = array();
        foreach ($existentAd as $ad) {
            $dataSet[$ad['external_id']] = $ad['external_id'];
        }

        return $dataSet;
    }

    /**
     * Find imported profession
     * @param string $name
     * @param string $source
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession
     */
    public function findExternalProfession($name, $source) {

        $q = $this->_em->createQueryBuilder()
                ->select('p')
                ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession', 'p')
                ->join('p.externalAdvertisementSource', 's')
                ->where('s.resource = :source')
                ->andWhere('p.name like :name')
                ->setParameter('source', $source)
                ->setParameter('name', $name)
                ->setMaxResults(1)
                ->getQuery();

        $q->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $profession = $q->getOneOrNullResult();

        return $profession;
    }

    /**
     * Find imported place
     * @param string $name
     * @param string $source
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace
     */
    public function findExternalPlace($name, $source) {

        $q = $this->_em->createQueryBuilder()
                ->select('p, s, ac, acc')
                ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace', 'p')
                ->leftJoin('p.externalAdvertisementSource', 's')
                ->leftJoin('p.advertisementCity', 'ac')
                ->leftJoin('p.advertisementCountry', 'acc')
                ->where('s.resource = :source')
                ->andWhere('(p.name like :name OR p.name = :emptyName)')
                ->andWhere("(p.name like  :name OR p.name IS NULL) ")
                ->setParameter('source', $source)
                ->setParameter('name', $name)
                ->setParameter('emptyName', '')
                ->setMaxResults(1)
                ->getQuery();

        $q->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $place = $q->getOneOrNullResult();

        return $place;
    }

    public function postExternalToInternal($externalAdvertisemen) {

        if (!($externalAdvertisemen instanceof ExternalAdvertisement)) {

            $externalAdvertisemen = (int) $externalAdvertisemen;

        } else {
            $externalAdvertisemen = (int) $externalAdvertisemen->getId();
        }

        $q = $this->_em->createQueryBuilder()
                ->select('a, ep, p')
                ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement', 'a')
                ->leftJoin('a.externalProfession', 'ep')
                ->leftJoin('ep.professions', 'p')
                ->where('a.id = :id')
                ->setParameter('id', $externalAdvertisemen)
                ->setMaxResults(1)
                ->getQuery();

        $externalAdvertisemen = $q->getOneOrNullResult();

        if (!$externalAdvertisemen) {
            return null;
        }
        //Note: will return only one AD
        $advertisement = $externalAdvertisemen->getAdvertisements();

        $isNew = false;
        if (!($advertisement instanceof Advertisement)) {
            $isNew = true;
            $advertisement = new Advertisement();
        } else {
            $this->_em->remove($advertisement);
            $this->_em->flush();
            $advertisement = new Advertisement();
            $isNew = true;
        }


        $country = $externalAdvertisemen->getExternalAdvertisementSource()->getCountry();



        $locales = $country->getLocales();
        $adCountry = $externalAdvertisemen->getExternalPlace()->getAdvertisementCountry();
        $adCity = $externalAdvertisemen->getExternalPlace()->getAdvertisementCity();

        if(!$adCountry) {
            $adCountry = $country->getDeafultAdvertisementCountry();
        }


        $adProfession = $externalAdvertisemen->getExternalProfession()->getProfessions();


        if ($isNew) {

            $advertisement->setCountry($country);
            $advertisement->setExternalAdvertisement($externalAdvertisemen);
            $advertisement->setAdvertisementCountry($adCountry);

            $advertisement->setAdvertisementCity($adCity);

            $expiredDate = $externalAdvertisemen->getExpireAt();
            $daysPeriod = $externalAdvertisemen->getAdvertisementType()->getDaysPeriod();
            $now = new \DateTime();
            $advertisement->setDateFrom(clone $now);
            $dateTo = $now->add(new \DateInterval('P' . $daysPeriod . 'D'));

            if ($expiredDate) {

                if ($expiredDate <= $dateTo) {
                    $dateTo = $expiredDate;
                }
            }

            $advertisement->setDateTo($dateTo);


            if ($adProfession) {

                foreach($adProfession as $p) {
                    $advertisement->addProfession($p);
                    $advertisement->addCategories($p->getCategory());
                }
            }

            $advertisement->setAdvertisementType($externalAdvertisemen->getAdvertisementType());
            $advertisement->setCountry($country);
            $advertisement->setStatus(Advertisement::STATUS_ACTIVE);
            $advertisement->setTitle($externalAdvertisemen->getTitle());

            $description = $externalAdvertisemen->getDescription();

            if(!$description) {
                $description = $externalAdvertisemen->getTitle();
            }

            $advertisement->setDescription($description);

            foreach ($locales as $locale) {
                $advertisement->addTranslation(new AdvertisementTranslation($locale, 'title', $externalAdvertisemen->getTitle()));
                $advertisement->addTranslation(new AdvertisementTranslation($locale, 'description', $description));
            }

            foreach ($advertisement->getTranslations() as $at) {
                /* @var $at \VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation */
                if ($at->getField() == 'title') {
                    $slug = new \VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug();
                    $advertisement->addSlug($slug->setLocale($at->getLocale())->setSlugSource($at->getContent()));
                }
            }


            $this->_em->persist($advertisement);

            if ($externalAdvertisemen->getCompanyName()) {
                $company = new Company();
                $company->setName($externalAdvertisemen->getCompanyName());
                $company->setAdvertisement($advertisement);
                $this->_em->persist($company);
            }

            $externalAdvertisemen->setApproved(true);
            $externalAdvertisemen->setRejected(false);
            //$this->_em->flush($externalAdvertisemen);

            $this->_em->flush();


        }

        return $advertisement;

    }

    /**
     *
     * @param ExternalAdvertisement $externalAdvertisement
     * @param null|Advertisement $advertisement
     * @return Advertisement
     */
    public function updateInternalFromExternal($externalAdvertisement, $advertisement = null) {

        if (!($externalAdvertisement instanceof ExternalAdvertisement)) {

            $externalAdvertisement = (int) $externalAdvertisement;

        } else {
            $externalAdvertisement = (int) $externalAdvertisement->getId();
        }

//        $this->_em->clear();
//        $ad = $this->_em->find('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 104468);
//
//        foreach ($ad->getTranslations() as $t){
//            var_dump($t->getField());
//        }
//        var_dump($ad->getTranslations()->count());die;

        $q = $this->_em->createQueryBuilder()
                ->select('a, ep, p, eas, easc')
                ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement', 'a')
                ->leftJoin('a.externalProfession', 'ep')
                ->leftJoin('a.externalAdvertisementSource', 'eas')
                ->leftJoin('eas.country', 'easc')
                ->leftJoin('ep.professions', 'p')
                ->where('a.id = :id')
                ->setParameter('id', $externalAdvertisement)
                ->setMaxResults(1)
//                ->getQuery()
//                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, false)
        ;

        $q = $q->getQuery()
//            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
//            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, 'lv')
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);


        $externalAdvertisement = $q->getOneOrNullResult();

        if (!$externalAdvertisement) {
            return null;
        }


        //Note: will return only one AD
        if (null == $advertisement){
            $advertisement = $externalAdvertisement->getAdvertisements();
        }

        $this->_em->clear();
        $advertisement = $this->_em->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')->findOneBy(array(
           'externalAdvertisement' => $externalAdvertisement->getId(),
        ));

        $externalAdProfessions = $externalAdvertisement->getExternalProfession()->getProfessions();
        $externalAdCategories =  new ArrayCollection();

        $adProfessions = $advertisement->getProfessions();
        $adCategories = $advertisement->getCategories();


        /*
        $advertisement
            ->setProfessions(array_intersect($adProfessions, $externalAdProfessions))
        ;*/

        // add new professions and categories
        if ($externalAdProfessions){
            foreach ($externalAdProfessions as $extProf){

               if (!$adProfessions->contains($extProf)){
                   $advertisement->addProfession($extProf);
               }

               $extProfCategory = $extProf->getCategory();
               if (!$adCategories->contains($extProfCategory)){
                   $advertisement->addCategorie($extProfCategory);
               }

               $externalAdCategories->add($extProfCategory);
            }
        }

        // remove old professions and categories
        if ($adProfessions){
            foreach ($adProfessions as $prof){

               if (!$externalAdProfessions->contains($prof)){
                   $advertisement->removeProfession($prof);
               }

               $profCategory = $prof->getCategory();
               if (!$externalAdCategories->contains($profCategory)){
                   $advertisement->removeCategorie($profCategory);
               }
            }
        }

//        $advertisement
//            ->setAdvertisementType($externalAdvertisement->getAdvertisementType())
//            ->setCountry($country)
//            ->setStatus(Advertisement::STATUS_ACTIVE)
//        ;

        $isTitleChanged = ($advertisement->getTitle() !== $externalAdvertisement->getTitle());

        if ($isTitleChanged){
            $advertisement
                ->setTitle($externalAdvertisement->getTitle())
            ;
        }

        $description = $externalAdvertisement->getDescription();

        if(!$description) {
            $description = $externalAdvertisement->getTitle();
        }

        $advertisement->setDescription($description);


        /**
         * update translations
         */


        $country = $externalAdvertisement->getExternalAdvertisementSource()->getCountry();

        $locales = $country->getLocales();

//        var_dump($locales);die;
//$locales = array('lv','ru','en');

        $adTranslations = $advertisement->getTranslations();

        $translationNewValues = array();
//        $translationNewValues = array(
//            'description'=>$description,
//            );

//        $this->_em->merge($advertisement);
//        $this->_em->flush();
//        die('z');


        /**
         * should we really update all languages since updated adv could have already translated (after first import) content
         */

        foreach ($locales as $locale) {
            $translationNewValues[$locale]['description'] = $description;
            if ($isTitleChanged){
                $translationNewValues[$locale]['title'] = $externalAdvertisement->getTitle();
            }
        }


        /**
         * update existing
         */
        if ($adTranslations){
            foreach ($adTranslations as $k=>$t){
                foreach ($locales as $locale) {
                    if (in_array($t->getField(), array_keys($translationNewValues[$locale])) && $t->getLocale() == $locale){
                       $adTranslations[$k]->setContent($translationNewValues[$locale][$t->getField()]);
                       unset($translationNewValues[$locale][$t->getField()]);
                    }
                }

            }
        }

        /**
         * adding new
         */
        $newAdded = false;
        foreach ($locales as $locale) {
            if (!empty($translationNewValues[$locale])){
                foreach ($translationNewValues[$locale] as $fieldName=>$fieldValue){
                    $tr = new AdvertisementTranslation();
                    $tr
                        ->setLocale($locale)
                        ->setField($fieldName)
                        ->setContent($fieldValue)
                    ;
                    $this->_em->persist($tr);
                    $newAdded = true;
                }

            }
        }

        if ($newAdded){
            $this->_em->flush();
        }

        $advertisement->setTranslations($adTranslations);

//        foreach ($advertisement->getTranslations() as $at) {
//            var_dump($t->getLocale(), $at->getField(), $at->getContent());
//        }
//        die;

        /** should we udpate slug?
         if ($isTitleChanged){
            //remove
            $advertisementSlugs = $advertisement->getSlugs();
            foreach($advertisementSlugs as $k=>$adSlug){
                $advertisement->removeSlug($adSlug);
            }

            //add new
            foreach ($advertisement->getTranslations() as $at) {
                /* @var $at \VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation */ /*
                if ($at->getField() == 'title') {
                    $slug = new \VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug();
                    $advertisement->addSlug($slug->setLocale($at->getLocale())->setSlugSource($at->getContent()));
                }
            }
         }
        */


        $this->_em->merge($advertisement);

        if ($externalAdvertisement->getCompanyName() && $externalAdvertisement->getCompanyName() != $advertisement->getCompanies()->getName()) {
            $company = $advertisement->getCompanies();
            $company
                ->setName($externalAdvertisement->getCompanyName())
            ;
            $this->_em->merge($company);
            $this->_em->flush($company);
        }

        $externalAdvertisement
            ->setApproved(true)
            ->setRejected(false)
         ;

        $this->_em->flush($advertisement);

        return $advertisement;

    }

}
