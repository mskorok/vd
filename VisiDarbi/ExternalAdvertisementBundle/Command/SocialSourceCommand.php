<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\ExternalAdvertisementBundle\Lib\LinkedIn;
use VisiDarbi\ExternalAdvertisementBundle\Lib\OAuthConsumer;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession;
use VisiDarbi\ExternalAdvertisementBundle\Entity\SocialAdvertisementData;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement;

#use VisiDarbi\ExternalAdvertisementBundle\Lib\OAuthConsumer;
/**
 * Command class to clear old (60 days) search keywords
 *
 * @author Aleksey
 */

class SocialSourceCommand extends ContainerAwareCommand {

    const TYPE_LINLEDIN = 'linkedin';
    
    
    protected $configParams;
    protected $linkedIn;
    protected $em;
    protected $er;
    protected $input;
    protected $output;
    protected $grabDate;
    protected $country;
    protected $source;
    protected $advertisementType;

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi-social:import')->setDescription('Grab linkedin advertisements');
        $this->addArgument('source', InputArgument::REQUIRED, 'Set source like, linkedin, ss.lv, etc');
        $this->addArgument('country', InputArgument::REQUIRED, 'Country for query and assign addvertisement');
        $this->addOption('fromdate', null,  InputOption::VALUE_OPTIONAL, 'Start advertisements from date');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->em = $this->getContainer()->get('Doctrine')->getEntityManager();
        $this->er = $this->getContainer()->get('Doctrine')
                ->getRepository('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement');

        $this->input = $input;
        $this->output = $output;
        
        if(!$input->getOption('fromdate')){
            $this->grabDate = new \DateTime();
            
        } else {
            $this->grabDate = new \DateTime($input->getOption('fromdate'));
        }
        
        $now = new \DateTime();
        
        $output->writeln('Run at - ' . $now->format('Y-m-d H:i'));
        $output->writeln('Parsing date - ' . $this->grabDate->format('Y-m-d'));

        $this->country = $this->getContainer()->get('Doctrine')
                ->getRepository('VisiDarbi\LocalePlaceBundle\Entity\Country')
                ->findOneBy(array('code' => $input->getArgument('country')));

        $this->source = $input->getArgument('source');


        $this->advertisementType = $this->getContainer()->get('Doctrine')
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementType')
                ->findOneBy(array('id' => \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType::TYPE_LIMITED_14_DAY));

        switch ($input->getArgument('source')) {
            case self::TYPE_LINLEDIN: $this->grabLinkedIn($input, $output);
                break;
        }

        $output->writeln('End of job parsing - ' . $input->getArgument('source'));
    }

    /**
     * Grab advertisements from linked in
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function grabLinkedIn(InputInterface $input, OutputInterface $output) {

        $existentAd = $this->er->getImportedExternalAdvertisementsIds($this->country, $this->source, $this->grabDate);

        $this->configParams = $this->getContainer()->getParameter(self::TYPE_LINLEDIN);

        $this->linkedin = new LinkedIn($this->configParams['APIKey'], $this->configParams['secretKey'], null);
        $this->linkedin->access_token = new OAuthConsumer($this->configParams['OAuthUserToken'], $this->configParams['OAuthUserSecret'], null);

        $count = 20;
        $start = 0;
        $stop = false;
        $n = 0;

        do {

            $output->writeln('Read advertisements starting from ' . $start);
            $data = $this->getDataPage($start, $count);
            $start = $start + 20;

            if (!count($data->jobs->job)) {
                $stop = true;
            } else {

                foreach ($data->jobs->job as $job) {

                    $n++;
                    $jobArray = (array) $job;

                    $output->writeln('Job ID: ' . $jobArray['id'] . '; Date: ' . join('-', (array) $jobArray['posting-date']));

                    $postDate = new \DateTime(join('-', (array) $jobArray['posting-date']));

                    if ($postDate < $this->grabDate) {

                        $output->writeln('Ignore ' . join('-', (array) $jobArray['posting-date']));
                        continue;

                    } else {
                        if(!isset($existentAd[$jobArray['id']])) {
                            $this->storeLinkedInJob($job);
                        } else {
                            $output->writeln('Job ID: ' . $jobArray['id']. ' already imported; record ignored.');
                        }
                    }
                }
            }

            usleep(500000);
            
        } while (!$stop);
    }

    public function getDataPage($start, $count) {

        $searchParams = array('start' => $start,
            'count' => $count,
            'date' => $this->grabDate->format('Ymd'),
            'country' => $this->input->getArgument('country'),
            'sort'=>'DA');

        $search_response = $this->linkedin->getJobList($searchParams);

        $data = simplexml_load_string($search_response);

        return $data;
    }

    protected function storeLinkedInJob($data) {

        $positionData = $data->position;
            
        $source = $this->getContainer()->get('Doctrine')
                    ->getRepository('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource')
                    ->findOneBy(array('resource' => $this->source));

        if(!$source) {
            $source = new ExternalAdvertisementSource();
            $source->setCountry($this->country);
            $source->setResource($this->source);
            $source->setIsSocial(true);
            $source->setShowAdvertisiment(true);
            $source->setShowAs('linkedin.com');
            $this->em->persist($source); 
        }

        
        $profession = $this->er->findExternalProfession($positionData->title, $this->source);
        $place = $this->er->findExternalPlace($positionData->location->name, $this->source);

        if (!$place) {
            $place = new ExternalPlace();
            $place->setName($positionData->location->name);
            $place->setExternalAdvertisementSource($source);
            $this->em->persist($place);
        }

        if (!$profession) {
            $profession = new ExternalProfession();
            $profession->setName($positionData->title);
            $profession->setExternalAdvertisementSource($source);
            $this->em->persist($profession);
        }

        $adData = new ExternalAdvertisement();

        $adData->setAdvertisementType($this->advertisementType);
        $adData->setExternalId($data->id);
        $adData->setExternalAdvertisementSource($source);
        $adData->setExternalProfession($profession);
        $adData->setExternalPlace($place);
        $adData->setApproved(true);
        $adData->setApprovedAt(new \DateTime());
        $adData->setDescription(strip_tags($data->description));
        $adData->setTitle($positionData->title);
        $this->setSocialAdvertisementLink($adData);

        if (isset($data->company->name)) {
            $adData->setCompanyName($data->company->name);
        }

        $attrName = 'expiration-date';
        if (!isset($data->$attrName)) {
            $dataExpired = $data->$attrName;
            $expireAt = new \DaeTime($dataExpired->year . '-' . $dataExpired->month . '-' . $dataExpired->day);
            $adData->setExpireAt($expireAt);
        }

        $this->em->persist($adData);
        $this->postProcessJob($adData);

        $this->em->flush();
    }

    private function setSocialAdvertisementLink($externalAdvertisement) {

        switch ($this->source) {
            case self::TYPE_LINLEDIN: {
                    $link = 'http://www.linkedin.com/jobs?viewJob=&jobId=' . $externalAdvertisement->getExternalId();
                    $externalAdvertisement->setLink($link);
                }break;
        }
    }

    public function postProcessJob($externalAdvertisement) {
        switch ($this->source) {
            case self::TYPE_LINLEDIN: {
                    //post to regualr AD
                    $this->er->postExternalToInternal($externalAdvertisement);
                }break;
        }
    }

}

?>
