<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\ExternalAdvertisementBundle\Lib\LinkedIn;
use VisiDarbi\ExternalAdvertisementBundle\Lib\OAuthConsumer;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession;
use VisiDarbi\ExternalAdvertisementBundle\Entity\SocialAdvertisementData;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement;
use VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

#use VisiDarbi\ExternalAdvertisementBundle\Lib\OAuthConsumer;
/**
 * Command class to grab, map grabbed or update grabbed advertisements
 * Run examples:
 * php app/console visidarbi-external:import parser parser=parser1 --env=dev
 * php app/console visidarbi-external:import map --env=dev
 * php app/console visidarbi-external:import update --env=dev
 *
 * @author Aleksey
 */

class ExternalSourceCommand extends ContainerAwareCommand {

    const SYS_COUNTRY ='lv';
    const PARSER1 = 'parser1';
    const PARSER2 = 'parser2';
    const PARSER3 = 'parser3';
    const PROCESS_LIMIT = 10;

    protected $er;
    protected $em;
    protected $input;
    protected $output;
    protected $country;
    protected $source;
    protected $sourcesCache = array();
    protected $advertisementType;

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi-external:import')->setDescription('Import from visidarbi external sources');
        $this->addArgument('action', InputArgument::REQUIRED, 'parser - import data from resources; map - map data in to DB');
        $this->addOption('parser', null,  InputOption::VALUE_OPTIONAL, 'parser1 or parser2', 'parser1');

    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->em = $this->getContainer()->get('Doctrine')->getEntityManager();
        $this->er = $this->getContainer()->get('Doctrine')
                    ->getRepository('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement');

        $this->input = $input;
        $this->output = $output;

        $this->country  = $this->getContainer()->get('Doctrine')
                    ->getRepository('VisiDarbi\LocalePlaceBundle\Entity\Country')
                    ->findOneBy(array('code' => self::SYS_COUNTRY));

        $this->advertisementType  = $this->getContainer()->get('Doctrine')
                    ->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementType')
                    ->findOneBy(array('id' => \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType::TYPE_LIMITED_14_DAY));

        switch ($input->getArgument('action')) {
            case 'parser': $this->importData($input, $output); // grap external ads
                break;
            case 'map': $this->mapData($input, $output); // map/publish grabbed ads
                break;
            case 'update': $this->updateMappedAdsData($input, $output); // update mapped ads data
                break;
        }
    }

    /**
     * updates already mapped advertisements (having "map_action" field not equals to "none")
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function updateMappedAdsData(InputInterface $input, OutputInterface $output) {

        $connection = $this->em->getConnection();
        $limit = self::PROCESS_LIMIT;

        $stop = false;
        $maxIterations = 50;
        $iteration = 0;
        do{
            $iteration++;
            $sql = "SELECT * FROM `slud_data` WHERE `is_mapped` = 1 AND `map_action` != 'none' ORDER BY `id`  LIMIT {$limit}";
            $stmt = $connection->prepare($sql);
            $stmt->execute();

            if($stmt->rowCount())
            {

                while ($row = $stmt->fetch())
                {

                    switch ($row['map_action']){
                        case 'deactivate':
                            $this->deactivateSingleMappedAdData($row);
                            break;
                        case 'update':
                        default:
                            $this->updateSingleMappedAdData($row);
                            break;
                    }

                    $sqlUpdate = "UPDATE `slud_data` SET `is_mapped` = 1, `map_action` = 'none', `updated_at` = '".date("Y-m-d H:i:s")."' WHERE `id` = :id";
                    $stmtUpdate = $connection->prepare($sqlUpdate);
                    $stmtUpdate->bindParam(':id', $row['id'], \PDO::PARAM_INT);
                    $stmtUpdate->execute();
                }


            } else {
                $stop = true;
            }


            if($iteration >= $maxIterations) {
                $stop = true;
            }

        } while(!$stop);

    }

    /**
     * deactivate single advertisement (which was mapped before) data
     * @param array $row
     */
    protected function deactivateSingleMappedAdData($row) {

//        $externalAdvertisement = $this->er->findOneBy(array('link'=>$row['link']));
        $q = $this->em->createQueryBuilder()
                ->select('a, ea')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->innerJoin('a.externalAdvertisement', 'ea')
                ->where('ea.link = :link')
//                ->andWhere('a.status = :status')
                ->setParameter('link', $row['link'])
//                ->setParameter('status', Advertisement::STATUS_ACTIVE)
                ->setMaxResults(1)
                ->getQuery();

        $q->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $advertisement = $q->getOneOrNullResult();
        /* @var $advertisement Advertisement */
        if ($advertisement && $advertisement->getStatus() != Advertisement::STATUS_INACTIVE_CLOSED){
            $this->output->writeln(sprintf("disabling advertisement %d", $advertisement->getId()));
            $advertisement
                ->setStatus(Advertisement::STATUS_INACTIVE_CLOSED)
            ;

            $this->em->merge($advertisement);
            $this->em->flush();
        }
    }

    /**
     * updates single advertisement (which was mapped before) data
     * @param array $row
     */
    protected function updateSingleMappedAdData($row) {

        /**
         * Strategy:
         * 1) Update ExternalPlace (location) if needed
         * 2) Update ExternalProfession (profession) if needed
         * 3) Update ExternalAdvertisement (or create new?)
         * 4) Update Advertisement (or map to new ExternalAd?)
         * 5) Update AdvertisementEmail
         */


        $q = $this->em->createQueryBuilder()
                ->select('a, ea')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->innerJoin('a.externalAdvertisement', 'ea')
                ->where('ea.link = :link')
                ->andWhere('a.status = :status')
                ->setParameter('link', $row['link'])
                ->setParameter('status', Advertisement::STATUS_ACTIVE)
                ->setMaxResults(1)
                ->getQuery();

        $q->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $advertisement = $q->getOneOrNullResult();
        /* @var $advertisement Advertisement */


        if ($advertisement){

            $this->output->writeln('Update advertisement id: ' . $advertisement->getId());

            /**
             * prepare ad data
             */
            $sludData = $this->prepareSingleSludData($row);

            /**
             * Updating external advetisement
             */
                $externalAdvertisement = $advertisement->getExternalAdvertisement();
                /* @var $externalAdvertisement ExternalAdvertisement */

                $externalAdvertisement
                    //->setExternalAdvertisementSource($source)
                    ->setExternalProfession($sludData['profession'])
                    ->setExternalPlace($sludData['externalPlace'])
//                    ->setExternalId(uniqid($row['resource']))
//                    ->setApproved(false)
                    ->setDescription($row['slud'])
                    ->setTitle($sludData['adTitle'])
                ;

                if(!empty($row['org'])) {
                    $externalAdvertisement
                        ->setCompanyName($row['org'])
                    ;
                }

                /*
                $externalAdvertisement
                    ->setAdvertisementType($this->advertisementType)
                ;*/

                $this->em->merge($externalAdvertisement);

                $this->em->flush();
                /**
                 * @test tested by Alex.B. till this line works OK
                 */
            /**
             * Updating advetisement
             */

               $advertisement = $this->er->updateInternalFromExternal($externalAdvertisement, $advertisement);

//            $this->em->merge($advertisement);
//            $this->em->flush();

            //$storedAd = $this->er->postExternalToInternal($adData);

            /**
             * Emails
             */

            if(!empty($row['emails'])) {

                $emails = unserialize($row['emails']);

                $adEmails = $advertisement->getAdvertisementEmails();

                // removing old
                $adEmailsArray = array();
                foreach ($adEmails as $adEmail) {
                    if (! in_array($adEmail->getEmail(), $emails)){
                       $advertisement->removeAdvertisementEmail($adEmail);
                       $this->em->remove($adEmail);
                    }
                    $adEmailsArray[] = $adEmail->getEmail();
                }

                // adding new
                foreach($emails as $email) {

                    if(empty($email)) {
                        continue;
                    }

                    if (! in_array($email, $adEmailsArray)){
                        $adEmail = new AdvertisementEmail();
                        $adEmail
                            ->setAdvertisement($advertisement)
                            ->setEmail($email)
                            ->setCountry($advertisement->getCountry())
                        ;

                        $this->em->persist($adEmail);
                        $this->em->flush($adEmail);
                        $advertisement->addAdvertisementEmail($adEmail);
                    }

                    /*
                    $emailsEntities =
                            $this->em->getRepository('VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail')
                            ->findBy(array('email'=>$email,
                                'advertisement'=> $storedAd));

                    if(empty($emailsEntities)) {

                        $emailAd = new AdvertisementEmail();
                        $emailAd->setAdvertisement($storedAd);
                        $emailAd->setEmail($email);
                        $emailAd->setCountry($storedAd->getCountry());
                        $this->em->persist($emailAd);

                    }

                    $this->em->flush();
                    */
                }

              $this->em->merge($advertisement);
              $this->em->flush($advertisement);
            }
        }

    }


    public function mapData(InputInterface $input, OutputInterface $output) {

        $connection = $this->em->getConnection();
        $limit = self::PROCESS_LIMIT;

        $stop = false;
        $maxIterations = 50;
        $iteration = 0;
        do{
            $iteration++;
            $sql = "SELECT * FROM `slud_data` WHERE `is_mapped` = 0 AND `map_action`='none' ORDER BY `id`  LIMIT {$limit}";
            $stmt = $connection->prepare($sql);
            $stmt->execute();

            if($stmt->rowCount())
            {

                while ($row = $stmt->fetch())
                {
                    $this->mapDataRow($row);
                    $sqlUpdate = "UPDATE `slud_data` SET `is_mapped` = 1, `updated_at` = '".date("Y-m-d H:i:s")."' WHERE `id` = :id";
                    $stmtUpdate = $connection->prepare($sqlUpdate);
                    $stmtUpdate->bindParam(':id', $row['id'], \PDO::PARAM_INT);
                    $stmtUpdate->execute();
                }


            } else {
                $stop = true;
            }


            if($iteration >= $maxIterations) {
                $stop = true;
            }

        } while(!$stop);

    }


    protected function mapDataRow($row) {

        $this->output->writeln('Map advertisement id: ' . $row['id']);

        if(isset($this->sourcesCache[$row['resource']])) {
            $source = $this->sourcesCache[$row['resource']];
        } else {

            $source = $this->getContainer()->get('Doctrine')
                        ->getRepository('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource')
                        ->findOneBy(array('resource' => $row['resource']));

            if(!$source) {
                $source = new ExternalAdvertisementSource();
                $source->setCountry($this->country);
                $source->setResource($row['resource']);
                $source->setIsSocial(false);
                $source->setShowAdvertisiment(true);
                $source->setShowAs($row['resource']);
                $this->em->persist($source);
            }

            $this->sourcesCache[$row['resource']] = $source;
        }

        $sludData = $this->prepareSingleSludData($row);
        /*
        if(!empty($row['loc2'])) {
            $locationImport = $row['loc2'];
        } else {
            $locationImport = $row['loc'];
        }

        $place = $this->er->findExternalPlace($locationImport, $row['resource']);

        if(!$place) {
            $place = new ExternalPlace();
            $place->setName($locationImport);
            $place->setExternalAdvertisementSource($source);
            $this->em->persist($place);
        }




        if(!empty($row['prof2'])) {
            $professionImport = $row['prof2'];
        } else {
            $professionImport = $row['prof'];
        }

        $adTitle = $professionImport;

        if(!empty($row['category_name'])) {
            $professionImport = $row['category_name'];
        }

        $profession = $this->er->findExternalProfession($professionImport, $row['resource']);

        if(!$profession) {
            $profession = new ExternalProfession();
            $profession->setName($professionImport);
            $profession->setExternalAdvertisementSource($source);
            $this->em->persist($profession);
        }
        */

        $adTitle = $sludData['adTitle'];
        $place = $sludData['externalPlace'];
        $profession = $sludData['profession'];

        $adData = new ExternalAdvertisement();
        $adData->setExternalAdvertisementSource($source);
        $adData->setExternalProfession($profession);
        $adData->setExternalPlace($place);
        $adData->setExternalId(uniqid($row['resource']));
        $adData->setApproved(false);
        $adData->setDescription($row['slud']);
        $adData->setTitle($adTitle);

        if(!empty($row['org'])) {
            $adData->setCompanyName($row['org']);
        }

        $adData->setLink($row['link']);
        $adData->setAdvertisementType($this->advertisementType);
        $this->em->persist($adData);

        $this->em->flush();

        //post to regualr AD
        try {
            $storedAd = $this->er->postExternalToInternal($adData);
        } catch (\Exception $e) {
            $this->output->writeln('Can\'t map external to internal AD ' . $adData->getId());
        }

        if(!empty($row['emails'])) {

            $emails = unserialize($row['emails']);

            foreach($emails as $email) {

                if(empty($email)) {
                    continue;
                }
                $emailsEntities =
                        $this->em->getRepository('VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail')
                        ->findBy(array('email'=>$email,
                            'advertisement'=> $storedAd));

                if(empty($emailsEntities)) {

                    $emailAd = new AdvertisementEmail();
                    $emailAd->setAdvertisement($storedAd);
                    $emailAd->setEmail($email);
                    $emailAd->setCountry($storedAd->getCountry());
                    $this->em->persist($emailAd);

                }

                $this->em->flush();
            }

        }
    }

    /**
     *
     * @param array $row
     * @return array
     */
    private function prepareSingleSludData($row) {

        if(isset($this->sourcesCache[$row['resource']])) {
            $source = $this->sourcesCache[$row['resource']];
        } else {

            $source = $this->getContainer()->get('Doctrine')
                        ->getRepository('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource')
                        ->findOneBy(array('resource' => $row['resource']));

            if(!$source) {
                $source = new ExternalAdvertisementSource();
                $source->setCountry($this->country);
                $source->setResource($row['resource']);
                $source->setIsSocial(false);
                $source->setShowAdvertisiment(true);
                $source->setShowAs($row['resource']);
                $this->em->persist($source);
            }

            $this->sourcesCache[$row['resource']] = $source;
        }

        /**
         * location
         */
        if(!empty($row['loc2'])) {
            $locationImport = $row['loc2'];
        } else {
            $locationImport = $row['loc'];
        }

        $place = $this->er->findExternalPlace($locationImport, $row['resource']);

        if(!$place) {
            $place = new ExternalPlace();
            $place->setName($locationImport);
            $place->setExternalAdvertisementSource($source);
            $this->em->persist($place);
        }

        /**
         * profession
         */
        if(!empty($row['prof2'])) {
            $professionImport = $row['prof2'];
        } else {
            $professionImport = $row['prof'];
        }

        $adTitle = $professionImport;

        if(!empty($row['category_name'])) {
            $professionImport = $row['category_name'];
        }

        $profession = $this->er->findExternalProfession($professionImport, $row['resource']);

        if(!$profession) {
            $profession = new ExternalProfession();
            $profession->setName($professionImport);
            $profession->setExternalAdvertisementSource($source);
            $this->em->persist($profession);
        }


        $sludData = array(
            'locationImport' => $locationImport,
            'externalPlace' => $place,
            'professionImport' => $professionImport,
            'profession' => $profession,
            'adTitle' => $adTitle,
        );

        return $sludData;
    }

    public function importData(InputInterface $input, OutputInterface $output) {

        if($input->getOption('parser') == self::PARSER1) {
            $cmd = "php " . __DIR__ . "/../../../../vendor/visi-darbi-parsers/index.php";
        } elseif($input->getOption('parser') == self::PARSER2) {
            $cmd = "php " . __DIR__ . "/../../../../vendor/visi-darbi-parsers/feedindex.php";
        }
        elseif($input->getOption('parser') == self::PARSER3){
             $cmd = "php " . __DIR__ . "/../../../../vendor/visi-darbi-parsers/multifeedindex.php";
        }
        else {
            $output->writeln('Unknown parser option: ' . $input->getOption('parser'));
            return;
        }

        $output->writeln('Run parser: ' . $cmd);

        $commandOutput = shell_exec($cmd);

        $output->writeln('Import script returns: ' . $commandOutput);

    }
}

?>
