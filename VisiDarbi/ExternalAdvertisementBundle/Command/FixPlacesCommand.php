<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\ExternalAdvertisementBundle\Lib\LinkedIn;
use VisiDarbi\ExternalAdvertisementBundle\Lib\OAuthConsumer;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession;
use VisiDarbi\ExternalAdvertisementBundle\Entity\SocialAdvertisementData;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement;
use VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail;

#use VisiDarbi\ExternalAdvertisementBundle\Lib\OAuthConsumer;
/**
 * Command class to clear old (60 days) search keywords
 *
 * @author Aleksey
 */

class FixPlacesCommand extends ContainerAwareCommand {

    const SYS_COUNTRY ='lv';
    const PARSER1 = 'parser1';
    const PARSER2 = 'parser2';
    const PROCESS_LIMIT = 10;
    
    protected $er;
    protected $em;
    protected $input;
    protected $output;
    protected $country;
    protected $source;
    protected $sourcesCache = array();
    protected $advertisementType;
    
    protected function configure() {
        parent::configure();
        $this->setName('visidarbi:external:fix-places')->setDescription('Fix places mapping');        
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $em = $this->getContainer()->get('Doctrine')->getEntityManager();        
        $i = 0;
        $stop = false;

            $query = $em
                    ->createQueryBuilder('a')
                    ->select('a.id, epc.id as city_id, epcc.id as country_id')
//                    ->select('a')
                    ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                    ->join('a.externalAdvertisement', 'ea')
                    ->join('ea.externalPlace', 'ep')
                    ->join('ep.advertisementCity', 'epc')
                    ->join('ep.advertisementCountry', 'epcc')
                    ->where('ep.advertisementCity IS NOT NULL AND a.AdvertisementCity IS NULL')
                    ->setMaxResults(10000);
            ;

            $query = $query->getQuery();
                    ;
            $r = $query->getResult();
            
            $output->writeln('Found AD to fix: '. count($r));

            foreach ($r as $ad) {
                $i++;
                $output->writeln('AD '. $ad['id']. ' set city to - ' . $ad['city_id']);

                $uad = $em->getRepository('VisiDarbiAdvertisementBundle:Advertisement')->findOneBy(array('id' => $ad['id']));

                
                $uad->setAdvertisementCity(
                    $em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity')
                        ->findOneBy(array('id' => $ad['city_id']))
                );

                $uad->setAdvertisementCountry(
                    $em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCountry')
                        ->findOneBy(array('id' => $ad['country_id']))
                );

                $em->merge($uad);
            }    
                $em->flush();

        $output->writeln('Fixed AD`s: '. $i);
        
        
    }
}

?>
