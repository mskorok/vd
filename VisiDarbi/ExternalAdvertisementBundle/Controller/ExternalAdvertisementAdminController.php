<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

/**
 * ExternalAdvertisementAdmin controller
 *
 * @author Aleksey
 */
class ExternalAdvertisementAdminController extends Controller {
    
    /**
        @Route("/approve/{id}", name="approve",
            requirements={"id" = "\d+"}
        )
        @Template("VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:approve.html.twig")
    */
    public function approveAction($id)
    {
        
        $er = $this->getDoctrine()->getRepository('\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement');
        $e = $this->getDoctrine()
                ->getEntityManager()
                ->find('\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement', $id);

        $er->postExternalToInternal($e);
        
        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));

    }
    /**
        @Route("/approve/{id}", name="delete",
            requirements={"id" = "\d+"}
        )
        @Template("VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementAdmin:approve.html.twig")
    */
    public function deleteAction($id)
    {
        $e = $this->getDoctrine()
                ->getEntityManager()
                ->find('\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement', $id);
        
        $e->setRejected(true);
        $e->setApproved(false);        
        $this->getDoctrine()->getEntityManager()->flush($e);
        
        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));

    }}

?>
