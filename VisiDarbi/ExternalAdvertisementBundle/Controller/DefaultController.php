<?php

namespace VisiDarbi\ExternalAdvertisementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('VisiDarbiExternalAdvertisementBundle:Default:index.html.twig', array('name' => $name));
    }
}
