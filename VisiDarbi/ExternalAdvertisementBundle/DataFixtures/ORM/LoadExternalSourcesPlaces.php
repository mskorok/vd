<?php

namespace VisiDarbi\ExternalAdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace;




class LoadExternalSourcesPlaces extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        return;


        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $this->getReference('country-lat');
        $locales = $country->getLocales();
        
        $query = $manager->createQuery('SELECT s FROM VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource s
            WHERE s.country = :country');
        $query->setParameter('country', $country);
        $source = $query->setMaxResults(1)->getResult();
        $source = $source[0];
        
        $query = $manager->createQuery('SELECT ac FROM VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry ac 
            WHERE ac.country = :country');
        $query->setParameter('country', $country);
        $advertisementCountries = $query->getResult();        
        
        $query = $manager->createQuery('SELECT ac FROM VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry c JOIN 
            VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity ac WHERE c.country = :country');
        $query->setParameter('country', $country);
        $advertisementCities = $query->getResult();                        

        
        for ($i = 1; $i <= 40; $i++) {
            
            $externalPlace = new ExternalPlace();
            $externalPlace->setName('External place #'.$i);
            $externalPlace->setExternalAdvertisementSource($source);
            
            if(rand(0, 1)) {
               $city = $advertisementCities[rand(0, count($advertisementCities)-1)];
               $externalPlace->setAdvertisementCity($city);
               $externalPlace->setAdvertisementCountry($city->getAdvertisementCountry());
            } else {
                if(rand(0, 1)) {
                    $adCountry = $advertisementCountries[rand(0, count($advertisementCountries)-1)];
                    $externalPlace->setAdvertisementCountry($adCountry);                  
                }
            }
            
            $manager->persist($externalPlace);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 65;
    }

}
