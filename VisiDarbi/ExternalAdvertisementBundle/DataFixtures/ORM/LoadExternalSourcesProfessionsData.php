<?php

namespace VisiDarbi\ExternalAdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession;

class LoadExternalSourcesProfessionsData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        return;
        
        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $this->getReference('country-lat');
        $locales = $country->getLocales();

        $query = $manager->createQuery('SELECT s FROM VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource s
            WHERE s.country = :country');
        $query->setParameter('country', $country);
        $source = $query->setMaxResults(1)->getResult();
        $source = $source[0];        
        
        $query = $manager->createQuery('SELECT p FROM VisiDarbi\ProfessionBundle\Entity\Profession p JOIN 
            VisiDarbi\ProfessionBundle\Entity\Category c WHERE c.country = :country');
        $query->setParameter('country', $country);
        $professions = $query->getResult();
                

        
        for ($i = 1; $i <= 40; $i++) {
            
            $externalProfession = new ExternalProfession();
            $externalProfession->setName('External profession #'.$i);
            $externalProfession->setExternalAdvertisementSource($source);
            
            if(rand(0, 1)) {
               $externalProfession->setProfession($professions[rand(0, count($professions)-1)]);
            }
            
            $manager->persist($externalProfession);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 66;
    }

}
