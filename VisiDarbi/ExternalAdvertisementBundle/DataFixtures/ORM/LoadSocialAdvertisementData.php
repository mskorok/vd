<?php

namespace VisiDarbi\ExternalAdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\ExternalAdvertisementBundle\Entity\SocialAdvertisementData;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalProfession;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementType;

/**
 * Description of LoadSocialAdvertisementData
 *
 * @author Aleksey
 */
class LoadSocialAdvertisementData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        return; 
        $sourcesCount = 50;

        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $this->getReference('country-lat');
        $locales = $country->getLocales();        
        
        $source = new ExternalAdvertisementSource();
        $source->setCountry($country);
        $source->setResource('http://www.linkedin.com/');
        $source->setIsSocial(false);
        $source->setShowAdvertisiment((bool) rand(0, 1));
        $manager->persist($source);
        
        $adProfession =  new ExternalProfession();
        $adProfession->setName('Profession from linkedin.com');
        $adProfession->setExternalAdvertisementSource($source);
        $manager->persist($adProfession);
        
        $adPlace = new ExternalPlace();
        $adPlace->setName('Place from linkedin.com');
        $adPlace->setExternalAdvertisementSource($source);
        $manager->persist($adPlace);
        
        $advertisementType =  $manager
                ->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType')
                ->findBy(array('id' => AdvertisementType::TYPE_LIMITED_7_DAY));
        
        
        for($i = 0; $i<=10; $i++) {
            
            $adData = new SocialAdvertisementData();
            $adData->setAdvertisementType($advertisementType[0]);
            $adData->setDescription('Social advertisement description #'.$i);
            $adData->setTitle('Social advertisement title #'.$i);
            $manager->persist($adData);
                        
            $externalAd = new ExternalAdvertisement();
            $externalAd->setExternalAdvertisementSource($source);
            $externalAd->setSocialAdvertisementData($adData);
            $externalAd->setCreatedAt(new \DateTime());
            $externalAd->setUpdatedAt(new \DateTime());
            $externalAd->setExternalProfession($adProfession);
            $externalAd->setExternalPlace($adPlace);
            $manager->persist($externalAd);
            
        }
        
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 67;
    }

}
