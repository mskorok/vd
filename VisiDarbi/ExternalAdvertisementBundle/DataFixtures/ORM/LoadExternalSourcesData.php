<?php

namespace VisiDarbi\ExternalAdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource;

class LoadExternalSourcesData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        return;
        
        $sourcesCount = 50;

        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $manager
                ->getRepository('\VisiDarbi\LocalePlaceBundle\Entity\Country')
                ->findOneBy(array('code' => 'lv'));
        $locales = $country->getLocales();

        $source = new ExternalAdvertisementSource();
        $source->setCountry($country);
        $source->setResource('linkedin');
        $source->setIsSocial(true);
        $source->setShowAdvertisiment(true);
        $source->setShowAs('linkedin.com');
        $manager->persist($source);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 60;
    }

}
