<?php

namespace VisiDarbi\AdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation;

/**
 * Rebuild advertisement city slug
 *
 * @author Aleksey
 */
class AdvertisementCitySlugGeneratorCommand extends ContainerAwareCommand {

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi:advertisement:cities-generate-slug')->setDescription('Regenerate Advertisiment city slug`s');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $advertsRepo = $em->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation');

        $result = $advertsRepo->createQueryBuilder('c')
            ->select('c.id')
            ->getQuery()
            ->getResult();

        foreach($result as $translation){

            $translationEntity = $em->find('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation', $translation['id']); 

            $translationEntity->setSlug($translationEntity->getContent());

            $em->flush();
        }

        //outputs in the terminal
        $output->writeln('Advertisement Cities Slugs updated.');
    }

}

?>
