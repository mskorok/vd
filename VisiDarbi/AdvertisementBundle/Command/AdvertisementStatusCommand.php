<?php

namespace VisiDarbi\AdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

/**
 * Command class to clear old (60 days) search keywords
 *
 * @author Aleksey
 */
class AdvertisementStatusCommand extends ContainerAwareCommand {

    protected function configure() {
        parent::configure();
        $this->setName('advertisement:check-status')->setDescription('Check and change advertisement statuses');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $now = new \DateTime();


        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $q = $em->createQueryBuilder()
                ->update('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->set('a.status', Advertisement::STATUS_INACTIVE_CLOSED)
                ->where('a.date_to <= :now_date')
                ->andWhere('a.status = :current_status')
                ->setParameter('now_date', $now)
                ->setParameter('current_status', Advertisement::STATUS_ACTIVE)
                ->getQuery();

        $q->execute();

        //outputs in the terminal
        $output->writeln('Advertisement statuses checked.');
    }

}

?>
