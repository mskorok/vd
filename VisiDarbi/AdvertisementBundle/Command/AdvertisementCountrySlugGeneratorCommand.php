<?php

namespace VisiDarbi\AdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation;

/**
 * Rebuild advertisement countries slug
 *
 * @author Aleksey
 */
class AdvertisementCountrySlugGeneratorCommand extends ContainerAwareCommand {

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi:advertisement:countries-generate-slug')->setDescription('Regenerate Advertisiment countries slug`s');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $advertsRepo = $em->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation');

        $result = $advertsRepo->createQueryBuilder('c')
            ->select('c.id')
            ->getQuery()
            ->getResult();

        foreach($result as $translations){

            $translationEntity = $em->find('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation', $translations['id']); 
            $translationEntity->setSlug(null);
            $em->flush();

        }

        //outputs in the terminal
        $output->writeln('Advertisement Countries Slugs updated.');
    }

}

?>
