<?php

namespace VisiDarbi\AdvertisementBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use VisiDarbi\EmailTemplatingBundle\EmailSender;
use Symfony\Bundle\FrameworkBundle\Controller;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Command class to send advertisement highlighting offers daily
 *
 * get list of this day created adverts which are external
 * get advert user emails and details
 * add advert highlighting links
 * send emails
 *
 *example
 * [2] => array(
 *      'adverts' => array(
 *          '0' => array(
 *              'adv_id' => 22,
 *              'adv_title' => 'ff',
 *              'adv_descr' => 'fdfskjf',
 *              'adv_link' => 'http://kk.kk.kk'
 *          )
 *       ),
 *      'email' = 'kk@kk.kk'
 */
class AdvertisementHighlightOfferCommand extends ContainerAwareCommand {

    /**
     * interval in seconds between mails to send
     */
    const MAIL_SENDING_INTERVAL = 1;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     * @DI\Inject("request")
     */
    protected $request;

    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     * @DI\Inject("visidarbi.country_manager")
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;

    /**
     * @var array of current day adverts
     */
    private $thisDayAdverts;

    /**
     * @var array of current day user adverts
     * user email
     * and advert details
     */
    private $thisDayUserAdverts;

    protected function configure() {
        parent::configure();
        $this->setName('visidarbi:advertisement:send_hl_offers')->setDescription('Send advertisement highlighting offers');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        //highlighting offer commands
        $this->collectThisDaysAdverts();
        $this->collectAdvertisementDetails();
        $this->processBatchHlOfferEmails();

        //outputs in the terminal
        $output->writeln("\nAll advertisement highlighting offers sent!");
    }

    /**
     * collect all of this day's imported adverts
     */
    private function collectThisDaysAdverts() {
        $today = date('Y-m-d');

        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $advertsRepo = $em->getRepository('VisiDarbiAdvertisementBundle:Advertisement');

        $this->thisDayAdverts = $advertsRepo->createQueryBuilder('a')
            ->select()
            ->where('a.cretaed_at LIKE :today')
            ->andWhere('a.is_external IS NOT NULL')
            ->setParameter('today', $today.'%')
            ->getQuery()
            ->getResult()
        ;

    }


    /**
     * get advert and user details
     */
    private function collectAdvertisementDetails() {
        echo "\nCollecting adverts..";

        $this->thisDayUserAdverts = array();
        if (!empty($this->thisDayAdverts)) {
            foreach ($this->thisDayAdverts as $advert) {
                //get advertisement emails
                //for sending hl offers use the first email in the list
                $emails = $advert->getAdvertisementEmails();

                //if user key doesn't exists, create one
                if (!array_key_exists($emails[0]->getEmail(), $this->thisDayUserAdverts)) {
                    $ua = array(
                        'email' => $emails[0]->getEmail(),
                        'adverts' => array()
                    );

                    $this->thisDayUserAdverts[$emails[0]->getEmail()] = $ua;
                }


                $currentCountry = $this->getContainer()->get('visidarbi.country_manager')->getCurrentCountry();
                $baseUrl = $currentCountry->getUri();
                $link = $baseUrl . $this->getContainer()->get('router')->generate('visidarbi_advertisement_highlight') .'/'. $advert->getSlugOrPublicId($currentCountry->getDefaultLocale());

                //get advert details
                $a = array();
                $a['adv_id'] = $advert->getId();
                $a['adv_title'] = $advert->getTitle();
                $a['adv_descr'] = $advert->getDescription();
                $a['adv_link'] = $link;

                //add advert details to user array
                $this->thisDayUserAdverts[$emails[0]->getEmail()]['adverts'][$advert->getId()] = $a;
            }
        }
    }


    /**
     * process collected emails,
     * generate linksList and pass
     * to sender
     */
    private function processBatchHlOfferEmails() {
        echo "\nProcessing batch emails..";

        //get a PDO instance connection
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();
        $this->translator = $this->getContainer()->get('translator');

        if (!empty($this->thisDayUserAdverts)) {
            foreach($this->thisDayUserAdverts as $ua) {

                $linkList = "";
                foreach($ua['adverts'] as $adv) {
                    $linkList .=
                        '<div>
                            <p>'.$this->translator->trans('Sludinājuma virsraksts', array(), 'highlighting_offer').': '.$adv['adv_title'].'</p>'
                            .'<p>'.$this->translator->trans('Sludinājuma ievadteksts', array(), 'highlighting_offer').': '.$adv['adv_descr'].'</p>'
                            .'<p> <a href="'.$adv['adv_link'].'">'.$adv['adv_link'].'</a></p>
                        </div>';
                }

                //send
                $this->sendEMailMessages($ua['email'], 'advHlLink', $linkList, EmailTemplate::ID_HIGHLIGHTING_OFFER, false);

                //set wait interval to 1 second to not breach mail sending limit
                sleep(self::MAIL_SENDING_INTERVAL);
            }
        }
    }

    /**
     * @param $email
     * @return JsonResponse
     */
    private function sendEMailMessages($email, $placeHolder, $data, $template, $showUnSubscribeLink)
    {

        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $this->emailSender = $this->getContainer()->get('visidarbi.email_templating.sender');
        $this->countryManager = $this->getContainer()->get('visidarbi.country_manager');
        $this->settingsManager = $this->getContainer()->get('visidarbi.settings.manager');
        $this->translator = $this->getContainer()->get('translator');

        if (empty($email)) {
            return $this->jsonResponse(
                'FAIL',
                $this->translator->trans('E-mail address not specified', array(), 'highlighting_offer')
            );
        }

        $user = $em->getRepository('VisiDarbiUserBundle:User')->findOneBy(array(
            'email' => $email
        ));

        if ($user === null) {
            return $this->jsonResponse(
                'FAIL',
                $this->translator->trans('User account with such e-mail not found', array(), 'highlighting_offer')
            );
        }

        if (! $user->isEnabled()) {
            return false;
        }

        if ($user->isLocked()) {
            return false;
        }

        $locale = $user->getCountry()->getDefaultLocale();

        // unSubscription link
        if ($showUnSubscribeLink) {
            $subscription = $em->getRepository('VisiDarbiUserBundle:EmailSubscriptionSettings')->findOneBy(array(
                'user' => $user->getId()
            ));

            if ($subscription) {
                $currentCountry = $this->getContainer()->get('visidarbi.country_manager')->getCurrentCountry();
                $baseUrl = $currentCountry->getUri();
                $link = $baseUrl . $this->getContainer()->get('router')->generate('unsubscribe_from_news', array('subscriptionId' => $subscription->getSubscriptionId()));

                $data .= '<div>
                    <p> <a href="'.$link.'">Unsubscribe</a></p>
                </div>';
            }
        }

        $this->emailSender->send(
            $this->countryManager->getCurrentCountry(),
            $locale,
            $template,
            $user->getEmail(),
            array(
                $placeHolder => $data
            ),
            array(
                'email' => $this->settingsManager->get('emails.sender_email'),
                'name' => $this->settingsManager->get('emails.sender_name')
            )
        );

        return true;
    }

}

?>
