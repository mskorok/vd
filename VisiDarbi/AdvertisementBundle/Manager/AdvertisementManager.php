<?php

namespace VisiDarbi\AdvertisementBundle\Manager;

use Doctrine\ORM\EntityManager;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation;
use Symfony\Component\HttpFoundation\File\File as FileObject;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation;
use VisiDarbi\AdvertisementBundle\Entity\DutyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\OfferTranslation;
use VisiDarbi\AdvertisementBundle\Entity\RequirementTranslation;
use VisiDarbi\StatisticsBundle\Entity\Filter;

use VisiDarbi\StatisticsBundle\Entity\TextFilter;

use Gedmo\Sluggable\Util\Urlizer;

class AdvertisementManager implements AdvertisementManagerInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /**
     * @var CountryManagerInterface
     */
    protected $countryManager;

    public function __construct(EntityManager $em, CountryManagerInterface $countryManager) {
        $this->em = $em;
        $this->countryManager = $countryManager;
    }

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
        $this->container = $container;
    }

    public function getFileFormatSize($format) {

        $uploadConfig = $this->getUploadConfig($format);
        return !empty($uploadConfig['max_size_view']) ? $uploadConfig['max_size_view'] : 0;
    }

    public function getUploadConfig($format) {

        $default = array(
            'tmp_path' => 'uploads/tmp',
            'stored_path' => '',
            'cache_path' => 'cache',
            'allow_type' => '',
            'max_size' => 0,
            'max_size_view' => ''
        );

        if (empty($format)) {
            return $default;
        }
        if (!in_array($format, array(Advertisement::FORMAT_IMAGE_PDF, Advertisement::FORMAT_HTML, 'html_image', 'upload_logo'))) {
            return $default;
        }
        if ($format == 'upload_logo') {
            $params = $this->container->getParameter($format);
        } else {
            $params = $this->container->getParameter('upload_'.strtolower($format).'_form');
        }
        if (isset($params['max_size'])) {
            $params['max_size_view'] = $params['max_size'] / 1024 / 1024;
            if ((int)$params['max_size_view'] !== $params['max_size_view']) {
                $params['max_size_view'] = round($params['max_size_view'], 2);
            }
        } else {
            $params['max_size_view'] = '';
        }

        return $params;
    }


    public function getTranslitirateFileName($file_name) {
        if (empty($file_name)) {
            return $file_name;
        }
        $data = pathinfo($file_name);

        if (empty($data)) {
            return $file_name;
        }
        $file_name = substr(Urlizer::transliterate($data['filename']), 0, 64).(!empty($data['extension']) ? '.'.$data['extension'] : '');

        return $file_name;

    }

    public function setFileFormat(Advertisement $advertisement, $data, $flush = true) {
        $format = $advertisement->getFormat();
        $uploadConfig = $this->getUploadConfig($format);
        if (empty($uploadConfig['stored_path'])) {
            return;
        }

        $advTranslations = $advertisement->getTranslations();
        $old_data = array();
        $translationIds = array();
        foreach ($advTranslations as $t) {
            if ($t->getField() == 'file_format') {
                $old_data[$t->getLocale()] = $t->getContent();
                $id = $t->getId();
                if (!empty($id)) {
                    $translationIds[$t->getLocale()] = $id;
                }

            }
        }

        $htmlImages = array();
        if ($advertisement->getFormat() == Advertisement::FORMAT_HTML) {
            $htmlImages = $this->saveHtmlImageFiles($advertisement, $data, $flush);
        }

        $locales = $this->getLocales();
        $files = array();
        foreach ($locales as $locale) {
            $files['file_format_'.$locale] = '';

            $stored_dir = $this->getFileFormatDir($advertisement, $locale);
            if (!empty($data[$locale]['file_format'])) {
                $file_data = $data[$locale]['file_format'];
                $this->container->get('logger')->addInfo('AdvertisementManager.setFileFormat file format data $file_data="'.json_encode($file_data).'", $stored_dir="'.$stored_dir.'"');
                if (!empty($file_data['fileNameOrig']) && !empty($file_data['fileTmpPath']) && file_exists($file_data['fileTmpPath'])) {
                    if ($advertisement->getFormat() == Advertisement::FORMAT_HTML) {
                        $this->htmlFormatHandler($locale, $file_data['fileTmpPath'], $htmlImages);
                    }
                    if (!empty($stored_dir) && is_dir($stored_dir)) {
                        if (rename($file_data['fileTmpPath'], $stored_dir . '/' . $file_data['fileNameOrig'])) {
                            $files['file_format_'.$locale] = $file_data['fileNameOrig'];
                        } else {
                            $this->container->get('logger')->addError('AdvertisementManager.setFileFormat rename failed, rename('.$file_data['fileTmpPath'], $stored_dir . '/' . $file_data['fileNameOrig'].')');
                        }

                    } else {
                        $this->container->get('logger')->addError('AdvertisementManager.setFileFormat empty stored_dir, $stored_dir="'.$stored_dir.'", is_dir($stored_dir)="'.is_dir($stored_dir)."'");
                    }
                    if (!empty($file_data['thumb'])) {
                        $path = $this->container->getParameter('kernel.root_dir') . '/../web/' . $file_data['thumb'];
                        if (is_file($path)) {
                            unlink($path);
                        }
                    }
                }
            }
            if (!empty($old_data[$locale]) && $old_data[$locale] != $files['file_format_'.$locale] && is_file($stored_dir . '/' .$old_data[$locale])) {
                unlink($stored_dir . '/' .$old_data[$locale]);
            }



        }

        if (!empty($files)) {
            if (!empty($translationIds)) {
                $qb = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementTranslation')->createQueryBuilder('t');
                $qb->delete();
                $qb->add('where', $qb->expr()->in('t.id', $translationIds));
                $qb->getQuery()
                    ->execute();
            }
            $this->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'file_format', $files, 'setFileFormat');
            $this->em->persist($advertisement);
        }

    }

    public function checkFileFormat($file_format, $data) {
        if (empty($file_format) || !is_array($file_format)) {
            return $file_format;
        }
        $new_file_format = array();
        foreach ($file_format as $locale=>$file_data) {
            if (!empty($data['file_format_'.$locale]) && !empty($file_data['file_format'])) {
                $new_file_format[$locale]['file_format'] = $file_data['file_format'];
            } else if(!empty($file_data['file_format']['fileTmpPath']) && is_file($file_data['file_format']['fileTmpPath'])) {
                unlink($file_data['file_format']['fileTmpPath']);
                if (!empty($file_data['file_format']['thumb'])) {
                    $path = $this->container->getParameter('kernel.root_dir') . '/../web/' . $file_data['file_format']['thumb'];
                    if (is_file($path)) {
                        unlink($path);
                    }
                }
            }
            if (!empty($file_data['html_image']) && is_array($file_data['html_image'])) {
                foreach ($file_data['html_image'] as $index => $row) {
                    if (!empty($data['html_image_'.$locale][$index])) {
                        $new_file_format[$locale]['html_image'][$index] = $row;
                    } elseif(!empty($row['fileTmpPath']) && is_file($row['fileTmpPath'])) {
                        unlink($row['fileTmpPath']);
                        if (!empty($row['thumb'])) {
                            $path = $this->container->getParameter('kernel.root_dir') . '/../web/' . $row['thumb'];
                            if (is_file($path)) {
                                unlink($path);
                            }
                        }
                    }
                }
            }
        }
        return $new_file_format;

    }

    public function getFormFormatFileData($format, $format_files, $locale) {
        $data = array(
            'result' => array(
                'errors' => '',
                'fileNameOrig' => ''
            )
        );
        if (!empty($format_files[$locale]) && !empty($format_files[$locale]['file_format']) && empty($format_files[$locale]['file_format']['errors'])) {
            $data['result'] = $format_files[$locale]['file_format'];
        }

        return json_encode($data);
    }

    public function generateFileFormatData(Advertisement $advertisement) {
        $format = $advertisement->getFormat();
        if (!in_array($format, array(Advertisement::FORMAT_IMAGE_PDF, Advertisement::FORMAT_HTML))) {
            return null;
        }
        $uploadConfig = $this->getUploadConfig($format);
        if (empty($uploadConfig['stored_path'])) {
            return null;
        }
        $formatFiles = array();
        $advTranslations = $advertisement->getTranslations();
        $tmpDir = $this->container->getParameter('kernel.root_dir') . '/../web/' . $uploadConfig['tmp_path'].'/';
        foreach ($advTranslations as $t) {
            if ($t->getField() == 'file_format') {
                $content = $t->getContent();
                if (!empty($content)) {
                    $fileInfo = $this->getFileFormatInfo($advertisement, $t->getLocale(), $content, false);
                    if (!empty($fileInfo['path'])) {
                        $fileName = md5(uniqid('',true)).(!empty($fileInfo['extension']) ? '.' : '' ) .$fileInfo['extension'];
                        $tmpPath = $tmpDir . $fileName;
                        if (copy($fileInfo['path'], $tmpPath)) {
                            $formatFiles[$t->getLocale()]['file_format'] = array(
                                'errors' => null,
                                'fileNameOrig' => $content,
                                'type' => $fileInfo['type'],
                                'fileName' => $fileName,
                                'fileTmpPath' => $tmpPath,
                                'locale' => $t->getLocale(),
                                'thumb' => ''
                            );
                            if ($fileInfo['type'] == 'image') {
                                $fileNameTemp = str_replace('.', '_file_format.', $fileName);
                                $formatFiles[$t->getLocale()]['file_format']['thumb'] = $this->createFileThumb($tmpDir,$fileName,$fileNameTemp, $uploadConfig);
                            }
                        }
                    }
                }

            }

        }
        return $formatFiles;
    }

    public function createFileThumb($dir, $fileName, $fileNameTemp, $uploadConfig, $web_path = null) {
        $uploadedResizedImg = $dir . $fileNameTemp;
        $img = $this->container->get('image.handling')->open($dir  . $fileName);
        $imgType = $img->guessType();
        $img->resize($uploadConfig['thumb']['width'], $uploadConfig['thumb']['height'])->save($uploadedResizedImg, $imgType);
        if (empty($web_path)) {
            $web_path = $uploadConfig['tmp_path'];
        }
        return $web_path.'/' .$fileNameTemp;
    }

    public function getFileFormatWebDir(Advertisement $advertisement, $locale, $format = null, $is_preview = false) {
        if (empty($format)) {
            $format = $advertisement->getFormat();
        }
        $uploadConfig = $this->getUploadConfig($format);

        if (empty($uploadConfig['stored_path'])) {
            $this->container->get('logger')->addError('AdvertisementManager.getFileFormatWebDir - empty stored_path, $format="'.$format.'", $uploadConfig="'.json_encode($uploadConfig).'"');
            return '';
        }
        $publicId = $advertisement->getPublicId();

        if (empty($publicId) || empty($locale)) {
            $this->container->get('logger')->addError('AdvertisementManager.getFileFormatWebDir - empty data, $publicId="'.$publicId.'", $locale="'.$locale.'"');
            return '';
        }

        $advertisementHash = $publicId;
        $storedPath = $uploadConfig['stored_path'];

        if ($is_preview){
           $storedPath = $uploadConfig['stored_preview_path'];
        }

        $dir = str_replace(array('{advertisement_hash}', '{locale}'), array($advertisementHash, $locale), $storedPath);

        return $dir;
    }

    public function getFileFormatDir(Advertisement $advertisement, $locale, $format = null, $is_preview = false) {
        $dir = $this->getFileFormatWebDir($advertisement, $locale, $format, $is_preview);

        if (empty($dir)) {
            $this->container->get('logger')->addError('AdvertisementManager.getFileFormatDir empty dir');
            return '';
        }
        $rootDir =  $this->container->getParameter('kernel.root_dir') . '/../web/';
        $dir = $rootDir.$dir;
        if (!is_dir($dir)) {
            if (!$this->_createDirs($rootDir, $dir)) {
                $this->container->get('logger')->addError('AdvertisementManager.getFileFormatDir can\'t create dir $dir="'.$dir.'", $rootDir="'.$rootDir.'", is_dir($rootDir)="'.is_dir($rootDir).'"');
            }
        }

        return is_dir($dir) ? $dir : '';
    }

    public function setHtmlImagesData(Advertisement $advertisement, &$data, $formatFiles, $copy = false) {

        if (!is_array($formatFiles)) {
            $formatFiles = array();
        }
        $uploadConfig = $this->getUploadConfig('html_image');
        if (empty($uploadConfig['stored_path'])) {
            return $formatFiles;
        }

        $items = $advertisement->getAdvertisementHtmlImages();
        $tmpDir = $this->container->getParameter('kernel.root_dir') . '/../web/' . $uploadConfig['tmp_path'].'/';

        if (!empty($items)) {
            foreach ($items as $item) {
                $itemTranslations = $item->getTranslations();
                foreach ($itemTranslations as $t) {
                    $locale = $t->getLocale();
                    if (empty($data["html_image_".$locale])) {
                        $data["html_image_".$locale] = array();
                    }
                    if (empty($ind[$locale])) {
                        $ind[$locale] = 0;
                    }
                    $data["html_image_".$locale][$ind[$locale]] = $t->getContent();
                    $content = $t->getContent();
                    if (!empty($content)) {
                        $fileInfo = $this->getFileFormatInfo($advertisement, $locale, $content, false, 'html_image');
                        if (!empty($fileInfo['path'])) {
                            if ($copy) {
                                $fileName = md5(uniqid('',true)).(!empty($fileInfo['extension']) ? '.' : '' ) .$fileInfo['extension'];
                                $dir = $tmpDir;
                                $webDir = $uploadConfig['tmp_path'];
                                if (!copy($fileInfo['path'], $tmpDir . $fileName)) {
                                    $fileName = '';
                                }

                            } else {
                                $dir = $this->getFileFormatDir($advertisement, $locale, 'html_image');
                                $webDir = $this->getFileFormatWebDir($advertisement, $locale, 'html_image');
                                $fileName = $content;
                            }

                            if (!empty($fileName)) {
                                $fileThumbName = str_replace('.', '_thumb.', $fileName);
                                $formatFiles[$locale]['html_image'][$ind[$locale]] = array(
                                    'errors' => null,
                                    'fileNameOrig' => $content,
                                    'type' => 'image',
                                    'fileName' => $fileName,
                                    'fileTmpPath' => $dir.'/'.$fileName,
                                    'locale' => $locale,
                                    'thumb' => (!is_file($dir.'/'.$fileThumbName)) ? $this->createFileThumb($dir.'/',$fileName,$fileThumbName, $uploadConfig, $webDir) : $webDir.'/'.$fileThumbName
                                );
                            }
                        }

                    }
                    $ind[$locale]++;

                }
            }
        }

        return $formatFiles;
    }

    public function getContentForLocale(Advertisement $advertisement, $locale) {

        $defValue = null;

        foreach ($advertisement->getTranslations() as $t) {
            if ($t->getField() == 'content') {
                if ($t->getLocale() == $this->countryManager->getCurrentCountry()->getDefaultLocale()) {
                    $defValue = $t->getContent();
                }
                if ($locale == $t->getLocale()) {
                    $content = $t->getContent();
                }
            }
        }
        if (empty($content)) {
            $content = $defValue;
        }
        return $content;
    }

    public function getFileFormatInfo(Advertisement $advertisement, $locale, $file_format = null, $use_default_locale = true, $format = null, $is_preview = false) {
        $attr = array(
            'type' => '',
            'path' => '',
            'web_path' => '',
            'extension' => ''
        );

        if (!isset($file_format)) {

            $defValue = null;
            foreach ($advertisement->getTranslations() as $t) {
                if ($t->getField() == 'file_format') {
                    if ($t->getLocale() == $this->countryManager->getCurrentCountry()->getDefaultLocale()) {
                        $defValue = $t->getContent();
                    }
                    if ($locale == $t->getLocale()) {
                        $file_format = $t->getContent();
                    }
                }
            }
            if (empty($file_format) && $use_default_locale) {
                $file_format = $defValue;
            }
        }


        if (empty($file_format)) {
            return $attr;
        }
        $dir = $this->getFileFormatDir($advertisement, $locale, $format, $is_preview);

        if (empty($dir) || !is_dir($dir)) {
            return $attr;
        }
        $path = $dir.'/'.$file_format;
        if (!file_exists($path) && $use_default_locale) {
            $locale = $this->countryManager->getCurrentCountry()->getDefaultLocale();
            $dir = $this->getFileFormatDir($advertisement, $locale, null, $is_preview);
            $path = $dir.'/'.$file_format;
        }
        if (file_exists($path)) {
            $file = new FileObject($path);
            $mimeType = $file->getMimeType();
            if ($mimeType == 'application/pdf') {
                $attr['type'] = 'pdf';
            } else if ($mimeType == 'text/html') {
                $attr['type'] = 'html';
            } else if (preg_match('/image\/.+/', $mimeType)) {
                $attr['type'] = 'image';
            }
            $attr['web_path'] = '/'.$this->getFileFormatWebDir($advertisement, $locale, $format, $is_preview).'/'.$file_format;
            $attr['path'] = $path;
            $attr['extension'] = $file->getExtension();
        }

        return $attr;
    }

    public function mergeFilesData($format_files, $param_name, $result) {
        $locales = $this->getLocales();

        $locale = '';
        $field = '';
        if (preg_match("/(.+)_(".implode('|', $locales).")_files/", $param_name, $m)) {
            $locale = $m[2];
            $field = $m[1];
        }
        if (empty($locale) || empty($field)) {
            return $format_files;
        }
        if (empty($format_files[$locale])) {
            $format_files[$locale] = array();
        }
        if (empty($format_files[$locale][$field])) {
            $format_files[$locale][$field] = array();
        }
        if ($field == 'html_image') {
            $format_files[$locale][$field][] = $result;
        } else {
            $format_files[$locale][$field] = $result;
        }
        return $format_files;
    }

    protected function getLocales() {
        return $this->countryManager
            ->getCurrentCountry()
            ->getLocales();
    }

    public function populateObjectTranslations($object, $translationClass, $baseFieldName, $dataArray, $customMethod = null) {

        $defaultLocale = $this->countryManager->getCurrentCountry()->getDefaultLocale();
        $locales = $this->getLocales();

        if (!$customMethod) {
            $methodName = 'set' . ucfirst($baseFieldName);
        } else {
            $methodName = $customMethod;
        }

        foreach ($dataArray as $k => $v) {
            foreach ($locales as $locale) {
                if ($k == $baseFieldName . '_' . $locale) {
                    if ($locale == $defaultLocale) {
                        $defaulyValue = $v;
                        call_user_func_array(array($object, $methodName), array($v));
                    }

                    if (empty($v)) {
                        $v = $defaulyValue;
                    }
                    $object->addTranslation(new $translationClass($locale, $baseFieldName, $v));
                }
            }
        }
    }


    public function encodeFilter(array $filterData){

        $decoded_filter = base64_encode(serialize($filterData));
        $filter_key = md5($decoded_filter);

        $filterExists = $this->getDecodedFilter($filter_key);
        if(!$filterExists){

            $filter = new Filter();
            $filter->setFilterKey($filter_key);
            $filter->setFilterValue($decoded_filter);

            $this->em->persist($filter);
            $this->em->flush();
        }

        return array(
            'value' => $decoded_filter,
            'key' => $filter_key
        );
    }

    public function getDecodedFilter($filterKey, $type=false){
        /** @var Filter $filterExists */
        $filterExists = $this->em->getRepository('VisiDarbi\StatisticsBundle\Entity\Filter')->findOneBy(array('filter_key' => $filterKey));

        if($filterExists){
            $filterResource = $filterExists->getFilterValue();
            $filterValue = unserialize(base64_decode(stream_get_contents($filterResource)));
            return $filterValue;
        }

        return false;
    }


    /**
     * Get Default Slug value
     * @access    public
     * @author    Aleksejs G
     * @param     string $slug  slug value
     * @return    string        What or Where original value based on user locale
     */
    public function getSlugOriginalValue($slug, $type, $locale){

        $data = array(
            'value' => false,
            'slug' => false
        );

        if($type == 'what'){
            $company = $this->em->getRepository('VisiDarbi\AdvertisementBundle\Entity\Company')
                            ->findOneBy(array('slug' => $slug));

            if($company){
                $data['value'] = $company->getName();
                $data['slug'] = $company->getSlug();
                return $data;
            }

            $profession = $this->em->getRepository('VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation')
                            ->findOneBy(
                                array(
                                    'slug' => $slug,
                                    'locale' => $locale,
                                    'field' => 'name',
                                )
                            );

            if($profession){
                $data['value'] = $profession->getContent();
                $data['slug'] = $profession->getSlug();
                return $data;
            }

        }

        if($type == 'where'){
            $city = $this->em->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation')
                        ->findOneBy(
                            array(
                                'locale' => $locale,
                                'field' => 'name',
                                'slug' => $slug
                            )
                        );

            if($city){
                $data['value'] = $city->getContent();
                $data['slug'] = $city->getSlug();
                return $data;
            }

            $advCountry = $this->em->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation')
                        ->findOneBy(
                            array(
                                'locale' => $locale,
                                'field' => 'name',
                                'slug' => $slug
                            )
                        );

            if($advCountry){
                $data['value'] = $advCountry->getContent();
                $data['slug'] = $advCountry->getSlug();
                return $data;
            }
        }

        return false;
    }


    public function processTextSearchSlug($type, $slug, $locale, $default_values){

        $result = array(
            'value' => false,
            'slug' => false
        );

        if($type == 'what'){
            $predefined = $this->getSlugOriginalValue($slug, 'what', $locale );
        }
        elseif($type == 'where'){
            $predefined = $this->getSlugOriginalValue($slug, 'where', $locale );
        }
        else{
            return $result;
        }

        if($predefined){
            $result['value'] = $predefined['value'];
            $result['slug'] = $predefined['slug'];
        }
        else{

            $checkSlug = $this->em->getRepository('VisiDarbi\StatisticsBundle\Entity\TextFilter')
                            ->findOneBy(
                                array(
                                    'slug' => $slug,
                                    'locale' => $locale,
                                    'type' => $type
                                )
                            );

            if($checkSlug){
                $result['value'] = $checkSlug->getValue();
                $result['slug'] = $checkSlug->getSlug();
            }
            elseif( isset($default_values['value']) && !empty($default_values['value']) ){

                $result['value'] = $default_values['value'];
                $result['slug'] = $default_values['slug'];

                $newTextFilter = new TextFilter();
                $newTextFilter->setSlug($result['slug']);
                $newTextFilter->setValue($result['value']);
                $newTextFilter->setLocale($locale);

                if($type == 'what')
                $newTextFilter->setType('what');

                if($type == 'where')
                    $newTextFilter->setType('where');

                $this->em->persist($newTextFilter);
                $this->em->flush();

            }
            else if($slug && !empty($slug)){

                $newTextFilter = new TextFilter();
                $newTextFilter->setValue($slug);
                $newTextFilter->setLocale($locale);

                if($type == 'what')
                $newTextFilter->setType('what');

                if($type == 'where')
                    $newTextFilter->setType('where');

                $this->em->persist($newTextFilter);
                $this->em->flush();

                $result['value'] = $newTextFilter->getValue();
                $result['slug'] = $newTextFilter->getSlug();
            }

        }


        return $result;

    }

    private function saveHtmlImageFiles(Advertisement $advertisement, $data, $flush = true) {
        $locales = $this->getLocales();
        $items = array();
        $result = array();

        $dutRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementHtmlImage');
        $qb = $dutRepo->createQueryBuilder('t');
        $qb->delete()
            ->where('t.advertisement = :adv_id')
            ->setParameter('adv_id', $advertisement->getId())
            ->getQuery()
            ->execute();

        foreach ($locales as $locale) {
            $stored_dir = $this->getFileFormatDir($advertisement, $locale, 'html_image');

            $web_dir = '/'.$this->getFileFormatWebDir($advertisement, $locale, 'html_image').'/';
            if (!empty($data[$locale]['html_image']) && is_array($data[$locale]['html_image'])) {
                $k = 0;
                foreach ($data[$locale]['html_image'] as $file_data) {
                    $this->container->get('logger')->addInfo('AdvertisementManager.saveHtmlImageFiles html image data $file_data="'.json_encode($file_data)."'");
                    if (!empty($file_data['fileNameOrig']) && !empty($file_data['fileTmpPath']) && file_exists($file_data['fileTmpPath'])) {
                        if (!empty($stored_dir) && is_dir($stored_dir)) {
                            if (rename($file_data['fileTmpPath'], $stored_dir . '/' . $file_data['fileNameOrig'])) {
                                $items[$k]['image_'.$locale] = $file_data['fileNameOrig'];
                                $result[$locale][$file_data['fileNameOrig']] = $web_dir.$file_data['fileNameOrig'];
                                $k++;
                            } else {
                                $this->container->get('logger')->addError('AdvertisementManager.saveHtmlImageFiles rename failed, rename('.$file_data['fileTmpPath'].', '.$stored_dir . '/' . $file_data['fileNameOrig'].')');
                            }
                        } else {
                            $this->container->get('logger')->addError('AdvertisementManager.saveHtmlImageFiles empty stored_dir, $stored_dir="'.$stored_dir.'", is_dir($stored_dir)="'.is_dir($stored_dir)."'");
                        }
                        if (!empty($file_data['thumb'])) {
                            $path = $this->container->getParameter('kernel.root_dir') . '/../web/' . $file_data['thumb'];
                            if (is_file($path)) {
                                unlink($path);
                            }
                        }
                    }
                }
            }
        }

        foreach ($items as $itemData) {

            $item = new AdvertisementHtmlImage();
            $this->populateObjectTranslations($item, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation', 'image', $itemData);
            $item->setAdvertisement($advertisement);
            $this->em->persist($item);
            if ($flush) {
                $this->em->flush();
            }

        }
        return $result;
    }

    private function htmlFormatHandler($locale, $html_file, $html_images) {
        if (!is_file($html_file)) {
            return;
        }
        $dirty_html = file_get_contents($html_file);
        $fp=fopen($html_file,"w");
        if (!$fp || empty($dirty_html)) {
            return;
        }

        $clean_html = preg_replace('~<script[^>]*?>.*?</script>~si', '', $dirty_html);

        if (!empty($html_images[$locale]) && is_array($html_images[$locale])) {
            foreach ($html_images[$locale] as $name => $web_path) {
                $clean_html = preg_replace('~<img([^>]+)src=[\'"]('.$name.')[\'"]([^>]*?)>~si', '<img\1 src="'.$web_path.'"\3>', $clean_html);
            }
        }
        fwrite($fp,$clean_html);

        fclose($fp);
    }

    private function _createDirs($rootDir, $dir, $mode = 0777) {
        if (empty($rootDir) || empty($dir)) {
            $this->container->get('logger')->addError('AdvertisementManager._createDirs('.$rootDir.', '.$dir.', '.$mode.') empty params');
            return false;
        }
        if (is_dir($dir)) {
            return true;
        }
        if (!is_dir($rootDir)) {
            $this->container->get('logger')->addError('AdvertisementManager._createDirs('.$rootDir.', '.$dir.', '.$mode.') is_dir($rootDir)="'.is_dir($rootDir)."'");
            return false;
        }
        $dirs = explode(DIRECTORY_SEPARATOR, str_replace($rootDir, '', $dir));
        $path = $rootDir;
        foreach ($dirs as $subdir) {
            if (!empty($subdir)) {
                $path .= $subdir . DIRECTORY_SEPARATOR;
                if(!is_dir($path)) {
                    if(!mkdir($path, $mode)) {
                        $this->container->get('logger')->addError('AdvertisementManager._createDirs('.$rootDir.', '.$dir.', '.$mode.') cant create dir $path="'.$path."'");
                        return false;
                    }
                }
            }

        }
        return true;
    }


}