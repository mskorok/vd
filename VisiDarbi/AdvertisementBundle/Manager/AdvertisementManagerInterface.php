<?php

namespace VisiDarbi\AdvertisementBundle\Manager;

use VisiDarbi\AdvertisementBundle\Entity\Advertisement;


interface AdvertisementManagerInterface
{
    public function getFileFormatSize($format);

    public function getUploadConfig($format);

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container);

    public function setFileFormat(Advertisement $advertisement, $data, $flush = true);

    public function checkFileFormat($file_format, $data);

    public function getFormFormatFileData($format, $format_files, $locale);

    public function generateFileFormatData(Advertisement $advertisement);

    public function getFileFormatWebDir(Advertisement $advertisement, $locale, $format = null);

    public function createFileThumb($dir, $fileName, $fileNameTemp, $uploadConfig, $web_path = null);

    public function getFileFormatDir(Advertisement $advertisement, $locale, $format = null);

    public function setHtmlImagesData(Advertisement $advertisement, &$data, $formatFiles);

    public function mergeFilesData($format_files, $param_name, $result);

    public function getContentForLocale(Advertisement $advertisement, $locale);

    public function getFileFormatInfo(Advertisement $advertisement, $locale, $file_format = null, $use_default_locale = true, $format = null);

    public function populateObjectTranslations($object, $translationClass, $baseFieldName, $dataArray, $customMethod = null);

}
