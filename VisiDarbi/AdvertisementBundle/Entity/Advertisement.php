<?php

namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @ORM\Table(
 *      indexes={
 *          @ORM\Index(name="publicId_idx", columns={"publicId"})
 *      }
 * )
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Advertisement {

    const STATUS_PENDING = 10;
    const STATUS_ACTIVE = 20;
    const STATUS_INACTIVE = 30;
    const STATUS_INACTIVE_CLOSED = 40;
    const STATUS_DELETED = 50;

    const FORMAT_FULL = 'Full';
    const FORMAT_HTML = 'Html';
    const FORMAT_IMAGE_PDF = 'ImagePdf';
    const FORMAT_FREE = 'Free';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $publicId;


    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $additional_description;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_from;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $date_to;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $status;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $cretaed_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $published_at;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updated_at;

    /**
     * @ORM\OneToOne(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement",
     *     inversedBy="advertisements"
     * )
     * @ORM\JoinColumn(name="external_advertisement_id", referencedColumnName="id", unique=true)
     */
    private $externalAdvertisement;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\ContactPerson", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $contactPerson;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Company", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $companies;

    /**
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail",
     *     mappedBy="advertisement"
     * )
     */
    private $advertisementEmails;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Requisites", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $requisites;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Duty", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $dutieses;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Requirement", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $requirementses;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Offer", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction", mappedBy="advertisement")
     */
    private $paymentTransactions;



    /**
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementType",
     *     inversedBy="advertisements"
     * )
     * @ORM\JoinColumn(name="advertisement_type_id", referencedColumnName="id", nullable=false)
     */
    private $advertisementType;

    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Category", inversedBy="Advertisements")
     * @ORM\JoinTable(
     *     name="CategoryToAdvertisement",
     *     joinColumns={@ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)}
     * )
     */
    private $Categories;

    /**
     * @ORM\ManyToMany(targetEntity="VisiDarbi\ProfessionBundle\Entity\Profession", inversedBy="Advertisements")
     * @ORM\JoinTable(
     *     name="ProfessionToAdvertisement",
     *     joinColumns={@ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="profession_id", referencedColumnName="id", nullable=false)}
     * )
     */
    private $Professions;

    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\UserBundle\Entity\User", inversedBy="advertisements")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", inversedBy="advetisements")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;


    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement", mappedBy="advertisement")
     */
    private $userFavoriteAdvertisements;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry",
     *     inversedBy="Advertisements"
     * )
     * @ORM\JoinColumn(name="advertisement_country_id", referencedColumnName="id", nullable=false)
     */
    private $AdvertisementCountry;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity",
     *     inversedBy="Advertisements"
     * )
     * @ORM\JoinColumn(name="advertisement_city_id", referencedColumnName="id", nullable=true)
     */
    private $AdvertisementCity;

    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Required for Translatable behaviour
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    protected $on_startpage = false;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaidPeriod", mappedBy="advertisement")
     */
    private $paidPeriods;


    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    protected $is_external = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = false})
     */
    protected $is_highlighted = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = false})
     */
    protected $is_highlighted_mobile = false;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true, "default" = 0})
     */
    private $views = 0;

	/**
     * @ORM\Column(type="string", nullable=true)
     */
    private $format;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    private $file_format;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $advertisementHtmlImages;

    /**
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug", mappedBy="advertisement", cascade={"persist", "remove"})
     */
    private $slugs;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new ArrayCollection();
        //$this->requisites = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dutieses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->requirementses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->paymentTransactions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userFavoriteAdvertisements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->advertisementHtmlImages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->slugs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->on_startpage = false;
        $this->is_external = false;
        $this->is_highlighted = false;
    }

    public function getLocale() {
        return $this->locale;
    }

    public function setLocale($locale) {
        $this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Advertisement
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Advertisement
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set additional_description
     *
     * @param string $additionalDescription
     * @return Advertisement
     */
    public function setAdditionalDescription($additionalDescription) {
        $this->additional_description = $additionalDescription;

        return $this;
    }

    /**
     * Get additional_description
     *
     * @return string
     */
    public function getAdditionalDescription() {
        return $this->additional_description;
    }

    /**
     * Set date_from
     *
     * @param \DateTime $dateFrom
     * @return Advertisement
     */
    public function setDateFrom($dateFrom) {
        $this->date_from = $dateFrom;

        return $this;
    }

    /**
     * Get date_from
     *
     * @return \DateTime
     */
    public function getDateFrom() {
        return $this->date_from;
    }

    /**
     * Set date_to
     *
     * @param \DateTime $dateTo
     * @return Advertisement
     */
    public function setDateTo($dateTo) {
        $this->date_to = $dateTo;

        return $this;
    }

    /**
     * Get date_to
     *
     * @return \DateTime
     */
    public function getDateTo() {
        return $this->date_to;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Advertisement
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set cretaed_at
     *
     * @param \DateTime $cretaedAt
     * @return Advertisement
     */
    public function setCretaedAt($cretaedAt) {
        $this->cretaed_at = $cretaedAt;

        return $this;
    }

    /**
     * Get cretaed_at
     *
     * @return \DateTime
     */
    public function getCretaedAt() {
        return $this->cretaed_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Advertisement
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set externalAdvertisement
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisement
     * @return Advertisement
     */
    public function setExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisement = null) {
        $this->externalAdvertisement = $externalAdvertisement;
        $this->externalAdvertisement->setAdvertisements($this);
        $this->is_external = true;
        return $this;
    }

    /**
     * Get externalAdvertisement
     *
     * @return \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement
     */
    public function getExternalAdvertisement() {
        return $this->externalAdvertisement;
    }

    /**
     * Set contactPerson
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\ContactPerson $contactPerson
     * @return Advertisement
     */
    public function setContactPerson(\VisiDarbi\AdvertisementBundle\Entity\ContactPerson $contactPerson = null) {
        $this->contactPerson = $contactPerson;
        $this->contactPerson->setAdvertisement($this);
        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\ContactPerson
     */
    public function getContactPerson() {
        return $this->contactPerson;
    }

    /**
     * Set companies
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Company $companies
     * @return Advertisement
     */
    public function setCompanies(\VisiDarbi\AdvertisementBundle\Entity\Company $companies = null) {
        $this->companies = $companies;
        $this->companies->setAdvertisement($this);
        return $this;
    }

    /**
     * Get companies
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Company
     */
    public function getCompanies() {
        return $this->companies;
    }

    /**
     * Set advertisementEmails
     *
     * @param \VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail $advertisementEmails
     * @return Advertisement
     */
    public function setAdvertisementEmails(\VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail $advertisementEmails = null) {
        $this->advertisementEmails = $advertisementEmails;

        return $this;
    }

    /**
     * Get advertisementEmails
     *
     * @return \VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail
     */
    public function getAdvertisementEmails() {
        return $this->advertisementEmails;
    }

    /**
     * Add requisites
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites
     * @return Advertisement
     */
    public function addRequisite(\VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites) {
        $requisites->setAdvertisement($this);
        $this->requisites = $requisites;

        return $this;
    }

    /**
     * Set requisites
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites
     * @return Advertisement
     */
    public function setRequisite(\VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites) {
        $this->requisites = $requisites;
        $this->requisites->setAdvertisement($this);
        return $this;
    }

//    /**
//     * Remove requisites
//     *
//     * @param \VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites
//     */
//    public function removeRequisite(\VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites)
//    {
//        $this->requisites->removeElement($requisites);
//    }

    /**
     * Get requisites
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Requisites
     */
    public function getRequisites() {
        return $this->requisites;
    }

    /**
     * Add dutieses
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Duty $dutieses
     * @return Advertisement
     */
    public function addDutiese(\VisiDarbi\AdvertisementBundle\Entity\Duty $dutieses) {
        $dutieses->setAdvertisement($this);
        $this->dutieses[] = $dutieses;

        return $this;
    }

    /**
     * Remove dutieses
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Duty $dutieses
     */
    public function removeDutiese(\VisiDarbi\AdvertisementBundle\Entity\Duty $dutieses) {
        $this->dutieses->removeElement($dutieses);
    }

    /**
     * Get dutieses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDutieses() {
        return $this->dutieses;
    }

    /**
     * Add requirementses
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requirement $requirementses
     * @return Advertisement
     */
    public function addRequirementse(\VisiDarbi\AdvertisementBundle\Entity\Requirement $requirementses) {
        $requirementses->setAdvertisement($this);
        $this->requirementses[] = $requirementses;

        return $this;
    }

    /**
     * Remove requirementses
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requirement $requirementses
     */
    public function removeRequirementse(\VisiDarbi\AdvertisementBundle\Entity\Requirement $requirementses) {
        $this->requirementses->removeElement($requirementses);
    }

    /**
     * Get requirementses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequirementses() {
        return $this->requirementses;
    }

    /**
     * Add offers
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Offer $offers
     * @return Advertisement
     */
    public function addOffer(\VisiDarbi\AdvertisementBundle\Entity\Offer $offers) {
        $offers->setAdvertisement($this);
        $this->offers[] = $offers;

        return $this;
    }

    /**
     * Remove offers
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Offer $offers
     */
    public function removeOffer(\VisiDarbi\AdvertisementBundle\Entity\Offer $offers) {
        $this->offers->removeElement($offers);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers() {
        return $this->offers;
    }

    /**
     * Add paymentTransactions
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $paymentTransactions
     * @return Advertisement
     */
    public function addPaymentTransaction(\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $paymentTransactions) {
        $this->paymentTransactions[] = $paymentTransactions;

        return $this;
    }

    /**
     * Remove paymentTransactions
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $paymentTransactions
     */
    public function removePaymentTransaction(\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $paymentTransactions) {
        $this->paymentTransactions->removeElement($paymentTransactions);
    }

    /**
     * Get paymentTransactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentTransactions() {
        return $this->paymentTransactions;
    }

    /**
     * Add userFavoriteAdvertisements
     *
     * @param \VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements
     * @return Advertisement
     */
    public function addUserFavoriteAdvertisement(\VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements) {
        $this->userFavoriteAdvertisements[] = $userFavoriteAdvertisements;

        return $this;
    }

    /**
     * Remove userFavoriteAdvertisements
     *
     * @param \VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements
     */
    public function removeUserFavoriteAdvertisement(\VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement $userFavoriteAdvertisements) {
        $this->userFavoriteAdvertisements->removeElement($userFavoriteAdvertisements);
    }

    /**
     * Get userFavoriteAdvertisements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserFavoriteAdvertisements() {
        return $this->userFavoriteAdvertisements;
    }

    /**
     * Set advertisementType
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType
     * @return Advertisement
     */
    public function setAdvertisementType(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType) {
        $this->advertisementType = $advertisementType;

        return $this;
    }

    /**
     * Get advertisementType
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType
     */
    public function getAdvertisementType() {
        return $this->advertisementType;
    }

    /**
     * Set category
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $category
     * @return Advertisement
     */
    public function setCategory(\VisiDarbi\ProfessionBundle\Entity\Category $category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Set category
     * @return Advertisement
     */
    public function setCategories($categories) {
        $this->Categories = $categories;

        return $this;
    }


    /**
     * Get category
     *
     * @return \VisiDarbi\ProfessionBundle\Entity\Category
     */
    public function getCategory() {
        return $this->category;
    }

    public function setProfessions($professions) {
        $this->Professions = $professions;

        return $this;
    }

    /**
     * Get profession
     *
     * @return \VisiDarbi\ProfessionBundle\Entity\Profession
     */
    public function getProfession() {
        return $this->profession;
    }

    /**
     * Set user
     *
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @return Advertisement
     */
    public function setUser(\VisiDarbi\UserBundle\Entity\User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \VisiDarbi\UserBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return Advertisement
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Get advertisement country
     * @return @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry
     */
    public function getAdvertisementCountry() {
        return $this->AdvertisementCountry;
    }

    /**
     * Get advertisement city
     * @return @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity
     */
    public function getAdvertisementCity() {
        return $this->AdvertisementCity;
    }

    /**
     * Set advertisement country
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $AdvertisementCountry
     */
    public function setAdvertisementCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $AdvertisementCountry) {
        $this->AdvertisementCountry = $AdvertisementCountry;
        return $this;
    }

    /**
     * Set advertisement city
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $AdvertisementCity
     */
    public function setAdvertisementCity($AdvertisementCity) {
        $this->AdvertisementCity = $AdvertisementCity;
        return $this;
    }

    public function addTranslation(AdvertisementTranslation $t) {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function setTranslations($translations) {
        $this->translations = $translations;
    }


    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation $translations) {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations() {
        return $this->translations;
    }

    public function __toString() {
        return (string) $this->getTitle();
    }


    /**
     * Set publicId
     *
     * @param string $publicId
     * @return Advertisement
     */
    public function setPublicId($publicId)
    {
        $this->publicId = $publicId;

        return $this;
    }

    /**
     * Get publicId
     *
     * @return string
     */
    public function getPublicId()
    {
        return $this->publicId;
    }

    /**
     * Set requisites
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites
     * @return Advertisement
     */
    public function setRequisites(\VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites = null)
    {
        $this->requisites = $requisites;
        $this->requisites->setAdvertisement($this);
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function generatePublicId(){
        $publicId = $this->getPublicId();
        if(!$this->getId() && empty($publicId)){
            return $this->setPublicId(uniqid());
        }

        return $this;
    }



    /**
     * Set on_startpage
     *
     * @param boolean $onStartpage
     * @return Advertisement
     */
    public function setOnStartpage($onStartpage)
    {
        $this->on_startpage = $onStartpage;

        return $this;
    }

    /**
     * Get on_startpage
     *
     * @return boolean
     */
    public function getOnStartpage()
    {
        return $this->on_startpage;
    }


    /**
     * Return is advertisiment is in active state
     * @return boolean
     */
    public function getIsActive(){
        return true;
    }

    public function getCompany()
    {
        return $this->getCompanies();
    }


    public function getLocationName()
    {
       if ($this->AdvertisementCity !== null) {
           return $this->AdvertisementCity->getName();
       }

       if ($this->AdvertisementCountry !== null) {

           return $this->AdvertisementCountry->getName();
       }


       return null;
    }

    public function getIsExternal() {
        return $this->is_external;
    }

    /**
     * check if advertisement is highlighted
     * @return boolean
     */
    public function getIsHighlighted() {
        return $this->is_highlighted;
    }

    /**
     * Set is_external
     *
     * @param boolean $isExternal
     * @return Advertisement
     */
    public function setIsExternal($isExternal)
    {
        $this->is_external = $isExternal;

        return $this;
    }

    /**
     * Set is_highlighted
     *
     * @param boolean $isHighlighted
     * @return Advertisement
     */
    public function setIsHighlighted($isHighlighted)
    {
        $this->is_highlighted = $isHighlighted;

        return $this;
    }

    /**
     * check if advertisement is highlighted at mobile version
     * @return boolean
     */
    public function getIsHighlightedMobile()
    {
        return $this->is_highlighted_mobile;
    }

    /**
     * Set is_highlighted_mobile
     * @param mixed $is_highlighted_mobile
     * @return $this
     */
    public function setIsHighlightedMobile($is_highlighted_mobile)
    {
        $this->is_highlighted_mobile = $is_highlighted_mobile;

        return $this;
    }



    /**
     * Set paidPeriods
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriods
     * @return Advertisement
     */
    public function setPaidPeriods(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriods = null)
    {
        $this->paidPeriods = $paidPeriods;

        return $this;
    }

    /**
     * Get paidPeriods
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod
     */
    public function getPaidPeriods()
    {
        return $this->paidPeriods;
    }

    /**
     * Add advertisementEmails
     *
     * @param \VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail $advertisementEmails
     * @return Advertisement
     */
    public function addAdvertisementEmail(\VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail $advertisementEmails)
    {
        $this->advertisementEmails[] = $advertisementEmails;

        return $this;
    }

    /**
     * Remove advertisementEmails
     *
     * @param \VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail $advertisementEmails
     */
    public function removeAdvertisementEmail(\VisiDarbi\EmailsCollectionBundle\Entity\AdvertisementEmail $advertisementEmails)
    {
        $this->advertisementEmails->removeElement($advertisementEmails);
    }

    /**
     * Add Categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     * @return Advertisement
     */
    public function addCategories(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->Categories[] = $categories;

        return $this;
    }

    /**
     * Remove Categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     */
    public function removeCategories(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->Categories->removeElement($categories);
    }

    /**
     * Get Categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->Categories;
    }

    /**
     * Add Professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     * @return Advertisement
     */
    public function addProfession($professions)
    {
        $this->Professions[] = $professions;

        return $this;
    }

    /**
     * Remove Professions
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Profession $professions
     */
    public function removeProfession(\VisiDarbi\ProfessionBundle\Entity\Profession $professions)
    {
        $this->Professions->removeElement($professions);
    }

    /**
     * Get Professions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfessions()
    {
        return $this->Professions;
    }

    /**
     * Returns published at date. If empty - returns creation date.
     * @return \DateTime
     */
    public function getPublishedAt() {

        if(empty($this->published_at)) {
            return $this->cretaed_at;
        }

        return $this->published_at;
    }

    /**
     * Sets published at date
     * @param \DateTime $publishedAt
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement
     */
    public function setPublishedAt($publishedAt) {
        $this->published_at = $publishedAt;
        return $this;
    }




    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

	/**
     * Set views
     *
     * @param string $title
     * @return Advertisement
     */
    public function setCount($views) {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return string
     */
    public function getCount() {
        return $this->views;
    }


    /**
     * Set format
     *
     * @param string $format
     * @return Advertisement
     */
    public function setFormat($format)
    {
        if (empty($format)) {
            $format = self::FORMAT_FULL;
        }

        if (!in_array($format, self::getFormatsList())) {
            throw new \InvalidArgumentException("Invalid format");
        }

        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        if (empty($this->format)) {
            $this->format = self::FORMAT_FULL;
        }
        return $this->format;
    }


    public static function getFormatsList() {
        return array(self::FORMAT_FULL, self::FORMAT_HTML, self::FORMAT_IMAGE_PDF, self::FORMAT_FREE);
    }

    public static function getFormatChoices() {
        return array(
            self::FORMAT_FULL => 'Visidarbi full form',
            self::FORMAT_HTML => 'HTML form',
            self::FORMAT_IMAGE_PDF => 'Picture/pdf form',
            self::FORMAT_FREE => 'Free text input form',
        );
    }

    public static function getValidFormat($format) {
        if (!in_array($format, self::getFormatsList())) {
            return self::FORMAT_FULL;
        } else {
            return $format;
        }
    }

    /**
     * Add Categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     * @return Advertisement
     */
    public function addCategorie(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->Categories[] = $categories;

        return $this;
    }

    /**
     * Remove Categories
     *
     * @param \VisiDarbi\ProfessionBundle\Entity\Category $categories
     */
    public function removeCategorie(\VisiDarbi\ProfessionBundle\Entity\Category $categories)
    {
        $this->Categories->removeElement($categories);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Advertisement
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set file_format
     *
     * @param string $fileFormat
     * @return Advertisement
     */
    public function setFileFormat($fileFormat)
    {
        $this->file_format = $fileFormat;

        return $this;
    }

    /**
     * Get file_format
     *
     * @return string
     */
    public function getFileFormat()
    {
        return $this->file_format;
    }

    /**
     * Add advertisementHtmlImages
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage $advertisementHtmlImages
     * @return Advertisement
     */
    public function addAdvertisementHtmlImage(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage $advertisementHtmlImages)
    {
        $advertisementHtmlImages->setAdvertisement($this);

        $this->advertisementHtmlImages[] = $advertisementHtmlImages;

        return $this;
    }

    /**
     * Remove advertisementHtmlImages
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage $advertisementHtmlImages
     */
    public function removeAdvertisementHtmlImage(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage $advertisementHtmlImages)
    {
        $this->advertisementHtmlImages->removeElement($advertisementHtmlImages);
    }

    /**
     * Get advertisementHtmlImages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvertisementHtmlImages()
    {
        return $this->advertisementHtmlImages;
    }

    public function getSlugOrPublicId($locale = null)
    {
        foreach ($this->slugs as $slug) {
            if ($slug->getLocale() == $locale) {
                return $slug->getSlug();
            }
        }

        return $this->publicId;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Advertisement
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Add slugs
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug $slugs
     * @return Advertisement
     */
    public function addSlug(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug $slugs)
    {
        $slugs->setAdvertisement($this);
        $this->slugs[] = $slugs;

        return $this;
    }

    /**
     * Remove slugs
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug $slugs
     */
    public function removeSlug(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug $slugs)
    {
        $this->slugs->removeElement($slugs);
    }

    /**
     * Get slugs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlugs()
    {
        return $this->slugs;
    }
}