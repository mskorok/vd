<?php
namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation")
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(type="string")
     */
    private $slug;

     /**
     * @Gedmo\Slug(fields={"name"}, unique=false, style="camel")
     * @ORM\Column(type="string", length=1)
     */
    private $az;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $registration_number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $logo_path;

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", inversedBy="companies")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false, unique=true, onDelete="CASCADE")
     */
    private $advertisement;


    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * This one is not to store - only for contain value
     * @var integer
     */
    private $advertisementsBySlug;

    public function getAdvertisementsBySlug() {
        return $this->advertisementsBySlug;
    }

    public function setAdvertisementsBySlug($advertisementsBySlug) {
        $this->advertisementsBySlug = $advertisementsBySlug;
        return $this;
    }

        /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get registration number
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registration_number;
    }

    /**
     * Set registration number
     *
     * @param string $registration_number
     * @return Company
     */
    public function setRegistrationNumber($registration_number)
    {
        $this->registration_number = $registration_number;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Company
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Company
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return Company
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;

        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    public function addTranslation(CompanyTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }


    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\OfferTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\OfferTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Company
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set az
     *
     * @param string $az
     * @return Company
     */
    public function setAz($az)
    {
        $this->az = $az;

        return $this;
    }

    /**
     * Get az
     *
     * @return string
     */
    public function getAz()
    {
        return $this->az;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeLogoOnRemove()
    {
        $filename = $this->getFullLogoPath();

        if($this->getLogo() && file_exists($filename)) {
            unlink($filename);
        }
    }

    public function getFullLogoPath(){
        $rootPath = __DIR__.'/../../../../web/';
        $filename = $rootPath . $this->getLogoPath() . '/' . $this->getLogo();
        return $filename;
    }

    public function isLogoFileExists(){
        return file_exists($this->getFullLogoPath());
    }

    public function getLogoPath() {
        return $this->logo_path;
    }

    public function setLogoPath($logoPath) {
        $this->logo_path = $logoPath;
        return $this;
    }



}