<?php
namespace VisiDarbi\AdvertisementBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity
 */
class PaidPeriod
{
    
    const STATUS_PENDING = 10;
    const STATUS_ACTIVE = 20;
    const STATUS_CLOSED = 30;
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="date", nullable=true)
     */
    private $from_date;

    /** 
     * @ORM\Column(type="date", nullable=true)
     */
    private $to_date;

    /** 
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /** 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @Gedmo\Timestampable(on="update") 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", inversedBy="paidPeriods")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", unique=true)
     */
    private $advertisement;

    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\UserBundle\Entity\User", inversedBy="paidPeriods")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction", mappedBy="paidPeriod")
     */
    private $paymentTransactions;    
    
    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementType", inversedBy="paidPeriod")
     * @ORM\JoinColumn(name="advertisement_type_id", referencedColumnName="id", nullable=false)
     */
    private $advertisementType;    

    /**
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Requisites", mappedBy="paid_period", cascade={"persist", "remove"})
     */
    private $requisites;    
    
    
    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $createdByAdmin;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set from_date
     *
     * @param \DateTime $fromDate
     * @return PaidPeriod
     */
    public function setFromDate($fromDate)
    {
        $this->from_date = $fromDate;
    
        return $this;
    }

    /**
     * Get from_date
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->from_date;
    }

    /**
     * Set to_date
     *
     * @param \DateTime $toDate
     * @return PaidPeriod
     */
    public function setToDate($toDate)
    {
        $this->to_date = $toDate;
    
        return $this;
    }

    /**
     * Get to_date
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->to_date;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PaidPeriod
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return PaidPeriod
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return PaidPeriod
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return PaidPeriod
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement = null)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    /**
     * Set user
     *
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @return PaidPeriod
     */
    public function setUser(\VisiDarbi\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \VisiDarbi\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set paymentTransactions
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $paymentTransactions
     * @return PaidPeriod
     */
    public function setPaymentTransactions(\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $paymentTransactions = null)
    {
        $this->paymentTransactions = $paymentTransactions;
    
        return $this;
    }

    /**
     * Get paymentTransactions
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction 
     */
    public function getPaymentTransactions()
    {
        return $this->paymentTransactions;
    }

    /**
     * Set advertisementType
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType
     * @return PaidPeriod
     */
    public function setAdvertisementType(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType)
    {
        $this->advertisementType = $advertisementType;
    
        return $this;
    }

    /**
     * Get advertisementType
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType 
     */
    public function getAdvertisementType()
    {
        return $this->advertisementType;
    }

    /**
     * Set requisites
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites
     * @return Advertisement
     */
    public function setRequisites(\VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites) {
        $this->requisites = $requisites;
        $this->requisites->setPaidPeriod($this);
        return $this;
    }


    /**
     * Get requisites
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Requisites 
     */
    public function getRequisites() {
        return $this->requisites;
    } 
    
    public function getCreatedByAdmin() 
    {
        return $this->createdByAdmin;
    }

    public function setCreatedByAdmin($createdByAdmin) 
    {
        $this->createdByAdmin = $createdByAdmin;
    }    
    
    public function isActive() 
    {
        return $this->status == self::STATUS_ACTIVE;
    }
    
    public function advertisementsLeft() 
    {
        if ($this->advertisementType->getUnlimited()) {
            return 'Unlimited';
        }
        else {
            return $this->isActive() ? 1 : 0;
        }
    }
    
}