<?php //
//namespace VisiDarbi\AdvertisementBundle\Entity;
//
//use Gedmo\Mapping\Annotation as Gedmo;
//use Doctrine\ORM\Mapping AS ORM;
//use Symfony\Component\Validator\Constraints as Assert;
//
///**
// * @ORM\Entity
// * @ORM\Table(name="slud_data_test", options={"engine"="MyISAM"},
// *     indexes={
// *          @ORM\Index(name="resource", columns={"resource"}),
// *          @ORM\Index(name="slud_checksum", columns={"slud_checksum"}),
// *     },
// *     uniqueConstraints={@ORM\UniqueConstraint(name="link", columns={"link"})}
// * )
// */
////ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\GrabbedAdvertisementEntityRepository")
////ORM\Index(name="prof", columns={"prof","slud","org","additional_keywords","category"}, flags={"fulltext"}),
////ORM\Index(name="loc", columns={"loc","loc2"}, flags={"fulltext"})
//class GrabbedAdvertisement
//{
//
//    const MAP_ACTION_NONE = 'none';
//    const MAP_ACTION_DEACTIVATE = 'deactivate';
//    const MAP_ACTION_UPDATE = 'update';
//
//    /**
//     * @ORM\Id
//     * @ORM\Column(type="integer")
//     * @ORM\GeneratedValue(strategy="AUTO")
//     */
//    private $id;
//
//    /**
//     * @ORM\Column(type="string", nullable=false, length=254)
//     */
//    private $prof;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=250)
//     */
//    private $prof2;
//
//    /**
//     * @ORM\Column(type="text", nullable=false)
//     */
//    private $slud;
//
//    /**
//     * @ORM\Column(type="string", nullable=false, length=254)
//     */
//    private $link;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=254)
//     */
//    private $loc;
//
//    /**
//     * @ORM\Column(type="string", nullable=true)
//     */
//    private $loc2;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=50)
//     */
//    private $region;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=254)
//     */
//    private $org;
//
//    /**
//     * @ORM\Column(type="integer", nullable=true, length=15, options={"unsigned"=true, "default" = 0})
//     */
//    private $date = 0;
//
//    /**
//     * @ORM\Column(type="integer", nullable=false, length=15, options={"unsigned"=true, "default" = 0} )
//     */
//    private $added = 0;
//
//    /**
//     * @ORM\Column(type="integer", nullable=true, length=15, options={"unsigned"=true})
//     */
//    private $active;
//
//    /**
//     * @ORM\Column(type="boolean", nullable=false)
//     */
//    private $type;
//
//    /**
//     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true, "default" = 0})
//     */
//    private $views = 0;
//
//    /**
//     * @ORM\Column(type="boolean", nullable=false, options={"default" = true})
//     */
//    private $public = true;
//
//    /**
//     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true, "default" = 0} )
//     */
//    private $shows = 0;
//
//    /**
//     * @ORM\Column(type="datetime", nullable=true)
//     */
//    private $imported_at;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=30)
//     */
//    private $portal;
//
//    /**
//     * @ORM\Column(type="boolean", nullable=true, options={"unsigned"=true, "default" = false})
//     */
//    private $is_sponsored = false;
//
//    /**
//     * @ORM\Column(type="string", nullable=true)
//     */
//    private $additional_keywords;
//
//    /**
//     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true, "default" = 0})
//     */
//    private $sponsored_expositions = 0;
//
//    /**
//     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true, "default" = 0})
//     */
//    private $sponsored_clicks = 0;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=150)
//     */
//    private $category;
//
//    /**
//     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true, "default" = 0})
//     */
//    private $clicks_left = 0;
//
//    /**
//     * @ORM\Column(type="string", nullable=true)
//     */
//    private $resource;
//
//    /**
//     * @ORM\Column(type="boolean", nullable=true, options={"default" = false})
//     */
//    private $is_mapped = false;
//
//    /**
//     * @ORM\Column(type="blob", nullable=true)
//     */
//    private $emails;
//
//    /**
//     * @ORM\Column(type="string", nullable=true)
//     */
//    private $category_name;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=32)
//     */
//    private $slud_checksum;
//
//    /**
//     * @ORM\Column(type="string", nullable=true, length=100)
//     */
//    private $company;
//
//    /**
//     * @ORM\Column(type="string", nullable=false, length=10, options={"default" = "none"})
//     */
//    private $map_action = self::MAP_ACTION_NONE;
//
//    /**
//     * @Gedmo\Timestampable(on="create")
//     * @ORM\Column(type="datetime", nullable=true)
//     */
//    private $created_at;
//
//    /**
//     * @Gedmo\Timestampable(on="update")
//     * @ORM\Column(type="datetime", nullable=true)
//     */
//    private $updated_at;
//
//
//    /**
//     * Constructor
//     */
//    public function __construct() {
//        $this->views = 0;
//        $this->active = 0;
//        $this->added = 0;
//        $this->date = 0;
//        $this->public = true;
//        $this->shows = 0;
//        $this->is_sponsored = false;
//        $this->sponsored_expositions = 0;
//        $this->sponsored_clicks = 0;
//        $this->clicks_left = 0;
//        $this->is_mapped = false;
//        $this->map_action = self::MAP_ACTION_NONE;
//    }
//
//    /**
//     * Get id
//     *
//     * @return integer
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    public function getProf(){
//        return $this->prof;
//    }
//
//    public function getProf2(){
//        return $this->prof2;
//    }
//
//    public function getSlud(){
//        return $this->slud;
//    }
//
//    public function getLink(){
//        return $this->link;
//    }
//
//    public function getLoc(){
//        return $this->loc;
//    }
//
//    public function getLoc2(){
//        return $this->loc2;
//    }
//
//    public function getRegion(){
//        return $this->region;
//    }
//
//    public function getOrg(){
//        return $this->org;
//    }
//
//    public function getDate(){
//        return $this->date;
//    }
//
//    public function getAdded(){
//        return $this->added;
//    }
//
//    public function getActive(){
//        return $this->active;
//    }
//
//    public function getType(){
//        return $this->type;
//    }
//
//    public function getViews(){
//        return $this->views;
//    }
//
//    public function getPublic(){
//        return $this->public;
//    }
//
//    public function getShows(){
//        return $this->shows;
//    }
//
//    public function getImportedAt(){
//        return $this->imported_at;
//    }
//
//    public function getPortal(){
//        return $this->portal;
//    }
//
//    public function getIsSponsored(){
//        return $this->is_sponsored;
//    }
//
//    public function getAdditionalKeywords(){
//        return $this->additional_keywords;
//    }
//
//    public function getSponsoredExpositions(){
//        return $this->sponsored_expositions;
//    }
//
//    public function getSponsoredClicks(){
//        return $this->sponsored_clicks;
//    }
//
//    public function getCategory(){
//        return $this->category;
//    }
//
//    public function getClicksLeft(){
//        return $this->clicks_left;
//    }
//
//    public function getResource(){
//        return $this->resource;
//    }
//
//    public function getIsMapped(){
//        return $this->is_mapped;
//    }
//
//    public function getEmails(){
//        return $this->emails;
//    }
//
//    public function getCategoryName(){
//        return $this->category_name;
//    }
//
//    public function getSludChecksum(){
//        return $this->slud_checksum;
//    }
//
//    public function getCompany(){
//        return $this->company;
//    }
//
//    public function getMapAction(){
//        return $this->map_action;
//    }
//
//    public function getCreatedAt(){
//        return $this->created_at;
//    }
//
//    public function getUpdatedAt(){
//        return $this->updated_at;
//    }
//
//    // SETTERS
//
//    public function setProf($prof){
//        $this->prof = $prof;
//        return $this;
//    }
//
//    public function setProf2($prof2){
//        $this->prof2 = $prof2;
//        return $this;
//    }
//
//    public function setSlud($slud){
//        $this->slud = $slud;
//        return $this;
//    }
//
//    public function setLink($link){
//        $this->link = $link;
//        return $this;
//    }
//
//    public function setLoc($loc){
//        $this->loc = $loc;
//        return $this;
//    }
//
//    public function setLoc2($loc2){
//        $this->loc2 = $loc2;
//        return $this;
//    }
//
//    public function setRegion($region){
//        $this->region = $region;
//        return $this;
//    }
//
//    public function setOrg($org){
//        $this->org = $org;
//        return $this;
//    }
//
//    public function setDate($date){
//        $this->date = $date;
//        return $this;
//    }
//
//    public function setAdded($added){
//        $this->added = $added;
//        return $this;
//    }
//
//    public function setActive($active){
//        $this->active = $active;
//        return $this;
//    }
//
//    public function setType($type){
//        $this->type = $type;
//        return $this;
//    }
//
//    public function setViews($views){
//        $this->views = $views;
//        return $this;
//    }
//
//    public function setPublic($public){
//        $this->public = $public;
//        return $this;
//    }
//
//    public function setShows($shows){
//        $this->shows = $shows;
//        return $this;
//    }
//
//    public function setImportedAt($imported_at){
//        $this->imported_at = $imported_at;
//        return $this;
//    }
//
//    public function setPortal($portal){
//        $this->portal = $portal;
//        return $this;
//    }
//
//    public function setIsSponsored($is_sponsored){
//        $this->is_sponsored = $is_sponsored;
//        return $this;
//    }
//
//    public function setAdditionalKeywords($additional_keywords){
//        $this->additional_keywords = $additional_keywords;
//        return $this;
//    }
//
//    public function setSponsoredExpositions($sponsored_expositions){
//        $this->sponsored_expositions = $sponsored_expositions;
//        return $this;
//    }
//
//    public function setSponsoredClicks($sponsored_clicks){
//        $this->sponsored_clicks = $sponsored_clicks;
//        return $this;
//    }
//
//    public function setCategory($category){
//        $this->category = $category;
//        return $this;
//    }
//
//    public function setClicksLeft($clicks_left){
//        $this->clicks_left = $clicks_left;
//        return $this;
//    }
//
//    public function setResource($resource){
//        $this->resource = $resource;
//        return $this;
//    }
//
//    public function setIsMapped($is_mapped){
//        $this->is_mapped = $is_mapped;
//        return $this;
//    }
//
//    public function setEmails($emails){
//        $this->emails = $emails;
//        return $this;
//    }
//
//    public function setCategoryName($category_name){
//        $this->category_name = $category_name;
//        return $this;
//    }
//
//    public function setSludChecksum($slud_checksum){
//        $this->slud_checksum = $slud_checksum;
//        return $this;
//    }
//
//    public function setCompany($company){
//        $this->company = $company;
//        return $this;
//    }
//
//    public function setMapAction($map_action){
//
//        if (!in_array($map_action, array(self::MAP_ACTION_NONE, self::MAP_ACTION_DEACTIVATE, self::MAP_ACTION_UPDATE))) {
//            throw new \InvalidArgumentException("Invalid map action");
//        }
//
//        $this->map_action = $map_action;
//        return $this;
//    }
//
//    public function setCreatedAt($created_at){
//        $this->created_at = $created_at;
//        return $this;
//    }
//
//    public function setUpdatedAt($updated_at){
//        $this->updated_at = $updated_at;
//        return $this;
//    }
//
//
//}