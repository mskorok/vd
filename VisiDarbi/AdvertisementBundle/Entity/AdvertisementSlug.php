<?php

namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AdvertisementSlug
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AdvertisementSlug
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slugSource", type="string", length=255)
     */
    private $slugSource;

    /**
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Gedmo\Slug(fields={"slugSource"}, updatable=false, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=255)
     */
    private $locale;
    
    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", 
     *     inversedBy="slugs"
     * )
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false)
     */
    private $advertisement;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slugSource
     *
     * @param string $slugSource
     * @return AdvertisementSlug
     */
    public function setSlugSource($slugSource)
    {
        $this->slugSource = $slugSource;
    
        return $this;
    }

    /**
     * Get slugSource
     *
     * @return string 
     */
    public function getSlugSource()
    {
        return $this->slugSource;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return AdvertisementSlug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return AdvertisementSlug
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    
        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return AdvertisementSlug
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }
}