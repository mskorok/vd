<?php
namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation")
 */
class AdvertisementType
{
    
    const TYPE_LIMITED_7_DAY = 10;
    const TYPE_LIMITED_14_DAY = 20;
    const TYPE_UNLIMITED_7_DAY = 30;
    const TYPE_UNLIMITED_14_DAY = 40;
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /** 
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /** 
     * @ORM\Column(type="integer", nullable=false)
     */
    private $days_period;

    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $payd;

    /** 
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $unlimited;    
    
    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice", 
     *     mappedBy="advertisementType"
     * )
     */
    private $advertisingTypePrices;

    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", mappedBy="advertisementType")
     */
    private $advertisements;


    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement", 
     *     mappedBy="advertisementType"
     * )
     */
    private $externalAdvertisements;    
    
    
    
    /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaidPeriod", mappedBy="advertisementType")
     */
    private $paidPeriod;    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->advertisingTypePrices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->advertisements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set id
     *
     * @param integer $id
     * @return AdvertisementType
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AdvertisementType
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set days_period
     *
     * @param integer $daysPeriod
     * @return AdvertisementType
     */
    public function setDaysPeriod($daysPeriod)
    {
        $this->days_period = $daysPeriod;
    
        return $this;
    }

    /**
     * Get days_period
     *
     * @return integer 
     */
    public function getDaysPeriod()
    {
        return $this->days_period;
    }

    /**
     * Set payd
     *
     * @param boolean $payd
     * @return AdvertisementType
     */
    public function setPayd($payd)
    {
        $this->payd = $payd;
    
        return $this;
    }

    /**
     * Get payd
     *
     * @return boolean 
     */
    public function getPayd()
    {
        return $this->payd;
    }

    /**
     * Set unlimited
     *
     * @param boolean $unlimited
     * @return AdvertisementType
     */
    public function setUnlimited($unlimited)
    {
        $this->unlimited = $unlimited;
        return $this;
    }
    
    /**
     * Get unlimited
     *
     * @return boolean 
     */
    public function getUnlimited()
    {
        return $this->unlimited;
    }

    /**
     * Add advertisingTypePrices
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices
     * @return AdvertisementType
     */
    public function addAdvertisingTypePrice(\VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices)
    {
        $this->advertisingTypePrices[] = $advertisingTypePrices;
    
        return $this;
    }

    /**
     * Remove advertisingTypePrices
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices
     */
    public function removeAdvertisingTypePrice(\VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice $advertisingTypePrices)
    {
        $this->advertisingTypePrices->removeElement($advertisingTypePrices);
    }

    /**
     * Get advertisingTypePrices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisingTypePrices()
    {
        return $this->advertisingTypePrices;
    }

    /**
     * Add advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return AdvertisementType
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->advertisements[] = $advertisements;
    
        return $this;
    }

    /**
     * Remove advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->advertisements->removeElement($advertisements);
    }

    /**
     * Get advertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisements()
    {
        return $this->advertisements;
    }
    
    public function addTranslation(AdvertisementTypeTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }    
    

    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }
    
    public function __toString() {
        return (string) $this->getName();
    }

    /**
     * Add externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     * @return AdvertisementType
     */
    public function addexternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements[] = $externalAdvertisements;
    
        return $this;
    }

    /**
     * Remove externalAdvertisements
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements
     */
    public function removeExternalAdvertisement(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement $externalAdvertisements)
    {
        $this->externalAdvertisements->removeElement($externalAdvertisements);
    }

    /**
     * Get externalAdvertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalAdvertisements()
    {
        return $this->externalAdvertisements;
    }

    /**
     * Add paidPeriod
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriod
     * @return AdvertisementType
     */
    public function addPaidPeriod(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriod)
    {
        $this->paidPeriod[] = $paidPeriod;
    
        return $this;
    }

    /**
     * Remove paidPeriod
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriod
     */
    public function removePaidPeriod(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriod)
    {
        $this->paidPeriod->removeElement($paidPeriod);
    }

    /**
     * Get paidPeriod
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPaidPeriod()
    {
        return $this->paidPeriod;
    }
    
    public function getPrice() 
    {
        return $this->advertisingTypePrices->first();
    }
}