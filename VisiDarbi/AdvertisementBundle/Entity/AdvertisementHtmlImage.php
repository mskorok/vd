<?php
namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation")
 */
class AdvertisementHtmlImage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", inversedBy="advertisementHtmlImages")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $advertisement;

    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return AdvertisementHtmlImage
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return AdvertisementHtmlImage
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    /**
     * Add translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation $translations
     * @return AdvertisementHtmlImage
     */
    public function addTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImageTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

}