<?php

namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation")
 */
class AdvertisementCountry extends \VisiDarbi\CommonBundle\Entity\Translated {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity", 
     *     mappedBy="AdvertisementCountry",
     *     cascade={"persist"}
     * )
     */
    private $AdvertisementCities;

    /**
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", 
     *     mappedBy="AdvertisementCountry"
     * )
     */
    private $Advertisements;

    /**
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace", 
     *     mappedBy="advertisementCountry"
     * )
     */
    private $externalPlaces;    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", inversedBy="advertisementCountries")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;

    /**
     * @Gedmo\Locale
     * @var type 
     */
    protected $locale;
    
    /**
     * Add Cities
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\City $cities
     * @return Country
     */
    public function addCitie(\VisiDarbi\LocalePlaceBundle\Entity\City $cities) {
        $this->Cities[] = $cities;

        return $this;
    }

    /**
     * Remove Cities
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\City $cities
     */
    public function removeAdvertisementCities(\VisiDarbi\LocalePlaceBundle\Entity\City $cities) {
        $this->AdvertisementCities->removeElement($cities);
    }

    /**
     * Get Cities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisementCities() {
        return $this->AdvertisementCities;
    }

    /**
     * Set Cities
     * @param \Doctrine\Common\Collections\Collection $AdvertisementCities
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry
     */
    public function setAdvertisementCities(\Doctrine\Common\Collections\Collection $AdvertisementCities) {
        $this->AdvertisementCities = $AdvertisementCities;
        return $this;
    }

    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new ArrayCollection();
        $this->AdvertisementCities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Advertisements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->AdvertisementCities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return AdvertisementCountry
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AdvertisementCountry
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Add AdvertisementCities
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $advertisementCities
     * @return AdvertisementCountry
     */
    public function addAdvertisementCitie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $advertisementCities) {
        $this->AdvertisementCities[] = $advertisementCities;

        return $this;
    }

    /**
     * Remove AdvertisementCities
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $advertisementCities
     */
    public function removeAdvertisementCitie(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity $advertisementCities) {
        $this->AdvertisementCities->removeElement($advertisementCities);
    }

    /**
     * Add Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return AdvertisementCountry
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements) {
        $this->Advertisements[] = $advertisements;

        return $this;
    }

    /**
     * Remove Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements) {
        $this->Advertisements->removeElement($advertisements);
    }
    

    /**
     * Add external place
     * @param \VisiDarbi\ExternalAdvertisementBundle\EntityA\ExternalPlace $externalPlace
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry
     */
    public function addExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace) {
        $this->externalPlaces[] = $externalPlace;
        return $this;
    }

    /**
     * Remove external place
     * @param \VisiDarbi\ExternalAdvertisementBundle\EntityA\ExternalPlace $externalPlace
     */
    public function removeExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace) {
        $this->externalPlaces->removeElement($externalPlace);
    }    
    

    /**
     * Get Advertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisements() {
        return $this->Advertisements;
    }

    public function addTranslation(AdvertisementCountryTranslation $t) {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation $translations) {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations() {
        return $this->translations;
    }

    public function __toString() {
        return (string) $this->getName();
    }

    /**
     * Returns system country
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set system country
     * @param type $country
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

}