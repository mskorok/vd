<?php
namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation")
 */
class ContactPerson
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $name;

    /** 
     * @Gedmo\Translatable  
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $position;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $fax;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $home_address;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $legal_address;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $actual_address;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;    
    
    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", inversedBy="contactPerson", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false, unique=true, onDelete="CASCADE")
     */
    private $advertisement;


    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;
    
    /**
     * Constructor
     */
    public function __construct()
    {        
        $this->translations = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ContactPerson
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return ContactPerson
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ContactPerson
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return ContactPerson
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return ContactPerson
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set home_address
     *
     * @param string $homeAddress
     * @return ContactPerson
     */
    public function setHomeAddress($homeAddress)
    {
        $this->home_address = $homeAddress;
    
        return $this;
    }

    /**
     * Get home_address
     *
     * @return string 
     */
    public function getHomeAddress()
    {
        return $this->home_address;
    }

    /**
     * Set legal_address
     *
     * @param string $legalAddress
     * @return ContactPerson
     */
    public function setLegalAddress($legalAddress)
    {
        $this->legal_address = $legalAddress;
    
        return $this;
    }

    /**
     * Get legal_address
     *
     * @return string 
     */
    public function getLegalAddress()
    {
        return $this->legal_address;
    }

    /**
     * Set actual_address
     *
     * @param string $actualAddress
     * @return ContactPerson
     */
    public function setActualAddress($actualAddress)
    {
        $this->actual_address = $actualAddress;
    
        return $this;
    }

    /**
     * Get actual_address
     *
     * @return string 
     */
    public function getActualAddress()
    {
        return $this->actual_address;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return ContactPerson
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }

    
    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return ContactPerson
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }    
    
    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }
    
    public function addTranslation(ContactPersonTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }    

    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\ContactPerson $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }
    
    
    public function __toString() {
        return (string) $this->getName();
    }
    
    /**
     * Get url
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set url
     * @param string $url
     * @return \VisiDarbi\AdvertisementBundle\Entity\ContactPerson
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    public function getFirstName() {
        $data = $this->splitName($this->getName());
        return $data['firstname'];
    }
    
    public function getLastName() {
        $data = $this->splitName($this->getName());
        return $data['lastname'];        
    }
    
    
    /**
     * Split one string name for first and last name
     * @param string $name
     * @return array
     */
    private function splitName($name) {
        $data = array();
        $name = explode(' ', $name);
        $data['firstname'] = $name[0];
        unset($name[0]);
        $data['lastname'] = implode(' ', $name);

        return $data;
    }    
    
}