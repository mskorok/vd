<?php
namespace VisiDarbi\AdvertisementBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 */
class Requisites
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $company_name;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $contact_person_name;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $PVN;

    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $no_PVN;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $registration_number;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $bank_code;

    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $bank_account;

    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", inversedBy="requisites")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=true, unique=true, onDelete="CASCADE")
     */
    private $advertisement;

    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaidPeriod", inversedBy="requisites")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", nullable=true, unique=true, onDelete="CASCADE")
     */
    private $paid_period;    
    
    
    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction", mappedBy="requisites")
     */
    private $paymentTransaction;  

    /**
     * Set id
     *
     * @param integer $id
     * @return Requisites
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     * @return Requisites
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;
    
        return $this;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Requisites
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Requisites
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contact_person_name
     *
     * @param string $contactPersonName
     * @return Requisites
     */
    public function setContactPersonName($contactPersonName)
    {
        $this->contact_person_name = $contactPersonName;
    
        return $this;
    }

    /**
     * Get contact_person_name
     *
     * @return string 
     */
    public function getContactPersonName()
    {
        return $this->contact_person_name;
    }

    /**
     * Set PVN
     *
     * @param string $pVN
     * @return Requisites
     */
    public function setPVN($pVN)
    {
        $this->PVN = $pVN;
    
        return $this;
    }

    /**
     * Get PVN
     *
     * @return string 
     */
    public function getPVN()
    {
        return $this->PVN;
    }

    /**
     * Set no_PVN
     *
     * @param boolean $noPVN
     * @return Requisites
     */
    public function setNoPVN($noPVN)
    {
        $this->no_PVN = $noPVN;
    
        return $this;
    }

    /**
     * Get no_PVN
     *
     * @return boolean 
     */
    public function getNoPVN()
    {
        return $this->no_PVN;
    }

    /**
     * Set registration_number
     *
     * @param string $registrationNumber
     * @return Requisites
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registration_number = $registrationNumber;
    
        return $this;
    }

    /**
     * Get registration_number
     *
     * @return string 
     */
    public function getRegistrationNumber()
    {
        return $this->registration_number;
    }

    /**
     * Set bank_code
     *
     * @param string $bankCode
     * @return Requisites
     */
    public function setBankCode($bankCode)
    {
        $this->bank_code = $bankCode;
    
        return $this;
    }

    /**
     * Get bank_code
     *
     * @return string 
     */
    public function getBankCode()
    {
        return $this->bank_code;
    }

    /**
     * Set bank_account
     *
     * @param string $bankAccount
     * @return Requisites
     */
    public function setBankAccount($bankAccount)
    {
        $this->bank_account = $bankAccount;
    
        return $this;
    }

    /**
     * Get bank_account
     *
     * @return string 
     */
    public function getBankAccount()
    {
        return $this->bank_account;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return Requisites
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        //$advertisement->setRequisites($this);
        $this->advertisement = $advertisement;
    
        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }
    /**
     * Get paymentTransaction
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction 
     */
    public function getPaymentTransaction()
    {
        return $this->paymentTransaction;
    }

    
    /**
     * Sets payment transaction
     * @param \VisiDarbi\AdvertisementBundle\Entity\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $transaction
     * @return \VisiDarbi\AdvertisementBundle\Entity\Requisites
     */
    public function setPaymentTransaction(\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $transaction)
    {
        $this->paymentTransaction = $transaction;
        return $this;
    }    
    
    
    /**
     * Get related paid period
     * @return \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod
     */
    public function getPaidPeriod() {
        return $this->paid_period;
    }

    /**
     * Sets related paid period
     * @param type \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod
     */
    public function setPaidPeriod($paid_period) {
        $this->paid_period = $paid_period;
    }
 

    
}