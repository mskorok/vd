<?php
namespace VisiDarbi\AdvertisementBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypePriceTranslation") 
 */
class AdvertisingTypePrice
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** 
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=false)
     */
    private $price;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementType", 
     *     inversedBy="advertisingTypePrices"
     * )
     * @ORM\JoinColumn(name="advertisement_type_id", referencedColumnName="id", nullable=false)
     */
    private $advertisementType;

    /** 
     * @ORM\ManyToOne(targetEntity="VisiDarbi\LocalePlaceBundle\Entity\Country", inversedBy="advertisingTypePrices")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    private $country;
    
    
    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $most_popular;    
    

    /** 
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=false)
     */
    private $description;    
    
    /** 
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_description;    
    
    /** 
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_description_first_page;    
    
    /** 
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_description_higlighting;    
    
    /** 
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_description_period;


    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_description_mobile_highlighting;


    
    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypePriceTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return AdvertisingTypePrice
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set advertisementType
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType
     * @return AdvertisingTypePrice
     */
    public function setAdvertisementType(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType $advertisementType)
    {
        $this->advertisementType = $advertisementType;
    
        return $this;
    }

    /**
     * Get advertisementType
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementType 
     */
    public function getAdvertisementType()
    {
        return $this->advertisementType;
    }

    /**
     * Set country
     *
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return AdvertisingTypePrice
     */
    public function setCountry(\VisiDarbi\LocalePlaceBundle\Entity\Country $country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \VisiDarbi\LocalePlaceBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    
    
    public function getMostPopular() {
        return $this->most_popular;
    }

    public function setMostPopular($most_popular) {
        $this->most_popular = $most_popular;
        return $this;
    }
    
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescriptionMobileHighlighting()
    {
        return $this->payment_description_mobile_highlighting;
    }

    /**
     * @param mixed $payment_description_mobile_highlighting
     */
    public function setPaymentDescriptionMobileHighlighting($payment_description_mobile_highlighting)
    {
        $this->payment_description_mobile_highlighting = $payment_description_mobile_highlighting;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescription()
    {
        return $this->payment_description;
    }

    /**
     * @param mixed $payment_description
     */
    public function setPaymentDescription($payment_description)
    {
        $this->payment_description = $payment_description;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescriptionFirstPage()
    {
        return $this->payment_description_first_page;
    }

    /**
     * @param mixed $payment_description_first_page
     */
    public function setPaymentDescriptionFirstPage($payment_description_first_page)
    {
        $this->payment_description_first_page = $payment_description_first_page;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescriptionHiglighting()
    {
        return $this->payment_description_higlighting;
    }

    /**
     * @param mixed $payment_description_higlighting
     */
    public function setPaymentDescriptionHiglighting($payment_description_higlighting)
    {
        $this->payment_description_higlighting = $payment_description_higlighting;
    }

    /**
     * @return mixed
     */
    public function getPaymentDescriptionPeriod()
    {
        return $this->payment_description_period;
    }

    /**
     * @param mixed $payment_description_period
     */
    public function setPaymentDescriptionPeriod($payment_description_period)
    {
        $this->payment_description_period = $payment_description_period;
    }



    public function addTranslation(AdvertisementTypePriceTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }    
    

    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypePriceTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypePriceTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }    
        
    public function __toString() {
        return $this->getCountry() . ' - ' . $this->getAdvertisementType() . ' - ' . $this->getPrice();
    }
}