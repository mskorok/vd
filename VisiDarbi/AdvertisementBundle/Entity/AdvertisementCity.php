<?php
namespace VisiDarbi\AdvertisementBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @Gedmo\TranslationEntity(class="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation")
 */
class AdvertisementCity extends \VisiDarbi\CommonBundle\Entity\Translated
{
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @Gedmo\Translatable 
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;
    
    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mark_on_frontend;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", nullable=true)
     */
    private $keywords;
    
     /** 
     * @ORM\OneToMany(targetEntity="VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace", mappedBy="advertisementCity")
     */
    private $externalPlace;

    /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry", 
     *     inversedBy="AdvertisementCities"
     * )
     * @ORM\JoinColumn(name="advertisement_country_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $AdvertisementCountry;

    /** 
     * @ORM\OneToMany(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", 
     *     mappedBy="AdvertisementCity"
     * )
     */
    private $Advertisements;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();        
        $this->externalPlace = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Advertisements = new \Doctrine\Common\Collections\ArrayCollection();        
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return AdvertisementCountry
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    

    /**
     * Add externalPlace
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace
     * @return AdvertisementCity
     */
    public function addExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace)
    {
        $this->externalPlace[] = $externalPlace;
    
        return $this;
    }

    /**
     * Remove externalPlace
     *
     * @param \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace
     */
    public function removeExternalPlace(\VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalPlace $externalPlace)
    {
        $this->externalPlace->removeElement($externalPlace);
    }

    /**
     * Get externalPlace
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExternalPlace()
    {
        return $this->externalPlace;
    }

    /**
     * Set AdvertisementCountry
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry
     * @return AdvertisementCity
     */
    public function setAdvertisementCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $advertisementCountry)
    {
        $this->AdvertisementCountry = $advertisementCountry;
    
        return $this;
    }

    /**
     * Get AdvertisementCountry
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry 
     */
    public function getAdvertisementCountry()
    {
        return $this->AdvertisementCountry;
    }
    
    
    public function addTranslation(AdvertisementCityTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }    

    /**
     * Remove translations
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation $translations
     */
    public function removeTranslation(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }   
    
    /**
     * Add Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     * @return AdvertisementCountry
     */
    public function addAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->Advertisements[] = $advertisements;
    
        return $this;
    }

    /**
     * Remove Advertisements
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements
     */
    public function removeAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisements)
    {
        $this->Advertisements->removeElement($advertisements);
    }

    /**
     * Get Advertisements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdvertisements()
    {
        return $this->Advertisements;
    }    
    
    /**
     * Returns mark on frontend
     * @return boolean
     */
    public function getMarkOnFrontend() {
        return $this->mark_on_frontend;
    }

    /**
     * Set mark on frontend option
     * @param boolean $markOnFrontend
     * @return \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity
     */
    public function setMarkOnFrontend($markOnFrontend) {
        $this->mark_on_frontend = $markOnFrontend;
        return $this;
    }    
    
    public function __toString() {
        return (string) $this->getName();
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return AdvertisementCity
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    
        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }
}