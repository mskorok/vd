<?php
namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 * @ORM\Table(name="company_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class CompanyTranslation extends AbstractPersonalTranslation
{

    private $object_id;

    /**
     * Convinient constructor
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($locale = '', $field = '', $value = '')
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Company", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
    }

?>
