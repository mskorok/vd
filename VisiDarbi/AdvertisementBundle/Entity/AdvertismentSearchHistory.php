<?php

namespace VisiDarbi\AdvertisementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AdvertismentSearchHistory
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AdvertismentSearchHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
        
    /**
     * @ORM\ManyToOne(targetEntity="VisiDarbi\UserBundle\Entity\User", inversedBy="advertismentSearchHistories") 
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */	
    private $user;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profession_keyword;

    /**
     * @var string
     * 
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"profession_keyword"})
     * 
    */
    private $profession_slug; 
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $place_keyword;

    /**
     * @var string
     * 
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"place_keyword"})
     * 
    */
   private $place_slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="search_count", type="integer")
     */
    private $search_count;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="last_searched_at", type="datetime")
     */
    private $last_searched_at;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;
   


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set search_count
     *
     * @param integer $searchCount
     * @return AdvertismentSearchHistory
     */
    public function setSearchCount($searchCount)
    {
        $this->search_count = $searchCount;
    
        return $this;
    }

    /**
     * Get search_count
     *
     * @return integer 
     */
    public function getSearchCount()
    {
        return $this->search_count;
    }

    /**
     * Set last_searched_at
     *
     * @param \DateTime $lastSearchedAt
     * @return AdvertismentSearchHistory
     */
    public function setLastSearchedAt($lastSearchedAt)
    {
        $this->last_searched_at = $lastSearchedAt;
    
        return $this;
    }

    /**
     * Get last_searched_at
     *
     * @return \DateTime 
     */
    public function getLastSearchedAt()
    {
        return $this->last_searched_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return AdvertismentSearchHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set user
     *
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @return AdvertismentSearchHistory
     */
    public function setUser(\VisiDarbi\UserBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \VisiDarbi\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set profession_keyword
     *
     * @param string $professionKeyword
     * @return AdvertismentSearchHistory
     */
    public function setProfessionKeyword($professionKeyword)
    {
        $this->profession_keyword = $professionKeyword;
    
        return $this;
    }

    /**
     * Get profession_keyword
     *
     * @return string 
     */
    public function getProfessionKeyword()
    {
        return $this->profession_keyword;
    }


    /**
     * Set profession_slug
     *
     * @param string $profession_slug
     * @return AdvertismentSearchHistory
     */
    public function setProfessionSlug($profession_slug)
    {
        $this->profession_slug = $profession_slug;
    
        return $this;
    }

    /**
     * Get profession_slug
     *
     * @return string 
     */
    public function getProfessionSlug()
    {
        return $this->profession_slug;
    }

    /**
     * Set place_keyword
     *
     * @param string $placeKeyword
     * @return AdvertismentSearchHistory
     */
    public function setPlaceKeyword($placeKeyword)
    {
        $this->place_keyword = $placeKeyword;
    
        return $this;
    }

    /**
     * Get place_keyword
     *
     * @return string 
     */
    public function getPlaceKeyword()
    {
        return $this->place_keyword;
    }


    /**
     * Set place_slug
     *
     * @param string $placeKeyword
     * @return AdvertismentSearchHistory
     */
    public function setPlaceSlug($place_slug)
    {
        $this->place_slug = $place_slug;
    
        return $this;
    }

    /**
     * Get place_slug
     *
     * @return string 
     */
    public function getPlaceSlug()
    {
        return $this->place_slug;
    }


    
    public function getKeywordString()
    {
        if (strlen($this->place_keyword) > 0 && strlen($this->profession_keyword) > 0) {
            return sprintf('%s, %s', $this->profession_keyword, $this->place_keyword);
        }
        
        if (strlen($this->profession_keyword) > 0) {
            return $this->profession_keyword;
        }
        
        if (strlen($this->place_keyword) > 0) {
            return $this->place_keyword;
        }
        
        return null;
    }
}