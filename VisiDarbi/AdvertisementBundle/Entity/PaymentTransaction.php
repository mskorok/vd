<?php
namespace VisiDarbi\AdvertisementBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping AS ORM;

/** 
 * @ORM\Entity(repositoryClass="VisiDarbi\AdvertisementBundle\EntityRepository\AdvertisementEntityRepository")
 */
class PaymentTransaction
{
    const RETURN_STATUS_SUCCESS = 20;
    const RETURN_STATUS_PENDING = 10;
    const RETURN_STATUS_REGECTED = 30;        
    
    const PYMENT_DNB = 10;
    const PYMENT_NORDEA = 20;
    const PYMENT_SEB = 30;
    const PYMENT_SWEDBANK = 40;
    const PYMENT_PAYPAL = 50;
     
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;

    /** 
     * @ORM\Column(type="text", nullable=true)
     */
    private $request;

    /** 
     * @ORM\Column(type="text", nullable=true)
     */
    private $response;

    /** 
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;
    
    /** 
     * @ORM\Column(type="string", nullable=true)
     */
    private $service_id;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;    

    /** 
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_method;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $payment_currency;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $merged_advertisements;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $merged_advertisements_mobile;

     /** 
     * @ORM\ManyToOne(
     *     targetEntity="VisiDarbi\AdvertisementBundle\Entity\Advertisement", 
     *     inversedBy="paymentTransactions"
     * )
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=true)
     */
    private $advertisement;

    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\PaidPeriod", inversedBy="paymentTransactions")
     * @ORM\JoinColumn(name="paid_period_id", referencedColumnName="id", unique=true,  nullable=true)
     */
    private $paidPeriod;    
    
    
    
    /** 
     * @ORM\OneToOne(targetEntity="VisiDarbi\AdvertisementBundle\Entity\Requisites", inversedBy="paymentTransaction")
     * @ORM\JoinColumn(name="requisites_id", referencedColumnName="id", unique=true,  nullable=true)
     */
    private $requisites;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $is_notification_sent;


    /**
     * Constructor
     */
    public function __construct() {
        $this->is_notification_sent = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return PaymentTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set request
     *
     * @param string $request
     * @return PaymentTransaction
     */
    public function setRequest($request)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return string 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set response
     *
     * @param string $response
     * @return PaymentTransaction
     */
    public function setResponse($response)
    {
        $this->response = $response;
    
        return $this;
    }

    /**
     * Get response
     *
     * @return string 
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PaymentTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set advertisement
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement
     * @return PaymentTransaction
     */
    public function setAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement)
    {
        $this->advertisement = $advertisement;
    
        return $this;
    }

    /**
     * Get advertisement
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement 
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    /**
     * Set paidPeriod
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriod
     * @return PaymentTransaction
     */
    public function setPaidPeriod(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $paidPeriod = null)
    {
        $this->paidPeriod = $paidPeriod;
    
        return $this;
    }

    /**
     * Get paidPeriod
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\PaidPeriod 
     */
    public function getPaidPeriod()
    {
        return $this->paidPeriod;
    }
    
    
    
    /**
     * Set Requisites
     *
     * @param \VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites
     * @return PaymentTransaction
     */
    public function setRequisites(\VisiDarbi\AdvertisementBundle\Entity\Requisites $requisites = null)
    {
        
        if($requisites) {
            $requisites->setPaymentTransaction($this);
            $this->requisites = $requisites;
        }
        
        return $this;
    }

    /**
     * Get Requisites
     *
     * @return \VisiDarbi\AdvertisementBundle\Entity\Requisites
     */
    public function getRequisites()
    {
        return $this->requisites;
    }    
    
    
    
    public function getServiceId() {
        return $this->service_id;
    }

    public function setServiceId($serviceId) {
        $this->service_id = $serviceId;
        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    /**
     * @param $description string
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
        return $this;
    }

    public function getMergedAdvertisements() {
        return unserialize($this->merged_advertisements);
    }

    public function setMergedAdvertisements(array $merged_advertisements) {
        $this->merged_advertisements = serialize($merged_advertisements);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMergedAdvertisementsMobile()
    {
        return unserialize($this->merged_advertisements_mobile);
    }

    /**
     * @param mixed $merged_advertisements_mobile
     */
    public function setMergedAdvertisementsMobile($merged_advertisements_mobile)
    {
        $this->merged_advertisements_mobile = serialize($merged_advertisements_mobile);
    }



    public function getPaymentMethod() {
        return $this->payment_method;
    }

    public function setPaymentMethod($payment_method) {
        $this->payment_method = $payment_method;
        return $this;
    }

    public function getPaymentCurrency() {
        return $this->payment_currency;
    }

    public function setPaymentCurrency($payment_currency) {
        $this->payment_currency = $payment_currency;
        return $this;
    }
    
    public function getTransactionSubject() {
        if($this->getAdvertisement()) {
            $subject =  $this->getAdvertisement();
        } else {        
            $subject =  $this->getPaidPeriod();
        }
        
        return $subject;
        
    }

    /**
     * check if notification email to backoffice sent
     * @return boolean
     */
    public function getIsNotificationSent() {
        return $this->is_notification_sent;
    }

    /**
     * Set is_notification_sent
     *
     * @param boolean $isNotificationSent
     * @return PaymentTransaction
     */
    public function setIsNotificationSent($isNotificationSent)
    {
        $this->is_notification_sent = $isNotificationSent;

        return $this;
    }

}