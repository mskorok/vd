<?php

namespace VisiDarbi\AdvertisementBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\JsonResponse;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use Doctrine\Common\Collections\ArrayCollection;
use VisiDarbi\CommonBundle\Controller\FrontendController;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep1Type;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep2Type;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStepFormatType;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep3Type;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep4Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use VisiDarbi\ProfessionBundle\Entity\Category as Category;
use VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementType;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Company;
use VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Duty;
use VisiDarbi\AdvertisementBundle\Entity\DutyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Offer;
use VisiDarbi\AdvertisementBundle\Entity\OfferTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Requirement;
use VisiDarbi\AdvertisementBundle\Entity\Requisites;
use VisiDarbi\AdvertisementBundle\Entity\RequirementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\ContactPerson;
use VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation;
use VisiDarbi\ProfessionBundle\Entity\Profession as Profession;
use VisiDarbi\ProfessionBundle\Entity\CategoryTranslation;
use VisiDarbi\UserBundle\Entity\LegalProfile;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\AdvertisementBundle\Entity\PaidPeriod;
use Symfony\Component\HttpFoundation\RedirectResponse;
use VisiDarbi\CommonBundle\Lib\UploadHandler;
use VisiDarbi\AdvertisementBundle\Form\ShareAdvertisementType;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity;

/**
 * AdvertisementController - advertisements front end controller
 *
 * @author Aleksey
 */
class AddAdvertisementController extends FrontendController {

    protected $adRepository;
    protected $currentCountry;
    protected $currentUser = null;
    protected $allowAddAdvertisement = false;
    protected $em;
    protected $session;
    protected $lastTransaction = null;

    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     * @DI\Inject("fos_user.user_manager")
     */
    protected $userManager;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     * @DI\Inject("fos_user.util.token_generator")
     */
    protected $tokenGenerator;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;

    /**
     * @var \VisiDarbi\AdvertisementBundle\Manager\AdvertisementManagerInterface
     * @DI\Inject("visidarbi.advertisement_manager")
     */
    protected $advertisementManager;

    private $secondCurrencyRate = null;
    private $secondCurrencySign = null;
    private $secondCurrency = null;

    public function preExecute() {


        $this->secondCurrencyRate = 1;
        $this->secondCurrencySign = null;
        $this->secondCurrency = $this->getCurrentCountry()->getSecondaryCurrencyByCode('EUR');

        if ($this->secondCurrency) {
            $this->secondCurrencyRate = $this->secondCurrency->getRateToPrimaryCurrency();
            $this->secondCurrencySign = $this->secondCurrency->getCurrency();
        }


        $this->adRepository = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement');

        $this->currentCountry = $this->getCurrentCountry();

        $this->currentUser = $this->get('security.context')->getToken()->getUser();

        //Disable save for legal person
        if ($this->currentUser instanceof \VisiDarbi\UserBundle\Entity\User) {
            $this->allowAddAdvertisement = (bool) $this->currentUser->isLegal();
        } else {
            $this->currentUser = null;
        }


        $this->em = $this->getDoctrine()->getEntityManager();
        $this->session = $this->get('session');
    }

    public function indexAction(Request $request) {
        $response = $this->forward('VisiDarbiAdvertisementBundle:Advertisement:list', array());

        return $response;
    }

    private function getPaidPeriod($id, $user)
    {
        $query = $this->em->createQueryBuilder('pp')
            ->select('pp', 't')
            ->from('VisiDarbi\AdvertisementBundle\Entity\PaidPeriod', 'pp')
            ->join('pp.advertisementType', 't')
            ->where('pp.user = :user')
            ->andWhere('pp.id = :id')
            ->setParameter('user', $user->getId())
            ->setParameter('id', $id);

        $query = $query->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    public function step1Action(Request $request) {

        $this->checkCurrentUserAccess();

        $this->session->set('step5', null);
        $this->session->remove('previewAdvertisementData');
        $this->session->remove('areNewFilesUploaded');

        $query = $this->em->createQueryBuilder('t')
                ->select('t.id, t.name, t.unlimited, p.price, p.description, p.payment_description, p.payment_description_first_page, p.payment_description_higlighting, p.payment_description_mobile_highlighting, p.payment_description_period ,  p.most_popular, t.days_period')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementType', 't')
                ->join('t.advertisingTypePrices', 'p')
                ->andWhere("p.country = :country")
                ->setParameter('country', $this->getCurrentCountry()->getId())
                ->orderBy('t.name');


        $query = $query->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);


        $types = $query->getResult();
        $items = array();

        foreach ($types as $type) {
            $item = array();
            $form = $this->createForm(new AddAdvertisementStep1Type(), array('advertisement_type' => $type['id']));
            $item['form'] = $form->createView();
            $item['formObj'] = $form;
            $item['type'] = $type;
            $item['secondPrice'] = number_format(($type['price'] / $this->secondCurrencyRate), 2);
            $items[$type['id']] = $item;
        }

        $usePaidPeriodId = $request->get('usePaidPeriodId');

        if(!$usePaidPeriodId && $this->currentUser !== null) {
            $usePaidPeriod = $this->getCurrentActiveUserPeriod($this->currentUser);

            if($usePaidPeriod instanceof PaidPeriod) {
                $usePaidPeriodId = $usePaidPeriod->getId();
            }

        }

        if (! empty($usePaidPeriodId) && $this->currentUser != null && ($pp = $this->getPaidPeriod($usePaidPeriodId, $this->currentUser)) != null) {
            $this->session->set('step1', array(
                    'valid' => 'true',
                    'form' => array( 'advertisement_type' => (string)$pp->getAdvertisementType()->getId() ),
                    'type' => $items[$pp->getAdvertisementType()->getId()]['type']
                )
            );
            return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step2'));
        }

        if ($request->isMethod('POST') && $request->get('addadvertisementstep1', null)) {

            $postData = $request->get('addadvertisementstep1', array());

            if (isset($postData['advertisement_type'])) {

                $form = $items[$postData['advertisement_type']]['formObj'];
                $form->bindRequest($request);
                if ($form->isValid()) {
                    $data = $form->getData();
                    $this->session->set('step1', array('valid' => 'true', 'form' => $data, 'type' => $items[$postData['advertisement_type']]['type']));


                    //If selected unlimited period - redirect to payment
                    if( $postData['advertisement_type'] == AdvertisementType::TYPE_UNLIMITED_14_DAY ||
                        $postData['advertisement_type'] == AdvertisementType::TYPE_UNLIMITED_7_DAY ) {

                        $this->session->set('payNow', true);
                        $this->session->set('step2', array('valid' => true, 'form' => $form->getData()));
                        return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step4'));
                    }

                    return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step2'));
                }
            }
        }

        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step1.html.twig', array(
                    'step' => 1,
                    'currency' => $this->getCurrentCountry()->getPrimaryCurrency()->getCode(),
                    'secondCurrencySign' => $this->secondCurrencySign,
                    'secondCurrency' => $this->secondCurrency,
                    'items' => $items,
                    'user' => $this->currentUser,
        ));
    }

    /**
     * Step 2
     * @return Response instance
     */
    public function step2Action(Request $request) {

        $this->checkCurrentUserAccess();


        $result = $this->validateStep('step2');
        if ($result !== true) {
            return $result;
        }

        $selectedCategory = array();
        $selectedProfession = array();
        $selectedCountry = null;
        $selectedCity = null;

        $categories =
                $this->adRepository
                ->getCategoriesListForAddFrom($this->getCurrentCountry(), $this->getLocale())
                ->getResult();

        $categoriesSet = array();

        foreach ($categories as $item) {
            $categoriesSet[$item['id']] = $item['name'];
        }

        $countries = $this->adRepository
                ->getAdvertisementCountriesForAddForm($this->getCurrentCountry(), $this->getLocale());

        $countriesSet = array();

        foreach ($countries as $item) {
            $countriesSet[$item['id']] = $item['name'];
        }

        $initData = array(
            'country' => $this->getCountryManager()->getCurrentCountry()->getId(),
            'country_data' => $this->getCountryManager()->getCurrentCountry()->getId());

        $form = $this->createForm(new AddAdvertisementStep2Type($categoriesSet, $countriesSet, $this->getLocale(), $this->em), $initData);

        if ($this->session->get('step2')) {
            $data = $this->session->get('step2');

            $form->setData($data['form']);
        }

        if ($request->isMethod('POST')) {

            $postData = $request->get($form->getName());

            if ($postData['pay_now'] == 1) {
                $this->session->set('payNow', true);
                $this->session->set('step2', array('valid' => true, 'form' => $form->getData()));
                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step4'));
            } else {
                $this->session->set('payNow', null);
            }

            $form->bindRequest($request);

            if ($form->isValid()) {
                $this->session->set('step2', array('valid' => true, 'form' => $form->getData()));
                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step_format'));
            } else {
                $this->session->set('step2', array('valid' => false, 'form' => $form->getData()));
            }


            $data['form'] = $form->getData();
            $selectedCategory = $data['form']['category'];
            $selectedProfession = $data['form']['profession'];
            $selectedCountry = $data['form']['country'];
            $selectedCity = $data['form']['city'];
        } else {

            $data = $this->session->get('step2');

            $selectedCategory = array();
            $selectedProfession = array();
            $selectedCountry = null;
            $selectedCity = null;

            if (!empty($data['form']['category']) && is_array($data['form']['category'])) {
                $selectedCategory = $data['form']['category'];
            }

            if (!empty($data['form']['profession']) && is_array($data['form']['profession'])) {
                $selectedProfession = $data['form']['profession'];
            }

            if (!empty($data['form']['country'])) {
                $selectedCountry = $data['form']['country'];
            }

            if (!empty($data['form']['city'])) {
                $selectedCity = $data['form']['city'];
            }
        }


        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step2.html.twig', array(
                    'form' => $form->createView(),
                    'step' => 2,
                    'user' => $this->currentUser,
                    'selectedCategory' => '"' . join('","', $selectedCategory) . '"',
                    'selectedProfession' => '"' . join('","', $selectedProfession) . '"',
                    'selectedCountry' => $selectedCountry,
                    'selectedCity' => $selectedCity,
        ));
    }

    /**
     * Step 2.5 choose format
     * @return Response instance
     */
    public function stepFormatAction(Request $request) {
        $this->checkCurrentUserAccess();

        $result = $this->validateStep('step_format');
        if ($result !== true) {
            return $result;
        }

        $initData = array();
        if ($this->session->get('step_format')) {
            $data = $this->session->get('step_format');
            $initData = !empty($data['form']) ? $data['form'] : array();
        }
        $upload_settings = $this->container->getParameter('upload_settings');
        $initData['tooltips_dir'] = '/'.$upload_settings['stored_path'].'/';

        $form = $this->createForm(new AddAdvertisementStepFormatType($this->get('translator')), $initData);

        if ($request->isMethod('POST')) {
            $form->bindRequest($request);
            if ($form->isValid()) {
                $this->session->set('step_format', array('valid' => true, 'form' => $form->getData()));
                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step3'));
            } else {
                $this->session->set('step_format', array('valid' => false, 'form' => $form->getData()));
            }
        }

        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step_format.html.twig', array(
            'form' => $form->createView(),
            'step' => 3,
            'user' => $this->currentUser,
        ));
    }

    public function step3Action(Request $request) {

        $this->checkCurrentUserAccess();

        $result = $this->validateStep('step3');
        if ($result !== true) {
            return $result;
        }

        $data = array();

        $previousContactPersonDataFound = false;

        //Get data from last AD
        if ($this->currentUser) {

            $lastAdvertisement = $this->getUserLastAdvertisement();

            if ($lastAdvertisement) {

                /*
                $data['company']['name'] = $lastAdvertisement->getCompanies()->getName();
                $data['company']['registration_number'] = $lastAdvertisement->getCompanies()->getRegistrationNumber();
                */

                if ($lastAdvertisement->getCompanies()->getLogo()) {

                    if (!$this->session->get('previousLogoCheck', null)) {
                        $uploadConfig = $this->container->getParameter('upload_logo');
                        $tmpDir = __DIR__ . '/../../../../web/' . $uploadConfig['tmp_path'];

                        $parts = explode('.', $lastAdvertisement->getCompanies()->getLogo());
                        $existentExtension = end($parts);
                        $fileName = md5(uniqid('', true));
                        $uploadedResizedLogo = $tmpDir . '/' . $fileName . '_logo';
                        $existentLogo = $lastAdvertisement->getCompanies()->getFullLogoPath();
                        copy($existentLogo, $tmpDir . '/' . $fileName . '.' . $existentExtension);
                        $existentLogo = $tmpDir . '/' . $fileName . '.' . $existentExtension;

                        try {
                            $img = $this->get('image.handling')->open($existentLogo);
                            $imgType = $img->guessType();
                            $img->resize(100, 100)
                                    ->save($uploadedResizedLogo . '.' . $imgType, $imgType);

                            $webPath = $uploadConfig['tmp_path'] . '/' . $fileName . '_logo.' . $imgType;

                            $this->session->set('logo', array('filename' => $fileName . '.' . $imgType, 'logo' => $webPath));
                        } catch (\Exception $e) {
                            $this->session->set('logo', null);
                        }

                        $this->session->set('previousLogoCheck', 1);
                    }
                }

                $companyTranslations = $lastAdvertisement->getCompanies()->getTranslations();
                $contactPerson = $lastAdvertisement->getContactPerson();
                $contactPersonTranslations = $lastAdvertisement->getContactPerson()->getTranslations();

                foreach ($this->getLocales() as $l) {
                    foreach ($companyTranslations as $t) {
                        if ($t->getLocale() == $l && $t->getField() == 'description') {
                            $data['company']['description_' . $l] = $t->getContent();
                        }
                    }
                }

                $data['contact_person']['name'] = $contactPerson->getName();
                $data['contact_person']['email'] = $contactPerson->getEmail();
                $data['contact_person']['phone'] = $contactPerson->getPhone();
                $data['contact_person']['fax'] = $contactPerson->getFax();
                $data['contact_person']['url'] = $contactPerson->getUrl();
                $data['contact_person']['actual_address'] = $contactPerson->getActualAddress();
                $data['contact_person']['legal_address'] = $contactPerson->getLegalAddress();

                foreach ($this->getLocales() as $l) {
                    foreach ($contactPersonTranslations as $t) {
                        if ($t->getLocale() == $l && $t->getField() == 'position') {
                            $data['contact_person']['position_' . $l] = $t->getContent();
                        }
                    }
                }
            }

            /**
             * if no last adv data => taking data from profile
             */
            if ($this->currentUser->isLegal()){

                $legalProfile = $this->currentUser->getLegalProfile();

                $data['company']['name'] = $legalProfile->getCompanyName();
                $data['company']['registration_number'] = $legalProfile->getRegistrationNumber();
            }

        }

        //Make empty duties, requirements and offers

        foreach ($this->getLocales() as $locale) {
            $data['duty_' . $locale] = array('');
            $data['offer_' . $locale] = array('');
            $data['requirement_' . $locale] = array('');
        }

        $step1Data = $this->session->get('step1');
        $step2Data = $this->session->get('step2');

        //Get profession from step 2


        if (!empty($step2Data['form']['profession'][0])) {


            $query =
                    $this->getDoctrine()
                    ->getEntityManager()
                    ->createQueryBuilder('p')
                    ->select('p')
                    ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                    ->join('p.translations', 't')
                    ->andWhere("t.locale = :locale")
                    ->andWhere("t.slug = :slug")
                    ->groupBy('t.slug')
                    ->setMaxResults(1)
                    ->setParameter('slug', $step2Data['form']['profession'][0])
                    ->setParameter('locale', $this->getLocale());
            $query = $query->getQuery()->getResult();


            $profession = $query[0];

            $professionTranslations = $profession->getTranslations();

            foreach ($this->getLocales() as $l) {
                foreach ($professionTranslations as $t) {
                    if ($t->getLocale() == $l && $t->getField() == 'name') {
                        $data['title_' . $l] = $t->getContent();
                    }
                }
            }
        }
        //Default date data
        $now = new \DateTime();
        $data['date_from'] = $now->format('Y-m-d');
        $data['date_to'] = clone $now;
        $data['date_to']->add(new \DateInterval('P' . ($step1Data['type']['days_period'] - 1) . 'D'));
        $data['date_to'] = $data['date_to']->format('Y-m-d');

        $initDateFrom = $data['date_from'];
        $initDateTo = $data['date_to'];

        $stored = array();
        if ($this->session->get('step3')) {
            $stored = $this->session->get('step3');

            /**
             * @todo check is it correct in aby cases
             */
            $stored['form']['date_from'] = $data['date_from'];
            $data = array_merge($stored['form'], $data);
        }

        if (array_key_exists('contact_person', $data) && trim($data['contact_person']['actual_address']) != "") {
            $previousContactPersonDataFound = true;
        }

        if ($this->session->get('format_files')) {
            $data['format_files'] = $this->session->get('format_files');
            if (!empty($data['format_files']) && is_array($data['format_files'])) {
                foreach ($data['format_files'] as $l => $row) {
                    if (!empty($row['html_image']) && is_array($row['html_image'])) {
                        foreach ($row['html_image'] as $k => $val) {
                            $data['html_image_'.$l][$k] = $val['fileNameOrig'];
                        }
                    }
                }
            }
        } else {
            $data['format_files'] = array();
        }
        $stepFormatData = $this->session->get('step_format');
        $data['format'] = Advertisement::getValidFormat(!empty($stepFormatData['form']['format']) ? $stepFormatData['form']['format'] : null);

        $form = $this->createForm(new AddAdvertisementStep3Type($this->getLocales(), $this->currentCountry->getDefaultLocale(), $this->container->get('exercise_html_purifier.default'),$this->get('translator')), $data);

        //$goodToKnowHelper = $this->get('visidarbi.settings.manager')->get('site.good_to_know_helper');

        if ($request->isMethod('POST')) {

            $postData = $request->get($form->getName());

            if ($postData['deleted_logo'] == 1) {
                $this->session->set('logo', null);
            }

            $this->session->set('format_files', $this->advertisementManager->checkFileFormat($data['format_files'], $postData));

            $defaultLocale = $this->getCurrentCountry()->getDefaultLocale();
            $locales = $this->getCurrentCountry()->getLocales();

            $fields = array('duty_', 'offer_', 'requirement_');

            foreach ($fields as $f) {
                if (!isset($postData[$f . $defaultLocale])) {
                    continue;
                }
                $fieldData = $postData[$f . $defaultLocale];
                $fieldCount = count($fieldData);
                if (isset($fieldData[$fieldCount - 1]) && empty($fieldData[$fieldCount - 1])) {

                    $unset = true;
                    foreach ($locales as $l) {
                        if ($l == $defaultLocale) {
                            continue;
                        }

                        if (!empty($postData[$f . $l][$fieldCount - 1])) {
                            $unset = false;
                            break;
                        }
                    }

                    if ($unset) {
                        foreach ($locales as $l) {
                            unset($postData[$f . $l][$fieldCount - 1]);
                        }
                    }
                }
            }
            $request->request->set($form->getName(), $postData);



            $form->bindRequest($request);

            if ($form->isValid()) {

                $testDateFrom = strtotime($postData['date_from']);
                $testDateTo = strtotime($postData['date_to']);

                $compareDateFrom = strtotime($initDateFrom);
                $cpmpareDateTo = strtotime($initDateTo);

                if ($testDateFrom < $compareDateFrom || $testDateTo > $cpmpareDateTo) {

                    $error = new \Symfony\Component\Form\FormError('Incorrect date period');
                    $form->get('date_1')->addError($error);
                    $this->session->set('step3', array('valid' => false, 'form' => $form->getData()));
                } else {

                    $this->session->set('step3', array('valid' => true, 'form' => $form->getData()));
                    $this->session->remove('previewAdvertisementData');
                    return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step4'));
                }
            } else {
                $this->session->set('step3', array('valid' => false, 'form' => $form->getData()));
            }
        }

        $logo = $this->session->get('logo', null);

        if (empty($logo['logo']) || !file_exists(__DIR__ . '/../../../../web/' . $logo['logo'])) {
            $logo = null;
        }

        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step3.html.twig', array(
                    'form' => $form->createView(),
                    'step' => 4,
                    'locales' => $this->getLocales(),
                    'defaultLocale' => $this->getCurrentCountry()->getDefaultLocale(),
                    'advertisementManager' => $this->advertisementManager,
                    'previousContactPersonDataFound' => $previousContactPersonDataFound,
                    //'goodToKnow' => $goodToKnowHelper,
                    'logo' => $logo
        ));
    }

    public function step4Action(Request $request) {

        $this->checkCurrentUserAccess();

        $addPeriod = (bool) $this->session->get('payNow');
        $step3Data = $this->session->get('step3');

        $user = $this->get('security.context')->getToken()->getUser();

        if (!($user instanceof \VisiDarbi\UserBundle\Entity\User)) {
            $user = null;
        }

        $result = $this->validateStep('step4');
        if ($result !== true) {
            return $result;
        }

        $step1Data = $this->session->get('step1');
        //Default date data
        $data = array();
        $data['priceFirstPage'] = (float)$this->get('visidarbi.settings.manager')
            ->get('prices.price_for_first_page');
        $data['priceFirstPageSecond'] = (float)$this->get('visidarbi.settings.manager')
                ->get('prices.price_for_first_page') / $this->secondCurrencyRate;
        $data['priceHighLightedMobile'] = (float)$this->get('visidarbi.settings.manager')
            ->get('prices.price_for_mobile_advert_highlighting');
        $data['priceHighLightedMobileSecond'] = (float)$this->get('visidarbi.settings.manager')
                ->get('prices.price_for_mobile_advert_highlighting') / $this->secondCurrencyRate;

        $data['advertisementTypePrice'] = $step1Data['type']['price'];

        $finalPrice = $step1Data['type']['price'];
        $finalPriceFirstPage = $step1Data['type']['price'] + $data['priceFirstPage'];

        $finalPriceHighLightedMobile = (float) $step1Data['type']['price'] + (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_mobile_advert_highlighting');
        $finalPriceFirstPageHighLightedMobile = $finalPriceHighLightedMobile + ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page'));




        $finalPriceSecond = $step1Data['type']['price'] / $this->secondCurrencyRate;
        $finalPriceFirstPageSecond = $finalPriceSecond + $data['priceFirstPageSecond'];

        $finalPriceHighLightedMobileSecond = ((float) $this->get('visidarbi.settings.manager')
                ->get('prices.price_for_mobile_advert_highlighting') )/ $this->secondCurrencyRate +
                ((float) $step1Data['type']['price']) / $this->secondCurrencyRate;
        $finalPriceFirstPageHighLightedMobileSecond = $finalPriceHighLightedMobileSecond + ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page')) / $this->secondCurrencyRate;



        $data['priceFirstPage'] = number_format($data['priceFirstPage'], 2);
        $data['priceFirstPageSecond'] = number_format($data['priceFirstPageSecond'], 2);
        $data['priceHighLightedMobile'] = number_format($data['priceHighLightedMobile'], 2);
        $data['priceHighLightedMobileSecond'] = number_format($data['priceHighLightedMobileSecond'], 2);

        $data['companyName'] = $step3Data['form']['company']['name'];
        $data['address'] = $step3Data['form']['contact_person']['legal_address'];
        $data['email'] = $step3Data['form']['contact_person']['email'];



        if ($this->session->get('step4')) {
            $stored = $this->session->get('step4');

            /**
             * @todo check is it correct in any cases
             */
            $data = $data + $stored['form'];
        }


        if ($user) {
            $data = $data + array('email' => $user->getEmail());
        }

        //Now check if user has active paid period
        //check is user logged in
        //added isset($step1Data['type']['unlimited'], cause of copy action doesn't need that param.


        if ($this->currentUser && $this->allowAddAdvertisement /*&& isset($step1Data['type']['unlimited']) */)
        {
//            && (
//                ! isset($step1Data['type']['unlimited'])
//                || ( isset($step1Data['type']['unlimited']) && $step1Data['type']['unlimited']) === true
//                )
//            ) {

            $period = $this->getCurrentActiveUserPeriod($user);

            if ($period && !$addPeriod) {

                $advertisement = $this->addAdvertisement($period);

                //$transaction = $this->createTransactionForAdvertisement($advertisement);

                if (!$period->getAdvertisementType()->getUnlimited()) {
                    $period->setStatus(PaidPeriod::STATUS_CLOSED);
                    $this->em->persist($period);
                }

                $this->em->flush();


                $this->session->set('lastAdvertisementId', $advertisement->getId());
                $this->session->set('advertisementByPeriod', $period->getId());
                $this->resetStepsBeforeSuccess();

                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step5'));
            }
        }



        $withVatValidation = true;

        if ($request->isMethod('POST')) {
            $f = new AddAdvertisementStep4Type();
            $postDate = $request->get($f->getName());
            if (isset($postDate['noVat']) && $postDate['noVat']) {
                $withVatValidation = false;
            } else {
                $withVatValidation = true;
            }
        }


        //Else process form for add AD

        $form = $this->createForm(new AddAdvertisementStep4Type($user, $withVatValidation), $data);

        $showPasswordField = false;

        if ($request->isMethod('POST')) {
            $form->bindRequest($request);

            $formData = $form->getData();

            if (!empty($formData['password']) && $formData['password'] != $formData['password_repeat']) {


                $error = new \Symfony\Component\Form\FormError('Password and repeat password not match');
                $form->get('password')->addError($error);
                $showPasswordField = true;
            }

            //Validate user - check that it not exists
            if (!empty($formData['password']) || !$this->currentUser) {

                $user =
                        $this->em
                        ->getRepository('VisiDarbi\UserBundle\Entity\User')
                        ->findOneBy(array('email' => $formData['email']));

                if ($user) {
                    $error = new \Symfony\Component\Form\FormError('User thith current email already exists');
                    $form->get('email')->addError($error);
                    $showPasswordField = true;
                }
            }





            if ($form->isValid()) {



                $this->session->set('step4', array('valid' => true, 'form' => $form->getData()));

                if ($addPeriod) {

                    //Add period
                    $period = $this->addPaidPeriod();

                    //Create requesits data
                    $requisites = new Requisites();
                    $requisites->getPaidPeriod($period);
                    $requisites->setAddress($formData['address']);
                    $requisites->setBankAccount($formData['bankAccount']);
                    $requisites->setBankCode($formData['bankCode']);
                    $requisites->setCompanyName($formData['companyName']);
                    $requisites->setContactPersonName($formData['contactPersonName']);
                    $requisites->setEmail($formData['email']);
                    $requisites->setNoPVN($formData['noVat']);
                    $requisites->setPVN($formData['vat']);
                    $requisites->setRegistrationNumber($formData['registrationNumber']);
                    $this->em->persist($requisites);

                    $price = $step1Data['type']['price'];

                    if($price > 0 ){

                        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');
                        $vatTotal = $price / 100 * $vatValue;
                        $priceTotal = $price + $vatTotal;

                        $price = number_format( $priceTotal, 2);
                    }

                    $transaction = $this->createTransactionForPeriod($period, $price);
                    $transaction->setRequisites($requisites);
                    $this->em->flush();

                    $this->lastTransaction = $transaction;
                    $this->session->set('lastLastTransactionId', $transaction->getId());
                    $this->session->set('lastPeriodId', $period->getId());

                    $this->resetStepsBeforeSuccess();

                    /**
                     * @todo something after create AD period and send transaction data
                     */
                    $paymentConfig = $this->container->getParameter('payment');
//                    $paymentDataArray = array('transaction_id' => $this->lastTransaction->getId(), 'amount' => $step1Data['type']['price']);

                    $currencyCode = $this->getCurrentCountry()->getPrimaryCurrency()->getCode();

                    $paymentDataArray = array(
                        'currency' => $currencyCode,
                        'amount' => $price,
                        'message' => urlencode($this->lastTransaction->getDescription()),
                        'method' => $paymentConfig['bank'][$formData['payment']],
                        'external_id' => $this->lastTransaction->getId(),
                        'url' =>  $this->generateUrl('visidarbi_advertisement_add_step5', array(), true),
                    );


                    $paramStr = array();
                    foreach ($paymentDataArray as $k => $v) {
                        $paramStr[] = $k . '=' . $v;
                    }

                    $paymentUrl = $paymentConfig['url'] . '?' . implode('&', $paramStr);

                    $this->lastTransaction->setPaymentCurrency($currencyCode);
                    $this->lastTransaction->setPaymentMethod($paymentConfig['bank'][$formData['payment']]);
                    $this->lastTransaction->setRequest($paymentUrl);
                    $this->em->flush($this->lastTransaction);

                    return new RedirectResponse($paymentUrl);
                } else {

                    //Add advertisement
                    $advertisement = $this->addAdvertisement();
                    $period = null;

                    if (! $advertisement->getAdvertisementType()->getUnlimited()) {
                        $period = $this->addPaidPeriod($advertisement);
                        $advertisement->setPaidPeriods($period);
                    }


                    $firstPagePrice = (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page');
                    $highLightedMobilePrice = (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_mobile_advert_highlighting');
                    $adPrice = $step1Data['type']['price'];


                    if($formData['isHighLightedMobile'] && $formData['onFirstPage']){
                        $price = $firstPagePrice + $adPrice + $highLightedMobilePrice;
                        $transactionDescription = $step1Data['type']['payment_description_first_page'].' '.$step1Data['type']['payment_description_mobile_highlighting'];
                    } elseif($formData['isHighLightedMobile']) {
                        $price = $highLightedMobilePrice + $adPrice;
                        $transactionDescription = $step1Data['type']['payment_description_mobile_highlighting'];
                    } elseif ($formData['onFirstPage']) {
                        $price = $firstPagePrice + $adPrice;
                        $transactionDescription = $step1Data['type']['payment_description_first_page'];
                    } else {
                        $price = $adPrice;
                        $transactionDescription = $step1Data['type']['payment_description'];
                    }

                    if($price > 0 ){

                        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');
                        $vatTotal = $price / 100 * $vatValue;
                        $priceTotal = $price + $vatTotal;

                        $price = number_format( $priceTotal, 2);
                    }

                    $transaction = $this->createTransactionForAdvertisement($advertisement, $price, $transactionDescription, $period);

                    $this->em->flush();

                    if ($advertisement->getAdvertisementType()->getUnlimited() && $period) {
                        $this->session->set('lastPeriodId', $period->getId());
                    }

                    $this->lastTransaction = $transaction;
                    $this->session->set('lastAdvertisementId', $advertisement->getId());
                    $this->session->set('lastTransactionId', $transaction->getId());
                    $this->resetStepsBeforeSuccess();


                    /**
                     * @todo something after create AD and send transaction data
                     */
                    $paymentConfig = $this->container->getParameter('payment');

                    $currencyCode = $this->getCurrentCountry()->getPrimaryCurrency()->getCode();

                    $paymentDataArray = array(
                        'currency' => $currencyCode,
                        'amount' => $price,
                        'message' => urlencode($this->lastTransaction->getDescription()),
                        'method' => $paymentConfig['bank'][$formData['payment']],
                        'external_id' => $this->lastTransaction->getId(),
                        'url' => $this->generateUrl('visidarbi_advertisement_add_step5', array(), true),
                    );

                    $paramStr = array();

                    foreach ($paymentDataArray as $k => $v) {
                        $paramStr[] = $k . '=' . $v;
                    }

                    $paymentUrl = $paymentConfig['url'] . '?' . implode('&', $paramStr);

                    $this->lastTransaction->setPaymentCurrency($currencyCode);
                    $this->lastTransaction->setPaymentMethod($paymentConfig['bank'][$formData['payment']]);
                    $this->lastTransaction->setRequest($paymentUrl);
                    $this->em->flush($this->lastTransaction);

                    return new RedirectResponse($paymentUrl);
                }
            } else {
                $this->session->set('step4', array('valid' => false, 'form' => $form->getData()));
            }
        }


        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');

        $vatTotal = $finalPrice / 100 * $vatValue;
        $vatTotalSecond = $finalPriceSecond / 100 * $vatValue;

        $vatTotalFirstPage = $finalPriceFirstPage / 100 * $vatValue;
        $vatTotalFirstPageSecond = $finalPriceFirstPageSecond / 100 * $vatValue;

        $vatTotalFirstPageHighLightedMobile = $finalPriceFirstPageHighLightedMobile / 100 * $vatValue;
        $vatTotalFirstPageHighLightedMobileSecond = $finalPriceFirstPageHighLightedMobileSecond / 100 * $vatValue;

        $vatTotalHighLightedMobile = $finalPriceHighLightedMobile / 100 * $vatValue;
        $vatTotalHighLightedMobileSecond = $finalPriceHighLightedMobileSecond / 100 * $vatValue;


        $finalPriceFirstPageWithVat = $finalPriceFirstPage + ($vatTotalFirstPage);
        $finalPriceFirstPageWithVatSecond =  $finalPriceFirstPageSecond + ($vatTotalFirstPageSecond);

        $finalPriceFirstPageHighLightedMobileWithVat = $finalPriceFirstPageHighLightedMobile + ($vatTotalFirstPageHighLightedMobile);
        $finalPriceFirstPageHighLightedMobileWithVatSecond =  $finalPriceFirstPageHighLightedMobileSecond + ($vatTotalFirstPageHighLightedMobileSecond);

        $finalPriceHighLightedMobileWithVat = $finalPriceHighLightedMobile + ($vatTotalHighLightedMobile);
        $finalPriceHighLightedMobileWithVatSecond =  $finalPriceHighLightedMobileSecond + ($vatTotalHighLightedMobileSecond);

        $finalPriceWithVat = $finalPrice + ($vatTotal);
        $finalPriceWithVatSecond = $finalPriceSecond + ($vatTotalSecond);


        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step4.html.twig', array(
                    'currency' => $this->getCurrentCountry()->getPrimaryCurrency()->getCode(),
                    'secondCurrency' => $this->secondCurrency,
                    'secondCurrencySign' => $this->secondCurrencySign,
                    'vatValue' => $vatValue,
                    'finalPrice' => number_format($finalPrice, 2),
                    'finalPriceFirstPage' =>  number_format($finalPriceFirstPage, 2),
                    'finalPriceSecond' => number_format($finalPriceSecond, 2),
                    'finalPriceFirstPageSecond' => number_format($finalPriceFirstPageSecond, 2),
                    'finalPriceFirstPageWithVat' => number_format($finalPriceFirstPageWithVat, 2),
                    'finalPriceFirstPageWithVatSecond' => number_format($finalPriceFirstPageWithVatSecond, 2),
                    'finalPriceHighLightedMobile'  => number_format($finalPriceHighLightedMobile, 2),
                    'finalPriceHighLightedMobileSecond'  => number_format($finalPriceHighLightedMobileSecond, 2),
                    'finalPriceFirstPageHighLightedMobile'  => number_format($finalPriceFirstPageHighLightedMobile, 2),
                    'finalPriceFirstPageHighLightedMobileSecond'  => number_format($finalPriceFirstPageHighLightedMobileSecond, 2),
                    'finalPriceHighLightedMobileWithVat'  => number_format($finalPriceHighLightedMobileWithVat, 2),
                    'finalPriceHighLightedMobileWithVatSecond'  => number_format($finalPriceHighLightedMobileWithVatSecond, 2),
                    'finalPriceFirstPageHighLightedMobileWithVat' => number_format($finalPriceFirstPageHighLightedMobileWithVat, 2),
                    'finalPriceFirstPageHighLightedMobileWithVatSecond' => number_format($finalPriceFirstPageHighLightedMobileWithVatSecond, 2),
                    'finalPriceWithVat' => number_format($finalPriceWithVat, 2),
                    'finalPriceWithVatSecond' => number_format($finalPriceWithVatSecond, 2),
                    'vatTotal' =>  number_format($vatTotal, 2),
                    'vatTotalSecond' =>  number_format($vatTotalSecond, 2),
                    'vatTotalFirstPage' => number_format($vatTotalFirstPage, 2),
                    'vatTotalFirstPageSecond' => number_format($vatTotalFirstPageSecond, 2),
                    'vatTotalHighLightedMobile'   => number_format($vatTotalHighLightedMobile, 2),
                    'vatTotalHighLightedMobileSecond'   => number_format($vatTotalHighLightedMobileSecond, 2),
                    'vatTotalFirstPageHighLightedMobile' => number_format($vatTotalFirstPageHighLightedMobile, 2),
                    'vatTotalFirstPageHighLightedMobileSecond' => number_format($vatTotalFirstPageHighLightedMobileSecond, 2),
                    'form' => $form->createView(),
                    'step' => 5,
                    'addPeriod' => $addPeriod,
                    'showPasswordField' => $showPasswordField,
                    'user' => $this->currentUser,
        ));
    }

    public function step5Action(Request $request) {

        $this->checkCurrentUserAccess();

        if ($request->get('AUTO', null) != 'YES') {
            if (!$this->session->get('step5')) {
                return $this->redirect($this->generateUrl('index'));
            }
        }

        $env = $this->container->get( 'kernel' )->getEnvironment();

        $lastAdvertisementId = null;
        $lastPeriodId = null;

        $returnTransactionId = $request->get('id');

        $paymentConfig = $this->container->getParameter('payment');

        $checksumParam = $request->get('checksum', null);


        $checksum = sha1("apJRW9c11h7SM3Kc,{$request->get('amount', null)},{$request->get('currency', null)},{$request->get('external_id', null)},{$request->get('payd_amount', null)}");

        if ($checksumParam != $checksum) {

            $returnStatus = null;
        } elseif (!isset($paymentConfig['status'][$request->get('status')])) {

            $returnStatus = null;
        } else {

            $returnStatus = $paymentConfig['status'][$request->get('status')];
        }

        $paymentStatus = null;
        $advertisement = null;
        $period = null;
        $lastTransactionId = $request->get('external_id', null);
        $isProfileAdded = $this->session->get('isProfileAdded', null);

        /**
         * for payment testing on dev env
         */
//        if ('dev' == $env){
//
//            $userRepository = $this->getDoctrine()
//                ->getEntityManager()
//                ->getRepository('VisiDarbi\UserBundle\Entity\User');
//
//            $lastTransactionId = $userRepository->getUserLastTransaction($this->currentUser);
//
//        }


        if (!$returnTransactionId || !$returnStatus) {
            $returnStatus = PaymentTransaction::RETURN_STATUS_REGECTED;
        }

        $advertisementByPeriod = false ;

        if ($this->session->get('advertisementByPeriod', null)) {

            $advertisementByPeriod = true;
            $returnStatus = PaymentTransaction::RETURN_STATUS_SUCCESS;

            $advertisement =
                    $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                    ->findOneById($this->session->get('lastAdvertisementId', null));


        }


        if ($lastTransactionId) {
            /**
             * @todo process lost transaction case
             */
            $transaction =
                    $this->getDoctrine()
                    ->getEntityManager()
                    ->find('\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction', $lastTransactionId);


            if (!$transaction) {

                $returnStatus = PaymentTransaction::RETURN_STATUS_REGECTED;
            } else {


                $transaction->setServiceId($returnTransactionId);
                $transaction->setStatus($returnStatus);
                $transaction->setResponse($this->getRequest()->getQueryString());

                $this->getDoctrine()
                        ->getEntityManager()->flush($transaction);

                $advertisement = $transaction->getAdvertisement();
                $period = $transaction->getPaidPeriod();


                /**
                * send payment notification to backoffice
                */
                $advertisementService = $this->get('visidarbi.advertisement.advertisement_service');

                $advertisementService->sendPaymentNotificationByTransactionEmailMessage($this->countryManager->getCurrentCountry(), $this->getRequest()->getLocale(), $transaction);

            }
        }

        if ($advertisement) {

            if ($returnStatus == PaymentTransaction::RETURN_STATUS_SUCCESS) {
                if ($advertisement->getStatus() == Advertisement::STATUS_PENDING) {
                    $advertisement->setStatus(Advertisement::STATUS_ACTIVE);
                    $advertisement->setPublishedAt(new \DateTime());
                    $this->getDoctrine()
                            ->getEntityManager()->persist($advertisement);
                }
            }
        }

        if ($period) {
            /* @var $period PaidPeriod */
            if ($returnStatus == PaymentTransaction::RETURN_STATUS_SUCCESS) {
                if ($period->getStatus() == PaidPeriod::STATUS_PENDING) {
                    $period->setStatus(PaidPeriod::STATUS_ACTIVE);
                    if (! $period->getAdvertisementType()->getUnlimited() && $advertisement) {
                        $period->setStatus(PaidPeriod::STATUS_CLOSED);
                    }
                    $this->getDoctrine()
                            ->getEntityManager()->persist($period);
                }
            }
        }

        $this->getDoctrine()
                ->getEntityManager()->flush();

        //Clear adv list cache
        $this->em->getConfiguration()->getResultCacheImpl()->deleteAll();



        //Reset session data
        $this->session->set('lastTransactionId', null);
        $this->session->set('lastAdvertisementId', null);
        $this->session->set('lastPeriodId', null);
        $this->session->set('isProfileAdded', null);
        $this->session->set('step5', null);
        $this->session->set('advertisementByPeriod', null);
        $this->session->set('advertisementUser', null);
        $this->session->set('isProfileAdded', null);
        $this->session->set('logo', null);
        $this->session->set('previousLogoCheck', null);
        $this->session->set('step_format', null);
        $this->session->set('format_files', null);

        if ($request->get('AUTO', null) != 'YES') {

            return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step5.html.twig', array('isProfileAdded' => $isProfileAdded,
                        'advertisement' => $advertisement,
                        'period' => $period,
                        'paymentStatus' => $returnStatus,
                        'step' => 6,));
        } else {

            return new Response(json_encode(true), 200, array('Content-Type' => 'application/json'));
        }
    }

    public function professionListAction(Request $request) {

        $categoryId = $request->get('category', null);

        if (!empty($categoryId)) {
            $categoryId = explode(',', $categoryId);
        }


        $professions =
                $this->adRepository
                ->getProfessionsListForAddForm($categoryId, $this->getLocale())
                ->getResult();


        return new Response(json_encode($professions), 200, array('Content-Type' => 'application/json'));
    }

    public function checkEmailAction(Request $request) {

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index'), 301 );
        }

        $result = array('r' => 0, 'l' => 0);

        $email = $request->get('val', null);

        $user =
                $this->em
                ->getRepository('VisiDarbi\UserBundle\Entity\User')
                ->findOneBy(array('email' => $email));


        if ($user) {
            $result['r'] = 1;
            $result['l'] = (int) $user->isLegal();
        }


        return new Response(json_encode($result), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Upload logo
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadAction(Request $request) {
        $this->container->get('logger')->addInfo('AddAdvertisementController.uploadAction: start upload url="'.$this->getRequest()->getUri().'"');
        $result = array();

        $format = $request->get('format', null);

        $param_name = str_replace('[]', '', $request->get('param_name', 'files'));

        $uploadConfig = $this->advertisementManager->getUploadConfig($param_name == 'files' ? 'upload_logo' : $format);
        $tmpDir = __DIR__ . '/../../../../web/' . $uploadConfig['tmp_path'].'/';

        $file_prefix = $param_name == 'files' ? '_logo' : '_'.str_replace('_files', '', $param_name);

        $this->container->get('logger')->addInfo('AddAdvertisementController.uploadAction:
            $format="'.$format.'", $param_name="'.$param_name.'", $file_prefix="'.$file_prefix.'",
            $uploadConfig="'.json_encode($uploadConfig).'"');

        $uploadHandler = new UploadHandler(array(
            'mkdir_mode' => 0775,
            'upload_dir' => $tmpDir,
            'param_name' => $param_name,
            'upload_url' => '/',
            'access_control_allow_methods' => array(
                'POST'
            ),
            'image_versions' => array(
                '' => array(
                    'jpeg_quality' => 95,
                )
            )
            ), false);

        $response = $uploadHandler->post(false);
        $fileData = $response[$param_name][0];
        $mimeType = $fileData->type;;
        $fileName = $fileData->name;
        $fileNameTemp = str_replace('.', $file_prefix.'.', $fileName);
        $this->container->get('logger')->addInfo('AddAdvertisementController.uploadAction:
            $fileData="'.json_encode($fileData).'",
            $mimeType="'.$mimeType.'",
            $fileName="'.$fileName.'",
            $fileData->size="'.$fileData->size.'"'
        );
        if (!in_array($mimeType, explode(',', $uploadConfig['allow_type']))) {
            $result['errors'] = $this->get('translator')->trans('Incorrect file type');
            $this->container->get('logger')->addInfo('AddAdvertisementController.uploadAction:error - Incorrect file type');
        }


        if ($fileData->size > $uploadConfig['max_size']) {
            $result['errors'] = $this->get('translator')
                    ->trans('Max file size must be %size%MB.', array('%size%' => $uploadConfig['max_size_view']));
            $this->container->get('logger')->addInfo('AddAdvertisementController.uploadAction:error - Max file size must be "'.($uploadConfig['max_size_view']).'"');
        }

        if (empty($result['errors'])) {
            $result = array();
            $locale = $this->getCurrentCountry()->getDefaultLocale();
            $locales = $this->getLocales();
            if (preg_match("/(.+)_(".implode('|', $locales).")_files/", $param_name, $m)) {
                $locale = $m[2];
                $field = $m[1];
            }
            $result['locale'] = $locale;
            $result['thumb'] = '';
            $result['type'] = $mimeType;
            if ($mimeType == 'application/pdf') {
                $result['type'] = 'pdf';
            } else if ($mimeType == 'text/html') {
                $result['type'] = 'html';
            } else if (preg_match('/image\/.+/', $mimeType)) {
                $result['thumb'] = $this->advertisementManager->createFileThumb($tmpDir, $fileName, $fileNameTemp, $uploadConfig);
                $result['type'] = 'image';
            }


            $result['fileTmpPath'] = $tmpDir  . $fileName;
            $result['fileName'] = $fileName;
            $result['fileNameOrig'] = $fileName;
            if (!empty($_FILES[$param_name]['name'])) {
                $result['fileNameOrig'] = current($_FILES[$param_name]['name']);
                if (!empty($uploadConfig['translitirate_file_name'])) {
                    $result['fileNameOrig'] = $this->advertisementManager->getTranslitirateFileName($result['fileNameOrig']);
                }
            }
            $result['errors'] = null;
            if ($file_prefix == '_logo') {
                $result['logo'] = $result['thumb'];
                $this->session->set('logo', array('filename' => $fileName, 'logo' => $result['thumb']));
            } else {
                $format_files = $this->session->get('format_files', array());
                $this->session->set('format_files', $this->advertisementManager->mergeFilesData($format_files, $param_name, $result));
            }

            $this->session->set('areNewFilesUploaded', true);
        }
        $this->container->get('logger')->addInfo('AddAdvertisementController.uploadAction: finish upload $result="'.json_encode($result).'"');
        return new JsonResponse($result);
    }

    public function cityListAction(Request $request) {

        $countryId = $request->get('country', null);

        $cities =
                $this->adRepository
                ->getCitiesListForAddForm($countryId, $this->getLocale())
                ->getResult();

        return new Response(json_encode($cities), 200, array('Content-Type' => 'application/json'));
    }


    /**
     * Preview an advertisement
     * Behavior #1: ad data submitted (post request) => generating preview data
     * Behavior #2: nothin submitted (get request) => show generated preview
     *
     * @param Request $request
     * @return json
     */
    public function previewAction(Request $request)
    {

        $this->checkCurrentUserAccess();

        $result = array(
            'error' => '',
        );

        $defaultLocale = $this->getCurrentCountry()->getDefaultLocale();

        $advertisement = new Advertisement();

        /**
         * Behavior #1
         * If data submitted - prepare everything for preview and save to session
         */
        if ($request->isMethod('POST')) {

//            $this->session->remove('previewAdvertisementData');

            $data = array();

            /* getting previous steps data */
            $step1Data = $this->session->get('step1');
            $step2Data = $this->session->get('step2');

            /* Get profession from step 2 */

            if (!empty($step2Data['form']['profession'][0])) {

                $query = $this->getDoctrine()
                    ->getEntityManager()
                    ->createQueryBuilder('p')
                    ->select('p')
                    ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                    ->join('p.translations', 't')
                    ->andWhere("t.locale = :locale")
                    ->andWhere("t.slug = :slug")
                    ->groupBy('t.slug')
                    ->setMaxResults(1)
                    ->setParameter('slug', $step2Data['form']['profession'][0])
                    ->setParameter('locale', $this->getLocale());
                $query = $query->getQuery()->getResult();


                $profession = $query[0];

                $professionTranslations = $profession->getTranslations();

                foreach ($this->getLocales() as $l) {
                    foreach ($professionTranslations as $t) {
                        if ($t->getLocale() == $l && $t->getField() == 'name') {
                            $data['title_' . $l] = $t->getContent();
                        }
                    }
                }
            }

            //Default date data
            $now = new \DateTime();
            $data['date_from'] = $now->format('Y-m-d');
            $data['date_to'] = clone $now;
            $data['date_to']->add(new \DateInterval('P' . ($step1Data['type']['days_period'] - 1) . 'D'));
            $data['date_to'] = $data['date_to']->format('Y-m-d');

            $initDateFrom = $data['date_from'];
            $initDateTo = $data['date_to'];

            if ($this->session->get('step3')) {
                $stored = $this->session->get('step3');

                /**
                 * @todo check is it correct in aby cases
                 */
                $stored['form']['date_from'] = $data['date_from'];
                $data = $stored['form'] + $data;
            }

            if ($this->session->get('format_files')) {
                $data['format_files'] = $this->session->get('format_files');
                if (!empty($data['format_files']) && is_array($data['format_files'])) {
                    foreach ($data['format_files'] as $l => $row) {
                        if (!empty($row['html_image']) && is_array($row['html_image'])) {
                            foreach ($row['html_image'] as $k => $val) {
                                $data['html_image_' . $l][$k] = $val['fileNameOrig'];
                            }
                        }
                    }
                }
            } else {
                $data['format_files'] = array();
            }

            $stepFormatData = $this->session->get('step_format');

            $data['format'] = Advertisement::getValidFormat(!empty($stepFormatData['form']['format']) ? $stepFormatData['form']['format'] : null);


            $form = $this->createForm(new AddAdvertisementStep3Type($this->getLocales(), $this->currentCountry->getDefaultLocale(), $this->container->get('exercise_html_purifier.default'), $this->get('translator')), $data);


            $postData = $request->get($form->getName());

            if ($postData['deleted_logo'] == 1) {
                $this->session->set('logo', null);
            }

            $this->session->set('format_files', $this->advertisementManager->checkFileFormat($data['format_files'], $postData));

            $defaultLocale = $this->getCurrentCountry()->getDefaultLocale();
            $locales = $this->getCurrentCountry()->getLocales();

            $fields = array('duty_', 'offer_', 'requirement_');

            foreach ($fields as $f) {
                if (!isset($postData[$f . $defaultLocale])) {
                    continue;
                }
                $fieldData = $postData[$f . $defaultLocale];
                $fieldCount = count($fieldData);
                if (isset($fieldData[$fieldCount - 1]) && empty($fieldData[$fieldCount - 1])) {

                    $unset = true;
                    foreach ($locales as $l) {
                        if ($l == $defaultLocale) {
                            continue;
                        }

                        if (!empty($postData[$f . $l][$fieldCount - 1])) {
                            $unset = false;
                            break;
                        }
                    }

                    if ($unset) {
                        foreach ($locales as $l) {
                            unset($postData[$f . $l][$fieldCount - 1]);
                        }
                    }
                }
            }
            $request->request->set($form->getName(), $postData);



            $form->bindRequest($request);

            $advertisementService = $this->get('visidarbi.advertisement.advertisement_service');

            if ($form->isValid()) {

                $testDateFrom = strtotime($postData['date_from']);
                $testDateTo = strtotime($postData['date_to']);

                $compareDateFrom = strtotime($initDateFrom);
                $cpmpareDateTo = strtotime($initDateTo);

                if ($testDateFrom < $compareDateFrom || $testDateTo > $cpmpareDateTo) {

                    $error = new \Symfony\Component\Form\FormError('Incorrect date period');
                    $form->get('date_1')->addError($error);
                    $formErrors = $advertisementService->getFormErrorMessagesAsArray($form);
                    $result['error'] = $formErrors;
                    return new Response(json_encode($result), 200, array('Content-Type' => 'application/json'));
                }
            } else {
                $formErrors = $advertisementService->getFormErrorMessagesAsArray($form);
                $result['error'] = $formErrors;
            }

            $formData = $form->getData();

            $previewAdvertisementData = (array)$this->session->get('previewAdvertisementData');

            if (array_key_exists('advertisementId', $previewAdvertisementData)){
                $advertisementId = $previewAdvertisementData['advertisementId'];
            } else {
                $advertisement->generatePublicId();
                $advertisementId = $advertisement->getPublicId();
            }

            $previewAdvertisementData = array('advertisementData' => $formData, 'advertisementId'=>$advertisementId);

            $this->session->set('previewAdvertisementData', $previewAdvertisementData);

            return new Response(json_encode($result), 200, array('Content-Type' => 'application/json'));
        }

        /**
         * Behavior #2
         * Composing advertisment
         */
        $currentLocale = $request->getLocale();
        $previewAdvertisementData = $this->session->get('previewAdvertisementData');

        $formData = $previewAdvertisementData['advertisementData'];
        $advertisementId = $previewAdvertisementData['advertisementId'];

        $formatFiles = $this->session->get('format_files');


//        $advertisement = new Advertisement();

        $contactPerson = new ContactPerson();

        $contactPerson
            ->setName($formData['contact_person']['name'])
            ->setEmail($formData['contact_person']['email'])
            ->setPhone($formData['contact_person']['phone'])
            ->setFax($formData['contact_person']['fax'])
            ->setUrl($formData['contact_person']['url'])
            ->setActualAddress($formData['contact_person']['actual_address'])
            ->setLegalAddress($formData['contact_person']['legal_address'])
            ->setPosition($formData['contact_person']['position_' . $currentLocale])
        ;


        $company = new Company();
        $company
            ->setName($formData['company']['name'])
            ->setRegistrationNumber($formData['company']['registration_number'])
            ->setDescription($formData['company']['description_' . $currentLocale])

        ;


        if ($this->session->get('logo')) {
            $logoPath = $this->session->get('logo');
            $company
                ->setLogo(basename($logoPath['logo']))
                ->setLogoPath(dirname($logoPath['logo']))
            ;
        }

        $step2Data = $this->session->get('step2');

        $advertisementCountry = $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry')
            ->findOneById((int) $step2Data['form']['country']);

        $advertisementCity = $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity')
            ->findOneById((int) $step2Data['form']['city']);

        $cretaedAt = date("Y-m-d H:i:s");
        $publishedAt = date("Y-m-d H:i:s", strtotime($formData['date_from']));

        $advertisement
            ->setPublicId($advertisementId)
            ->setContactPerson($contactPerson)
            ->setTitle($formData['title_' . $currentLocale])
            ->setDateTo($formData['date_to'])
            ->setCompanies($company)
            ->setAdvertisementCity($advertisementCity)
            ->setAdvertisementCountry($advertisementCountry)
            ->setCretaedAt($cretaedAt)
            ->setPublishedAt($publishedAt)
            ->setCount(1)
            ->setFormat($formData['format']) // free | full | html | imagepdf
            ->setStatus(Advertisement::STATUS_ACTIVE)
            ->setIsHighlighted(1)

        ;

        /**
         * Case if format = free form
         */
        if ($formData['format'] == Advertisement::FORMAT_FREE) {

            $t = new AdvertisementTranslation();
            $t
                ->setObject($advertisement)
                ->setField('content')
                ->setLocale($currentLocale)
                ->setContent($formData['content_' . $currentLocale])
            ;

            $advertisement->addTranslation($t);
        }

        /**
         * Case if format = full form
         */
        if ($formData['format'] == Advertisement::FORMAT_FULL) {
            foreach ($formData['requirement_' . $currentLocale] as $req_description) {
                $requirement = new Requirement();
                $requirement->setDescription($req_description);

                $advertisement->addRequirementse($requirement);
            }
            foreach ($formData['duty_' . $currentLocale] as $duty_description) {
                $duty = new Duty();
                $duty->setDescription($duty_description);

                $advertisement->addDutiese($duty);
            }
            foreach ($formData['offer_' . $currentLocale] as $offer_description) {
                $offer = new Offer();
                $offer->setDescription($offer_description);
                $advertisement->addOffer($offer);
            }

            $advertisement
                ->setDescription($formData['description_' . $currentLocale])
                ->setAdditionalDescription($formData['additional_description_' . $currentLocale]);
        }

        /**
         * Case if format = html form
         */
        if ($formData['format'] == Advertisement::FORMAT_HTML && is_array($formatFiles)) {

            $formatDir = $this->advertisementManager->getFileFormatDir($advertisement, $currentLocale, 'html_image', true);
            /**
             * processing html images uploaded
             * copy files from tmp folder
             */
            if (array_key_exists('html_image', $formatFiles[$currentLocale])) {

                foreach ($formatFiles[$currentLocale]['html_image'] as $htmlImageData) {
                    $origFilename = $htmlImageData['fileNameOrig'];
                    $newPath = $formatDir . '/' . $origFilename;
                    if (file_exists($htmlImageData['fileTmpPath']) && (!file_exists($newPath) || $this->session->get('areNewFilesUploaded')) ) {
                        copy($htmlImageData['fileTmpPath'], $newPath);
                    }
                }
            }


            $formatDir = $this->advertisementManager->getFileFormatDir($advertisement, $currentLocale, Advertisement::FORMAT_HTML, true);

            /**
             * processing advertisement content html file uploaded
             * copy files from tmp folder
             */
            $origFilename = $formatFiles[$currentLocale]['file_format']['fileNameOrig'];
            $newPath = $formatDir . '/' . $origFilename;

            if (file_exists($formatFiles[$currentLocale]['file_format']['fileTmpPath']) && (!file_exists($newPath) || $this->session->get('areNewFilesUploaded')) ) {

                copy($formatFiles[$currentLocale]['file_format']['fileTmpPath'], $newPath);
            }

            if (file_exists($newPath)){
                $t = new AdvertisementTranslation();
                $t
                    ->setObject($advertisement)
                    ->setField('file_format')
                    ->setLocale($currentLocale)
                    ->setContent($origFilename)
                ;

                $advertisement->addTranslation($t);
            }
        }

        /**
         * Case if format = imagepdf form
         */
        if ($formData['format'] == Advertisement::FORMAT_IMAGE_PDF && is_array($formatFiles)) {

            /**
             * processing image/pdf files uploaded
             * copy files from tmp folder
             */
            $formatDir = $this->advertisementManager->getFileFormatDir($advertisement, $currentLocale, Advertisement::FORMAT_IMAGE_PDF, true);


            $origFilename = $formatFiles[$currentLocale]['file_format']['fileNameOrig'];
            $newPath = $formatDir . '/' . $origFilename;

            if (file_exists($formatFiles[$currentLocale]['file_format']['fileTmpPath']) && (!file_exists($newPath) || $this->session->get('areNewFilesUploaded')) ) {

                copy($formatFiles[$currentLocale]['file_format']['fileTmpPath'], $newPath);
            }

            if (file_exists($newPath)){
                $t = new AdvertisementTranslation();
                $t
                    ->setObject($advertisement)
                    ->setField('file_format')
                    ->setLocale($currentLocale)
                    ->setContent($origFilename)
                ;

                $advertisement->addTranslation($t);
            }
        }

        $this->session->remove('areNewFilesUploaded');

        /**
         * create social share form
         */
        $shareForm = $this->createForm(new ShareAdvertisementType());

        if ($this->currentUser) {
            $shareForm->setData(array('email_from' => $this->currentUser->getEmail()));
        }

        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:preview_advertisement.html.twig', array(
                'isPreview' => true,
                'open_ad' => 1,
                'user' => $this->currentUser,
                'shareForm' => $shareForm->createView(),
                'locale' => $this->getLocale(),
                'advertisement' => $advertisement,
                'allowSaveAdvertisement' => false,
                'defaultLocale' => $this->getCurrentCountry()->getDefaultLocale(),
                'advertisementManager' => $this->advertisementManager,
                'favorites' => $this->currentUserFavorites,
        ));
    }

    private function checkCurrentUserAccess() {
        if ($this->currentUser && $this->currentUser->isPrivate()) {
            return $this->redirect($this->generateUrl('index'));
        }
    }

    private function getStepAliasesList() {
        return array(
            'step1' => 1,
            'step2' => 2,
            'step_format' => 3,
            'step3' => 4,
            'step4' => 5,
            'step5' => 6,
        );
    }

    private function getStepByAlias($stepAlias) {
        $map = $this->getStepAliasesList();
        return !empty($map[$stepAlias]) ? $map[$stepAlias] : 0;
    }

    private function getAliasByStep($step) {
        $map = $this->getStepAliasesList();
        $alias = array_search($step, $map);
        return !empty($alias) ? $alias : 'step1';
    }

    private function validateStep($stepAlias) {

        $currentStep = $this->getStepByAlias($stepAlias);
        for ($i = $currentStep - 1; $i >= 1; $i--) {
            $prevStepAlias = $this->getAliasByStep($i);
            if (!$this->session->get($prevStepAlias)) {
                if (!($currentStep == 5 && in_array($i, array(3,4)) && $this->session->get('payNow'))) {
                    return $this->redirect($this->generateUrl('visidarbi_advertisement_add_' . $prevStepAlias));
                }
            }

            if (!($currentStep == 5 && in_array($i, array(3,4)) && $this->session->get('payNow'))) {

                $prevStepData = $this->session->get($prevStepAlias);

                if (empty($prevStepData['valid']) || !$prevStepData['valid']) {
                    return $this->redirect($this->generateUrl('visidarbi_advertisement_add_' . $prevStepAlias));
                }
            }
        }

        return true;
    }

    private function addAdvertisement($byPeriod = null) {

        $dataStep1 = $step1Data = $this->session->get('step1');
        $dataStep2 = $step1Data = $this->session->get('step2');
        $dataStep3 = $step1Data = $this->session->get('step3');
        $dataStep4 = $step1Data = $this->session->get('step4');
        $dataStepFormat = $this->session->get('step_format');

        //Check for user
        if ($this->currentUser && $this->allowAddAdvertisement) {

            $user = $this->currentUser;
        } else {

            //If user logged in
            if ($this->currentUser) {
                $user = $this->currentUser;
            } else {

                //check if user add passwords
                if ($dataStep4['form']['password']) {
                    $user = $this->createUserBySetp4($dataStep4);
                    $this->session->set('isProfileAdded', true);
                } else {
                    $user = null;
                }
            }
        }

        $this->session->set('advertisementUser', $user);

        $advertisementType =
                $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType')
                ->findOneById((int) $dataStep1['type']['id']);

        $advertisementCountry = $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry')
                ->findOneById((int) $dataStep2['form']['country']);

        $advertisementCity = $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity')
                ->findOneById((int) $dataStep2['form']['city']);


        $query =
                $this->getDoctrine()
                ->getEntityManager()
                ->createQueryBuilder('c')
                ->select('c')
                ->from('VisiDarbi\ProfessionBundle\Entity\Category', 'c')
                ->andWhere("c.id IN (:categories)")
                ->setParameter('categories', $dataStep2['form']['category']);

        $advertisementCategories = $query->getQuery()->getResult();



//        $advertisementCategory = $this->em->getRepository('\VisiDarbi\ProfessionBundle\Entity\Category')
//                ->findById((int) $dataStep2['form']['category']);


        $query =
                $this->getDoctrine()
                ->getEntityManager()
                ->createQueryBuilder('p')
                ->select('p')
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->join('p.translations', 't')
                ->andWhere("t.locale = :locale")
                ->andWhere("t.slug IN (:slug)")
                ->andWhere("p.category IN (:categories)")
                ->setParameter('slug', $dataStep2['form']['profession'])
                ->setParameter('categories', $dataStep2['form']['category'])
                ->setParameter('locale', $this->getLocale());

        $advertisementProfessions = $query->getQuery()->getResult();



//        $advertisementProfession = $this->em->getRepository('\VisiDarbi\ProfessionBundle\Entity\Profession')
//                ->findOneById((int) $dataStep2['form']['profession']);


        $advertisement = new \VisiDarbi\AdvertisementBundle\Entity\Advertisement();

        if ($user) {
            $advertisement->setUser($user);
        }

        foreach ($advertisementCategories as $c) {
            $advertisement->addCategories($c);
        }

        foreach ($advertisementProfessions as $p) {
            $advertisement->addProfession($p);
        }

        $advertisement->setAdvertisementCountry($advertisementCountry);
        $advertisement->setAdvertisementType($advertisementType);
        $advertisement->setCountry($this->getCurrentCountry());
        if ($advertisementCity) {
            $advertisement->setAdvertisementCity($advertisementCity);
        }

        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'title', $dataStep3['form']);
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'description', $dataStep3['form']);
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'additional_description', $dataStep3['form'], 'setAdditionalDescription');
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'content', $dataStep3['form']);

        foreach ($advertisement->getTranslations() as $at) {
            /* @var $at \VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation */
            if ($at->getField() == 'title') {
                $slug = new \VisiDarbi\AdvertisementBundle\Entity\AdvertisementSlug();
                $advertisement->addSlug($slug->setLocale($at->getLocale())->setSlugSource($at->getContent()));
            }
        }

        $advertisement->setDateFrom(new \DateTime($dataStep3['form']['date_from']));

        if (!$byPeriod) {
            $advertisement->setDateTo(new \DateTime($dataStep3['form']['date_to']));
        } else {
            $advertisement->setDateTo($byPeriod->getToDate());
        }

        $advertisement->setStatus(Advertisement::STATUS_PENDING);
        $advertisement->setFormat(!empty($dataStepFormat['form']['format']) ? $dataStepFormat['form']['format'] : null);
        $advertisement->setIsHighlighted(1);

        $this->em->persist($advertisement);

        $this->advertisementManager->setFileFormat($advertisement, $this->session->get('format_files', null));

        //Create company data
        $companyData = $dataStep3['form']['company'];
        $company = new Company();

        $logo = $this->session->get('logo', null);

        if (!empty($logo)) {

            $company->setLogo($logo['filename']);


            $uploadConfig = $this->container->getParameter('upload_logo');
            $webPath = $path = $this->get('kernel')->getRootDir() . '/../web';

            $company->setLogoPath($uploadConfig['stored_path']);

            //move file from tmp folder
            if (file_exists($webPath . '/' . $uploadConfig['tmp_path'] . '/' . $logo['filename'])) {
                rename($webPath . '/' . $uploadConfig['tmp_path'] . '/' . $logo['filename'], $webPath . '/' . $uploadConfig['stored_path'] . '/' . $logo['filename']);
            }
        }

        $company->setName($companyData['name']);
        $company->setRegistrationNumber($companyData['registration_number']);

        $this->advertisementManager->populateObjectTranslations($company, 'VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation', 'description', $companyData);
        $company->setAdvertisement($advertisement);
        $this->em->persist($company);


        if (!$byPeriod) {
            //Create requesits data
            $requisites = new Requisites();
            $requisites->setAdvertisement($advertisement);
            $requisites->setAddress($dataStep4['form']['address']);
            $requisites->setBankAccount($dataStep4['form']['bankAccount']);
            $requisites->setBankCode($dataStep4['form']['bankCode']);
            $requisites->setCompanyName($dataStep4['form']['companyName']);
            $requisites->setContactPersonName($dataStep4['form']['contactPersonName']);
            $requisites->setEmail($dataStep4['form']['email']);
            $requisites->setNoPVN($dataStep4['form']['noVat']);
            $requisites->setPVN($dataStep4['form']['vat']);
            $requisites->setRegistrationNumber($dataStep4['form']['registrationNumber']);
            $advertisement->setOnStartpage($dataStep4['form']['onFirstPage']);
            $advertisement->setRequisites($requisites);
            $this->em->persist($requisites);
        } else {
            //$advertisement->setPaidPeriods($byPeriod);
            //$advertisement->setStatus(Advertisement::STATUS_ACTIVE);
        }

        //Create contact person data
        $contactPerson = new ContactPerson();
        $contactPerson->setActualAddress($dataStep3['form']['contact_person']['actual_address']);
        $contactPerson->setAdvertisement($advertisement);
        $contactPerson->setEmail($dataStep3['form']['contact_person']['email']);
        $contactPerson->setFax($dataStep3['form']['contact_person']['fax']);
        $contactPerson->setName($dataStep3['form']['contact_person']['name']);
        $contactPerson->setPhone($dataStep3['form']['contact_person']['phone']);
        $contactPerson->setUrl($dataStep3['form']['contact_person']['url']);
        $contactPerson->setLegalAddress($dataStep3['form']['contact_person']['legal_address']);
        $this->advertisementManager->populateObjectTranslations($contactPerson, 'VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation', 'position', $dataStep3['form']['contact_person']);
        $this->em->persist($contactPerson);


        //group localized collections
        $items = $this->collectItemsData($dataStep3['form']);

        //Create duties
        foreach ($items['duties'] as $dutyData) {
            $duty = new Duty();
            $this->advertisementManager->populateObjectTranslations($duty, 'VisiDarbi\AdvertisementBundle\Entity\DutyTranslation', 'description', $dutyData);
            $duty->setAdvertisement($advertisement);
            $this->em->persist($duty);
        }

        //Create offers
        foreach ($items['offers'] as $offerData) {
            $offer = new Offer();
            $this->advertisementManager->populateObjectTranslations($offer, 'VisiDarbi\AdvertisementBundle\Entity\OfferTranslation', 'description', $offerData);
            $offer->setAdvertisement($advertisement);
            $this->em->persist($offer);
        }

        //Create requirements
        foreach ($items['requirements'] as $requirementData) {
            $requirement = new Requirement();
            $this->advertisementManager->populateObjectTranslations($requirement, 'VisiDarbi\AdvertisementBundle\Entity\RequirementTranslation', 'description', $requirementData);
            $requirement->setAdvertisement($advertisement);
            $this->em->persist($requirement);
        }
        return $advertisement;
    }

    private function collectItemsData($data) {
        $items = array(
            'duties' => array(),
            'offers' => array(),
            'requirements' => array(),
        );

        if (empty($data)) {
            return $items;
        }
        $defaultLocale = $this->getCurrentCountry()->getDefaultLocale();
        $fields = array('duties' => 'duty_', 'offers' => 'offer_', 'requirements' => 'requirement_');

        foreach ($this->getLocales() as $locale) {
            foreach ($fields as $item_key => $f ) {
                if (!empty($data[$f . $locale]) && is_array($data[$f . $locale])) {
                    foreach ($data[$f . $locale] as $k => $v) {
                        if ($defaultLocale == $locale) {
                            if (empty($v)) {
                                continue;
                            }
                        } else {
                            if (empty($v)) {
                                $v = $data[$f . $defaultLocale][$k];
                            }
                            if (empty($v)) {
                                continue;
                            }
                        }
                        $items[$item_key][$k]['description_' . $locale] = $v;
                    }
                }

            }
        }
        return $items;
    }

    protected function checkForPaidPeriod($user, $typeData) {

        $period = $this->getCurrentActiveUserPeriod($user, $typeData);

        if ($period) {
            $this->session->set('usePeriod', $period);
            return true;
        }

        $this->session->set('usePeriod', null);
        return false;
    }

    /**
     * Find user PaidPeriod
     * @param \VisiDarbi\UserBundle\Entity\User $user
     * @param int $typeId
     * @return mixed
     */
    protected function getCurrentActiveUserPeriod($user) {

        //Find period that mutch for AD type and current date
        $query = $this->em->createQueryBuilder('pp')
                ->select('pp', 't')
                ->from('VisiDarbi\AdvertisementBundle\Entity\PaidPeriod', 'pp')
                ->join('pp.advertisementType', 't')
                ->where('pp.user = :user')
                ->andWhere('pp.from_date <= :nowDate')
                ->andWhere('pp.to_date >= :nowDate')
                ->andWhere('pp.status = :status')
                ->setParameter('user', $user->getId())
                ->setParameter('nowDate', new \DateTime())
                ->setParameter('status', PaidPeriod::STATUS_ACTIVE)
                ->orderBy('pp.from_date', 'DESC')
                ->setMaxResults(1);


        $query = $query->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $result = $query->getResult();

        if (!empty($result)) {
            return $result[0];
        }

        return null;
    }

    /**
     * Returns user last advertisement
     * @return \VisiDarbi\AdvertisementBundle\Entity\Advertisement
     */
    public function getUserLastAdvertisement() {

        $query = $this->em->createQueryBuilder('a')
                ->select('a')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'a')
                ->where('a.user = :user')
                ->setParameter('user', $this->currentUser->getId())
                ->orderBy('a.cretaed_at', 'DESC')
                ->setMaxResults(1);

        $query = $query->getQuery();
//        $query = $query->getQuery()
//                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $result = $query->getResult();

        if (!empty($result)) {
            return $result[0];
        }

        return null;
    }


    /**
     * Add period to user
     */
    protected function addPaidPeriod($advertisement = null) {


        $dataStep1 = $step1Data = $this->session->get('step1');

        $advertisementType =
                $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementType')
                ->findOneById((int) $dataStep1['type']['id']);


        $period = new PaidPeriod();
        $period->setUser($this->currentUser);

        if ($advertisement) {
            $period->setAdvertisement($advertisement);
        }

        $period->setAdvertisementType($advertisementType);
        $period->setStatus(PaidPeriod::STATUS_PENDING);

        $dateFrom = new \DateTime();
        $dateTo = clone $dateFrom;
        $dateTo->add(new \DateInterval('P' . $advertisementType->getDaysPeriod() . 'D'));
        $period->setFromDate(new \DateTime());
        $period->setToDate($dateTo);
        $this->em->persist($period);

        return $period;
    }

    private function createUserBySetp4($dataStep4) {
        $user = new User();
        $user->setType(User::TYPE_LEGAL);

        $user->setEmail($dataStep4['form']['email']);
        $user->setCountry($this->getCurrentCountry());
        $user->setFullName($dataStep4['form']['contactPersonName']);
        $user->setPassword(md5(rand()));
        $user->setEnabled($dataStep4['form']['password']);
        $user->setRoles(array('ROLE_USER'));
        $this->get('fos_user.user_manager')->updateUser($user, false);


        $lp = new LegalProfile();
        $lp->setCompanyName($dataStep4['form']['companyName']);
        $lp->setFactualAddress($dataStep4['form']['address']);
        $lp->setLegalAddress($dataStep4['form']['address']);
        $lp->setRegistrationNumber($dataStep4['form']['registrationNumber']);
        if (!$dataStep4['form']['noVat']) {
            $lp->setVatNumber($dataStep4['form']['vat']);
        }

        //$this->em->persist($lp);
        $user->setLegalProfile($lp);
        $this->em->persist($user);

        $user->setEnabled(false);

        $user->setConfirmationToken($this->tokenGenerator->generateToken());

        $this->sendConfirmationEmailMessage($user);

        $this->userManager->updateUser($user);

        return $user;
    }

    protected function sendConfirmationEmailMessage(User $user) {
        $confirmLink = $this->generateUrl('confirm_registration', array('token' => $user->getConfirmationToken()), true);

        $this->emailSender->send(
                $this->countryManager->getCurrentCountry(), $this->getRequest()->getLocale(), EmailTemplate::ID_REG_CONFIRMATION, $user->getEmail(), array(
            'confirmLink' => sprintf('<a href="%s">%s</a>', $confirmLink, $confirmLink)
                ), array(
            'email' => $this->settingsManager->get('emails.sender_email'),
            'name' => $this->settingsManager->get('emails.sender_name')
                )
        );
    }

    protected function createTransactionForPeriod(\VisiDarbi\AdvertisementBundle\Entity\PaidPeriod $period, $amount = null) {

        $step1data = $this->session->get('step1');

        $transaction = new PaymentTransaction();
        $transaction->setPaidPeriod($period);
        $transaction->setDescription($step1data['type']['payment_description_period']);

        if (empty($amount)) {
            $transaction->setAmount($step1data['type']['price']);
        } else {
            $transaction->setAmount($amount);
        }

        $this->em->persist($transaction);
        return $transaction;
    }

    protected function createTransactionForAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement, $amount = null, $description = null, $period = null) {

        $step1data = $this->session->get('step1');

        $transaction = new PaymentTransaction();
        $transaction->setAdvertisement($advertisement);

        if (empty($description)) {
            $transaction->setDescription('Advertisement type: ' . $advertisement->getAdvertisementType()->getName());
        } else {
            $transaction->setDescription($description);
        }

        if (empty($amount)) {
            $transaction->setAmount($step1data['type']['price']);
        } else {
            $transaction->setAmount($amount);
        }

        $requisites = $advertisement->getRequisites();

        $transaction->setRequisites($requisites);
        $transaction->setPaidPeriod($period);

        $this->em->persist($transaction);
        return $transaction;
    }

    protected function resetStepsBeforeSuccess() {
        $this->session->set('step1', null);
        $this->session->set('step2', null);
        $this->session->set('step_format', null);
        $this->session->set('format_files', null);
        $this->session->set('fromAdvId', null);
        $this->session->set('step3', null);
        $this->session->set('step4', null);
        $this->session->set('step5', true);
        $this->session->set('payNow', null);
    }


}

?>
