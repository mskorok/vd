<?php
namespace VisiDarbi\AdvertisementBundle\Controller;

use VisiDarbi\CommonBundle\Controller\FrontendController;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\Query\Expr;

/**
 * CategoryPageController - category A-Z pages controller
 *
 * @author Aleksey
 */
class CategoryPageController extends FrontendController {

    protected $adRepository;
    

    
    /**
     * Default action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function indexAction(Request $request) {
        $response = $this->forward('VisiDarbiAdvertisementBundle:CategoryPage:place', array());

        return $response;
    }

    /**
     * Profession category page action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function professionAction(Request $request) {
        
        $this->adRepository = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement');        

        $az = array();
        $category = null;
        $systemCountry = $this->getCurrentCountry();
        $selectedLetter = $request->get('letter', null);
        $viewMode = $request->get('view', null);
       
        if(!$viewMode){      
            
            $categories = $this->adRepository
                    ->getNotEmptyCategories($systemCountry, $this->getLocale());

            $categories->useResultCache(true, 2600, 'categories_list_'.$this->getLocale().'_'.md5(serialize($categories->getParameters())));
            $categories->getResult();
            
        } else if( $viewMode == $this->get('translator')->trans('profession-action-all-in-translit', array(), 'routing_parametrs') ){
           
            $az = $this->adRepository
                  ->getAZforProfessions($systemCountry, $this->getLocale());
            
           
            if(!$selectedLetter || array_search($selectedLetter, $az)===false) {
                $selectedLetter = $az[0];
            }             
            
            $categories = $this->adRepository
                    ->getProfessionListByLetter($systemCountry, $this->getLocale(), $selectedLetter);

            $categories->useResultCache(true, 2600, 'categories_list_'.$this->getLocale().'_'.md5(serialize($categories->getParameters())));

            $categories = $categories->getResult();

            //return;
            $data = array();
            $hash = array();
            
            foreach ($categories as $key => $value) {
                if(!empty($hash[$value->getName()])) {
                    continue;
                }
                $data[] = $value;
                $hash[$value->getName()] = $value->getName();
            }
            
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
            $data, $this->get('request')->get('page', 1), 10, array('distinct' => false));

            $pagination->setUsedRoute('visidarbi_category_profession_full');
        
            return $this->render('VisiDarbiAdvertisementBundle:CategoryPage:profession.html.twig',
                array(
                    'categories' => $categories,
                    'locale' => $this->getLocale(),
                    'view' => $viewMode,
                    'letters' => $az,
                    'selecteLetter' => $selectedLetter,
                    'pagination' => $pagination
                ));           
            
        } else {
            
            $categories = $this->adRepository
                    ->getNotEmptyProfessionByCategory($viewMode, $this->getLocale());

            $categories->useResultCache(true, 2600, 'categories_list_'.$this->getLocale().'_'.md5(serialize($categories->getParameters())));
            $categories->getResult();
            
            
            if(isset($categories[0])) {
                $category = $categories[0];
                $category = $category->getCategory();
            }
        
        }
        
        return $this->render('VisiDarbiAdvertisementBundle:CategoryPage:profession.html.twig',
                array('categories' => $categories,
                    'locale' => $this->getLocale(),
                    'view' => $viewMode,
                    'letters' => $az,
                    'selecteLetter' => $selectedLetter,
                    'category' => $category));
    }

    /**
     * Places page action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function placeAction(Request $request) {
       
        $this->adRepository = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement');
        
        $systemCountry = $this->getCurrentCountry();
        $adDefaultCountry = $systemCountry->getDeafultAdvertisementCountry();
  
        $advertisementCountries = 
                $this->adRepository
                ->getAdvertisementCountries($systemCountry, $this->getLocale());

        $advCityRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity');
        $qb = $advCityRepo->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, COUNT(a.id) as adv_count, ct.slug AS city_slug')
            ->leftJoin('c.Advertisements', 'a')
            ->leftJoin('c.translations', 'ct', Expr\Join::WITH, "ct.locale=:locale AND ct.field = 'name' " )
            ->where('c.mark_on_frontend = 1')
            ->andWhere('c.AdvertisementCountry = :country')
            ->andWhere('a.status = :adStatus')
            ->setParameter('country', $adDefaultCountry)
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->setParameter('locale', $this->getLocale())
            ->groupBy('c.id')
            ->orderBy('c.name', 'ASC')
            ->orderBy('city_name', 'ASC')
            ->having('adv_count > 0')
        ;
        
        $query = $qb->getQuery();
        
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale());


        $query->useResultCache(true, 2600, 'advertisementSelectedCities_list_'.$this->getLocale().'_'.md5(serialize($query->getParameters())));


        $advertisementSelectedCities = $query->getResult();        
        
        $advCityRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity');
        $qb = $advCityRepo->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, COUNT(a.id) as adv_count, ct.slug AS city_slug')
            ->leftJoin('c.Advertisements', 'a')
            ->leftJoin('c.translations', 'ct', Expr\Join::WITH, "ct.locale=:locale AND ct.field = 'name' " )
            ->where('c.mark_on_frontend = 0')
            ->andWhere('c.AdvertisementCountry = :country')
            ->andWhere('a.status = :adStatus')
            ->setParameter('country', $adDefaultCountry)
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->setParameter('locale', $this->getLocale())
            ->groupBy('c.id')
            ->orderBy('c.name', 'ASC')
            ->orderBy('city_name', 'ASC')
            ->having('adv_count > 0');
        
        $query = $qb->getQuery();
        
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale());


        $query->useResultCache(true, 2600, 'advertisementCities_list_'.$this->getLocale().'_'.md5(serialize($query->getParameters())));

        $advertisementCities = $query->getResult();         

        return $this->render('VisiDarbiAdvertisementBundle:CategoryPage:place.html.twig',
                array('adCountry' => $advertisementCountries,
                    'adCity' => $advertisementCities,
                    'adCitySelected' => $advertisementSelectedCities,
                    'locale' => $this->getLocale(),
                    'defaultAdCountry' => $adDefaultCountry));
    }

    /**
     * Company page action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function companyAction(Request $request) {
        
        $this->adRepository = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement');        
        
        $selectedLetter = $request->get('letter', null);
        
        $letters = $this->adRepository->getAZforCompanies($this->getCurrentCountry());
        
        if(!$selectedLetter || array_search($selectedLetter, $letters)===false) {
            $selectedLetter = $letters[0];
        }
        
        $companies = $this->adRepository->getCompaniesListByLetter($this->getCurrentCountry(), $selectedLetter);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $companies, $this->get('request')->get('page', 1), 36, array('distinct' => false));
        
        //Set for each company count of AD
        foreach($pagination as $v){
            $v->setAdvertisementsBySlug(
                    $this->adRepository
                    ->getCompanyAdvertisementsAmount($v));
        }

        $pagination->setUsedRoute('visidarbi_category_company_full');

        return $this->render('VisiDarbiAdvertisementBundle:CategoryPage:company.html.twig',
                array('pagination' => $pagination,
                    'letters' => $letters,
                    'selecteLetter' => $selectedLetter,
                    'locale' => $this->getLocale()));
    }

}

?>
