<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 15.12.4
 * Time: 08.09
 */

namespace VisiDarbi\AdvertisementBundle\Controller;


use VisiDarbi\CommonBundle\Controller\FrontendController;
use VisiDarbi\LocalePlaceBundle\Entity\SecondaryCurrency;

trait HighLight
{
    /**
     *  считает для одного объявления подсвеченные + первая страница
     * @return array
     */
    private function calculatePaymentAmount(){

        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');

        $finalPrice =  (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_advert_highlighting');
        $finalPriceSecond = $this->secondCurrencyRate ? $finalPrice / $this->secondCurrencyRate : false;
        $finalPriceFirstPage = ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page') + $finalPrice);
        $finalPriceFirstPageSecond = $this->secondCurrencyRate ? $finalPriceFirstPage / $this->secondCurrencyRate : false;

        $vatTotal = $finalPrice / 100 * $vatValue;
        $vatTotalSecond = $finalPriceSecond / 100 * $vatValue;

        $vatTotalFirstPage = $finalPriceFirstPage / 100 * $vatValue;
        $vatTotalFirstPageSecond = $finalPriceFirstPageSecond / 100 * $vatValue;

        $finalPriceFirstPageWithVat = $finalPriceFirstPage + ($vatTotalFirstPage);
        $finalPriceFirstPageWithVatSecond =  $finalPriceFirstPageSecond + ($vatTotalFirstPageSecond);

        $finalPriceWithVat = $finalPrice + ($vatTotal);
        $finalPriceWithVatSecond = $finalPriceSecond + ($vatTotalSecond);

        return array(
            'vatValue' => $vatValue,
            'finalPrice' => $finalPrice,
            'finalPriceSecond' => number_format($finalPriceSecond, 2),
            'finalPriceFirstPage' => number_format($finalPriceFirstPage, 2),
            'finalPriceFirstPageSecond' => number_format($finalPriceFirstPageSecond, 2),
            'vatTotal' => number_format($vatTotal, 2),
            'vatTotalSecond' => number_format($vatTotalSecond, 2),
            'vatTotalFirstPage' => number_format($vatTotalFirstPage, 2),
            'vatTotalFirstPageSecond' => number_format($vatTotalFirstPageSecond, 2),
            'finalPriceFirstPageWithVat' => number_format($finalPriceFirstPageWithVat, 2),
            'finalPriceFirstPageWithVatSecond' => number_format($finalPriceFirstPageWithVatSecond, 2),
            'finalPriceWithVat' => number_format($finalPriceWithVat, 2),
            'finalPriceWithVatSecond' => number_format($finalPriceWithVatSecond, 2),
        );


    }

    /**
     *  считает только первую страницу
     * @param int $count
     * @param int $mobileCount
     * @return array
     */
    public function calculatePaymentAmountMultipleHighlights($count = 1, $mobileCount = 0){

        /** @var  $this FrontendController */
        /** @var SecondaryCurrency $secondCurrency */
        $secondCurrency = $this->getCurrentCountry()->getSecondaryCurrencyByCode('EUR');

        $secondCurrencyRate = ($secondCurrency)? $secondCurrency->getRateToPrimaryCurrency():null;


        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');

        $finalPrice = ((float) $count * $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page'));
        $finalPriceSecond = $secondCurrencyRate ? $finalPrice / $secondCurrencyRate : false;

        $vatTotal = $finalPrice / 100 * $vatValue;
        $vatTotalSecond = $finalPriceSecond / 100 * $vatValue;

        $finalPriceWithVat = $finalPrice + ($vatTotal);
        $finalPriceWithVatSecond = $finalPriceSecond + ($vatTotalSecond);



//        $finalPrice = $step1Data['type']['price'];
//        $finalPriceFirstPage = $finalPrice + (float)$this->get('visidarbi.settings.manager')
//            ->get('prices.price_for_first_page';
//
//        $finalPriceHighLightedMobile = (float) $step1Data['type']['price'] + (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_mobile_advert_highlighting');
//        $finalPriceFirstPageHighLightedMobile = $finalPriceHighLightedMobile + ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page'));
//
//
//
//
//        $finalPriceSecond = $step1Data['type']['price'] / $this->secondCurrencyRate;
//        $finalPriceFirstPageSecond = $finalPriceSecond + $data['priceFirstPageSecond'];
//
//        $finalPriceHighLightedMobileSecond = ((float) $this->get('visidarbi.settings.manager')
//                ->get('prices.price_for_mobile_advert_highlighting') )/ $this->secondCurrencyRate +
//            ((float) $step1Data['type']['price']) / $this->secondCurrencyRate;
//        $finalPriceFirstPageHighLightedMobileSecond = $finalPriceHighLightedMobileSecond + ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page')) / $this->secondCurrencyRate;








        return array(
            'vatValue' => $vatValue,

            'finalPrice' => number_format($finalPrice, 2),
            'finalPriceSecond' => number_format($finalPriceSecond, 2),

            'vatTotal' => number_format($vatTotal, 2),
            'vatTotalSecond' => number_format($vatTotalSecond, 2),

            'finalPriceWithVat' => number_format($finalPriceWithVat, 2),
            'finalPriceWithVatSecond' => number_format($finalPriceWithVatSecond, 2),

        );

    }

}