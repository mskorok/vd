<?php

namespace VisiDarbi\AdvertisementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('VisiDarbiAdvertisementBundle:Default:index.html.twig', array('name' => $name));
    }
}
