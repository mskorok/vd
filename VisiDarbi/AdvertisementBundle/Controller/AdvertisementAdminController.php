<?php

namespace VisiDarbi\AdvertisementBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;

/**
 * Description of AdvertisementAdminClass
 *
 * @author Aleksey
 */
class AdvertisementAdminController extends Controller {

    /**
     * @Route("/get-cities/{country}", name="visidarbi_admin_get_cities", options={"expose"=true}, defaults={"country" = null})
     */
    public function getCitiesAction($country) {
        
        $currentCountry = $this->container->get('visidarbi.country_manager')->getCurrentAdminCountry();
        $countryId = (int) $country;

        $query = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity')
                ->createQueryBuilder('c')
                ->select('c')
                ->where('c.AdvertisementCountry = :acountry')
                ->setParameter(':acountry', $countryId)
                ->orderBy('c.name');

        $query = $query->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $currentCountry->getDefaultLocale());
                //->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)           
        
        
        $result = $query->getResult();

        $cities = array();
        foreach ($result as $city) {
            $cities[] = array('id' => $city->getId(), 'title' => (string) $city);
        }

        return new Response(json_encode($cities), 200);
    }
    
    /**
     * @Route("/get-professions/{category}", name="visidarbi_admin_get_professions", options={"expose"=true}, defaults={"category" = null})
     */
    public function getProfessionsAction($category) {
        
        
        $category = explode(',', $category);

        $query = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('VisiDarbi\ProfessionBundle\Entity\Profession')
                ->createQueryBuilder('c')
                ->select('c')
                ->join('c.category', 'cc')
                ->where('c.category IN (:category)')
                ->orderBy('cc.name, c.name ')
                ->setParameter(':category', $category);

        $query = $query->getQuery();
        
        $currentCountry = $this->container->get('visidarbi.country_manager')->getCurrentAdminCountry();
        
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $currentCountry->getDefaultLocale());
                //->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)        
        
        $result = $query->getResult();

        $categories = array();
        foreach ($result as $category) {
            $categories[] = array('id' => $category->getId(), 'title' => $category->getCategory()->getName() . ' / ' . (string) $category);
        }

        return new Response(json_encode($categories), 200);
    }    
    
    public function deleteAction($id = null) {
        return parent::deleteAction($id = null);
    }

}

?>
