<?php

namespace VisiDarbi\AdvertisementBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use VisiDarbi\CommonBundle\Controller\FrontendController;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep2Type;
use VisiDarbi\AdvertisementBundle\Form\HighlightAdvertisementStep1Type;
use VisiDarbi\AdvertisementBundle\Form\HighlightAdvertisementStep2Type;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep3Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\Company;
use VisiDarbi\AdvertisementBundle\Entity\Duty;
use VisiDarbi\AdvertisementBundle\Entity\Offer;
use VisiDarbi\AdvertisementBundle\Entity\Requirement;
use VisiDarbi\AdvertisementBundle\Entity\Requisites;
use VisiDarbi\AdvertisementBundle\Entity\ContactPerson;
use VisiDarbi\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JMS\SecurityExtraBundle\Annotation\Secure;



/**
 * AdvertisementController - advertisements front end controller
 *
 * @author Aleksey
 */
class UpdateAdvertisementController extends FrontendController {



    protected $adRepository;
    protected $currentCountry;
    protected $secondCurrency;
    protected $secondCurrencyRate;
    protected $secondCurrencySign;

    protected $currentUser = null;

    /**
     * @var \VisiDarbi\AdvertisementBundle\Manager\AdvertisementManagerInterface
     * @DI\Inject("visidarbi.advertisement_manager")
     */
    protected $advertisementManager;

    protected $allowAddAdvertisement = false;
    protected $em;
    protected $session;
    protected $lastTransaction = null;

    public function preExecute() {


        $this->secondCurrencyRate = null;
        $this->secondCurrencySign = null;
        $this->secondCurrency = $this->getCurrentCountry()->getSecondaryCurrencyByCode('EUR');

        if ($this->secondCurrency) {
            $this->secondCurrencyRate = $this->secondCurrency->getRateToPrimaryCurrency();
            $this->secondCurrencySign = $this->secondCurrency->getCurrency();
        }


        $this->adRepository = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement');

        $this->currentCountry = $this->getCurrentCountry();

        $this->currentUser = $this->get('security.context')->getToken()->getUser();

        //Disable save for legal person
        if ($this->currentUser instanceof \VisiDarbi\UserBundle\Entity\User) {
            $this->allowAddAdvertisement = (bool) $this->currentUser->isLegal();
        }

        $this->em = $this->getDoctrine()->getEntityManager();
        $this->session = $this->get('session');
    }

    public function indexAction(Request $request) {
        $response = $this->forward('VisiDarbiAdvertisementBundle:Advertisement:list', array());

        return $response;
    }

    private function validateStep($currentStep) {

        for ($i = $currentStep - 1; $i >= 1; $i--) {

            if (!$this->session->get('step' . $i)) {
                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step' . $i));
            }

            $prevStepData = $this->session->get('step' . $i);

            if (empty($prevStepData['valid']) || !$prevStepData['valid']) {
                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step' . $i));
            }
        }

        return true;
    }

    /**
     * Highlight advertisement action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function highlightAction(Request $request)
    {
        $advertisementId = $request->get('advertisementId', null);
        //advertisementId can be publicId or slug

        $advertisement = $this->getDoctrine()
                ->getEntityManager()->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                ->getByPublicIdOrSlug($advertisementId);

        if (!$advertisement || $advertisement->getIsHighlighted()) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
        }

        if($advertisement->getCompanies() && $advertisement->getCompanies()->getLogo()) {

            if(!$this->session->get('previousLogoCheck', null)) {

                $uploadConfig = $this->container->getParameter('upload_logo');
                $tmpDir = __DIR__ .'/../../../../web/'.$uploadConfig['tmp_path'];

                $parts = explode('.', $advertisement->getCompanies()->getLogo());
                $existentExtension = end($parts);
                $fileName = md5(uniqid('',true));
                $uploadedResizedLogo = $tmpDir . '/' . $fileName . '_logo';
                $existentLogo = $advertisement->getCompanies()->getFullLogoPath();
                copy($existentLogo, $tmpDir . '/' . $fileName . '.' .$existentExtension);
                $existentLogo = $tmpDir . '/' . $fileName . '.' .$existentExtension;

                try {
                    $img = $this->get('image.handling')->open($existentLogo);
                    $imgType = $img->guessType();
                    $img->resize(100,100)
                        ->save($uploadedResizedLogo.'.'.$imgType, $imgType);

                    $webPath = $uploadConfig['tmp_path'] . '/' . $fileName . '_logo.' . $imgType;

                    $this->session->set('logo', array('filename'=>$fileName . '.' . $imgType, 'logo'=> $webPath));
                } catch(\Exception $e) {
                    $this->session->set('logo', null);
                }

                $this->session->set('previousLogoCheck', 1);
            }
        }

        //before step 3, advertisement step 2 and step 1 details must be imported into session

        //build step1 session
        $advType =  $advertisement->getAdvertisementType();
        $advTypePrice = $advType->getAdvertisingTypePrices();

        $data1 = array(
            'advertisement_type' => $advType->getId(),
        );

        $data2 = array(
            'id' => $advType->getId(),
            'name' => $advType->getName(),
            'price' => "",
            'days_period' => $advType->getDaysPeriod()
        );

        $this->session->set('step1', array('valid' => 'true', 'form' => $data1, 'type' => $data2));

        //build step2 session
        $advCategories = $advertisement->getCategories();
        $advProfessions = $advertisement->getProfessions();
        $advCity = $advertisement->getAdvertisementCity();
        $advCountry = $advertisement->getAdvertisementCountry();

        $advCategoriesIds = array();
        if(!empty($advCategories)) {
            foreach($advCategories as $c){
                $advCategoriesIds[] = $c->getId();
            }
        }

        $advProfessionsIds = array();
        if(!empty($advProfessions)) {
            foreach($advProfessions as $p){
                $advProfessionsIds[] = $p->getSlug('name', $this->getLocale());
            }
        }

        $data3 = array(
            'categories' => $advCategoriesIds,
            'professions' => $advProfessionsIds,
            'country' => $advCountry == null ? null : $advCountry->getId(),
            'city' => $advCity == null ? null : $advCity->getId(),
            'country_data' => ''
        );

        $this->session->set('step2', array('valid' => true, 'form' => $data3));

        //step3 start
        $result = $this->validateStep(3);
        if ($result !== true) {
            return $result;
        }

        //Fill fields
        $data = array();

        //fill advertisement translations
        $advTranslations = $advertisement->getTranslations();
        foreach ($advTranslations as $t) {
            $data[$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        //fill company translations
        if ($advertisement->getCompany()) {
            $data["company"]["name"] = $advertisement->getCompany()->getName();
            $data["company"]["registration_number"] = $advertisement->getCompany()->getRegistrationNumber();
        }

        if ($this->session->get('step3')) {
            $stored = $this->session->get('step3');

            /**
            * @todo check is it correct in aby cases
            */

            $data = $stored['form'] + $data;
        }


        $categories =
                $this->adRepository
                ->getCategoriesListForAddFrom($this->getCurrentCountry(), $this->getLocale())
                ->getResult();

        $categoriesSet = array();

        foreach ($categories as $item) {
            $categoriesSet[$item['id']] = $item['name'];
        }

        $countries = $this->adRepository
                ->getAdvertisementCountriesForAddForm($this->getCurrentCountry(), $this->getLocale());

        $countriesSet = array();

        foreach ($countries as $item) {
            $countriesSet[$item['id']] = $item['name'];
        }

        $initData = array(
            'country' => $this->getCountryManager()->getCurrentCountry()->getId(),
            'country_data' => $this->getCountryManager()->getCurrentCountry()->getId());

        $data = $data +  $initData;

        $form = $this->createForm(new HighlightAdvertisementStep1Type($this->getLocales(), $this->currentCountry->getDefaultLocale(), $categoriesSet, $countriesSet, $this->getLocale(), $this->em), $data);

        if ($request->isMethod('POST')) {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $this->session->set('step3', array('advertisementId' => $advertisementId, 'valid' => true, 'form' => $form->getData()));
                    return $this->redirect($this->generateUrl('visidarbi_advertisement_highlight_step2'));
                } else {
                    $this->session->set('step3', array('valid' => false, 'form' => $form->getData()));
                }
            }

        $logo = "";
        if ($advertisement->getCompany() && $advertisement->getCompany()->getLogo() && $advertisement->getCompany()->isLogoFileExists()) {
            $logo = $advertisement->getCompany()->getFullLogoPath();
        }

        $data = $this->session->get('step3');



        $selectedCategory = array();
        $selectedProfession = array();
        $selectedCountry = null;
        $selectedCity = null;

        if (!empty($data['form']['category'])) {
            $selectedCategory = $data['form']['category'];
        } else {
            $selectedCategory = $advCategoriesIds;
        }

        if (!empty($data['form']['profession'])) {
            $selectedProfession = $data['form']['profession'];
        } else {
            $selectedProfession = $advProfessionsIds;
        }

        if (!empty($data['form']['country'])) {
            $selectedCountry = $data['form']['country'];
        } else {
            if($advCountry) {
                $selectedCountry = $advCountry->getId();
            }
        }

        if (!empty($data['form']['city'])) {
            $selectedCity = $data['form']['city'];
        } else {
            if($advCity) {
                $selectedCity = $advCity->getId();
            }
        }

        return $this->render('VisiDarbiAdvertisementBundle:HighlightAdvertisement:step1.html.twig', array(
            'form' => $form->createView(),
            'backLink' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $this->generateUrl('visidarbi_advertisement_list'),
            'logo' => array('logo' => $logo),
            'step' => 1,
            'locales' => $this->getLocales(),
            'selectedCategory' => '"' . join('","', $selectedCategory) . '"',
            'selectedProfession' => '"' . join('","', $selectedProfession) . '"',
            'selectedCountry' => $selectedCountry,
            'selectedCity' => $selectedCity,
        ));
    }

        /**
         * Highlight advertisement action
         * @param \Symfony\Component\HttpFoundation\Request $request
         * @return Response A Response instance
         */


    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return bool|\Symfony\Component\HttpFoundation\RedirectResponse
     * Function collect data from highlightAction method
     * and show payment screen
     */
    public function hlstep2Action(Request $request) {


        $step3Data = $this->session->get('step3');
        $advertisementId = $step3Data['advertisementId'];
        //$advertisementId = $request->get('advertisementId', null);


        $advertisement = $this->getDoctrine()
            ->getEntityManager()->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                    ->getByPublicIdOrSlug($advertisementId);

        //$advertisement = $this->getDoctrine()
        //    ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
        //    ->findOneBy(array('publicId' => $advertisementId));

        if (!$advertisement) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
        }

        $user = $this->get('security.context')->getToken()->getUser();

        if (!($user instanceof \VisiDarbi\UserBundle\Entity\User)) {
            $user = null;
        }

        //Default date data
        $data = array();

        $data['advertisementHlPrice'] = (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_advert_highlighting');
        $data['companyName'] = $step3Data['form']['company']['name'];



        $data['priceFirstPage'] = (float)$this->get('visidarbi.settings.manager')
            ->get('prices.price_for_first_page');
        $data['priceFirstPageSecond'] = (float)$this->get('visidarbi.settings.manager')
                ->get('prices.price_for_first_page') / $this->secondCurrencyRate;
        $data['priceHighLightedMobile'] = (float)$this->get('visidarbi.settings.manager')
            ->get('prices.price_for_mobile_advert_highlighting');
        $data['priceHighLightedMobileSecond'] = (float)$this->get('visidarbi.settings.manager')
                ->get('prices.price_for_mobile_advert_highlighting') / $this->secondCurrencyRate;

        $data['priceFirstPage'] = number_format($data['priceFirstPage'], 2);
        $data['priceFirstPageSecond'] = number_format($data['priceFirstPageSecond'], 2);
        $data['priceHighLightedMobile'] = number_format($data['priceHighLightedMobile'], 2);
        $data['priceHighLightedMobileSecond'] = number_format($data['priceHighLightedMobileSecond'], 2);






        if ($this->session->get('step4')) {
            $stored = $this->session->get('step4');

            /**
             * @todo check is it correct in any cases
             */
            $data = $data + $stored['form'];
        }


        if ($user) {
            $data = $data + array('email' => $user->getEmail());
        }

        $withVatValidation = true;

        if ($request->isMethod('POST')) {

            $f = new HighlightAdvertisementStep2Type();
            $postDate = $request->get($f->getName());
            if(isset($postDate['noVat']) && $postDate['noVat']) {
                $withVatValidation = false;
            } else {
                $withVatValidation = true;
            }
        }

        //process form for updating AD

        $form = $this->createForm(new HighlightAdvertisementStep2Type($user, $withVatValidation), $data);

        $showPasswordField = false;

        //Get all types of available payment settings, amount, product types and so on
        $paymentPricesDetails = $this->calculatePaymentAmount();

        if ($request->isMethod('POST')) {
            $form->bindRequest($request);

            $formData = $form->getData();

            if (!empty($formData['password']) && $formData['password'] != $formData['password_repeat']) {

                $error = new \Symfony\Component\Form\FormError('Password and repeat password not match');
                $form->get('password')->addError($error);
                $showPasswordField = true;
            }

            //Validate user - check that it not exists
            if (!empty($formData['password'])) {

                $user =
                    $this->em
                        ->getRepository('VisiDarbi\UserBundle\Entity\User')
                        ->findOneBy(array('email' => $formData['email']));

                if ($user) {
                    $error = new \Symfony\Component\Form\FormError('User thith current email already exists');
                    $form->get('email')->addError($error);
                    $showPasswordField = true;
                }
            }



        if ($form->isValid()) {

            $this->session->set('step4', array('valid' => true, 'form' => $form->getData()));

                $period = null;

                if ($advertisement->getAdvertisementType()->getUnlimited()) {
                    $period = $this->addPaidPeriod($advertisement);
                    $advertisement->setPaidPeriods($period);
                }



            if($formData['isHighLightedMobile'] && $formData['onFirstPage']){
                $price = $paymentPricesDetails['finalPriceFirstPageHighLightedMobileWithVat'];
                $transactionDescription = $this->get('translator')->trans(
                    'Advertisement highlighting on portal www.visidarbi.lv on first page and mobile version.');
            } elseif($formData['isHighLightedMobile']) {
                $price = $paymentPricesDetails['finalPriceHighLightedMobileWithVat'];
                $transactionDescription = $this->get('translator')->trans(
                    'Advertisement highlighting on portal www.visidarbi.lv on mobile version.');
            } elseif ($formData['onFirstPage']) {
                $price = $paymentPricesDetails['finalPriceFirstPageWithVat'];
                $transactionDescription = $this->get('translator')->trans(
                    'Advertisement highlighting on portal www.visidarbi.lv with place on first page.');
            } else {
                $price = $paymentPricesDetails['finalPriceWithVat'];
                $transactionDescription = $this->get('translator')->trans(
                    'Advertisement highlighting on portal www.visidarbi.lv.');
            }


                $requisites = $advertisement->getRequisites();

                if ($requisites == null) {
                    $requisites = new Requisites();
                }

                //Create requesits data
                $requisites->setAdvertisement($advertisement);
                $requisites->getPaidPeriod($period);
                $requisites->setAddress($formData['address']);
                $requisites->setBankAccount($formData['bankAccount']);
                $requisites->setBankCode($formData['bankCode']);
                $requisites->setCompanyName($formData['companyName']);
                $requisites->setContactPersonName($formData['contactPersonName']);
                $requisites->setEmail($formData['email']);
                $requisites->setNoPVN($formData['noVat']);
                $requisites->setPVN($formData['vat']);
                $requisites->setRegistrationNumber($formData['registrationNumber']);
                $this->em->persist($requisites);

                $transaction = $this->createTransactionForAdvertisement($advertisement, $price, $transactionDescription);
                $transaction->setRequisites($requisites);

                $this->em->flush();

                if ($advertisement->getAdvertisementType()->getUnlimited() && $period) {
                    $this->session->set('lastPeriodId', $period->getId());
                }

                $this->lastTransaction = $transaction;
                $this->session->set('lastAdvertisementId', $advertisement->getId());
                $this->session->set('lastTransactionId', $transaction->getId());
                //$this->resetStepsBeforeSuccess();

                /**
                 * @todo something after create AD and send transaction data
                 */

                $paymentConfig = $this->container->getParameter('paymenthl');

                $currencyCode = $this->getCurrentCountry()->getPrimaryCurrency()->getCode();

                $paymentDataArray = array(
                    'currency' => $currencyCode,
                    'amount' => $price,
                    'message' => urlencode($this->lastTransaction->getDescription()),
                    'method' => $paymentConfig['bank'][$formData['payment']],
                    'external_id' => $this->lastTransaction->getId(),
                    'url' => $this->generateUrl('visidarbi_advertisement_highlight_step3', array(), true),
                );

                $paramStr = array();
                foreach($paymentDataArray as $k => $v) {
                    $paramStr[] = $k . '=' .$v;
                }

                $paymentUrl = $paymentConfig['url'] . '?' . implode('&', $paramStr);

                $this->lastTransaction->setPaymentCurrency($currencyCode);
                $this->lastTransaction->setPaymentMethod($paymentConfig['bank'][$formData['payment']]);
                $this->lastTransaction->setRequest($paymentUrl);
                $this->em->flush($this->lastTransaction);

                return new RedirectResponse($paymentUrl);
            }
        }
        else {
            $this->session->set('step4', array('valid' => false, 'form' => $form->getData()));
        }



        return $this->render('VisiDarbiAdvertisementBundle:HighlightAdvertisement:step2.html.twig',
            array(
                'currency' => $this->getCurrentCountry()->getPrimaryCurrency()->getCode(),
                'form' => $form->createView(),
                'secondCurrency' => $this->secondCurrency,
                'secondCurrencySign' => $this->secondCurrencySign,
                'vatValue' => $paymentPricesDetails['vatValue'],
                'finalPrice' => $paymentPricesDetails['finalPrice'],
                'finalPriceSecond' => $paymentPricesDetails['finalPriceSecond'],
                'finalPriceFirstPage' => $paymentPricesDetails['finalPriceFirstPage'],
                'finalPriceFirstPageSecond' =>$paymentPricesDetails['finalPriceFirstPageSecond'],
                'finalPriceHighLightedMobile'  => $paymentPricesDetails['finalPriceHighLightedMobile'],
                'finalPriceHighLightedMobileSecond'  => $paymentPricesDetails['finalPriceHighLightedMobileSecond'],
                'finalPriceFirstPageHighLightedMobile'  => $paymentPricesDetails['finalPriceFirstPageHighLightedMobile'],
                'finalPriceFirstPageHighLightedMobileSecond'  => $paymentPricesDetails['finalPriceFirstPageHighLightedMobileSecond'],
                'vatTotal' => $paymentPricesDetails['vatTotal'],
                'vatTotalSecond' => $paymentPricesDetails['$vatTotalSecond'],
                'vatTotalFirstPage' => $paymentPricesDetails['vatTotalFirstPage'],
                'vatTotalFirstPageSecond' => $paymentPricesDetails['vatTotalFirstPageSecond'],
                'vatTotalHighLightedMobile'   => $paymentPricesDetails['vatTotalHighLightedMobile'],
                'vatTotalHighLightedMobileSecond'   => $paymentPricesDetails['vatTotalHighLightedMobileSecond'],
                'vatTotalFirstPageHighLightedMobile' => $paymentPricesDetails['vatTotalFirstPageHighLightedMobile'],
                'vatTotalFirstPageHighLightedMobileSecond' => $paymentPricesDetails['vatTotalFirstPageHighLightedMobileSecond'],
                'finalPriceFirstPageWithVat' => $paymentPricesDetails['finalPriceFirstPageWithVat'],
                'finalPriceFirstPageWithVatSecond' => $paymentPricesDetails['finalPriceFirstPageWithVatSecond'],
                'finalPriceWithVat' => $paymentPricesDetails['finalPriceWithVat'],
                'finalPriceWithVatSecond' => $paymentPricesDetails['finalPriceWithVatSecond'],
                'finalPriceHighLightedMobileWithVat'  => $paymentPricesDetails['finalPriceHighLightedMobileWithVat'],
                'finalPriceHighLightedMobileWithVatSecond'  => $paymentPricesDetails['finalPriceHighLightedMobileWithVatSecond'],
                'finalPriceFirstPageHighLightedMobileWithVat' => $paymentPricesDetails['finalPriceFirstPageHighLightedMobileWithVat'],
                'finalPriceFirstPageHighLightedMobileWithVatSecond' => $paymentPricesDetails['finalPriceFirstPageHighLightedMobileWithVatSecond'],
                'step' => 2,
                'showPasswordField' => $showPasswordField,
                'user' => $this->currentUser,
            )
        );
    }

    private function calculatePaymentAmount(){

        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');

        $finalPrice =  (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_advert_highlighting');
        $finalPriceSecond = $this->secondCurrencyRate ? $finalPrice / $this->secondCurrencyRate : false;
        $finalPriceFirstPage = ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page') + $finalPrice);
        $finalPriceFirstPageSecond = $this->secondCurrencyRate ? $finalPriceFirstPage / $this->secondCurrencyRate : false;


        $finalPriceHighLightedMobile = $finalPrice + (float) $this->get('visidarbi.settings.manager')->get('prices.price_for_mobile_advert_highlighting');
        $finalPriceFirstPageHighLightedMobile = $finalPriceHighLightedMobile + ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page'));


        $finalPriceHighLightedMobileSecond = ((float) $this->get('visidarbi.settings.manager')
                ->get('prices.price_for_mobile_advert_highlighting') )/ $this->secondCurrencyRate +
            $finalPrice / $this->secondCurrencyRate;
        $finalPriceFirstPageHighLightedMobileSecond = $finalPriceHighLightedMobileSecond + ((float) $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page')) / $this->secondCurrencyRate;


        $vatTotal = $finalPrice / 100 * $vatValue;
        $vatTotalSecond = $finalPriceSecond / 100 * $vatValue;

        $vatTotalFirstPage = $finalPriceFirstPage / 100 * $vatValue;
        $vatTotalFirstPageSecond = $finalPriceFirstPageSecond / 100 * $vatValue;

        $finalPriceFirstPageWithVat = $finalPriceFirstPage + ($vatTotalFirstPage);
        $finalPriceFirstPageWithVatSecond =  $finalPriceFirstPageSecond + ($vatTotalFirstPageSecond);


        $vatTotalFirstPageHighLightedMobile = $finalPriceFirstPageHighLightedMobile / 100 * $vatValue;
        $vatTotalFirstPageHighLightedMobileSecond = $finalPriceFirstPageHighLightedMobileSecond / 100 * $vatValue;

        $vatTotalHighLightedMobile = $finalPriceHighLightedMobile / 100 * $vatValue;
        $vatTotalHighLightedMobileSecond = $finalPriceHighLightedMobileSecond / 100 * $vatValue;

        $finalPriceFirstPageHighLightedMobileWithVat = $finalPriceFirstPageHighLightedMobile + ($vatTotalFirstPageHighLightedMobile);
        $finalPriceFirstPageHighLightedMobileWithVatSecond =  $finalPriceFirstPageHighLightedMobileSecond + ($vatTotalFirstPageHighLightedMobileSecond);

        $finalPriceHighLightedMobileWithVat = $finalPriceHighLightedMobile + ($vatTotalHighLightedMobile);
        $finalPriceHighLightedMobileWithVatSecond =  $finalPriceHighLightedMobileSecond + ($vatTotalHighLightedMobileSecond);




        $finalPriceWithVat = $finalPrice + ($vatTotal);
        $finalPriceWithVatSecond = $finalPriceSecond + ($vatTotalSecond);

        return array(
            'vatValue' => $vatValue,
            'finalPrice' => $finalPrice,
            'finalPriceSecond' => number_format($finalPriceSecond, 2),
            'finalPriceFirstPage' => number_format($finalPriceFirstPage, 2),
            'finalPriceFirstPageSecond' => number_format($finalPriceFirstPageSecond, 2),
            'finalPriceHighLightedMobile'  => number_format($finalPriceHighLightedMobile, 2),
            'finalPriceHighLightedMobileSecond'  => number_format($finalPriceHighLightedMobileSecond, 2),
            'finalPriceFirstPageHighLightedMobile'  => number_format($finalPriceFirstPageHighLightedMobile, 2),
            'finalPriceFirstPageHighLightedMobileSecond'  => number_format($finalPriceFirstPageHighLightedMobileSecond, 2),
            'vatTotal' => number_format($vatTotal, 2),
            'vatTotalSecond' => number_format($vatTotalSecond, 2),
            'vatTotalFirstPage' => number_format($vatTotalFirstPage, 2),
            'vatTotalFirstPageSecond' => number_format($vatTotalFirstPageSecond, 2),
            'vatTotalHighLightedMobile'   => number_format($vatTotalHighLightedMobile, 2),
            'vatTotalHighLightedMobileSecond'   => number_format($vatTotalHighLightedMobileSecond, 2),
            'vatTotalFirstPageHighLightedMobile' => number_format($vatTotalFirstPageHighLightedMobile, 2),
            'vatTotalFirstPageHighLightedMobileSecond' => number_format($vatTotalFirstPageHighLightedMobileSecond, 2),
            'finalPriceFirstPageWithVat' => number_format($finalPriceFirstPageWithVat, 2),
            'finalPriceFirstPageWithVatSecond' => number_format($finalPriceFirstPageWithVatSecond, 2),
            'finalPriceWithVat' => number_format($finalPriceWithVat, 2),
            'finalPriceWithVatSecond' => number_format($finalPriceWithVatSecond, 2),
            'finalPriceHighLightedMobileWithVat'  => number_format($finalPriceHighLightedMobileWithVat, 2),
            'finalPriceHighLightedMobileWithVatSecond'  => number_format($finalPriceHighLightedMobileWithVatSecond, 2),
            'finalPriceFirstPageHighLightedMobileWithVat' => number_format($finalPriceFirstPageHighLightedMobileWithVat, 2),
            'finalPriceFirstPageHighLightedMobileWithVatSecond' => number_format($finalPriceFirstPageHighLightedMobileWithVatSecond, 2),
        );


    }


    public function calculatePaymentAmountMultipleHighlights($count = 1, $countMobile = 0){

        $vatValue = $this->get('visidarbi.settings.manager')->get('payments.pvn');
        $finalPrice = 0;
        if($count !== 0){
            $finalPrice += ((float) $count * $this->get('visidarbi.settings.manager')->get('prices.price_for_first_page'));
        }
        if($countMobile !== 0){
            $finalPrice += ((float) $countMobile * $this->get('visidarbi.settings.manager')->get('prices.price_for_mobile_advert_highlighting'));
        }

        $finalPriceSecond = $this->secondCurrencyRate ? $finalPrice / $this->secondCurrencyRate : false;

        $vatTotal = $finalPrice / 100 * $vatValue;
        $vatTotalSecond = $finalPriceSecond / 100 * $vatValue;

        $finalPriceWithVat = $finalPrice + ($vatTotal);
        $finalPriceWithVatSecond = $finalPriceSecond + ($vatTotalSecond);

        return array(
            'vatValue' => $vatValue,

            'finalPrice' => number_format($finalPrice, 2),
            'finalPriceSecond' => number_format($finalPriceSecond, 2),

            'vatTotal' => number_format($vatTotal, 2),
            'vatTotalSecond' => number_format($vatTotalSecond, 2),

            'finalPriceWithVat' => number_format($finalPriceWithVat, 2),
            'finalPriceWithVatSecond' => number_format($finalPriceWithVatSecond, 2),

        );

    }

    public function hlstep3Action(Request $request) {

        if (!$this->session->get('step3') || !$this->session->get('step4')) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_highlight'));
        }

        $lastAdvertisementId = $this->session->get('lastAdvertisementId');
        $lastPeriodId = null;

        $returnTransactionId = $request->get('id');

        $paymentConfig = $this->container->getParameter('payment');


        $checksumParam = $request->get('checksum', null);
        $checksum = sha1("apJRW9c11h7SM3Kc,{$request->get('amount', null)},{$request->get('currency', null)},{$request->get('external_id', null)},{$request->get('payd_amount', null)}");

        if($checksumParam != $checksum){

            $returnStatus =  null;

        }elseif(!isset($paymentConfig['status'][$request->get('status')])) {

            $returnStatus =  null;

        } else {

            $returnStatus =  $paymentConfig['status'][$request->get('status')];

        }

        $paymentStatus = null;
        $advertisement = null;
        $period = null;
        $lastTransactionId = $request->get('external_id', null);

        if (!$returnTransactionId || !$returnStatus) {
            $returnStatus = PaymentTransaction::RETURN_STATUS_REGECTED;
        }

        if ($this->session->get('advertisementByPeriod', null)) {
            $returnStatus = PaymentTransaction::RETURN_STATUS_SUCCESS;
        }

        if ($lastTransactionId) {
            /**
             * @todo process lost transaction case
             */
            $transaction =
                $this->getDoctrine()
                    ->getEntityManager()
                    ->find('\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction', $lastTransactionId);


            if(!$transaction) {

                $returnStatus = PaymentTransaction::RETURN_STATUS_REGECTED;

            } else {


                $transaction->setServiceId($returnTransactionId);
                $transaction->setStatus($returnStatus);
                $transaction->setResponse($this->getRequest()->getQueryString());

                $this->getDoctrine()
                    ->getEntityManager()->flush($transaction);

                $advertisement = $transaction->getAdvertisement();

                /**
                * send payment notification to backoffice
                */
                $advertisementService = $this->get('visidarbi.advertisement.advertisement_service');

                $advertisementService->sendPaymentNotificationByTransactionEmailMessage($this->currentCountry, $this->getRequest()->getLocale(), $transaction);
            }
        }

        $advData = $this->session->get('step3');

        if ($returnStatus == PaymentTransaction::RETURN_STATUS_SUCCESS) {

            //save advertisement changes

                //save edited form data
                //delete current advertisement title and description translations
                $advTransRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementTranslation');
                $qb = $advTransRepo->createQueryBuilder('t');
                $qb->delete()
                    ->where('t.object = :adv_id')
                    ->andWhere('t.field = :adv_field')
                    ->setParameter('adv_id', $advertisement->getId())
                    ->setParameter('adv_field', 'title')
                    ->getQuery()
                    ->execute();

                $qb->delete()
                ->where('t.object = :adv_id')
                ->andWhere('t.field = :adv_field')
                ->setParameter('adv_id', $advertisement->getId())
                ->setParameter('adv_field', 'description')
                ->getQuery()
                ->execute();

                //insert new advertisement translations
                $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'title', $advData['form']);
                $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'description', $advData['form']);

                $user = $this->get('security.context')->getToken()->getUser();

                //if authorized user
                if ($user instanceof \VisiDarbi\UserBundle\Entity\User) {
                    //then set this user as owner of this advertisement
                    $advertisement->setUser($user);
                }

                $this->em->persist($advertisement);
                $this->em->flush();

                //Create company data
                $companyData = $advData['form']['company'];
                $company = new Company();

                $logo =  $this->session->get('logo', null);

                if(!empty($logo)){

                    $company->setLogo($logo['filename']);


                    $uploadConfig = $this->container->getParameter('upload_logo');
                    $webPath = $path = $this->get('kernel')->getRootDir() . '/../web';

                    $company->setLogoPath($uploadConfig['stored_path']);

                    //move file from tmp folder
                    if(file_exists($webPath . '/' .$uploadConfig['tmp_path'] . '/' .$logo['filename'])) {
                        rename( $webPath . '/' .$uploadConfig['tmp_path'] . '/' .$logo['filename'],
                            $webPath . '/' .$uploadConfig['stored_path'] . '/' .$logo['filename']);
                    }
                }

                $company->setName($companyData['name']);
                $company->setRegistrationNumber($companyData['registration_number']);

                //delete current advertisement company
                $compTransRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Company');
                $qb = $compTransRepo->createQueryBuilder('t');
                $qb->delete()
                    ->where('t.advertisement = :adv_id')
                    ->setParameter('adv_id', $advertisement->getId())
                    ->getQuery()
                    ->execute();

                //insert new company translations
                $this->advertisementManager->populateObjectTranslations($company, 'VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation', 'description', $companyData);
                $company->setAdvertisement($advertisement);
                $this->em->persist($company);


            $advertisement =
                $this->getDoctrine()
                    ->getEntityManager()
                    ->find('\VisiDarbi\AdvertisementBundle\Entity\Advertisement', $lastAdvertisementId);

            //place on first page
            $dataStep4 = $this->session->get('step4');
            $advertisement->setOnStartpage($dataStep4['form']['onFirstPage']);
            if(isset($dataStep4['form']['isHighLightedMobile'])){
                $advertisement->setIsHighlightedMobile($dataStep4['form']['isHighLightedMobile']);
            }


            //highlight advertisement
            $advertisement->setIsHighlighted(true);

            $dataStep3 = $step1Data = $this->session->get('step3');


            $advertisementCountry = $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry')
                    ->findOneById((int) $dataStep3['form']['country']);

            $advertisementCity = $this->em->getRepository('\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity')
                    ->findOneById((int) $dataStep3['form']['city']);

            $query =
                    $this->getDoctrine()
                    ->getEntityManager()
                    ->createQueryBuilder('c')
                    ->select('c')
                    ->from('VisiDarbi\ProfessionBundle\Entity\Category', 'c')
                    ->andWhere("c.id IN (:categories)")
                    ->setParameter('categories', $dataStep3['form']['category']);

            $advertisementCategories = $query->getQuery()->getResult();

            $query =
                    $this->getDoctrine()
                    ->getEntityManager()
                    ->createQueryBuilder('p')
                    ->select('p')
                    ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                    ->join('p.translations', 't')
                    ->andWhere("t.locale = :locale")
                    ->andWhere("t.slug IN (:slug)")
                    ->andWhere("p.category IN (:categories)")
                    ->setParameter('slug', $dataStep3['form']['profession'])
                    ->setParameter('categories', $dataStep3['form']['category'])
                    ->setParameter('locale', $this->getLocale());

            $advertisementProfessions = $query->getQuery()->getResult();


            foreach ($advertisementCategories as $c) {
                $advertisement->addCategories($c);
            }

            foreach ($advertisementProfessions as $p) {
                $advertisement->addProfession($p);
            }

            $advertisement->setAdvertisementCountry($advertisementCountry);

            if ($advertisementCity) {
                $advertisement->setAdvertisementCity($advertisementCity);
            }

        }

        $this->getDoctrine()
            ->getEntityManager()->flush();

        //Reset session data
        $this->session->set('lastTransactionId', null);
        $this->session->set('lastAdvertisementId', null);
        $this->session->set('lastPeriodId', null);
        $this->session->set('isProfileAdded', null);
        $this->session->set('step1', null);
        $this->session->set('step2', null);
        $this->session->set('step_format', null);
        $this->session->set('format_files', null);
        $this->session->set('step3', null);
        $this->session->set('step4', null);
        $this->session->set('advertisementByPeriod', null);
        $this->session->set('advertisementUser', null);
        $this->session->set('isProfileAdded', null);
        $this->session->set('logo', null);
        $this->session->set('previousLogoCheck', null);


        return $this->render('VisiDarbiAdvertisementBundle:HighlightAdvertisement:step3.html.twig', array(
            'advertisement' => $advertisement,
            'paymentStatus' => $returnStatus,
            'step' => 3,));
    }


    public function hlstepfast1Action(Request $request){

        $user = $this->get('security.context')->getToken()->getUser();
        $stepFast = $this->session->get('stepfast1');
        $stepData = isset($stepFast['ids']) ? $stepFast['ids'] : [];
        $stepDataMobile = isset($stepFast['ids_mobile']) ? $stepFast['ids_mobile'] : [];
        if (! $user->isLegal()) {
            return $this->redirect($this->generateUrl('index'));
        }

        if(empty($stepData) && empty($stepDataMobile)){
            return $this->redirect($this->generateUrl('index'));
        }

        $advertHlCount = (is_array($stepData))?count($stepData):0;
        $advertHlCountMobile = (is_array($stepDataMobile))?count($stepDataMobile):0;

        $data = array();

        $data = $data + array(
            'email' => $user->getEmail(),
            'companyName' => $user->getLegalProfile()->getCompanyName(),
            'address' => $user->getLegalProfile()->getLegalAddress(),
            'contactPersonName' => $user->getFullName(),
            'vat' => $user->getLegalProfile()->getVatNumber() ,
            'noVat' => !$user->getLegalProfile()->getVatNumber() ? true : false,
            'registrationNumber' => $user->getLegalProfile()->getRegistrationNumber()
        );

        $paymentPricesDetails = $this->calculatePaymentAmountMultipleHighlights($advertHlCount, $advertHlCountMobile);


        $withVatValidation = true;

        if ($request->isMethod('POST')) {

            $f = new HighlightAdvertisementStep2Type();
            $postDate = $request->get($f->getName());
            if(isset($postDate['noVat']) && $postDate['noVat']) {
                $withVatValidation = false;
            } else {
                $withVatValidation = true;
            }


        }

        $highLightForm = $this->createForm(new HighlightAdvertisementStep2Type($user, $withVatValidation), $data);

        if ($request->isMethod('POST')) {

            $highLightForm->bind($request);
            $formData = $highLightForm->getData();

            if($highLightForm->isValid()){

                $this->session->set('stepfast2', true);

                $uniq_trans_id = uniqid('parent_');

                $transactionDescription = '';

                if(!empty($advertHlCount)){
                    $transactionDescription .= $this->get('translator')->trans(
                        'Advertisement (%adv_count%) highlighting on portal www.visidarbi.lv with place on first page.',
                        array('%adv_count%' => $advertHlCount));
                }

                if(!empty($advertHlCountMobile)){
                    $transactionDescription .= $this->get('translator')->trans(
                        ' Advertisement (%adv_count%) highlighting on portal www.visidarbi.lv with place on Mobile version.',
                        array('%adv_count%' => $advertHlCountMobile));

                }



                $requisites = new Requisites();
                $requisites->setAddress($formData['address']);
                $requisites->setBankAccount($formData['bankAccount']);
                $requisites->setBankCode($formData['bankCode']);
                $requisites->setCompanyName($formData['companyName']);
                $requisites->setContactPersonName($formData['contactPersonName']);
                $requisites->setEmail($formData['email']);
                $requisites->setNoPVN($formData['noVat']);
                $requisites->setPVN($formData['vat']);
                $requisites->setRegistrationNumber($formData['registrationNumber']);
                $this->em->persist($requisites);

                $transaction = $this->createTransactionForAdvertisement(
                    null,
                    $paymentPricesDetails['finalPriceWithVat'],
                    $transactionDescription,
                    $stepData,
                    $stepDataMobile
                );
                $transaction->setRequisites($requisites);

                $this->em->flush();

                $paymentConfig = $this->container->getParameter('paymenthl');

                $currencyCode = $this->getCurrentCountry()->getPrimaryCurrency()->getCode();

                $paymentDataArray = array(
                    'currency' => $currencyCode,
                    'amount' => $paymentPricesDetails['finalPriceWithVat'],
                    'message' => urlencode($transaction->getDescription()),
                    'method' => $paymentConfig['bank'][$formData['payment']],
                    'external_id' => $transaction->getId(),
                    'url' => $this->generateUrl('visidarbi_advertisement_highlight_fast2', array(), true),
                );

                $paramStr = array();
                foreach($paymentDataArray as $k => $v) {
                    $paramStr[] = $k . '=' .$v;
                }

                $paymentUrl = $paymentConfig['url'] . '?' . implode('&', $paramStr);

                $transaction->setPaymentCurrency($currencyCode);
                $transaction->setPaymentMethod($paymentConfig['bank'][$formData['payment']]);
                $transaction->setRequest($paymentUrl);
                $this->em->flush($transaction);

                return new RedirectResponse($paymentUrl);


            }
            else{
                $this->session->set( 'stepfast2', false );
            }
        }

        return $this->render('VisiDarbiAdvertisementBundle:HighlightAdvertisement:fast1.html.twig', array(

            'step' => '2',

            'form' => $highLightForm->createView(),

            'currency' => $this->getCurrentCountry()->getPrimaryCurrency()->getCode(),
            'secondCurrency' => $this->secondCurrency,
            'secondCurrencySign' => $this->secondCurrencySign,

            'vatValue' => $paymentPricesDetails['vatValue'],

            'vatTotal' =>  $paymentPricesDetails['vatTotal'],
            'vatTotalSecond' =>  $paymentPricesDetails['vatTotalSecond'],

            'finalPrice' => $paymentPricesDetails['finalPrice'],
            'finalPriceSecond' => $paymentPricesDetails['finalPriceSecond'],

            'finalPriceWithVat' => $paymentPricesDetails['finalPriceWithVat'],
            'finalPriceWithVatSecond' => $paymentPricesDetails['finalPriceWithVatSecond'],
        ));
    }


    public function hlstepfast2Action(Request $request){

        if (!$this->session->get('stepfast2')) {
            return $this->redirect($this->generateUrl('my_advertisments'));
        }


        $returnTransactionId = $request->get('id');

        $paymentConfig = $this->container->getParameter('payment');


        $checksumParam = $request->get('checksum', null);
        $checksum = sha1("apJRW9c11h7SM3Kc,{$request->get('amount', null)},{$request->get('currency', null)},{$request->get('external_id', null)},{$request->get('payd_amount', null)}");

        if($checksumParam != $checksum){
            $returnStatus =  null;
        }
        elseif(!isset($paymentConfig['status'][$request->get('status')])) {
            $returnStatus =  null;
        }
        else {
            $returnStatus =  $paymentConfig['status'][$request->get('status')];
        }


        $paymentStatus = null;
        $advertisement = null;
        $period = null;
        $lastTransactionId = $request->get('external_id', null);


        if (!$returnTransactionId || !$returnStatus) {
            $returnStatus = PaymentTransaction::RETURN_STATUS_REGECTED;
        }

        if ($lastTransactionId) {
                /**
                 * @todo process lost transaction case
                 */
                $transaction =
                    $this->getDoctrine()
                        ->getEntityManager()
                        ->find('\VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction', $lastTransactionId);

        }
        if(!$transaction) {
                $returnStatus = PaymentTransaction::RETURN_STATUS_REGECTED;

        } else {


            $transaction->setServiceId($returnTransactionId);
            $transaction->setStatus($returnStatus);
            $transaction->setResponse($this->getRequest()->getQueryString());

            $this->getDoctrine()
                ->getEntityManager()->flush($transaction);

            $advertisement = $transaction->getAdvertisement();
        }


        if ($returnStatus == PaymentTransaction::RETURN_STATUS_SUCCESS) {

            $advertisements = $transaction->getMergedAdvertisements();
            $advertisementsMobile = $transaction->getMergedAdvertisementsMobile();

            if(is_array($advertisements)){

                foreach($advertisements as $adv){

                    $advertisement = $this->getDoctrine()
                        ->getEntityManager()->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                        ->findOneBy(array('publicId' => $adv));

                    $advertisement->setOnStartpage(1);


                    $this->em->persist($advertisement);
                }


            }
            if(is_array($advertisementsMobile)){

                foreach($advertisementsMobile as $adv){

                    $advertisement = $this->getDoctrine()
                        ->getEntityManager()->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                        ->findOneBy(array('publicId' => $adv));

                    $advertisement->setIsHighlightedMobile(1);


                    $this->em->persist($advertisement);
                }


            }
            $this->em->flush();
        }

        $this->session->set('stepfast1', null);
        $this->session->set('stepfast2', null);

        $this->getDoctrine()->getEntityManager()->flush();

        return $this->render('VisiDarbiAdvertisementBundle:HighlightAdvertisement:fast2.html.twig', array(
                'paymentStatus' => $returnStatus,
                'step' => 3,
            )
        );

    }

    /**
     * Copy advertisement action
     * @Secure(roles="ROLE_USER")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function copyAction(Request $request)
    {

        $advertisementId = $request->get('advertisementId', null);

        $adv = $this->getDoctrine()
            ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
            ->findOneBy(array('publicId' => $advertisementId));

        if (!$adv) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
        }

        //before step 3, advertisement step 2 and step format and step 1 details must be imported into session

        //build step1 session
        $advType =  $adv->getAdvertisementType();

        $data1 = array(
            'advertisement_type' => $advType->getId(),
        );

        $advTypePrice = $this->getDoctrine()
            ->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice')
            ->findOneBy(array('advertisementType' => $advType))
            ->getPrice();

        if (!$adv) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
        }


        $data2 = array(
            'id' => $advType->getId(),
            'name' => $advType->getName(),
            'price' => $advTypePrice,
            'days_period' => $advType->getDaysPeriod()
        );

        $this->session->set('step1', array('valid' => 'true', 'form' => $data1, 'type' => $data2));

        //build step2 session

        /**
         * getting ad categories
         */
        $advCategories = $adv->getCategories();

        $advCategoriesSet = array();
        $categoriesSetForWidget = array();

        foreach ($advCategories as $item) {
            $advCategoriesSet[] = $item->getId();
            $categoriesSetForWidget[$item->getId()] = $item->getName();
        }

        /**
         * getting ad countries
         */
        $advCountry = $adv->getAdvertisementCountry();
        $countriesSetForWidget = array($advCountry->getId()=>$advCountry->getName());

        /**
         * getting ad professions
         */
        $advProfessions = $adv->getProfessions();
        $advProfessionsSet = array();

        foreach ($advProfessions as $item) {
            $advProfessionsSet[$item->getId()] = $item->getName();
        }

        $advCity = $adv->getAdvertisementCity();

        $step3data = array(
            'category' => $advCategoriesSet,
            'profession' => $advProfessionsSet,
            'country' => $advCountry == null ? null : $advCountry->getId(),
            'city' => $advCity == null ? null : $advCity->getId(),
            'country_data' => ''
        );

        $form = $this->createForm(new AddAdvertisementStep2Type($categoriesSetForWidget, $countriesSetForWidget, $this->getLocale(), $this->em), $step3data);
        $step3data = $form->getData();


        $this->session->set('step2', array('valid' => true, 'form' => $step3data));

        $dataFormat = array(
            'format' => $adv->getFormat()
        );
        $this->session->set('step_format', array('valid' => true, 'form' => $dataFormat));

        //step3 start
        $result = $this->validateStep(3);

        if ($result !== true) {
            return $result;
        }

        //Fill fields
        $data = array();

        if (!$this->session->get('format_files', null) || $this->session->get('fromAdvId', null) != $adv->getId()) {
            $format_files = $this->advertisementManager->generateFileFormatData($adv);
            $format_files = $this->advertisementManager->setHtmlImagesData($adv, $data, $format_files, true);
            $this->session->set('format_files', $format_files);
            $this->session->set('fromAdvId', $adv->getId());
        }

        //fill advertisement translations
        $advTranslations = $adv->getTranslations();
        foreach ($advTranslations as $t) {
            $data[$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        //fill company translations
        $data["company"]["name"] = $adv->getCompany()->getName();
        $data["company"]["registration_number"] = $adv->getCompany()->getRegistrationNumber();
        $cTranslations = $adv->getCompany()->getTranslations();
        foreach ($cTranslations as $t) {
            $data["company"][$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        foreach ($this->getLocales() as $locale) {
            $data['duty_' . $locale] = array();
            $data['offer_' . $locale] = array();
            $data['requirement_' . $locale] = array();
        }

        //fill duty translations
        $duties = $adv->getDutieses();
        foreach ($duties as $duty) {
            $dutyTranslations = $duty->getTranslations();

            foreach ($dutyTranslations as $t) {
                array_push($data["duty_".$t->getLocale()], $t->getContent());
            }
        }

        //fill offer translations
        $offers = $adv->getOffers();
        foreach ($offers as $offer) {
            $offerTranslations = $offer->getTranslations();

            foreach ($offerTranslations as $t) {
                array_push($data["offer_".$t->getLocale()], $t->getContent());
            }
        }

        //fill requirement translations
        $requirements = $adv->getRequirementses();
        foreach ($requirements as $req) {
            $reqTranslations = $req->getTranslations();

            foreach ($reqTranslations as $t) {
                array_push($data["requirement_".$t->getLocale()], $t->getContent());
            }
        }

        //fill contact person data
        $cp = $adv->getContactPerson();
        $data["contact_person"] = array(
            'name' => $cp->getName(),
            'email' => $cp->getEmail(),
            'phone' => $cp->getPhone(),
            'fax' => $cp->getFax(),
            'url' => $cp->getUrl(),
            'legal_address' => $cp->getLegalAddress(),
            'actual_address' => $cp->getActualAddress()

        );

        $cpTranslations = $cp->getTranslations();
        foreach ($cpTranslations as $t) {
            $data["contact_person"][$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        //Default date data
        $now = new \DateTime();
        $data['date_from'] = $now->format('Y-m-d');
        $data['date_to'] = clone $now;
        $data['date_to']->add(new \DateInterval('P' . ($adv->getAdvertisementType()->getDaysPeriod() -1) . 'D'));
        $data['date_to'] = $data['date_to']->format('Y-m-d');



        if ($this->session->get('step3')) {
            $stored = $this->session->get('step3');

            /**
             * @todo check is it correct in aby cases
             */

            $data = $data + $stored['form'];
        }

        if ($this->session->get('format_files')) {
            $data['format_files'] = $this->session->get('format_files');
        } else {
            $data['format_files'] = array();
        }

        $stepFormatData = $this->session->get('step_format');
        $data['format'] = Advertisement::getValidFormat(!empty($stepFormatData['form']['format']) ? $stepFormatData['form']['format'] : null);

        $form = $this->createForm(new AddAdvertisementStep3Type($this->getLocales(), $this->currentCountry->getDefaultLocale(), $this->container->get('exercise_html_purifier.default')), $data);

        if ($request->isMethod('POST')) {

            $form->bindRequest($request);

            if ($form->isValid()) {
                $this->session->set('step3', array('valid' => true, 'form' => $form->getData()));
                return $this->redirect($this->generateUrl('visidarbi_advertisement_add_step4'));
            } else {
                $this->session->set('step3', array('valid' => false, 'form' => $form->getData()));
            }
        }

        $logo = "";
        if ($adv->getCompany()->getLogo() && $adv->getCompany()->isLogoFileExists()) {
            $logo = $adv->getCompany()->getFullLogoPath();
        }

        return $this->render('VisiDarbiAdvertisementBundle:AddAdvertisement:step3.html.twig', array(
            'form' => $form->createView(),
            'logo' => array('logo' => $logo),
            'step' => 4,
            'locales' => $this->getLocales(),
            'advertisementManager' => $this->advertisementManager,
            'previousContactPersonDataFound' => true,
            'defaultLocale' => $this->getCurrentCountry()->getDefaultLocale(),
        ));

        //step3 end
    }

    /**
     * Edit advertisement action
     * @Secure(roles="ROLE_USER")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function editAction(Request $request)
    {
        $advertisementId = $request->get('advertisementId', null);

        $adv = $this->getDoctrine()
            ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
            ->findOneBy(array('publicId' => $advertisementId));

        if (!$adv) {
            return $this->redirect($this->generateUrl('my_advertisments'));
        }

        if($adv->getCompanies()->getLogo()) {

            if(!$this->session->get('previousLogoCheck', null)) {

                $uploadConfig = $this->container->getParameter('upload_logo');
                $tmpDir = __DIR__ .'/../../../../web/'.$uploadConfig['tmp_path'];

                $parts = explode('.', $adv->getCompanies()->getLogo());
                $existentExtension = end($parts);
                $fileName = md5(uniqid('',true));
                $uploadedResizedLogo = $tmpDir . '/' . $fileName . '_logo';
                $existentLogo = $adv->getCompanies()->getFullLogoPath();
                copy($existentLogo, $tmpDir . '/' . $fileName . '.' .$existentExtension);
                $existentLogo = $tmpDir . '/' . $fileName . '.' .$existentExtension;

                try {
                    $img = $this->get('image.handling')->open($existentLogo);
                    $imgType = $img->guessType();
                    $img->resize(100,100)
                        ->save($uploadedResizedLogo.'.'.$imgType, $imgType);

                    $webPath = $uploadConfig['tmp_path'] . '/' . $fileName . '_logo.' . $imgType;

                    $this->session->set('logo', array('filename'=>$fileName . '.' . $imgType, 'logo'=> $webPath));
                } catch(\Exception $e) {
                    $this->session->set('logo', null);
                }

                $this->session->set('previousLogoCheck', 1);
            }
        }

        //Fill fields
        $data = array();

        //fill advertisement translations
        $advTranslations = $adv->getTranslations();
        foreach ($advTranslations as $t) {
            $data[$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        //fill company translations
        $data["company"]["name"] = $adv->getCompany()->getName();
        $data["company"]["registration_number"] = $adv->getCompany()->getRegistrationNumber();
        $cTranslations = $adv->getCompany()->getTranslations();
        foreach ($cTranslations as $t) {
            $data["company"][$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        foreach ($this->getLocales() as $locale) {
            $data['duty_' . $locale] = array();
            $data['offer_' . $locale] = array();
            $data['requirement_' . $locale] = array();
        }

        //fill duty translations
        $duties = $adv->getDutieses();
        foreach ($duties as $duty) {
            $dutyTranslations = $duty->getTranslations();

            foreach ($dutyTranslations as $t) {
                array_push($data["duty_".$t->getLocale()], $t->getContent());
            }
        }

        //fill offer translations
        $offers = $adv->getOffers();
        foreach ($offers as $offer) {
            $offerTranslations = $offer->getTranslations();

            foreach ($offerTranslations as $t) {
                array_push($data["offer_".$t->getLocale()], $t->getContent());
            }
        }

        //fill requirement translations
        $requirements = $adv->getRequirementses();
        foreach ($requirements as $req) {
            $reqTranslations = $req->getTranslations();

            foreach ($reqTranslations as $t) {
                array_push($data["requirement_".$t->getLocale()], $t->getContent());
            }
        }

        //fill contact person data
        $cp = $adv->getContactPerson();
        $data["contact_person"] = array(
            'name' => $cp->getName(),
            'email' => $cp->getEmail(),
            'phone' => $cp->getPhone(),
            'fax' => $cp->getFax(),
            'url' => $cp->getUrl(),
            'legal_address' => $cp->getLegalAddress(),
            'actual_address' => $cp->getActualAddress()

        );

        $cpTranslations = $cp->getTranslations();
        foreach ($cpTranslations as $t) {
            $data["contact_person"][$t->getField().'_' . $t->getLocale()] = $t->getContent();
        }

        //Default date data
        $now = new \DateTime();
        $data['date_from'] = $now->format('Y-m-d');
        $data['date_to'] = clone $now;
        $data['date_to']->add(new \DateInterval('P' . $adv->getAdvertisementType()->getDaysPeriod() . 'D'));
        $data['date_to'] = $data['date_to']->format('Y-m-d');

        if ($this->session->get('step3')) {
            $stored = $this->session->get('step3');

            /**
             * @todo check is it correct in aby cases
             */
            $data = $data + $stored['form'];
        }

        $data['format'] = Advertisement::getValidFormat($adv->getFormat());

        if (!$this->session->get('format_files', null) || $this->session->get('fromAdvId', null) != $adv->getId()) {
            $this->session->set('format_files', $this->advertisementManager->generateFileFormatData($adv));
            $this->session->set('fromAdvId', $adv->getId());
        }

        $format_files = $this->advertisementManager->setHtmlImagesData($adv, $data, $this->session->get('format_files', null));
        $this->session->set('format_files', $format_files);

        if ($this->session->get('format_files')) {
            $data['format_files'] = $this->session->get('format_files');
        } else {
            $data['format_files'] = array();
        }

        $form = $this->createForm(new AddAdvertisementStep3Type($this->getLocales(), $this->currentCountry->getDefaultLocale(), $this->container->get('exercise_html_purifier.default')), $data);

        if ($request->isMethod('POST')) {
            $form->bindRequest($request);

            $this->session->set('format_files', $this->advertisementManager->checkFileFormat($data['format_files'], $request->get($form->getName())));

            if ($form->isValid()) {
                $this->session->set('step3', array('advertisementId' => $advertisementId,'valid' => true, 'form' => $form->getData()));
                return $this->redirect($this->generateUrl('visidarbi_advertisement_update'));
            } else {
                $this->session->set('step3', array('valid' => false, 'form' => $form->getData()));
            }

        }

        $logo = "";
        if ($adv->getCompany()->getLogo() && $adv->getCompany()->isLogoFileExists()) {
            $logo = $adv->getCompany()->getFullLogoPath();
        }

        return $this->render('VisiDarbiAdvertisementBundle:EditAdvertisement:step3.html.twig', array(
            'form' => $form->createView(),
            'logo' => array('logo' => $logo),
            'step' => 3,
            'locales' => $this->getLocales(),
            'advertisementManager' => $this->advertisementManager,
            'defaultLocale' => $this->getCurrentCountry()->getDefaultLocale()
        ));

        //step3 end

    }

    /**
     * Update advertisement action
     * @Secure(roles="ROLE_USER")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function updateAction() {

        $dataStep3 = $this->session->get('step3');

        //print_r($dataStep3);
        $advertisement = $this->em->getRepository('VisiDarbiAdvertisementBundle:Advertisement')
            ->findOneBy(array('publicId' => $dataStep3['advertisementId']));

        if ($advertisement == null) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
        }

        //delete current advertisement translations
        $advTransRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementTranslation');
        $translations = $advertisement->getTranslations();

        $translationIds = array();
        foreach($translations as $translation) {
            $translationIds[] = $translation->getId();
        }

        if (!empty($translationIds)) {
            $qb = $advTransRepo->createQueryBuilder('t');
            $qb->delete();

            $qb->add('where', $qb->expr()->in('t.id', $translationIds));

            $qb->getQuery()
                ->execute();
        }

        //insert new advertisement translations
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'title', $dataStep3['form']);
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'description', $dataStep3['form']);
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'additional_description', $dataStep3['form'], 'setAdditionalDescription');
        $this->advertisementManager->populateObjectTranslations($advertisement, 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation', 'content', $dataStep3['form']);

        $advertisement->setDateFrom(new \DateTime($dataStep3['form']['date_from']));

        $this->em->persist($advertisement);

        $this->advertisementManager->setFileFormat($advertisement, $this->session->get('format_files', null));

        $this->em->flush();

        //Create company data
        $companyData = $dataStep3['form']['company'];
        $company = new Company();

        $logo =  $this->session->get('logo', null);

        if(!empty($logo)){

            $company->setLogo($logo['filename']);


            $uploadConfig = $this->container->getParameter('upload_logo');
            $webPath = $path = $this->get('kernel')->getRootDir() . '/../web';

            $company->setLogoPath($uploadConfig['stored_path']);

            //move file from tmp folder
            if(file_exists($webPath . '/' .$uploadConfig['tmp_path'] . '/' .$logo['filename'])) {
                rename( $webPath . '/' .$uploadConfig['tmp_path'] . '/' .$logo['filename'],
                    $webPath . '/' .$uploadConfig['stored_path'] . '/' .$logo['filename']);
            }
        }

        $company->setName($companyData['name']);
        $company->setRegistrationNumber($companyData['registration_number']);

        //delete current advertisement company
        $compTransRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Company');
        $qb = $compTransRepo->createQueryBuilder('t');
        $qb->delete()
            ->where('t.advertisement = :adv_id')
            ->setParameter('adv_id', $advertisement->getId())
            ->getQuery()
            ->execute();

        //insert new company translations
        $this->advertisementManager->populateObjectTranslations($company, 'VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation', 'description', $companyData);
        $company->setAdvertisement($advertisement);
        $this->em->persist($company);

        //Create contact person data
        $contactPerson = new ContactPerson();
        $contactPerson->setActualAddress($dataStep3['form']['contact_person']['actual_address']);
        $contactPerson->setAdvertisement($advertisement);
        $contactPerson->setEmail($dataStep3['form']['contact_person']['email']);
        $contactPerson->setFax($dataStep3['form']['contact_person']['fax']);
        $contactPerson->setName($dataStep3['form']['contact_person']['name']);
        $contactPerson->setPhone($dataStep3['form']['contact_person']['phone']);
        $contactPerson->setUrl($dataStep3['form']['contact_person']['url']);
        $contactPerson->setLegalAddress($dataStep3['form']['contact_person']['legal_address']);

        //delete current advertisement contact person
        $cpTransRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:ContactPerson');
        $qb = $cpTransRepo->createQueryBuilder('t');
        $qb->delete()
            ->where('t.advertisement = :adv_id')
            ->setParameter('adv_id', $advertisement->getId())
            ->getQuery()
            ->execute();

        $this->advertisementManager->populateObjectTranslations($contactPerson, 'VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation', 'position', $dataStep3['form']['contact_person']);
        $this->em->persist($contactPerson);


        $duties = array();
        $offers = array();
        $requirements = array();
        //group localized collections

        $defaultLocale = $this->getCurrentCountry()->getDefaultLocale();

        foreach ($this->getLocales() as $locale) {

            $f = 'duty_';
            foreach ($dataStep3['form'][$f . $locale] as $k => $v) {

                if ($defaultLocale == $locale) {
                    if (empty($v)) {
                        continue;
                    }
                } else {
                    if (empty($v)) {
                        $v = $dataStep3['form'][$f . $defaultLocale][$k];
                    }
                    if (empty($v)) {
                        continue;
                    }
                }

                $duties[$k]['description_' . $locale] = $v;
            }

            $f = 'offer_';
            foreach ($dataStep3['form']['offer_' . $locale] as $k => $v) {

                if ($defaultLocale == $locale) {
                    if (empty($v)) {
                        continue;
                    }
                } else {
                    if (empty($v)) {
                        $v = $dataStep3['form'][$f . $defaultLocale][$k];
                    }
                    if (empty($v)) {
                        continue;
                    }
                }

                $offers[$k]['description_' . $locale] = $v;
            }

            $f = 'requirement_';
            foreach ($dataStep3['form']['requirement_' . $locale] as $k => $v) {

                if ($defaultLocale == $locale) {
                    if (empty($v)) {
                        continue;
                    }
                } else {
                    if (empty($v)) {
                        $v = $dataStep3['form'][$f . $defaultLocale][$k];
                    }
                    if (empty($v)) {
                        continue;
                    }
                }

                $requirements[$k]['description_' . $locale] = $v;
            }
        }

        //remove current advertisement duties
        $dutRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Duty');
        $qb = $dutRepo->createQueryBuilder('t');
        $qb->delete()
            ->where('t.advertisement = :adv_id')
            ->setParameter('adv_id', $advertisement->getId())
            ->getQuery()
            ->execute();

        //Create new advertisement duties
        foreach ($duties as $dutyData) {
            $duty = new Duty();
            $this->advertisementManager->populateObjectTranslations($duty, 'VisiDarbi\AdvertisementBundle\Entity\DutyTranslation', 'description', $dutyData);
            $duty->setAdvertisement($advertisement);
            $this->em->persist($duty);
            $this->em->flush();
        }

        //remove current advertisement offers
        $offRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Offer');
        $qb = $offRepo->createQueryBuilder('t');
        $qb->delete()
            ->where('t.advertisement = :adv_id')
            ->setParameter('adv_id', $advertisement->getId())
            ->getQuery()
            ->execute();

        //Create new advertisement offers
        foreach ($offers as $offerData) {
            $offer = new Offer();
            $this->advertisementManager->populateObjectTranslations($offer, 'VisiDarbi\AdvertisementBundle\Entity\OfferTranslation', 'description', $offerData);
            $offer->setAdvertisement($advertisement);
            $this->em->persist($offer);
            $this->em->flush();
        }

        //remove requirements
        $reqRepo = $this->em->getRepository('VisiDarbiAdvertisementBundle:Requirement');
        $qb = $reqRepo->createQueryBuilder('t');
        $qb->delete()
            ->where('t.advertisement = :adv_id')
            ->setParameter('adv_id', $advertisement->getId())
            ->getQuery()
            ->execute();

        //Create requirements
        foreach ($requirements as $requirementData) {
            $requirement = new Requirement();
            $this->advertisementManager->populateObjectTranslations($requirement, 'VisiDarbi\AdvertisementBundle\Entity\RequirementTranslation', 'description', $requirementData);
            $requirement->setAdvertisement($advertisement);
            $this->em->persist($requirement);
            $this->em->flush();
        }

        $this->em->flush();
        $this->resetStepsBeforeSuccess(false);
        //Clear adv list cache
        $this->em->getConfiguration()->getResultCacheImpl()->deleteAll();
        //return $this->render('VisiDarbiAdvertisementBundle:EditAdvertisement:step5.html.twig', array());
        return $this->redirect($this->generateUrl('my_advertisments'));
    }

    protected function createTransactionForAdvertisement(\VisiDarbi\AdvertisementBundle\Entity\Advertisement $advertisement = null, $amount = null, $description = null, array $advertisements = null, array $advertisementsMobile = null) {

        $step1data = $this->session->get('step1');

        $paymentTransactions = false;
        if($advertisement){
            $paymentTransactions = $advertisement->getPaymentTransactions();
        }

        if (count($paymentTransactions) > 0 && $paymentTransactions) {
            $transaction = $paymentTransactions[0];
        }
        else {
            $transaction = new PaymentTransaction();
        }

        if($advertisement){
            $transaction->setAdvertisement($advertisement);
        }


        if (empty($description)) {
            $transaction->setDescription('Advertisement type: ' . $advertisement->getAdvertisementType()->getName());
        } else {
            $transaction->setDescription($description);
        }

        if (empty($amount)) {
            $transaction->setAmount($step1data['type']['price']);
        } else {
            $transaction->setAmount($amount);
        }

        if($advertisements && is_array($advertisements)){
            $transaction->setMergedAdvertisements($advertisements);
        }

        if($advertisementsMobile && is_array($advertisementsMobile)){
            $transaction->setMergedAdvertisementsMobile($advertisementsMobile);
        }

        if($advertisement){
            $requisites = $advertisement->getRequisites();

            $transaction->setRequisites($requisites);
        }

        $this->em->persist($transaction);
        return $transaction;
    }

    protected function resetStepsBeforeSuccess($resetPayNow = true) {
        $this->session->set('step1', null);
        $this->session->set('step2', null);
        $this->session->set('step_format', null);
        $this->session->set('format_files', null);
        $this->session->set('fromAdvId', null);
        $this->session->set('step3', null);
        $this->session->set('step4', null);
        $this->session->set('step5', true);
        if ($resetPayNow) {
            $this->session->set('payNow', null);
        }

    }



}

?>
