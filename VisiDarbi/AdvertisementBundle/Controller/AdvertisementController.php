<?php

namespace VisiDarbi\AdvertisementBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\HttpUtils;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\JsonResponse;

use Foolz\SphinxQL\Connection;

use VisiDarbi\CommonBundle\SphinxQL\SphinxQL;

use VisiDarbi\StatisticsBundle\Entity\Search;

use VisiDarbi\CommonBundle\Controller\FrontendController;

use VisiDarbi\AdvertisementBundle\Form\ShareAdvertisementType;
use VisiDarbi\AdvertisementBundle\Form\SubscribeAdvertisementType;


use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

use VisiDarbi\StatisticsBundle\Entity\Filter;
use VisiDarbi\StatisticsBundle\Entity\TextFilter;

use VisiDarbi\UserBundle\Entity\EmailSubscriptionSettings;

use VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory;

/**
 * AdvertisementController - advertisements front end controller
 *
 */
class AdvertisementController extends FrontendController {

    protected $adRepository;
    protected $translated;
    protected $currentCountry;
    protected $currentUser = null;

    protected $currentUserFavorites = array();

    protected $sortRoutesUrlValues = array();

    protected $redirectResponse = false;

    protected $allowSaveAdvertisement = true;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     * @DI\Inject("visidarbi.email_templating.sender")
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     * @DI\Inject("visidarbi.settings.manager")
     */
    protected $settingsManager;

    /**
     * @var \VisiDarbi\AdvertisementBundle\Manager\AdvertisementManagerInterface
     * @DI\Inject("visidarbi.advertisement_manager")
     */
    protected $advertisementManager;

    public function preExecute() {

        $this->adRepository = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement');

        $this->currentCountry = $this->getCurrentCountry();

        $this->translated = $this->get('translator');

        $userObject = $this->get('security.context')->getToken()->getUser();

        if ($userObject instanceof \VisiDarbi\UserBundle\Entity\User) {

            $this->currentUser = $userObject;

            $this->allowSaveAdvertisement = (bool) $this->currentUser->isPrivate();

            if($this->allowSaveAdvertisement){

                /**
                 * @todo  REFACTOR LOGIC and cache
                 */
                $favorites = $this->currentUser->getUserFavoriteAdvertisement();

                foreach ($favorites as $k => $v) {
                    $this->currentUserFavorites[$v->getAdvertisement()->getId()] = $v;
                }
            }

        }


    }

    public function indexAction(Request $request) {
        $response = $this->forward('VisiDarbiAdvertisementBundle:Advertisement:list', array());

        return $response;
    }


    public function listAction_new(Request $request) {
        $em = $this->getDoctrine()->getEntityManager();
        $textFilter = false;
        $filterSearch = false;

        $filtered = false;
        $searched = false;
        $this->sortRoutesUrlValues = $this->getPaginationRoutesUrlValues();

        $filterValue = $request->get('filter', null);

        $filterSearch = $this->advertisementManager->getDecodedFilter($filterValue);

        if(!$filterSearch && $filterValue){
            return $this->redirect( $this->generateUrl('visidarbi_advertisement_list'), 302 );
        }

        $whatValue = false;
        $whatSlug = false;

        $whereValue = false;
        $whereSlug = false;


        $sphinxTotalcount = false;

        $sphinxFilter = false;
        //Process What / Where field values and generated slugs
        if ($request->get('what') || $request->get('where')) {
            $searched = true;

            $session = $this->container->get('session');

            $searchSesion = $session->getFlashBag()->get('searchFields');
            $whatSession = isset($searchSesion[0]['what']) ? $searchSesion[0]['what'] : false;
            $whereSession = isset($searchSesion[0]['where']) ? $searchSesion[0]['where'] : false;
            $userManualSearch = isset($searchSesion[0]['manualSearch']) ? true : false;

            if( ($whatSession || $whereSession) && !$userManualSearch){

                if(!empty($whatSession)){
                    $whatValue = isset($whatSession['value']) ? $whatSession['value'] : false;
                    $whatSlug = isset($whatSession['slug']) ? $whatSession['slug'] : false;
                }
                elseif(!empty($whereSession)){
                    $whereValue = isset($whereSession['value']) ? $whereSession['value'] : false;
                    $whereSlug = isset($whereSession['slug']) ? $whereSession['slug'] : false;
                }
            }
            else{

                $whatValues = $this->advertisementManager->processTextSearchSlug('what', $request->get('what'), $this->getLocale(), $whatSession );

                $whatValue = $whatValues['value'];
                $whatSlug = $whatValues['slug'];

                $whereValues = $this->advertisementManager->processTextSearchSlug('where', $request->get('where'), $this->getLocale(), $whereSession);

                $whereValue = $whereValues['value'];
                $whereSlug = $whereValues['slug'];


            }

            //Inspect if slug value in request is equal to generated, if not -> redirect to list
            if($request->get('what') != $whatSlug || $request->get('where') != $whereSlug || (!$whatValue && !$whereValue) ){
                return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
            }

            $session->getFlashBag()->set('searchFields', array(
                'what' => array( 'slug' => $whatSlug, 'value' => $whatValue ),
                'where' => array( 'slug' => $whereSlug, 'value' => $whereValue )
            ));

            if($this->currentUser && ($whereValue || $whatValue)) {
                $this->storeSearchHistory($whatValue, $whereValue, $this->currentUser);
            }

            $request->request->set('searchWhat', $whatValue);
            $request->request->set('searchWhere', $whereValue);

        }



        $sphinxResults = $this->getSphinxResults(
            array(
                'what' => $whatValue,
                'where' => $whereValue,
                'filter' => $filterSearch,
                'page' => $request->get('page', 1),
                'sort_by' => $this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field'),
                'sort_direction' => $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction')
            )
        );

        $textFilter = $sphinxResults['ids'];
        $sphinxTotalcount = $sphinxResults['total'];
        $sphinxFilter = $sphinxResults['filter'];


        $advertisements = $em->createQueryBuilder('adv')

            ->select("advertisement, company, adcountry, adcity, externalAd, resource, favorite, slugs")
            //->select(')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')

            ->leftJoin('advertisement.companies', 'company')
            ->join('advertisement.AdvertisementCountry', 'adcountry')
            ->leftJoin('advertisement.AdvertisementCity', 'adcity')
            //->leftJoin('advertisement.profession', 'profession')
            //->leftJoin('advertisement.category', 'category')
            ->leftJoin('advertisement.externalAdvertisement', 'externalAd')
            ->leftJoin('externalAd.externalAdvertisementSource', 'resource' )
            ->leftJoin('advertisement.userFavoriteAdvertisements', 'favorite')
            ->leftJoin('advertisement.slugs', 'slugs')

            ->andWhere(' resource.show_advertisiment = 1 ')
            ->orWhere(' resource.show_advertisiment IS NULL ')
            ->andWhere('advertisement.country = :country')
            ->andWhere('advertisement.status = :status')
            ->setParameter('country', $this->currentCountry->getId())
            ->setParameter('status', Advertisement::STATUS_ACTIVE)
            ->groupBy('advertisement.id')
        ;

        $advertisements->andWhere('advertisement.id IN (:adids)');
        $advertisements->setParameter('adids', $textFilter);


        if($this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field') == 'advertisement.date_from' && $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction') == 'desc'){
            $advertisements->addOrderBy('advertisement.is_highlighted', 'DESC');
        }

        //Manage sorting query
        if (!$request->get('sort') && !$filterSearch) {
            $advertisements->addOrderBy('advertisement.date_from', 'DESC');

        }  else if (!$request->get('sort') && $filterSearch) {
            $advertisements->addOrderBy('advertisement.date_from', 'DESC');

        }
        elseif($this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field') && $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction')) {
            $advertisements->addOrderBy(
                $this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field'),
                $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction')
            );
        }

        $advertisements = $advertisements->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->setHint('knp_paginator.count', $sphinxTotalcount);

        //END OF SELECT AND PROCESS ADVERTISEMENT LIST
        $advertisements->useResultCache(true, 600, 'advertisement_list_'.$this->getLocale().'_'.md5(serialize($advertisements->getParameters())));

        // Initialize ShareForm
        $shareForm = $this->createForm(new ShareAdvertisementType());

        if ($this->currentUser) {
            $shareForm->setData(
                array(
                    'email_from' => $this->currentUser->getEmail()
                )
            );
        }

        // Initialize and set default options for Paginator
        $paginator = $this->getAdvertisementPagination($request, $advertisements);

        $pagination = $paginator['pagination'];
        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */

        //Initialize Subsribe Form
        $subscribeForm = $this->createForm(new SubscribeAdvertisementType());

        if ($this->currentUser) {
            $subscribeForm->setData(
                array(
                    'email' => $this->currentUser->getEmail()
                )
            );
        }


        if($this->redirectResponse ){
            return $this->redirect($this->redirectResponse['url'], $this->redirectResponse['status']);
        }

        return $this->render(
            'VisiDarbiAdvertisementBundle:Advertisement:list.html.twig',
            array(

                'shareForm' => $shareForm->createView(),
                'subscribeForm' => $subscribeForm->createView(),
                'favorites' => $this->currentUserFavorites,
                'allowSaveAdvertisement' => $this->allowSaveAdvertisement,

                'pagination' => $pagination,
                'paginationPageUrl' => $paginator['page_route'],
                'paginationSortUrl' => $paginator['sort_route'],

                'sortKeysValues' => $this->sortRoutesUrlValues['sortKeys'],

                'filterData' => $this->getSphinxFilters( $sphinxFilter),
                'settledFilters' => $filterSearch,

                'filtered' => $filtered,
                'search' => $searched,

                'searchWhat' => $whatValue,
                'searchWhere' => $whereValue,

                'searchWhatSlug' => $whatSlug,
                'searchWhereSlug' => $whereSlug,

                'filterKey' => $filterValue,

                'user' => $this->currentUser
            )
        );

    }

    protected function getSphinxResults($params){
        $config = $this->container->getParameter('sphinx');

        if (empty($params)) {
            return array();
        }

        if (! empty($params['what']) || ! empty($params['where'])) {

            if (! empty($params['what'])) {

                $params['what'] = trim($params['what']);
                $keyword = new Search();
                //Store keyword
                $keyword->setKeywordType(Search::KEYWORD_TYPE_WHAT);
                $keyword->setKeyword($params['what']);
                $this->getDoctrine()->getEntityManager()->persist($keyword);

            }

            if (! empty($params['where'])) {

                $params['where'] = trim($params['where']);
                $keyword = new Search();
                //Store keyword
                $keyword->setKeywordType(Search::KEYWORD_TYPE_WHERE);
                $keyword->setKeyword($params['where']);
                $this->getDoctrine()->getEntityManager()->persist($keyword);
            }

            $this->getDoctrine()->getEntityManager()->flush();
        }

        $conn = new Connection();
        $query = SphinxQL::forge($conn);


        $return = array();


        $return['ids'] = array();
        $result = $this->sphinxSearch($params, $query, $config, 'id');
        foreach ($result as $v) {
            $return['ids'][] = $v['id'];
        }


        $return['total'] = count($this->sphinxSearch($params, $query, $config, 'total'));

        $return['filter']['profession']=array();
        $result = $this->sphinxSearch($params, $query, $config, 'profession_id');
        foreach ($result as $v) {
            $return['filter']['profession'][$v['@groupby']] = $v['@count'];
        }

        $return['filter']['city']=array();
        $result = $this->sphinxSearch($params, $query, $config, 'city_id');
        foreach ($result as $v) {
            $return['filter']['city'][$v['@groupby']] = $v['@count'];
        }

        $return['filter']['source']=array();
        $result = $this->sphinxSearch($params, $query, $config, 'source_id');
        foreach ($result as $v) {
            $return['filter']['source'][$v['@groupby']] = $v['@count'];
        }

        $return['filter']['company']=array();
        $result = $this->sphinxSearch($params, $query, $config, 'company_id');
        foreach ($result as $v) {
            $return['filter']['company'][$v['@groupby']] = $v['@count'];
        }

        $return['filter']['country']=array();
        $result = $this->sphinxSearch($params, $query, $config, 'country_id');
        foreach ($result as $v) {
            $return['filter']['country'][$v['@groupby']] = $v['@count'];
        }

        $return['filter']['category']=array();
        $result = $this->sphinxSearch($params, $query, $config, 'category_id');
        foreach ($result as $v) {
            $return['filter']['category'][$v['@groupby']] = $v['@count'];
        }

        return $return;

    }

    protected function sphinxSearch($params, $query, $config, $type = 'id'){

        switch ($type){
            case "id":
            case "total":
                $query->selectFromStr('@id, *');
                $query->from($config['indexName']);

                if(!empty($params['filter'])){

                    $filter = $params['filter'];

                    if(!empty($filter['countries'])){
                        $query->where('country_id','IN',array_keys($filter['countries']));
                    }

                    if(!empty($filter['cities'])){
                        $query->where('city_id','IN',array_keys($filter['cities']));
                    }

                    if(!empty($filter['resources'])){
                        $query->where('source_id','IN',array_keys($filter['resources']));
                    }

                    if(!empty($filter['companies'])){
                        $query->where('company_slug_id','IN',array_keys($filter['companies']));
                    }

                    if(!empty($filter['positions'])){
                        $query->where('category_id','IN',array_keys($filter['positions']));
                    }

                }
                break;
            default:
                $query->selectFromStr('@groupby, @count, @count as skaits');
                $query->from($config['indexName']);

            break;
        }

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {
            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }




        switch ($type){
            case "id":
                $order = 'asc';
                if($params['sort_direction']=='desc'){ $order = 'desc'; }

                switch($params['sort_by']){
                    case 'company.name':
                        $query->orderBy('cname', $order);
                        break;
                    case 'resource.resource':
                        $query->orderBy('aresource', $order);
                        break;
                    case 'advertisement.title':
                        $query->orderBy('atitle', $order);
                        break;
                    case 'advertisement.date_from':
                    default:
                        if($order == 'desc'){
                            $query->orderBy('is_highlighted', $order);
                        }
                        $query->orderBy('date_from', $order);
                        break;
                }

                $perpage = 10;
                $start_from = (empty($params['page'])?1:(int) $params['page'])*$perpage-$perpage;
                $query->limit($start_from, $perpage);
                $query->option('max_matches', $config['maxResults']);
                break;
            case "total":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                break;
            case "profession_id":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                $query->groupBy("profession_id");
                $query->orderBy('skaits', 'DESC');
                break;
            case "city_id":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                $query->groupBy("city_id");
                $query->orderBy('skaits', 'DESC');
                break;

            case "source_id":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                $query->groupBy("source_id");
                $query->orderBy('skaits', 'DESC');
                break;

            case "company_id":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                $query->groupBy("min_company_id");
                $query->orderBy('skaits', 'DESC');
                break;

            case "country_id":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                $query->groupBy("country_id");
                $query->orderBy('skaits', 'DESC');
                break;

            case "category_id":
                $query->limit(0, $config['maxResults']);
                $query->option('max_matches', $config['maxResults']);
                $query->groupBy("category_id");
                $query->orderBy('skaits', 'DESC');
                break;
        }
        return $query->execute();

    }

    protected function getSphinxFilters($searchFilter){
        $filterData = array();

        $resources = $this->adRepository->getResourcesListByID($this->currentCountry, null, $searchFilter['source']);
        $companies = $this->adRepository->getCompaniesListForFilterByID($this->currentCountry, null, $searchFilter['company']);
        $categories = $this->adRepository->getCategoriesListForFilterByID($this->currentCountry, $this->getLocale(), null, $searchFilter['category']);
        $cities = $this->adRepository->getCitiesListForFilterByID($this->currentCountry->getDeafultAdvertisementCountry(), $this->getLocale(), null, null, $searchFilter['city']);
        $countries = $this->adRepository->getCountriesListForFilterByID($this->currentCountry->getDeafultAdvertisementCountry(), $this->getLocale(), null, $searchFilter['country']);


        $filterData['resources'] = $resources->useResultCache(true, 600, 'advertisement_list_resource_filter_'.md5(serialize($resources->getParameters())))
            ->getResult();

        $filterData['companies'] = $companies->useResultCache(true, 600, 'advertisement_list_companies_filter_'.md5(serialize($companies->getParameters())))
            ->getResult();

        $filterData['categories'] = $categories->useResultCache(true, 600, 'advertisement_list_categories_filter_'.$this->getLocale().'_'.md5(serialize($categories->getParameters())))
            ->getResult();

        $filterData['cities'] = $cities->useResultCache(true, 600, 'advertisement_list_cities_filter_'.$this->getLocale().'_'.md5(serialize($cities->getParameters())))
            ->getResult();

        $filterData['countries'] = $countries->useResultCache(true, 600, 'advertisement_list_countries_filter_'.$this->getLocale().'_'.md5(serialize($cities->getParameters())))
            ->getResult();


        foreach($filterData['companies'] as $rid => $obj){
            $filterData['companies'][$rid]['adv_count'] = $searchFilter['company'][$obj['id']];
        }
        foreach($filterData['categories'] as $rid => $obj){
            $filterData['categories'][$rid]['adv_count'] = $searchFilter['category'][$obj['category_id']];
        }
        foreach($filterData['cities'] as $rid => $obj){
            $filterData['cities'][$rid]['adv_count'] = $searchFilter['city'][$obj['city_id']];
        }
        foreach($filterData['countries'] as $rid => $obj){
            $filterData['countries'][$rid]['adv_count'] = $searchFilter['country'][$obj['country_id']];
        }
        return $filterData;


    }


    public function listAction(Request $request) {

        $em = $this->getDoctrine()->getEntityManager();

        $textFilter = false;
        $filterSearch = false;

        $filtered = false;
        $searched = false;

        $this->sortRoutesUrlValues = $this->getPaginationRoutesUrlValues();

        //Get Filter values
        $filterValue = $request->get('filter', null);

        $filterSearch = $this->advertisementManager->getDecodedFilter($filterValue);

        if(!$filterSearch && $filterValue){
            return $this->redirect( $this->generateUrl('visidarbi_advertisement_list'), 302 );
        }

        //SELECT AND PROCESS ADVERTISEMENT LIST
        $advertisements = $em->createQueryBuilder('adv')

                ->select("advertisement, company, adcountry, adcity, externalAd, resource, favorite, slugs")
                //->select(')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')

                ->leftJoin('advertisement.companies', 'company')
                ->join('advertisement.AdvertisementCountry', 'adcountry')
                ->leftJoin('advertisement.AdvertisementCity', 'adcity')
                //->leftJoin('advertisement.profession', 'profession')
                //->leftJoin('advertisement.category', 'category')
                ->leftJoin('advertisement.externalAdvertisement', 'externalAd')
                ->leftJoin('externalAd.externalAdvertisementSource', 'resource' )
                ->leftJoin('advertisement.userFavoriteAdvertisements', 'favorite')
                ->leftJoin('advertisement.slugs', 'slugs')

                ->andWhere(' resource.show_advertisiment = 1 ')
                ->orWhere(' resource.show_advertisiment IS NULL ')
                ->andWhere('advertisement.country = :country')
                ->andWhere('advertisement.status = :status')
                ->setParameter('country', $this->currentCountry->getId())
                ->setParameter('status', Advertisement::STATUS_ACTIVE)
                ->groupBy('advertisement.id')
        ;
        $advertCount = $em->createQueryBuilder()
            ->select('DISTINCT advertisement.id')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'advertisement')
            ->leftJoin('advertisement.companies', 'company')
            ->join('advertisement.AdvertisementCountry', 'adcountry')
            ->leftJoin('advertisement.AdvertisementCity', 'adcity')
            //->leftJoin('advertisement.profession', 'profession')
            //->leftJoin('advertisement.category', 'category')
            ->leftJoin('advertisement.externalAdvertisement', 'externalAd')
            ->leftJoin('externalAd.externalAdvertisementSource', 'resource' )
            ->leftJoin('advertisement.userFavoriteAdvertisements', 'favorite')
            ->leftJoin('advertisement.slugs', 'slugs')
            ->andWhere(' resource.show_advertisiment = 1 ')
            ->orWhere(' resource.show_advertisiment IS NULL ')
            ->andWhere('advertisement.country = :country')
            ->andWhere('advertisement.status = :status')
            ->setParameter('country', $this->getCurrentCountry()->getId())
            ->setParameter('status', Advertisement::STATUS_ACTIVE)
           // ->groupBy('advertisement.id')
        ;



        $whatValue = false;
        $whatSlug = false;

        $whereValue = false;
        $whereSlug = false;


        $sphinxTotalcount = 0;

        $sphinxFilter = false;
        //Process What / Where field values and generated slugs
        if ($request->get('what') || $request->get('where')) {
            $searched = true;

            $session = $this->container->get('session');

            $searchSesion = $session->getFlashBag()->get('searchFields');
            $whatSession = isset($searchSesion[0]['what']) ? $searchSesion[0]['what'] : false;
            $whereSession = isset($searchSesion[0]['where']) ? $searchSesion[0]['where'] : false;
            $userManualSearch = isset($searchSesion[0]['manualSearch']) ? true : false;

            if( ($whatSession || $whereSession) && !$userManualSearch){

                if(!empty($whatSession)){
                    $whatValue = isset($whatSession['value']) ? $whatSession['value'] : false;
                    $whatSlug = isset($whatSession['slug']) ? $whatSession['slug'] : false;
                }
                elseif(!empty($whereSession)){
                    $whereValue = isset($whereSession['value']) ? $whereSession['value'] : false;
                    $whereSlug = isset($whereSession['slug']) ? $whereSession['slug'] : false;
                }
            }
            else{

                $whatValues = $this->advertisementManager->processTextSearchSlug('what', $request->get('what'), $this->getLocale(), $whatSession );

                $whatValue = $whatValues['value'];
                $whatSlug = $whatValues['slug'];

                $whereValues = $this->advertisementManager->processTextSearchSlug('where', $request->get('where'), $this->getLocale(), $whereSession);

                $whereValue = $whereValues['value'];
                $whereSlug = $whereValues['slug'];


            }

            //Inspect if slug value in request is equal to generated, if not -> redirect to list
            if($request->get('what') != $whatSlug || $request->get('where') != $whereSlug || (!$whatValue && !$whereValue) ){
                return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
            }

            $session->getFlashBag()->set('searchFields', array(
                'what' => array( 'slug' => $whatSlug, 'value' => $whatValue ),
                'where' => array( 'slug' => $whereSlug, 'value' => $whereValue )
            ));

            //$textFilter = $this->getSearchFilter(array('what' => $whatValue, 'where' => $whereValue));

  	        $sphinxResults = $this->getSearchFilter(array('what' => $whatValue, 'where' => $whereValue,
                'page' => $request->get('page', 1),
                'sort_by' => $this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field'),
                'sort_direction' => $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction')
            ));

            $textFilter = $sphinxResults['ids'];
            $sphinxTotalcount = $sphinxResults['total'];
            $sphinxFilter = $sphinxResults['filter'];

            if($this->currentUser && ($whereValue || $whatValue)) {
                $this->storeSearchHistory($whatValue, $whereValue, $this->currentUser);
            }

            if (!empty($textFilter)) {
                $advertisements->andWhere('advertisement.id IN (:adids)');
                $advertisements->setParameter('adids', $textFilter);

                $advertCount->andWhere('advertisement.id IN (:adids)');
                $advertCount->setParameter('adids', $textFilter);

            }
            else{
                $advertisements->andWhere('true = false');

                $advertCount->andWhere('true = false');
            }

            $request->request->set('searchWhat', $whatValue);
            $request->request->set('searchWhere', $whereValue);

        }

        //Process Filter
        //filtering
        if (isset($filterSearch['cities']) && !empty($filterSearch['cities'])) {
            $filtered = true;
            $advertisements->andWhere('adcity.id IN (:adcityid)');
            $advertisements->setParameter('adcityid', array_keys($filterSearch['cities']));

            $advertCount->andWhere('adcity.id IN (:adcityid)');
            $advertCount->setParameter('adcityid', array_keys($filterSearch['cities']));
        }

        if (isset($filterSearch['countries']) && !empty($filterSearch['countries'])) {
            $filtered = true;
            $advertisements->andWhere('adcountry.id IN (:adcountryid)');
            $advertisements->setParameter('adcountryid', array_keys($filterSearch['countries']));

            $advertCount->andWhere('adcountry.id IN (:adcountryid)');
            $advertCount->setParameter('adcountryid', array_keys($filterSearch['countries']));
        }

        if (isset($filterSearch['positions']) && !empty($filterSearch['positions'])) {
            $filtered = true;
            $advertisements->leftJoin('advertisement.Categories', 'ac');
            $advertisements->andWhere('ac IN (:categoryid)');
            $advertisements->setParameter('categoryid', array_keys($filterSearch['positions']));

            $advertCount->leftJoin('advertisement.Categories', 'ac');
            $advertCount->andWhere('ac IN (:categoryid)');
            $advertCount->setParameter('categoryid', array_keys($filterSearch['positions']));
        }


        if (isset($filterSearch['professions']) && !empty($filterSearch['professions'])) {
            $filtered = true;
            $advertisements->leftJoin('advertisement.Professions', 'ap');
            //$advertisements->leftJoin('ap.translations', 'apt');
            //$advertisements->andWhere('apt.slug IN (:professionslug)');
            $advertisements->andWhere('ap.id IN (:professionsIds)');
            $advertisements->setParameter('professionsIds', array_keys($filterSearch['professions']));

            $advertCount->leftJoin('advertisement.Professions', 'ap');
            //$advertisements->leftJoin('ap.translations', 'apt');
            //$advertisements->andWhere('apt.slug IN (:professionslug)');
            $advertCount->andWhere('ap.id IN (:professionsIds)');
            $advertCount->setParameter('professionsIds', array_keys($filterSearch['professions']));
        }

        if (isset($filterSearch['companies']) &&  !empty($filterSearch['companies'])) {
            $filtered = true;

            /**
             * @todo remove code below when Company entity logic will be fixed (duplicate rows currently)
             */
            $companiesArray = $this->getDoctrine()
                            ->getEntityManager()
                            ->createQueryBuilder('c')
                            ->select('c.slug')
                            ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
                            ->where('c.id IN (:cids)')
                            ->setParameter('cids', array_keys($filterSearch['companies']))
                            ->getQuery()
                            ->useResultCache(true, 1000, 'advertisement_companies_filter_'.md5(serialize($advertisements->getParameters())).'_'.$this->getCurrentCountry()->getId().'_'.$this->getLocale())
                            ->getResult();

            $filterSearch['company_slug'] = array();
            foreach ($companiesArray as $v) {
                $filterSearch['company_slug'][] = $v['slug'];
            }

            $advertisements->andWhere('company.slug IN (:companySlugs)');
            $advertisements->setParameter('companySlugs', $filterSearch['company_slug']);


            $advertCount->andWhere('company.slug IN (:companySlugs)');
            $advertCount->setParameter('companySlugs', $filterSearch['company_slug']);

            /**
             * @todo return code below back when Company entity logic will be fixed (duplicate rows currently)
             */
            /*
            $advertisements->andWhere('company.id IN (:companyIds)');
            $advertisements->setParameter('companyIds', array_keys($filterSearch['companies']));             *
             */
        }

        if (isset($filterSearch['resources']) &&  !empty($filterSearch['resources'])) {
            $filtered = true;
            $advertisements->andWhere('resource.id IN (:resourceid)');
            $advertisements->setParameter('resourceid', array_keys($filterSearch['resources']));

            $advertCount->andWhere('resource.id IN (:resourceid)');
            $advertCount->setParameter('resourceid', array_keys($filterSearch['resources']));
        }


            if($this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field') == 'advertisement.date_from' && $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction') == 'desc'){
                $advertisements->addOrderBy('advertisement.is_highlighted', 'DESC');
            }

            //Manage sorting query
            if (!$request->get('sort') && !$filterSearch) {
                $advertisements->addOrderBy('advertisement.date_from', 'DESC');

             }  else if (!$request->get('sort') && $filterSearch) {
                $advertisements->addOrderBy('advertisement.date_from', 'DESC');

            }
            elseif($this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field') && $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction')) {
                $advertisements->addOrderBy(
                    $this->getPaginationSortValue($request, 'visidarbi_advertisement_list' , 'field'),
                    $this->getPaginationDirectionValue($request, 'visidarbi_advertisement_list' , 'direction')
                );
            }

        $advertisements = $advertisements->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getLocale())
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        $count = $advertCount->getQuery()->getResult();



        if(is_array($count)){
            $count = count($count);
        } elseif($count) {
            $count = (int) $count;
        }else{
            $count = 0;
        }
        $advertisements->setHint('knp_paginator.count', $count);


        $params = $advertisements->getParameters();

        //END OF SELECT AND PROCESS ADVERTISEMENT LIST
        $advertisements->useResultCache(true, 600, 'advertisement_list_'.$this->getLocale().'_'.md5(serialize($advertisements->getParameters())));

        // Initialize ShareForm
        $shareForm = $this->createForm(new ShareAdvertisementType());

        if ($this->currentUser) {
            $shareForm->setData(
                array(
                    'email_from' => $this->currentUser->getEmail()
                )
            );
        }

        // Initialize and set default options for Paginator
        $paginator = $this->getAdvertisementPagination($request, $advertisements);

        $pagination = $paginator['pagination'];
        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */



        //Initialize Subsribe Form
        $subscribeForm = $this->createForm(new SubscribeAdvertisementType());

        if ($this->currentUser) {
            $subscribeForm->setData(
                array(
                    'email' => $this->currentUser->getEmail()
                )
            );
        }


        if($this->redirectResponse ){
            return $this->redirect($this->redirectResponse['url'], $this->redirectResponse['status']);
        }
        $pageData = $pagination->getPaginationData();
        return $this->render(
            'VisiDarbiAdvertisementBundle:Advertisement:list.html.twig',
            array(

                'shareForm' => $shareForm->createView(),
                'subscribeForm' => $subscribeForm->createView(),
                'favorites' => $this->currentUserFavorites,
                'allowSaveAdvertisement' => $this->allowSaveAdvertisement,

                'pagination' => $pagination,
                'paginationPageUrl' => $paginator['page_route'],
                'paginationSortUrl' => $paginator['sort_route'],

                'sortKeysValues' => $this->sortRoutesUrlValues['sortKeys'],

                'filterData' => $this->getFiltersData($textFilter, $sphinxFilter),
                'settledFilters' => $filterSearch,

                'filtered' => $filtered,
                'search' => $searched,

                'searchWhat' => $whatValue,
                'searchWhere' => $whereValue,

                'searchWhatSlug' => $whatSlug,
                'searchWhereSlug' => $whereSlug,

                'filterKey' => $filterValue,

                'user' => $this->currentUser
            )
        );

    }

    /**
     * Open advertisement action
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function showAction(Request $request) {


        $advertisementId = $request->get('advertisementId', null);

        $advertisement = $this->getDoctrine()
                ->getEntityManager()->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                ->getActiveAdvertisementByPublicIdOrSlug($advertisementId);

        if (!$advertisement) {
            return $this->redirect($this->generateUrl('visidarbi_advertisement_list'));
        }

        $statisticManager = $this->get('visidarbi.statistics.view_counter');
        $statisticManager->registerAdvertisementView($advertisement);


        //If it's external advertisement
        if($advertisement->getExternalAdvertisement()) {

            $url = $advertisement->getExternalAdvertisement()->getLink();
            $isUrlSecured = preg_match("/^https/", $url);

            if ( ($request->isSecure() && $isUrlSecured) || (! $request->isSecure() && ! $isUrlSecured) ){
                $url = preg_replace("(^http?://)","//",$advertisement->getExternalAdvertisement()->getLink());
            }
            elseif (! $isUrlSecured && $request->isSecure()){
                return $this->redirect( $this->generateUrl('visidarbi_advertisement_show_non_secure', array('advertisementId'=> $advertisementId) ), 302 );
            }


            return $this->render('VisiDarbiAdvertisementBundle:Advertisement:show_external.html.twig', array(
                        'advertisement' => $advertisement,
                        'url' => $url,
                ));
        }


        $shareForm = $this->createForm(new ShareAdvertisementType());



        if ($this->currentUser) {
            $shareForm->setData(array('email_from' => $this->currentUser->getEmail()));
        }

        return $this->render('VisiDarbiAdvertisementBundle:Advertisement:show.html.twig', array(
                    'open_ad' => 1,
                    'user' => $this->currentUser,
                    'shareForm' => $shareForm->createView(),
                    'locale' => $this->getLocale(),
                    'advertisement' => $advertisement,
                    'advertisementManager' => $this->advertisementManager,
                    'allowSaveAdvertisement' => $this->allowSaveAdvertisement,
                    'favorites' => $this->currentUserFavorites,));
    }

     /**
     * Save/remove advertisement in favorites
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response A Response instance
     */
    public function saveAction(Request $request) {

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index'), 302 );
        }

        $response = array('error' => 1);

        if (($this->currentUser instanceof \VisiDarbi\UserBundle\Entity\User) && $this->currentUser->isPrivate() && $request->get('id', null)) {

            $advertisementId = $request->get('id', null);

            $advertisement = $this->getDoctrine()
                    ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                    ->findOneBy(array('publicId' => $advertisementId));

            if ($advertisement) {

                $record = $this->getDoctrine()
                        ->getRepository('VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement')
                        ->findOneBy(
                            array(
                                'User' => $this->currentUser,
                                'advertisement' => $advertisement
                            )
                        );

                if (!$record) {
                    $action = 'saved';

                    $entity = new \VisiDarbi\UserBundle\Entity\UserFavoriteAdvertisement();
                    $entity->setUser($this->currentUser);
                    $entity->setAdvertisement($advertisement);

                    $this->getDoctrine()->getEntityManager()->persist($entity);
                    $this->getDoctrine()->getEntityManager()->flush();

                }
                else {
                    $action = 'removed';

                    $this->getDoctrine()->getEntityManager()->remove($record);
                    $this->getDoctrine()->getEntityManager()->flush();
                }

                $response = array('success' => 1, 'action' => $action);
            }
        }

        return new Response(
            json_encode( $response ),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    public function shareAction(Request $request) {

        $result = array('errors' => null,
            'success' => 0);


        $shareForm = $this->createForm(new ShareAdvertisementType());

        if ($request->isMethod('POST')) {
            $shareForm->bindRequest($request);

            if ($shareForm->isValid()) {

                $data = $shareForm->getData();

                $advertisement = $this->getDoctrine()
                        ->getRepository('VisiDarbi\AdvertisementBundle\Entity\Advertisement')
                        ->findOneBy(array('publicId' => $data['share_id']));

                $externalAdvertisement = $advertisement->getExternalAdvertisement();

                if($externalAdvertisement instanceof \VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisement) {

                    $shareLink = $externalAdvertisement->getLink();

                } else {

                    $shareLink = $this->generateUrl('visidarbi_advertisement_show', array('advertisementId' => $data['share_id']), true);

                }

                $this->emailSender->send(
                        $this->getCurrentCountry(),
                        $request->getLocale(),
                        EmailTemplate::ID_SHARE_ADVERTISEMENT,
                        $data['email_to'],

                        array(
                        'shareLink' => sprintf('<a href="%s">%s</a>', $shareLink, $shareLink),
                        'emailFrom' =>  $data['email_from'],
                        'message' =>  $data['message'],

                        ), array(
                        'email' => $this->settingsManager->get('emails.sender_email'),
                        'name' => $this->settingsManager->get('emails.sender_name')
                        )
                );

                $result['success'] = 1;

            } else {
                $errorsSet = array();
                $errors = $shareForm->getErrors();


               foreach($shareForm as $k=>$v){
                   if($v->hasErrors()){
                       foreach($v->getErrors() as $e){
                            $errorsSet[$k][]= $this->translated->trans(/** @Ignore */ $e->getMessage());
                        }
                   }
               }

                $result['errors'] = $errorsSet;
            }

        }

        return new Response(json_encode($result), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Subscribe by selected filter
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function subscribeAction(Request $request) {

        if(!$request->isXmlHttpRequest() || ( $this->currentUser && !$this->currentUser->isPrivate() ) ){
            throw new NotFoundHttpException('Access denied.');
        }

        $result = array();

        if($request->getMethod() == 'POST') {
            $subscribeForm = $this->createForm(new SubscribeAdvertisementType(), null, array(
                'user_authorized' => ($this->currentUser ? true : false)
            ));
            $subscribeForm->bindRequest($request);

            if($subscribeForm->isValid()) {
                $data = $subscribeForm->getData();
                $filters = $this->advertisementManager->getDecodedFilter($data['filter']);

                $this->subscribeOnAdvertisements($data, $filters);

                $result['success'] = 1;
            }
            else {
                $errorsSet = array();
                foreach($subscribeForm as $k=>$v){
                    if($v->hasErrors()){
                        foreach($v->getErrors() as $e){
                            $errorsSet[$k][]= $this->translated->trans(/** @Ignore */ $e->getMessage());
                        }
                    }
                }

                $result['errors'] = $errorsSet;
            }
        }

        return new JsonResponse($result);
    }

    /**
     * Subscribe on advertisements by e-mail
     * @param string $email
     * @param array $filtersData
     */
    protected function subscribeOnAdvertisements($formData, $filtersData) {

        $em = $this->getDoctrine()->getEntityManager();
        $subscriberEntity = false;
        if($this->currentUser){
            $subscriberEntity = new EmailSubscriptionSettings();
            $subscriberEntity->setUser($this->currentUser);
        }
        else{
            $subscriberEntity = new EmailSubscriptionSettings();
            $subscriberEntity->setEmail($formData['email']);
        }

        $subscriberEntity->setLanguage($this->getLocale());

        /**
         * Process form data, lets check if such parametrs exists
         */

        //CATEGORIES
        $categories = $em->getRepository('VisiDarbiProfessionBundle:Category')
                        ->createQueryBuilder('c');
        if(isset($filtersData['positions']) && !empty($filtersData['positions'])){
            $categories->where('c.id in (:ids)');
            $categories->setParameter('ids', $filtersData['positions']);
        }


        $categories = $categories->getQuery()->getResult();

        //COUNTRIES
        $countries = $em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCountry')
                    ->createQueryBuilder('c');
        if(isset($filtersData['countries']) && !empty($filtersData['countries'])){
            $countries->where('c.id in (:ids)');
            $countries->setParameter('ids', array_keys($filtersData['countries']));
        }

        $countries = $countries->getQuery()->getResult();

        //CITIES
        $cities = $em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCity')
                    ->createQueryBuilder('c');
        if(isset($filtersData['cities']) && !empty($filtersData['cities'])){
            $cities->where('c.id in (:ids)');
            $cities->setParameter('ids', array_keys($filtersData['cities']));
        }

        if(isset($filtersData['countries']) && !empty($filtersData['countries']) && (isset($filtersData['cities']) && empty($filtersData['cities']))  ){
            $cities = array();
        }
        else{
            $cities = $cities->getQuery()->getResult();
        }


        //SOURCES
        $sources = $em->getRepository('VisiDarbiExternalAdvertisementBundle:ExternalAdvertisementSource')
                    ->createQueryBuilder('s');
        if(isset($filtersData['resources']) && !empty($filtersData['resources'])){
            $sources->where('s.id in (:ids)');
            $sources->setParameter('ids', array_keys($filtersData['resources'])) ;
        }


        $sources = $sources->getQuery()->getResult();

        /**
         * Save user newsletter settings
         */

        //CATEGORIES AND PROFESSIONS
        $subscriberEntity->getCategories()->clear();
        $subscriberEntity->getProfessions()->clear();

        foreach ($categories as $category) {
            $subscriberEntity->addCategorie($category);

            $professions = $category->getProfessions();
            if($professions){
                foreach ($professions as $profession) {
                    $subscriberEntity->addProfession($profession);
                }
            }
        }

        $subscriberEntity->getCountries()->clear();
        foreach ($countries as $country) {
            $subscriberEntity->addCountrie($country);
        }


        $subscriberEntity->getCities()->clear();
        foreach ($cities as $city) {
            $subscriberEntity->addCitie($city);
        }

        $subscriberEntity->getSources()->clear();
        foreach ($sources as $source) {
            $subscriberEntity->addSource($source);
        }

        if ($subscriberEntity->getAdvertisementMaxAge() === null) {
            $subscriberEntity->setDefaultAdvertisementMaxAge();
        }

        if ($subscriberEntity->getSendingFrequency() === null) {
            $subscriberEntity->setDefaultFrequency();
        }


        if(isset($formData['searchWhat']) && !empty($formData['searchWhat'])){
            $whatValues = $this->advertisementManager->processTextSearchSlug('what', $formData['searchWhat'], $this->getLocale(), false);

            $subscriberEntity->setWhatValue($whatValues['value']);
            $subscriberEntity->setWhatSlug($whatValues['slug']);
        }
        else{
            $subscriberEntity->setWhatValue(null);
            $subscriberEntity->setWhatSlug(null);
        }

        if(isset($formData['searchWhere']) && !empty($formData['searchWhere'])){
            $whereValues = $this->advertisementManager->processTextSearchSlug('where', $formData['searchWhere'], $this->getLocale(), false);

            $subscriberEntity->setWhereValue($whereValues['value']);
            $subscriberEntity->setWhereSlug($whereValues['slug']);
        }
        else{
            $subscriberEntity->setWhereValue(null);
            $subscriberEntity->setWhereSlug(null);
        }


        if(isset($formData['filter']) && !empty($formData['filter']) && $filtersData && !empty($filtersData)){
                $subscriberEntity->setFilterKey($formData['filter']);

        }

        $subscriberEntity->setUpdatedAt(new \DateTime());
        $subscriberEntity->generateSubscriptionId();

        $em->persist($subscriberEntity);
        $em->flush();

    }

    /**
     * Get filter url in Json format
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Json string
     */
    public function getFilterUrlAction(Request $request){

        if(!$request->isXmlHttpRequest()){
            return $this->redirect( $this->generateUrl('index'), 302 );
        }

        $result = array('status' => 'true');

        $whatSearchSlug = $request->get('what', null);
        $whereSearchSlug = $request->get('where', null);

        $data['countries'] = $request->get('countries');
        $data['cities'] = $request->get('cities');
        $data['positions'] = $request->get('positions');
        $data['companies'] = $request->get('companies');
        $data['resources'] = $request->get('resources');

        $filter_key = false;
        if($data['countries'] || $data['cities'] || $data['positions'] || $data['companies'] || $data['resources']){

            $filteredData = array();
            foreach($data as $key => $valueArray){
                $filteredValues = array();
                if($valueArray && is_array($valueArray)){
                    foreach($valueArray as $value){
                        if( (int)$value && !isset($filteredValues[(int)$value])){
                            $filteredValues[(int)$value] = true;
                        }
                    }
                }
                $filteredData[$key] = $filteredValues;
            }

            $filter_decoded = $this->advertisementManager->encodeFilter($filteredData);

           $filter_key = $filter_decoded['key'];
        }


        $result['link'] = $this->generateUrl('visidarbi_advertisement_list', array());

        if($filter_key){
            if ($whatSearchSlug && $whereSearchSlug) {
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_filter', array( 'filter' => $filter_key, 'what' => $whatSearchSlug, 'where' => $whereSearchSlug ));

            } elseif ($whatSearchSlug && !$whereSearchSlug) {
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_what_filter', array( 'filter' => $filter_key, 'what' => $whatSearchSlug ));

            } elseif ($whereSearchSlug && !$whatSearchSlug) {
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_where_filter', array( 'filter' => $filter_key, 'where' => $whereSearchSlug ));

            }
            else{
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_filter', array( 'filter' => $filter_key ));
            }
        }
        else{
            if ($whatSearchSlug && $whereSearchSlug) {
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search', array( 'what' => $whatSearchSlug, 'where' => $whereSearchSlug ));

            } elseif ($whatSearchSlug && !$whereSearchSlug) {
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_what', array( 'what' => $whatSearchSlug ));

            } elseif ($whereSearchSlug && !$whatSearchSlug) {
                $result['link'] = $this->generateUrl('visidarbi_advertisement_list_search_where', array( 'where' => $whereSearchSlug ));

            }
        }

        return new JsonResponse($result);
    }

    protected function getFiltersData($textFilter, $searchFilter){
        $filterData = array();

        if($searchFilter){
            $resources = $this->adRepository->getResourcesListByID($this->currentCountry, null, $searchFilter['source']);
            $companies = $this->adRepository->getCompaniesListForFilterByID($this->currentCountry, null, $searchFilter['company']);
            $categories = $this->adRepository->getCategoriesListForFilterByID($this->currentCountry, $this->getLocale(), null, $searchFilter['category']);
            $cities = $this->adRepository->getCitiesListForFilterByID($this->currentCountry->getDeafultAdvertisementCountry(), $this->getLocale(), null, null, $searchFilter['city']);
            $countries = $this->adRepository->getCountriesListForFilterByID($this->currentCountry->getDeafultAdvertisementCountry(), $this->getLocale(), null, $searchFilter['country']);

        }else{
            $resources = $this->adRepository->getResourcesList($this->currentCountry, null, $textFilter);
            $companies = $this->adRepository->getCompaniesListForFilter($this->currentCountry, null, $textFilter);
            $categories = $this->adRepository->getCategoriesListForFilter($this->currentCountry, $this->getLocale(), null, $textFilter);
            $cities = $this->adRepository->getCitiesListForFilter($this->currentCountry->getDeafultAdvertisementCountry(), $this->getLocale(), null, null, $textFilter);
            $countries = $this->adRepository->getCountriesListForFilter($this->currentCountry->getDeafultAdvertisementCountry(), $this->getLocale(), null, $textFilter);
        }

        $filterData['resources'] = $resources->useResultCache(true, 600, 'advertisement_list_resource_filter_'.md5(serialize($resources->getParameters())))
                                            ->getResult();

        $filterData['companies'] = $companies->useResultCache(true, 600, 'advertisement_list_companies_filter_'.md5(serialize($companies->getParameters())))
                                            ->getResult();

        $filterData['categories'] = $categories->useResultCache(true, 600, 'advertisement_list_categories_filter_'.$this->getLocale().'_'.md5(serialize($categories->getParameters())))
                                                ->getResult();

        $filterData['cities'] = $cities->useResultCache(true, 600, 'advertisement_list_cities_filter_'.$this->getLocale().'_'.md5(serialize($cities->getParameters())))
                                        ->getResult();

        $filterData['countries'] = $countries->useResultCache(true, 600, 'advertisement_list_countries_filter_'.$this->getLocale().'_'.md5(serialize($cities->getParameters())))
                                        ->getResult();


        if($searchFilter){ 
           /* foreach($filterData['resources'] as $rid => $obj){
                $filterData['resources'][$rid]['adv_count'] = $searchFilter['source'][$obj['id']];
            } */
            foreach($filterData['companies'] as $rid => $obj){
                $filterData['companies'][$rid]['adv_count'] = $searchFilter['company'][$obj['id']];
            }
            foreach($filterData['categories'] as $rid => $obj){
                $filterData['categories'][$rid]['adv_count'] = $searchFilter['category'][$obj['category_id']];
            }
            foreach($filterData['cities'] as $rid => $obj){
                $filterData['cities'][$rid]['adv_count'] = $searchFilter['city'][$obj['city_id']];
            }
            foreach($filterData['countries'] as $rid => $obj){
                $filterData['countries'][$rid]['adv_count'] = $searchFilter['country'][$obj['country_id']];
            }
        }

        return $filterData;
    }

    protected function getSearchFilter($params) {

        $config = $this->container->getParameter('sphinx');

        if (empty($params)) {
            return array();
        }

        $conn = new Connection();
        SphinxQL::forge($conn);

        if (! empty($params['what']) || ! empty($params['where'])) {

            if (! empty($params['what'])) {

                $params['what'] = trim($params['what']);
                $keyword = new Search();
                //Store keyword
                $keyword->setKeywordType(Search::KEYWORD_TYPE_WHAT);
                $keyword->setKeyword($params['what']);
                $this->getDoctrine()->getEntityManager()->persist($keyword);

            }

            if (! empty($params['where'])) {

                $params['where'] = trim($params['where']);
                $keyword = new Search();
                //Store keyword
                $keyword->setKeywordType(Search::KEYWORD_TYPE_WHERE);
                $keyword->setKeyword($params['where']);
                $this->getDoctrine()->getEntityManager()->persist($keyword);
            }

            $this->getDoctrine()->getEntityManager()->flush();
        }



        ////// Dabonam kopējo skaitu ///////
        $query = SphinxQL::forge();
        $query->selectFromStr('@id,  (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

                $searchPhrase = $params['what'];
                if($this->getSphinxKeyword($searchPhrase)) {
                    $paramSettled = true;
                    $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
                }
        }

        if (!empty($params['where'])) {

                $searchPhrase = $params['where'];
                if($this->getSphinxKeyword($searchPhrase)) {
                    $paramSettled = true;
                    $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
                }
        }

        if(!$paramSettled) {
            return null;
        }



	
        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);

        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        $total = count($result);
        //////// Dabonam mūsu 10 ierakstu idus ////
        $order = 'asc';
        if($params['sort_direction']=='desc'){ $order = 'desc'; }

        switch($params['sort_by']){
            case 'company.name':
                $query->orderBy('cname', $order);
                break;
            case 'resource.resource':
                $query->orderBy('aresource', $order);
                break;
            case 'advertisement.title':
                $query->orderBy('atitle', $order);
                break;
            case 'advertisement.date_from':
            default:
                if($order == 'desc'){
                    $query->orderBy('is_highlighted', $order);
                }
                $query->orderBy('date_from', $order);
                break;
        }

        //$perpage = 10;
        //$start_from = (empty($params['page'])?1:(int) $params['page'])*$perpage-$perpage;
        //$query->limit($start_from, $perpage);
        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $result = $query->execute();
        $ids=array();
        foreach ($result as $v) {
            $ids[] = $v['id'];
        }



        /////// Grupējam //////
        $groups = array();

        //// Pēc profession_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("profession_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['profession'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc profession_id /////

        //// Pēc city_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("city_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['city'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc city_id /////

        //// Pēc source_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("source_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['source'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc source_id /////

        //// Pēc company_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("company_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['company'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc company_id /////


        //// Pēc country_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("country_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['country'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc country_id /////

        //// Pēc category_id /////
        $query->selectFromStr('@id, @groupby, @count, (@weight * adwigth) as w, * ');
        $query->from($config['indexName']);
        $paramSettled = false;

        if (!empty($params['what'])) {

            $searchPhrase = $params['what'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('what')
                    ->addToMatchGroup('what', 'adtcontent', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'cdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'offerdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'rdescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('what', 'adprofessiondescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        if (!empty($params['where'])) {

            $searchPhrase = $params['where'];
            if($this->getSphinxKeyword($searchPhrase)) {
                $paramSettled = true;
                $query->addMatchGroup('where')
                    ->addToMatchGroup('where', 'adcitydescription', $this->getSphinxKeyword($searchPhrase))
                    //->addOrToMatchGroup('where', 'cname', $this->getSphinxKeyword($searchPhrase))
                    ->addOrToMatchGroup('where', 'adcountrydescription', $this->getSphinxKeyword($searchPhrase));
            }
        }

        $query->limit(0, $config['maxResults']);
        $query->option('max_matches', $config['maxResults']);
        $query->groupBy("category_id");
        //$query->orderBy('w', 'ASC');
        $result = $query->execute();

        foreach ($result as $v) {
            $groups['category'][$v['@groupby']] = $v['@count'];
        }
        //// END Pēc category_id /////

        return array('ids'=>$ids,'total'=>$total, 'filter'=>$groups);


    }


    protected function getSphinxKeyword($sQuery) {

        $sSphinxKeyword = null;
        $aKeyword = array();
        $aRequestString = preg_split('/[\s,-]+/', $sQuery, 5);
        if ($aRequestString) {
            foreach ($aRequestString as $sValue) {
                if (strlen($sValue) >= 3) {
                    $aKeyword[] .= "(" . SphinxQL::forge()->escapeMatch($sValue) . " | *" . SphinxQL::forge()->escapeMatch($sValue) . "*)";
                }
            }

            if(!empty($aKeyword)){
                $sSphinxKeyword = implode(" & ", $aKeyword);
            }
        }

        return $sSphinxKeyword;
    }


    protected  function getAdvertisementPagination(Request $request, $advertisementsQuery){

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $advertisementsQuery,
            $request->get('page', 1),
            10,
            array(
                'distinct' => true,
                'sortDirectionValues' => $this->sortRoutesUrlValues['sortDirectionValues']
            )
        );

        $pagination->setCurrentPageNumber($request->get('page', 1));

        $paginationPageUrl = $this->getPaginationUsedRoute($request);
        $paginationSortUrl = $paginationPageUrl . (!preg_match('/.+_sort$/i', $paginationPageUrl) ? '_sort' : '');
        $pagination->setUsedRoute($paginationPageUrl);

        $pagination->setParam('sort',  $this->getPaginationSortValue($request, $paginationPageUrl) );
        $pagination->setParam('direction', $this->getPaginationDirectionValue($request, $paginationPageUrl));

        return array(
            'pagination' => $pagination,
            'page_route' => $paginationPageUrl,
            'sort_route' => $paginationSortUrl
        );

    }

    /**
     * Get pagination route
     * @todo  REFACTOR THIS PRINCIPLE
     * @param Request $request
     * @return string
     */
    protected function getPaginationUsedRoute(Request $request) {

        $route = 'visidarbi_advertisement_list';

        if ($request->get('what') && $request->get('where')) {
            $route = 'visidarbi_advertisement_list_search';
        } elseif ($request->get('what') && !$request->get('where')) {
            $route = 'visidarbi_advertisement_list_search_what';
        } elseif ($request->get('where') && !$request->get('what')) {
            $route = 'visidarbi_advertisement_list_search_where';
        }

        if ($request->get('filter', null)) {
            $route .= '_filter';
        }

        $route .= '_page';

        $route .= '_sort';

        return $route;
    }

    protected function getPaginationRoutesUrlValues(){

        return array(
            'sortDirectionValues' => array(
                'asc' => $this->translated->trans('direction-ascending-in-translit', array(), 'routing_parametrs'),
                'desc' => $this->translated->trans('direction-descending-in-translit', array(), 'routing_parametrs')
            ),

            'sortKeys' => array(
                'advertisement_title' => array(
                    'value' => $this->translated->trans('sort-by-advertisement-title-in-translit', array(), 'routing_parametrs'),
                    'field' => 'advertisement.title',
                ),
                'company_name' => array(
                    'value' => $this->translated->trans('sort-by-company-name-in-translit', array(), 'routing_parametrs'),
                    'field' => 'company.name'
                ),
                'advertisement_date' => array(
                    'value' => $this->translated->trans('sort-by-advertisement-date_from-in-translit', array(), 'routing_parametrs'),
                    'field' => 'advertisement.date_from'
                ),
                'resource_in' => array(
                    'value' => $this->translated->trans('sort-by-resource-resource-in-translit', array(), 'routing_parametrs'),
                    'field' => 'resource.resource'
                )
            ),

            'default' => array(
                'sortDirection' => 'desc',
                'sortKeys' => 'advertisement_date'
            )
        );

    }

    protected function getPaginationSortValue(Request $request, $route, $type = 'value'){

        $available_values = $this->sortRoutesUrlValues['sortKeys'];
        $default_values = $this->sortRoutesUrlValues['default'];

        if($request->get('sort')){
            foreach( $available_values as $key => $values ){

                if($values['value'] == $request->get('sort')){
                    if($type == 'value'){
                        return $values['value'];
                    }
                    elseif($type == 'field'){
                        return $values['field'];
                    }
                }
            }

            $this->redirectToDefaultUrl($request, $route);
        }
        else{
            if($type == 'value'){
                return $available_values[$default_values['sortKeys']]['value'];
            }
            elseif($type == 'field'){
                return $available_values[$default_values['sortKeys']]['field'];
            }
        }
    }

    protected function getPaginationDirectionValue(Request $request, $route, $type = 'value'){

        $available_values = $this->sortRoutesUrlValues['sortDirectionValues'];
        $default_values = $this->sortRoutesUrlValues['default'];

        if($request->get('direction')){
            foreach( $available_values as $key => $value ){
                if($value == $request->get('direction')){
                    if($type == 'value'){
                        return $value;
                    }
                    elseif($type == 'direction'){
                        return $key;
                    }
                }
            }

            $this->redirectToDefaultUrl($request, $route);
        }
        else{
            if($type == 'value'){
                return $available_values[$default_values['sortDirection']];
            }
            elseif($type == 'direction'){
                return $default_values['sortDirection'];
            }

        }

    }

    protected  function redirectToDefaultUrl(Request $request, $route){

        $sortValues = $this->sortRoutesUrlValues['sortKeys'];
        $directionValues = $this->sortRoutesUrlValues['sortDirectionValues'];

        $default_values = $this->sortRoutesUrlValues['default'];

        $params['what'] = $request->get('what');
        $params['where'] = $request->get('where');

        $params['sort'] = $sortValues[$default_values['sortKeys']]['value'];
        $params['direction'] = $directionValues[$default_values['sortDirection']];

        $params['page'] = (int)$request->get('page') ? $request->get('page') : 1;


        $this->redirectResponse = array( 'url' => $this->generateUrl($route, $params ), 'status' => 301);

    }


    public function storeSearchHistory($keywordWhat, $keywordWhere, $user) {

        $keywordWhat = trim($keywordWhat);
        $keywordWhere = trim($keywordWhere);

        $history = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory')
                ->findOneBy(
                        array('profession_keyword'=>$keywordWhat,
                            'place_keyword' => $keywordWhere,
                            'user'=>$user->getId()));


        if(!$history) {

            $history = new AdvertismentSearchHistory();
            $history->setPlaceKeyword($keywordWhere);
            $history->setProfessionKeyword($keywordWhat);
            $history->setUser($user);
            $history->setSearchCount(1);

        } else {

            $history->setSearchCount($history->getSearchCount() + 1);
            $history->setLastSearchedAt(new \DateTime());

        }


        $this->getDoctrine()->getEntityManager()->persist($history);
        $this->getDoctrine()->getEntityManager()->flush();


    }


}
