<?php

namespace VisiDarbi\AdvertisementBundle\Service;

use \RecursiveIteratorIterator;
use \RecursiveArrayIterator;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;

class AdvertisementService
{
    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender\EmailSenderInterface
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     */
    protected $settingsManager;
    /**
     * @var \VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface
     */
    protected $countryManager;

    protected $em;

    /**
     * returns form error messages as array
     * @param \Symfony\Component\Form\Form $form
     * @return array
     */
    public function getFormErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }
        if ($form->hasChildren()) {
            foreach ($form->getChildren() as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = $this->getFormErrorMessages($child);
                }
            }
        }
        return $errors;
    }


    /**
     * returns form error messages as array of error messages and error field ids
     * @param \Symfony\Component\Form\Form $form
     * @return string
     */
    public function getFormErrorMessagesAsArray(\Symfony\Component\Form\Form $form) {

        $errorArray = $this->getFormErrorMessages($form);


        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($errorArray));

        $formName = $form->getName();
        $errorArray = array();
        $fieldIds = array();
        foreach($iterator as $k=>$v) {

          $fieldIds[] = $formName.'_'.($iterator->getSubIterator(0)->key()).($iterator->getDepth() > 1 ? '_'.$k : '');

          $errorArray[] = $v;
        }

        return array('messages'=>$errorArray,'fieldIds'=>$fieldIds);
    }

    /**
     * send notification to backoffice on new payment event
     * @param string $currentCountry
     * @param string $locale
     * @param \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $transaction
     */
    public function sendPaymentNotificationByTransactionEmailMessage($currentCountry, $locale, \VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction $transaction) {

        if ($transaction->getIsNotificationSent()){
            return false;
        }

        $advertisement = $transaction->getAdvertisement();
        $period = $transaction->getPaidPeriod();
        $Requsities = $transaction->getRequisites();

        /*
        $clientInfo =  '';

        if ($advertisement->getUser()){
            $ClientName = '';
            $legalOrPrivate = $this->currentUser->isLegal() ? 'Legal' : 'Private';
            if ($this->currentUser->isLegal()){
                $legalProfile = $this->currentUser->getLegalProfile();
                $ClientName = $legalProfile->getCompanyName();
            } else {
                $ClientName = $this->currentUser->getPrivateProfile()->getUser()->getFullName();
            }

            $clientInfo .=  "{$ClientName} (Registered {$legalOrPrivate})";
        } else {
            $Requsities = $transaction->getRequisites();
            $CompanyName = $Requsities->getCompanyName();
            $legalOrPrivate = trim($CompanyName)!="" ? 'Legal' : 'Private';
            $ClientName = $CompanyName ? $CompanyName : $Requsities->getContactPersonName();
            $clientInfo .=  "{$ClientName} (Guest {$legalOrPrivate})";
        }*/

        $clientInfo =  '';
        $CompanyName = $Requsities->getCompanyName();
        $ContactPersonName = $Requsities->getContactPersonName();
        $clientIsRegistered = $advertisement && $advertisement->getUser() ? true : false;

        $clientInfo .= addslashes($CompanyName).", ".addslashes($ContactPersonName)." (".($clientIsRegistered ? 'Registered' : 'Guest').")";

        $serviceInfo = '';
        if ($advertisement) {
           $serviceInfo = "Advertisement (Title: {$advertisement->getTitle()}. PublicId: {$advertisement->getPublicId()}). ";
        }

        if ($period) {
           $serviceInfo .= "Period ({$period->getFromDate()->format("Y-m-d")} - {$period->getToDate()->format("Y-m-d")})";
        }

        $serviceInfo .= " {$transaction->getAmount()} {$transaction->getPaymentCurrency()}, {$transaction->getPaymentMethod()}";

        //$recipientAddress = $this->settingsManager->get('emails.sender_email');
        $recipientAddress = "info@visidarbi.lv";
        /**
         * @todo create new field in db to store recipientAddress
         */
        $this->emailSender->send(
                $currentCountry, $locale, EmailTemplate::ID_GOT_NEW_PAYMENT, $recipientAddress, array(
            'clientName' => $clientInfo,
            'serviceName' => $serviceInfo
                ), array(
            'email' => $recipientAddress,
            'name' => $this->settingsManager->get('emails.sender_name')
                )
        );

        $transaction
            ->setIsNotificationSent(true)
        ;

        $this->em->persist($transaction);
        $this->em->flush();

    }

    public function setEmailSender($emailSender)
    {
        $this->emailSender = $emailSender;
    }

    public function setSettingsManager($settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }

    public function setEntityManager($entityManager)
    {
        $this->em = $entityManager;
    }

}
