<?php

namespace VisiDarbi\AdvertisementBundle\Twig;

use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use Twig_Extension;
use Twig_Function_Method;
use Doctrine\ORM\EntityManager;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;

class AdvertismentExtension extends Twig_Extension
{
    /**
     * @var CountryManagerInterface
     */
    private $countryManager;
    
    /**
     * @var EntityManager 
     */
    protected $em;
    
    private $advCnt = null;


    public function __construct(CountryManagerInterface $sm, EntityManager $em)
    {
        $this->countryManager = $sm;
        $this->em = $em;
    }

    public function getFunctions()
    {
        return array(
            'total_advertisement_count' => new Twig_Function_Method($this, 'getTotalAdvertisementCount'),
        );
    }

    public function getTotalAdvertisementCount()
    {
        if ($this->advCnt !== null) {
           return $this->advCnt; 
        }
        
        $country = $this->countryManager->getCurrentCountry();
        $query = $this->em->createQuery('SELECT COUNT(a.id) FROM VisiDarbiAdvertisementBundle:Advertisement a WHERE a.country = :country AND a.status = :status');
        $query->setParameter('country', $country);
        $query->setParameter('status', Advertisement::STATUS_ACTIVE);
        $query->useResultCache(true, 300, 'advertisements_total_count');
        
        $this->advCnt = $query->getSingleScalarResult();

        $queryCacheProfile = $query->getQueryCacheProfile();
        
        return $this->advCnt;
    }

    public function getName()
    {
        return 'visidarbi_advertisement';
    }
}
