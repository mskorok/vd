<?php

namespace VisiDarbi\AdvertisementBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;
use VisiDarbi\LocalePlaceBundle\Entity\Country;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Doctrine\ORM\Query\Expr;
use VisiDarbi\UserBundle\Entity\User;

/**
 * Custom EntityRepository for advertisements entities needs
 *
 * @author Aleksey
 */
class AdvertisementEntityRepository extends EntityRepository {

    private $companyesAdvertisementsCount;

    protected function countCompaniesAdvertisements() {
        $query = $this->_em->createQuery(
                "
                SELECT COUNT(ca) adAmount, c.slug FROM VisiDarbi\AdvertisementBundle\Entity\Company c
                JOIN c.advertisement ca
                WHERE ca.status = :status
                GROUP BY c.slug
                ")
                ->setParameter('status', Advertisement::STATUS_ACTIVE);

        $dataSet = array();

        foreach ($query->getArrayResult() as $k => $v) {
            $dataSet[$v['slug']] = $v['adAmount'];
        }

        $this->companyesAdvertisementsCount = $dataSet;
    }

    /**
     * Returns companies advertisements amount by company slug
     * @param \VisiDarbi\AdvertisementBundle\Entity\Company $company
     * @return int
     */
    public function getCompanyAdvertisementsAmount(\VisiDarbi\AdvertisementBundle\Entity\Company $company) {

        //Load companies advertisements count by slug if not loaded
        if ($this->companyesAdvertisementsCount == null) {
            $this->countCompaniesAdvertisements();
        }

        //get advertisemnts gount by slug
        if (isset($this->companyesAdvertisementsCount[$company->getSlug()])) {
            return $this->companyesAdvertisementsCount[$company->getSlug()];
        }

        return 0;
    }

    /**
     * Returns categories with advertisements
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry
     * @param string $locale
     * @return Doctrine\ORM\Query
     */
    public function getNotEmptyCategories(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale) {

        return $this->_em->createQuery("
                SELECT c, cp FROM VisiDarbi\ProfessionBundle\Entity\Category c
                JOIN c.professions cp

                JOIN cp.Advertisements cpa
                WHERE c.country = :country  AND cpa.status = :status
                ORDER BY c.name
                ")
                        ->setParameters(array('country' => $systemCountry, 'status'=>  Advertisement::STATUS_ACTIVE))
                        ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                      //  ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                      //  ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;
    }

    /**
     * Get proffesions with advertisements by category
     * @param string $categorySlug
     * @param string $locale
     * @return Doctrine\ORM\Query
     */
    public function getNotEmptyProfessionByCategory($categorySlug, $locale) {

        return $this->_em->createQuery("
                        SELECT p, pa, pc, pcc, pt FROM VisiDarbi\ProfessionBundle\Entity\Profession p
                        JOIN p.translations pt
                        JOIN p.category pc
                        JOIN pc.country pcc
                        JOIN pc.translations pct WITH (pct.slug = :category)
                        JOIN p.Advertisements pa
                        WHERE pa.status = :status
                        ORDER BY p.name"
                        )
                        ->setParameters(array('category' => $categorySlug, 'status'=>  Advertisement::STATUS_ACTIVE))
                        ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                        //->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                        ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
    }

    /**
     * Returns all advertisements countries by system country with locale translations
     * @param VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry
     * @param string $locale
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getAdvertisementCountries(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale) {




        $advCountryRepo = $this->_em->getRepository('VisiDarbiAdvertisementBundle:AdvertisementCountry');
        $qb = $advCountryRepo->createQueryBuilder('c')
                ->select('c.id as country_id, c.name as country_name, COUNT(a.id) as adv_count, ct.slug AS country_slug')
                ->leftJoin('c.Advertisements', 'a')
                ->leftJoin('c.translations', 'ct', Expr\Join::WITH, "ct.locale=:locale " )
                ->where('c.country = :country')
                ->setParameter('country', $systemCountry->getId())
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->setParameter('locale', $locale)
                ->groupBy('c.id')
                ->orderBy('country_name', 'ASC')
                ->having('adv_count > 0')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $advCountries = $query->getResult();

        return $advCountries;

        return $this->_em->createQueryBuilder('ac')
                        ->select('ac, aca')
                        ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'ac')
                        ->leftJoin('ac.Advertisements', 'aca')
                        ->andWhere("ac.country = :country")
                        ->orderBy('ac.name')
                        ->setParameter('country', $systemCountry->getId())
                        ->getQuery()
                        ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                        ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
                        ->getResult()
        ;
    }

    /**
     * Returns all advertisements countries by system country with locale translations
     * @param VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry
     * @param string $locale
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getAdvertisementCountriesForAddForm(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale) {

        return $this->_em->createQueryBuilder('ac')
                        ->select('ac.id as id, ac.name as name')
                        ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'ac')
                        ->andWhere("ac.country = :country")
                        ->orderBy('ac.name')
                        ->setParameter('country', $systemCountry->getId())
                        ->getQuery()
                        ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                        ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
                        ->getResult()
        ;
    }

    /**
     * Returns advertisements selected cities by country with locale translations
     * @param VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country
     * @param string $locale
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getAdvertisementSelectedCitiesForCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country, $locale) {

        return $this->prepareCitiesQuery($country, $locale, true)->getResult();
    }

    /**
     * Returns advertisements non selected cities by country with locale translations
     * @param VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country
     * @param string $locale
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getAdvertisementNonSelectedCitiesForCountry(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country, $locale) {

        return $this->prepareCitiesQuery($country, $locale, false)->getResult();
    }

    /**
     * Prepare query for advertisiment cities requets
     * @param \VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country
     * @param string $locale
     * @param boolean $selected
     * @return type
     */
    private function prepareCitiesQuery(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $country, $locale, $selected) {

        return $this->_em->createQueryBuilder('ac')
                        ->select('ac, aca')
                        ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'ac')
                        ->join('ac.Advertisements', 'aca')
                       // ->andWhere("ac.AdvertisementCountry = :country")
                        ->andWhere("ac.mark_on_frontend = :selected")
                        ->orderBy('ac.name')
                        ->setParameter('selected', $selected)
                        //->setParameter('country', $country->getId())
                        ->getQuery()
                        ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                        ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                        ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)

        ;
    }

    /**
     * Professions by letter
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry
     * @param string $locale
     * @param string $letter
     * @return Doctrine\ORM\Query
     */
    public function getProfessionListByLetter(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale, $letter) {

        $letterIn = explode('-', $letter);
        foreach ($letterIn as $k => $v) {
            $letterIn[$k] = '\'' . $v . '\'';
        }
        $letterIn = implode(',', $letterIn);


        $query = $this->_em->createQueryBuilder('p')
                ->select('p, cp, c')
                ->distinct()
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->join('p.translations', 't')
                ->join('p.Advertisements', 'cp')
                ->join('p.category', 'c')
                ->where("(t.az IN ({$letterIn}))" )
                ->andWhere("t.locale=:locale")
                ->andWhere("cp.country = :country")
                 ->andWhere('cp.status = :astatus')
              //  ->orderBy('p.name')
//                ->groupBy('p.name')
                ->setParameter('locale', $locale)
                ->setParameter('country', $systemCountry->getId())
                ->setParameter('astatus', Advertisement::STATUS_ACTIVE)
                ->groupBy('t.slug')
                ->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                //->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                //->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        return $query;
    }

    /**
     * Professions by letter
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry
     * @param string $locale
     * @param string $letter
     * @return Doctrine\ORM\Query
     */
    public function getProfessionsListForAddForm($categoryId, $locale) {

        $query = $this->_em->createQueryBuilder('p')
                ->select('p.id, p.name, t.slug')
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->join('p.translations', 't')
                ->andWhere("p.category IN (:category)")
                ->andWhere("t.locale = :locale")
                ->orderBy('p.name')
                ->groupBy('t.slug')
                ->setParameter('category', $categoryId)
                ->setParameter('locale', $locale);
        $query = $query->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, true)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true)
        ;

        return $query;
    }

    /**
     * Returns AZ list for professions
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @param string $locale
     * @return array
     */
    public function getAZforProfessions(\VisiDarbi\LocalePlaceBundle\Entity\Country $country, $locale) {

        $az = $this->_em->createQuery("
                            SELECT DISTINCT pt.az FROM VisiDarbi\ProfessionBundle\Entity\ProfessionTranslation pt
                            JOIN pt.object p
                            JOIN p.Advertisements pa
                            WHERE pa.country = :country AND pt.locale = :locale AND pa.status = :status
                            ORDER BY pt.az
                            "
                )
                ->setParameters(array(
                    'status' => Advertisement::STATUS_ACTIVE,
                    'country' => $country,
                    'locale' => $locale,))
                ->getResult();

        return $this->prepareAZ($az);
    }

    /**
     * Get AZ list for companies
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return array
     */
    public function getAZforCompanies(\VisiDarbi\LocalePlaceBundle\Entity\Country $country) {

        $az = $this->_em->createQuery("
                    SELECT DISTINCT c.az FROM VisiDarbi\AdvertisementBundle\Entity\Company c
                    JOIN c.advertisement ca
                    WHERE ca.country = :country AND ca.status = :status AND c.az IS NOT NULL
                    ORDER BY c.az
                    "
                )
                ->setParameters(array('country' => $country, 'status' => Advertisement::STATUS_ACTIVE))
                ->getResult();

        return $this->prepareAZ($az);
    }

    /**
     * Returns companies by letter
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry
     * @param string $letter
     * @return Doctrine\ORM\Query
     */
    public function getCompaniesListByLetter(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $letter) {

        $letterIn = explode('-', $letter);
        foreach ($letterIn as $k => $v) {
            $letterIn[$k] = '\'' . $v . '\'';
        }
        $letterIn = implode(',', $letterIn);

        $query = $this->_em->createQueryBuilder('c')
                ->select('DISTINCT c.id')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
                ->join('c.advertisement', 'a')
                ->where("c.az IN ({$letterIn})")
                ->andWhere("a.country = :country")
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->setParameter('country', $systemCountry->getId())
                ->groupBy('c.slug')
                ->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $dCompanies = $query->getArrayResult();

        $ids = array();
        foreach($dCompanies as $v){
            $ids[] = $v['id'];
        }

        if(empty($ids)) {
            return array();
        }

        $query = $this->_em->createQueryBuilder('c')
                ->select('c')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
                ->where("c.id IN (:ids)")
                ->setParameter('ids', $ids)
                ->orderBy('c.slug')
                ->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);


        return $query;
    }

    protected function getCompaniesQb(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry) {

        $query = $this->_em->createQueryBuilder('c')
                ->select('c, a')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
                ->join('c.advertisement', 'a')
                ->andWhere("a.country = :country")
                ->setParameter('country', $systemCountry->getId());

        return $query;
    }

    public function getCompaniesListForFilter(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $limit = 5, $searchFilter = null) {

        $qb = $this->_em->createQueryBuilder('c')
                ->select('c.name, c.id, COUNT(c.id) as adv_count')
                ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
                ->join('c.advertisement', 'a')
                ->where('a.country = :country')
                ->setParameter('country', $systemCountry->getId())
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->groupBy('c.slug')
                ->orderBy('c.name', 'ASC');

        if ($searchFilter) {
            $qb->andWhere('a.id IN (:filter)');
            $qb->setParameter('filter', $searchFilter);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }


        $companies = $qb->getQuery();
        $companies->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $companies;
    }

    public function getCompaniesListForFilterByID(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $limit = 5, $searchFilter = null) {

        $qb = $this->_em->createQueryBuilder('c')
            ->select('c.name, c.id, 0 as adv_count')
            ->from('VisiDarbi\AdvertisementBundle\Entity\Company', 'c')
            ->orderBy('c.name', 'ASC');

        if ($searchFilter) {
            $qb->andWhere('c.id IN (:filter)');
            $qb->setParameter('filter', array_keys($searchFilter));
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }


        $companies = $qb->getQuery();
        $companies->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $companies;
    }


    public function getCategoriesListForFilter(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale, $limit = 5, $searchFilter = null) {


//profession categories
        $qb = $this->_em->createQueryBuilder('c')
            ->select('c.id as category_id, c.name as category_name, COUNT(a.id) as adv_count')
            ->from('VisiDarbi\ProfessionBundle\Entity\Category', 'c')
            ->join('c.Advertisements', 'a')
            ->where('c.country = :country')
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->setParameter('country', $systemCountry->getId())
            ->groupBy('c.id')
            ->orderBy('c.name', 'asc')
        ;

        if ($searchFilter) {
            $qb->andWhere('a.id IN (:filter)');
            $qb->setParameter('filter', $searchFilter);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }


    public function getCategoriesListForFilterByID(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale, $limit = 5, $searchFilter = null) {
//profession categories
        $qb = $this->_em->createQueryBuilder('c')
            ->select('c.id as category_id, c.name as category_name, 0 as adv_count')
            ->from('VisiDarbi\ProfessionBundle\Entity\Category', 'c')
            ->where('c.country = :country')
            ->setParameter('country', $systemCountry->getId())
            ->groupBy('c.id')
            ->orderBy('c.name', 'asc')
        ;

        if ($searchFilter) {
            $qb->andWhere('c.id IN (:filter)');
            $qb->setParameter('filter', array_keys($searchFilter));
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getCategoriesListForAddFrom(\VisiDarbi\LocalePlaceBundle\Entity\Country $systemCountry, $locale) {
        //profession categories
        $qb = $this->_em->createQueryBuilder('c')
                ->select('c.id as id, c.name as name')
                ->from('VisiDarbi\ProfessionBundle\Entity\Category', 'c')
                ->where('c.country = :country')
                ->setParameter('country', $systemCountry->getId())
                ->orderBy('c.name', 'asc')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getCountriesListForFilter(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $defaultCountry, $locale, $limit = 5, $searchFilter = null) {

        $qb = $this->_em->createQueryBuilder('c')
                ->select('c.id as country_id, c.name as country_name, COUNT(a.id) as adv_count')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'c')
                ->join('c.Advertisements', 'a')
                ->andWhere('c.country = :country')
                ->andWhere('c.id != :default_country')
                ->andWhere('a.status = :adStatus')
                ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
                ->setParameter('country', $defaultCountry->getId())
                ->setParameter('default_country', $defaultCountry->getId())
                ->groupBy('c.id')
                ->orderBy('c.name', 'ASC')
        ;

        if ($searchFilter) {
            $qb->andWhere('a.id IN (:filter)');
            $qb->setParameter('filter', $searchFilter);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getCountriesListForFilterByID(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $defaultCountry, $locale, $limit = 5, $searchFilter = null) {

        $qb = $this->_em->createQueryBuilder('c')
            ->select('c.id as country_id, c.name as country_name, 0 as adv_count')
            ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'c')
            ->andWhere('c.country = :country')
            ->andWhere('c.id != :default_country')
            ->setParameter('country', $defaultCountry->getId())
            ->setParameter('default_country', $defaultCountry->getId())
            ->orderBy('c.name', 'ASC')
        ;

        if ($searchFilter) {
            $qb->andWhere('c.id IN (:filter)');
            $qb->setParameter('filter', array_keys($searchFilter));
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getCitiesListForFilter(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $defaultCountry, $locale, $markedOnFrontend = null, $limit = 5, $searchFilter = null) {

        $qb = $this->_em->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, c.keywords as city_keywords, COUNT(a.id) as adv_count')
            ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'c')
            ->join('c.Advertisements', 'a')
            ->andWhere('c.AdvertisementCountry = :country')
            ->andWhere('a.status = :adStatus')
            ->setParameter('adStatus', Advertisement::STATUS_ACTIVE)
            ->setParameter('country', $defaultCountry->getId())
            ->groupBy('c.id')
            ->orderBy('c.name', 'ASC')
        ;

        if ($searchFilter) {
            $qb->andWhere('a.id IN (:filter)');
            $qb->setParameter('filter', $searchFilter);
        }

        if ($markedOnFrontend !== null) {
            $qb->andWhere('c.mark_on_frontend = :mark')
                ->setParameter('mark', (bool) $markedOnFrontend);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getCitiesListForFilterByID(\VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry $defaultCountry, $locale, $markedOnFrontend = null, $limit = 5, $searchFilter = null) {

        $qb = $this->_em->createQueryBuilder('c')
            ->select('c.id as city_id, c.name as city_name, c.keywords as city_keywords, 0 as adv_count')
            ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'c')
            ->join('c.Advertisements', 'a')
            ->where('c.AdvertisementCountry = :country')
            ->setParameter('country', $defaultCountry->getId())
            ->groupBy('c.id')
            ->orderBy('c.name', 'ASC')
        ;

        if ($searchFilter) {
            $qb->andWhere('c.id IN (:filter)');
            $qb->setParameter('filter', array_keys($searchFilter));
        }

        if ($markedOnFrontend !== null) {
            $qb->andWhere('c.mark_on_frontend = :mark')
                ->setParameter('mark', (bool) $markedOnFrontend);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    /**
     * Returns cities by given country
     * @param int $countryId
     * @param sting $locale
     * @return Doctrine\ORM\Query
     */
    public function getCitiesListForAddForm($countryId, $locale) {

        $qb = $this->_em->createQueryBuilder('c')
                ->select('c.id as id, c.name as name')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'c')
                ->andWhere('c.AdvertisementCountry = :country')
                ->setParameter('country', $countryId)
                ->orderBy('c.name', 'ASC')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    /**
     * Prepare AZ letters array
     * @param array $dataArray
     * @return array
     */
    private function prepareAZ($dataArray) {

        $letters = array();
        foreach ($dataArray as $v) {
            $letters[] = $v['az'];
        }

        $letters = array_chunk($letters, 2);

        foreach ($letters as $k => $v) {
            $letters[$k] = implode('-', $v);
        }

        return $letters;
    }

    /**
     * Gets top/featured ads for homepage slider block
     * @param Country $country
     * @param string $locale
     * @param int $limit
     * @return array
     */
    public function getFeaturedAdvertisements(Country $country, $locale, $limit) {

        /**
         * workaround for getting random records without slowing database performance
         */
        $queryResult = $this->_em->createQuery(
             "SELECT MAX(a.id) AS maxId FROM VisiDarbi\AdvertisementBundle\Entity\Advertisement a WHERE a.country = :country AND a.on_startpage = 1 AND a.status = :status")
             ->setParameter('country', $country->getId())
             ->setParameter('status', Advertisement::STATUS_ACTIVE)
            ->getSingleResult();

        $advMaxId = (int)$queryResult['maxId'];

        $qb = $this->getActiveAdvertisementQb($country)
                ->select('a, comp, ae, ea, cp, req, ac, actry, slugs, floor(1 + RAND() * '.$advMaxId.') AS HIDDEN rand')
                ->leftJoin('a.companies', 'comp')
                ->leftJoin('a.advertisementEmails', 'ae')
                ->leftJoin('a.externalAdvertisement', 'ea')
                ->leftJoin('a.contactPerson', 'cp')
                ->leftJoin('a.requisites', 'req')
                ->leftJoin('a.AdvertisementCity', 'ac')
                ->leftJoin('a.AdvertisementCountry', 'actry')
                ->leftJoin('a.slugs', 'slugs')
                ->andWhere('a.on_startpage = 1')
                ->setMaxResults($limit)
                ->groupBy('a.id')
                ->orderBy('rand');

        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $res = $query
                    ->useResultCache(true, 600, 'featured_advertisements_'.$country->getId().'_'.$locale )
                    ->getResult();

        shuffle($res);
        return $res;
    }

    public function getLatestAdvertisements(Country $country, $locale, $limit) {
        $qb = $this->getActiveAdvertisementQb($country)
                ->select('a, comp, ae, ea, cp, req, ac, actry, slugs')
                ->leftJoin('a.companies', 'comp')
                ->leftJoin('a.advertisementEmails', 'ae')
                ->leftJoin('a.externalAdvertisement', 'ea')
                ->leftJoin('a.contactPerson', 'cp')
                ->leftJoin('a.requisites', 'req')
                ->leftJoin('a.AdvertisementCity', 'ac')
                ->leftJoin('a.AdvertisementCountry', 'actry')
                ->leftJoin('a.slugs', 'slugs')
                //->leftJoin('a.translations', 'at', Expr\Join::WITH, '(at.locale=:locale AND at.field=\'title\')')
                //->leftJoin('comp.translations', 'compt')
                ->setMaxResults($limit)
                //->setParameter('locale', $locale)
                ->orderBy('a.id', 'DESC')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query
                    ->useResultCache(true, 600, 'latest_advertisements_'.$country->getId().'_'.$locale.'_'.$limit )
                    ->getResult();
    }

    public function getAdvertisementsFromSocialNetworks(Country $country, $locale, $limit, $resource) {
        $qb = $this->getActiveAdvertisementQb($country)
                ->select('a, comp, ae, ea, cp, req, ac, actry')
                ->leftJoin('a.companies', 'comp')
                ->leftJoin('a.advertisementEmails', 'ae')
                ->leftJoin('a.externalAdvertisement', 'ea')
                ->leftJoin('ea.externalAdvertisementSource', 'eas')
                ->leftJoin('a.contactPerson', 'cp')
                ->leftJoin('a.requisites', 'req')
                ->leftJoin('a.AdvertisementCity', 'ac')
                ->leftJoin('a.AdvertisementCountry', 'actry')
                //->leftJoin('a.translations', 'at', Expr\Join::WITH, '(at.locale=:locale AND at.field=\'title\')')
                //->leftJoin('comp.translations', 'compt')
                ->setMaxResults($limit)
                //->setParameter('locale', $locale)
                ->andWhere('eas.resource = :resource')
                ->setParameter(':resource', $resource)
                ->orderBy('a.id', 'DESC')
        ;

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getResult();
    }

    /**
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getActiveAdvertisementQb(Country $country) {
        $qb = $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.country = :country')
                ->setParameter('country', $country->getId())
                ->andWhere('a.status = :status')
                ->setParameter('status', Advertisement::STATUS_ACTIVE)
        ;

        return $qb;
    }

    /**
     * Returns resources list by country
     * @param \VisiDarbi\LocalePlaceBundle\Entity\Country $country
     * @return \Doctrine\ORM\Query
     */
    public function getResourcesList(Country $country, $limit = 5, $searchFilter = null) {

        $query = $this->_em->createQueryBuilder('r')
            ->select('r')
            ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource', 'r')
            ->where('r.show_advertisiment = 1')
            ->andWhere("r.country = :country")
            ->setParameter('country', $country->getId())
            ->orderBy('r.show_as', 'ASC')
            ->orderBy('r.resource', 'ASC');

        if ($searchFilter) {
            $query->leftJoin('r.externalAdvertisements', 'ea');
            $query->leftJoin('ea.advertisements', 'a');
            $query->andWhere('a.id IN (:filter)');
            $query->setParameter('filter', $searchFilter);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        $query = $query->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getResourcesListByID(Country $country, $limit = 5, $searchFilter = null) {

        $query = $this->_em->createQueryBuilder('r')
            ->select('r')
            ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource', 'r')
            ->where('r.show_advertisiment = 1')
            ->andWhere("r.country = :country")
            ->setParameter('country', $country->getId())
            ->orderBy('r.show_as', 'ASC')
            ->orderBy('r.resource', 'ASC');

        if ($searchFilter) {
            $query->andWhere('r.id IN (:filter)');
            $query->setParameter('filter', array_keys($searchFilter));
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        $query = $query->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query;
    }

    public function getUserActiveAdvertisementQb(User $user)
    {
        return $this->createQueryBuilder('h')
            ->select('h')
            ->where('h.user = :user')
            ->andWhere('h.status != :pending')
            ->andWhere('h.status != :deleted')
            ->andWhere('h.status != :expired')
            ->setParameter('pending', Advertisement::STATUS_PENDING)
            ->setParameter('deleted', Advertisement::STATUS_DELETED)
            ->setParameter('expired', Advertisement::STATUS_INACTIVE_CLOSED)
            ->setParameter('user', $user->getId())
         ;
    }


    public function getByPublicIdOrSlug($publicIdOrSlug)
    {
        return $this->createQueryBuilder('a')
                ->select('a')
                ->leftJoin('a.slugs', 'slg')
                ->where('(a.publicId = :publicId OR slg.slug = :publicId)')
                ->setParameters(array('publicId' => $publicIdOrSlug))
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
    }

    public function getActiveAdvertisementByPublicIdOrSlug($publicIdOrSlug)
    {
        return $this->createQueryBuilder('a')
                ->select('a')
                ->leftJoin('a.slugs', 'slg')
                ->where('(a.publicId = :publicId OR slg.slug = :publicId)')
                ->andWhere('a.status = :status')
                ->setParameter('publicId', $publicIdOrSlug)
                ->setParameter('status', Advertisement::STATUS_ACTIVE)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
    }

}
