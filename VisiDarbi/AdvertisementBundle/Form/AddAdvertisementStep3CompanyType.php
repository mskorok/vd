<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class AddAdvertisementStep3CompanyType extends DummyTransAbstractType {

    protected $locales;
    protected $defaultLocale;

    public function __construct($locales, $defaultLocale, $translator = null) {
        $this->locales = $locales;
        $this->defaultLocale = $defaultLocale;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('name', 'text', array('required' => true, 'label' => 'Company name',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Company name must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Company name can\'t be empty.')))
        )));

        $regNrCharLimit = 40;
        $builder->add('registration_number', 'text', array('required' => true, 'label' => 'Company reg. number',
            'constraints' =>
            array(
                new MaxLength(array('limit' => $regNrCharLimit, 'message' => $this->trans('Company registration number must not be longer than %chars_num% characters.', array('%chars_num%'=>$regNrCharLimit)))),
                new NotBlank(array('message' => $this->trans('Company reg. number can\'t be empty.')))
        )));

        foreach ($this->locales as $locale) {
            $builder->add('description_' . $locale, 'textarea', array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Company description',
                'constraints' =>
                array(
                    new MaxLength(array('limit' => 500, 'message' => ($this->trans('Company description  must be not longer than 500 characters (%locale%).',['%locale%' => $locale],'messages')))),
                    ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Company description can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
            )));
        }
    }

    public function getName() {
        return 'addadvertisementstep3company';
    }

}

?>
