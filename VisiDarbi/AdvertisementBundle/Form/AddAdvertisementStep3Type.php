<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep3CompanyType;
use VisiDarbi\AdvertisementBundle\Form\AddAdvertisementStep3ContactPersonType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Callback;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Exercise\HTMLPurifierBundle\Form\HTMLPurifierTransformer;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class AddAdvertisementStep3Type extends DummyTransAbstractType {

    protected $locales;
    protected $defaultLocale;
    protected $purifier;
    protected $translator;

    public function __construct($locales, $defaultLocale, $purifier, $translator = null) {
        $this->locales = $locales;
        $this->defaultLocale = $defaultLocale;
        $this->purifier = $purifier;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $format = !empty($options['data']['format']) ? $options['data']['format']:null;

        foreach ($this->locales as $locale) {

            $builder->add('title_' . $locale, 'text', array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Title',
                'constraints' =>
                    array(
                        new MaxLength(array('limit' => 80, 'message' => ($this->trans('Title must be not longer than 80 characters (%locale%).',['%locale%' => $locale],'messages')))),
                        ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Title can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
            )));

            if ($format == Advertisement::FORMAT_FREE) {
                $transformer = new HTMLPurifierTransformer($this->purifier);
                $builder->add(
                    $builder->create('content_' . $locale, 'ckeditor', array('required' => ($locale == $this->defaultLocale) ? false : false, 'label' => false,
                    'config_name' => 'advertisement',
                    'attr' => array('class'=>'content-editor'),
                    'constraints' =>
                        array(
                            ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Content can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                )))
                ->addViewTransformer($transformer));
            } elseif($format == Advertisement::FORMAT_IMAGE_PDF) {
                $builder->add('file_format_' . $locale, null, array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Upload picture/pdf file (up to %size%MB)',
                    'attr' => array('class' => 'file_format_' . $locale . '_files_h', 'style'=>'display:none;'),
                    'constraints' =>
                        array(
                            ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Picture/pdf file can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                        )));
            } elseif($format == Advertisement::FORMAT_HTML) {

                $builder->add('file_format_' . $locale, null, array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Upload HTML file',
                        'attr' => array('class' => 'file_format_' . $locale . '_files_h', 'style'=>'display:none;'),
                        'constraints' =>
                            array(
                                ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('HTML file can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                            )));
                $builder->add('html_image_' . $locale, 'collection', array( 'type'=>'hidden', 'allow_add'=>true, 'allow_delete'=>true, 'by_reference'=>false,  'label' => 'Upload Images',
                        'options'=> array('attr' => array(), 'required' => false,
                )));
            } else {
                $builder->add('description_' . $locale, 'textarea', array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Description',
                    'constraints' =>
                    array(
                        new MaxLength(array('limit' => 1000, 'message' => ($this->trans('Description must be not longer than 1000 characters (%locale%).',['%locale%' => $locale],'messages')))),
                        ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Description can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                )));
                $builder->add('additional_description_' . $locale, 'textarea', array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Additional description',
                    'constraints' =>
                    array(
                        new MaxLength(array('limit' => 500, 'message' => ($this->trans('Additional description must be not longer than 500 characters (%locale%).',['%locale%' => $locale],'messages')))),
                        ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Additional description can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                )));
                $builder->add('duty_' . $locale, 'collection', array( 'type'=>'text', 'allow_add'=>true, 'max_length' => 100, 'label' => 'Duties','options'=> array('attr' => array('maxlength' => 100), 'required' => ($locale == $this->defaultLocale) ? true : false,
                    'constraints' =>
                        array(
                            new MaxLength(array('limit' => 100, 'message' => ($this->trans('Duties must be not longer than 100 characters (%locale%).',['%locale%' => $locale],'messages')))),
                            ($locale == $this->defaultLocale) ? new NotBlank(array('message' => $this->trans('Duties can\'t be empty (%locale%).',['%locale%' => $locale],'messages'))) : new Callback(array('methods' => array()))
                    )
                )));
                $builder->add('offer_' . $locale, 'collection', array('type'=>'text', 'allow_add'=>true,  'label' => 'Offers', 'options'=> array('attr' => array('maxlength' => 100),'required' => ($locale == $this->defaultLocale) ? true : false,
                    'constraints' =>
                    array(
                        new MaxLength(array('limit' => 100, 'message' => ($this->trans('Offers must be not longer than 100 characters (%locale%).',['%locale%' => $locale],'messages')))),
                        ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Offers can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                ))));
                $builder->add('requirement_' . $locale, 'collection', array('type'=>'text', 'allow_add'=>true, 'max_length' => 100, 'label' => 'Requirements', 'options'=> array('attr' => array('maxlength' => 100), 'required' => ($locale == $this->defaultLocale) ? true : false,
                    'constraints' =>
                    array(
                        new MaxLength(array('limit' => 100, 'message' => ($this->trans('Requirements must be not longer than 100 characters (%locale%).',['%locale%' => $locale],'messages')))),
                        ($locale == $this->defaultLocale) ? new NotBlank(array('message' => ($this->trans('Requirements can\'t be empty (%locale%).',['%locale%' => $locale],'messages')))) : new Callback(array('methods' => array()))
                ))));
            }
        }

        $builder->add('date_from', 'hidden', array('required' => true, 'label' => 'Publication terms',
            'constraints' =>
            array(
                new Date(array('message' => $this->trans('Incorrect date from value.'))),
                new NotBlank(array('message' => $this->trans('Publication terms (date from) can\'t be empty.')))
        )));
        $builder->add('date_to', 'hidden', array('required' => true, 'label' => 'Publication terms',
            'constraints' =>
            array(
                new Date(array('message' => $this->trans('Incorrect date to value.'))),
                new NotBlank(array('message' => $this->trans('Publication terms (date to) can\'t be empty.')))
        )));


        $builder->add('date_1', 'text', array('required' => true, 'label' => 'Terms'));
        $builder->add('deleted_logo', 'hidden', array('attr' => array('class'=>'deleted-logo')));

        $builder->add('company', new AddAdvertisementStep3CompanyType($this->locales, $this->defaultLocale, $this->translator));
        $builder->add('contact_person', new AddAdvertisementStep3ContactPersonType($this->locales, $this->defaultLocale, $this->translator));

    }

    public function getName() {
        return 'addadvertisementstep3';
    }

    public function getDefaultOptions(array $options) {
        return array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
                // 'intention'       => 'task_item',
        );
    }

}

?>
