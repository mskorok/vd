<?php


namespace VisiDarbi\AdvertisementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class AddAdvertisementStep1Type extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder->add('advertisement_type', 'hidden');
        
    }

    public function getName() {
        return 'addadvertisementstep1';
    }

}


?>
