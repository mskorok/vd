<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\AdvertisementBundle\Entity\PaymentTransaction;
use VisiDarbi\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Regex;

use Symfony\Component\Form\FormView;
use  Symfony\Component\Form\FormInterface;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class HighlightAdvertisementStep2Type extends DummyTransAbstractType {

    protected $withPassword;
    protected $user;
    protected $withVatValidation;

    public function __construct($user = null, $withVatValidation = true) {

        $this->withVatValidation = $withVatValidation;
        $this->user = $user;
        
        if(!$user) {
            $this->withPassword = true;
        }
        
        
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('companyName', 'text', array('required' => true,  'max_length'=>'50', 'label' => 'Company name',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Company name must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Company name can\'t be empty.')))
        )));
        $builder->add('email', 'email', array('required' => true, 'max_length'=>'50', 'label' => 'E-mail',
            'constraints' =>
            array(
                new Email(array('message' => $this->trans('Incorrect email address.'))),
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Email must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Contact person name can\'t be empty.')))
//        )) + (($this->user)?array('attr'=>array('value' => $this->user->getEmail())):array()));
        )));

        $builder->add('password', 'password', array('label' => 'Password', 'max_length'=>'20', 'attr'=>array('class'=>'hidden-pass'),
            'constraints' =>
            array(
                new MaxLength(array('limit' => 20, 'message' => $this->trans('Password must be not longer than 16 characters.'))),
        )));

        $builder->add('password_repeat', 'password', array('label' => 'Password', 'attr'=>array('class'=>'hidden-pass')));
        
        
//        $builder->add('email', 'email', array('required' => true,  'max_length'=>'50', 'label' => 'E-mail',
//            'constraints' =>
//            array(
//                new Email(array('message' => 'Incorrect email address.')),
//                new MaxLength(array('limit' => 50, 'message' => 'Email must be not longer than 50 characters.')),
//                new NotBlank(array('message' => 'Contact person name can\'t be empty.'))
//        )));
//        
        
        $builder->add('address', 'text', array('required' => true,  'max_length'=>'200', 'label' => 'Company adress',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 200, 'message' => $this->trans('Address must be not longer than 200 characters.'))),
                new NotBlank(array('message' => $this->trans('Address can\'t be empty.')))
        )));
        $builder->add('contactPersonName', 'text', array('required' => true,  'max_length'=>'100', 'label' => 'Contact person name',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 100, 'message' => $this->trans('Contact person name must be not longer than 100 characters.'))),
                new NotBlank(array('message' => $this->trans('Contact person name can\'t be empty.')))
        )));
        $builder->add('vat', 'text',
            User::vatValidationParams(
                array(
                    'required' => $this->withVatValidation ? true : false, 'label' => 'VAT',
                    'maxMessage' => $this->trans("VAT must not be longer than {{ limit }} characters"),
                    'notBlankMessage' => $this->trans('VAT can\'t be empty.'),
                    'regexMessage' => $this->trans('VAT must contain only alphanumeric symbols.'),
                ), 'advertisement', $this->withVatValidation
            ));
        $builder->add('noVat', 'checkbox', array('required' => false,  'max_length'=>'11', 'label' => 'No VAT'));
        $builder->add('registrationNumber', 'text',
            User::registrationNumValidationParams(
                array(
                    'required' => true, 'label' => 'Registration number',
                    'maxMessage' => $this->trans('Registration number must not be longer than {{ limit }} characters'),
                    'minMessage' => $this->trans('Registration number must be more or equal to {{ limit }} characters'),
                    'notBlankMessage' => $this->trans('Registration number can\'t be empty.'),
                    'regexMessage' => $this->trans('Registration number must contain only alphanumeric symbols.'),
                ), 'advertisement'
            ));
        $builder->add('bankCode', 'text', array('required' => true,  'max_length'=>'50', 'label' => 'Bank code',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Bank code must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Bank code can\'t be empty.')))
        )));
        $builder->add('bankAccount', 'text', array('required' => true, 'max_length'=>'21', 'label' => 'Bank Account',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 21, 'message' => $this->trans('Bank account code must be not longer than 21 characters.'))),
                new NotBlank(array('message' => $this->trans('Bank account can\'t be empty.')))
        )));
        $builder->add('onFirstPage', 'checkbox', array('required' => false, 'label' => 'Place on first page'));
        $builder->add('priceFirstPage', 'hidden', array('required' => false));
        $builder->add('priceFirstPageSecond', 'hidden', array('required' => false));
        
        $builder->add('advertisementHlPrice', 'hidden', array('required' => false));
        $builder->add('termsAndConditions', 'checkbox', array('required' => true, 'label' => 'I agree',
            'constraints' =>
            array(
                new NotBlank(array('message' => $this->trans('You must agree with terms and conditions'))))));

        $builder->add('payment', 'choice', array('expanded' => true, 'multiple' => false, 'choices' => array(
            PaymentTransaction::PYMENT_DNB => '',
            PaymentTransaction::PYMENT_NORDEA => '',
            PaymentTransaction::PYMENT_SEB => '',
            PaymentTransaction::PYMENT_SWEDBANK => '',
            PaymentTransaction::PYMENT_PAYPAL => '',
        ), 'required' => true,  'label' => 'Payment method',
            'constraints' =>
            array(
                new NotBlank(array('message' => $this->trans('Payment method must be selected')))
            )));
    }

    public function getName() {
        return 'addadvertisementstep4';
    }

    public function getDefaultOptions(array $options) {
        return array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
                // 'intention'       => 'task_item',
        );
    }

    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['withPassword'] =  $this->withPassword;
    }   
    
}

?>
