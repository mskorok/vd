<?php


namespace VisiDarbi\AdvertisementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VisiDarbi\AdvertisementBundle\Form\DataTransformer\UploadedFileTransformer;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;


class AdvertisementAdminFileType extends AbstractType {


    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['file_name'] = $options['file_name'];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver
            ->setDefaults(array(
                'data_class' => null,
                'required' => false,
                'mapped' => false,
                'by_reference' => false,
                'file_name' => ''
            ))
        ;
    }

    public function getParent() {
        return 'file';
    }

    public function getName() {
        return 'advertisement_admin_file';
    }
}
