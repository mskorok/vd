<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Callback;

use VisiDarbi\UserBundle\Validator\Constraints\UniqSubscriberEmail;

class SubscribeAdvertisementType extends DummyTransAbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['user_authorized']) {

            $builder->add('email', 'email', array(
                    'attr' => array('maxlength' => 255),
                    'error_bubbling' => false,
                    'label' => 'Your e-mail',
                    'constraints' => array(
                        new Email(array('message' => $this->trans('Incorrect email'))),
                        new NotBlank(array('message' => $this->trans('Email can\'t be empty.'))),
                        new MaxLength(array('limit' => 255, 'message' => $this->trans('Email must be not longer than 255 characters.'))),
                        new UniqSubscriberEmail(array('message' => $this->trans('Email already registered')))
                    )
                )
            );

            $builder->add('captcha', 'genemu_captcha', array('error_bubbling' => false, 'label' => 'Security code.'));
        }

        $builder->add('filter', 'hidden', array());
        $builder->add('searchWhere', 'hidden', array());
        $builder->add('searchWhat', 'hidden', array());


    }

    public function getName() {
        return 'subscribeadvertisementform';
    }


    public function getDefaultOptions(array $options) {
        return array(
            'user_authorized' => false,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
                // 'intention'       => 'task_item',
        );
    }

}
