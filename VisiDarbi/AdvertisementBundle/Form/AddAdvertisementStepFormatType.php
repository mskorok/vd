<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class AddAdvertisementStepFormatType extends AbstractType {

    private $translator;

    public function __construct(TranslatorInterface $translator = null) {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $labels = array();

        $builder->add('format', 'choice', array('expanded' => true, 'multiple' => false, 'choices' => Advertisement::getFormatChoices(), 'required' => true,  'label' => false,
            'constraints' =>
                array(
                    new NotBlank(array('message' => $this->translator->trans('Choose how to add advertisement')))
                )));
        $builder->add('tooltips_dir', 'hidden', array(
            'property_path' => false,
        ));
    }

    public function getName() {
        return 'addadvertisementstepformat';
    }

    public function getDefaultOptions(array $options) {
        return array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // 'intention'       => 'task_item',
        );
    }

}

?>
