<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class AddAdvertisementStep2Type extends DummyTransAbstractType {

    protected $countries;
    protected $categories;
    protected $professions;
    protected $cities;
    protected $em;

    public function __construct($categories, $countries, $locale, $er) {
        $this->categories = $categories;
        $this->countries = $countries;
        $this->em = $er;

        $query = $this->em->createQueryBuilder('p')
                ->select('p.name, t.slug')
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->join('p.translations', 't')
                ->andWhere("t.locale = :locale")
                ->orderBy('p.name')
                ->groupBy('t.slug')
                ->setParameter('locale', $locale);
        $data = $query->getQuery();
        $data->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $data = $data->getArrayResult();

        foreach ($data as $k => $v) {
            $this->professions[$v['slug']] = $v ['name'];
        }



        $data = $er->createQueryBuilder('p')
                ->select('c.id, c.name')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'c');
        $data = $data->getQuery();
        $data->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $data = $data->getArrayResult();

        foreach ($data as $k => $v) {
            $this->cities[$v['id']] = $v ['name'];
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('category', 'choice', array('error_bubbling' => false, 'multiple' => true, 'choices' => $this->categories, 'required' => true, 'label' => 'Category',
            'constraints' => array(
                new NotBlank(array('message' => $this->trans('Category must be selected')))
        )));
        $builder->add('profession', 'choice', array(
            'choices' => $this->professions,
            'error_bubbling' => false, 'multiple' => true, 'required' => true, 'label' => 'Profession',
            'constraints' => array(
                new NotBlank(array('message' => $this->trans('Profession must be selected')))
        )));
        $builder->add('country', 'hidden', array('error_bubbling' => false, 'required' => true,
            'constraints' => array(
                new NotBlank(array('message' => $this->trans('Country must be selected')))
        )));
        $builder->add('city', 'choice', array(
            'choices' => $this->cities,
            'error_bubbling' => false, 'required' => false, 'empty_data' => null, 'label' => 'City'));
        $builder->add('country_data', 'choice', array('error_bubbling' => false, 'choices' => $this->countries, 'required' => false, 'label' => 'Country'));

        $builder->add('pay_now', 'hidden', array('required' => false, 'attr'=>array('class'=>'pay_now_field')));
    }

    public function getName() {
        return 'addadvertisementstep2';
    }

    public function getDefaultOptions(array $options) {
        return array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
                // 'intention'       => 'task_item',
        );
    }

}

?>
