<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Callback;

class ShareAdvertisementType extends DummyTransAbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder->add('share_id', 'hidden', array('error_bubbling' => false, 'attr'=>array('value'=>'', 'class'=>'share_id')));
        $builder->add('email_from', 'email', array( 'attr'=> array('maxlength'=>50), 'error_bubbling' => false, 'label'=>'E-mail', 'constraints' =>
            array(
                new Email(array('message' => $this->trans('Incorrect sender email'))),
                new NotBlank(array('message' => $this->trans('Sender email can\'t be empty.'))),
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Sender email must be not longer than 50 characters.'))))));
        $builder->add('email_to', 'email', array('attr'=> array('maxlength'=>50), 'error_bubbling' => false, 'required'=>true, 'label'=>'Recipient e-mail', 'constraints' =>
            array(
                new Email(array('message' => $this->trans('Recipient email - incorrect email address.'))),
                new NotBlank(array('message' => $this->trans('Recipient email can\'t be empty.'))),
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Recipient email must be not longer than 100 characters.'))))));
        $builder->add('message', 'textarea', array('error_bubbling' => false, 'label'=>'Hi! I suggest you this vacance from visidarbi.lv.', 'constraints' =>
            array(
                new MaxLength(array('limit' => 100, 'message' => $this->trans('Message must be not longer than 100 characters.'))))));
        $builder->add('captcha', 'genemu_captcha', array('error_bubbling' => false, 'label'=>'Security code.'));
        
    }

    public function getName() {
        return 'shareadvertisementform';
    }

}
