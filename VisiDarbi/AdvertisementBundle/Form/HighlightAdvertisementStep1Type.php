<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use VisiDarbi\AdvertisementBundle\Form\HighlightAdvertisementStep1CompanyType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class HighlightAdvertisementStep1Type extends DummyTransAbstractType {

    protected $locales;
    protected $defaultLocale;
    protected $countries;
    protected $categories;
    protected $professions;
    protected $cities;
    protected $em;    

    public function __construct($locales, $defaultLocale, $categories, $countries, $locale, $er) {
        
        $this->locales = $locales;
        $this->defaultLocale = $defaultLocale;
        
        $this->categories = $categories;
        $this->countries = $countries;
        $this->em = $er;

        $query = $this->em->createQueryBuilder('p')
                ->select('p.name, t.slug')
                ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
                ->join('p.translations', 't')
                ->andWhere("t.locale = :locale")
                ->orderBy('p.name')
                ->groupBy('t.slug')
                ->setParameter('locale', $locale);
        $data = $query->getQuery();
        $data->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $data = $data->getArrayResult();

        foreach ($data as $k => $v) {
            $this->professions[$v['slug']] = $v ['name'];
        }

        $data = $er->createQueryBuilder('p')
                ->select('c.id, c.name')
                ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'c');
        $data = $data->getQuery();
        $data->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN, false)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        $data = $data->getArrayResult();

        foreach ($data as $k => $v) {
            $this->cities[$v['id']] = $v ['name'];
        }        
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('category', 'choice', array('error_bubbling' => false, 'multiple' => true, 'choices' => $this->categories, 'required' => true, 'label' => 'Category',
            'constraints' => array(
                new NotBlank(array('message' => $this->trans('Category must be selected')))
        )));
        $builder->add('profession', 'choice', array(
            'choices' => $this->professions,
            'error_bubbling' => false, 'multiple' => true, 'required' => true, 'label' => 'Profession',
            'constraints' => array(
                new NotBlank(array('message' => $this->trans('Profession must be selected')))
        )));
        $builder->add('country', 'hidden', array('error_bubbling' => false, 'required' => true,
            'constraints' => array(
                new NotBlank(array('message' => $this->trans('Country must be selected')))
        )));
        $builder->add('city', 'choice', array(
            'choices' => $this->cities,
            'error_bubbling' => false, 'required' => false, 'empty_data' => null, 'label' => 'City'));
        $builder->add('country_data', 'choice', array('error_bubbling' => false, 'choices' => $this->countries, 'required' => false, 'label' => 'Country'));        
        
        foreach ($this->locales as $locale) {

            $builder->add('title_' . $locale, 'text', array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Title',
                'constraints' =>
                array(
                    new MaxLength(array('limit' => 50, 'message' => str_replace('%locale%', $locale, $this->trans('Title must be not longer than 50 characters (%locale%).')))),
                    ($locale == $this->defaultLocale) ? new NotBlank(array('message' => str_replace('%locale%', $locale, $this->trans('Title can\'t be empty (%locale%).')))) : new Callback(array('methods' => array()))
                )));
            $builder->add('description_' . $locale, 'textarea', array('required' => ($locale == $this->defaultLocale) ? true : false, 'label' => 'Description',
                'constraints' =>
                array(
                    new MaxLength(array('limit' => 1000, 'message' => str_replace('%locale%', $locale, $this->trans('Description must be not longer than 1000 characters (%locale%).')))),
                    ($locale == $this->defaultLocale) ? new NotBlank(array('message' => str_replace('%locale%', $locale, $this->trans('Description can\'t be empty (%locale%).')))) : new Callback(array('methods' => array()))
                )));
        }

        $builder->add('company', new HighlightAdvertisementStep1CompanyType($this->locales, $this->defaultLocale));
    }

    public function getName() {
        return 'highlightadvertisementstep1';
    }

    public function getDefaultOptions(array $options) {
        return array(
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            // 'intention'       => 'task_item',
        );
    }

}

?>
