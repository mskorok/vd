<?php

namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * HighlightAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class HighlightAdvertisementStep1CompanyType extends DummyTransAbstractType {

    protected $locales;
    protected $defaultLocale;

    public function __construct($locales, $defaultLocale) {
        $this->locales = $locales;
        $this->defaultLocale = $defaultLocale;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('name', 'text', array('required' => true, 'label' => 'Company name',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Company name must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Company name can\'t be empty.')))
        )));
    }

    public function getName() {
        return 'highlightadvertisementstep1company';
    }

}

?>
