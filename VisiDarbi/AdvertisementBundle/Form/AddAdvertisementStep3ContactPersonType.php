<?php


namespace VisiDarbi\AdvertisementBundle\Form;

use VisiDarbi\CommonBundle\Form\DummyTransAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * AddAdvertisementStep1Type form
 *
 * @author Aleksey
 */
class AddAdvertisementStep3ContactPersonType extends DummyTransAbstractType {

    protected $locales;
    protected $defaultLocale;

    public function __construct($locales, $defaultLocale, $translator = null) {
        $this->locales = $locales;
        $this->defaultLocale = $defaultLocale;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('name', 'text', array('required'=>true, 'label'=>'Name',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Contact person name must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Contact person name can\'t be empty.')))
        )));
        $builder->add('email', 'email', array('required'=>true, 'label'=>'Email',
            'constraints' =>
            array(
                new Email(array('message' => $this->trans('Contact person incorrect email address.'))),
                new MaxLength(array('limit' => 50, 'message' => $this->trans('Contact person email must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Contact person email can\'t be empty.')))
        )));
        $builder->add('phone', 'text', array('required'=>true, 'label'=>'Phone',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 13, 'message' => $this->trans('Contact person phone must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Contact person phone can\'t be empty.')))
        )));
        $builder->add('fax', 'text', array('required'=>false, 'label'=>'Fax',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 13, 'message' => $this->trans('Contact person fax must be not longer than 50 characters.'))),
        )));
        $builder->add('url', 'text', array('required'=>false, 'label'=>'Home page address',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 255, 'message' => $this->trans('Home page address must be not longer than 50 characters.'))),
                new Url(array('message' => $this->trans('Incorrect home page address')))
        )));;
        $builder->add('actual_address', 'text', array('required'=>true, 'label'=>'Actual address',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 200, 'message' => $this->trans('Actual address must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Actual address can\'t be empty.')))
        )));
        $builder->add('legal_address', 'text', array('required'=>true, 'label'=>'Legal address',
            'constraints' =>
            array(
                new MaxLength(array('limit' => 200, 'message' => $this->trans('Legal address must be not longer than 50 characters.'))),
                new NotBlank(array('message' => $this->trans('Legal address can\'t be empty.')))
        )));


        foreach($this->locales as $locale) {
            $builder->add('position_'.$locale, 'text', array( 'required'=>false, 'label'=>'Position',
                'constraints' =>
                        array(
                            new MaxLength(array('limit' => 50, 'message' => ($this->trans('Position be not longer than 50 characters (%locale%).',['%locale%' => $locale],'messages')))),
                )));
        }
  }

    public function getName() {
        return 'addadvertisementstep3contactperson';
    }


}


?>
