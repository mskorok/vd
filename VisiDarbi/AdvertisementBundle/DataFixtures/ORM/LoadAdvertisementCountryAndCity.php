<?php

namespace VisiDarbi\AdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountryTranslation;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementCityTranslation;
class LoadAdvertisementCountryAndCity extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        
        
        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $this->getReference('country-lat');
        $locales = $country->getLocales();


        for ($i = 1; $i <= 2; $i++) {

            $advertisementCountry = new AdvertisementCountry();
            $advertisementCountry->setCountry($country);
            $advertisementCountry->setName('Country #' . $i);

            foreach ($locales as $k => $v) {
                $advertisementCountry->addTranslation(new AdvertisementCountryTranslation($v, 'name', 'Country #' . $i . ' - ' . $v));
            }

            $manager->persist($advertisementCountry);
            
            if($i == 1) {
                $country->setDeafultAdvertisementCountry($advertisementCountry);
                $manager->persist($country);
            }

            for ($ii = 1; $ii <= 30; $ii++) {
                $advertisementCity = new AdvertisementCity();
                $advertisementCity->setName('City #' . $i . '_' . $ii);
                $advertisementCity->setMarkOnFrontend(rand(0,1));

                foreach ($locales as $k => $v) {
                    $advertisementCity->addTranslation(new AdvertisementCityTranslation($v, 'name', 'City #' . $i . '_' . $ii . ' - ' . $v));
                }

                $advertisementCity->setAdvertisementCountry($advertisementCountry);
                $manager->persist($advertisementCity);
            }
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 45;
    }

}
