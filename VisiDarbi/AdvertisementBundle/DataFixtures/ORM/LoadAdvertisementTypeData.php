<?php

namespace VisiDarbi\AdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementType;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice;


class LoadAdvertisementTypeData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
 
        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $country = $this->getReference('country-lat');
        $locales = $country->getLocales();        
        
        //7 days limited
        $advertisementType = new AdvertisementType();
        $advertisementType->setId(AdvertisementType::TYPE_LIMITED_7_DAY);        
        $advertisementType->setPayd(true);
        $advertisementType->setUnlimited(false);
        $advertisementType->setDaysPeriod(7);
        $advertisementType->setName('7 days');
        
        foreach ($locales as $k => $v) {
            $advertisementType->addTranslation(new AdvertisementTypeTranslation($v, 'name', '7 days'));
        }        
        
        $manager->persist($advertisementType);
        
        $typePrice = new AdvertisingTypePrice();
        $typePrice->setCountry($country);
        $typePrice->setAdvertisementType($advertisementType);
        $typePrice->setPrice($this->getRandomPrice());
        $typePrice->setDescription( $advertisementType->getName());
        $manager->persist($typePrice);      
        
        //14 days limited
        $advertisementType = new AdvertisementType();
        $advertisementType->setId(AdvertisementType::TYPE_LIMITED_14_DAY);        
        $advertisementType->setPayd(true);
        $advertisementType->setUnlimited(false);
        $advertisementType->setDaysPeriod(14);
        $advertisementType->setName('14 days');
        
        foreach ($locales as $k => $v) {
            $advertisementType->addTranslation(new AdvertisementTypeTranslation($v, 'name', '14 days'));
        }        
        
        $manager->persist($advertisementType);

        $typePrice = new AdvertisingTypePrice();
        $typePrice->setDescription( $advertisementType->getName());
        $typePrice->setCountry($country);
        $typePrice->setAdvertisementType($advertisementType);
        $typePrice->setPrice($this->getRandomPrice());
        $manager->persist($typePrice);         
        
        //7 days unlimited
        $advertisementType = new AdvertisementType();
        $advertisementType->setId(AdvertisementType::TYPE_UNLIMITED_7_DAY);
        $advertisementType->setPayd(true);
        $advertisementType->setUnlimited(true);
        $advertisementType->setDaysPeriod(7);
        $advertisementType->setName('7 days unlimited');
        
        foreach ($locales as $k => $v) {
            $advertisementType->addTranslation(new AdvertisementTypeTranslation($v, 'name', '7 unlimited'));
        }        
        
        $manager->persist($advertisementType);           

        $typePrice = new AdvertisingTypePrice();
        $typePrice->setDescription( $advertisementType->getName());
        $typePrice->setCountry($country);
        $typePrice->setAdvertisementType($advertisementType);
        $typePrice->setPrice($this->getRandomPrice());
        $manager->persist($typePrice); 
        
        //14 days unlimited
        $advertisementType = new AdvertisementType();
        $advertisementType->setId(AdvertisementType::TYPE_UNLIMITED_14_DAY);
        $advertisementType->setPayd(true);
        $advertisementType->setUnlimited(true);
        $advertisementType->setDaysPeriod(14);
        $advertisementType->setName('14 days unlimited');
        
        foreach ($locales as $k => $v) {
            $advertisementType->addTranslation(new AdvertisementTypeTranslation($v, 'name', '14 unlimited'));
        }        
        
        $manager->persist($advertisementType);        

        $typePrice = new AdvertisingTypePrice();
        $typePrice->setDescription( $advertisementType->getName());
        $typePrice->setCountry($country);
        $typePrice->setAdvertisementType($advertisementType);
        $typePrice->setPrice($this->getRandomPrice());
        $manager->persist($typePrice); 
        
        $manager->flush();
    
        
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 40;
    }

    private function getRandomPrice() {
        return rand(0*10, 50*10) / 10;
    }
}
