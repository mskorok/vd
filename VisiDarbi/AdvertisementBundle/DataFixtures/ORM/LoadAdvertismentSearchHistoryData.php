<?php

namespace VisiDarbi\AdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use VisiDarbi\AdvertisementBundle\Entity\AdvertismentSearchHistory;

class LoadAdvertismentSearchHistoryData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) 
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        
        return;
        
        $country = $this->getReference('country-lat');
        $user = $manager->getRepository('VisiDarbiUserBundle:User')->findOneBy(array( 'country' => $country, 'email' => 'user1@example.com', 'is_admin' => false ));   
        
        if ($user != null) {
            
            for ($i = 0; $i < 48; $i++ ) {
                
                $history = new AdvertismentSearchHistory();
                $history->setUser($user);
                $history->setProfessionKeyword($this->generateRandomString(rand(5, 17)));
                $history->setPlaceKeyword($this->generateRandomString(rand(5, 17)));
                $history->setSearchCount(rand(1, 30));
                
                
                $createdAt = new \DateTime('now');
                $createdAt->sub(new \DateInterval('P10D'));
                $history->setCreatedAt($createdAt);
                

                if ($history->getSearchCount() == 1) {
                    $history->setLastSearchedAt($createdAt);
                }
                else {
                    $updatedAt = clone  $createdAt;
                    $updatedAt->add(new \DateInterval('PT'. rand(5, 120) .'H'));
                    $history->setLastSearchedAt($updatedAt);
                }

                
                $manager->persist($history);
            }
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() 
    {
        return 57;
    }
    
    function generateRandomString($length = 10) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
