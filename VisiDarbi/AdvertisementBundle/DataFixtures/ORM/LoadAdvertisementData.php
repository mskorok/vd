<?php

namespace VisiDarbi\AdvertisementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VisiDarbi\ProfessionBundle\Entity\Category as Category;
use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementType;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementTypeTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Company;
use VisiDarbi\AdvertisementBundle\Entity\CompanyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Duty;
use VisiDarbi\AdvertisementBundle\Entity\DutyTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Offer;
use VisiDarbi\AdvertisementBundle\Entity\OfferTranslation;
use VisiDarbi\AdvertisementBundle\Entity\Requirement;
use VisiDarbi\AdvertisementBundle\Entity\Requisites;
use VisiDarbi\AdvertisementBundle\Entity\RequirementTranslation;
use VisiDarbi\AdvertisementBundle\Entity\ContactPerson;
use VisiDarbi\AdvertisementBundle\Entity\ContactPersonTranslation;


use VisiDarbi\ProfessionBundle\Entity\Profession as Profession;
use VisiDarbi\ProfessionBundle\Entity\CategoryTranslation;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadAdvertisementData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        return;

        $abc = array('a','b','d','f','g','x','q','p','w');
        $companyName = null;
        $advertisimentCount = 30;

        /** @var VisiDarbi\LocalePlaceBundle\Entity\Country $country */
        $advertisementType =  $manager->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementType')->findOneBy(array('id' =>  AdvertisementType::TYPE_LIMITED_14_DAY));
        $professions = $manager->getRepository('VisiDarbi\ProfessionBundle\Entity\Profession')->findAll();
        $user = $manager->getRepository('VisiDarbi\UserBundle\Entity\LegalProfile')->findOneBy(array())->getUser();

        $countryInfo =  $manager->getRepository('VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry')->findAll();
        $countryInfoCount = count($countryInfo);


        for($i=1; $i<=$advertisimentCount; $i++) {

            $profession = $professions[rand(0, count($professions)-1)];
            $category = $profession->getCategory();
            $country = $category->getCountry();
            $locales = $country->getLocales();

            $advertisement = new Advertisement();
            $advertisement->setCategory($category);
            $advertisement->setProfession($profession);
            $advertisement->setAdvertisementType($advertisementType);
            $advertisement->setCountry($country);
            $now = new \DateTime();
            $advertisement->setCretaedAt(clone $now);
            $advertisement->setDateFrom(clone $now);
            $advertisement->setUpdatedAt(clone $now);
            $advertisement->setDateTo($now->add(new \DateInterval('P7D')));
            $advertisement->setUser($user);

            $advertisement->setTitle('Advertisement title #'.$i);
            $advertisement->setDescription('Advertisement description '.$i);
            $advertisement->setAdditionalDescription('Advertisement additional description '.$i);
            $advertisement->setStatus(Advertisement::STATUS_ACTIVE);

            //with 30% chance place advertisment on startpage
            if (\mt_rand(0, 2) == 1) {
                $advertisement->setOnStartpage(true);
            }

            foreach($locales as $locale) {
                $advertisement->addTranslation(new AdvertisementTranslation($locale, 'title', 'Advertisement title #'.$i.$locale));
                $advertisement->addTranslation(new AdvertisementTranslation($locale, 'description', 'Advertisement description '.$i.$locale));
                $advertisement->addTranslation(new AdvertisementTranslation($locale, 'additional_description', 'Advertisement additional description '.$i.$locale));
            }

            $advertisement->setUser($user);

            if(rand(0,1)) {
                $countryToSet = $countryInfo[rand(0, $countryInfoCount - 1)];
            } else {
                $countryToSet = $countryInfo[0];
            }
            $advertisement->setAdvertisementCountry($countryToSet);

            if(rand(0,1)) {
               $city = $countryToSet->getAdvertisementCities();
               $city = $city[rand(0, count($city)-1)];
               $advertisement->setAdvertisementCity($city);
            }

            $manager->persist($advertisement);
            $user->getLegalProfile()->setLastAdvertisementAt(new \DateTime());
            $manager->persist($user);
            //Create contact person

            $contactPerson = new ContactPerson();
            $contactPerson->setEmail('contactpersonemail'.$i.'@example.com');
            $contactPerson->setActualAddress('Actual Address '.$i);
            $contactPerson->setFax('123-123-12'.$i);
            $contactPerson->setHomeAddress('Home address '.$i);
            $contactPerson->setLegalAddress('Legal address '.$i);
            $contactPerson->setName('Name of person '.$i);
            $contactPerson->setPhone('123-123-12'.$i);
            $contactPerson->setPosition('person position '.$i);

            foreach($locales as $locale) {
                $contactPerson->addTranslation(new ContactPersonTranslation($locale, 'position', 'person position '.$i.$locale));
            }

            $contactPerson->setAdvertisement($advertisement);
            $manager->persist($contactPerson);

            //Create duties
            for($ii=1; $ii<=3; $ii++){
                $duty = new Duty();
                $duty->setDescription('Duty description '.$i.' - '.$ii);

                foreach($locales as $locale) {
                    $duty->addTranslation(new DutyTranslation($locale, 'description', 'Duty '.$i.$locale.' - '.$ii));
                }

                $duty->setAdvertisement($advertisement);
                $manager->persist($duty);
            }

            //Create offers
            for($ii=1; $ii<=3; $ii++){
                $offer = new Offer();
                $offer->setDescription('Offer description '.$i.' - '.$ii);

                foreach($locales as $locale) {
                    $offer->addTranslation(new OfferTranslation($locale, 'description', 'Offer '.$i.$locale.' - '.$ii));
                }

                $offer->setAdvertisement($advertisement);
                $manager->persist($offer);
            }

            //Create Requirement
            for($ii=1; $ii<=3; $ii++){
                $requirement = new Requirement();
                $requirement->setDescription('Requirement description '.$i.' - '.$ii);

                foreach($locales as $locale) {
                    $requirement->addTranslation(new RequirementTranslation($locale, 'description', 'Offer '.$i.$locale.' - '.$ii));
                }

                $requirement->setAdvertisement($advertisement);
                $manager->persist($requirement);
            }


            //Create company
            $company = new Company();

            if(rand(0,1) || !$companyName){
                $companyName = $abc[rand(0,count($abc)-1)].'SIA ACME (АБЦ)#'.$i;
            }

            $companyRegNr = rand(10000, 999999);

            $company->setName($companyName);
            $company->setRegistrationNumber($companyRegNr);
            $company->setDescription('Company description'.$i);
            foreach($locales as $locale) {
                $company->addTranslation(new CompanyTranslation($locale, 'description', 'Company description'.$i.$locale));
            }

            $company->setAdvertisement($advertisement);
            $manager->persist($company);



            //Create requisites
            $requisites = new Requisites();
            $requisites->setAddress('Requisites address '.$i);
            $requisites->setBankAccount('Requisites bank account '.$i);
            $requisites->setBankCode('Requisites bank kode '.$i);
            $requisites->setCompanyName('Requisites company ACME#'.$i);
            $requisites->setContactPersonName('Requisites contact persone '.$i);
            $requisites->setEmail('requisites.email'.$i.'@example.com');
            $requisites->setPVN('PVN123456789-'.$i);
            $requisites->setNoPVN(false);
            $requisites->setRegistrationNumber('Requisites reg. number #'.$i);


            $requisites->setAdvertisement($advertisement);
            $manager->persist($requisites);

        }

        $manager->flush();

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 50;
    }

}
