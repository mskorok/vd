<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;
use Doctrine\ORM\EntityRepository;


/**
 * Description of AdvertisementAdmin
 *
 * @author Aleksey
 */
class AdvertisementCityAdmin extends CommonAdmin {

    protected $baseRouteName = 'advertisementcity';
    protected $baseRoutePattern = 'advertisementcity';

    protected $ownerId = null;
    
    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->add('name', null, array('label' => 'City name'))
                ->add('mark_on_frontend', null, array('label' => 'Mark on frontend'))
                ->add('AdvertisementCountry', null, array('label' => 'Country'))
                ->add('_advertisementsCount', null, array(
                    'label' => 'Advertisement',
                    'sortable' => false,
                    'template' => 'VisiDarbiAdvertisementBundle:CitiesAdmin:advertisement_count.html.twig'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        
        $attr = array();
        
        if ($this->getRoot()->getClassnameLabel() == 'AdvertisementCountry') {           
            $this->ownerId = $this->getRoot()->getSubject()->getId();           
            
            $attr = array('readonly'=>'readonly', 'style' => 'width: 300px;');
        }        
        
        $formMapper
                ->add('name', 'a2lix_translations', $this->getTranslatableFieldParam(
                                'VisiDarbi\AdvertisementBundle\Entity\AdvertisementCity', 'name', 'Name', true))
                ->add('mark_on_frontend', null, array('label' => 'Mark on frontend'))
                ->add('AdvertisementCountry', 'entity', array('attr'=>$attr,'label' => 'Country', 'required' => true, 'class' => 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'property' => 'name', 'query_builder' =>
                    function(EntityRepository $er) {
                    
                        $q = $er->createQueryBuilder('fms')
                         ->orderBy('t.content', 'ASC');            
                        $q = $this->sortByTranslations($q);
                        
                        if($this->ownerId){
                            $q->andWhere('fms.id = :ownerId');
                            $q->setParameter('ownerId', $this->ownerId);
                        }
                        
                        return $q;
                    }))
        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('id', null, array('label' => 'ID'))
                ->add('name', null, array('label' => 'Name'))
                ->add('AdvertisementCountry', null, array('label' => 'Country'))

        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

    public function createQuery($context = 'list') {

        $query = $this->getModelManager()->createQuery($this->getClass(), 'o');
        $query->select(array('o', 'c'))
                ->innerJoin('o.AdvertisementCountry', 'c')
                ->where('c.country = :country')
                ->setParameter(':country', $this->getCountryManager()->getCurrentAdminCountry());

        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

}

?>
