<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\Requirement;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Requirement
 *
 * @author Aleksey
 */
class RequirementAdmin extends CommonAdmin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('description', 'a2lix_translations',
                     $this->getTranslatableFieldParam(
                             'VisiDarbi\AdvertisementBundle\Entity\Requirement', 
                             'description', 'Description',true))                
        ;
    }
}