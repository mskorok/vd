<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementHtmlImage;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;
use VisiDarbi\AdvertisementBundle\Form\AdvertisementAdminFileType;

class HtmlImageAdmin extends CommonAdmin {


    protected function configureFormFields(FormMapper $formMapper) {

        $htmlImageTranslations = array();

        $locales = $this->container->get('visidarbi.country_manager')->getCurrentAdminCountry()->getLocales();

        $data = $formMapper->getFormBuilder()->getForm()->getViewData();
        if (!empty($data) && $data instanceof AdvertisementHtmlImage) {
            $id = $data->getId();
            if (!empty($id)) {
                $imgTranslations = $data->getTranslations();
                foreach ($imgTranslations as $t) {
                    $htmlImageTranslations[$t->getLocale()][$t->getField()] = $t->getContent();
                }
            }
        }
        foreach ($locales as $locale) {
            $formMapper->add('html_image_'.$locale, new AdvertisementAdminFileType(), array(
                    'label' => 'Image - '.$locale,
                    'file_name' => !empty($htmlImageTranslations[$locale]['image']) ? $htmlImageTranslations[$locale]['image'] : ''
                ), array('type' => 'advertisement_admin_file'));
        }
    }
}

?>
