<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use VisiDarbi\AdvertisementBundle\Entity\Advertisement;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\Offer;
use VisiDarbi\UserBundle\Entity\User;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;
use JMS\TranslationBundle\Annotation\Ignore;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\File\File;
use VisiDarbi\AdvertisementBundle\Form\AdvertisementAdminFileType;

/**
 * Description of AdvertisementAdmin
 *
 * @author Aleksey
 */
class AdvertisementAdmin extends CommonAdmin {

    protected $baseRouteName = 'advertisement';
    protected $baseRoutePattern = 'advertisement';


    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            //->add('country', null, array('label' => 'System country'))
            ->add('AdvertisementCountry', null, array('label' => 'Country'))
            ->add('AdvertisementCity', null, array('label' => 'City'))
            ->add('_map_to', null, array(
                'label' => 'Profession',
                'template' => 'VisiDarbiAdvertisementBundle:AdvertisementAdmin:advertisement_profession.html.twig'
            ))

            // ->add('profession', null, array('label' => 'Profession'))
            ->add('companies.name', null, array('label' => 'Company'))
            ->add('title', null, array('label' => 'Title'))
            ->add('description', null, array('label' => 'Description', 'template' => 'VisiDarbiCommonBundle:Admin:description_short.html.twig'))
            ->add('cretaed_at', 'datetime', array('widget' => 'single_text',
                'datetime_format' => 'yyy-m-d',))
            ->add('externalAdvertisement.externalAdvertisementSource', 'null', array('label' => 'Source',
                'template' => 'VisiDarbiAdvertisementBundle:AdvertisementAdmin:advertisement_source.html.twig'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    //'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))

        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $currentCountry = $this->container->get('visidarbi.country_manager')->getCurrentAdminCountry();
        $locales = $currentCountry->getLocales();

        $advertisement = $this->getSubject();

        if (!$advertisement->getId()) {
            if ($this->getRequest()->get('format', null)) {
                $advertisement->setFormat($this->getRequest()->get('format'));
            } else {
                $formId = $this->getUniqid();
                $requestData = $this->getRequest()->request->get($formId);
                if (!empty($requestData['format'])) {
                    $advertisement->setFormat($requestData['format']);
                }
            }

        }
        $format = $advertisement->getFormat();

        $isSocial = false;

        if ($advertisement->getExternalAdvertisement()) {
            $isSocial = $advertisement->getExternalAdvertisement()->getExternalAdvertisementSource()->getIsSocial();
        }

        if (!$advertisement->getId()) {
            $formMapper->add('format', 'choice', array(
                'choices' => Advertisement::getFormatChoices(),
                'data' => $advertisement->getFormat(),
                'attr' => array('class' => 'format-selector span5')
            ));
        }

        $formMapper
            ->add('logo', 'file', array(
                'label' => 'Update logo',
                'required' => false,
                'mapped' => false,
                'help' => '<div id="logoPreview"></div>',
                'attr' => array('class' => 'logo-field')
            ));

        if ($advertisement->getCompany() && $advertisement->getCompany()->getLogo()) {
            $formMapper->add('delete_logo', 'checkbox', array(
                'label' => 'Delete logo',
                'required' => false,
                'mapped' => false,
                'attr' => array('class' => 'delete-logo')
            ));
        }
        $formMapper->add('Categories', null, array(
            'class' => 'VisiDarbi\ProfessionBundle\Entity\Category', 'property' => 'name', 'query_builder' =>
                function(EntityRepository $er) {

                    $q = $er->createQueryBuilder('fms')
                        ->orderBy('t.content', 'ASC');

                    $q = $this->sortByTranslations($q);

                    return $q->andWhere('fms.country = :country')
                        ->setParameter('country', $this->getCountryManager()->getCurrentAdminCountry());
                },
            'label' => 'Category',
            'required' => true,
            'attr' => array('class' => 'category-selector span5')
        ))


//                ->add('Professions', 'choice', array('multiple'=>true,
//                    'choices' => $this->getProfessionsChoiceData(),
//
//                    'label' => 'Profession',
//                    'required' => false,
//                    'attr' => array('class' => 'profession-selector span5')
//                ))
        ;
        $pathInfo = $this->getRequest()->getPathInfo();

        if (strstr($pathInfo, 'append-form-field-element') === false) {

            $formMapper->add('Professions', null, array(
                'empty_value'=>false,
                'multiple' => true,
                'label' => 'Profession',
                'required' => false,
                'attr' => array('class' => 'profession-selector span5')
            ));
        }

        $formMapper->add('on_startpage', null, array('label' => 'Izcelt sākumlapā', 'required' => false));
        $formMapper->add('is_highlighted', null, array('label' => 'Izcelt sludinājumu sarakstā', 'required' => false));
        $formMapper->add('is_highlighted_mobile', null, array('label' => 'Izcelt sludinājumu sarakstā mobile', 'required' => false));

        $formMapper->add('AdvertisementCountry', null, array(
            'class' => 'VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 'property' => 'name', 'query_builder' =>
                function(EntityRepository $er) {

                    $q = $er->createQueryBuilder('fms')
                        ->orderBy('t.content', 'ASC');

                    $q = $this->sortByTranslations($q);

                    return $q->andWhere('fms.country = :country')
                        ->setParameter('country', $this->getCountryManager()->getCurrentAdminCountry());
                },
            'label' => 'Country',
            'required' => true,
            'attr' => array('class' => 'country-selector span5')
        ))
            ->add('AdvertisementCity', null, array(
                'label' => 'City',
                'required' => false,
                'attr' => array('class' => 'city-selector span5')
            ));

        $fields = array('title'=>array('type'=>'text'));
        if ($format == Advertisement::FORMAT_FREE) {
            $fields['content'] = array('type'=>'ckeditor', 'config'=>array('width'=>'650px'), 'config_name' => 'advertisement',);
        } elseif (!in_array($format, array(Advertisement::FORMAT_IMAGE_PDF, Advertisement::FORMAT_HTML))) {
            $fields['description'] = array('type'=>'textarea');
            $fields['additional_description'] = array('type'=>'textarea');
        }
        $formMapper->add('Translatable', 'a2lix_translations', $this->getTranslatableFieldParam(
                'VisiDarbi\AdvertisementBundle\Entity\Advertisement', 'title', 'Title', false,
                array(
                    'remove_extra_fields' => true,
                    'required_locales' => array($this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale()),
                    'fields' => $fields
                )
            )
        );

        $translations = array();
        $advTranslations = $advertisement->getTranslations();
        foreach ($advTranslations as $t) {
            $translations[$t->getLocale()][$t->getField()] = $t->getContent();
        }

        if(in_array($format, array(Advertisement::FORMAT_IMAGE_PDF, Advertisement::FORMAT_HTML))) {
            foreach ($locales as $locale) {
                $formMapper->add('file_format_'.$locale, new AdvertisementAdminFileType(), array(
                        'label' => $format == Advertisement::FORMAT_IMAGE_PDF ? 'Upload picture/pdf file - '.$locale : 'Upload HTML file - '.$locale,
                        'file_name' => !empty($translations[$locale]['file_format']) ? $translations[$locale]['file_format'] : ''
                    ), array('type' => 'advertisement_admin_file'));
                if (!empty($translations[$locale]['file_format'])) {
                    $formMapper->add('delete_file_format_'.$locale, 'checkbox', array(
                        'label' => 'Delete '.($format == Advertisement::FORMAT_IMAGE_PDF ? 'picture/pdf file' : 'HTML file'),
                        'required' => false,
                        'mapped' => false,
                    ));
                }
            }
            if ($format == Advertisement::FORMAT_HTML) {
                $formMapper
                    ->add('advertisementHtmlImages', 'sonata_type_collection', array('sonata_admin' => 'visidarbi.advertisement.admin.advertisementhtmlimage', 'required' => false, 'by_reference' => false, 'mapped'=>true, 'label' => 'Upload image'), array(
                        'edit' => 'inline',
                        'inline' => 'table',
                    ));
            }

        }

        $formMapper->add('date_from', 'date', array(
            'format' => 'yyyy-MM-dd',
            'label' => 'Date from',
            'required' => true,
        ))
            ->add('date_to', 'date', array(
                'label' => 'Date to',
                'format' => 'yyyy-MM-dd',
                'required' => true
            ))
            ->add('status', 'choice', array(
                'choices' => array(
                    \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_ACTIVE => 'Active',
                    \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_INACTIVE => 'Inactive',
                    \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_INACTIVE_CLOSED => 'Inactive - closed',
                    \VisiDarbi\AdvertisementBundle\Entity\Advertisement::STATUS_PENDING => 'Pending',
                ),
                'data' => $advertisement->getStatus()
            ))
            ->add('advertisementType', null, array(
                'required' => true,
                'label' => 'Type'));

        if (empty($format) || $format == Advertisement::FORMAT_FULL) {
            $formMapper->with('Offers', array('collapsed' => false))
                ->add('offers', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => 'Offers'), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
                ->end()
                ->with('Duties', array('collapsed' => false))
                ->add('dutieses', 'sonata_type_collection', array('sonata_admin' => 'visidarbi.advertisement.admin.duty', 'required' => false, 'by_reference' => false, 'label' => 'Duties'), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
                ->end()
                ->with('Requirements', array('collapsed' => false))
                ->add('requirementses', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => 'Requirements'), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
                ->end();
        }

        $formMapper->with('Company', array('collapsed' => false))
            ->add('companies', 'sonata_type_admin', array('label' => /** @Ignore */false), array('required' => true, 'edit' => 'inline'))
            ->end();

        if (!$isSocial) {
            $formMapper->end()
                ->with('Contact Person', array('collapsed' => false))
                ->add('contactPerson', 'sonata_type_admin', array('label' => /** @Ignore */false), array('required' => false, 'edit' => 'inline'))
                ->end();
        }

        if (!$isSocial) {
            $formMapper->with('Requisites', array('collapsed' => false))
                ->add('requisites', 'sonata_type_admin', array('label' => /** @Ignore */false), array('required' => false, 'edit' => 'inline'))
                ->end();
        }
        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
			->add('id', null, array('label' => 'ID'))
            ->add('text', 'doctrine_orm_callback', array(
            'callback' => array($this, 'getSearchFilter'),
            'label' => 'Serach by text',
            'field_type' => 'text'))
            ->add('publicId', null, array('label' => 'Public ID'))
            ->add('Professions.id', 'doctrine_orm_choice', array('label' => 'Professions',
                'field_options' => array(
                    'choices' => $this->getProfessionsChoice(),
                ),
                'field_type' => 'choice'
            ))
            ->add('Categories', null, array('label' => 'Categories', 'field_options' => array('class' => 'VisiDarbi\ProfessionBundle\Entity\Category', 'property' => 'name', 'query_builder' =>
                function(EntityRepository $er) {

                    $q = $er->createQueryBuilder('fms')
                        ->orderBy('t.content', 'ASC');

                    $q->join('fms.translations', 't');
                    $q->andWhere('t.field = :field')
                        ->andWhere('t.locale =:locale')
                        ->setParameter('field', 'name')
                        ->setParameter('locale', $this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale());

                    return $q;
                })))
            ->add('companies.name', null, array('label' => 'Company'))
            ->add('country', null, array('label' => 'Country'))
            ->add('cretaed_at', null, array('label' => 'Added at', 'field_options' => array('date_format' => 'yyyy-MM-dd')))
            ->add('externalAdvertisement.externalAdvertisementSource', 'doctrine_orm_callback', 
                    array(
                    'callback' => array($this, 'getAdSourceFilter'),
                    'field_type' => 'choices',                        
                    'label' => 'Source',
                    'field_options' => array(
                    'choices' => $this->getSourceChoice(),
                ),
                'field_type' => 'choice'
            ))
                                                
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

    public function getTemplate($name) {
        if ($name == 'edit') {
            return 'VisiDarbiAdvertisementBundle:AdvertisementAdmin:advertisement_edit.html.twig';
        }
        return parent::getTemplate($name);
    }

    public function prePersist($advertisement) {
        parent::prePersist($advertisement);
        $formId = $this->getUniqid();
        $requestData = $this->getRequest()->request->get($formId);
        $defaultLocale = $this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale();
        if (!empty($requestData['Translatable'][$defaultLocale]['title'])) {
            $advertisement->setTitle($requestData['Translatable'][$defaultLocale]['title']);
        }
        $this->saveFile($advertisement);
        $advertisement->generatePublicId();
        $this->saveFileFormat($advertisement);
    }

    public function preUpdate($advertisement) {
        parent::preUpdate($advertisement);
        $this->saveFile($advertisement);
        $this->saveFileFormat($advertisement);
        //Clear adv list cache
        $this->getEm()->getConfiguration()->getResultCacheImpl()->deleteAll();
    }

    public function saveFileFormat($advertisement) {
        $formId = $this->getUniqid();
        $requestData = $this->getRequest()->request->get($formId);
        $files = $this->getRequest()->files->get($formId);
        $format_files = array();
        $uploadConfig = $this->getAdvertisementManager()->getUploadConfig($advertisement->getFormat());
        $locales = $this->container->get('visidarbi.country_manager')->getCurrentAdminCountry()->getLocales();
        $advTranslations = $advertisement->getTranslations();
        $old_data = array();
        foreach ($advTranslations as $t) {
            if ($t->getField() == 'file_format') {
                $old_data[$t->getLocale()] = $t->getContent();
            }
        }

        $htmlImageTranslations = array();
        if ($advertisement->getFormat() == Advertisement::FORMAT_HTML) {
            foreach ($advertisement->getAdvertisementHtmlImages() as $ind=>$htmlImage) {
                $id = $htmlImage->getId();
                $image = $htmlImage->getImage();
                if (empty($id) &&  empty($image)) {
                    $advertisement->removeAdvertisementHtmlImage($htmlImage);
                } else {
                    $imgTranslations = $htmlImage->getTranslations();
                    foreach ($imgTranslations as $t) {
                        $htmlImageTranslations[$ind][$t->getLocale()][$t->getField()] = $t->getContent();
                    }
                }

            }
        }

        foreach ($locales as $locale) {
            $path = '';
            if (!empty($files['file_format_'.$locale])) {
                $file = $files['file_format_'.$locale];
                $extension = $file->guessExtension();
                $newName = md5(uniqid($file->getClientOriginalName(), true)) . '.' . $extension;
                $webPath = __DIR__ . '/../../../../web';
                $path = $webPath . '/' . $uploadConfig['tmp_path'] . '/'. $newName;
                $file->move($webPath . '/' . $uploadConfig['tmp_path'] . '/', $newName);
                $fileNameOrig = $uploadConfig['translitirate_file_name'] ? $this->getAdvertisementManager()->getTranslitirateFileName($file->getClientOriginalName()) : $file->getClientOriginalName();
            } elseif(!empty($old_data[$locale])) {
                $fileNameOrig = $old_data[$locale];
                $dir = $this->getAdvertisementManager()->getFileFormatDir($advertisement, $locale);
                if (!empty($dir)) {
                    $path = $dir . '/'.$fileNameOrig;
                }
            }
            if (!empty($path) && is_file($path)) {
                if (!empty($requestData['delete_file_format_'.$locale])) {
                    $path = '';
                    $fileNameOrig = '';
                }
                $format_files[$locale]['file_format'] = array(
                    'locale' => $locale,
                    'fileTmpPath' => $path,
                    'fileNameOrig' => $fileNameOrig
                );
            }
        }
        if ($advertisement->getFormat() == Advertisement::FORMAT_HTML && !empty($files['advertisementHtmlImages']) && is_array($files['advertisementHtmlImages'])) {
            foreach ($files['advertisementHtmlImages'] as $ind=>$htmlImage) {
                foreach ($locales as $i=>$locale) {
                    $path = '';
                    if (!empty($htmlImage['html_image_'.$locale])) {
                        $file = $htmlImage['html_image_'.$locale];
                        $extension = $file->guessExtension();
                        $newName = md5(uniqid($file->getClientOriginalName(), true)) . '.' . $extension;
                        $webPath = __DIR__ . '/../../../../web';
                        $path = $webPath . '/' . $uploadConfig['tmp_path'] . '/'. $newName;
                        $file->move($webPath . '/' . $uploadConfig['tmp_path'] . '/', $newName);
                        $fileNameOrig = $file->getClientOriginalName();
                    }
                    if (!empty($path) && is_file($path)) {
                        $format_files[$locale]['html_image'][$ind] = array(
                            'locale' => $locale,
                            'fileTmpPath' => $path,
                            'fileNameOrig' => $fileNameOrig
                        );
                    }
                }
            }

            foreach ($htmlImageTranslations as $ind => $oldData) {
                foreach ($oldData as $locale => $data) {
                    if (empty($format_files[$locale]['html_image'][$ind]) && !empty($data['image'])) {
                        $fileNameOrig = $data['image'];
                        $dir = $this->getAdvertisementManager()->getFileFormatDir($advertisement, $locale, 'html_image');
                        if (!empty($dir)) {
                            $path = $dir . '/'.$fileNameOrig;
                            if (!empty($path) && is_file($path)) {
                                $format_files[$locale]['html_image'][$ind] = array(
                                    'locale' => $locale,
                                    'fileTmpPath' => $path,
                                    'fileNameOrig' => $fileNameOrig
                                );
                            }
                        }
                    }
                }
            }
        }
        if (!empty($format_files)) {
            $this->getAdvertisementManager()->setFileFormat($advertisement, $format_files, false);
            return true;
        }
        return false;

    }

    public function saveFile($advertisement) {

        $formId = $this->getUniqid();
        $requestData = $this->getRequest()->request->get($formId);

        if (isset($requestData['delete_logo']) && $requestData['delete_logo']) {
            unlink($advertisement->getCompany()->getFullLogoPath());
            $advertisement->getCompany()->setLogo(null);
            return;
        }


        $files = $this->getRequest()->files->get($formId);

        if (isset($files['logo'])) {

            $uploadConfig = $this->container->getParameter('upload_logo');
            $file = $files['logo'];
            $extension = $file->guessExtension();

            $newName = md5(uniqid($file->getClientOriginalName(), true)) . '.' . $extension;

            $webPath = __DIR__ . '/../../../../web';

            $file->move($webPath . '/' . $uploadConfig['stored_path'] . '/', $newName);

            if ($advertisement->getCompany()->getLogo() != $newName) {
                $advertisement->getCompany()->removeLogoOnRemove();
            }
            $advertisement->getCompany()->setLogo($newName);
            $advertisement->getCompany()->setLogoPath($uploadConfig['stored_path']);
        }

    }

    public function getFormTheme(){
        return array_merge(
            parent::getFormTheme(),
            array('VisiDarbiAdvertisementBundle:Form:advertisement_file_field.html.twig')
        );
    }

    public function validate(\Sonata\AdminBundle\Validator\ErrorElement $errorElement, $object) {
        parent::validate($errorElement, $object);

        $isSocial = false;
        if ($object->getExternalAdvertisement()) {
            $isSocial = $object->getExternalAdvertisement()->getExternalAdvertisementSource()->getIsSocial();
        }
        if (!$isSocial) {
            $reg_num_params = User::registrationNumValidationParams(array(), 'admin');
            $vat_params = User::vatValidationParams(array(), 'admin');
            $errorElement
                ->with('requisites.registration_number')
                    ->assertLength(array('max' => $reg_num_params['max_length'], 'min'=>$reg_num_params['min_length'], 'minMessage'=>'[0,Inf]Registration number must be more or equal to {{ limit }} characters', 'maxMessage'=>'[0,Inf]Registration number must not be longer than {{ limit }} characters'))
                    ->assertRegex(array('pattern' => $reg_num_params['pattern'], 'message'=>'Registration number must contain only alphanumeric symbols.'))
                ->end()
                ->with('requisites.PVN')
                    ->assertMaxLength(array('limit' => $vat_params['max_length'], 'message'=>'[0,Inf]VAT number must not be longer than {{ limit }} characters'))
                    ->assertRegex(array('pattern' => $vat_params['pattern'], 'message'=>'VAT number must contain only alphanumeric symbols.'))
                ->end()
            ;
        }

        $formId = $this->getUniqid();
        $files = $this->getRequest()->files->get($formId);

        $file = $files['logo'];

        if ($file) {
            $uploadConfig = $this->getAdvertisementManager()->getUploadConfig('upload_logo');
            $this->validateFile($file, $uploadConfig);
        }
        $locales = $this->container->get('visidarbi.country_manager')->getCurrentAdminCountry()->getLocales();
        foreach ($locales as $locale) {
            if (!empty($files['file_format_'.$locale])) {
                $uploadConfig = $this->getAdvertisementManager()->getUploadConfig($object->getFormat());
                $this->validateFile($files['file_format_'.$locale], $uploadConfig);
            }
        }
        if ($object->getFormat() == Advertisement::FORMAT_HTML && !empty($files['advertisementHtmlImages'])) {
            foreach ($files['advertisementHtmlImages'] as $htmlImage) {
                foreach ($locales as $locale) {
                    if (!empty($htmlImage['html_image_'.$locale])) {
                        $uploadConfig = $this->getAdvertisementManager()->getUploadConfig('html_image');
                        $this->validateFile($htmlImage['html_image_'.$locale], $uploadConfig);
                    }
                }
            }
        }

    }

    private function validateFile($file, $uploadConfig) {
        if (empty($file) || empty($uploadConfig)) {
            return;
        }
        $mimeType = $file->getMimeType();
        $extension = $file->guessExtension();
        $size = $file->getSize();

        if (!in_array($mimeType, explode(',', $uploadConfig['allow_type']))) {
            $this->getForm()->addError(new \Symfony\Component\Form\FormError($this->trans('Incorrect file type')));
        }

        if ($size > $uploadConfig['max_size']) {
            $this->getForm()->addError(
                new \Symfony\Component\Form\FormError(
                    $this->trans('Max file size mus be %size% MB.', array('%size%' => $uploadConfig['max_size_view']))));
        }
    }

    private function getProfessionsChoice() {

        $professionsList = array();

        $query = $this->getEm()->createQueryBuilder('p')
            ->select('p, c')
            ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
            ->join('p.category', 'c')
            ->where('c.country = :country')
            ->orderBy('c.name, p.name')
            ->setParameter('country', $this->getCountryManager()->getCurrentAdminCountry());

        $query = $query->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $r = $query->getResult();

        foreach ($r as $p) {
            $professionsList[$p->getId()] = $p->getAdminName();
        }

        return $professionsList;
    }

    private function getProfessionsChoiceData() {

        $professionsList = array();

        $query = $this->getEm()->createQueryBuilder('p')
            ->select('p')
            ->from('VisiDarbi\ProfessionBundle\Entity\Profession', 'p')
            ->join('p.category', 'c')
            ->where('c.country = :country')
            ->orderBy('c.name, p.name')
            ->setParameter('country', $this->getCountryManager()->getCurrentAdminCountry());

        $query = $query->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $this->getCountryManager()->getCurrentAdminCountry()->getDefaultLocale())
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $r = $query->getResult();


        foreach ($r as $p) {
            $professionsList[$p->getId()] = $p;
        }

        return $professionsList;
    }

    private function getAdvertisementManager() {
        return $this->container->get('visidarbi.advertisement_manager');
    }


    public function getAdSourceFilter($queryBuilder, $alias, $field, $value) {

        if (!$value['value'] && $value['value']!==-100) {
            return;
        }
        
        
        if($value['value']==-100) {
            
            $queryBuilder->andWhere($alias.'.externalAdvertisement IS NULL');
            
            return true;
            
        }

        
        $queryBuilder->leftJoin($alias.'.externalAdvertisement', 'ea')
                ->leftJoin('ea.externalAdvertisementSource', 's');
        
        $queryBuilder->andWhere('s.id = :value')
                ->setParameter('value', $value['value']);
            

        return true;
    }
    
    
    public function getSearchFilter($queryBuilder, $alias, $field, $value) {

        if (!$value['value']) {
            return;
        }

        $exp = new \Doctrine\ORM\Query\Expr();

        $adTranslationsCondition = '((t.field = :fieldDescription OR t.field = :fieldTitle)' . ' AND ' . $exp->like('t.content', $exp->literal('%' . $value['value'] . '%')) . ')';

        $exp1 = new \Doctrine\ORM\Query\Expr();

        $adContactConditionPhone = '(' . $exp1->like('cp.phone', $exp1->literal('%' . $value['value'] . '%')) . ')';

        $exp2 = new \Doctrine\ORM\Query\Expr();

        $adContactConditionMail = '(' . $exp2->like('cp.email', $exp2->literal('%' . $value['value'] . '%')) . ')';


        $queryBuilder->leftJoin($alias . '.translations', 't')
                ->leftJoin($alias . '.contactPerson', 'cp')
                ->andWhere($adTranslationsCondition . ' OR' . $adContactConditionPhone . ' OR ' . $adContactConditionMail)
                ->setParameter('fieldDescription', 'description')
                ->setParameter('fieldTitle', 'title')
        ;

        return true;
    }

    public function getSourceChoice() {

        $sourceList = array('-100' => 'visidarbi');

        $query = $this->getEm()->createQueryBuilder('s')
                ->select('s')
                ->from('VisiDarbi\ExternalAdvertisementBundle\Entity\ExternalAdvertisementSource', 's')
                ->orderBy('s.resource');

        $query = $query->getQuery()
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $r = $query->getResult();


        foreach ($r as $s) {
            $sourceList[$s->getId()] = $s;
        }

        return $sourceList;
    }

}

?>
