<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\Duty;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;
/**
 * Description of DutyAdmin
 *
 * @author Aleksey
 */
class DutyAdmin extends CommonAdmin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('description', 'a2lix_translations',
                     $this->getTranslatableFieldParam(
                             'VisiDarbi\AdvertisementBundle\Entity\Duty', 
                             'description', 'Description',true))
        ;
    }

}

?>
