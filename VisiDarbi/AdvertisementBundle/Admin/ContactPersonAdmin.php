<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\ConatctPerson;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of DutyAdmin
 *
 * @author Aleksey
 */
class ContactPersonAdmin extends CommonAdmin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('name', null, array(
                    'label' => 'Name',
                    'required' => false
                ))
                
                ->add('position', 'a2lix_translations',
                     $this->getTranslatableFieldParam(
                             'VisiDarbi\AdvertisementBundle\Entity\ContactPerson', 
                             'position', 'Position',false)) 
                ->add('email', null, array(
                    'label' => 'Email',
                    'required' => false
                ))
                ->add('phone', null, array(
                    'label' => 'Phone',
                    'required' => false
                ))
                ->add('fax', null, array(
                    'label' => 'Fax',
                    'required' => false
                ))
                ->add('actual_address', null, array(
                    'label' => 'Actual address',
                    'required' => false
                ))
                ->add('legal_address', null, array(
                    'label' => 'Legal address',
                    'required' => false
                ))
                ->add('url', null, array(
                    'label' => 'Home page',
                    'required' => false
                ))        
;
    }

}

?>
