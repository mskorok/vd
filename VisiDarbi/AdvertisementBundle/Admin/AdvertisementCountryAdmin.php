<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of AdvertisementAdmin
 *
 * @author Aleksey
 */
class AdvertisementCountryAdmin extends CommonAdmin {

    protected $baseRouteName = 'advertisementcountry';
    protected $baseRoutePattern = 'advertisementcountry';

    protected function configureListFields(ListMapper $listMapper) {
        
        $q = $this->getObjectIdentifier();
        
        $listMapper
                ->addIdentifier('id', 'string', array('label' => 'ID'))
                ->add('name', 'string', array('label' => 'Country name'))
                ->add('_citiesCount', null, array(
                    'label' => 'Cities count',
                    'sortable' => false,
                    'template' => 'VisiDarbiAdvertisementBundle:CountriesAdmin:cities_count.html.twig'))
                ->add('_advertisementsCount', null, array(
                    'label' => 'Advertisement',
                    'sortable' => false,
                    'template' => 'VisiDarbiAdvertisementBundle:CountriesAdmin:advertisement_count.html.twig'))                
                ->add('_action', 'actions', array(
                    'actions' => array(
                        //'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                    'label' => 'Actions'
                ))                
                ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('name', 'a2lix_translations',
                     $this->getTranslatableFieldParam(
                             'VisiDarbi\AdvertisementBundle\Entity\AdvertisementCountry', 
                             'name', 'Name',true))                    
                ->add('AdvertisementCities', 'sonata_type_collection', 
                        array('type_options'=>array( 'delete'=>false), 'required' => false, 'by_reference' => false, 'label' => 'Cities'), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position'
                ))            
            ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name', null, array('label' => 'Name'))
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }

}

?>
