<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\Offer;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * AdvertisingTypePriceAdmin class
 *
 * @author Aleksey
 */
class AdvertisingTypePriceAdmin extends CommonAdmin {

    protected $baseRouteName = 'advertisementprice';
    protected $baseRoutePattern = 'advertisementprice';

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                //->add('country', null, array('label' => 'Country'))
                ->add('advertisementType', null, array('label' => 'Advertisement Type'))
                ->add('price', null, array('label' => 'Price'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                    ),
                    'label' => 'Actions'
                ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('advertisementType', null, array(
                    'label' => 'Advertisement Type', 
                    'read_only' => true,
                    'disabled' => true,
                    'required' => true
                ))
                ->add('price', null, array(
                    'label' => 'Price',
                    'required' => true
                ))
                ->add('most_popular', null, array(
                    'label' => 'Is most popular',
                    'required' => false
                ))
                ->add('translatable', 'a2lix_translations',
                     $this->getTranslatableFieldParam(
                             'VisiDarbi\AdvertisementBundle\Entity\Advertisement', 
                             'title', 'Title', false, array('fields' => array('description'=>array('type'=>'ckeditor', 'required' => false, 
                    'attr' => array('class' => 'ckeditor span5'),
                    'config_name' => 'admin'),
                    'payment_description'=>array('type'=>'text', 'required' => false, 
                                                'attr' => array('class' => 'span5')),                                 
                    'payment_description_first_page'=>array('type'=>'text', 'required' => false, 
                                                'attr' => array('class' => 'span5')),                                 
                    'payment_description_higlighting'=>array('type'=>'text', 'required' => false, 
                                                'attr' => array('class' => 'span5')),
                    'payment_description_mobile_highlighting'=>array('type'=>'text', 'required' => false,
                                                'attr' => array('class' => 'span5')),
                    'payment_description_period'=>array('type'=>'text', 'required' => false, 
                                                'attr' => array('class' => 'span5'))                                 
                                 
                                 
                                 ))))                
//                ->add('description', 'ckeditor', array(
//                    'required' => false, 
//                    'attr' => array('class' => 'ckeditor span5'),
//                    'config_name' => 'admin'
//                ))                
        ;
    }

    public function getBatchActions() {

        //remove batch actions;
        //$actions = parent::getBatchActions();
        return array();
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
//            ->remove('edit')
        ;
    }    

}

?>
