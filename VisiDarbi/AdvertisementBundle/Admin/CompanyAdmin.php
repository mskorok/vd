<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\ConatctPerson;
use VisiDarbi\CommonBundle\Admin\CommonAdmin;

/**
 * Description of DutyAdmin
 *
 * @author Aleksey
 */
class CompanyAdmin extends CommonAdmin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('name', null, array(
                    'label' => 'Name',
                    'required' => false
                ))
                ->add('registration_number', null, array(
                    'label' => 'Registration number',
                    'required' => false
                ))

                ->add('description', 'a2lix_translations',
                     $this->getTranslatableFieldParam(
                             'VisiDarbi\AdvertisementBundle\Entity\ContactPerson',
                             'description', 'Description',false))

                ;
    }

}

?>
