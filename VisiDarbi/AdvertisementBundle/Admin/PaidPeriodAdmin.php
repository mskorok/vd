<?php

namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\PaidPeriod;
use VisiDarbi\LocalePlaceBundle\Manager\CountryManagerInterface;
use VisiDarbi\EmailTemplatingBundle\Entity\EmailTemplate;
use VisiDarbi\AdvertisementBundle\Entity\AdvertisementType;
use Doctrine\ORM\EntityManager;

class PaidPeriodAdmin extends Admin
{
    protected $parentAssociationMapping = 'user';

    public function configureRoutes(RouteCollection $collection)
    {
        /**
         * remove route and button in form
         */
        $collection->remove('edit');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$parent = $this->getParent()->getObject($this->getParent()->getRequest()->get('id'));
        //var_dump($parent);
        //die();
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('advertisementType', null, array('label' => 'Advertisement type'))
            ->add('from_date', null, array('label' => 'Active from'))
            ->add('to_date', null, array('label' => 'Active to'))
            ->add('advertisementsLeft', 'string', array('label' => 'Advertisements left'))
            //->add('status', 'string', array(
            //    'label' => 'Status',
            //    'template' => 'VisiDarbiCommonBundle:Admin:Field/list_dictionary.html.twig',
            //    'dictionary' => array(
            //        PaidPeriod::STATUS_PENDING => 'Pending',
            //        PaidPeriod::STATUS_ACTIVE => 'Active',
            //        PaidPeriod::STATUS_CLOSED => 'Closed',
            //    )
            //))
            ->add('_action', 'actions', array(
                'actions' => array(
                    //'edit' => array(),
                    'delete' => array(),
                ),
                'label' => 'Actions'
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('advertisementType', null, array(
                'required' => true,
                'label' => 'Advertisement type',
            ))

            //->add('allowHighlight', null, array(
            //    'label' => 'Allow highlight',
            //))
        ;
    }

    public function prePersist($paidPeriod)
    {
        /* @var $paidPeriod PaidPeriod*/
        $now = new \DateTime('now');

        $dateFrom = new \DateTime('now');
        $dateTo = clone $dateFrom;
        $dateTo->add(new \DateInterval('P' . $paidPeriod->getAdvertisementType()->getDaysPeriod() . 'D'));

        $paidPeriod->setFromDate($dateFrom);
        $paidPeriod->setToDate($dateTo);
        $paidPeriod->setStatus(PaidPeriod::STATUS_ACTIVE);

        $paidPeriod->setCreatedByAdmin(true);
    }

    public function postPersist($paidPeriod)
    {
        $this->sendNotification($paidPeriod, EmailTemplate::ID_SERVICE_ACTIVATED);
    }

    public function postRemove($paidPeriod)
    {
        $this->sendNotification($paidPeriod, EmailTemplate::ID_SERVICE_DEACTIVATED);
    }

    private function sendNotification(PaidPeriod $service, $template)
    {
        $country = $this->countryManager->getCurrentAdminCountry();
        $locale = $country->getDefaultLocale();
        $this->emailSender->send(
            $country,
            $locale,
            $template,
            $service->getUser()->getEmail(),
            array(
                'serviceTitle' => $this->getAdvTypeTitle($service->getAdvertisementType(), $locale),
            ),
            array(
                'email' => $this->settingsManager->get('emails.sender_email'),
                'name' => $this->settingsManager->get('emails.sender_name')
            )
        );
    }

    protected function getAdvTypeTitle(AdvertisementType $advertisementType, $locale)
    {
        $qb = $this->em->createQueryBuilder('pp')
            ->select('atp')
            ->from('VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice', 'atp')
            ->where('atp.advertisementType = :advertisementType')
            ->setParameter(':advertisementType', $advertisementType->getId())
        ;

        $query = $qb->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        $advTypePrice = $query->getOneOrNullResult();
        /* @var $advTypePrice \VisiDarbi\AdvertisementBundle\Entity\AdvertisingTypePrice */

        return $advTypePrice == null ? '' : strip_tags($advTypePrice->getDescription());
    }

    /**
     * @var CountryManagerInterface
     */
    protected $countryManager;

    /**
     * @var \VisiDarbi\EmailTemplatingBundle\EmailSender
     */
    protected $emailSender;

    /**
     * @var \VisiDarbi\SettingsBundle\Manager\SettingsManagerInterface
     */
    protected $settingsManager;

    /**
     * @var EntityManager
     */
    protected $em;

    public function setCountryManager(CountryManagerInterface $cm)
    {
        $this->countryManager = $cm;
    }


    public function setEmailSender($emailSender)
    {
        $this->emailSender = $emailSender;
    }

    public  function setSettingsManager($settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'VisiDarbiCommonBundle:Admin:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
