<?php
namespace VisiDarbi\AdvertisementBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use VisiDarbi\AdvertisementBundle\Entity\Requisites;

/**
 * Requisites
 *
 * @author Aleksey
 */
class RequisitesAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add( 'company_name', null, array(
                    'label' => 'Company name',
                    'required' => false)
                )
                ->add( 'email', null, array(
                    'label' => 'Email',
                    'required' => false)
                )            
                ->add( 'address', null, array(
                    'label' => 'Address',
                    'required' => false)
                )
                ->add( 'contact_person_name', null, array(
                    'label' => 'Contact person name',
                    'required' => false)
                )
                ->add( 'PVN', null, array(
                    'label' => 'PVN',
                    'required' => false)
                )                
                ->add( 'no_PVN', null, array(
                    'label' => 'No PVN',
                    'required' => false)
                )
                ->add( 'registration_number', null, array(
                    'label' => 'Registration number',
                    'required' => false)
                )
                ->add( 'bank_code', null, array(
                    'label' => 'Bank kode',
                    'required' => false)
                )                 
                ->add( 'bank_account', null, array(
                    'label' => 'Bank account',
                    'required' => false)
                )
            ;
    }
}